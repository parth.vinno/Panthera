package com.nollysoft.config;

import java.util.concurrent.TimeUnit;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.nollysoft.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PcidssRequirement.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SaqQuestion.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Address.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AssessorCompany.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Organization.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.RequirementCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.CompensatingControlWorksheet.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.HostingProvider.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ManagedService.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentProcessing.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ScopeVerificationSelectedService.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ScopeVerificationSelectedService.class.getName() + ".hostingProviders", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ScopeVerificationSelectedService.class.getName() + ".managedServices", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ScopeVerificationSelectedService.class.getName() + ".paymentProcessings", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Country.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.State.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ScopeVerificationNotSelectedService.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ServiceType.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SelectedServiceType.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ServiceTypeNotSelected.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentCardBusinessDescription.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Location.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.EnvironmentDescription.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ThirdPartyServiceProvider.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ServiceByServiceProvider.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ThirdPartyServiceProvider.class.getName() + ".serviceByServiceProviders", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentApplicationList.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentApplication.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentApplication.class.getName() + ".paymentApplicationLists", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.TestedRequirementSummary.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ComplianceAttestation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AcknowledgementStatement.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AcknowledgementStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Attestation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.QSAAttestation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ISAAttestation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AOCLegalExceptionDetails.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.NonCompliantRequirementActionPlan.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SaqInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AssessmentInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.UserGroup.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.UserGroup.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.User.class.getName() + ".userGroups", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Resource.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Resource.class.getName() + ".userGroups", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SaqResponse.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ExpectedTest.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.CompensatingControlWorksheetResponse.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.RequirementNotTestedExplanation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.NonApplicabilityExplanation.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.NonApplicabilityExplanation.class.getName() + ".pcidssRequirementIds", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.QANotification.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SaqReview.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AOCNotification.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.QSANotification.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.SaqReviewComment.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ServiceCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.MerchantBusinessType.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.MerchantBusinessTypeSelected.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentChannel.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentChannelBusinessServes.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.PaymentChannelSaqCovers.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.OtherServiceResponse.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.TestedRequirementHeader.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Tenant.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Tenant.class.getName() + ".addressIds", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.AssessmentType.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Industry.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Privilege.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.Authority.class.getName() + ".privileges", jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.ExecutiveSummaryStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.nollysoft.domain.OrganizationSummaryStatus.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
