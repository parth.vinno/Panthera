package com.nollysoft.config;

import org.springframework.core.env.Environment;

/**
 * @author bhavinsuhas 
 * 
 * This class is responsible to fetch environment properties based on key.
 */
public class MessageSource {

	public static Environment environment;

	public static Environment setInstance(Environment environmentArg) {
		environment = environmentArg;
		return environment;
	}

	public static String getMsg(String key) {
		return environment.getProperty(key);
	}

}
