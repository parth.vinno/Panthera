package com.nollysoft.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class RoutingDataSource extends AbstractRoutingDataSource {
	@Override
	protected Object determineCurrentLookupKey() {
		System.out.println(DbContextHolder.getDbType());
		return DbContextHolder.getDbType();
	}
}
