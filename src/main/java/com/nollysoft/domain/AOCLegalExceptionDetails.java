package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AOCLegalExceptionDetails.
 */
@Entity
@Table(name = "aoc_legal_exception_detail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AOCLegalExceptionDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "legal_constraint_detail")
	private String legalConstraintDetail;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pcidss_requirement_id")
	private PcidssRequirement pcidssRequirementId;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLegalConstraintDetail() {
		return legalConstraintDetail;
	}

	public AOCLegalExceptionDetails legalConstraintDetail(String legalConstraintDetail) {
		this.legalConstraintDetail = legalConstraintDetail;
		return this;
	}

	public void setLegalConstraintDetail(String legalConstraintDetail) {
		this.legalConstraintDetail = legalConstraintDetail;
	}

	public PcidssRequirement getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public AOCLegalExceptionDetails pcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
		return this;
	}

	public void setPcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AOCLegalExceptionDetails aocLegalExceptionDetails = (AOCLegalExceptionDetails) o;
		if (aocLegalExceptionDetails.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), aocLegalExceptionDetails.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("AOCLegalExceptionDetails{").append("id=" + getId())
				.append(", legalConstraintDetail='" + getLegalConstraintDetail() + "'")
				.append(", pcidssRequirementId=" + getPcidssRequirementId() + "}").toString();
	}
}
