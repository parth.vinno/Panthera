package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AOCNotification.
 */
@Entity
@Table(name = "aoc_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AOCNotification implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "notification_date", nullable = false)
	private LocalDate notificationDate;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "aoc_id")
	private User aocId;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "qa_notification_id")
	private QANotification qaNotificationId;
	
	@PrePersist
	public void onCreate(){
		this.notificationDate = LocalDate.now();
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getNotificationDate() {
		return notificationDate;
	}

	public AOCNotification notificationDate(LocalDate notificationDate) {
		this.notificationDate = notificationDate;
		return this;
	}

	public void setNotificationDate(LocalDate notificationDate) {
		this.notificationDate = notificationDate;
	}

	public User getAocId() {
		return aocId;
	}

	public AOCNotification aocId(User aocId) {
		this.aocId = aocId;
		return this;
	}

	public void setAocId(User aocId) {
		this.aocId = aocId;
	}

	public QANotification getQaNotificationId() {
		return qaNotificationId;
	}

	public AOCNotification qaNotificationId(QANotification qaNotification) {
		this.qaNotificationId = qaNotification;
		return this;
	}

	public void setQaNotificationId(QANotification qaNotification) {
		this.qaNotificationId = qaNotification;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AOCNotification aocNotification = (AOCNotification) o;
		if (aocNotification.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), aocNotification.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("AOCNotification{").append("id=" + getId())
				.append(", notificationDate='" + getNotificationDate() + "'}").toString();
	}
}
