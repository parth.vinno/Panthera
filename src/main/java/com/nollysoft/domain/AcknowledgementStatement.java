package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AcknowledgementStatement.
 */
@Entity
@Table(name = "acknowledgement_statement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AcknowledgementStatement implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "statement", nullable = false)
	private String statement;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatement() {
		return statement;
	}

	public AcknowledgementStatement statement(String statement) {
		this.statement = statement;
		return this;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AcknowledgementStatement acknowledgementStatement = (AcknowledgementStatement) o;
		if (acknowledgementStatement.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), acknowledgementStatement.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("AcknowledgementStatement{").append("id=" + getId())
				.append(", statement='" + getStatement() + "'}").toString();
	}
}
