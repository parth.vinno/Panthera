package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AcknowledgementStatus.
 */
@Entity
@Table(name = "acknowledgement_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AcknowledgementStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "status_response")
	private Boolean statusResponse;

	@OneToOne
	@JoinColumn(name = "acknowledgement_statement_id", nullable = false)
	private AcknowledgementStatement acknowledgementStatementId;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isStatusResponse() {
		return statusResponse;
	}

	public AcknowledgementStatus statusResponse(Boolean statusResponse) {
		this.statusResponse = statusResponse;
		return this;
	}

	public void setStatusResponse(Boolean statusResponse) {
		this.statusResponse = statusResponse;
	}

	public AcknowledgementStatement getAcknowledgementStatementId() {
		return acknowledgementStatementId;
	}

	public AcknowledgementStatus acknowledgementStatementId(AcknowledgementStatement acknowledgementStatementId) {
		this.acknowledgementStatementId = acknowledgementStatementId;
		return this;
	}

	public void setAcknowledgementStatementId(AcknowledgementStatement acknowledgementStatementId) {
		this.acknowledgementStatementId = acknowledgementStatementId;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AcknowledgementStatus acknowledgementStatus = (AcknowledgementStatus) o;
		if (acknowledgementStatus.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), acknowledgementStatus.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("AcknowledgementStatus{").append("id=" + getId())
				.append(", statusResponse='" + isStatusResponse() + "'")
				.append(", acknowledgementStatementId=" + getAcknowledgementStatementId() + "}").toString();
	}
}
