package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Objects;

/**
 * A AssessmentInfo.
 */
@Entity
@Table(name = "assessment_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssessmentInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "assessment_type")
	private String assessmentType;

	@Column(name = "assessment_start_date")
	private LocalDate assessmentStartDate;

	@Column(name = "assessment_end_date")
	private LocalDate assessmentEndDate;

	@Column(name = "onsite_time_description")
	private String onsiteTimeDescription;

	@Column(name = "assessor")
	private String assessor;

	@PrePersist
	private void onCreate() {
		this.assessmentStartDate = LocalDate.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssessmentType() {
		return assessmentType;
	}

	public AssessmentInfo assessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
		return this;
	}

	public void setAssessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
	}

	public LocalDate getAssessmentStartDate() {
		return assessmentStartDate;
	}

	public AssessmentInfo assessmentStartDate(LocalDate assessmentStartDate) {
		this.assessmentStartDate = assessmentStartDate;
		return this;
	}

	public void setAssessmentStartDate(LocalDate assessmentStartDate) {
		this.assessmentStartDate = assessmentStartDate;
	}

	public LocalDate getAssessmentEndDate() {
		return assessmentEndDate;
	}

	public AssessmentInfo assessmentEndDate(LocalDate assessmentEndDate) {
		this.assessmentEndDate = assessmentEndDate;
		return this;
	}

	public void setAssessmentEndDate(LocalDate assessmentEndDate) {
		this.assessmentEndDate = assessmentEndDate;
	}

	public String getOnsiteTimeDescription() {
		return onsiteTimeDescription;
	}

	public void setOnsiteTimeDescription(String onsiteTimeDescription) {
		this.onsiteTimeDescription = onsiteTimeDescription;
	}

	public String getAssessor() {
		return assessor;
	}

	public AssessmentInfo assessor(String assessor) {
		this.assessor = assessor;
		return this;
	}

	public void setAssessor(String assessor) {
		this.assessor = assessor;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AssessmentInfo assessmentInfo = (AssessmentInfo) o;
		if (assessmentInfo.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), assessmentInfo.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("AssessmentInfo{").append("id=" + getId())
				.append(", assessmentType='" + getAssessmentType() + "'")
				.append(", assessmentStartDate='" + getAssessmentStartDate() + "'")
				.append(", assessmentEndDate='" + getAssessmentEndDate() + "'")
				.append(", assessor='" + getAssessor() + "}").toString();
	}
}
