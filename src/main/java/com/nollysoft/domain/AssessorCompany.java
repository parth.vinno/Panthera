package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AssessorCompany. Model/Table for part 1B, Common for both service provider and merchant
 */
@Entity
@Table(name = "assessor_company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssessorCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "lead_qsa_contact_name")
    private String leadQsaContactName;

    @Column(name = "title")
    private String title;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "email")
    private String email;
    
    @Column(name = "company_url")
    private String url;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public AssessorCompany companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLeadQsaContactName() {
        return leadQsaContactName;
    }

    public AssessorCompany leadQsaContactName(String leadQsaContactName) {
        this.leadQsaContactName = leadQsaContactName;
        return this;
    }

    public void setLeadQsaContactName(String leadQsaContactName) {
        this.leadQsaContactName = leadQsaContactName;
    }

    public String getTitle() {
        return title;
    }

    public AssessorCompany title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTelephone() {
        return telephone;
    }

    public AssessorCompany telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public AssessorCompany email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public AssessorCompany address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public AssessorCompany url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AssessorCompany assessorCompany = (AssessorCompany) o;
        if (assessorCompany.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), assessorCompany.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("AssessorCompany{").append("id=" + getId())
				.append(", leadQsaContactName='" + getLeadQsaContactName() + "'")
				.append(", companyName='" + getCompanyName() + "'").append(", title='" + getTitle() + "'")
				.append(", telephone='" + getTelephone() + "'").append(", url='" + getUrl() + "'").append(", email='" + getEmail() + "}").toString();
	}
}
