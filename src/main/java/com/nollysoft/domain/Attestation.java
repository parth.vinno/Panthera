package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Attestation.
 */
@Entity
@Table(name = "attestation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Attestation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "attestor_title", nullable = false)
	private String attestorTitle;

	@NotNull
	@Column(name = "attestor_name", nullable = false)
	private String attestorName;

	@NotNull
	@Column(name = "attestor_signature", nullable = false)
	private String attestorSignature;

	@Column(name = "attestation_date")
	private LocalDateTime attestationDate;

	@PrePersist
	protected void onCreate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	@PreUpdate
	protected void onUpdate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttestorTitle() {
		return attestorTitle;
	}

	public Attestation attestorTitle(String attestorTitle) {
		this.attestorTitle = attestorTitle;
		return this;
	}

	public void setAttestorTitle(String attestorTitle) {
		this.attestorTitle = attestorTitle;
	}

	public String getAttestorName() {
		return attestorName;
	}

	public Attestation attestorName(String attestorName) {
		this.attestorName = attestorName;
		return this;
	}

	public void setAttestorName(String attestorName) {
		this.attestorName = attestorName;
	}

	public String getAttestorSignature() {
		return attestorSignature;
	}

	public Attestation attestorSignature(String attestorSignature) {
		this.attestorSignature = attestorSignature;
		return this;
	}

	public void setAttestorSignature(String attestorSignature) {
		this.attestorSignature = attestorSignature;
	}

	public LocalDateTime getAttestationDate() {
		return attestationDate;
	}

	public Attestation attestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
		return this;
	}

	public void setAttestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Attestation attestation = (Attestation) o;
		if (attestation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), attestation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("Attestation{").append("id=" + getId())
				.append(", attestorTitle='" + getAttestorTitle() + "'")
				.append(", attestorName='" + getAttestorName() + "'")
				.append(", attestorSignature='" + getAttestorSignature() + "'")
				.append(", attestationDate='" + getAttestationDate() + "'}").toString();
	}
}
