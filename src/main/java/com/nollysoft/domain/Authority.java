package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Authority.
 */
@Entity
@Table(name = "authority")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Authority implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@ManyToMany
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@JoinTable(name = "authority_privilege", joinColumns = {
			@JoinColumn(name = "authority_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "privilege_id", referencedColumnName = "id") })
	private Set<Privilege> privileges = new HashSet<>();
    
    @ManyToMany(mappedBy = "authorities")
    private Collection<UserGroup> userGroups;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Authority name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Privilege> getPrivileges() {
		return privileges;
	}

	public Authority privileges(Set<Privilege> privileges) {
		this.privileges = privileges;
		return this;
	}

    public Authority addPrivilege(Privilege privilege) {
        this.privileges.add(privilege);
        privilege.getAuthorities().add(this);
        return this;
    }

    public Authority removePrivilege(Privilege privilege) {
        this.privileges.remove(privilege);
        privilege.getAuthorities().remove(this);
        return this;
    }

	public void setPrivileges(Set<Privilege> privileges) {
		this.privileges = privileges;
	}

	public Collection<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(Collection<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Authority authority = (Authority) o;
		if (authority.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), authority.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Authority{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
	}
}
