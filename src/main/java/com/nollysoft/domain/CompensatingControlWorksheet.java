package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CompensatingControlWorksheet.
 */
@Entity
@Table(name = "compensating_control_worksheet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CompensatingControlWorksheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "required_information")
    private String requiredInformation;

    @Column(name = "compensating_control_type")
    private String compensatingControlType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequiredInformation() {
        return requiredInformation;
    }

    public CompensatingControlWorksheet requiredInformation(String requiredInformation) {
        this.requiredInformation = requiredInformation;
        return this;
    }

    public void setRequiredInformation(String requiredInformation) {
        this.requiredInformation = requiredInformation;
    }

    public String getCompensatingControlType() {
        return compensatingControlType;
    }

    public CompensatingControlWorksheet compensatingControlType(String compensatingControlType) {
        this.compensatingControlType = compensatingControlType;
        return this;
    }

    public void setCompensatingControlType(String compensatingControlType) {
        this.compensatingControlType = compensatingControlType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompensatingControlWorksheet compensatingControlWorksheet = (CompensatingControlWorksheet) o;
        if (compensatingControlWorksheet.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), compensatingControlWorksheet.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CompensatingControlWorksheet{" +
            "id=" + getId() +
            ", requiredInformation='" + getRequiredInformation() + "'" +
            ", compensatingControlType='" + getCompensatingControlType() + "'" +
            "}";
    }
}
