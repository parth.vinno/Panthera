package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nollysoft.service.dto.CompensatingControlWorkSheetResponseDTO;

/**
 * A CompensatingControlWorksheetResponse.
 */
@Entity
@Table(name = "compensating_control_worksheet_response")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CompensatingControlWorksheetResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "explanation", nullable = false)
	private String explanation;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "compensating_control_worksheet_id")
	private CompensatingControlWorksheet compensatingControlWorksheetId;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "saq_question_id")
	private SaqQuestion saqQuestionId;

	public CompensatingControlWorksheetResponse() {}

	public CompensatingControlWorksheetResponse(
			CompensatingControlWorkSheetResponseDTO CompensatingControlWorkSheetResponseDTO) {
		if (CompensatingControlWorkSheetResponseDTO.getId() != null) {
			this.id = CompensatingControlWorkSheetResponseDTO.getId();
		}
		this.explanation = CompensatingControlWorkSheetResponseDTO.getExplanation();
		this.compensatingControlWorksheetId = CompensatingControlWorkSheetResponseDTO
				.getCompensatingControlWorksheetId();
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExplanation() {
		return explanation;
	}

	public CompensatingControlWorksheetResponse explanation(String explanation) {
		this.explanation = explanation;
		return this;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public CompensatingControlWorksheet getCompensatingControlWorksheetId() {
		return compensatingControlWorksheetId;
	}

	public CompensatingControlWorksheetResponse compensatingControlWorksheetId(
			CompensatingControlWorksheet compensatingControlWorksheet) {
		this.compensatingControlWorksheetId = compensatingControlWorksheet;
		return this;
	}

	public void setCompensatingControlWorksheetId(CompensatingControlWorksheet compensatingControlWorksheet) {
		this.compensatingControlWorksheetId = compensatingControlWorksheet;
	}

	public SaqQuestion getSaqQuestionId() {
		return saqQuestionId;
	}

	public CompensatingControlWorksheetResponse saqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
		return this;
	}

	public void setSaqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse = (CompensatingControlWorksheetResponse) o;
		if (compensatingControlWorksheetResponse.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), compensatingControlWorksheetResponse.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("CompensatingControlWorksheetResponse{").append("id=" + getId())
				.append(", compensatingControlWorksheetId='" + getCompensatingControlWorksheetId() + "'")
				.append(", explanation='" + getExplanation() + "'}").toString();
	}
}
