package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ComplianceAttestation.
 */
@Entity
@Table(name = "compliance_attestation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ComplianceAttestation implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum COMPLIANT_STATEMENT {
		COMPLIANT, NON_COMPLIANT, COMPLIANT_WITH_LEGAL_EXCEPTION;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "compliant_statement", nullable = false)
	private COMPLIANT_STATEMENT compliantStatement;

	@Column(name = "compliance_target_date")
	private LocalDate complianceTargetDate;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public COMPLIANT_STATEMENT getCompliantStatement() {
		return compliantStatement;
	}

	public ComplianceAttestation compliantStatement(COMPLIANT_STATEMENT compliantStatement) {
		this.compliantStatement = compliantStatement;
		return this;
	}

	public void setCompliantStatement(COMPLIANT_STATEMENT compliantStatement) {
		this.compliantStatement = compliantStatement;
	}

	public LocalDate getComplianceTargetDate() {
		return complianceTargetDate;
	}

	public ComplianceAttestation complianceTargetDate(LocalDate complianceTargetDate) {
		this.complianceTargetDate = complianceTargetDate;
		return this;
	}

	public void setComplianceTargetDate(LocalDate complianceTargetDate) {
		this.complianceTargetDate = complianceTargetDate;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ComplianceAttestation complianceAttestation = (ComplianceAttestation) o;
		if (complianceAttestation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), complianceAttestation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("ComplianceAttestation{").append("id=" + getId())
				.append(", compliantStatement='" + getCompliantStatement() + "'")
				.append(", complianceTargetDate='" + getComplianceTargetDate() + "'}").toString();
	}
}
