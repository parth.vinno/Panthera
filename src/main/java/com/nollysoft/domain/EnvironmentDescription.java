package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EnvironmentDescription.
 */
@Entity
@Table(name = "environment_description")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EnvironmentDescription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "network_segmented", nullable = false)
    private Boolean networkSegmented;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
		return description;
	}

	public EnvironmentDescription description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean isNetworkSegmented() {
        return networkSegmented;
    }

    public EnvironmentDescription networkSegmented(Boolean networkSegmented) {
        this.networkSegmented = networkSegmented;
        return this;
    }

    public void setNetworkSegmented(Boolean networkSegmented) {
        this.networkSegmented = networkSegmented;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EnvironmentDescription environmentDescription = (EnvironmentDescription) o;
        if (environmentDescription.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), environmentDescription.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("EnvironmentDescription{").append("id=" + getId())
				.append(", description='" + getDescription() + "'")
				.append(", networkSegmented='" + isNetworkSegmented() + "'}").toString();
	}
}
