package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nollysoft.domain.enumeration.Section;

/**
 * A ExecutiveSummaryStatus.
 */
@Entity
@Table(name = "executive_summary_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ExecutiveSummaryStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "section", nullable = false)
    private Section section;

    @Column(name = "last_updated_at")
    private LocalDateTime lastUpdatedAt;

	@PrePersist
	@PreUpdate
	public void onCreateOrUpdate() {
		this.lastUpdatedAt = LocalDateTime.now(ZoneId.of("UTC"));
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSection() {
    	if (section != null) {
            return section.toString();
		}
        return null;
    }

    @JsonIgnore
    public Section getSectionAsEnum() {
        return section;
    }

    public ExecutiveSummaryStatus section(Section section) {
        this.section = section;
        return this;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public LocalDateTime getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public ExecutiveSummaryStatus lastUpdatedAt(LocalDateTime lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
        return this;
    }

    public void setLastVisitedAt(LocalDateTime lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExecutiveSummaryStatus executiveSummaryStatus = (ExecutiveSummaryStatus) o;
        if (executiveSummaryStatus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), executiveSummaryStatus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("ExecutiveSummaryStatus{").append("id=" + getId()).append(", section='" + getSection())
				.append("'" + ", lastUpdatedAt='" + getLastUpdatedAt() + "'}").toString();
	}
}
