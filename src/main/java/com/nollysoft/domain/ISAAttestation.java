package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ISAAttestation.
 */
@Entity
@Table(name = "isa_attestation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ISAAttestation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "isa_attestor_name", nullable = false)
	private String isaAttestorName;

	@NotNull
	@Column(name = "isa_attestor_title", nullable = false)
	private String isaAttestorTitle;

	@NotNull
	@Column(name = "isa_attestor_company", nullable = false)
	private String isaAttestorCompany;

	@Column(name = "attestation_date")
	private LocalDateTime attestationDate;

	@PrePersist
	protected void onCreate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	@PreUpdate
	protected void onUpdate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsaAttestorName() {
		return isaAttestorName;
	}

	public ISAAttestation isaAttestorName(String isaAttestorName) {
		this.isaAttestorName = isaAttestorName;
		return this;
	}

	public void setIsaAttestorName(String isaAttestorName) {
		this.isaAttestorName = isaAttestorName;
	}

	public String getIsaAttestorTitle() {
		return isaAttestorTitle;
	}

	public ISAAttestation isaAttestorTitle(String isaAttestorTitle) {
		this.isaAttestorTitle = isaAttestorTitle;
		return this;
	}

	public void setIsaAttestorTitle(String isaAttestorTitle) {
		this.isaAttestorTitle = isaAttestorTitle;
	}

	public String getIsaAttestorCompany() {
		return isaAttestorCompany;
	}

	public ISAAttestation isaAttestorCompany(String isaAttestorCompany) {
		this.isaAttestorCompany = isaAttestorCompany;
		return this;
	}

	public void setIsaAttestorCompany(String isaAttestorCompany) {
		this.isaAttestorCompany = isaAttestorCompany;
	}

	public LocalDateTime getAttestationDate() {
		return attestationDate;
	}

	public ISAAttestation attestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
		return this;
	}

	public void setAttestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ISAAttestation iSAAttestation = (ISAAttestation) o;
		if (iSAAttestation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), iSAAttestation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("ISAAttestation{").append("id=" + getId())
				.append(", isaAttestorName='" + getIsaAttestorName() + "'")
				.append(", isaAttestorTitle='" + getIsaAttestorTitle() + "'")
				.append(", isaAttestorCompany='" + getIsaAttestorCompany() + "'")
				.append(", attestationDate='" + getAttestationDate() + "'}").toString();
	}
}
