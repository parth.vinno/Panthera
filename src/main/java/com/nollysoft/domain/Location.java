package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Location implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "facility_type", nullable = false)
	private String facilityType;

	@NotNull
	@Column(name = "number_of_facility_type", nullable = false)
	private Integer numberOfFacilityType;

	@NotNull
	@Column(name = "location", nullable = false)
	private String location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public Location facilityType(String facilityType) {
		this.facilityType = facilityType;
		return this;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Integer getNumberOfFacilityType() {
		return numberOfFacilityType;
	}

	public Location numberOfFacilityType(Integer numberOfFacilityType) {
		this.numberOfFacilityType = numberOfFacilityType;
		return this;
	}

	public void setNumberOfFacilityType(Integer numberOfFacilityType) {
		this.numberOfFacilityType = numberOfFacilityType;
	}

	public String getLocation() {
		return location;
	}

	public Location location(String location) {
		this.location = location;
		return this;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Location location = (Location) o;
		if (location.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), location.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("Location{").append("id=" + getId())
				.append(", facilityType='" + getFacilityType() + "'")
				.append(", numberOfFacilityType='" + getNumberOfFacilityType() + "'")
				.append(", location='" + getLocation() + "'}").toString();
	}
}
