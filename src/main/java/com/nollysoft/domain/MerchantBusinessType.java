package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MerchantBusinessType.
 */
@Entity
@Table(name = "merchant_business_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MerchantBusinessType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "business_type", nullable = false)
	private String businessType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBusinessType() {
		return businessType;
	}

	public MerchantBusinessType businessType(String businessType) {
		this.businessType = businessType;
		return this;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MerchantBusinessType merchantBusinessType = (MerchantBusinessType) o;
		if (merchantBusinessType.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), merchantBusinessType.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("MerchantBusinessType{").append("id=" + getId())
				.append(", businessType='" + getBusinessType() + "'}").toString();
	}
}
