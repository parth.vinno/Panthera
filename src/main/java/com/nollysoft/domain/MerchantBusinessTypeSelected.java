package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MerchantBusinessTypeSelected.
 */
@Entity
@Table(name = "merchant_business_type_selected")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MerchantBusinessTypeSelected implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "specification")
	private String specification;

	@OneToOne
	@JoinColumn(name = "merchant_business_type_id")
	private MerchantBusinessType merchantBusinessTypeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpecification() {
		return specification;
	}

	public MerchantBusinessTypeSelected specification(String specification) {
		this.specification = specification;
		return this;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public MerchantBusinessType getMerchantBusinessTypeId() {
		return merchantBusinessTypeId;
	}

	public MerchantBusinessTypeSelected merchantBusinessTypeId(MerchantBusinessType merchantBusinessType) {
		this.merchantBusinessTypeId = merchantBusinessType;
		return this;
	}

	public void setMerchantBusinessTypeId(MerchantBusinessType merchantBusinessType) {
		this.merchantBusinessTypeId = merchantBusinessType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MerchantBusinessTypeSelected merchantBusinessTypeSelected = (MerchantBusinessTypeSelected) o;
		if (merchantBusinessTypeSelected.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), merchantBusinessTypeSelected.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("MerchantBusinessTypeSelected{").append("id=" + getId())
				.append(", specification='" + getSpecification() + "'}").toString();
	}
}
