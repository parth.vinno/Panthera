package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nollysoft.service.dto.NonApplicabilityExplanationDTO;

/**
 * A NonApplicabilityExplanation.
 */
@Entity
@Table(name = "non_applicability_explanation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NonApplicabilityExplanation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "reason_requirement_not_applicable", nullable = false)
	private String reasonRequirementNotApplicable;

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "saq_question_id")
	private SaqQuestion saqQuestionId;

	public NonApplicabilityExplanation() {

	}

	public NonApplicabilityExplanation(NonApplicabilityExplanationDTO nonApplicabilityExplanationDTO) {
		if (nonApplicabilityExplanationDTO.getId() != null) {
			this.id = nonApplicabilityExplanationDTO.getId();
		}
		this.reasonRequirementNotApplicable = nonApplicabilityExplanationDTO.getReasonRequirementNotApplicable();
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReasonRequirementNotApplicable() {
		return reasonRequirementNotApplicable;
	}

	public NonApplicabilityExplanation reasonRequirementNotApplicable(String reasonRequirementNotApplicable) {
		this.reasonRequirementNotApplicable = reasonRequirementNotApplicable;
		return this;
	}

	public void setReasonRequirementNotApplicable(String reasonRequirementNotApplicable) {
		this.reasonRequirementNotApplicable = reasonRequirementNotApplicable;
	}

	public SaqQuestion getSaqQuestionId() {
		return saqQuestionId;
	}

	public void setSaqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		NonApplicabilityExplanation nonApplicabilityExplanation = (NonApplicabilityExplanation) o;
		if (nonApplicabilityExplanation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), nonApplicabilityExplanation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("NonApplicabilityExplanation{").append("id=" + getId())
				.append(", reasonRequirementNotApplicable='" + getReasonRequirementNotApplicable() + "}").toString();
	}
}
