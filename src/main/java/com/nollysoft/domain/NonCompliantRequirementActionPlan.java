package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A NonCompliantRequirementActionPlan.
 */
@Entity
@Table(name = "non_compliant_requirement_action_plan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NonCompliantRequirementActionPlan implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "compliant", nullable = false)
	private boolean compliant;

	@Column(name = "remediation_plan")
	private String remediationPlan;

	@Column(name = "remediation_date")
	private LocalDate remediationDate;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pcidssRequirement_id")
	private PcidssRequirement pcidssRequirementId;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isCompliant() {
		return compliant;
	}

	public NonCompliantRequirementActionPlan isCompliant(Boolean compliant) {
		this.compliant = compliant;
		return this;
	}

	public void setIsCompliant(Boolean compliant) {
		this.compliant = compliant;
	}

	public String getRemediationPlan() {
		return remediationPlan;
	}

	public NonCompliantRequirementActionPlan remediationPlan(String remediationPlan) {
		this.remediationPlan = remediationPlan;
		return this;
	}

	public void setRemediationPlan(String remediationPlan) {
		this.remediationPlan = remediationPlan;
	}

	public LocalDate getRemediationDate() {
		return remediationDate;
	}

	public NonCompliantRequirementActionPlan remediationDate(LocalDate remediationDate) {
		this.remediationDate = remediationDate;
		return this;
	}

	public void setRemediationDate(LocalDate remediationDate) {
		this.remediationDate = remediationDate;
	}

	public PcidssRequirement getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public NonCompliantRequirementActionPlan pcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
		return this;
	}

	public void setPcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan = (NonCompliantRequirementActionPlan) o;
		if (nonCompliantRequirementActionPlan.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), nonCompliantRequirementActionPlan.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("NonCompliantRequirementActionPlan{").append("id=" + getId())
				.append(", compliant='" + isCompliant() + "'")
				.append(", remediationPlan='" + getRemediationPlan() + "'")
				.append(", remediationDate='" + getRemediationDate() + "'")
				.append(", pcidssRequirementId=" + getPcidssRequirementId() + "}").toString();
	}
}
