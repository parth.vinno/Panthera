package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nollysoft.domain.enumeration.OrganizationSection;

/**
 * A OrganizationSummaryStatus.
 */
@Entity
@Table(name = "organization_summary_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrganizationSummaryStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	public final static int TOTAL_SECTIONS = 3;

	public static enum STATUS {
		START, RESUME, REVISIT;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "section")
	private OrganizationSection section;

	@Column(name = "last_updated_at")
	private LocalDateTime lastUpdatedAt;

	@Transient
	private STATUS status;

	@Transient
	private OrganizationSection lastUpdatedSection;

	@PrePersist
	@PreUpdate
	public void onCreateOrUpdate() {
		this.lastUpdatedAt = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSection() {
		if (section != null) {
			return section.toString();
		} else
			return null;
	}

	@JsonIgnore
	public OrganizationSection getSectionAsEnum() {
		return section;
	}

	public OrganizationSummaryStatus section(OrganizationSection section) {
		this.section = section;
		return this;
	}

	public void setSection(OrganizationSection section) {
		this.section = section;
	}

	public LocalDateTime getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public OrganizationSummaryStatus lastUpdatedAt(LocalDateTime lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
		return this;
	}

	public void setLastUpdatedAt(LocalDateTime lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getStatus() {
		return status.toString();
	}

	public STATUS getStatusEnum() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getLastUpdatedSection() {
		return lastUpdatedSection.toString();
	}

	public OrganizationSection getLastUpdatedSectionEnum() {
		return lastUpdatedSection;
	}

	public void setLastUpdatedSection(OrganizationSection lastUpdatedSection) {
		this.lastUpdatedSection = lastUpdatedSection;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrganizationSummaryStatus organizationSummaryStatus = (OrganizationSummaryStatus) o;
		if (organizationSummaryStatus.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), organizationSummaryStatus.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("OrganizationSummaryStatus{").append("id=" + getId())
				.append(", section='" + getSection()).append("'" + ", lastUpdatedAt='" + getLastUpdatedAt() + "'}")
				.toString();
	}
}
