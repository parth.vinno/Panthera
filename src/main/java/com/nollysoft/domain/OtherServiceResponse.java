package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A OtherServiceResponse.
 */
@Entity
@Table(name = "other_service_response")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OtherServiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;
 
	public static enum SCOPE {
		SELECTED, NOT_SELECTED;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "other")
    private String other;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name = "scope")
    private SCOPE scope;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "service_type_id")
    private ServiceType serviceTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOther() {
        return other;
    }

    public OtherServiceResponse other(String other) {
        this.other = other;
        return this;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public SCOPE getScope() {
		return scope;
	}

    public OtherServiceResponse scope(SCOPE scope) {
        this.scope = scope;
        return this;
    }

	public void setScope(SCOPE scope) {
		this.scope = scope;
	}

	public ServiceType getServiceTypeId() {
        return serviceTypeId;
    }

    public OtherServiceResponse serviceTypeId(ServiceType serviceType) {
        this.serviceTypeId = serviceType;
        return this;
    }

    public void setServiceTypeId(ServiceType serviceType) {
        this.serviceTypeId = serviceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OtherServiceResponse otherServiceResponse = (OtherServiceResponse) o;
        if (otherServiceResponse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), otherServiceResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("OtherServiceResponse{").append("id=" + getId())
				.append(", other='" + getOther() + "'}").toString();
	}
}
