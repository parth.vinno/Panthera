package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * A PaymentApplication.
 */
@Entity
@Table(name = "payment_application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentApplication implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "is_payment_application_more_than_one", nullable = false)
	private Boolean isPaymentApplicationMoreThanOne;

	@JsonManagedReference
	@Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "paymentApplicationId", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PaymentApplicationList> paymentApplicationLists;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isIsPaymentApplicationMoreThanOne() {
		return isPaymentApplicationMoreThanOne;
	}

	public PaymentApplication isPaymentApplicationMoreThanOne(Boolean isPaymentApplicationMoreThanOne) {
		this.isPaymentApplicationMoreThanOne = isPaymentApplicationMoreThanOne;
		return this;
	}

	public void setIsPaymentApplicationMoreThanOne(Boolean isPaymentApplicationMoreThanOne) {
		this.isPaymentApplicationMoreThanOne = isPaymentApplicationMoreThanOne;
	}

	public Set<PaymentApplicationList> getPaymentApplicationLists() {
		return paymentApplicationLists;
	}

	public PaymentApplication paymentApplicationLists(Set<PaymentApplicationList> paymentApplicationLists) {
		this.paymentApplicationLists = paymentApplicationLists;
		return this;
	}

	public void setPaymentApplicationLists(Set<PaymentApplicationList> paymentApplicationLists) {
		this.paymentApplicationLists = paymentApplicationLists;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentApplication paymentApplication = (PaymentApplication) o;
		if (paymentApplication.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentApplication.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("PaymentApplication{").append("id=" + getId())
				.append(", isPaymentApplicationMoreThanOne='" + isIsPaymentApplicationMoreThanOne() + "'}").toString();
	}
}
