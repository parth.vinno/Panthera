package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * A PaymentApplicationList.
 */
@Entity
@Table(name = "payment_application_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentApplicationList implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "payment_application_name", nullable = false)
	private String paymentApplicationName;

	@Column(name = "version_number")
	private String versionNumber;

	@NotNull
	@Column(name = "application_vendor", nullable = false)
	private String applicationVendor;

	@Column(name = "is_application_padss_listed")
	private Boolean isApplicationPadssListed;

	@Column(name = "expiry_date")
	private LocalDate expiryDate;

	@JsonBackReference
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "payment_application_id")
	private PaymentApplication paymentApplicationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPaymentApplicationName() {
		return paymentApplicationName;
	}

	public PaymentApplicationList paymentApplicationName(String paymentApplicationName) {
		this.paymentApplicationName = paymentApplicationName;
		return this;
	}

	public void setPaymentApplicationName(String paymentApplicationName) {
		this.paymentApplicationName = paymentApplicationName;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public PaymentApplicationList versionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
		return this;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getApplicationVendor() {
		return applicationVendor;
	}

	public PaymentApplicationList applicationVendor(String applicationVendor) {
		this.applicationVendor = applicationVendor;
		return this;
	}

	public void setApplicationVendor(String applicationVendor) {
		this.applicationVendor = applicationVendor;
	}

	public Boolean isIsApplicationPadssListed() {
		return isApplicationPadssListed;
	}

	public PaymentApplicationList isApplicationPadssListed(Boolean isApplicationPadssListed) {
		this.isApplicationPadssListed = isApplicationPadssListed;
		return this;
	}

	public void setIsApplicationPadssListed(Boolean isApplicationPadssListed) {
		this.isApplicationPadssListed = isApplicationPadssListed;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public PaymentApplicationList expiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
		return this;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public PaymentApplication getPaymentApplicationId() {
		return paymentApplicationId;
	}

	public PaymentApplicationList paymentApplicationId(PaymentApplication paymentApplication) {
		this.paymentApplicationId = paymentApplication;
		return this;
	}

	public void setPaymentApplicationId(PaymentApplication paymentApplication) {
		this.paymentApplicationId = paymentApplication;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentApplicationList paymentApplicationList = (PaymentApplicationList) o;
		if (paymentApplicationList.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentApplicationList.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("PaymentApplicationList{").append("id=" + getId())
				.append(", paymentApplicationName='" + getPaymentApplicationName() + "'")
				.append(", versionNumber='" + getVersionNumber() + "'")
				.append(", applicationVendor='" + getApplicationVendor() + "'")
				.append(", isApplicationPadssListed='" + isIsApplicationPadssListed() + "'")
				.append(", expiryDate='" + getExpiryDate() + "'}").toString();
	}
}
