package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PaymentCardBusinessDescription.
 */
@Entity
@Table(name = "payment_card_business_description")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentCardBusinessDescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "description_transmit", nullable = false)
	private String descriptionTransmit;

	@Column(name = "description_impact", nullable = false)
	private String descriptionImpact;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescriptionTransmit() {
		return descriptionTransmit;
	}

	public PaymentCardBusinessDescription descriptionTransmit(String descriptionTransmit) {
		this.descriptionTransmit = descriptionTransmit;
		return this;
	}

	public void setDescriptionTransmit(String descriptionTransmit) {
		this.descriptionTransmit = descriptionTransmit;
	}

	public String getDescriptionImpact() {
		return descriptionImpact;
	}

	public PaymentCardBusinessDescription descriptionImpact(String descriptionImpact) {
		this.descriptionImpact = descriptionImpact;
		return this;
	}

	public void setDescriptionImpact(String descriptionImpact) {
		this.descriptionImpact = descriptionImpact;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentCardBusinessDescription paymentCardBusinessDescription = (PaymentCardBusinessDescription) o;
		if (paymentCardBusinessDescription.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentCardBusinessDescription.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("PaymentCardBusinessDescription{").append("id=" + getId())
				.append(", descriptionTransmit='" + getDescriptionTransmit() + "'")
				.append(", descriptionImpact='" + getDescriptionImpact() + "'}").toString();
	}
}
