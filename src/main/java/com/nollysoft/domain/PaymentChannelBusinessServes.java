package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PaymentChannelBusinessServes.
 */
@Entity
@Table(name = "payment_channel_business_serves")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentChannelBusinessServes implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(optional = false)
	@NotNull
	@JoinColumn(name = "payment_channel_id")
	private PaymentChannel paymentChannelId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaymentChannel getPaymentChannelId() {
		return paymentChannelId;
	}

	public PaymentChannelBusinessServes paymentChannelId(PaymentChannel paymentChannel) {
		this.paymentChannelId = paymentChannel;
		return this;
	}

	public void setPaymentChannelId(PaymentChannel paymentChannel) {
		this.paymentChannelId = paymentChannel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentChannelBusinessServes paymentChannelBusinessServes = (PaymentChannelBusinessServes) o;
		if (paymentChannelBusinessServes.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentChannelBusinessServes.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("PaymentChannelBusinessServes{").append("id=" + getId() + "'}").toString();
	}
}
