package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nollysoft.service.dto.PcidssRequirementResultSetDTO;

/**
 * A PcidssRequirement.
 */
@Entity
@Table(name = "pcidss_requirement")
@SqlResultSetMapping(name = "pcidssRequirementResultSetDTO", classes = {
		@ConstructorResult(targetClass = PcidssRequirementResultSetDTO.class, columns = {
				@ColumnResult(name = "id", type = Long.class), 
				@ColumnResult(name = "questionCount", type = Long.class),
				@ColumnResult(name = "responseCount", type = Long.class) 
				}) 
		})
@NamedNativeQuery(
		name = "PcidssRequirement.getPcidssRequirementIdsBySaqQuestionResponseRequired", 
		query = "SELECT p.id AS id, COUNT(s.id) AS questionCount, COUNT(res.id) AS responseCount FROM saq_question s LEFT JOIN pcidss_requirement p ON s.pcidss_requirement_id = p.id LEFT JOIN saq_response res ON s.id = res.saq_question_id WHERE s.response_required = true GROUP BY p.id", 
		resultSetMapping = "pcidssRequirementResultSetDTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PcidssRequirement implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "requirement")
	private String requirement;

	@Column(name = "requirement_number")
	private String requirementNumber;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "requirement_category_id")
	private RequirementCategory requirementCategoryId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequirementNumber() {
		return requirementNumber;
	}

	public void setRequirementNumber(String requirementNumber) {
		this.requirementNumber = requirementNumber;
	}

	public String getRequirement() {
		return requirement;
	}

	public PcidssRequirement requirement(String requirement) {
		this.requirement = requirement;
		return this;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public RequirementCategory getRequirementCategoryId() {
		return requirementCategoryId;
	}

	public PcidssRequirement requirementCategoryId(RequirementCategory requirementCategory) {
		this.requirementCategoryId = requirementCategory;
		return this;
	}

	public void setRequirementCategoryId(RequirementCategory requirementCategory) {
		this.requirementCategoryId = requirementCategory;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PcidssRequirement pcidssRequirement = (PcidssRequirement) o;
		if (pcidssRequirement.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), pcidssRequirement.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("PcidssRequirement{").append("id=" + getId())
				.append(", requirement='" + getRequirement() + "'}").toString();
	}
}
