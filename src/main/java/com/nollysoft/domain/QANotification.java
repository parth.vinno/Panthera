package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A QANotification.
 */
@Entity
@Table(name = "qa_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QANotification implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum NOTIFICATION_STATUS {
		NEW, PENDING, RESPONSE_REQUIRED, REVIEW_REQUIRED, SENT_TO_AOC;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "notification_date", nullable = false)
	private LocalDate notificationDate = LocalDate.now();

	private LocalDate correctionDate;

	@NotNull
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private NOTIFICATION_STATUS status;

	@OneToOne(optional = false)
	@NotNull
	@JoinColumn(name = "qa_id")
	private User qaId;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "qsa_id")
	private User qsaId;

	@PrePersist
	protected void onCreate() {
		notificationDate = LocalDate.now();
		status = NOTIFICATION_STATUS.NEW;
	}

	@PreUpdate
	protected void onUpdate() {
		correctionDate = LocalDate.now();
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getNotificationDate() {
		return notificationDate;
	}

	public QANotification notificationDate(LocalDate notificationDate) {
		this.notificationDate = notificationDate;
		return this;
	}

	public void setNotificationDate(LocalDate notificationDate) {
		this.notificationDate = notificationDate;
	}

	public LocalDate getCorrectionDate() {
		return correctionDate;
	}

	public QANotification correctionDate(LocalDate correctionDate) {
		this.correctionDate = correctionDate;
		return this;
	}

	public void setCorrectionDate(LocalDate correctionDate) {
		this.correctionDate = correctionDate;
	}

	public String getStatus() {
		return status.toString();
	}

	public QANotification status(NOTIFICATION_STATUS status) {
		this.status = status;
		return this;
	}

	public void setStatus(NOTIFICATION_STATUS status) {
		this.status = status;
	}

	@JsonIgnore
	public NOTIFICATION_STATUS getStatusType() {
		return status;
	}

	public User getQaId() {
		return qaId;
	}

	public QANotification qaId(User qaId) {
		this.qaId = qaId;
		return this;
	}

	public void setQaId(User qaId) {
		this.qaId = qaId;
	}

	public User getQsaId() {
		return qsaId;
	}

	public QANotification qsaId(User qsaId) {
		this.qsaId = qsaId;
		return this;
	}

	public void setQsaId(User qsaId) {
		this.qsaId = qsaId;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		QANotification qaNotification = (QANotification) o;
		if (qaNotification.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), qaNotification.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("QANotification{").append("id=" + getId())
				.append(", notificationDate='" + getNotificationDate() + "'}").toString();
	}
}
