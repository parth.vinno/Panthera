package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A QSAAttestation.
 */
@Entity
@Table(name = "qsa_attestation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QSAAttestation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "qsa_attestor_name", nullable = false)
	private String qsaAttestorName;

	@NotNull
	@Column(name = "qsa_attestor_signature", nullable = false)
	private String qsaAttestorSignature;

	@NotNull
	@Column(name = "qsa_attestor_company", nullable = false)
	private String qsaAttestorCompany;

	@Column(name = "attestation_date")
	private LocalDateTime attestationDate;

	@PrePersist
	protected void onCreate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	@PreUpdate
	protected void onUpdate() {
		attestationDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQsaAttestorName() {
		return qsaAttestorName;
	}

	public QSAAttestation qsaAttestorName(String qsaAttestorName) {
		this.qsaAttestorName = qsaAttestorName;
		return this;
	}

	public void setQsaAttestorName(String qsaAttestorName) {
		this.qsaAttestorName = qsaAttestorName;
	}

	public String getQsaAttestorSignature() {
		return qsaAttestorSignature;
	}

	public QSAAttestation qsaAttestorSignature(String qsaAttestorSignature) {
		this.qsaAttestorSignature = qsaAttestorSignature;
		return this;
	}

	public void setQsaAttestorSignature(String qsaAttestorSignature) {
		this.qsaAttestorSignature = qsaAttestorSignature;
	}

	public String getQsaAttestorCompany() {
		return qsaAttestorCompany;
	}

	public QSAAttestation qsaAttestorCompany(String qsaAttestorCompany) {
		this.qsaAttestorCompany = qsaAttestorCompany;
		return this;
	}

	public void setQsaAttestorCompany(String qsaAttestorCompany) {
		this.qsaAttestorCompany = qsaAttestorCompany;
	}

	public LocalDateTime getAttestationDate() {
		return attestationDate;
	}

	public QSAAttestation attestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
		return this;
	}

	public void setAttestationDate(LocalDateTime attestationDate) {
		this.attestationDate = attestationDate;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		QSAAttestation qSAAttestation = (QSAAttestation) o;
		if (qSAAttestation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), qSAAttestation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("QSAAttestation{").append("id=" + getId())
				.append(", qsaAttestorName='" + getQsaAttestorName() + "'")
				.append(", qsaAttestorSignature='" + getQsaAttestorSignature() + "'")
				.append(", qsaAttestorCompany='" + getQsaAttestorCompany() + "'")
				.append(", attestationDate='" + getAttestationDate() + "'}").toString();
	}
}
