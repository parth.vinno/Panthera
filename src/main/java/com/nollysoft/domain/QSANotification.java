package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A QSANotification.
 */
@Entity
@Table(name = "qsa_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QSANotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "notification_date", nullable = false)
    private LocalDate notificationDate;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(name = "qa_user_id")
    private User qaUserId;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(name = "qsa_user_id")
    private User qsaUserId;
    
    @PrePersist
    public void onCreate(){
    	this.notificationDate = LocalDate.now();
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getNotificationDate() {
        return notificationDate;
    }

    public QSANotification notificationDate(LocalDate notificationDate) {
        this.notificationDate = notificationDate;
        return this;
    }

    public void setNotificationDate(LocalDate notificationDate) {
        this.notificationDate = notificationDate;
    }

    public User getQaUserId() {
        return qaUserId;
    }

    public QSANotification qaUserId(User qaUserId) {
        this.qaUserId = qaUserId;
        return this;
    }

    public void setQaUserId(User qaUserId) {
        this.qaUserId = qaUserId;
    }

    public User getQsaUserId() {
        return qsaUserId;
    }

    public QSANotification qsaUserId(User qsaUserId) {
        this.qsaUserId = qsaUserId;
        return this;
    }

    public void setQsaUserId(User qsaUserId) {
        this.qsaUserId = qsaUserId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QSANotification qsaNotification = (QSANotification) o;
        if (qsaNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), qsaNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QSANotification{" +
            "id=" + getId() +
            ", notificationDate='" + getNotificationDate() + "'" +
            "}";
    }
}
