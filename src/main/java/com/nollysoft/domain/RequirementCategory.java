package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * A RequirementCategory.
 */
@Entity
@Table(name = "requirement_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RequirementCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "category_name")
	private String categoryName;

	@Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "requirementCategoryId", cascade = CascadeType.REMOVE)
	private Set<PcidssRequirement> pcidssRequirements;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public RequirementCategory categoryName(String categoryName) {
		this.categoryName = categoryName;
		return this;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public RequirementCategory() {
	}

	public Set<PcidssRequirement> getPcidssRequirements() {
		return pcidssRequirements;
	}

	public void setPcidssRequirements(Set<PcidssRequirement> pcidssRequirements) {
		this.pcidssRequirements = pcidssRequirements;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RequirementCategory requirementCategory = (RequirementCategory) o;
		if (requirementCategory.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), requirementCategory.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "RequirementCategory{" + "id=" + getId() + ", categoryName='" + getCategoryName() + "'" + "}";
	}
}

