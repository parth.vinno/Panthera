package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nollysoft.service.dto.RequirementNotTestedExplanationDTO;

/**
 * A RequirementNotTestedExplanation.
 */
@Entity
@Table(name = "requirement_not_tested_explanation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RequirementNotTestedExplanation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "requirement_part_not_tested")
	private String requirementPartNotTested;

	@NotNull
	@Column(name = "reason", nullable = false)
	private String reason;

	@OneToOne(optional = false, fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "saq_question_id")
	private SaqQuestion saqQuestionId;

	public RequirementNotTestedExplanation() {

	}

	public RequirementNotTestedExplanation(RequirementNotTestedExplanationDTO requirementNotTestedExplantionDTO) {
		if (requirementNotTestedExplantionDTO.getId() != null) {
			this.id = requirementNotTestedExplantionDTO.getId();
		}
		this.requirementPartNotTested = requirementNotTestedExplantionDTO.getRequirementPartNotTested();
		this.reason = requirementNotTestedExplantionDTO.getReason();
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequirementPartNotTested() {
		return requirementPartNotTested;
	}

	public RequirementNotTestedExplanation requirementPartNotTested(String requirementPartNotTested) {
		this.requirementPartNotTested = requirementPartNotTested;
		return this;
	}

	public void setRequirementPartNotTested(String requirementPartNotTested) {
		this.requirementPartNotTested = requirementPartNotTested;
	}

	public String getReason() {
		return reason;
	}

	public RequirementNotTestedExplanation reason(String reason) {
		this.reason = reason;
		return this;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public SaqQuestion getSaqQuestionId() {
		return saqQuestionId;
	}

	public RequirementNotTestedExplanation saqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
		return this;
	}

	public void setSaqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RequirementNotTestedExplanation requirementNotTestedExplanation = (RequirementNotTestedExplanation) o;
		if (requirementNotTestedExplanation.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), requirementNotTestedExplanation.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("RequirementNotTestedExplanation{").append("id=" + getId())
				.append(", requirementPartNotTested='" + getRequirementPartNotTested() + "'")
				.append(", reason='" + getReason() + "'}").toString();
	}
}
