package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A SaqInfo.
 */
@Entity
@Table(name = "saq_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaqInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "saq_version")
	private String saqVersion;

	@Column(name = "revision_number")
	private String revisionNumber;

	@Column(name = "description")
	private String description;

	@Column(name = "saq_version_date")
	private LocalDate saqVersionDate;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSaqVersion() {
		return saqVersion;
	}

	public SaqInfo saqVersion(String saqVersion) {
		this.saqVersion = saqVersion;
		return this;
	}

	public void setSaqVersion(String saqVersion) {
		this.saqVersion = saqVersion;
	}

	public String getRevisionNumber() {
		return revisionNumber;
	}

	public SaqInfo revisionNumber(String revisionNumber) {
		this.revisionNumber = revisionNumber;
		return this;
	}

	public void setRevisionNumber(String revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getDescription() {
		return description;
	}

	public SaqInfo description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getSaqVersionDate() {
		return saqVersionDate;
	}

	public SaqInfo saqVersionDate(LocalDate saqVersionDate) {
		this.saqVersionDate = saqVersionDate;
		return this;
	}

	public void setSaqVersionDate(LocalDate saqVersionDate) {
		this.saqVersionDate = saqVersionDate;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SaqInfo saqInfo = (SaqInfo) o;
		if (saqInfo.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), saqInfo.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "SaqInfo{" + "id=" + getId() + ", saqVersion='"
				+ getSaqVersion() + "'" + ", revisionNumber='"
				+ getRevisionNumber() + "'" + ", description='"
				+ getDescription() + "'" + ", saqVersionDate='"
				+ getSaqVersionDate() + "'" + "}";
	}
}
