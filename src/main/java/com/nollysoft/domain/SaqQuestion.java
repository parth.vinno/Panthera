package com.nollysoft.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A SaqQuestion.
 */
@Entity
@Table(name = "saq_question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaqQuestion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "question_number", nullable = false)
	private String questionNumber;

	@NotNull
	@Column(name = "response_required", nullable = false)
	private boolean responseRequired;

	@Column(name = "question_text")
	private String questionText;

	@NotNull
	@Column(name = "part")
	private boolean part;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "parent_id")
	private SaqQuestion parentId;
	
	@Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "parentId", cascade = CascadeType.ALL, orphanRemoval = true)
	Set<SaqQuestion> subQuestions = new LinkedHashSet<>();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "pcidss_requirement_id")
	private PcidssRequirement pcidssRequirementId;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "saq_question_expected_test", joinColumns = @JoinColumn(name = "saq_question_id"), inverseJoinColumns = @JoinColumn(name = "expected_test_id", referencedColumnName = "id"))
	private Set<ExpectedTest> expectedTests = new LinkedHashSet<ExpectedTest>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public SaqQuestion id(Long id) {
		this.id = id;
		return this;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionNumber() {
		return questionNumber;
	}

	public SaqQuestion questionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
		return this;
	}

	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}

	public Boolean isResponseRequired() {
		return responseRequired;
	}

	public SaqQuestion responseRequired(Boolean responseRequired) {
		this.responseRequired = responseRequired;
		return this;
	}

	public void setResponseRequired(Boolean responseRequired) {
		this.responseRequired = responseRequired;
	}

	public String getQuestionText() {
		return questionText;
	}

	public SaqQuestion questionText(String questionText) {
		this.questionText = questionText;
		return this;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public Boolean isPart() {
		return part;
	}

	public SaqQuestion part(Boolean part) {
		this.part = part;
		return this;
	}

	public void setPart(Boolean part) {
		this.part = part;
	}

	public SaqQuestion getParentId() {
		return parentId;
	}

	public SaqQuestion parentId(SaqQuestion saqResponse) {
		this.parentId = saqResponse;
		return this;
	}

	public void setParentId(SaqQuestion saqResponse) {
		this.parentId = saqResponse;
	}

	public Set<SaqQuestion> getSubQuestions() {
		return subQuestions;
	}

	public void setSubQuestions(Set<SaqQuestion> subQuestions) {
		this.subQuestions = subQuestions;
	}

	public PcidssRequirement getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public SaqQuestion pcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
		return this;
	}

	public void setPcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
	}

	public Set<ExpectedTest> getExpectedTests() {
		return expectedTests;
	}

	public SaqQuestion expectedTests(Set<ExpectedTest> expectedTests) {
		this.expectedTests = expectedTests;
		return this;
	}

	public void setExpectedTests(Set<ExpectedTest> expectedTests) {
		this.expectedTests = expectedTests;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SaqQuestion saqQuestion = (SaqQuestion) o;
		if (saqQuestion.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), saqQuestion.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("SaqQuestion{").append("id=" + getId())
				.append(", questionNumber='" + getQuestionNumber() + "'")
				.append(", responseRequired='" + isResponseRequired() + "'")
				.append(", questionText='" + getQuestionText() + "'").append(", part='" + isPart() + "'}").toString();
	}
}