package com.nollysoft.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A SaqResponse.
 */
@Entity
@Table(name = "saq_response")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaqResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static enum QUESTION_RESPONSE {
		YES, YES_WITH_CCW, NO, NA, NOT_TESTED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "question_response", nullable = false)
	@Enumerated(EnumType.STRING)
	private QUESTION_RESPONSE questionResponse;

	@NotNull
	@OneToOne
	@JoinColumn(name = "saq_question_id")
	private SaqQuestion saqQuestionId;
	
	@NotNull
	Date createdAt = Date.from(Instant.now());

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public QUESTION_RESPONSE getQuestionResponse() {
		return questionResponse;
	}

	public SaqResponse questionResponse(QUESTION_RESPONSE questionResponse) {
		this.questionResponse = questionResponse;
		return this;
	}

	public void setQuestionResponse(QUESTION_RESPONSE questionResponse) {
		this.questionResponse = questionResponse;
	}

	public SaqQuestion getSaqQuestionId() {
		return saqQuestionId;
	}

	public SaqResponse saqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
		return this;
	}

	public void setSaqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SaqResponse saqResponse = (SaqResponse) o;
		if (saqResponse.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), saqResponse.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("SaqResponse{").append("id=" + getId())
				.append(", questionResponse='" + getQuestionResponse() + "'}").toString();
	}
}