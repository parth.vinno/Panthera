package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * A SaqReview.
 */
@Entity
@Table(name = "saq_review")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaqReview implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum REVIEW_STATUS {
		REVIEWED, CORRECTED, FIXED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonManagedReference
	@Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "saqReviewId", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<SaqReviewComment> comments;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private REVIEW_STATUS status;

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "saq_question_id")
	private SaqQuestion saqQuestionId;

	@Column(name = "created_date")
	private LocalDateTime createdDate;

	@Column(name = "last_updated_date")
	private LocalDateTime lastUpdatedDate;

	@PrePersist
	protected void onCreate() {
		lastUpdatedDate = createdDate = LocalDateTime.now(ZoneId.of("UTC"));
		status = REVIEW_STATUS.REVIEWED;
	}

	@PreUpdate
	protected void onUpdate() {
		lastUpdatedDate = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<SaqReviewComment> getComments() {
		return comments;
	}

	public SaqReview comments(Set<SaqReviewComment> comments) {
		this.comments = comments;
		return this;
	}

	public void setComments(Set<SaqReviewComment> comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status.toString();
	}

	public SaqReview status(REVIEW_STATUS status) {
		this.status = status;
		return this;
	}

	public void setStatus(REVIEW_STATUS status) {
		this.status = status;
	}

	@JsonIgnore
	public REVIEW_STATUS getStatusType() {
		return status;
	}

	public SaqQuestion getSaqQuestionId() {
		return saqQuestionId;
	}

	public SaqReview saqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
		return this;
	}

	public void setSaqQuestionId(SaqQuestion saqQuestion) {
		this.saqQuestionId = saqQuestion;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public SaqReview createdDate(LocalDateTime requestedDate) {
		this.createdDate = requestedDate;
		return this;
	}

	public void setCreatedDate(LocalDateTime requestedDate) {
		this.createdDate = requestedDate;
	}

	public LocalDateTime getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public SaqReview lastUpdatedDate(LocalDateTime lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
		return this;
	}

	public void setLastUpdatedDate(LocalDateTime lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SaqReview saqReview = (SaqReview) o;
		if (saqReview.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), saqReview.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("SaqReview{").append("id=" + getId()).append(", comments='" + getComments() + "'}")
				.toString();
	}
}
