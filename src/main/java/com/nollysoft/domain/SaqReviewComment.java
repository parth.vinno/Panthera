package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * A SaqReviewComment.
 */
@Entity
@Table(name = "saq_review_comment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaqReviewComment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "comment", nullable = false)
	private String comment;

	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@Column(name = "last_updated_at")
	private LocalDateTime lastUpdatedAt;

	@JsonBackReference
	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "saq_review_id")
	private SaqReview saqReviewId;

	@PrePersist
	public void onCreate() {
		this.createdAt = lastUpdatedAt = LocalDateTime.now(ZoneId.of("UTC"));
	}

	@PreUpdate
	public void onUpdate() {
		this.lastUpdatedAt = LocalDateTime.now(ZoneId.of("UTC"));
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public SaqReviewComment comment(String comment) {
		this.comment = comment;
		return this;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public SaqReviewComment createdAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public SaqReviewComment lastUpdatedAt(LocalDateTime lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
		return this;
	}

	public void setLastUpdatedAt(LocalDateTime lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public SaqReview getSaqReviewId() {
		return saqReviewId;
	}

	public SaqReviewComment saqReviewId(SaqReview saqReview) {
		this.saqReviewId = saqReview;
		return this;
	}

	public void setSaqReviewId(SaqReview saqReview) {
		this.saqReviewId = saqReview;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SaqReviewComment saqReviewComment = (SaqReviewComment) o;
		if (saqReviewComment.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), saqReviewComment.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("SaqReviewComment{").append("id=" + getId()).append(", comment='" + getComment() + "'")
				.append(", createdAt='" + getCreatedAt() + "'").append(", lastUpdatedAt=" + getLastUpdatedAt() + "'}")
				.toString();
	}
}
