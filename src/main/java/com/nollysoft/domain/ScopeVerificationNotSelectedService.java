package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ScopeVerificationNotSelectedService.
 */
@Entity
@Table(name = "scope_verification_not_selected_service")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ScopeVerificationNotSelectedService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "not_assessed_service_name")
	private String notAssessedServiceName;

	@Column(name = "not_assessed_service_type")
	private String notAssessedServiceType;

	@Column(name = "comment")
	private String comment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotAssessedServiceName() {
		return notAssessedServiceName;
	}

	public ScopeVerificationNotSelectedService notAssessedServiceName(String notAssessedServiceName) {
		this.notAssessedServiceName = notAssessedServiceName;
		return this;
	}

	public void setNotAssessedServiceName(String notAssessedServiceName) {
		this.notAssessedServiceName = notAssessedServiceName;
	}

	public String getNotAssessedServiceType() {
		return notAssessedServiceType;
	}

	public ScopeVerificationNotSelectedService notAssessedServiceType(String notAssessedServiceType) {
		this.notAssessedServiceType = notAssessedServiceType;
		return this;
	}

	public void setNotAssessedServiceType(String notAssessedServiceType) {
		this.notAssessedServiceType = notAssessedServiceType;
	}

	public String getComment() {
		return comment;
	}

	public ScopeVerificationNotSelectedService comment(String comment) {
		this.comment = comment;
		return this;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ScopeVerificationNotSelectedService scopeVerificationNotSelectedService = (ScopeVerificationNotSelectedService) o;
		if (scopeVerificationNotSelectedService.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), scopeVerificationNotSelectedService.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("ScopeVerificationSelectedService{").append("id=" + getId())
				.append(", notAssessedServiceName='" + getNotAssessedServiceName() + "'")
				.append(", notAssessedServiceType='" + getNotAssessedServiceType() + "'")
				.append(", comment='" + getComment() + "'}").toString();
	}
}
