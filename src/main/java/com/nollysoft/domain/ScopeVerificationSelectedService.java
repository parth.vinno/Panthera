package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ScopeVerificationSelectedService.
 */
@Entity
@Table(name = "scope_verification_selected_service")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ScopeVerificationSelectedService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "assessed_service_name")
	private String assessedServiceName;

	@Column(name = "assessed_service_type")
	private String assessedServiceType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssessedServiceName() {
		return assessedServiceName;
	}

	public ScopeVerificationSelectedService assessedServiceName(String assessedServiceName) {
		this.assessedServiceName = assessedServiceName;
		return this;
	}

	public void setAssessedServiceName(String assessedServiceName) {
		this.assessedServiceName = assessedServiceName;
	}

	public String getAssessedServiceType() {
		return assessedServiceType;
	}

	public ScopeVerificationSelectedService assessedServiceType(String assessedServiceType) {
		this.assessedServiceType = assessedServiceType;
		return this;
	}

	public void setAssessedServiceType(String assessedServiceType) {
		this.assessedServiceType = assessedServiceType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ScopeVerificationSelectedService scopeVerificationSelectedService = (ScopeVerificationSelectedService) o;
		if (scopeVerificationSelectedService.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), scopeVerificationSelectedService.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("ScopeVerificationSelectedService{").append("id=" + getId())
				.append(", assessedServiceName='" + getAssessedServiceName() + "'")
				.append(", assessedServiceType='" + getAssessedServiceType() + "'}").toString();
	}
}
