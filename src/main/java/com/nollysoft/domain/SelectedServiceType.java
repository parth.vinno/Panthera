package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A SelectedServiceType.
 */
@Entity
@Table(name = "selected_service_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SelectedServiceType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne
	@NotNull
	@JoinColumn(name = "service_type_id")
	private ServiceType serviceTypeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceType getServiceTypeId() {
		return serviceTypeId;
	}

	public SelectedServiceType serviceTypeId(ServiceType serviceType) {
		this.serviceTypeId = serviceType;
		return this;
	}

	public void setServiceTypeId(ServiceType serviceType) {
		this.serviceTypeId = serviceType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SelectedServiceType selectedServiceType = (SelectedServiceType) o;
		if (selectedServiceType.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), selectedServiceType.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("SelectedServiceType{").append("id=" + getId() + "'}").toString();
	}
}
