package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ServiceByServiceProvider.
 */
@Entity
@Table(name = "service_by_service_provider")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ServiceByServiceProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "provider_name", nullable = false)
    private String providerName;

    @NotNull
    @Column(name = "description_of_service_provider", nullable = false)
    private String descriptionOfServiceProvider;

    @NotNull
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "third_party_service_provider_id")
    private ThirdPartyServiceProvider thirdPartyServiceProviderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProviderName() {
        return providerName;
    }

    public ServiceByServiceProvider providerName(String providerName) {
        this.providerName = providerName;
        return this;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getDescriptionOfServiceProvider() {
        return descriptionOfServiceProvider;
    }

	public ServiceByServiceProvider descriptionOfServiceProvider(String descriptionOfServiceProvider) {
		this.descriptionOfServiceProvider = descriptionOfServiceProvider;
		return this;
	}

	public void setDescriptionOfServiceProvider(String descriptionOfServiceProvider) {
		this.descriptionOfServiceProvider = descriptionOfServiceProvider;
	}

	public ThirdPartyServiceProvider getThirdPartyServiceProviderId() {
		return thirdPartyServiceProviderId;
	}

	public ServiceByServiceProvider thirdPartyServiceProviderId(ThirdPartyServiceProvider thirdPartyServiceProvider) {
		this.thirdPartyServiceProviderId = thirdPartyServiceProvider;
		return this;
	}

	public void setThirdPartyServiceProviderId(ThirdPartyServiceProvider thirdPartyServiceProvider) {
		this.thirdPartyServiceProviderId = thirdPartyServiceProvider;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceByServiceProvider serviceByServiceProvider = (ServiceByServiceProvider) o;
        if (serviceByServiceProvider.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceByServiceProvider.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("ServiceByServiceProvider{").append("id=" + getId())
				.append(", providerName='" + getProviderName() + "'")
				.append(", descriptionOfServiceProvider='" + getDescriptionOfServiceProvider() + "'}").toString();
	}
}
