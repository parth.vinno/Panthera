package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * A ServiceType.
 */
@Entity
@Table(name = "service_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ServiceType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "service_type")
	private String serviceType;

	@JsonBackReference
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "service_category_id")
	private ServiceCategory serviceCategoryId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceType() {
		return serviceType;
	}

	public ServiceType serviceType(String serviceType) {
		this.serviceType = serviceType;
		return this;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public ServiceCategory getServiceCategoryId() {
		return serviceCategoryId;
	}

	public ServiceType serviceCategoryId(ServiceCategory serviceCategoryId) {
		this.serviceCategoryId = serviceCategoryId;
		return this;
	}

	public void setServiceCategoryId(ServiceCategory serviceCategoryId) {
		this.serviceCategoryId = serviceCategoryId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ServiceType serviceType = (ServiceType) o;
		if (serviceType.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), serviceType.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("ServiceType{").append("id=" + getId())
				.append(", serviceType='" + getServiceType() + "'")
				.append(", serviceCategoryId='" + getServiceCategoryId() + "'}").toString();
	}
}
