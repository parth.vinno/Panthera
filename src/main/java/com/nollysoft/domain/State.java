package com.nollysoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A State.
 */
@Entity
@Table(name = "state")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class State implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "short_name")
    private String shortName;

    @Column(name = "long_name")
    private String longName;

    @Column(name = "country_code")
    private String countryCode;

    public String getShortName() {
        return shortName;
    }

    public State shortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public State longName(String longName) {
        this.longName = longName;
        return this;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public State countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        State state = (State) o;
        if (state.getLongName() == null || getLongName() == null) {
            return false;
        }
        return Objects.equals(getLongName(), state.getLongName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getLongName());
    }

    @Override
    public String toString() {
        return "State{" +
            ", shortName='" + getShortName() + "'" +
            ", longName='" + getLongName() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            "}";
    }
}
