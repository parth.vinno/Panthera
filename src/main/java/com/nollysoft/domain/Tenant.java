package com.nollysoft.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Tenant.
 */
@Entity
@Table(name = "tenant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tenant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "company_url")
    private String companyUrl;

    @Column(name = "contact_person_deskphone")
    private String contactPersonDeskphone;

    @Column(name = "contact_person_email")
    private String contactPersonEmail;

    @Column(name = "contact_person_mobilephone")
    private String contactPersonMobilephone;

    @Column(name = "contact_person_name")
    private String contactPersonName;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "name")
    private String name;

	@Column(name = "company_abbreviation")
	private String companyAbbreviation;

    @Column(name = "company_phone_number")
    private String companyPhoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "service_start_date")
    private LocalDate serviceStartDate;

    @Column(name = "tenant_id")
    private String tenantId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address addressId;

    @ManyToOne
    @JoinColumn(name = "assessment_type_id")
    private AssessmentType assessmentTypeId;

    @ManyToOne
    @JoinColumn(name = "industry_id")
    private Industry industryId;

    @NotNull
    @Column(name = "deleted")
    private boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public Tenant companyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
        return this;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getContactPersonDeskphone() {
        return contactPersonDeskphone;
    }

    public Tenant contactPersonDeskphone(String contactPersonDeskphone) {
        this.contactPersonDeskphone = contactPersonDeskphone;
        return this;
    }

    public void setContactPersonDeskphone(String contactPersonDeskphone) {
        this.contactPersonDeskphone = contactPersonDeskphone;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public Tenant contactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
        return this;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getContactPersonMobilephone() {
        return contactPersonMobilephone;
    }

    public Tenant contactPersonMobilephone(String contactPersonMobilephone) {
        this.contactPersonMobilephone = contactPersonMobilephone;
        return this;
    }

    public void setContactPersonMobilephone(String contactPersonMobilephone) {
        this.contactPersonMobilephone = contactPersonMobilephone;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public Tenant contactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
        return this;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Tenant createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getName() {
        return name;
    }

    public Tenant name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyAbbreviation() {
		return companyAbbreviation;
	}

    public Tenant companyAbbreviation(String companyAbbreviation) {
        this.companyAbbreviation = companyAbbreviation;
        return this;
    }

	public void setCompanyAbbreviation(String companyAbbreviation) {
		this.companyAbbreviation = companyAbbreviation;
	}

	public String getCompanyPhoneNumber() {
        return companyPhoneNumber;
    }

    public Tenant companyPhoneNumber(String companyPhoneNumber) {
        this.companyPhoneNumber = companyPhoneNumber;
        return this;
    }

    public void setCompanyPhoneNumber(String companyPhoneNumber) {
        this.companyPhoneNumber = companyPhoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Tenant email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getServiceStartDate() {
        return serviceStartDate;
    }

    public Tenant serviceStartDate(LocalDate serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
        return this;
    }

    public void setServiceStartDate(LocalDate serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
    }

    public String getTenantId() {
        return tenantId;
    }

    public Tenant tenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Address getAddressId() {
        return addressId;
    }

    public Tenant addressId(Address address) {
        this.addressId = address;
        return this;
    }

    public void setAddressId(Address address) {
        this.addressId = address;
    }

    public AssessmentType getAssessmentTypeId() {
		return assessmentTypeId;
	}

	public void setAssessmentTypeId(AssessmentType assessmentTypeId) {
		this.assessmentTypeId = assessmentTypeId;
	}

	public Industry getIndustryId() {
		return industryId;
	}

	public void setIndustryId(Industry industryId) {
		this.industryId = industryId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tenant tenant = (Tenant) o;
        if (tenant.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tenant.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("Tenant{").append("id=" + getId()).append(", companyUrl='" + getCompanyUrl() + "'")
				.append(", contactPersonDeskphone='" + getContactPersonDeskphone() + "'")
				.append(", contactPersonEmail='" + getContactPersonEmail() + "'")
				.append(", contactPersonMobilephone='" + getContactPersonMobilephone() + "'")
				.append(", contactPersonName='" + getContactPersonName() + "'")
				.append(", createDate='" + getCreateDate() + "'").append(", name='" + getName() + "'")
				.append(", companyAbbreviation='" + getCompanyAbbreviation() + "'")
				.append(", companyPhoneNumber='" + getCompanyPhoneNumber() + "'").append(", email='" + getEmail() + "'")
				.append(", serviceStartDate='" + getServiceStartDate() + "'").append(", type='" + getTenantId() + "'}")
				.toString();
	}
}
