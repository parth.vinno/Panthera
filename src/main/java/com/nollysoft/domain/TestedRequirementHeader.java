package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * A TestedRequirementHeader.
 */
@Entity
@Table(name = "tested_requirement_header")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TestedRequirementHeader implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "service_assessed_name", nullable = false)
	private String serviceAssessedName;

	@JsonManagedReference
	@Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "testedRequirementHeaderId", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<TestedRequirementSummary> testedRequirementSummaries;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceAssessedName() {
		return serviceAssessedName;
	}

	public TestedRequirementHeader serviceAssessedName(String serviceAssessedName) {
		this.serviceAssessedName = serviceAssessedName;
		return this;
	}

	public void setServiceAssessedName(String serviceAssessedName) {
		this.serviceAssessedName = serviceAssessedName;
	}

	public Set<TestedRequirementSummary> getTestedRequirementSummaries() {
		return testedRequirementSummaries;
	}

	public TestedRequirementHeader paymentApplicationLists(Set<TestedRequirementSummary> testedRequirementSummaries) {
		this.testedRequirementSummaries = testedRequirementSummaries;
		return this;
	}

	public void setTestedRequirementSummaries(Set<TestedRequirementSummary> testedRequirementSummaries) {
		this.testedRequirementSummaries = testedRequirementSummaries;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TestedRequirementHeader assessedService = (TestedRequirementHeader) o;
		if (assessedService.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), assessedService.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("TestedRequirementHeader{").append("id=" + getId())
				.append(", name='" + getServiceAssessedName() + "'}").toString();
	}
}
