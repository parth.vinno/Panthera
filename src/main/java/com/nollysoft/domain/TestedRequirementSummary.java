package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * A TestedRequirementSummary.
 */
@Entity
@Table(name = "tested_requirement_summary")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TestedRequirementSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum ASSESSED {
		FULL, PARTIAL, NONE;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "assessed")
	private ASSESSED assessed;

	@Column(name = "justification")
	private String justification;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "pcidss_requirement_id")
	private PcidssRequirement pcidssRequirementId;

	@JsonBackReference
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "tested_requirement_header_id")
	private TestedRequirementHeader testedRequirementHeaderId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ASSESSED getAssessed() {
		return assessed;
	}

	public TestedRequirementSummary assessed(ASSESSED assessed) {
		this.assessed = assessed;
		return this;
	}

	public void setAssessed(ASSESSED assessed) {
		this.assessed = assessed;
	}

	public String getJustification() {
		return justification;
	}

	public TestedRequirementSummary justification(String justification) {
		this.justification = justification;
		return this;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public PcidssRequirement getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public TestedRequirementSummary pcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
		return this;
	}

	public void setPcidssRequirementId(PcidssRequirement pcidssRequirement) {
		this.pcidssRequirementId = pcidssRequirement;
	}

	public TestedRequirementHeader getTestedRequirementHeaderId() {
		return testedRequirementHeaderId;
	}

	public TestedRequirementSummary testedRequirementHeaderId(TestedRequirementHeader testedRequirementHeaderId) {
		this.testedRequirementHeaderId = testedRequirementHeaderId;
		return this;
	}

	public void setTestedRequirementHeaderId(TestedRequirementHeader testedRequirementHeaderId) {
		this.testedRequirementHeaderId = testedRequirementHeaderId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TestedRequirementSummary testedRequirementSummary = (TestedRequirementSummary) o;
		if (testedRequirementSummary.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), testedRequirementSummary.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return new StringBuilder("TestedRequirementSummary{").append("id=" + getId())
				.append(", full='" + getAssessed() + "'").append(", justification='" + getJustification() + "'}")
				.toString();
	}
}
