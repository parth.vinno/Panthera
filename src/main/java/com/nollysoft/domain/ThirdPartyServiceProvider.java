package com.nollysoft.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * A ThirdPartyServiceProvider.
 */
@Entity
@Table(name = "third_party_service_provider")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ThirdPartyServiceProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "qir_relationship_present", nullable = false)
    private boolean qirRelationshipPresent;

    @Column(name = "qir_company_name", nullable = false)
    private String qirCompanyName;

    @Column(name = "qir_individual_name", nullable = false)
    private String qirIndividualName;

    @Column(name = "service_description")
    private String serviceDescription;

    @NotNull
    @Column(name = "service_provider_relation_ship_present", nullable = false)
    private boolean serviceProviderRelationShipPresent;

    @JsonManagedReference
    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "thirdPartyServiceProviderId", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ServiceByServiceProvider> serviceByServiceProviders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isQirRelationshipPresent() {
        return qirRelationshipPresent;
    }

    public ThirdPartyServiceProvider qirRelationshipPresent(Boolean qirRelationshipPresent) {
        this.qirRelationshipPresent = qirRelationshipPresent;
        return this;
    }

    public void setQirRelationshipPresent(Boolean qirRelationshipPresent) {
        this.qirRelationshipPresent = qirRelationshipPresent;
    }

    public String getQirCompanyName() {
        return qirCompanyName;
    }

    public ThirdPartyServiceProvider qirCompanyName(String qirCompanyName) {
        this.qirCompanyName = qirCompanyName;
        return this;
    }

    public void setQirCompanyName(String qirCompanyName) {
        this.qirCompanyName = qirCompanyName;
    }

    public String getQirIndividualName() {
        return qirIndividualName;
    }

    public ThirdPartyServiceProvider qirIndividualName(String qirIndividualName) {
        this.qirIndividualName = qirIndividualName;
        return this;
    }

    public void setQirIndividualName(String qirIndividualName) {
        this.qirIndividualName = qirIndividualName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public ThirdPartyServiceProvider serviceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
        return this;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public boolean isServiceProviderRelationShipPresent() {
        return serviceProviderRelationShipPresent;
    }

	public ThirdPartyServiceProvider serviceProviderRelationShipPresent(Boolean serviceProviderRelationShipPresent) {
		this.serviceProviderRelationShipPresent = serviceProviderRelationShipPresent;
		return this;
	}

	public void setServiceProviderRelationShipPresent(Boolean serviceProviderRelationShipPresent) {
		this.serviceProviderRelationShipPresent = serviceProviderRelationShipPresent;
	}

	public Set<ServiceByServiceProvider> getServiceByServiceProviders() {
		return serviceByServiceProviders;
	}

	public ThirdPartyServiceProvider serviceByServiceProviders(
			Set<ServiceByServiceProvider> serviceByServiceProviders) {
		this.serviceByServiceProviders = serviceByServiceProviders;
		return this;
	}

	public void setServiceByServiceProviders(Set<ServiceByServiceProvider> serviceByServiceProviders) {
		this.serviceByServiceProviders = serviceByServiceProviders;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ThirdPartyServiceProvider thirdPartyServiceProvider = (ThirdPartyServiceProvider) o;
        if (thirdPartyServiceProvider.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thirdPartyServiceProvider.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return new StringBuilder("ThirdPartyServiceProvider{").append("id=" + getId())
				.append(", isQirRelationshipPresent='" + isQirRelationshipPresent() + "'")
				.append(", qirCompanyName='" + getQirCompanyName() + "'")
				.append(", qirIndividualName='" + getQirIndividualName() + "'")
				.append(", serviceDescription='" + getServiceDescription() + "'")
				.append(", isServiceProviderRelationShipPresent='" + isServiceProviderRelationShipPresent() + "'}")
				.toString();
	}
}
