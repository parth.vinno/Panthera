package com.nollysoft.domain.enumeration;

/**
 * The Section enumeration.
 */
public enum OrganizationSection {
	ORGANIZATION, ASSESSOR, SUMMARY;
}
