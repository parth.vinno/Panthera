package com.nollysoft.domain.enumeration;

/**
 * The ScopeSelected enumeration.
 */
public enum ScopeSelected {
    SELECTED,  NOT_SELECTED
}
