package com.nollysoft.domain.enumeration;

public enum UserRole {
	QA , QSA_AOC, ISA_AOC, QSA_OFFICER_AOC;
}
