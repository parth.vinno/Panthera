package com.nollysoft.repository;

import com.nollysoft.domain.AOCLegalExceptionDetails;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AOCLegalExceptionDetails entity.
 */
@Repository
public interface AOCLegalExceptionDetailsRepository extends JpaRepository<AOCLegalExceptionDetails, Long> {

}
