package com.nollysoft.repository;

import com.nollysoft.domain.AOCNotification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AOCNotification entity.
 */
@Repository
public interface AOCNotificationRepository extends JpaRepository<AOCNotification, Long> {

}
