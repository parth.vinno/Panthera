package com.nollysoft.repository;

import com.nollysoft.domain.AcknowledgementStatement;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AcknowledgementStatement entity.
 */
@Repository
public interface AcknowledgementStatementRepository extends JpaRepository<AcknowledgementStatement, Long> {

}
