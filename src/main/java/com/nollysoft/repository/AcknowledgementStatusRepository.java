package com.nollysoft.repository;

import com.nollysoft.domain.AcknowledgementStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AcknowledgementStatus entity.
 */
@Repository
public interface AcknowledgementStatusRepository extends JpaRepository<AcknowledgementStatus, Long> {

}
