package com.nollysoft.repository;

import com.nollysoft.domain.AssessmentInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AssessmentInfo entity.
 */
@Repository
public interface AssessmentInfoRepository extends JpaRepository<AssessmentInfo, Long> {

}
