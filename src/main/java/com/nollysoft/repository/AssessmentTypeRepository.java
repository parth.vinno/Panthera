package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.AssessmentType;

/**
 * Spring Data JPA repository for the AssessmentType entity.
 */
@Repository
public interface AssessmentTypeRepository extends JpaRepository<AssessmentType, Long> {

}
