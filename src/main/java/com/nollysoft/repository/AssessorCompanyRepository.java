package com.nollysoft.repository;

import com.nollysoft.domain.AssessorCompany;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AccessorCompany entity.
 */
@Repository
public interface AssessorCompanyRepository extends JpaRepository<AssessorCompany,Long> {
    
}
