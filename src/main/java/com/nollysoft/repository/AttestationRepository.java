package com.nollysoft.repository;

import com.nollysoft.domain.Attestation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Attestation entity.
 */
@Repository
public interface AttestationRepository extends JpaRepository<Attestation, Long> {

}
