package com.nollysoft.repository;

import com.nollysoft.domain.Authority;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority,Long> {

	Authority findByName(String name);
}
