package com.nollysoft.repository;

import com.nollysoft.domain.CompensatingControlWorksheet;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CompensatingControlWorksheet entity.
 */
@Repository
public interface CompensatingControlWorksheetRepository extends JpaRepository<CompensatingControlWorksheet,Long> {
    
}
