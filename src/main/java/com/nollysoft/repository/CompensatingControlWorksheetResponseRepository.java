package com.nollysoft.repository;

import com.nollysoft.domain.CompensatingControlWorksheetResponse;
import com.nollysoft.domain.RequirementNotTestedExplanation;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the CompensatingControlWorksheetResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompensatingControlWorksheetResponseRepository extends JpaRepository<CompensatingControlWorksheetResponse, Long> {

	@Query("SELECT ccwr FROM CompensatingControlWorksheetResponse AS ccwr JOIN ccwr.saqQuestionId AS s  WHERE s.id =:saqQuestionId")
	List<CompensatingControlWorksheetResponse> findALLBySaqQuestionId(@Param("saqQuestionId") Long saqQuestionId);
}
