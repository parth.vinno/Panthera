package com.nollysoft.repository;

import com.nollysoft.domain.ComplianceAttestation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ComplianceAttestation entity.
 */
@Repository
public interface ComplianceAttestationRepository extends JpaRepository<ComplianceAttestation, Long> {

}
