package com.nollysoft.repository;

import com.nollysoft.domain.EnvironmentDescription;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EnvironmentDescription entity.
 */
@Repository
public interface EnvironmentDescriptionRepository extends JpaRepository<EnvironmentDescription,Long> {
    
}
