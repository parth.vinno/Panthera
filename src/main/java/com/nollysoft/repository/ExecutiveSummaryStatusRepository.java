package com.nollysoft.repository;

import com.nollysoft.domain.ExecutiveSummaryStatus;
import com.nollysoft.domain.enumeration.Section;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the ExecutiveSummaryStatus entity.
 */
@Repository
public interface ExecutiveSummaryStatusRepository extends JpaRepository<ExecutiveSummaryStatus, Long> {

	ExecutiveSummaryStatus findOneBySection(Section section);

	@Query(nativeQuery = true, value = "SELECT * FROM executive_summary_status ORDER BY last_updated_at DESC limit 1")
	ExecutiveSummaryStatus findLastUpdatedSection();

}
