package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.ExpectedTest;

/**
 * Spring Data JPA repository for the ExpectedTest entity.
 */
@Repository
public interface ExpectedTestRepository extends JpaRepository<ExpectedTest, Long> {

	@Query(nativeQuery = true, value = "SELECT COUNT(e.id) FROM expected_test AS e JOIN saq_question_expected_test AS se ON e.id = se.expected_test_id WHERE e.id =:id")
	int countById(@Param("id") Long id);
}
