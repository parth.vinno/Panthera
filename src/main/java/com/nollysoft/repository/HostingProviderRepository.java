package com.nollysoft.repository;

import com.nollysoft.domain.HostingProvider;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the HostingProvider entity.
 */
@Repository
public interface HostingProviderRepository extends JpaRepository<HostingProvider,Long> {
    
}
