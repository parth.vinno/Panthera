package com.nollysoft.repository;

import com.nollysoft.domain.ISAAttestation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ISAAttestation entity.
 */
@Repository
public interface ISAAttestationRepository extends JpaRepository<ISAAttestation, Long> {

}
