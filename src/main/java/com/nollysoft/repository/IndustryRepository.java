package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.Industry;

/**
 * Spring Data JPA repository for the Industry entity.
 */
@Repository
public interface IndustryRepository extends JpaRepository<Industry, Long> {

}
