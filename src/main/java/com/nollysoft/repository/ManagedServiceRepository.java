package com.nollysoft.repository;

import com.nollysoft.domain.ManagedService;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ManagedService entity.
 */
@Repository
public interface ManagedServiceRepository extends JpaRepository<ManagedService,Long> {
    
}
