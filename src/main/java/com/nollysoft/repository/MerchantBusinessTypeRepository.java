package com.nollysoft.repository;

import com.nollysoft.domain.MerchantBusinessType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MerchantBusinessType entity.
 */
@Repository
public interface MerchantBusinessTypeRepository extends JpaRepository<MerchantBusinessType,Long> {
    
}
