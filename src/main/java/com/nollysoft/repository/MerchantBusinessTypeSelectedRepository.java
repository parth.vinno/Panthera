package com.nollysoft.repository;

import com.nollysoft.domain.MerchantBusinessTypeSelected;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MerchantBusinessTypeSelected entity.
 */
@Repository
public interface MerchantBusinessTypeSelectedRepository extends JpaRepository<MerchantBusinessTypeSelected,Long> {
    
}
