package com.nollysoft.repository;

import com.nollysoft.domain.NonApplicabilityExplanation;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the NonApplicabilityExplanation entity.
 */
@Repository
public interface NonApplicabilityExplanationRepository extends JpaRepository<NonApplicabilityExplanation, Long> {

	@Query("SELECT na FROM NonApplicabilityExplanation AS na JOIN na.saqQuestionId AS s  WHERE s.id =:saqQuestionId")
	NonApplicabilityExplanation findOneBySaqQuestionId(@Param("saqQuestionId") Long saqQuestionId);

}
