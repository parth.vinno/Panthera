package com.nollysoft.repository;

import com.nollysoft.domain.NonCompliantRequirementActionPlan;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NonCompliantRequirementActionPlan entity.
 */
@Repository
public interface NonCompliantRequirementActionPlanRepository extends JpaRepository<NonCompliantRequirementActionPlan, Long> {

}
