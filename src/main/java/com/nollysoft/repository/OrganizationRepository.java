package com.nollysoft.repository;

import com.nollysoft.domain.Organization;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Organization entity.
 */
@Repository
public interface OrganizationRepository extends JpaRepository<Organization,Long> {
    
}
