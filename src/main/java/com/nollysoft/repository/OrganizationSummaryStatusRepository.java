package com.nollysoft.repository;

import com.nollysoft.domain.OrganizationSummaryStatus;
import com.nollysoft.domain.enumeration.OrganizationSection;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the OrganizationSummaryStatus entity.
 */
@Repository
public interface OrganizationSummaryStatusRepository extends JpaRepository<OrganizationSummaryStatus, Long> {
	
	OrganizationSummaryStatus findOneBySection(OrganizationSection section);
	
	@Query(nativeQuery = true, value = "SELECT * FROM organization_summary_status ORDER BY last_updated_at DESC limit 1")
	OrganizationSummaryStatus findLastUpdatedSection();
}
