package com.nollysoft.repository;

import com.nollysoft.domain.OtherServiceResponse;
import com.nollysoft.domain.OtherServiceResponse.SCOPE;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the OtherServiceResponse entity.
 */
@Repository
public interface OtherServiceResponseRepository extends JpaRepository<OtherServiceResponse,Long> {

	void deleteAllByScope(SCOPE valueOf);
    
}
