package com.nollysoft.repository;

import com.nollysoft.domain.PaymentApplicationList;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentApplicationList entity.
 */
@Repository
public interface PaymentApplicationListRepository extends JpaRepository<PaymentApplicationList,Long> {
    
}
