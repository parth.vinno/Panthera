package com.nollysoft.repository;

import com.nollysoft.domain.PaymentApplication;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentApplication entity.
 */
@Repository
public interface PaymentApplicationRepository extends JpaRepository<PaymentApplication,Long> {
    
}
