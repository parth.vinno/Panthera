package com.nollysoft.repository;

import com.nollysoft.domain.PaymentCardBusinessDescription;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentCardBusinessDescription entity.
 */
@Repository
public interface PaymentCardBusinessDescriptionRepository extends JpaRepository<PaymentCardBusinessDescription,Long> {
    
}
