package com.nollysoft.repository;

import com.nollysoft.domain.PaymentChannelBusinessServes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentChannelBusinessServes entity.
 */
@Repository
public interface PaymentChannelBusinessServesRepository extends JpaRepository<PaymentChannelBusinessServes,Long> {
    
}
