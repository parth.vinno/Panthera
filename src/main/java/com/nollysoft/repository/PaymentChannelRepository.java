package com.nollysoft.repository;

import com.nollysoft.domain.PaymentChannel;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentChannel entity.
 */
@Repository
public interface PaymentChannelRepository extends JpaRepository<PaymentChannel,Long> {
    
}
