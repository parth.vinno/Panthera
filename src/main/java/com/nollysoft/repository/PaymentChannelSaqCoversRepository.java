package com.nollysoft.repository;

import com.nollysoft.domain.PaymentChannelSaqCovers;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentchannelSaqCovers entity.
 */
@Repository
public interface PaymentChannelSaqCoversRepository extends JpaRepository<PaymentChannelSaqCovers,Long> {
    
}
