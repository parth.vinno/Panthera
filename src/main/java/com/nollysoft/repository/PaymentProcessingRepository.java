package com.nollysoft.repository;

import com.nollysoft.domain.PaymentProcessing;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentProcessing entity.
 */
@Repository
public interface PaymentProcessingRepository extends JpaRepository<PaymentProcessing,Long> {
    
}
