package com.nollysoft.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.service.dto.PcidssRequirementResultSetDTO;

/**
 * Spring Data JPA repository for the PcidssRequirement entity.
 */
@Repository
public interface PcidssRequirementRepository extends JpaRepository<PcidssRequirement, Long> {

	@Query("SELECT pcidssRequirement FROM PcidssRequirement AS pcidssRequirement JOIN pcidssRequirement.requirementCategoryId AS requirementCategory WHERE requirementCategory.id =:requirementCategoryId")
	public Page<PcidssRequirement> getPcidssRequirementByRequirementCategory(
			@Param("requirementCategoryId") Long requirementCategoryId, Pageable pageable);

	@Query(nativeQuery = true, name = "PcidssRequirement.getPcidssRequirementIdsBySaqQuestionResponseRequired")
	List<PcidssRequirementResultSetDTO> getPcidssRequirementIdsBySaqQuestionResponseRequired();

	@Query("SELECT p FROM PcidssRequirement AS p JOIN p.requirementCategoryId AS c WHERE c.id =:requirementCategoryId")
	List<PcidssRequirement> getPcidssRequirementIdsForCategory(
			@Param("requirementCategoryId") Long requirementCategoryId);

	@Query(nativeQuery = true, value = "SELECT min(p.id) FROM saq_review AS sr JOIN saq_question sq ON sr.saq_question_id = sq.id JOIN pcidss_requirement AS p ON sq.pcidss_requirement_id = p.id JOIN requirement_category r ON p.requirement_category_id = r.id WHERE r.id =:requirementCategoryId AND sr.status = 'CORRECTED' GROUP BY p.id;")
	Set<BigInteger> getPcidssRequirementIdsForCategoryWithCorrectedResponse(
			@Param("requirementCategoryId") Long requirementCategoryId);
	
	PcidssRequirement findOneByRequirementNumber(String requirementNumber);
}

