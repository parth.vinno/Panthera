package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.Privilege;


/**
 * Spring Data JPA repository for the Privilege entity.
 */
@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege,Long> {
    
}
