package com.nollysoft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.User;

/**
 * Spring Data JPA repository for the QANotification entity.
 */
@Repository
public interface QANotificationRepository extends JpaRepository<QANotification, Long> {

    @Query("select qa_notification from QANotification qa_notification where qa_notification.qsaId.login = ?#{principal.username}")
    List<QANotification> findByQsaIdIsCurrentUser();

	QANotification findByQaId(User qaUser);

	QANotification findByQsaId(User qsaUser);

}
