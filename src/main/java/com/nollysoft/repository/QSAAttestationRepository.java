package com.nollysoft.repository;

import com.nollysoft.domain.QSAAttestation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the QSAAttestation entity.
 */
@Repository
public interface QSAAttestationRepository extends JpaRepository<QSAAttestation, Long> {

}
