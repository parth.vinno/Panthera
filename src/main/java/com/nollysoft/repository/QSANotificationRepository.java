package com.nollysoft.repository;

import com.nollysoft.domain.QSANotification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the QSANotification entity.
 */
@Repository
public interface QSANotificationRepository extends JpaRepository<QSANotification, Long> {

    @Query("select qsa_notification from QSANotification qsa_notification where qsa_notification.qaUserId.login = ?#{principal.username}")
    List<QSANotification> findByQaUserIdIsCurrentUser();

}
