package com.nollysoft.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.RequirementCategory;

/**
 * Spring Data JPA repository for the RequirementCategory entity.
 */
@Repository
public interface RequirementCategoryRepository extends JpaRepository<RequirementCategory, Long> {

	@Query("SELECT r.id FROM RequirementCategory AS r")
	Set<Long> getAllIds();

	@Query(nativeQuery = true, value = "SELECT id from (SELECT r.id, COUNT(s.id) AS questions_count, COUNT(res.id) AS response_count FROM saq_question s LEFT JOIN pcidss_requirement p ON s.pcidss_requirement_id = p.id LEFT JOIN requirement_category r ON p.requirement_category_id = r.id LEFT JOIN saq_response res ON s.id = res.saq_question_id WHERE s.response_required=true GROUP BY r.id) AS cat_response WHERE questions_count=response_count;")
	Set<Long> getRequirementCategoryIdsBySaqQuestionResponseRequired();

	@Query(nativeQuery = true, value = "SELECT min(r.id) FROM saq_review AS sr JOIN saq_question sq ON sr.saq_question_id = sq.id JOIN pcidss_requirement AS p ON sq.pcidss_requirement_id = p.id JOIN requirement_category r ON p.requirement_category_id = r.id WHERE sr.status = 'CORRECTED'GROUP BY r.id;")
	List<BigInteger> getRequirementCategoryIdsWithCorrectedResponse();
	
	RequirementCategory getOneByCategoryName(String categoryName);
}
