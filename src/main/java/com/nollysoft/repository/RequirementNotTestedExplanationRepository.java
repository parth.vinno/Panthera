package com.nollysoft.repository;

import com.nollysoft.domain.NonApplicabilityExplanation;
import com.nollysoft.domain.RequirementNotTestedExplanation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the RequirementNotTestedExplanation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequirementNotTestedExplanationRepository extends JpaRepository<RequirementNotTestedExplanation, Long> {

	@Query("SELECT rn FROM RequirementNotTestedExplanation AS rn JOIN rn.saqQuestionId AS s  WHERE s.id =:saqQuestionId")
	RequirementNotTestedExplanation findOneBySaqQuestionId(@Param("saqQuestionId") Long saqQuestionId);
}
