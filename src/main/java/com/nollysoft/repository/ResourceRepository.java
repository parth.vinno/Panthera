package com.nollysoft.repository;

import com.nollysoft.domain.Resource;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Resource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourceRepository extends JpaRepository<Resource,Long> {
    
    @Query("select distinct resource from Resource resource left join fetch resource.userGroups")
    List<Resource> findAllWithEagerRelationships();

    @Query("select resource from Resource resource left join fetch resource.userGroups where resource.id =:id")
    Resource findOneWithEagerRelationships(@Param("id") Long id);
    
}
