package com.nollysoft.repository;

import com.nollysoft.domain.SaqInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SaqInfo entity.
 */
@Repository
public interface SaqInfoRepository extends JpaRepository<SaqInfo, Long> {

}
