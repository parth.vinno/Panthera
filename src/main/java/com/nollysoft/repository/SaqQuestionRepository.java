package com.nollysoft.repository;

import java.math.BigInteger;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.SaqQuestion;

/**
 * Spring Data JPA repository for the SaqQuestion entity.
 */
@Repository
public interface SaqQuestionRepository extends JpaRepository<SaqQuestion, Long> {

	Page<SaqQuestion> findByParentIdIsNull(Pageable pageable);

	@Query("SELECT s FROM SaqQuestion AS s JOIN s.pcidssRequirementId AS p  WHERE p.id =:pcidssRequirementId AND s.parentId IS NULL")
	Set<SaqQuestion> getByPcidssRequirementIdAndParentIdIsNull(@Param("pcidssRequirementId") Long pcidssRequirementId);

	@Query(nativeQuery = true, value = "SELECT q.id AS qId FROM pcidss_requirement AS p JOIN saq_question AS q ON p.id = q.pcidss_requirement_id JOIN saq_response AS r ON q.id = r.saq_question_id WHERE p.id =:pcidssRequirementId ORDER BY r.created_at DESC limit 1")
	Long getLastRespondedQuestionId(@Param("pcidssRequirementId") Long pcidssRequirementId);

	SaqQuestion findByQuestionNumber(String questionNumber);

	@Query("SELECT saqQuestion FROM SaqQuestion AS saqQuestion JOIN saqQuestion.pcidssRequirementId AS pcidssRequirement WHERE pcidssRequirement.id =:pcidssRequirementId AND saqQuestion.parentId IS NULL")
	Page<SaqQuestion> findSaqQuestionByPcidssRequirement(@Param("pcidssRequirementId") Long pcidssRequirementId, Pageable pageable);

	@Query(nativeQuery = true, value = "SELECT min(sq.id) FROM saq_review AS sr JOIN saq_question sq ON sr.saq_question_id = sq.id JOIN pcidss_requirement AS p ON sq.pcidss_requirement_id = p.id WHERE p.id =:pcidssRequirementId AND sr.status = 'CORRECTED' GROUP BY sq.id;")
	Set<BigInteger> getQuestionIdsForRequirementWithCorrectedResponse(@Param("pcidssRequirementId") Long pcidssRequirementId);
}

