package com.nollysoft.repository;

import com.nollysoft.domain.SaqResponse;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the SaqResponse entity.
 */
@Repository
public interface SaqResponseRepository extends JpaRepository<SaqResponse, Long> {

	@Query("SELECT sr FROM SaqResponse AS sr JOIN sr.saqQuestionId AS s  WHERE s.id =:saqQuestionId")
	SaqResponse findOneBySaqQuestionId(@Param("saqQuestionId") Long saqQuestionId);

	@Query("SELECT COUNT(sr.id) FROM SaqResponse AS sr JOIN sr.saqQuestionId AS s JOIN s.pcidssRequirementId AS p WHERE p.id =:pcidssRequirementId")
	int countByPcidssRequirementId(@Param("pcidssRequirementId") Long pcidssRequirementId);
}
