package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.SaqReviewComment;

/**
 * Spring Data JPA repository for the SaqReviewComment entity.
 */
@Repository
public interface SaqReviewCommentRepository extends JpaRepository<SaqReviewComment, Long> {

}
