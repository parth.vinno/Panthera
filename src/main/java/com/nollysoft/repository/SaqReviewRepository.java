package com.nollysoft.repository;

import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.domain.SaqReview;
import com.nollysoft.domain.SaqReview.REVIEW_STATUS;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SaqReview entity.
 */
@Repository
public interface SaqReviewRepository extends JpaRepository<SaqReview, Long> {

	SaqReview findOneBySaqQuestionId(SaqQuestion saqQuestionId);

	Page<SaqReview> findByStatus(REVIEW_STATUS status, Pageable pageable);
	
	@Query("SELECT COUNT(sr) FROM SaqReview as sr")
	long count();
	
	@Query(nativeQuery = true, value = "SELECT * FROM saq_review ORDER BY last_updated_date DESC limit 1")
	SaqReview getLastUpdated();
}
