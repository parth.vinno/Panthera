package com.nollysoft.repository;

import com.nollysoft.domain.ScopeVerificationNotSelectedService;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ScopeVerificationNotSelectedService entity.
 */
@Repository
public interface ScopeVerificationNotSelectedServiceRepository extends JpaRepository<ScopeVerificationNotSelectedService,Long> {
    
}
