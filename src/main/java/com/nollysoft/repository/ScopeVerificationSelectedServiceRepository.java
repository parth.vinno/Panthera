package com.nollysoft.repository;

import com.nollysoft.domain.ScopeVerificationSelectedService;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ScopeVerificationSelectedService entity.
 */
@Repository
public interface ScopeVerificationSelectedServiceRepository extends JpaRepository<ScopeVerificationSelectedService,Long> {
    
}
