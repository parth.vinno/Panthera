package com.nollysoft.repository;

import com.nollysoft.domain.SelectedServiceType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SelectedServiceType entity.
 */
@Repository
public interface SelectedServiceTypeRepository extends JpaRepository<SelectedServiceType,Long> {
    
}
