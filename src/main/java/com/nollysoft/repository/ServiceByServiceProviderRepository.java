package com.nollysoft.repository;

import com.nollysoft.domain.ServiceByServiceProvider;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceByServiceProvider entity.
 */
@Repository
public interface ServiceByServiceProviderRepository extends JpaRepository<ServiceByServiceProvider,Long> {
    
}
