package com.nollysoft.repository;

import com.nollysoft.domain.ServiceCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceCategory entity.
 */
@Repository
public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory,Long> {
    
}
