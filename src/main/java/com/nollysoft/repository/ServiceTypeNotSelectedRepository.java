package com.nollysoft.repository;

import com.nollysoft.domain.ServiceTypeNotSelected;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceTypeNotSelected entity.
 */
@Repository
public interface ServiceTypeNotSelectedRepository extends JpaRepository<ServiceTypeNotSelected,Long> {
    
}
