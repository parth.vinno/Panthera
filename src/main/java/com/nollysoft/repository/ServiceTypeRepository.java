package com.nollysoft.repository;

import com.nollysoft.domain.ServiceType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceType entity.
 */
@Repository
public interface ServiceTypeRepository extends JpaRepository<ServiceType,Long> {
    
}
