package com.nollysoft.repository;

import com.nollysoft.domain.State;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the State entity.
 */
@Repository
public interface StateRepository extends JpaRepository<State,String> {

    @Query("from State s where s.countryCode = :countryCode order by longName asc")
    List<State> findAllByCountryCodeOrderByLongName(@Param(value = "countryCode") String countryCode);
}
