package com.nollysoft.repository;

import com.nollysoft.domain.Tenant;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tenant entity.
 */
@Repository
public interface TenantRepository extends JpaRepository<Tenant,Long> {

	Tenant findOneByCompanyAbbreviation(String companyAbbreviation);

	Tenant findOneByTenantIdAndDeletedIsFalse(String tenantId);

	Page<Tenant> findByDeletedIsFalse(Pageable pageable);

	Tenant findOneByIdAndDeletedIsFalse(Long id);
    
}
