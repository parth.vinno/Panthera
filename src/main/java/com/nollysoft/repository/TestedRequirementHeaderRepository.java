package com.nollysoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.TestedRequirementHeader;

/**
 * Spring Data JPA repository for the TestedRequirementHeader entity.
 */
@Repository
public interface TestedRequirementHeaderRepository extends JpaRepository<TestedRequirementHeader, Long> {

}
