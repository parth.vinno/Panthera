package com.nollysoft.repository;

import com.nollysoft.domain.TestedRequirementSummary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TestedRequirementSummary entity.
 */
@Repository
public interface TestedRequirementSummaryRepository extends JpaRepository<TestedRequirementSummary,Long> {
    
}
