package com.nollysoft.repository;

import com.nollysoft.domain.ThirdPartyServiceProvider;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ThirdPartyServiceProvider entity.
 */
@Repository
public interface ThirdPartyServiceProviderRepository extends JpaRepository<ThirdPartyServiceProvider,Long> {
    
}
