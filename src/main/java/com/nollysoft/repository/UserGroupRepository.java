package com.nollysoft.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.UserGroup;

/**
 * Spring Data JPA repository for the UserGroup entity.
 */
@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

	@Query("select distinct user_group from UserGroup user_group left join fetch user_group.authorities")
	List<UserGroup> findAllWithEagerRelationships();

	@Query("select user_group from UserGroup user_group left join fetch user_group.authorities where user_group.id =:id")
	UserGroup findOneWithEagerRelationships(@Param("id") Long id);

	UserGroup findOneByName(String name);

	Page<UserGroup> findAllByNameNot(Pageable pageable, String string);

}
