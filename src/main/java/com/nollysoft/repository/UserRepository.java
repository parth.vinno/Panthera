package com.nollysoft.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nollysoft.domain.User;
import com.nollysoft.domain.enumeration.UserRole;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findOneByActivationKey(String activationKey);

	List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

	Optional<User> findOneByResetKey(String resetKey);

	Optional<User> findOneByEmail(String email);

	Optional<User> findOneByLogin(String login);

	@EntityGraph(attributePaths = "userGroups")
	User findOneWithUserGroupsById(Long id);

	@EntityGraph(attributePaths = "userGroups")
	Optional<User> findOneWithUserGroupsByLogin(String login);

	Page<User> findAllByLoginNot(Pageable pageable, String login);

	Page<User> findAllByUserGroupsNameOrUserGroupsNameAndDeletedIsFalse(Pageable pageable, String userGroupName1, String userGroupName2);

	Page<User> findAllByTenantIdAndDeletedIsFalse(Pageable pageable, String tenantId);

	List<User> findAllByTenantId(String tenantId);

	List<User> findByRole(UserRole role);

	int countByUserGroupsName(String userGroupName);
}
