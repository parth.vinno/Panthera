package com.nollysoft.security;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.Privilege;
import com.nollysoft.domain.User;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.service.TenancyService;
import com.nollysoft.tenancy.user.TenantUser;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);

    private final UserRepository userRepository;

    private final TenancyService tenancyService;

	public DomainUserDetailsService(UserRepository userRepository, TenancyService tenancyService) {
		this.userRepository = userRepository;
		this.tenancyService = tenancyService;
	}
    
    private List<String> getAuthoritiesWithPermission(User user){
    	List<String> roles1 = new ArrayList<>();
    	user.getUserGroups().stream().forEach(group->{
    		group.getAuthorities().stream().forEach(authority ->{
				roles1.add(authority.getName());
				
				for (Privilege privilege: authority.getPrivileges()) {
					roles1.add("ROLE_"+privilege.getName());
				}
			});
    	});
    	return roles1;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        Optional<User> userFromDatabase = userRepository.findOneWithUserGroupsByLogin(lowercaseLogin);
		return userFromDatabase.map(user -> {
			if (user.isDeleted()) {
				throw new UserDeletedException("error.userDeleted");
			}
			if (!user.getActivated()) {
				user.setActivated(true);
				try {
					tenancyService.activateUserInTenantDB(user);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
            List<String> roles = getAuthoritiesWithPermission(user);
            List<GrantedAuthority> grantedAuthorities = roles.stream()
                    .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
//            return new org.springframework.security.core.userdetails.User(lowercaseLogin,
//                user.getPassword(),
//                grantedAuthorities);
            
            // Return TenantUser which is the type org.springframework.security.core.userdetails.User
            TenantUser tenantUser = new TenantUser(lowercaseLogin, user.getPassword(), grantedAuthorities);
            tenantUser.setTenantId(user.getTenantId());
            return tenantUser;
        }).orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " +
        "database"));
    }
}
