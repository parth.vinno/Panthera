package com.nollysoft.security;

import org.springframework.security.core.AuthenticationException;

/**
 * This exception is thrown in case of a not activated user trying to authenticate.
 */
public class UserDeletedException extends AuthenticationException {

    private static final long serialVersionUID = 1L;

    public UserDeletedException(String message) {
        super(message);
    }

    public UserDeletedException(String message, Throwable t) {
        super(message, t);
    }
}
