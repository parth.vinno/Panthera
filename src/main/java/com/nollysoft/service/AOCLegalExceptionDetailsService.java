package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.AOCLegalExceptionDetails;
import com.nollysoft.repository.AOCLegalExceptionDetailsRepository;
import com.nollysoft.service.dto.AOCLegalExceptionDetailsDTO;

/**
 * Service Implementation for managing AOCLegalExceptionDetails.
 */
@Service
@Transactional
public class AOCLegalExceptionDetailsService {

	private final Logger log = LoggerFactory.getLogger(AOCLegalExceptionDetailsService.class);

	private final AOCLegalExceptionDetailsRepository aocLegalExceptionDetailsRepository;

	public AOCLegalExceptionDetailsService(AOCLegalExceptionDetailsRepository aocLegalExceptionDetailsRepository) {
		this.aocLegalExceptionDetailsRepository = aocLegalExceptionDetailsRepository;
	}

	/**
	 * Save an AOCLegalExceptionDetails.
	 *
	 * @param AOCLegalExceptionDetails
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AOCLegalExceptionDetails save(AOCLegalExceptionDetails aocLegalExceptionDetails) {
		log.debug("Request to save AOCLegalExceptionDetails : {}", aocLegalExceptionDetails);
		return aocLegalExceptionDetailsRepository.save(aocLegalExceptionDetails);
	}

	/**
	 * Delete and Save list of AOCLegalExceptionDetails.
	 *
	 * @param list
	 *            of AOCLegalExceptionDetails the entity to save
	 * @return the persisted list of entity
	 */
	public List<AOCLegalExceptionDetailsDTO> saveOrUpdate(List<AOCLegalExceptionDetails> aocLegalExceptionDetails) {
		log.debug("Request to save AOCLegalExceptionDetails : {}", aocLegalExceptionDetails);
		validate(aocLegalExceptionDetails);
		aocLegalExceptionDetailsRepository.deleteAll();
		List<AOCLegalExceptionDetailsDTO> result = new ArrayList<>();
		aocLegalExceptionDetailsRepository.save(aocLegalExceptionDetails).stream().forEach(detail -> {
			result.add(new AOCLegalExceptionDetailsDTO(detail));
		});
		return result;
	}

	private void validate(List<AOCLegalExceptionDetails> aocLegalExceptionDetails) {
		aocLegalExceptionDetails.stream().forEach(aocLegalExceptionDetail -> {
			Long id = aocLegalExceptionDetail.getId();
			if (id != null && aocLegalExceptionDetailsRepository.findOne(id) == null)
				throw new RuntimeException("No AOCLegalExceptionDetails with {id}:" + id);
		});
	}

	/**
	 * Get All AOCLegalExceptionDetails.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<AOCLegalExceptionDetailsDTO> get() {
		log.debug("Request to get AOCLegalExceptionDetails : {}");
		List<AOCLegalExceptionDetailsDTO> result = new ArrayList<>();
		aocLegalExceptionDetailsRepository.findAll().stream().forEach(detail -> {
			result.add(new AOCLegalExceptionDetailsDTO(detail));
		});
		return result;
	}

	/**
	 * Delete the AOCLegalExceptionDetails by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AOCLegalExceptionDetails : {}", id);
		aocLegalExceptionDetailsRepository.delete(id);
	}
}
