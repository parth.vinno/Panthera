package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.AOCNotification;
import com.nollysoft.domain.User;
import com.nollysoft.repository.AOCNotificationRepository;
import com.nollysoft.repository.QANotificationRepository;

/**
 * Service Implementation for managing AOCNotification.
 */
@Service
@Transactional
public class AOCNotificationService {

	private final Logger log = LoggerFactory.getLogger(AOCNotificationService.class);

	private final AOCNotificationRepository aocNotificationRepository;

	private final QANotificationRepository qaNotificationRepository;

	public AOCNotificationService(AOCNotificationRepository aocNotificationRepository,
			QANotificationRepository qaNotificationRepository) {
		this.aocNotificationRepository = aocNotificationRepository;
		this.qaNotificationRepository = qaNotificationRepository;
	}

	/**
	 * Save a aocNotification.
	 *
	 * @param aocNotification
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AOCNotification save(AOCNotification aocNotification) {
		log.debug("Request to save AOCNotification : {}", aocNotification);
		return aocNotificationRepository.save(aocNotification);
	}

	/**
	 * Get all the aocNotifications.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AOCNotification> findAll() {
		log.debug("Request to get all AOCNotifications");
		return aocNotificationRepository.findAll();
	}

	/**
	 * Get one aocNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AOCNotification findOne(Long id) {
		log.debug("Request to get AOCNotification : {}", id);
		return aocNotificationRepository.findOne(id);
	}

	/**
	 * Delete the aocNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AOCNotification : {}", id);
		aocNotificationRepository.delete(id);
	}

	public AOCNotification create(User aocUser, User qaUser) {
		AOCNotification aocNotification = new AOCNotification();
		aocNotification.setAocId(aocUser);
		aocNotification.setQaNotificationId(qaNotificationRepository.findByQaId(qaUser));
		return aocNotificationRepository.save(aocNotification);
	}
}
