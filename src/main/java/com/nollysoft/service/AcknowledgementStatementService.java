package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.AcknowledgementStatement;
import com.nollysoft.repository.AcknowledgementStatementRepository;
import com.nollysoft.repository.SaqInfoRepository;

/**
 * Service Implementation for managing AcknowledgementStatement.
 */
@Service
@Transactional
public class AcknowledgementStatementService {

	private final Logger log = LoggerFactory
			.getLogger(AcknowledgementStatementService.class);

	private final AcknowledgementStatementRepository acknowledgementStatementRepository;

	private final SaqInfoRepository saqInfoRepository;

	public AcknowledgementStatementService(
			AcknowledgementStatementRepository acknowledgementStatementRepository , SaqInfoRepository saqInfoRepository) {
		this.acknowledgementStatementRepository = acknowledgementStatementRepository;
		this.saqInfoRepository = saqInfoRepository;
	}

	/**
	 * Save a AcknowledgementStatement.
	 *
	 * @param AcknowledgementStatement
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AcknowledgementStatement save(AcknowledgementStatement acknowledgementStatement) {
		log.debug("Request to save Attestation : {}", acknowledgementStatement);
		return acknowledgementStatementRepository.save(acknowledgementStatement);
	}

	/**
	 * Get All AcknowledgementStatement.
	 *
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<AcknowledgementStatement> get() {
		log.debug("Request to get AcknowledgementStatement : {}");
		List<AcknowledgementStatement> acknowledgementStatements = new ArrayList<AcknowledgementStatement>();
		acknowledgementStatementRepository.findAll().stream().forEach(acknowledgementStatement -> {
			String statement = acknowledgementStatement.getStatement();
			if (statement.contains("{version}")) {
				String saqVersion = saqInfoRepository.findAll().get(0).getSaqVersion();
				acknowledgementStatement.setStatement(statement.replace("{version}", saqVersion));
			}
			acknowledgementStatements.add(acknowledgementStatement);
		});
		return acknowledgementStatements;
	}

	/**
	 * Delete the AcknowledgementStatement by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AcknowledgementStatement : {}", id);
		acknowledgementStatementRepository.delete(id);
	}
}
