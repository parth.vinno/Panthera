package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.AcknowledgementStatus;
import com.nollysoft.repository.AcknowledgementStatusRepository;

/**
 * Service Implementation for managing AcknowledgementStatus.
 */
@Service
@Transactional
public class AcknowledgementStatusService {

	private final Logger log = LoggerFactory.getLogger(AcknowledgementStatusService.class);

	private final AcknowledgementStatusRepository acknowledgementStatusRepository;

	public AcknowledgementStatusService(AcknowledgementStatusRepository acknowledgementStatusRepository) {
		this.acknowledgementStatusRepository = acknowledgementStatusRepository;
	}

	/**
	 * Save a AcknowledgementStatus.
	 *
	 * @param AcknowledgementStatus
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AcknowledgementStatus save(AcknowledgementStatus acknowledgementStatus) {
		log.debug("Request to save Attestation : {}", acknowledgementStatus);
		return acknowledgementStatusRepository.save(acknowledgementStatus);
	}
	
	/**
	 * Delete and Save list of AcknowledgementStatus.
	 *
	 * @param list
	 *            of AcknowledgementStatus the entity to save
	 * @return the persisted list of entity
	 */
	public List<AcknowledgementStatus> saveOrUpdate(List<AcknowledgementStatus> acknowledgementStatuses) {
		log.debug("Request to save AcknowledgementStatus : {}", acknowledgementStatuses);
		validate(acknowledgementStatuses);
		acknowledgementStatusRepository.deleteAll();
		return acknowledgementStatusRepository.save(acknowledgementStatuses);
	}

	private void validate(List<AcknowledgementStatus> acknowledgementStatuses) {
		acknowledgementStatuses.stream().forEach(acknowledgementStatus -> {
			Long id = acknowledgementStatus.getId();
			if (id != null && acknowledgementStatusRepository.findOne(id) == null)
				throw new RuntimeException("No AcknowledgementStatus with {id}:" + id);
		});
	}

	/**
	 * Get All AcknowledgementStatus.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<AcknowledgementStatus> get() {
		log.debug("Request to get AcknowledgementStatus : {}");
		return acknowledgementStatusRepository.findAll();
	}

	/**
	 * Delete the AcknowledgementStatus by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AcknowledgementStatus : {}", id);
		acknowledgementStatusRepository.delete(id);
	}
}
