package com.nollysoft.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.AssessmentInfo;
import com.nollysoft.domain.User;
import com.nollysoft.repository.AssessmentInfoRepository;

/**
 * Service Implementation for managing AssessmentInfo.
 */
@Service
@Transactional
public class AssessmentInfoService {

    private final Logger log = LoggerFactory.getLogger(AssessmentInfoService.class);

    private final AssessmentInfoRepository assessmentInfoRepository;

	private final UserService userService;

	public AssessmentInfoService(AssessmentInfoRepository assessmentInfoRepository, UserService userService) {
		this.assessmentInfoRepository = assessmentInfoRepository;
		this.userService = userService;
	}

    /**
     * Save a assessmentInfo.
     *
     * @param assessmentInfo the entity to save
     * @return the persisted entity
     */
    public AssessmentInfo save(AssessmentInfo assessmentInfo) {
        log.debug("Request to save AssessmentInfo : {}", assessmentInfo);
        return assessmentInfoRepository.save(assessmentInfo);
    }

	/**
	 * Save assessment start Info.
	 *
	 * @param assessmentType
	 * @param assessor
	 * @return the persisted entity
	 */
	public AssessmentInfo saveAssessmentStartInfo() {
		log.debug("Request to save Assessment start Info");
		AssessmentInfo assessmentInfo = new AssessmentInfo();
		if (assessmentInfoRepository.findAll().size() == 0) {
			assessmentInfo.setAssessor(userService.getUserWithAuthorities().getFirstName());
			assessmentInfo.setAssessmentType(getAssessmentType(userService.getUserWithAuthorities()));
		} else {
			assessmentInfo = assessmentInfoRepository.findAll().get(0);
		}
		return assessmentInfoRepository.save(assessmentInfo);
	}

	private String getAssessmentType(User user) {
		String tenantId = user.getTenantId();
		String assessmentType = tenantId.split("_")[1];
		return assessmentType;
	}

    /**
     * Save assessment end Info.
     *
     * @return the persisted entity
     */
	public AssessmentInfo saveAssessmentEndInfo() {
		log.debug("Request to update Assessment end Info");
		AssessmentInfo assessmentInfo = new AssessmentInfo();
		if (assessmentInfoRepository.findAll().size() != 0) {
			assessmentInfo = assessmentInfoRepository.findAll().get(0);
			assessmentInfo.assessmentEndDate(LocalDate.now(ZoneId.of("UTC")));
			assessmentInfo = assessmentInfoRepository.save(assessmentInfo);
		}
        return assessmentInfo;
	}

    /**
     * Get all the assessmentInfos.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AssessmentInfo> findAll() {
        log.debug("Request to get all AssessmentInfos");
        return assessmentInfoRepository.findAll();
    }

    /**
     * Get one assessmentInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AssessmentInfo findOne(Long id) {
        log.debug("Request to get AssessmentInfo : {}", id);
        return assessmentInfoRepository.findOne(id);
    }

    /**
     * Delete the assessmentInfo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AssessmentInfo : {}", id);
        assessmentInfoRepository.delete(id);
    }
}
