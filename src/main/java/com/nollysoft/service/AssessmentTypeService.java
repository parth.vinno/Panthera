package com.nollysoft.service;

import com.nollysoft.domain.AssessmentType;
import com.nollysoft.repository.AssessmentTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing AssessmentType.
 */
@Service
@Transactional
public class AssessmentTypeService {

	private final Logger log = LoggerFactory.getLogger(AssessmentTypeService.class);

	private final AssessmentTypeRepository assessmentTypeRepository;

	public AssessmentTypeService(AssessmentTypeRepository assessmentTypeRepository) {
		this.assessmentTypeRepository = assessmentTypeRepository;
	}

	/**
	 * Save a assessmentType.
	 *
	 * @param assessmentType
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AssessmentType save(AssessmentType assessmentType) {
		log.debug("Request to save AssessmentType : {}", assessmentType);
		return assessmentTypeRepository.save(assessmentType);
	}

	/**
	 * Get all the assessmentTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AssessmentType> findAll(Pageable pageable) {
		log.debug("Request to get all AssessmentTypes");
		return assessmentTypeRepository.findAll(pageable);
	}

	/**
	 * Get one assessmentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AssessmentType findOne(Long id) {
		log.debug("Request to get AssessmentType : {}", id);
		return assessmentTypeRepository.findOne(id);
	}

	/**
	 * Delete the assessmentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AssessmentType : {}", id);
		assessmentTypeRepository.delete(id);
	}
}
