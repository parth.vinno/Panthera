package com.nollysoft.service;

import com.nollysoft.domain.AssessorCompany;
import com.nollysoft.repository.AssessorCompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing AssessorCompany.
 */
@Service
@Transactional
public class AssessorCompanyService {

    private final Logger log = LoggerFactory.getLogger(AssessorCompanyService.class);

    private final AssessorCompanyRepository assessorCompanyRepository;

    public AssessorCompanyService(AssessorCompanyRepository assessorCompanyRepository) {
        this.assessorCompanyRepository = assessorCompanyRepository;
    }

    /**
     * Save a assessorCompany.
     *
     * @param assessorCompany the entity to save
     * @return the persisted entity
     */
    public AssessorCompany save(AssessorCompany assessorCompany) {
        log.debug("Request to save AssessorCompany : {}", assessorCompany);
        return assessorCompanyRepository.save(assessorCompany);
        
    }

    /**
     *  Get all the assessorCompanies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AssessorCompany> findAll(Pageable pageable) {
        log.debug("Request to get all AccessorCompanies");
            return assessorCompanyRepository.findAll(pageable);
    }

    /**
     *  Get one assessorCompany by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AssessorCompany findOne(Long id) {
        log.debug("Request to get AssessorCompany : {}", id);
        return assessorCompanyRepository.findOne(id);
    }

    /**
     *  Delete the  assessorCompany by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AssessorCompany : {}", id);
        assessorCompanyRepository.delete(id);
    }
}
