package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.Attestation;
import com.nollysoft.repository.AttestationRepository;

/**
 * Service Implementation for managing Attestation.
 */
@Service
@Transactional
public class AttestationService {

	private final Logger log = LoggerFactory.getLogger(AttestationService.class);

	private final AttestationRepository attestationRepository;

	public AttestationService(AttestationRepository attestationRepository) {
		this.attestationRepository = attestationRepository;
	}

	/**
	 * Save a Attestation.
	 *
	 * @param Attestation
	 *            the entity to save
	 * @return the persisted entity
	 */
	public Attestation save(Attestation attestation) {
		log.debug("Request to save Attestation : {}", attestation);
		return attestationRepository.save(attestation);
	}

	/**
	 * Get Attestation.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<Attestation> get() {
		log.debug("Request to get Attestation : {}");
		return attestationRepository.findAll();
	}

	/**
	 * Delete the Attestation by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Attestation : {}", id);
		attestationRepository.delete(id);
	}
}
