package com.nollysoft.service;

import com.nollysoft.domain.CompensatingControlWorksheetResponse;
import com.nollysoft.repository.CompensatingControlWorksheetResponseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CompensatingControlWorksheetResponse.
 */
@Service
@Transactional
public class CompensatingControlWorksheetResponseService {

    private final Logger log = LoggerFactory.getLogger(CompensatingControlWorksheetResponseService.class);

    private final CompensatingControlWorksheetResponseRepository compensatingControlWorksheetResponseRepository;

    public CompensatingControlWorksheetResponseService(CompensatingControlWorksheetResponseRepository compensatingControlWorksheetResponseRepository) {
        this.compensatingControlWorksheetResponseRepository = compensatingControlWorksheetResponseRepository;
    }

    /**
     * Save a compensatingControlWorksheetResponse.
     *
     * @param compensatingControlWorksheetResponse the entity to save
     * @return the persisted entity
     */
    public CompensatingControlWorksheetResponse save(CompensatingControlWorksheetResponse compensatingControlWorksheetResponse) {
        log.debug("Request to save CompensatingControlWorksheetResponse : {}", compensatingControlWorksheetResponse);
        return compensatingControlWorksheetResponseRepository.save(compensatingControlWorksheetResponse);
    }

    /**
     * Get all the compensatingControlWorksheetResponses.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CompensatingControlWorksheetResponse> findAll() {
        log.debug("Request to get all CompensatingControlWorksheetResponses");
        return compensatingControlWorksheetResponseRepository.findAll();
    }

    /**
     * Get one compensatingControlWorksheetResponse by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CompensatingControlWorksheetResponse findOne(Long id) {
        log.debug("Request to get CompensatingControlWorksheetResponse : {}", id);
        return compensatingControlWorksheetResponseRepository.findOne(id);
    }

    /**
     * Delete the compensatingControlWorksheetResponse by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CompensatingControlWorksheetResponse : {}", id);
        compensatingControlWorksheetResponseRepository.delete(id);
    }
}
