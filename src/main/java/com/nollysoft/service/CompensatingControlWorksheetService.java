package com.nollysoft.service;

import com.nollysoft.domain.CompensatingControlWorksheet;
import com.nollysoft.repository.CompensatingControlWorksheetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CompensatingControlWorksheet.
 */
@Service
@Transactional
public class CompensatingControlWorksheetService {

    private final Logger log = LoggerFactory.getLogger(CompensatingControlWorksheetService.class);

    private final CompensatingControlWorksheetRepository compensatingControlWorksheetRepository;

    public CompensatingControlWorksheetService(CompensatingControlWorksheetRepository compensatingControlWorksheetRepository) {
        this.compensatingControlWorksheetRepository = compensatingControlWorksheetRepository;
    }

    /**
     * Save a compensatingControlWorksheet.
     *
     * @param compensatingControlWorksheet the entity to save
     * @return the persisted entity
     */
    public CompensatingControlWorksheet save(CompensatingControlWorksheet compensatingControlWorksheet) {
        log.debug("Request to save CompensatingControlWorksheet : {}", compensatingControlWorksheet);
        return compensatingControlWorksheetRepository.save(compensatingControlWorksheet);
    }

    /**
     *  Get all the compensatingControlWorksheets.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CompensatingControlWorksheet> findAll(Pageable pageable) {
        log.debug("Request to get all CompensatingControlWorksheets");
        return compensatingControlWorksheetRepository.findAll(pageable);
    }

    /**
     *  Get one compensatingControlWorksheet by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CompensatingControlWorksheet findOne(Long id) {
        log.debug("Request to get CompensatingControlWorksheet : {}", id);
        return compensatingControlWorksheetRepository.findOne(id);
    }

    /**
     *  Delete the  compensatingControlWorksheet by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CompensatingControlWorksheet : {}", id);
        compensatingControlWorksheetRepository.delete(id);
    }
}
