package com.nollysoft.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ComplianceAttestation;
import com.nollysoft.repository.AOCLegalExceptionDetailsRepository;
import com.nollysoft.repository.AcknowledgementStatusRepository;
import com.nollysoft.repository.AttestationRepository;
import com.nollysoft.repository.ComplianceAttestationRepository;
import com.nollysoft.repository.ISAAttestationRepository;
import com.nollysoft.repository.NonCompliantRequirementActionPlanRepository;
import com.nollysoft.repository.QSAAttestationRepository;

import io.undertow.util.BadRequestException;

/**
 * Service Implementation for managing ComplianceAttestation.
 */
@Service
@Transactional
public class ComplianceAttestationService {

    private final Logger log = LoggerFactory.getLogger(ComplianceAttestationService.class);

    private final ComplianceAttestationRepository complianceAttestationRepository;

    private final AOCLegalExceptionDetailsRepository aocLegalExceptionDetailsRepository;

    private final AcknowledgementStatusRepository acknowledgementStatusRepository;

    private final AttestationRepository attestationRepository;

    private final QSAAttestationRepository qsaAttestationRepository;

    private final ISAAttestationRepository isaAttestationRepository;

    private final NonCompliantRequirementActionPlanRepository nonCompliantRequirementActionPlanRepository;

    public ComplianceAttestationService(ComplianceAttestationRepository complianceAttestationRepository,
                                        AOCLegalExceptionDetailsRepository aocLegalExceptionDetailsRepository,
                                        AcknowledgementStatusRepository acknowledgementStatusRepository,
                                        AttestationRepository attestationRepository, QSAAttestationRepository qsaAttestationRepository,
                                        ISAAttestationRepository isaAttestationRepository,
                                        NonCompliantRequirementActionPlanRepository nonCompliantRequirementActionPlanRepository) {
        this.complianceAttestationRepository = complianceAttestationRepository;
        this.aocLegalExceptionDetailsRepository = aocLegalExceptionDetailsRepository;
        this.acknowledgementStatusRepository = acknowledgementStatusRepository;
        this.attestationRepository = attestationRepository;
        this.qsaAttestationRepository = qsaAttestationRepository;
        this.isaAttestationRepository = isaAttestationRepository;
        this.nonCompliantRequirementActionPlanRepository = nonCompliantRequirementActionPlanRepository;
    }

    /**
     * Save a ComplianceAttestation.
     *
     * @param ComplianceAttestation
     *            the entity to save
     * @return the persisted entity
     * @throws BadRequestException
     */
    public ComplianceAttestation save(ComplianceAttestation complianceAttestation) throws BadRequestException {
        log.debug("Request to save ComplianceAttestation : {}", complianceAttestation);
        if (complianceAttestation.getId() != null) {
            clearDataForComplianceUpdate(complianceAttestation);
        }
        validate(complianceAttestation);
        return complianceAttestationRepository.save(complianceAttestation);
    }

    /**
     *
     * Get All ComplianceAttestation.
     *
     * @return the list of the entity
     */
    @Transactional(readOnly = true)
    public List<ComplianceAttestation> get() {
        log.debug("Request to get Attestation : {}");
        return complianceAttestationRepository.findAll();
    }

    /**
     * Delete the ComplianceAttestation by id.
     *
     * @param id
     *            the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ComplianceAttestation : {}", id);
        complianceAttestationRepository.delete(id);
    }

    /**
     * Clear all related data if compliance statement modified on update
     * @param complianceAttestation
     */
    private void clearDataForComplianceUpdate(ComplianceAttestation complianceAttestation) {
        ComplianceAttestation existingComplianceAttestation = complianceAttestationRepository
            .findOne(complianceAttestation.getId());
        if (existingComplianceAttestation != null && (existingComplianceAttestation
            .getCompliantStatement() != complianceAttestation.getCompliantStatement())) {
            aocLegalExceptionDetailsRepository.deleteAll();
            acknowledgementStatusRepository.deleteAll();
            attestationRepository.deleteAll();
            qsaAttestationRepository.deleteAll();
            isaAttestationRepository.deleteAll();
            nonCompliantRequirementActionPlanRepository.deleteAll();
        }
    }

    /**
     * throw exception for invalid input
     * @param complianceAttestation
     * @throws BadRequestException
     */
    private void validate(ComplianceAttestation complianceAttestation) throws BadRequestException {
        if (complianceAttestation.getCompliantStatement() == ComplianceAttestation.COMPLIANT_STATEMENT.NON_COMPLIANT) {
            if (complianceAttestation.getComplianceTargetDate() == null) {
                throw new BadRequestException("Provide Compliance target date");
            } else if (complianceAttestation.getComplianceTargetDate().isBefore(LocalDate.now())) {
                throw new BadRequestException("Target Date cannot be before today");
            }
        }
    }
}
