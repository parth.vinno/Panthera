package com.nollysoft.service;

import com.nollysoft.domain.Country;
import com.nollysoft.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing Country.
 */
@Service
@Transactional
public class CountryService {

    private final Logger log = LoggerFactory.getLogger(CountryService.class);

    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    /**
     * Save a country.
     *
     * @param country the entity to save
     * @return the persisted entity
     */
    public Country save(Country country) {
        log.debug("Request to save Country : {}", country);
        return countryRepository.save(country);
    }

    /**
     *  Get all the countries.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Country> findAll() {
        log.debug("Request to get all Countries");
        return countryRepository.findAll();
    }

    /**
     *  Get one country by id.
     *
     *  @param countryCode the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Country findOne(String countryCode) {
        log.debug("Request to get Country : {}", countryCode);
        return countryRepository.findOne(countryCode);
    }

    /**
     *  Delete the  country by id.
     *
     *  @param countryCode the id of the entity
     */
    public void delete(String countryCode) {
        log.debug("Request to delete Country : {}", countryCode);
        countryRepository.delete(countryCode);
    }
}
