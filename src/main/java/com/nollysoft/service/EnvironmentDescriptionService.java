package com.nollysoft.service;

import com.nollysoft.domain.EnvironmentDescription;
import com.nollysoft.repository.EnvironmentDescriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing EnvironmentDescription.
 */
@Service
@Transactional
public class EnvironmentDescriptionService {

    private final Logger log = LoggerFactory.getLogger(EnvironmentDescriptionService.class);

    private final EnvironmentDescriptionRepository environmentDescriptionRepository;

    public EnvironmentDescriptionService(EnvironmentDescriptionRepository environmentDescriptionRepository) {
        this.environmentDescriptionRepository = environmentDescriptionRepository;
    }

    /**
     * Save a environmentDescription.
     *
     * @param environmentDescription the entity to save
     * @return the persisted entity
     */
    public EnvironmentDescription save(EnvironmentDescription environmentDescription) {
        log.debug("Request to save EnvironmentDescription : {}", environmentDescription);
        return environmentDescriptionRepository.save(environmentDescription);
    }

    /**
     *  Get all the environmentDescriptions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EnvironmentDescription> findAll(Pageable pageable) {
        log.debug("Request to get all EnvironmentDescriptions");
        return environmentDescriptionRepository.findAll(pageable);
    }

    /**
     *  Get one environmentDescription by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public EnvironmentDescription findOne(Long id) {
        log.debug("Request to get EnvironmentDescription : {}", id);
        return environmentDescriptionRepository.findOne(id);
    }

    /**
     *  Delete the  environmentDescription by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EnvironmentDescription : {}", id);
        environmentDescriptionRepository.delete(id);
    }
}
