package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ExecutiveSummaryStatus;
import com.nollysoft.domain.enumeration.Section;
import com.nollysoft.repository.ExecutiveSummaryStatusRepository;
import com.nollysoft.service.dto.ExecutiveSummaryStatusDTO;
import com.nollysoft.service.dto.ExecutiveSummaryStatusDTO.STATUS;

/**
 * Service Implementation for managing ExecutiveSummaryStatus.
 */
@Service
@Transactional
public class ExecutiveSummaryStatusService {

	private final Logger log = LoggerFactory.getLogger(ExecutiveSummaryStatusService.class);

	private final ExecutiveSummaryStatusRepository executiveSummaryStatusRepository;

	public ExecutiveSummaryStatusService(ExecutiveSummaryStatusRepository executiveSummaryStatusRepository) {
		this.executiveSummaryStatusRepository = executiveSummaryStatusRepository;
	}

	/**
	 * Save a executiveSummaryStatus.
	 *
	 * @param executiveSummaryStatus
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ExecutiveSummaryStatus save(ExecutiveSummaryStatus executiveSummaryStatus) {
		log.debug("Request to save ExecutiveSummaryStatus : {}", executiveSummaryStatus);
		return executiveSummaryStatusRepository.save(executiveSummaryStatus);
	}

	/**
	 * Save or Update a executiveSummaryStatus.
	 *
	 * @param section of
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ExecutiveSummaryStatus createOrUpdate(String section) {
		log.debug("Request to save ExecutiveSummaryStatus : {}", section);
		ExecutiveSummaryStatus executiveSummaryStatus = executiveSummaryStatusRepository
				.findOneBySection(Section.valueOf(section));
		if (executiveSummaryStatus != null) {
			return executiveSummaryStatusRepository.save(executiveSummaryStatus);
		} else {
			executiveSummaryStatus = new ExecutiveSummaryStatus();
			executiveSummaryStatus.setSection(Section.valueOf(section));
			return executiveSummaryStatusRepository.save(executiveSummaryStatus);
		}
	}

	/**
	 * Get all the executiveSummaryStatuses.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<ExecutiveSummaryStatus> findAll() {
		log.debug("Request to get all ExecutiveSummaryStatuses");
		return executiveSummaryStatusRepository.findAll();
	}

	/**
	 * Get one executiveSummaryStatus by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ExecutiveSummaryStatus findOne(Long id) {
		log.debug("Request to get ExecutiveSummaryStatus : {}", id);
		return executiveSummaryStatusRepository.findOne(id);
	}

	/**
	 * Delete the executiveSummaryStatus by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ExecutiveSummaryStatus : {}", id);
		executiveSummaryStatusRepository.delete(id);
	}

	public ExecutiveSummaryStatusDTO getStatus(int totalSections) {
		ExecutiveSummaryStatusDTO executiveSummaryStatusDTO = new ExecutiveSummaryStatusDTO();
		int sectionsCompleted = executiveSummaryStatusRepository.findAll().size();
		if (sectionsCompleted == 0) {
			executiveSummaryStatusDTO.setStatus(STATUS.START);
		} else if (sectionsCompleted < totalSections) {
			executiveSummaryStatusDTO.setStatus(STATUS.RESUME);
			executiveSummaryStatusDTO.setLastUpdatedSection(
					executiveSummaryStatusRepository.findLastUpdatedSection().getSectionAsEnum());
		} else {
			executiveSummaryStatusDTO.setStatus(STATUS.REVISIT);
		}
		return executiveSummaryStatusDTO;
	}
}
