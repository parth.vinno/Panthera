package com.nollysoft.service;

import com.nollysoft.domain.ExpectedTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.nollysoft.repository.ExpectedTestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing ExpectedTest.
 */
@Service
@Transactional
public class ExpectedTestService {

    private final Logger log = LoggerFactory.getLogger(ExpectedTestService.class);

    private final ExpectedTestRepository expectedTestRepository;

    public ExpectedTestService(ExpectedTestRepository expectedTestRepository) {
        this.expectedTestRepository = expectedTestRepository;
    }

    /**
     * Save a expectedTest.
     *
     * @param expectedTest the entity to save
     * @return the persisted entity
     */
    public ExpectedTest save(ExpectedTest expectedTest) {
        log.debug("Request to save ExpectedTest : {}", expectedTest);
        return expectedTestRepository.save(expectedTest);
    }

    /**
     * Get all the expectedTests.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ExpectedTest> findAll() {
        log.debug("Request to get all ExpectedTests");
        return expectedTestRepository.findAll();
    }
    
    /**
	 * Get all the expectedTests.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ExpectedTest> findAll(Pageable pageable) {
		log.debug("Request to get all ExpectedTests");
		return expectedTestRepository.findAll(pageable);
	}

    /**
     * Get one expectedTest by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ExpectedTest findOne(Long id) {
        log.debug("Request to get ExpectedTest : {}", id);
        return expectedTestRepository.findOne(id);
    }

    /**
     * Get count expectedTest by id.
     *
     * @param id the id of the entity
     * @return count
     */
    @Transactional(readOnly = true)
    public int getUseCount(Long id) {
        log.debug("Request to get ExpectedTest : {}", id);
        return expectedTestRepository.countById(id);
    }

    /**
     * Delete the expectedTest by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ExpectedTest : {}", id);
        expectedTestRepository.delete(id);
    }
}
