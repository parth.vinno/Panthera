package com.nollysoft.service;

import com.nollysoft.domain.HostingProvider;
import com.nollysoft.repository.HostingProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing HostingProvider.
 */
@Service
@Transactional
public class HostingProviderService {

    private final Logger log = LoggerFactory.getLogger(HostingProviderService.class);

    private final HostingProviderRepository hostingProviderRepository;

    public HostingProviderService(HostingProviderRepository hostingProviderRepository) {
        this.hostingProviderRepository = hostingProviderRepository;
    }

    /**
     * Save a hostingProvider.
     *
     * @param hostingProvider the entity to save
     * @return the persisted entity
     */
    public HostingProvider save(HostingProvider hostingProvider) {
        log.debug("Request to save HostingProvider : {}", hostingProvider);
        return hostingProviderRepository.save(hostingProvider);
    }

    /**
     *  Get all the hostingProviders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<HostingProvider> findAll(Pageable pageable) {
        log.debug("Request to get all HostingProviders");
        return hostingProviderRepository.findAll(pageable);
    }

    /**
     *  Get one hostingProvider by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public HostingProvider findOne(Long id) {
        log.debug("Request to get HostingProvider : {}", id);
        return hostingProviderRepository.findOne(id);
    }

    /**
     *  Delete the  hostingProvider by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete HostingProvider : {}", id);
        hostingProviderRepository.delete(id);
    }
}
