package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ISAAttestation;
import com.nollysoft.repository.ISAAttestationRepository;

/**
 * Service Implementation for managing ISAAttestation.
 */
@Service
@Transactional
public class ISAAttestationService {

	private final Logger log = LoggerFactory.getLogger(ISAAttestationService.class);

	private final ISAAttestationRepository isaAttestationRepository;

	public ISAAttestationService(ISAAttestationRepository isaAttestationRepository) {
		this.isaAttestationRepository = isaAttestationRepository;
	}

	/**
	 * Save an ISAAttestation.
	 *
	 * @param ISAAttestation
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ISAAttestation save(ISAAttestation isaAttestation) {
		log.debug("Request to save ISAAttestation : {}", isaAttestation);
		return isaAttestationRepository.save(isaAttestation);
	}

	/**
	 * Get All ISAAttestation.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<ISAAttestation> get() {
		log.debug("Request to get ISAAttestation : {}");
		return isaAttestationRepository.findAll();
	}

	/**
	 * Delete the ISAAttestation by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ISAAttestation : {}", id);
		isaAttestationRepository.delete(id);
	}
}
