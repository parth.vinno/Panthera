package com.nollysoft.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.nollysoft.domain.User;

import io.github.jhipster.config.JHipsterProperties;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	private static final String USER = "user";

	private static final String PASSWORD = "password";

	private static final String BASE_URL = "baseUrl";

	private static final String QA_NAME = "qaName";

	private static final String QSA_NAME = "qsaName";
	
	private static final String AOC_NAME = "aocName";

	private static final String COMPANY_NAME = "companyName";

	private static final String TODAYS_DATE = "date";

	private static final String ASSESSMENT_DATE = "assessmentDate";

	private final JHipsterProperties jHipsterProperties;

	private final JavaMailSender javaMailSender;

	private final MessageSource messageSource;

	private final SpringTemplateEngine templateEngine;

	public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
			MessageSource messageSource, SpringTemplateEngine templateEngine) {

		this.jHipsterProperties = jHipsterProperties;
		this.javaMailSender = javaMailSender;
		this.messageSource = messageSource;
		this.templateEngine = templateEngine;
	}

	@Async
	public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
				isHtml, to, subject, content);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			message.setTo(to);
			message.setFrom(jHipsterProperties.getMail().getFrom());
			message.setSubject(subject);
			message.setText(content, isHtml);
			javaMailSender.send(mimeMessage);
			log.debug("Sent email to User '{}'", to);
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.warn("Email could not be sent to user '{}'", to, e);
			} else {
				log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
			}
		}
	}

	@Async
	public void sendEmailFromTemplate(User user, String password, String templateName, String titleKey) {
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(PASSWORD, password);
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		String content = templateEngine.process(templateName, context);
		String subject = messageSource.getMessage(titleKey, null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);

	}

	@Async
	public void sendActivationEmail(User user) {
		log.debug("Sending activation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, null, "activationEmail", "email.activation.title");
	}

	@Async
	public void sendCreationEmail(User user, String password) {
		log.debug("Sending creation email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, password, "creationEmail", "email.creation.title");
	}

	@Async
	public void sendPasswordResetMail(User user) {
		log.debug("Sending password reset email to '{}'", user.getEmail());
		sendEmailFromTemplate(user, null, "passwordResetEmail", "email.reset.title");
	}

	@Async
	public void sendNotificationFromTemplate(User user, String qsaName, String qaName, String toEmail,
			String templateName, String titleKey, LocalDate assessmentDate, String companyName) {
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		LocalDate date = LocalDate.now();
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		context.setVariable(QSA_NAME, qsaName);
		context.setVariable(QA_NAME, qaName);
		context.setVariable(TODAYS_DATE, date);
		context.setVariable(ASSESSMENT_DATE, assessmentDate);
		context.setVariable(COMPANY_NAME, companyName);
		String content = templateEngine.process(templateName, context);
		String subject = messageSource.getMessage(titleKey, null, locale);
		sendEmail(toEmail, subject, content, false, true);

	}

	@Async
	public void sendNotificationMailToQA(User qsaUser, User qaUser, LocalDate assessmentDate, String companyName) {
		log.debug("Sending email notification to '{}'", qaUser.getEmail());
		String qaName = new StringBuilder(qaUser.getFirstName()).append(" ").append(qaUser.getLastName())
				.toString();
		String qsaName = new StringBuilder(qsaUser.getFirstName()).append(" ").append(qsaUser.getLastName()).toString();
		sendNotificationFromTemplate(qsaUser, qsaName, qaName, qaUser.getEmail(), "qaNotification",
				"email.qa.notification.title", assessmentDate, companyName);
	}

	@Async
	public void sendNotificationMailToQSA(User qaUser, User qsaUser, LocalDate assessmentDate, String companyName) {
		log.debug("Sending email notification to '{}'", qsaUser.getEmail());
		String qaName = new StringBuilder(qaUser.getFirstName()).append(" ").append(qaUser.getLastName())
				.toString();
		String qsaName = new StringBuilder(qsaUser.getFirstName()).append(" ").append(qsaUser.getLastName()).toString();
		sendNotificationFromTemplate(qaUser, qsaName, qaName, qsaUser.getEmail(), "qsaNotification",
				"email.qsa.notification.title", assessmentDate, companyName);
	}

	@Async
	public void sendNotificationMailToAOC(User qaUser, List<User> aocUsers, LocalDate assessmentDate,
			String companyName) {
		log.debug("Sending email notification to '{}' AOC Officers");
		String qaName = new StringBuilder(qaUser.getFirstName()).append(" ").append(qaUser.getLastName()).toString();
		sendAOCNotificationFromTemplate(qaUser, qaName, aocUsers, "aocNotification",
				"email.aoc.notification.title", assessmentDate, companyName);
	}

	@Async
	private void sendAOCNotificationFromTemplate(User qaUser, String qaName, List<User> aocUsers,
			String templateName, String titleKey, LocalDate assessmentDate, String companyName) {
		Locale locale = Locale.forLanguageTag(qaUser.getLangKey());
		Context context = new Context(locale);
		LocalDate date = LocalDate.now();
		context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		context.setVariable(QA_NAME, qaName);
		context.setVariable(TODAYS_DATE, date);
		context.setVariable(ASSESSMENT_DATE, assessmentDate);
		context.setVariable(COMPANY_NAME, companyName);
		String subject = messageSource.getMessage(titleKey, null, locale);
		aocUsers.stream().forEach(aocUser -> {
			String aocName = new StringBuilder(aocUser.getFirstName()).append(" ")
					.append(aocUser.getLastName()).toString();
			context.setVariable(AOC_NAME, aocName);
			String content = templateEngine.process(templateName, context);
			sendEmail(aocUser.getEmail(), subject, content, false, true);

		});

	}
}
