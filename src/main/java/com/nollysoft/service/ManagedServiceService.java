package com.nollysoft.service;

import com.nollysoft.domain.ManagedService;
import com.nollysoft.repository.ManagedServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ManagedService.
 */
@Service
@Transactional
public class ManagedServiceService {

    private final Logger log = LoggerFactory.getLogger(ManagedServiceService.class);

    private final ManagedServiceRepository managedServiceRepository;

    public ManagedServiceService(ManagedServiceRepository managedServiceRepository) {
        this.managedServiceRepository = managedServiceRepository;
    }

    /**
     * Save a managedService.
     *
     * @param managedService the entity to save
     * @return the persisted entity
     */
    public ManagedService save(ManagedService managedService) {
        log.debug("Request to save ManagedService : {}", managedService);
        return managedServiceRepository.save(managedService);
    }

    /**
     *  Get all the managedServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ManagedService> findAll(Pageable pageable) {
        log.debug("Request to get all ManagedServices");
        return managedServiceRepository.findAll(pageable);
    }

    /**
     *  Get one managedService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ManagedService findOne(Long id) {
        log.debug("Request to get ManagedService : {}", id);
        return managedServiceRepository.findOne(id);
    }

    /**
     *  Delete the  managedService by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ManagedService : {}", id);
        managedServiceRepository.delete(id);
    }
}
