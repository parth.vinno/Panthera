package com.nollysoft.service;

import com.nollysoft.domain.MerchantBusinessTypeSelected;
import com.nollysoft.repository.MerchantBusinessTypeSelectedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing MerchantBusinessTypeSelected.
 */
@Service
@Transactional
public class MerchantBusinessTypeSelectedService {

	private final Logger log = LoggerFactory.getLogger(MerchantBusinessTypeSelectedService.class);

	private final MerchantBusinessTypeSelectedRepository merchantBusinessTypeSelectedRepository;

	public MerchantBusinessTypeSelectedService(
			MerchantBusinessTypeSelectedRepository merchantBusinessTypeSelectedRepository) {
		this.merchantBusinessTypeSelectedRepository = merchantBusinessTypeSelectedRepository;
	}

	/**
	 * Save a merchantBusinessTypeSelected.
	 *
	 * @param merchantBusinessTypeSelected
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MerchantBusinessTypeSelected save(MerchantBusinessTypeSelected merchantBusinessTypeSelected) {
		log.debug("Request to save MerchantBusinessTypeSelected : {}", merchantBusinessTypeSelected);
		return merchantBusinessTypeSelectedRepository.save(merchantBusinessTypeSelected);
	}

	/**
	 * Save a list of merchantBusinessTypeSelected.
	 *
	 * @param list
	 *            of merchantBusinessTypeSelected to save
	 * @return the persisted list of entities
	 */
	public List<MerchantBusinessTypeSelected> create(List<MerchantBusinessTypeSelected> merchantBusinessTypesSelected) {
		merchantBusinessTypeSelectedRepository.deleteAll();
		List<MerchantBusinessTypeSelected> merchantBusinessTypesSelectedlist = new ArrayList<>();
		merchantBusinessTypesSelected.stream().forEach(merchantBusinessType -> {
			merchantBusinessTypesSelectedlist.add(merchantBusinessTypeSelectedRepository.save(merchantBusinessType));
		});
		return merchantBusinessTypesSelectedlist;
	}

	/**
	 * Get all the merchantBusinessTypeSelecteds.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<MerchantBusinessTypeSelected> findAll() {
		log.debug("Request to get all MerchantBusinessTypeSelecteds");
		return merchantBusinessTypeSelectedRepository.findAll();
	}

	/**
	 * get all the merchantBusinessTypeSelecteds where MerchantBusinessTypeId is
	 * null.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<MerchantBusinessTypeSelected> findAllWhereMerchantBusinessTypeIdIsNull() {
		log.debug("Request to get all merchantBusinessTypeSelecteds where MerchantBusinessTypeId is null");
		return StreamSupport.stream(merchantBusinessTypeSelectedRepository.findAll().spliterator(), false).filter(
				merchantBusinessTypeSelected -> merchantBusinessTypeSelected.getMerchantBusinessTypeId() == null)
				.collect(Collectors.toList());
	}

	/**
	 * Get one merchantBusinessTypeSelected by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MerchantBusinessTypeSelected findOne(Long id) {
		log.debug("Request to get MerchantBusinessTypeSelected : {}", id);
		return merchantBusinessTypeSelectedRepository.findOne(id);
	}

	/**
	 * Delete the merchantBusinessTypeSelected by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete MerchantBusinessTypeSelected : {}", id);
		merchantBusinessTypeSelectedRepository.delete(id);
	}
}
