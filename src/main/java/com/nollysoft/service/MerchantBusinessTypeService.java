package com.nollysoft.service;

import com.nollysoft.domain.MerchantBusinessType;
import com.nollysoft.repository.MerchantBusinessTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing MerchantBusinessType.
 */
@Service
@Transactional
public class MerchantBusinessTypeService {

    private final Logger log = LoggerFactory.getLogger(MerchantBusinessTypeService.class);

    private final MerchantBusinessTypeRepository merchantBusinessTypeRepository;

    public MerchantBusinessTypeService(MerchantBusinessTypeRepository merchantBusinessTypeRepository) {
        this.merchantBusinessTypeRepository = merchantBusinessTypeRepository;
    }

    /**
     * Save a merchantBusinessType.
     *
     * @param merchantBusinessType the entity to save
     * @return the persisted entity
     */
    public MerchantBusinessType save(MerchantBusinessType merchantBusinessType) {
        log.debug("Request to save MerchantBusinessType : {}", merchantBusinessType);
        return merchantBusinessTypeRepository.save(merchantBusinessType);
    }

    /**
     *  Get all the merchantBusinessTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<MerchantBusinessType> findAll() {
        log.debug("Request to get all MerchantBusinessTypes");
        return merchantBusinessTypeRepository.findAll();
    }

    /**
   	 * Get all the merchantBusinessType.
   	 *
   	 * @param pageable
   	 *            the pagination information
   	 * @return the list of entities
   	 */
   	@Transactional(readOnly = true)
   	public Page<MerchantBusinessType> findAll(Pageable pageable) {
   		log.debug("Request to get all MerchantBusinessTypes");
   		return merchantBusinessTypeRepository.findAll(pageable);
   	}
 
    /**
     *  Get one merchantBusinessType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MerchantBusinessType findOne(Long id) {
        log.debug("Request to get MerchantBusinessType : {}", id);
        return merchantBusinessTypeRepository.findOne(id);
    }

    /**
     *  Delete the  merchantBusinessType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MerchantBusinessType : {}", id);
        merchantBusinessTypeRepository.delete(id);
    }
}
