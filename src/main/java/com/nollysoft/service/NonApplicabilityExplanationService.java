package com.nollysoft.service;

import com.nollysoft.domain.NonApplicabilityExplanation;
import com.nollysoft.repository.NonApplicabilityExplanationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing NonApplicabilityExplanation.
 */
@Service
@Transactional
public class NonApplicabilityExplanationService {

    private final Logger log = LoggerFactory.getLogger(NonApplicabilityExplanationService.class);

    private final NonApplicabilityExplanationRepository nonApplicabilityExplanationRepository;

    public NonApplicabilityExplanationService(NonApplicabilityExplanationRepository nonApplicabilityExplanationRepository) {
        this.nonApplicabilityExplanationRepository = nonApplicabilityExplanationRepository;
    }

    /**
     * Save a nonApplicabilityExplanation.
     *
     * @param nonApplicabilityExplanation the entity to save
     * @return the persisted entity
     */
    public NonApplicabilityExplanation save(NonApplicabilityExplanation nonApplicabilityExplanation) {
        log.debug("Request to save NonApplicabilityExplanation : {}", nonApplicabilityExplanation);
        return nonApplicabilityExplanationRepository.save(nonApplicabilityExplanation);
    }

    /**
     * Get all the nonApplicabilityExplanations.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<NonApplicabilityExplanation> findAll() {
        log.debug("Request to get all NonApplicabilityExplanations");
        return nonApplicabilityExplanationRepository.findAll();
    }

    /**
     * Get one nonApplicabilityExplanation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public NonApplicabilityExplanation findOne(Long id) {
        log.debug("Request to get NonApplicabilityExplanation : {}", id);
        return nonApplicabilityExplanationRepository.findOne(id);
    }

    /**
     * Delete the nonApplicabilityExplanation by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NonApplicabilityExplanation : {}", id);
        nonApplicabilityExplanationRepository.delete(id);
    }
}
