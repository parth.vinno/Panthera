package com.nollysoft.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.NonCompliantRequirementActionPlan;
import com.nollysoft.repository.NonCompliantRequirementActionPlanRepository;
import com.nollysoft.service.dto.NonCompliantRequirementActionPlanDTO;

import io.undertow.util.BadRequestException;

/**
 * Service Implementation for managing NonCompliantRequirementActionPlan.
 */
@Service
@Transactional
public class NonCompliantRequirementActionPlanService {

	private final Logger log = LoggerFactory.getLogger(NonCompliantRequirementActionPlanService.class);

	private final NonCompliantRequirementActionPlanRepository nonCompliantRequirementActionPlanRepository;

	public NonCompliantRequirementActionPlanService(
			NonCompliantRequirementActionPlanRepository nonCompliantRequirementActionPlanRepository) {
		this.nonCompliantRequirementActionPlanRepository = nonCompliantRequirementActionPlanRepository;
	}

	/**
	 * Save a new NonCompliantRequirementActionPlan.
	 *
	 * @param NonCompliantRequirementActionPlan
	 *            the entity to save
	 * @return the persisted entity
	 */
	public NonCompliantRequirementActionPlan save(NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan) {
		log.debug("Request to save NonCompliantRequirementActionPlans : {}", nonCompliantRequirementActionPlan);
		if (nonCompliantRequirementActionPlan.getRemediationDate().isBefore(LocalDate.now())) {
			throw new RuntimeException("Remediation Date cannot be before today");
		}
		return nonCompliantRequirementActionPlanRepository.save(nonCompliantRequirementActionPlan);
	}

	/**
	 * Save List of NonCompliantRequirementActionPlan.
	 *
	 * @param List
	 *            of NonCompliantRequirementActionPlan the entities to save
	 * @return the persisted entities
	 * @throws RuntimeException
	 */
	public List<NonCompliantRequirementActionPlanDTO> saveOrUpdate(
			List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlans) throws BadRequestException {
		log.debug("Request to save NonCompliantRequirementActionPlans : {}", nonCompliantRequirementActionPlans);
		nonCompliantRequirementActionPlans.stream().forEach(nonCompliantRequirementActionPlan -> {
			validate(nonCompliantRequirementActionPlan);
		});
		List<NonCompliantRequirementActionPlanDTO> result = new ArrayList<>();
		nonCompliantRequirementActionPlanRepository.save(nonCompliantRequirementActionPlans).stream().forEach(plan -> {
			result.add(new NonCompliantRequirementActionPlanDTO(plan));
		});
		return result;
	}

	private void validate(NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan) {
		Long id = nonCompliantRequirementActionPlan.getId();
		if (id != null && nonCompliantRequirementActionPlanRepository.findOne(id) == null)
			throw new RuntimeException("No AOCLegalExceptionDetails with {id}:" + id);
		if (!nonCompliantRequirementActionPlan.isCompliant()) {
			if ((nonCompliantRequirementActionPlan.getRemediationDate() == null
					|| nonCompliantRequirementActionPlan.getRemediationPlan() == null)) {
				throw new RuntimeException("Provide Both remediation Date and Plan");
			} else if (nonCompliantRequirementActionPlan.getRemediationDate().isBefore(LocalDate.now())) {
				throw new RuntimeException("Remediation Date cannot be before today");
			}
		}
	}

	/**
	 * Get NonCompliantRequirementActionPlan.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<NonCompliantRequirementActionPlanDTO> get() {
		log.debug("Request to get NonCompliantRequirementActionPlan : {}");
		List<NonCompliantRequirementActionPlanDTO> result = new ArrayList<>();
		nonCompliantRequirementActionPlanRepository.findAll().stream().forEach(plan -> {
			result.add(new NonCompliantRequirementActionPlanDTO(plan));
		});
		return result;
	}

	/**
	 * Delete the NonCompliantRequirementActionPlan by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete NonCompliantRequirementActionPlan : {}", id);
		nonCompliantRequirementActionPlanRepository.delete(id);
	}
}
