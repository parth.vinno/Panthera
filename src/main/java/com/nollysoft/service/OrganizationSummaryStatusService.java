package com.nollysoft.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.OrganizationSummaryStatus;
import com.nollysoft.domain.OrganizationSummaryStatus.STATUS;
import com.nollysoft.domain.enumeration.OrganizationSection;
import com.nollysoft.repository.OrganizationSummaryStatusRepository;

/**
 * Service Implementation for managing OrganizationSummaryStatus.
 */
@Service
@Transactional
public class OrganizationSummaryStatusService {

	private final Logger log = LoggerFactory.getLogger(OrganizationSummaryStatusService.class);

	private final OrganizationSummaryStatusRepository organizationSummaryStatusRepository;

	public OrganizationSummaryStatusService(OrganizationSummaryStatusRepository organizationSummaryStatusRepository) {
		this.organizationSummaryStatusRepository = organizationSummaryStatusRepository;
	}

	/**
	 * Save or Update a OrganizationSummaryStatus.
	 *
	 * @param section
	 *            of the entity to save
	 * @return the persisted entity
	 */
	public OrganizationSummaryStatus createOrUpdate(OrganizationSection section) {
		log.debug("Request to save ExecutiveSummaryStatus : {}", section);
		OrganizationSummaryStatus executiveSummaryStatus = organizationSummaryStatusRepository
				.findOneBySection(section);
		if (executiveSummaryStatus != null) {
			executiveSummaryStatus.setLastUpdatedAt(LocalDateTime.now(ZoneId.of("UTC")));
			return organizationSummaryStatusRepository.save(executiveSummaryStatus);
		} else {
			executiveSummaryStatus = new OrganizationSummaryStatus();
			executiveSummaryStatus.setSection(section);
			return organizationSummaryStatusRepository.save(executiveSummaryStatus);
		}
	}
	/**
	 * Get all the organizationSummaryStatus.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<OrganizationSummaryStatus> findAll() {
		log.debug("Request to get all ExecutiveSummaryStatuses");
		return organizationSummaryStatusRepository.findAll();
	}
	
	/**
	 * Get one organizationSummaryStatus by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public OrganizationSummaryStatus getOrganizationSummaryStatus() {
		OrganizationSummaryStatus status = new OrganizationSummaryStatus();
		int sectionsCompleted = organizationSummaryStatusRepository.findAll().size();
		if (sectionsCompleted == 0) {
			status.setStatus(STATUS.START);
		} else if (sectionsCompleted < OrganizationSummaryStatus.TOTAL_SECTIONS) {
			status.setStatus(STATUS.RESUME);
			status.setLastUpdatedSection(
					organizationSummaryStatusRepository.findLastUpdatedSection().getSectionAsEnum());
		} else {
			status.setStatus(STATUS.REVISIT);
		}
		return status;
	}
}
