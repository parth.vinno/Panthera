package com.nollysoft.service;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.OtherServiceResponse;
import com.nollysoft.domain.OtherServiceResponse.SCOPE;
import com.nollysoft.repository.OtherServiceResponseRepository;

/**
 * Service Implementation for managing OtherServiceResponse.
 */
@Service
@Transactional
public class OtherServiceResponseService {

    private final Logger log = LoggerFactory.getLogger(OtherServiceResponseService.class);

    private final OtherServiceResponseRepository otherServiceResponseRepository;

    public OtherServiceResponseService(OtherServiceResponseRepository otherServiceResponseRepository) {
        this.otherServiceResponseRepository = otherServiceResponseRepository;
    }

    /**
     * Save a otherServiceResponse.
     *
     * @param otherServiceResponse the entity to save
     * @return the persisted entity
     */
    public OtherServiceResponse save(OtherServiceResponse otherServiceResponse) {
        log.debug("Request to save OtherServiceResponse : {}", otherServiceResponse);
        return otherServiceResponseRepository.save(otherServiceResponse);
    }

    public List<OtherServiceResponse> create(List<OtherServiceResponse> otherServiceResponse, String scope) {
		log.debug("Request to save OtherServiceResponse : {}", otherServiceResponse);
		otherServiceResponseRepository.deleteAllByScope(SCOPE.valueOf(scope));
		List<OtherServiceResponse> curselectedServiceTypes = new LinkedList<>();
		otherServiceResponse.stream().forEach(servicetype -> {
			servicetype.setScope(SCOPE.valueOf(scope));
			curselectedServiceTypes.add(otherServiceResponseRepository.save(servicetype));
		});
		return curselectedServiceTypes;
	}

    /**
     *  Get all the otherServiceResponses.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<OtherServiceResponse> findAll() {
        log.debug("Request to get all OtherServiceResponses");
        return otherServiceResponseRepository.findAll();
    }

    /**
     *  Get one otherServiceResponse by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public OtherServiceResponse findOne(Long id) {
        log.debug("Request to get OtherServiceResponse : {}", id);
        return otherServiceResponseRepository.findOne(id);
    }

    /**
     *  Delete the  otherServiceResponse by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete OtherServiceResponse : {}", id);
        otherServiceResponseRepository.delete(id);
    }
}
