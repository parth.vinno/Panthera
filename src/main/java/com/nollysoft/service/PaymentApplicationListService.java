package com.nollysoft.service;

import com.nollysoft.domain.PaymentApplicationList;
import com.nollysoft.repository.PaymentApplicationListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PaymentApplicationList.
 */
@Service
@Transactional
public class PaymentApplicationListService {

    private final Logger log = LoggerFactory.getLogger(PaymentApplicationListService.class);

    private final PaymentApplicationListRepository paymentApplicationListRepository;

    public PaymentApplicationListService(PaymentApplicationListRepository paymentApplicationListRepository) {
        this.paymentApplicationListRepository = paymentApplicationListRepository;
    }

    /**
     * Save a paymentApplicationList.
     *
     * @param paymentApplicationList the entity to save
     * @return the persisted entity
     */
    public PaymentApplicationList save(PaymentApplicationList paymentApplicationList) {
        log.debug("Request to save PaymentApplicationList : {}", paymentApplicationList);
        return paymentApplicationListRepository.save(paymentApplicationList);
    }

    /**
     *  Get all the paymentApplicationLists.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentApplicationList> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentApplicationLists");
        return paymentApplicationListRepository.findAll(pageable);
    }

    /**
     *  Get one paymentApplicationList by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentApplicationList findOne(Long id) {
        log.debug("Request to get PaymentApplicationList : {}", id);
        return paymentApplicationListRepository.findOne(id);
    }

    /**
     *  Delete the  paymentApplicationList by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentApplicationList : {}", id);
        paymentApplicationListRepository.delete(id);
    }
}
