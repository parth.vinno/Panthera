package com.nollysoft.service;

import com.nollysoft.domain.PaymentCardBusinessDescription;
import com.nollysoft.repository.PaymentCardBusinessDescriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PaymentCardBusinessDescription.
 */
@Service
@Transactional
public class PaymentCardBusinessDescriptionService {

    private final Logger log = LoggerFactory.getLogger(PaymentCardBusinessDescriptionService.class);

    private final PaymentCardBusinessDescriptionRepository paymentCardBusinessDescriptionRepository;

    public PaymentCardBusinessDescriptionService(PaymentCardBusinessDescriptionRepository paymentCardBusinessDescriptionRepository) {
        this.paymentCardBusinessDescriptionRepository = paymentCardBusinessDescriptionRepository;
    }

    /**
     * Save a paymentCardBusinessDescription.
     *
     * @param paymentCardBusinessDescription the entity to save
     * @return the persisted entity
     */
    public PaymentCardBusinessDescription save(PaymentCardBusinessDescription paymentCardBusinessDescription) {
        log.debug("Request to save PaymentCardBusinessDescription : {}", paymentCardBusinessDescription);
        return paymentCardBusinessDescriptionRepository.save(paymentCardBusinessDescription);
    }

    /**
     *  Get all the paymentCardBusinessDescriptions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentCardBusinessDescription> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentCardBusinessDescriptions");
        return paymentCardBusinessDescriptionRepository.findAll(pageable);
    }

    /**
     *  Get one paymentCardBusinessDescription by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentCardBusinessDescription findOne(Long id) {
        log.debug("Request to get PaymentCardBusinessDescription : {}", id);
        return paymentCardBusinessDescriptionRepository.findOne(id);
    }

    /**
     *  Delete the  paymentCardBusinessDescription by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentCardBusinessDescription : {}", id);
        paymentCardBusinessDescriptionRepository.delete(id);
    }
}
