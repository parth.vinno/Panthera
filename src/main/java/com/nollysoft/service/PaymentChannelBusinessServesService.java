package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.PaymentChannelBusinessServes;
import com.nollysoft.repository.PaymentChannelBusinessServesRepository;

/**
 * Service Implementation for managing PaymentChannelBusinessServes.
 */
@Service
@Transactional
public class PaymentChannelBusinessServesService {

	private final Logger log = LoggerFactory.getLogger(PaymentChannelBusinessServesService.class);

	private final PaymentChannelBusinessServesRepository paymentChannelBusinessServesRepository;

	public PaymentChannelBusinessServesService(
			PaymentChannelBusinessServesRepository paymentChannelBusinessServesRepository) {
		this.paymentChannelBusinessServesRepository = paymentChannelBusinessServesRepository;
	}

	/**
	 * Save a paymentChannelBusinessServes.
	 *
	 * @param paymentChannelBusinessServes
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PaymentChannelBusinessServes save(PaymentChannelBusinessServes paymentChannelBusinessServes) {
		log.debug("Request to save PaymentChannelBusinessServes : {}", paymentChannelBusinessServes);
		return paymentChannelBusinessServesRepository.save(paymentChannelBusinessServes);
	}

	/**
	 * Save a list of paymentChannelBusinessServes.
	 *
	 * @param list
	 *            of paymentChannelBusinessServes to save
	 * @return the persisted list of entities
	 */
	public List<PaymentChannelBusinessServes> create(List<PaymentChannelBusinessServes> paymentChannelBusinessServes) {
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = new ArrayList<>();
		paymentChannelBusinessServesRepository.deleteAll();
		paymentChannelBusinessServes.stream().forEach(paymentChannel -> {
			paymentChannelBusinessServesList.add(paymentChannelBusinessServesRepository.save(paymentChannel));
		});
		return paymentChannelBusinessServesList;
	}

	/**
	 * Get all the paymentChannelBusinessServes.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<PaymentChannelBusinessServes> findAll() {
		log.debug("Request to get all PaymentChannelBusinessServes");
		return paymentChannelBusinessServesRepository.findAll();
	}

	/**
	 * Get one paymentChannelBusinessServes by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PaymentChannelBusinessServes findOne(Long id) {
		log.debug("Request to get PaymentChannelBusinessServes : {}", id);
		return paymentChannelBusinessServesRepository.findOne(id);
	}

	/**
	 * Delete the paymentChannelBusinessServes by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PaymentChannelBusinessServes : {}", id);
		paymentChannelBusinessServesRepository.delete(id);
	}
}
