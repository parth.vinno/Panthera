package com.nollysoft.service;

import com.nollysoft.domain.PaymentChannelSaqCovers;
import com.nollysoft.repository.PaymentChannelSaqCoversRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service Implementation for managing PaymentchannelSaqCovers.
 */
@Service
@Transactional
public class PaymentChannelSaqCoversService {

    private final Logger log = LoggerFactory.getLogger(PaymentChannelSaqCoversService.class);

    private final PaymentChannelSaqCoversRepository paymentChannelSaqCoversRepository;

    public PaymentChannelSaqCoversService(PaymentChannelSaqCoversRepository paymentChannelSaqCoversRepository) {
        this.paymentChannelSaqCoversRepository = paymentChannelSaqCoversRepository;
    }

    /**
     * Save a paymentChannelSaqCovers.
     *
     * @param paymentChannelSaqCovers the entity to save
     * @return the persisted entity
     */
    public PaymentChannelSaqCovers save(PaymentChannelSaqCovers paymentChannelSaqCovers) {
        log.debug("Request to save PaymentchannelSaqCovers : {}", paymentChannelSaqCovers);
        return paymentChannelSaqCoversRepository.save(paymentChannelSaqCovers);
    }
    
    /**
     * Save a list of paymentChannelSaqCovers.
     *
     * @param list of paymentChannelSaqCovers to save
     * @return the persisted list of entities
     */
	public List<PaymentChannelSaqCovers> create(List<PaymentChannelSaqCovers> paymentchannelsSaqCovers) {
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = new ArrayList<>();
		paymentChannelSaqCoversRepository.deleteAll();
		paymentchannelsSaqCovers.stream().forEach(paymentChannel -> {
			paymentChannelSaqCoversList.add(paymentChannelSaqCoversRepository.save(paymentChannel));
		});
		return paymentChannelSaqCoversList;
	}

    /**
     *  Get all the paymentChannelSaqCovers.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PaymentChannelSaqCovers> findAll() {
        log.debug("Request to get all PaymentchannelSaqCovers");
        return paymentChannelSaqCoversRepository.findAll();
    }

    /**
     *  Get one paymentChannelSaqCovers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentChannelSaqCovers findOne(Long id) {
        log.debug("Request to get PaymentchannelSaqCovers : {}", id);
        return paymentChannelSaqCoversRepository.findOne(id);
    }

    /**
     *  Delete the  paymentChannelSaqCovers by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentchannelSaqCovers : {}", id);
        paymentChannelSaqCoversRepository.delete(id);
    }
}
