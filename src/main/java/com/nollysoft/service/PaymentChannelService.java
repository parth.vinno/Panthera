package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.PaymentChannel;
import com.nollysoft.repository.PaymentChannelRepository;

/**
 * Service Implementation for managing PaymentChannel.
 */
@Service
@Transactional
public class PaymentChannelService {

    private final Logger log = LoggerFactory.getLogger(PaymentChannelService.class);

    private final PaymentChannelRepository paymentChannelRepository;

    public PaymentChannelService(PaymentChannelRepository paymentChannelRepository) {
        this.paymentChannelRepository = paymentChannelRepository;
    }

    /**
     * Save a paymentChannel.
     *
     * @param paymentChannel the entity to save
     * @return the persisted entity
     */
    public PaymentChannel save(PaymentChannel paymentChannel) {
        log.debug("Request to save PaymentChannel : {}", paymentChannel);
        return paymentChannelRepository.save(paymentChannel);
    }

    /**
     *  Get all the paymentChannels.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PaymentChannel> findAll() {
        log.debug("Request to get all PaymentChannels");
        return paymentChannelRepository.findAll();
    }

    /**
   	 * Get all the paymentChannels.
   	 *
   	 * @param pageable
   	 *            the pagination information
   	 * @return the list of entities
   	 */
   	@Transactional(readOnly = true)
   	public Page<PaymentChannel> findAll(Pageable pageable) {
   		log.debug("Request to get all Payment Channel");
   		return paymentChannelRepository.findAll(pageable);
   	}

    /**
     *  Get one paymentChannel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentChannel findOne(Long id) {
        log.debug("Request to get PaymentChannel : {}", id);
        return paymentChannelRepository.findOne(id);
    }

    /**
     *  Delete the  paymentChannel by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentChannel : {}", id);
        paymentChannelRepository.delete(id);
    }
}
