package com.nollysoft.service;

import com.nollysoft.domain.PaymentProcessing;
import com.nollysoft.repository.PaymentProcessingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PaymentProcessing.
 */
@Service
@Transactional
public class PaymentProcessingService {

    private final Logger log = LoggerFactory.getLogger(PaymentProcessingService.class);

    private final PaymentProcessingRepository paymentProcessingRepository;

    public PaymentProcessingService(PaymentProcessingRepository paymentProcessingRepository) {
        this.paymentProcessingRepository = paymentProcessingRepository;
    }

    /**
     * Save a paymentProcessing.
     *
     * @param paymentProcessing the entity to save
     * @return the persisted entity
     */
    public PaymentProcessing save(PaymentProcessing paymentProcessing) {
        log.debug("Request to save PaymentProcessing : {}", paymentProcessing);
        return paymentProcessingRepository.save(paymentProcessing);
    }

    /**
     *  Get all the paymentProcessings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentProcessing> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentProcessings");
        return paymentProcessingRepository.findAll(pageable);
    }

    /**
     *  Get one paymentProcessing by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentProcessing findOne(Long id) {
        log.debug("Request to get PaymentProcessing : {}", id);
        return paymentProcessingRepository.findOne(id);
    }

    /**
     *  Delete the  paymentProcessing by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentProcessing : {}", id);
        paymentProcessingRepository.delete(id);
    }
}
