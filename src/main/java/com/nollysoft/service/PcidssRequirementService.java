package com.nollysoft.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.PcidssRequirementRepository;
import com.nollysoft.repository.RequirementCategoryRepository;
import com.nollysoft.repository.SaqQuestionRepository;
import com.nollysoft.service.dto.PcidssRequirementAdminDTO;
import com.nollysoft.service.dto.PcidssRequirementPageDTO;
import com.nollysoft.service.dto.PcidssRequirementResultSetDTO;
import com.nollysoft.service.dto.PcidssRequirementUserDTO;
import com.nollysoft.service.dto.PcidssRequirementUserDTO.REQUIREMENT_STATUS;

/**
 * Service Implementation for managing PcidssRequirement.
 */
@Service
@Transactional
public class PcidssRequirementService {

	private final Logger log = LoggerFactory.getLogger(PcidssRequirementService.class);

	private final PcidssRequirementRepository pcidssRequirementRepository;

	private final RequirementCategoryRepository requirementCategoryRepository;

	private final SaqQuestionRepository saqQuestionRepository;

	public PcidssRequirementService(PcidssRequirementRepository pcidssRequirementRepository,
			RequirementCategoryRepository requirementCategoryRepository, SaqQuestionRepository saqQuestionRepository) {
		this.pcidssRequirementRepository = pcidssRequirementRepository;
		this.requirementCategoryRepository = requirementCategoryRepository;
		this.saqQuestionRepository = saqQuestionRepository;
	}

	/**
	 * Save a pcidssRequirement.
	 *
	 * @param pcidssRequirement
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Transactional
	public PcidssRequirement save(PcidssRequirement pcidssRequirement) {
		log.debug("Request to save PcidssRequirement : {}", pcidssRequirement);

		return pcidssRequirementRepository.save(pcidssRequirement);
	}

	/**
	 * Save list of pcidssRequirement.
	 *
	 * @param list
	 *            of pcidssRequirement to save
	 * @return the persisted entities
	 */
	public List<PcidssRequirementAdminDTO> saveRequirements(List<PcidssRequirement> pcidssRequirements,
			Long categoryId) {
		List<PcidssRequirementAdminDTO> pcidssRequirementDTOs = new ArrayList<>();
		pcidssRequirements.stream().forEach(requirement -> {
			requirement.setRequirementCategoryId(requirementCategoryRepository.findOne(categoryId));
			pcidssRequirementDTOs.add(new PcidssRequirementAdminDTO(pcidssRequirementRepository.save(requirement)));
		});
		return pcidssRequirementDTOs;
	}

	/**
	 * Get all the pcidssRequirements.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public PcidssRequirementPageDTO findAll(Pageable pageable) {
		log.debug("Request to get all PcidssRequirements");
		Page<PcidssRequirement> result = pcidssRequirementRepository.findAll(pageable);
		List<PcidssRequirementAdminDTO> requirements = new ArrayList<>();
		result.getContent().stream().forEach(content -> {
			requirements.add(new PcidssRequirementAdminDTO(content));
		});
		PcidssRequirementPageDTO results = new PcidssRequirementPageDTO(requirements, pageable,
				result.getTotalElements());
		return results;
	}

	/**
	 * Get all the pcidssRequirements with corrected SaqResponses.
	 * 
	 * @return the list of entities
	 */
	public List<PcidssRequirementUserDTO> findAllWithCorrectedSaqResponses(Long categoryId) {
		log.debug("Request to get all PcidssRequirements with corrected responses for a category");
		List<PcidssRequirementUserDTO> pcidssRequirements = new ArrayList<>();
		Set<BigInteger> requirementIds = pcidssRequirementRepository
				.getPcidssRequirementIdsForCategoryWithCorrectedResponse(categoryId);
		if (requirementIds != null) {
			requirementIds.stream().forEach(id -> {
				pcidssRequirements.add(findOneWithCorrectedSaqQuestionIds(id.longValue()));
			});
		}
		return pcidssRequirements;
	}

	/**
	 * Get all the pcidssRequirements with status and availability.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<PcidssRequirementUserDTO> findAllWithStatusAndAvailability() {
		log.debug("Request to get all PcidssRequirements with status and availability");
		List<PcidssRequirementResultSetDTO> pcidssRequirementResultSetDTOs = pcidssRequirementRepository
				.getPcidssRequirementIdsBySaqQuestionResponseRequired();
		List<PcidssRequirement> list = pcidssRequirementRepository.findAll();
		List<PcidssRequirementUserDTO> dtoList = new ArrayList<>();
		list.stream().forEach(requirement -> {
			dtoList.add(new PcidssRequirementUserDTO(requirement));
		});
		setStatus(pcidssRequirementResultSetDTOs, dtoList);
		setAvailability(dtoList);
		List<PcidssRequirementUserDTO> pcidssRequirements = setQuestionIds(dtoList);
		return pcidssRequirements;
	}

	private List<PcidssRequirementUserDTO> setStatus(List<PcidssRequirementResultSetDTO> pcidssRequirementResultSetDTOs,
			List<PcidssRequirementUserDTO> pcidssRequirements) {
		List<PcidssRequirementUserDTO> pcidssRequirementOutputDTOs = pcidssRequirements;
		pcidssRequirementResultSetDTOs.stream().forEach(dto -> {
			pcidssRequirementOutputDTOs.stream().forEach(requirement -> {
				if (requirement.getId() == dto.getId()) {
					if (dto.getQuestionCount() == dto.getResponseCount()) {
						requirement.setRequirementStatus(REQUIREMENT_STATUS.REVISIT);
					} else if (dto.getResponseCount() > 0) {
						requirement.setRequirementStatus(REQUIREMENT_STATUS.RESUME);
					}
				}
			});
		});
		return pcidssRequirementOutputDTOs;
	}

	private List<PcidssRequirementUserDTO> setAvailability(List<PcidssRequirementUserDTO> list) {
		List<PcidssRequirementUserDTO> pcidssRequirements = new ArrayList<>();
		PcidssRequirementUserDTO pcidssRequirement = new PcidssRequirementUserDTO();
		Iterator<PcidssRequirementUserDTO> itr = list.iterator();
		do {
			pcidssRequirement = itr.next();
			pcidssRequirement.setAvailable(true);
			pcidssRequirements.add(pcidssRequirement);
		} while (itr.hasNext() && pcidssRequirement.getRequirementStatus() == REQUIREMENT_STATUS.REVISIT);
		return list;
	}

	/**
	 * Get one pcidssRequirement by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PcidssRequirement findOne(Long id) {
		log.debug("Request to get PcidssRequirement : {}", id);
		return pcidssRequirementRepository.findOne(id);
	}

	/**
	 * Get one pcidssRequirement by id with SaqQuestionIds.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PcidssRequirementUserDTO findOneWithSaqQuestionIds(Long id) {
		log.debug("Request to get PcidssRequirement : {}", id);
		Set<SaqQuestion> saqQuestions = saqQuestionRepository.getByPcidssRequirementIdAndParentIdIsNull(id);
		Map<String, Long> map = new TreeMap<String, Long>(getComparator());

		saqQuestions.stream().forEach(saqQuestion -> {
			if (saqQuestion.isResponseRequired()) {
				map.put(saqQuestion.getQuestionNumber(), saqQuestion.getId());
			}
			getQuestionIds(saqQuestion, map);
		});
		PcidssRequirementUserDTO pcidssRequirement = new PcidssRequirementUserDTO(
				pcidssRequirementRepository.findOne(id));
		Long[] arr = new Long[map.size()];
		pcidssRequirement.setQuestionIds(map.values().toArray(arr));
		return pcidssRequirement;
	}

	/**
	 * Get one pcidssRequirement by id with corrected SaqQuestionIds.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PcidssRequirementUserDTO findOneWithCorrectedSaqQuestionIds(Long id) {
		log.debug("Request to get PcidssRequirement : {}", id);
		Set<BigInteger> saqQuestionIds = saqQuestionRepository.getQuestionIdsForRequirementWithCorrectedResponse(id);
		Map<String, Long> map = new TreeMap<String, Long>(getComparator());
		if (saqQuestionIds != null) {
			saqQuestionIds.stream().forEach(questionid -> {
				SaqQuestion saqQuestion = saqQuestionRepository.findOne(questionid.longValue());
				map.put(saqQuestion.getQuestionNumber(), saqQuestion.getId());
			});
		}
		PcidssRequirementUserDTO pcidssRequirement = new PcidssRequirementUserDTO(
				pcidssRequirementRepository.findOne(id));
		Long[] arr = new Long[map.size()];
		pcidssRequirement.setQuestionIds(map.values().toArray(arr));
		return pcidssRequirement;
	}

	private Map<String, Long> getQuestionIds(SaqQuestion saqQuestion, Map<String, Long> map) {
		saqQuestion.getSubQuestions().stream().forEach(subQuestion -> {
			if (subQuestion.isResponseRequired()) {
				map.put(subQuestion.getQuestionNumber(), subQuestion.getId());
			}
			getQuestionIds(subQuestion, map);
		});
		return map;
	}

	public static Comparator<String> getComparator() {
		Comparator<String> c = new Comparator<String>() {
			@Override
			public int compare(String object1, String object2) {
				String[] arr1 = object1.split("\\.");
				String[] arr2 = object2.split("\\.");
				int size1 = arr1.length;
				int size2 = arr2.length;
				for (int i = 0; i < (size1 < size2 ? size1 : size2); i++) {
					Integer n1, n2;
					try {
						n1 = Integer.parseInt(arr1[i]);
						try {
							n2 = Integer.parseInt(arr2[i]);
							int comparison = n1.compareTo(n2);
							if (comparison != 0) {
								return comparison;
							}
						} catch (Exception e) {
							return 1;
						}
					} catch (Exception e) {
						try {
							n2 = Integer.parseInt(arr2[i]);
							return -1;
						} catch (Exception e1) {
							int comparison = arr1[i].compareTo(arr2[i]);
							if (comparison != 0) {
								return arr1[i].compareTo(arr2[i]);
							}
						}
					}
				}
				return size1 < size2 ? -1 : 1;
			}
		};
		return c;
	}

	/**
	 * Delete the pcidssRequirement by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PcidssRequirement : {}", id);
		pcidssRequirementRepository.delete(id);
	}

	/**
	 * Get pcidssRequirements by categoryId.
	 *
	 * @param categoryId
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public PcidssRequirementPageDTO findPcidssRequirementByRequirmentCategory(Long requirementCategoryId,
			Pageable pageable) {
		log.debug("Request to get PcidssRequirement : {}", requirementCategoryId);
		Page<PcidssRequirement> result = pcidssRequirementRepository
				.getPcidssRequirementByRequirementCategory(requirementCategoryId, pageable);
		List<PcidssRequirementAdminDTO> requirements = new ArrayList<>();
		result.getContent().stream().forEach(content -> {
			requirements.add(new PcidssRequirementAdminDTO(content));
		});
		PcidssRequirementPageDTO results = new PcidssRequirementPageDTO(requirements, pageable,
				result.getTotalElements());
		return results;
	}

	/**
	 * Get pcidssRequirements with status by categoryId.
	 *
	 * @param categoryId
	 * @return the list of entities
	 */
	public List<PcidssRequirementUserDTO> findPcidssRequirementWithStatusByRequirmentCategory(
			Long requirementCategoryId) {
		List<PcidssRequirementResultSetDTO> pcidssRequirementResultSetDTOs = pcidssRequirementRepository
				.getPcidssRequirementIdsBySaqQuestionResponseRequired();
		pcidssRequirementResultSetDTOs = getForCategoryId(requirementCategoryId, pcidssRequirementResultSetDTOs);
		List<PcidssRequirement> list = pcidssRequirementRepository
				.getPcidssRequirementIdsForCategory(requirementCategoryId);
		List<PcidssRequirementUserDTO> pcidssRequirementUserDTOs = new ArrayList<>();
		list.stream().forEach(requirement -> {
			pcidssRequirementUserDTOs.add(new PcidssRequirementUserDTO(requirement));
		});
		setStatus(pcidssRequirementResultSetDTOs, pcidssRequirementUserDTOs);
		setAvailability(pcidssRequirementUserDTOs);
		List<PcidssRequirementUserDTO> pcidssRequirements = setQuestionIds(pcidssRequirementUserDTOs);
		return pcidssRequirements;
	}

	private List<PcidssRequirementUserDTO> setQuestionIds(List<PcidssRequirementUserDTO> list) {
		List<PcidssRequirementUserDTO> listWithQuestionIds = new ArrayList<>();
		list.stream().forEach(requirement -> {
			PcidssRequirementUserDTO pcidssRequirementUserDTO = requirement;
			requirement = findOneWithSaqQuestionIds(requirement.getId());
			requirement.setAvailable(pcidssRequirementUserDTO.isAvailable());
			requirement.setRequirementStatus(pcidssRequirementUserDTO.getRequirementStatus());
			if (requirement.getRequirementStatus() == REQUIREMENT_STATUS.RESUME) {
				requirement.setLastRespondedQuestionId(
						saqQuestionRepository.getLastRespondedQuestionId(requirement.getId()));
			}
			listWithQuestionIds.add(requirement);
		});
		return listWithQuestionIds;
	}

	private List<PcidssRequirementResultSetDTO> getForCategoryId(Long requirementCategoryId,
			List<PcidssRequirementResultSetDTO> pcidssRequirementResultSetDTOs) {
		List<PcidssRequirementResultSetDTO> curResultSet = new ArrayList<PcidssRequirementResultSetDTO>();
		pcidssRequirementResultSetDTOs.stream().forEach(dto -> {
			PcidssRequirement pcidssRequirement = pcidssRequirementRepository.findOne(dto.getId());
			if (pcidssRequirement.getRequirementCategoryId().getId() == requirementCategoryId) {
				curResultSet.add(dto);
			}
		});
		return curResultSet;
	}

	/**
	 * Get one pcidssRequirement by requirementNumber.
	 *
	 * @param requirementNumber
	 *            the requirementNumber of the entity
	 * @return the entity
	 */
public PcidssRequirement findOneByRequirementNumber(String requirementNumber) {
		log.debug("Request to get Requirement : {}", requirementNumber);
		return pcidssRequirementRepository.findOneByRequirementNumber(requirementNumber);
	}
}