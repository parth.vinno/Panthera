package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;
import com.nollysoft.domain.User;
import com.nollysoft.repository.QANotificationRepository;
import com.nollysoft.repository.SaqReviewRepository;
import com.nollysoft.service.dto.QANotificationDTO;
import com.nollysoft.service.dto.SaqQuestionUserDTO;

/**
 * Service Implementation for managing QANotification.
 */
@Service
@Transactional
public class QANotificationService {

	private final Logger log = LoggerFactory.getLogger(QANotificationService.class);

	private final QANotificationRepository qaNotificationRepository;

	private final SaqReviewRepository saqReviewRepository;

	public QANotificationService(QANotificationRepository qaNotificationRepository,
			SaqReviewRepository saqReviewRepository) {
		this.qaNotificationRepository = qaNotificationRepository;
		this.saqReviewRepository = saqReviewRepository;
	}

	/**
	 * Save a qaNotification.
	 *
	 * @param qaNotification
	 *            the entity to save
	 * @return the persisted entity
	 */
	public QANotification save(QANotification qaNotification) {
		log.debug("Request to save QANotification : {}", qaNotification);
		return qaNotificationRepository.save(qaNotification);
	}

	/**
	 * Get all the qaNotifications.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<QANotification> findAll() {
		log.debug("Request to get all QANotifications");
		return qaNotificationRepository.findAll();
	}

	/**
	 * Get one qaNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public QANotification findOne(Long id) {
		log.debug("Request to get QANotification : {}", id);
		return qaNotificationRepository.findOne(id);
	}

	/**
	 * Delete the qaNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete QANotification : {}", id);
		qaNotificationRepository.delete(id);
	}

	/**
	 * Save a qaNotification.
	 *
	 * @param notification
	 * @param user
	 * @return the persisted entity
	 */
	public QANotification saveWithNotification(User qaUser, User qsaUser) {
		QANotification qaNotification = qaNotificationRepository.findByQaId(qaUser);
		if (qaNotification != null) {
			qaNotification.setStatus(NOTIFICATION_STATUS.REVIEW_REQUIRED);
		} else {
			qaNotification = new QANotification();
		}
		qaNotification.setQaId(qaUser);
		qaNotification.setQsaId(qsaUser);
		return qaNotificationRepository.save(qaNotification);
	}

	/**
	 * Update a qaNotification.
	 *
	 * @param NOTIFICATION_STATUS
	 *            to update
	 * @return the updated entity
	 */
	public QANotification updateStatus(NOTIFICATION_STATUS status) {
		QANotification qaNotification = qaNotificationRepository.findAll().get(0);
		qaNotification.setStatus(status);
		return qaNotificationRepository.save(qaNotification);
	}

	/**
	 * Get the qaNotification with status.
	 *
	 * @return qaNotification with status
	 */
	public QANotificationDTO findWithstatus() {
		if (qaNotificationRepository.findAll().size() > 0) {

			QANotification qaNotification = qaNotificationRepository.findAll().get(0);
			long reviewCount = saqReviewRepository.count();
			if (qaNotification.getStatusType() == NOTIFICATION_STATUS.NEW && (reviewCount > 0)) {
				qaNotification.setStatus(NOTIFICATION_STATUS.PENDING);
			}
			QANotificationDTO qaNotificationDTO = new QANotificationDTO(qaNotification);
			if (qaNotification.getStatusType() == NOTIFICATION_STATUS.PENDING) {
				qaNotificationDTO.setLastReviewedQuestion(
						new SaqQuestionUserDTO(saqReviewRepository.getLastUpdated().getSaqQuestionId()));
			}
			return qaNotificationDTO;
		} else
			return null;
	}
}
