package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.QSAAttestation;
import com.nollysoft.repository.QSAAttestationRepository;

/**
 * Service Implementation for managing ISAAttestation.
 */
@Service
@Transactional
public class QSAAttestationService {

	private final Logger log = LoggerFactory.getLogger(QSAAttestationService.class);

	private final QSAAttestationRepository qsaAttestationRepository;

	public QSAAttestationService(QSAAttestationRepository qsaAttestationRepository) {
		this.qsaAttestationRepository = qsaAttestationRepository;
	}

	/**
	 * Save a QSAAttestation.
	 *
	 * @param QSAAttestation
	 *            the entity to save
	 * @return the persisted entity
	 */
	public QSAAttestation save(QSAAttestation qsaAttestation) {
		log.debug("Request to save QSAAttestation : {}", qsaAttestation);
		return qsaAttestationRepository.save(qsaAttestation);
	}

	/**
	 * Get QSAAttestation.
	 * 
	 * @return the list of the entity
	 */
	@Transactional(readOnly = true)
	public List<QSAAttestation> get() {
		log.debug("Request to get QSAAttestation : {}");
		return qsaAttestationRepository.findAll();
	}

	/**
	 * Delete the QSAAttestation by id.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete QSAAttestation : {}", id);
		qsaAttestationRepository.delete(id);
	}
}
