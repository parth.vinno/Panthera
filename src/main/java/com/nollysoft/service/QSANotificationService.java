package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.QSANotification;
import com.nollysoft.domain.User;
import com.nollysoft.repository.QSANotificationRepository;

/**
 * Service Implementation for managing QSANotification.
 */
@Service
@Transactional
public class QSANotificationService {

	private final Logger log = LoggerFactory.getLogger(QSANotificationService.class);

	private final QSANotificationRepository qsaNotificationRepository;

	public QSANotificationService(QSANotificationRepository qsaNotificationRepository) {
		this.qsaNotificationRepository = qsaNotificationRepository;
	}

	/**
	 * Save a qsaNotification.
	 *
	 * @param qsaNotification
	 *            the entity to save
	 * @return the persisted entity
	 */
	public QSANotification save(QSANotification qsaNotification) {
		log.debug("Request to save QSANotification : {}", qsaNotification);
		return qsaNotificationRepository.save(qsaNotification);
	}

	/**
	 * Get all the qsaNotifications.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<QSANotification> findAll() {
		log.debug("Request to get all QSANotifications");
		return qsaNotificationRepository.findAll();
	}

	/**
	 * Get one qsaNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public QSANotification findOne(Long id) {
		log.debug("Request to get QSANotification : {}", id);
		return qsaNotificationRepository.findOne(id);
	}

	/**
	 * Delete the qsaNotification by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete QSANotification : {}", id);
		qsaNotificationRepository.delete(id);
	}

	/**
	 * Save a qsaNotification.
	 *
	 * @param qaUser
	 * @param qsaUser
	 * @return the persisted entity
	 */
	public QSANotification create(User qaUser, User qsaUser) {
		QSANotification qsaNotification = new QSANotification();
		qsaNotification.setQaUserId(qaUser);
		qsaNotification.setQsaUserId(qsaUser);
		log.debug("Request to save QSANotification : {}", qsaNotification);
		return qsaNotificationRepository.save(qsaNotification);
	}
}
