package com.nollysoft.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.RequirementCategory;
import com.nollysoft.repository.RequirementCategoryRepository;
import com.nollysoft.service.dto.RequirementCategoryAdminDTO;
import com.nollysoft.service.dto.RequirementCategoryPageDTO;
import com.nollysoft.service.dto.RequirementCategoryUserDTO;

/**
 * Service Implementation for managing RequirementCategory.
 */
@Service
@Transactional
public class RequirementCategoryService {

	private final Logger log = LoggerFactory.getLogger(RequirementCategoryService.class);

	private final RequirementCategoryRepository requirementCategoryRepository;

	public RequirementCategoryService(RequirementCategoryRepository requirementCategoryRepository) {
		this.requirementCategoryRepository = requirementCategoryRepository;
	}

	/**
	 * Save a requirementCategory.
	 *
	 * @param requirementCategory
	 *            the entity to save
	 * @param dbserviceprovider
	 * @return the persisted entity
	 */
	public RequirementCategory save(RequirementCategory requirementCategory) {
		log.debug("Request to save RequirementCategory : {}", requirementCategory);
		return requirementCategoryRepository.save(requirementCategory);
	}

	/**
	 * Get all the requirementCategories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public RequirementCategoryPageDTO findAll(Pageable pageable) {
		log.debug("Request to get all RequirementCategories");
		Page<RequirementCategory> result = requirementCategoryRepository.findAll(pageable);
		List<RequirementCategoryAdminDTO> categories = new ArrayList<>();
		result.getContent().stream().forEach(content -> {
			categories.add(new RequirementCategoryAdminDTO(content));
		});
		RequirementCategoryPageDTO results = new RequirementCategoryPageDTO(categories, pageable,
				result.getTotalElements());
		return results;
	}

	/**
	 * Get requirementCategories with available as true
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<RequirementCategoryUserDTO> findAllWithAvailability() {

		log.debug("Request to get all RequirementCategories with status available as true");
		List<RequirementCategoryUserDTO> requirementCategories = getCategoryDTOList(
				requirementCategoryRepository.findAll());
		Iterator<Long> allCategoryIds = requirementCategoryRepository.getAllIds().iterator();
		Iterator<Long> completedCategoryIds = requirementCategoryRepository
				.getRequirementCategoryIdsBySaqQuestionResponseRequired().iterator();
		while (completedCategoryIds.hasNext()) {
			Long id = allCategoryIds.next();
			requirementCategories.stream().forEach(category -> {
				if (category.getId() == id) {
					category.setAvailable(true);
					return;
				}
			});
			completedCategoryIds.next();
		}
		if (allCategoryIds.hasNext()) {

			Long nextId = allCategoryIds.next();
			requirementCategories.stream().forEach(category -> {
				if (category.getId() == nextId) {
					category.setAvailable(true);
					return;
				}
			});
		}
		return requirementCategories;
	}

	private List<RequirementCategoryUserDTO> getCategoryDTOList(List<RequirementCategory> allCategories) {
		List<RequirementCategoryUserDTO> requirementCategories = new ArrayList<>();
		allCategories.stream().forEach(category -> {
			requirementCategories.add(new RequirementCategoryUserDTO(category));
		});
		return requirementCategories;
	}

	/**
	 * Get requirementCategories with corrected saqResponse
	 *
	 * @return the list of entities
	 */
	public List<RequirementCategoryUserDTO> findAllWithCorrectedSaqResponse() {
		List<RequirementCategoryUserDTO> categories = new ArrayList<>();
		List<BigInteger> categoryIds = requirementCategoryRepository.getRequirementCategoryIdsWithCorrectedResponse();
		if (categoryIds != null) {
			categoryIds.stream().forEach(id -> {
				categories.add(new RequirementCategoryUserDTO(requirementCategoryRepository.findOne(id.longValue())));
			});
		}
		return categories;
	}

	/**
	 * Get one requirementCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public RequirementCategory findOne(Long id) {
		log.debug("Request to get RequirementCategory : {}", id);
		return requirementCategoryRepository.findOne(id);
	}

	/**
	 * Delete the requirementCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete RequirementCategory : {}", id);
		requirementCategoryRepository.delete(id);
	}
	
	/**
	 * Find the requirementCategory by Category Name.
	 *
	 * @param categoryName
	 *            the categoryName of the entity
	 */
	public RequirementCategory findOneByCategoryName(String categoryName) {
		log.debug("Request to get RequirementCategory : {}", categoryName);
		return requirementCategoryRepository.getOneByCategoryName(categoryName);
	}
	
}