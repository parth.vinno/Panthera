package com.nollysoft.service;

import com.nollysoft.domain.RequirementNotTestedExplanation;
import com.nollysoft.repository.RequirementNotTestedExplanationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing RequirementNotTestedExplanation.
 */
@Service
@Transactional
public class RequirementNotTestedExplanationService {

    private final Logger log = LoggerFactory.getLogger(RequirementNotTestedExplanationService.class);

    private final RequirementNotTestedExplanationRepository requirementNotTestedExplanationRepository;

    public RequirementNotTestedExplanationService(RequirementNotTestedExplanationRepository requirementNotTestedExplanationRepository) {
        this.requirementNotTestedExplanationRepository = requirementNotTestedExplanationRepository;
    }

    /**
     * Save a requirementNotTestedExplanation.
     *
     * @param requirementNotTestedExplanation the entity to save
     * @return the persisted entity
     */
    public RequirementNotTestedExplanation save(RequirementNotTestedExplanation requirementNotTestedExplanation) {
        log.debug("Request to save RequirementNotTestedExplanation : {}", requirementNotTestedExplanation);
        return requirementNotTestedExplanationRepository.save(requirementNotTestedExplanation);
    }

    /**
     * Get all the requirementNotTestedExplanations.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<RequirementNotTestedExplanation> findAll() {
        log.debug("Request to get all RequirementNotTestedExplanations");
        return requirementNotTestedExplanationRepository.findAll();
    }

    /**
     * Get one requirementNotTestedExplanation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequirementNotTestedExplanation findOne(Long id) {
        log.debug("Request to get RequirementNotTestedExplanation : {}", id);
        return requirementNotTestedExplanationRepository.findOne(id);
    }

    /**
     * Delete the requirementNotTestedExplanation by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RequirementNotTestedExplanation : {}", id);
        requirementNotTestedExplanationRepository.delete(id);
    }
}
