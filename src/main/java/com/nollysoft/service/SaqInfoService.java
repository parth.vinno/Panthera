package com.nollysoft.service;

import com.nollysoft.domain.SaqInfo;
import com.nollysoft.repository.SaqInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing SaqInfo.
 */
@Service
@Transactional
public class SaqInfoService {

    private final Logger log = LoggerFactory.getLogger(SaqInfoService.class);

    private final SaqInfoRepository saqInfoRepository;

    public SaqInfoService(SaqInfoRepository saqInfoRepository) {
        this.saqInfoRepository = saqInfoRepository;
    }

    /**
     * Save a saqInfo.
     *
     * @param saqInfo the entity to save
     * @return the persisted entity
     */
    public SaqInfo save(SaqInfo saqInfo) {
        log.debug("Request to save SaqInfo : {}", saqInfo);
        return saqInfoRepository.save(saqInfo);
    }

    /**
     * Get all the saqInfos.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SaqInfo> findAll() {
        log.debug("Request to get all SaqInfos");
        return saqInfoRepository.findAll();
    }

    /**
     * Get one saqInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SaqInfo findOne(Long id) {
        log.debug("Request to get SaqInfo : {}", id);
        return saqInfoRepository.findOne(id);
    }

    /**
     * Delete the saqInfo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SaqInfo : {}", id);
        saqInfoRepository.delete(id);
    }
}
