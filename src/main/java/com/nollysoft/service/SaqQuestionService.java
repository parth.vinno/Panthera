package com.nollysoft.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ExpectedTest;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.ExpectedTestRepository;
import com.nollysoft.repository.SaqQuestionRepository;
import com.nollysoft.service.dto.SaqQuestionAdminDTO;
import com.nollysoft.service.dto.SaqQuestionPageDTO;

/**
 * Service Implementation for managing SaqQuestion.
 */
@Service
@Transactional
public class SaqQuestionService {

	private final Logger log = LoggerFactory.getLogger(SaqQuestionService.class);

	private final SaqQuestionRepository saqQuestionRepository;
	private final ExpectedTestRepository expectedTestRepository;

	public SaqQuestionService(SaqQuestionRepository saqQuestionRepository,
			ExpectedTestRepository expectedTestRepository) {
		this.saqQuestionRepository = saqQuestionRepository;
		this.expectedTestRepository = expectedTestRepository;
	}

	/**
	 * Save a saqQuestion.
	 *
	 * @param saqQuestion
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SaqQuestion save(SaqQuestion saqQuestion) {
		setQuestionMapping(saqQuestion);
		return saqQuestionRepository.save(saqQuestion);
	}

	private void setQuestionMapping(SaqQuestion saqQuestion) {
		mapExpectedTests(saqQuestion);
		for (SaqQuestion q : saqQuestion.getSubQuestions()) {
			q.setParentId(saqQuestion);
			setQuestionMapping(q);
		}
	}

	private void mapExpectedTests(SaqQuestion saqQuestion) {
		Set<ExpectedTest> expectedTests = saqQuestion.getExpectedTests();
		Set<Long> ids = expectedTests.parallelStream().map(e -> {
			if (e.getId() == null) {
				return expectedTestRepository.save(e).getId();
			}
			return e.getId();
		}).collect(Collectors.toSet());

		saqQuestion.setExpectedTests(new HashSet<>(expectedTestRepository.findAll(ids)));
	}

	/**
	 * Get all the saqQuestions.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public SaqQuestionPageDTO findAll(Pageable pageable) {
		log.debug("Request to get all SaqQuestions");
		Page<SaqQuestion> result = saqQuestionRepository.findByParentIdIsNull(pageable);
		List<SaqQuestionAdminDTO> questions = new ArrayList<>();
		result.getContent().stream().forEach((question) -> {
			questions.add(new SaqQuestionAdminDTO(question));
		});

		return new SaqQuestionPageDTO(questions, pageable, result.getTotalElements());
	}

	/**
	 * Get one saqQuestion by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public SaqQuestion findOne(Long id) {
		log.debug("Request to get SaqQuestion : {}", id);
		return saqQuestionRepository.findOne(id);
	}

	/**
	 * Delete the saqQuestion by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete SaqQuestion : {}", id);
		saqQuestionRepository.delete(id);
	}

	/**
	 * Get all the saqQuestions by pcidssrequirement.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public SaqQuestionPageDTO findSaqQuestionByPcidssRequirement(Long pcidssRequirementId, Pageable pageable) {
		log.debug("Request to get all SaqQuestions", pcidssRequirementId);
		Page<SaqQuestion> result = saqQuestionRepository.findSaqQuestionByPcidssRequirement(pcidssRequirementId,
				pageable);
		List<SaqQuestionAdminDTO> questions = new ArrayList<>();
		result.getContent().stream().forEach((question) -> {
			questions.add(new SaqQuestionAdminDTO(question));
		});
		return new SaqQuestionPageDTO(questions, pageable, result.getTotalElements());
	}

	/**
	 * Get one saqQuestion by questionNumber.
	 *
	 * @param questionNumber
	 *            the questionNumber of the entity
	 * @return the entity
	 */
	public SaqQuestion findOneByQuestionNumber(String questionNumber) {
		log.debug("Request to get SaqQuestion : {}", questionNumber);
		return saqQuestionRepository.findByQuestionNumber(questionNumber);
	}
}
