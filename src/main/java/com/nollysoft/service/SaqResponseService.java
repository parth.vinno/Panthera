package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.CompensatingControlWorksheetResponse;
import com.nollysoft.domain.NonApplicabilityExplanation;
import com.nollysoft.domain.RequirementNotTestedExplanation;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.domain.SaqResponse;
import com.nollysoft.domain.SaqResponse.QUESTION_RESPONSE;
import com.nollysoft.repository.CompensatingControlWorksheetRepository;
import com.nollysoft.repository.CompensatingControlWorksheetResponseRepository;
import com.nollysoft.repository.NonApplicabilityExplanationRepository;
import com.nollysoft.repository.RequirementNotTestedExplanationRepository;
import com.nollysoft.repository.SaqQuestionRepository;
import com.nollysoft.repository.SaqResponseRepository;
import com.nollysoft.service.dto.CompensatingControlWorkSheetResponseDTO;
import com.nollysoft.service.dto.NonApplicabilityExplanationDTO;
import com.nollysoft.service.dto.RequirementNotTestedExplanationDTO;
import com.nollysoft.service.dto.SaqQuestionUserDTO;
import com.nollysoft.service.dto.SaqResponseDTO;

/**
 * Service Implementation for managing SaqResponse.
 */
@Service
@Transactional
public class SaqResponseService {

	private final Logger log = LoggerFactory.getLogger(SaqResponseService.class);

	private final SaqResponseRepository saqResponseRepository;

	private final SaqQuestionRepository saqQuestionRepository;

	private final NonApplicabilityExplanationRepository nonApplicabilityExplanationRepository;

	private final RequirementNotTestedExplanationRepository requirementNotTestedExplanationRepository;

	private final CompensatingControlWorksheetRepository compensatingControlWorksheetRepository;

	private final CompensatingControlWorksheetResponseRepository compensatingControlWorksheetResponseRepository;

	public SaqResponseService(SaqResponseRepository saqResponseRepository, SaqQuestionRepository saqQuestionRepository,
			NonApplicabilityExplanationRepository nonApplicabilityExplanationRepository,
			RequirementNotTestedExplanationRepository requirementNotTestedExplanationRepository,
			CompensatingControlWorksheetRepository compensatingControlWorksheetRepository,
			CompensatingControlWorksheetResponseRepository compensatingControlWorksheetResponseRepository) {
		this.saqResponseRepository = saqResponseRepository;
		this.saqQuestionRepository = saqQuestionRepository;
		this.nonApplicabilityExplanationRepository = nonApplicabilityExplanationRepository;
		this.requirementNotTestedExplanationRepository = requirementNotTestedExplanationRepository;
		this.compensatingControlWorksheetRepository = compensatingControlWorksheetRepository;
		this.compensatingControlWorksheetResponseRepository = compensatingControlWorksheetResponseRepository;
	}

	/**
	 * Save a saqResponse.
	 *
	 * @param saqResponse
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SaqResponse save(SaqResponse saqResponse) {
		log.debug("Request to save SaqResponse : {}", saqResponse);
		return saqResponseRepository.save(saqResponse);
	}

	/**
	 * Save a saqResponse.
	 *
	 * @param saqResponse
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SaqResponseDTO saveWithAppendixes(SaqResponseDTO saqResponseDTO) {
		log.debug("Request to save SaqResponse : {}", saqResponseDTO);
		SaqResponse saqResponse = new SaqResponse();
		SaqQuestion saqQuestion = saqQuestionRepository.findOne(saqResponseDTO.getSaqQuestion().getId());
		saqResponse.setQuestionResponse(saqResponseDTO.getQuestionResponse());
		saqResponse.setSaqQuestionId(saqQuestion);
		if (saqResponseDTO.getId() != null) {
			deleteAppendixForCurrentResponse(saqResponseDTO);
			saqResponse.setId(saqResponseDTO.getId());
		}
		saveAppendix(saqResponseDTO, saqQuestion);
		saqResponseDTO.setId(saqResponseRepository.save(saqResponse).getId());

		return saqResponseDTO;
	}

	private void deleteAppendixForCurrentResponse(SaqResponseDTO saqResponseDTO) {
		SaqResponse curSaqResponse = saqResponseRepository.findOne(saqResponseDTO.getId());
		if (saqResponseDTO.getQuestionResponse() != curSaqResponse.getQuestionResponse()) {
			if (curSaqResponse.getQuestionResponse() == QUESTION_RESPONSE.NA) {
				nonApplicabilityExplanationRepository.delete(saqResponseDTO.getNonApplicabilityExplanation().getId());
				saqResponseDTO.setNonApplicabilityExplanation(new NonApplicabilityExplanationDTO());
			} else if (curSaqResponse.getQuestionResponse() == QUESTION_RESPONSE.NOT_TESTED) {
				requirementNotTestedExplanationRepository
						.delete(saqResponseDTO.getRequirementNotTestedExplanation().getId());
				saqResponseDTO.setRequirementNotTestedExplanation(new RequirementNotTestedExplanationDTO());
			} else if (curSaqResponse.getQuestionResponse() == QUESTION_RESPONSE.YES_WITH_CCW) {
				saqResponseDTO.getCompensatingControlWorksheetResponses().forEach(ccwr -> {
					compensatingControlWorksheetResponseRepository.delete(ccwr.getId());
				});
				saqResponseDTO.setCompensatingControlWorksheetResponses(getCompensatingControlWorkSheetResponseList());
			}
		}
	}

	private void saveAppendix(SaqResponseDTO saqResponseDTO, SaqQuestion saqQuestion) {
		if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.NA) {
			NonApplicabilityExplanation nonApplicabilityExplanation = new NonApplicabilityExplanation(
					saqResponseDTO.getNonApplicabilityExplanation());
			nonApplicabilityExplanation.setSaqQuestionId(saqQuestion);
			nonApplicabilityExplanationRepository.save(nonApplicabilityExplanation);
			saqResponseDTO
					.setNonApplicabilityExplanation(new NonApplicabilityExplanationDTO(nonApplicabilityExplanation));
		} else if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.NOT_TESTED) {
			RequirementNotTestedExplanation requirementNotTestedExplanation = new RequirementNotTestedExplanation(
					saqResponseDTO.getRequirementNotTestedExplanation());
			requirementNotTestedExplanation.setSaqQuestionId(saqQuestion);
			requirementNotTestedExplanationRepository.save(requirementNotTestedExplanation);
			saqResponseDTO.setRequirementNotTestedExplanation(
					new RequirementNotTestedExplanationDTO(requirementNotTestedExplanation));
		} else if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.YES_WITH_CCW) {
			List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponses = new ArrayList<>();
			saqResponseDTO.getCompensatingControlWorksheetResponses().stream().forEach(ccwrDTO -> {
				CompensatingControlWorksheetResponse ccwr = new CompensatingControlWorksheetResponse(ccwrDTO);
				ccwr.setSaqQuestionId(saqQuestion);
				compensatingControlWorksheetResponseRepository.save(ccwr);
				compensatingControlWorksheetResponses.add(new CompensatingControlWorkSheetResponseDTO(ccwr));
			});
			saqResponseDTO.setCompensatingControlWorksheetResponses(compensatingControlWorksheetResponses);
		}
	}

	/**
	 * Get all the saqResponses.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<SaqResponse> findAll() {
		log.debug("Request to get all SaqResponses");
		return saqResponseRepository.findAll();
	}

	/**
	 * get all the saqResponses where SaqQuestionId is null.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<SaqResponse> findAllWhereSaqQuestionIdIsNull() {
		log.debug("Request to get all saqResponses where SaqQuestionId is null");
		return StreamSupport.stream(saqResponseRepository.findAll().spliterator(), false)
				.filter(saqResponse -> saqResponse.getSaqQuestionId() == null).collect(Collectors.toList());
	}

	/**
	 * Get one saqResponse by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public SaqResponse findOne(Long id) {
		log.debug("Request to get SaqResponse : {}", id);
		return saqResponseRepository.findOne(id);
	}

	/**
	 * Get one saqResponse by saqQuestionId.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public SaqResponseDTO findOneBySaqQuestionId(Long saqQuestionId) {
		log.debug("Request to get SaqResponse : {}", saqQuestionId);
		SaqResponseDTO saqResponseDTO = new SaqResponseDTO(new NonApplicabilityExplanationDTO(), new RequirementNotTestedExplanationDTO(),
				getCompensatingControlWorkSheetResponseList());
		SaqResponse saqResponse = saqResponseRepository.findOneBySaqQuestionId(saqQuestionId);
		saqResponseDTO.setSaqQuestion(new SaqQuestionUserDTO(saqQuestionRepository.findOne(saqQuestionId)));
		if (saqResponse != null) {
			setAppendix(saqResponseDTO, saqResponse);
		}
		return saqResponseDTO;
	}
	
	private List<CompensatingControlWorkSheetResponseDTO> getCompensatingControlWorkSheetResponseList() {
		List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponses = new ArrayList<>();
		compensatingControlWorksheetRepository.findAll().stream().forEach(compensatingControlWorksheet -> {
			CompensatingControlWorksheetResponse compensatingControlWorksheetResponse = new CompensatingControlWorksheetResponse();
			compensatingControlWorksheetResponse.setCompensatingControlWorksheetId(compensatingControlWorksheet);
			compensatingControlWorksheetResponses
					.add(new CompensatingControlWorkSheetResponseDTO(compensatingControlWorksheetResponse));
		});
		return compensatingControlWorksheetResponses;
	}

	private void setAppendix(SaqResponseDTO saqResponseDTO, SaqResponse saqResponse) {
		saqResponseDTO.setQuestionResponse(saqResponse.getQuestionResponse());
		saqResponseDTO.setId(saqResponse.getId());
		if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.NA) {
			saqResponseDTO.setNonApplicabilityExplanation(
					new NonApplicabilityExplanationDTO(nonApplicabilityExplanationRepository
							.findOneBySaqQuestionId(saqResponse.getSaqQuestionId().getId())));
		} else if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.NOT_TESTED) {
			saqResponseDTO.setRequirementNotTestedExplanation(
					new RequirementNotTestedExplanationDTO(requirementNotTestedExplanationRepository
							.findOneBySaqQuestionId(saqResponse.getSaqQuestionId().getId())));
		} else if (saqResponseDTO.getQuestionResponse() == QUESTION_RESPONSE.YES_WITH_CCW) {
			List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponsesWithExplanation = new ArrayList<>();
			compensatingControlWorksheetResponseRepository
					.findALLBySaqQuestionId(saqResponse.getSaqQuestionId().getId()).stream().forEach(ccwr -> {
						compensatingControlWorksheetResponsesWithExplanation
								.add(new CompensatingControlWorkSheetResponseDTO(ccwr));
					});
			saqResponseDTO
					.setCompensatingControlWorksheetResponses(compensatingControlWorksheetResponsesWithExplanation);
		}
	}

	/**
	 * Delete the saqResponse by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete SaqResponse : {}", id);
		saqResponseRepository.delete(id);
	}
}
