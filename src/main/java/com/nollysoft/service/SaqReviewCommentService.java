package com.nollysoft.service;

import com.nollysoft.domain.SaqReviewComment;
import com.nollysoft.repository.SaqReviewCommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing SaqReviewComment.
 */
@Service
@Transactional
public class SaqReviewCommentService {

    private final Logger log = LoggerFactory.getLogger(SaqReviewCommentService.class);

    private final SaqReviewCommentRepository saqReviewCommentRepository;

    public SaqReviewCommentService(SaqReviewCommentRepository saqReviewCommentRepository) {
        this.saqReviewCommentRepository = saqReviewCommentRepository;
    }

    /**
     * Save a saqReviewComment.
     *
     * @param saqReviewComment the entity to save
     * @return the persisted entity
     */
    public SaqReviewComment save(SaqReviewComment saqReviewComment) {
        log.debug("Request to save SaqReviewComment : {}", saqReviewComment);
        return saqReviewCommentRepository.save(saqReviewComment);
    }

    /**
     * Get all the saqReviewComments.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SaqReviewComment> findAll() {
        log.debug("Request to get all SaqReviewComments");
        return saqReviewCommentRepository.findAll();
    }

    /**
     * Get one saqReviewComment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SaqReviewComment findOne(Long id) {
        log.debug("Request to get SaqReviewComment : {}", id);
        return saqReviewCommentRepository.findOne(id);
    }

    /**
     * Delete the saqReviewComment by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SaqReviewComment : {}", id);
        saqReviewCommentRepository.delete(id);
    }
}
