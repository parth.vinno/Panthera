package com.nollysoft.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.SaqReview;
import com.nollysoft.domain.SaqReview.REVIEW_STATUS;
import com.nollysoft.domain.SaqReviewComment;
import com.nollysoft.repository.SaqQuestionRepository;
import com.nollysoft.repository.SaqReviewRepository;
import com.nollysoft.service.dto.SaqReviewDTO;
import com.nollysoft.service.dto.SaqReviewPageDTO;

/**
 * Service Implementation for managing SaqReview.
 */
@Service
@Transactional
public class SaqReviewService {

	private final Logger log = LoggerFactory.getLogger(SaqReviewService.class);

	private final SaqReviewRepository saqReviewRepository;

	private final SaqQuestionRepository saqQuestionRepository;

	public SaqReviewService(SaqReviewRepository saqReviewRepository, SaqQuestionRepository saqQuestionRepository) {
		this.saqReviewRepository = saqReviewRepository;
		this.saqQuestionRepository = saqQuestionRepository;
	}

	/**
	 * Save a saqReview.
	 *
	 * @param saqReview
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SaqReview save(SaqReview saqReview) {
		log.debug("Request to save SaqReview : {}", saqReview);
		return saqReviewRepository.save(saqReview);
	}

	/**
	 * Get all the saqReviews.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<SaqReview> findAll() {
		log.debug("Request to get all SaqReviews");
		return saqReviewRepository.findAll();
	}

	/**
	 * Get all the saqQuestions.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public SaqReviewPageDTO findAll(String status, Pageable pageable) {
		log.debug("Request to get all SaqReviews");
		Page<SaqReview> result = new PageImpl<>(new ArrayList<>());
		Map<String, Long> map = new TreeMap<String, Long>(PcidssRequirementService.getComparator());
		if (status != null) {
			result = saqReviewRepository.findByStatus(REVIEW_STATUS.valueOf(status), pageable);
		} else {
			result = saqReviewRepository.findAll(pageable);
		}
		List<SaqReviewDTO> saqReviews = new ArrayList<>();
		result.getContent().stream().forEach((saqReview) -> {
			map.put(saqReview.getSaqQuestionId().getQuestionNumber(), saqReview.getSaqQuestionId().getId());
		});
		Set<Long> saqQuestionIds = new LinkedHashSet<>(map.values());
		saqQuestionIds.stream().forEach(questionId -> {
			saqReviews.add(new SaqReviewDTO(
					saqReviewRepository.findOneBySaqQuestionId(saqQuestionRepository.findOne(questionId))));
		});
		return new SaqReviewPageDTO(saqReviews, pageable, result.getTotalElements());
	}

	/**
	 * Get one saqReview by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public SaqReview findOne(Long id) {
		log.debug("Request to get SaqReview : {}", id);
		return saqReviewRepository.findOne(id);
	}

	/**
	 * Delete the saqReview by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete SaqReview : {}", id);
		saqReviewRepository.delete(id);
	}

	/**
	 * Save a saqReview.
	 * 
	 * @param saqQuestionId
	 * @param saqReview
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SaqReview create(Long saqQuestionId, String status, SaqReview saqReview) {
		saqReview.setSaqQuestionId(saqQuestionRepository.findOne(saqQuestionId));
		saqReview.setStatus(REVIEW_STATUS.valueOf(status));
		Set<SaqReviewComment> comments = new LinkedHashSet<>();
		saqReview.getComments().stream().forEach(comment -> {
			comment.setSaqReviewId(saqReview);
			comments.add(comment);
		});
		saqReview.setComments(comments);
		SaqReview curSaqReview = saqReviewRepository.save(saqReview);
		if (saqReview.getId() != null && saqReview.getComments().size() == 0) {
			saqReviewRepository.delete(saqReview.getId());
		}
		return curSaqReview;
	}

	/**
	 * Get one saqReview by saqQuestionId.
	 *
	 * @param saqQuestionId
	 * @return the entity
	 */
	public SaqReview findBySaqQuestionId(Long saqQuestionId) {
		return saqReviewRepository.findOneBySaqQuestionId(saqQuestionRepository.findOne(saqQuestionId));
	}

	/**
	 * Update a saqReview.
	 * 
	 * @param saqQuestionId
	 * @return the updated entity
	 */
	public SaqReview update(Long saqQuestionId) {
		SaqReview saqReview = saqReviewRepository.findOneBySaqQuestionId(saqQuestionRepository.findOne(saqQuestionId));
		if (saqReview != null) {
			saqReview.setStatus(REVIEW_STATUS.CORRECTED);
		}
		return saqReviewRepository.save(saqReview);
	}
}
