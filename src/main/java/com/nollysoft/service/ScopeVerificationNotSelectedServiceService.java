package com.nollysoft.service;

import com.nollysoft.domain.ScopeVerificationNotSelectedService;
import com.nollysoft.repository.ScopeVerificationNotSelectedServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ScopeVerificationNotSelectedService.
 */
@Service
@Transactional
public class ScopeVerificationNotSelectedServiceService {

    private final Logger log = LoggerFactory.getLogger(ScopeVerificationNotSelectedServiceService.class);

    private final ScopeVerificationNotSelectedServiceRepository scopeVerificationNotSelectedServiceRepository;

    public ScopeVerificationNotSelectedServiceService(ScopeVerificationNotSelectedServiceRepository scopeVerificationNotSelectedServiceRepository) {
        this.scopeVerificationNotSelectedServiceRepository = scopeVerificationNotSelectedServiceRepository;
    }

    /**
     * Save a scopeVerificationNotSelectedService.
     *
     * @param scopeVerificationNotSelectedService the entity to save
     * @return the persisted entity
     */
    public ScopeVerificationNotSelectedService save(ScopeVerificationNotSelectedService scopeVerificationNotSelectedService) {
        log.debug("Request to save ScopeVerificationNotSelectedService : {}", scopeVerificationNotSelectedService);
        return scopeVerificationNotSelectedServiceRepository.save(scopeVerificationNotSelectedService);
    }

    /**
     *  Get all the scopeVerificationNotSelectedServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ScopeVerificationNotSelectedService> findAll(Pageable pageable) {
        log.debug("Request to get all ScopeVerificationNotSelectedServices");
        return scopeVerificationNotSelectedServiceRepository.findAll(pageable);
    }

    /**
     *  Get one scopeVerificationNotSelectedService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ScopeVerificationNotSelectedService findOne(Long id) {
        log.debug("Request to get ScopeVerificationNotSelectedService : {}", id);
        return scopeVerificationNotSelectedServiceRepository.findOne(id);
    }

    /**
     *  Delete the  scopeVerificationNotSelectedService by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ScopeVerificationNotSelectedService : {}", id);
        scopeVerificationNotSelectedServiceRepository.delete(id);
    }
}
