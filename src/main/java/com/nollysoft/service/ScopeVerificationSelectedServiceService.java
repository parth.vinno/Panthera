package com.nollysoft.service;

import com.nollysoft.domain.ScopeVerificationSelectedService;
import com.nollysoft.repository.ScopeVerificationSelectedServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ScopeVerificationSelectedService.
 */
@Service
@Transactional
public class ScopeVerificationSelectedServiceService {

    private final Logger log = LoggerFactory.getLogger(ScopeVerificationSelectedServiceService.class);

    private final ScopeVerificationSelectedServiceRepository scopeVerificationSelectedServiceRepository;

    public ScopeVerificationSelectedServiceService(ScopeVerificationSelectedServiceRepository scopeVerificationSelectedServiceRepository) {
        this.scopeVerificationSelectedServiceRepository = scopeVerificationSelectedServiceRepository;
    }

    /**
     * Save a scopeVerificationSelectedService.
     *
     * @param scopeVerificationSelectedService the entity to save
     * @return the persisted entity
     */
    public ScopeVerificationSelectedService save(ScopeVerificationSelectedService scopeVerificationSelectedService) {
        log.debug("Request to save ScopeVerificationSelectedService : {}", scopeVerificationSelectedService);
        return scopeVerificationSelectedServiceRepository.save(scopeVerificationSelectedService);
    }

    /**
     *  Get all the scopeVerificationSelectedServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ScopeVerificationSelectedService> findAll(Pageable pageable) {
        log.debug("Request to get all ScopeVerificationSelectedServices");
        return scopeVerificationSelectedServiceRepository.findAll(pageable);
    }

    /**
     *  Get one scopeVerificationSelectedService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ScopeVerificationSelectedService findOne(Long id) {
        log.debug("Request to get ScopeVerificationSelectedService : {}", id);
        return scopeVerificationSelectedServiceRepository.findOne(id);
    }

    /**
     *  Delete the  scopeVerificationSelectedService by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ScopeVerificationSelectedService : {}", id);
        scopeVerificationSelectedServiceRepository.delete(id);
    }
}
