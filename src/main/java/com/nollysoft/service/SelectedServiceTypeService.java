package com.nollysoft.service;

import com.nollysoft.domain.SelectedServiceType;
import com.nollysoft.repository.SelectedServiceTypeRepository;
import com.nollysoft.service.dto.SelectedServiceTypeDTO;
import com.nollysoft.service.dto.SelectedServiceTypePageDTO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing SelectedServiceType.
 */
@Service
@Transactional
public class SelectedServiceTypeService {

	private final Logger log = LoggerFactory.getLogger(SelectedServiceTypeService.class);

	private final SelectedServiceTypeRepository selectedServiceTypeRepository;

	public SelectedServiceTypeService(SelectedServiceTypeRepository selectedServiceTypeRepository) {
		this.selectedServiceTypeRepository = selectedServiceTypeRepository;
	}

	/**
	 * Save a selectedServiceType.
	 *
	 * @param selectedServiceType
	 *            the entity to save
	 * @return the persisted entity
	 */
	public SelectedServiceType save(SelectedServiceType selectedServiceType) {
		log.debug("Request to save SelectedServiceType : {}", selectedServiceType);
		return selectedServiceTypeRepository.save(selectedServiceType);
	}

	/**
	 * Clear and Save list of selectedServiceType.
	 *
	 * @param list
	 *            of selectedServiceType to save
	 * @return the persisted list of entities
	 */
	public List<SelectedServiceType> create(List<SelectedServiceType> selectedServiceTypes) {
		log.debug("Request to save SelectedServiceType : {}", selectedServiceTypes);
		selectedServiceTypeRepository.deleteAll();
		List<SelectedServiceType> curselectedServiceTypes = new LinkedList<>();
		selectedServiceTypes.stream().forEach(servicetype -> {
			curselectedServiceTypes.add(selectedServiceTypeRepository.save(servicetype));
		});
		return curselectedServiceTypes;
	}

	/**
	 * Get all the selectedServiceTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public SelectedServiceTypePageDTO findAll(Pageable pageable) {
		log.debug("Request to get all SelectedServiceTypes");
		Page<SelectedServiceType> result = selectedServiceTypeRepository.findAll(pageable);
		List<SelectedServiceTypeDTO> selectedServices = new ArrayList<>();
		result.getContent().stream().forEach(service -> {
			selectedServices.add(new SelectedServiceTypeDTO(service));
		});
		return new SelectedServiceTypePageDTO(selectedServices, pageable, result.getTotalElements());
	}

	/**
	 * Get one selectedServiceType by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public SelectedServiceType findOne(Long id) {
		log.debug("Request to get SelectedServiceType : {}", id);
		return selectedServiceTypeRepository.findOne(id);
	}

	/**
	 * Delete the selectedServiceType by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete SelectedServiceType : {}", id);
		selectedServiceTypeRepository.delete(id);
	}
}
