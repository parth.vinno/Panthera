package com.nollysoft.service;

import com.nollysoft.domain.ServiceByServiceProvider;
import com.nollysoft.repository.ServiceByServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ServiceByServiceProvider.
 */
@Service
@Transactional
public class ServiceByServiceProviderService {

    private final Logger log = LoggerFactory.getLogger(ServiceByServiceProviderService.class);

    private final ServiceByServiceProviderRepository serviceByServiceProviderRepository;

    public ServiceByServiceProviderService(ServiceByServiceProviderRepository serviceByServiceProviderRepository) {
        this.serviceByServiceProviderRepository = serviceByServiceProviderRepository;
    }

    /**
     * Save a serviceByServiceProvider.
     *
     * @param serviceByServiceProvider the entity to save
     * @return the persisted entity
     */
    public ServiceByServiceProvider save(ServiceByServiceProvider serviceByServiceProvider) {
        log.debug("Request to save ServiceByServiceProvider : {}", serviceByServiceProvider);
        return serviceByServiceProviderRepository.save(serviceByServiceProvider);
    }

    /**
     *  Get all the serviceByServiceProviders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ServiceByServiceProvider> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceByServiceProviders");
        return serviceByServiceProviderRepository.findAll(pageable);
    }

    /**
     *  Get one serviceByServiceProvider by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ServiceByServiceProvider findOne(Long id) {
        log.debug("Request to get ServiceByServiceProvider : {}", id);
        return serviceByServiceProviderRepository.findOne(id);
    }

    /**
     *  Delete the  serviceByServiceProvider by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ServiceByServiceProvider : {}", id);
        serviceByServiceProviderRepository.delete(id);
    }
}
