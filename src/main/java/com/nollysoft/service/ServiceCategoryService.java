package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ServiceCategory;
import com.nollysoft.repository.ServiceCategoryRepository;

/**
 * Service Implementation for managing ServiceCategory.
 */
@Service
@Transactional
public class ServiceCategoryService {

    private final Logger log = LoggerFactory.getLogger(ServiceCategoryService.class);

    private final ServiceCategoryRepository serviceCategoryRepository;

    public ServiceCategoryService(ServiceCategoryRepository serviceCategoryRepository) {
        this.serviceCategoryRepository = serviceCategoryRepository;
    }

    /**
     * Save a serviceCategory.
     *
     * @param serviceCategory the entity to save
     * @return the persisted entity
     */
    public ServiceCategory save(ServiceCategory serviceCategory) {
        log.debug("Request to save ServiceCategory : {}", serviceCategory);
        return serviceCategoryRepository.save(serviceCategory);
    }

    /**
     *  Get all the serviceCategories.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ServiceCategory> findAll() {
        log.debug("Request to get all ServiceCategories");
        return serviceCategoryRepository.findAll();
    }

    /**
     *  Get one serviceCategory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ServiceCategory findOne(Long id) {
        log.debug("Request to get ServiceCategory : {}", id);
        return serviceCategoryRepository.findOne(id);
    }
    /**
   	 * Get all the serviceCategory.
   	 *
   	 * @param pageable
   	 *            the pagination information
   	 * @return the list of entities
   	 */
   	@Transactional(readOnly = true)
   	public Page<ServiceCategory> findAll(Pageable pageable) {
   		log.debug("Request to get all Payment Channel");
   		return serviceCategoryRepository.findAll(pageable);
   	}
    /**
     *  Delete the  serviceCategory by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ServiceCategory : {}", id);
        serviceCategoryRepository.delete(id);
    }
}
