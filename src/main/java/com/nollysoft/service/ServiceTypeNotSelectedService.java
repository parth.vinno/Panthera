package com.nollysoft.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ServiceTypeNotSelected;
import com.nollysoft.repository.ServiceTypeNotSelectedRepository;
import com.nollysoft.service.dto.ServiceTypeNotSelectedDTO;
import com.nollysoft.service.dto.ServiceTypeNotSelectedPageDTO;

/**
 * Service Implementation for managing ServiceTypeNotSelected.
 */
@Service
@Transactional
public class ServiceTypeNotSelectedService {

	private final Logger log = LoggerFactory.getLogger(ServiceTypeNotSelectedService.class);

	private final ServiceTypeNotSelectedRepository serviceTypeNotSelectedRepository;

	public ServiceTypeNotSelectedService(ServiceTypeNotSelectedRepository serviceTypeNotSelectedRepository) {
		this.serviceTypeNotSelectedRepository = serviceTypeNotSelectedRepository;
	}

	/**
	 * Save a serviceTypeNotSelected.
	 *
	 * @param serviceTypeNotSelected
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ServiceTypeNotSelected save(ServiceTypeNotSelected serviceTypeNotSelected) {
		log.debug("Request to save ServiceTypeNotSelected : {}", serviceTypeNotSelected);
		return serviceTypeNotSelectedRepository.save(serviceTypeNotSelected);
	}

	/**
	 * Clear and Save list of serviceTypeNotSelected.
	 *
	 * @param list
	 *            of serviceTypeNotSelected to save
	 * @return the persisted list of entities
	 */
	public List<ServiceTypeNotSelected> create(List<ServiceTypeNotSelected> selectedServiceTypes) {
		log.debug("Request to save SelectedServiceType : {}", selectedServiceTypes);
		serviceTypeNotSelectedRepository.deleteAll();
		List<ServiceTypeNotSelected> curServiceTypesNotSelected = new LinkedList<>();
		selectedServiceTypes.stream().forEach(servicetype -> {
			curServiceTypesNotSelected.add(serviceTypeNotSelectedRepository.save(servicetype));
		});
		return curServiceTypesNotSelected;
	}

	/**
	 * Get all the serviceTypeNotSelecteds.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public ServiceTypeNotSelectedPageDTO findAll(Pageable pageable) {
		log.debug("Request to get all ServiceTypeNotSelecteds");
		Page<ServiceTypeNotSelected> result = serviceTypeNotSelectedRepository.findAll(pageable);
		List<ServiceTypeNotSelectedDTO> serviceTypesNotSelected = new ArrayList<>();
		result.getContent().stream().forEach(notSelectedService -> {
			serviceTypesNotSelected.add(new ServiceTypeNotSelectedDTO(notSelectedService));
		});
		return new ServiceTypeNotSelectedPageDTO(serviceTypesNotSelected, pageable, result.getTotalElements());
	}

	/**
	 * Get one serviceTypeNotSelected by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ServiceTypeNotSelected findOne(Long id) {
		log.debug("Request to get ServiceTypeNotSelected : {}", id);
		return serviceTypeNotSelectedRepository.findOne(id);
	}

	/**
	 * Delete the serviceTypeNotSelected by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ServiceTypeNotSelected : {}", id);
		serviceTypeNotSelectedRepository.delete(id);
	}
}
