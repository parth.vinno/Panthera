package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.ServiceType;
import com.nollysoft.repository.ServiceTypeRepository;
import com.nollysoft.service.dto.ServiceTypeDTO;
import com.nollysoft.service.dto.ServiceTypePageDTO;


/**
 * Service Implementation for managing ServiceType.
 */
@Service
@Transactional
public class ServiceTypeService {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeService.class);

    private final ServiceTypeRepository serviceTypeRepository;

    public ServiceTypeService(ServiceTypeRepository serviceTypeRepository) {
        this.serviceTypeRepository = serviceTypeRepository;
    }

    /**
     * Save a serviceType.
     *
     * @param serviceType the entity to save
     * @return the persisted entity
     */
    public ServiceType save(ServiceType serviceType) {
        log.debug("Request to save ServiceType : {}", serviceType);
        return serviceTypeRepository.save(serviceType);
    }

    /**
     *  Get all the serviceTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public ServiceTypePageDTO findAll(Pageable pageable) {
        log.debug("Request to get all ServiceTypes");
        Page<ServiceType> result = serviceTypeRepository.findAll(pageable);
		List<ServiceTypeDTO> services = new ArrayList<>();
		result.getContent().stream().forEach((service) -> {
			services.add(new ServiceTypeDTO(service));
		});
        return new ServiceTypePageDTO(services, pageable, result.getTotalElements());
    }

    /**
     *  Get one serviceType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ServiceType findOne(Long id) {
        log.debug("Request to get ServiceType : {}", id);
        return serviceTypeRepository.findOne(id);
    }

    /**
     *  Delete the  serviceType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ServiceType : {}", id);
        serviceTypeRepository.delete(id);
    }
}
