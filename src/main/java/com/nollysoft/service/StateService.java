package com.nollysoft.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.State;
import com.nollysoft.repository.StateRepository;


/**
 * Service Implementation for managing State.
 */
@Service
@Transactional
public class StateService {

    private final Logger log = LoggerFactory.getLogger(StateService.class);

    private final StateRepository stateRepository;

    public StateService(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    /**
     * Save a state.
     *
     * @param state the entity to save
     * @return the persisted entity
     */
    public State save(State state) {
        log.debug("Request to save State : {}", state);
        return stateRepository.save(state);
    }

    /**
     *  Get all the states.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<State> findAllByCountryCode(String countryCode) {
        log.debug("Request to get all States");
        return stateRepository.findAllByCountryCodeOrderByLongName(countryCode);
    }

    @Transactional(readOnly = true)
    public List<State> findAll() {
        log.debug("Request to get all States");
        return stateRepository.findAll();
    }

    /**
     *  Get one state by id.
     *
     *  @param shortName the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public State findOne(String shortName) {
        log.debug("Request to get State : {}", shortName);
        return stateRepository.findOne(shortName);
    }

    /**
     *  Delete the  state by id.
     *
     *  @param shortName the id of the entity
     */
    public void delete(String shortName) {
        log.debug("Request to delete State : {}", shortName);
        stateRepository.delete(shortName);
    }
}
