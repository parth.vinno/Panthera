package com.nollysoft.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.nollysoft.domain.Address;
import com.nollysoft.domain.Tenant;
import com.nollysoft.domain.User;
import com.nollysoft.repository.UserRepository;

@Service
public class TenancyService {

	@Value("${spring.datasource.dataSourceClassName}")
	String jdbcDriver;

	@Value("${spring.datasource.tenant.url}")
	String url;

	@Value("${spring.datasource.username}")
	String userName;

	@Value("${spring.datasource.password}")
	String password;

	@Value("${application.defaultDatabase}")
	String defaultDB;

	@Autowired
	private UserRepository userRepository;

	public void createTenantDB(String dbName) throws Exception {
		Connection conn1 = null;
		Connection conn2 = null;
		Statement s = null;
		InputStream in1 = null;
		InputStream in2 = null;
		ScriptRunner sr1 =null;
		ScriptRunner sr2 =null;
		Reader reader1 = null;
		Reader reader2 = null;
		try {
			Class.forName(jdbcDriver);
			conn1 = DriverManager.getConnection(url + "?user=" + userName + "&password=" + password);
			s = conn1.createStatement();
			s.executeUpdate("CREATE DATABASE " + dbName);
			Resource resource = new ClassPathResource("config/script/panthera-non-seed-tables.sql");
			in1 = resource.getInputStream();
			String aSQLScriptFilePath1 = resource.getURL().toString();
			conn2 = DriverManager.getConnection(url + dbName, userName, password);

			try {
				// Initialize object for ScriptRunner
				sr1 = new ScriptRunner(conn2);

				// Give the input file to Reader
				reader1 = new BufferedReader(new InputStreamReader(in1, "UTF-8"));

				// Execute script
				sr1.runScript(reader1);

			} catch (Exception e) {
				System.err.println("Failed to Execute" + aSQLScriptFilePath1 + " The error is " + e.getMessage());
			}
			resource = new ClassPathResource("config/script/panthera-seed-tables.sql");
			in2 = resource.getInputStream();
			String aSQLScriptFilePath2 = resource.getURL().toString();

			try {
				// Initialize object for ScriptRunner
				sr2 = new ScriptRunner(conn2);

				// Give the input file to Reader
				reader2 = new BufferedReader(new InputStreamReader(in2, "UTF-8"));

				// Execute script
				sr2.runScript(reader2);

			} catch (Exception e) {
				System.err.println("Failed to Execute" + aSQLScriptFilePath2 + " The error is " + e.getMessage());
			}
		} finally {
			if (reader1 != null) {
				reader1.close();
			}
			if (reader2 != null) {
				reader2.close();
			}
			if (sr1 != null) {
				sr1.closeConnection();
			}
			if (sr1 != null) {
				sr2.closeConnection();
			}
			if (in1 != null) {
				in1.close();
			}
			if (in2 != null) {
				in2.close();
			}
			if (s != null) {
				s.close();
			}
			if (conn1 != null) {
				conn1.close();
			}
			if (conn2 != null) {
				conn2.close();
			}
		}
	}

	@Async
	public void createUserInDefaultDB(User user) {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + defaultDB, userName, password);
			Statement s = conn.createStatement();

			Set<Long> groupIds = new HashSet<>();
			user.getUserGroups().stream().forEach(group -> {
				groupIds.add(group.getId());
			});

			String createdDate = user.getCreatedDate().toString().substring(0, 10)
					.concat(" " + user.getCreatedDate().toString().substring(11, 19));
			String lastModDate = user.getLastModifiedDate().toString().substring(0, 10)
					.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));
			String resetDate = null;
			if (user.getResetDate() != null) {
				resetDate = user.getLastModifiedDate().toString().substring(0, 10)
						.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));
			}

			s.executeQuery(
					"INSERT INTO jhi_user  (id, login, password_hash, first_name, last_name, salutation, title, gender, phone, email, image_url, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date, tenant_id, deleted, role) VALUES(NULL,"
							+ getQueryValue(user.getLogin()) + "," + getQueryValue(user.getPassword()) + ","
							+ getQueryValue(user.getFirstName()) + "," + getQueryValue(user.getLastName()) + ","
							+ getQueryValue(user.getSalutation()) + "," + getQueryValue(user.getTitle()) + ","
							+ getQueryValue(user.getGender().toString()) + "," + getQueryValue(user.getPhone()) + ","
							+ getQueryValue(user.getEmail()) + "," + getQueryValue(user.getImageUrl()) + ", "
							+ user.getActivated() + "," + getQueryValue(user.getLangKey()) + ","
							+ getQueryValue(user.getActivationKey()) + "," + getQueryValue(user.getResetKey()) + ","
							+ getQueryValue(user.getCreatedBy()) + "," + getQueryValue(createdDate) + ","
							+ getQueryValue(resetDate) + "," + getQueryValue(user.getLastModifiedBy()) + ","
							+ getQueryValue(lastModDate) + "," + getQueryValue(user.getTenantId()) + ","
							+ user.isDeleted() + "," + getQueryValue(user.getRole()) + ")");

			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login =" + getQueryValue(user.getLogin()));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			Long userId = curUserId;
			groupIds.stream().forEach(groupId -> {
				try {
					s.executeQuery("INSERT INTO jhi_user_user_group (jhi_user_id, user_group_id) VALUES(" + userId + ","
							+ groupId + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Async
	public void updateUserInDefaultDB(User user, String curLogin) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + defaultDB, userName, password);
			Statement s = conn.createStatement();
			Set<Long> groupIds = new HashSet<>();
			user.getUserGroups().stream().forEach(group -> {
				groupIds.add(group.getId());
			});
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login = '" + curLogin + "'");
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			String lastModDate = user.getLastModifiedDate().toString().substring(0, 10)
					.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));

			s.executeQuery("DELETE FROM jhi_user_user_group WHERE jhi_user_id = " + curUserId);

			s.executeQuery("UPDATE jhi_user SET first_name =" + getQueryValue(user.getFirstName()) + ", last_name ="
					+ getQueryValue(user.getLastName()) + ", salutation =" + getQueryValue(user.getSalutation())
					+ ", title =" + getQueryValue(user.getTitle()) + ", gender ="
					+ getQueryValue(user.getGender().toString()) + ", phone =" + getQueryValue(user.getPhone())
					+ ", image_url =" + getQueryValue(user.getImageUrl()) + ", lang_key ="
					+ getQueryValue(user.getLangKey()) + ", last_modified_by ="
					+ getQueryValue(user.getLastModifiedBy()) + ", last_modified_date =" + getQueryValue(lastModDate)
					+ ", role =" + getQueryValue(user.getRole()) + "WHERE id = " + curUserId);

			Long userId = curUserId;
			groupIds.stream().forEach(groupId -> {
				try {
					s.executeQuery("INSERT INTO jhi_user_user_group (jhi_user_id,user_group_id) VALUES(" + userId + "," + groupId + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	@Async
	public void deleteUserFromDefaultDB(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			userRepository.delete(user);
		});
	}

	@Async
	public void changePasswordInDefaultDB(User user) {
		userRepository.findOneByLogin(user.getLogin()).ifPresent(curUser -> {
			curUser.setPassword(user.getPassword());
			userRepository.save(curUser);
		});
	}

	@Async
	public void createUserInTenantDB(User user) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + user.getTenantId(), userName, password);
			Statement s = conn.createStatement();

			Set<Long> groupIds = new HashSet<>();
			user.getUserGroups().stream().forEach(group -> {
				groupIds.add(group.getId());
			});

			String createdDate = user.getCreatedDate().toString().substring(0, 10)
					.concat(" " + user.getCreatedDate().toString().substring(11, 19));
			String lastModDate = user.getLastModifiedDate().toString().substring(0, 10)
					.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));
			String resetDate = null;
			if (user.getResetDate() != null) {
				resetDate = user.getLastModifiedDate().toString().substring(0, 10)
						.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));
			}

			s.executeQuery(
					"INSERT INTO jhi_user  (id, login, password_hash, first_name, last_name, salutation, title, gender, phone, email, image_url, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date, tenant_id, deleted, role) VALUES(NULL,"
							+ getQueryValue(user.getLogin()) + "," + getQueryValue(user.getPassword()) + ","
							+ getQueryValue(user.getFirstName()) + "," + getQueryValue(user.getLastName()) + ","
							+ getQueryValue(user.getSalutation()) + "," + getQueryValue(user.getTitle()) + ","
							+ getQueryValue(user.getGender().toString()) + "," + getQueryValue(user.getPhone()) + ","
							+ getQueryValue(user.getEmail()) + "," + getQueryValue(user.getImageUrl()) + ", "
							+ user.getActivated() + "," + getQueryValue(user.getLangKey()) + ","
							+ getQueryValue(user.getActivationKey()) + "," + getQueryValue(user.getResetKey()) + ","
							+ getQueryValue(user.getCreatedBy()) + "," + getQueryValue(createdDate) + ","
							+ getQueryValue(resetDate) + "," + getQueryValue(user.getLastModifiedBy()) + ","
							+ getQueryValue(lastModDate) + "," + getQueryValue(user.getTenantId()) + ","
							+ user.isDeleted() + "," + getQueryValue(user.getRole()) + ")");

			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login =" + getQueryValue(user.getLogin()));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			Long userId = curUserId;
			groupIds.stream().forEach(groupId -> {
				try {
					s.executeQuery("INSERT INTO jhi_user_user_group (jhi_user_id, user_group_id) VALUES(" + userId + ","
							+ groupId + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Async
	public void activateUserInTenantDB(User user) throws SQLException {
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + user.getTenantId(), userName, password);
			s = conn.createStatement();
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login =" + getQueryValue(user.getLogin()));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			s.executeQuery("UPDATE jhi_user SET activated = true WHERE id =" + curUserId);
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Async
	public void updateUserInTenantDB(User user, String curLogin) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + user.getTenantId(), userName, password);
			Statement s = conn.createStatement();
			Set<Long> groupIds = new HashSet<>();
			user.getUserGroups().stream().forEach(group -> {
				groupIds.add(group.getId());
			});
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login =" + getQueryValue(curLogin));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			String lastModDate = user.getLastModifiedDate().toString().substring(0, 10)
					.concat(" " + user.getLastModifiedDate().toString().substring(11, 19));

			s.executeQuery("DELETE FROM jhi_user_user_group WHERE jhi_user_id = " + curUserId);

			s.executeQuery("UPDATE jhi_user SET first_name =" + getQueryValue(user.getFirstName()) + ", last_name ="
					+ getQueryValue(user.getLastName()) + ", salutation =" + getQueryValue(user.getSalutation())
					+ ", title =" + getQueryValue(user.getTitle()) + ", gender ="
					+ getQueryValue(user.getGender().toString()) + ", phone =" + getQueryValue(user.getPhone())
					+ ", image_url =" + getQueryValue(user.getImageUrl()) + ", lang_key ="
					+ getQueryValue(user.getLangKey()) + ", last_modified_by ="
					+ getQueryValue(user.getLastModifiedBy()) + ", last_modified_date =" + getQueryValue(lastModDate)
					+ ", role =" + getQueryValue(user.getRole()) + "WHERE id = " + curUserId);

			Long userId = curUserId;
			groupIds.stream().forEach(groupId -> {
				try {
					s.executeQuery("INSERT INTO jhi_user_user_group (jhi_user_id, user_group_id) VALUES(" + userId + "," + groupId + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Async
	public void deleteUserFromTenantDB(User user) throws Exception {
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + user.getTenantId(), userName, password);
			s = conn.createStatement();
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login = '" + user.getLogin() + "'");
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}

			s.executeQuery("DELETE FROM jhi_user_user_group WHERE jhi_user_id = " + curUserId);

			s.executeQuery("DELETE FROM jhi_user WHERE id = " + curUserId);
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	public boolean userWithLoginPresent(String login) throws Exception {
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + defaultDB, userName, password);
			s = conn.createStatement();
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE login =" + getQueryValue(login));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			if (curUserId != null) {
				return true;
			} else {
				return false;
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	public boolean userWithEmailPresent(String email) throws Exception {
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url + defaultDB, userName, password);
			s = conn.createStatement();
			rs = s.executeQuery("SELECT id FROM jhi_user WHERE email =" + getQueryValue(email));
			Long curUserId = null;
			if (rs.next()) {
				curUserId = rs.getLong("id");
			}
			if (curUserId != null) {
				return true;
			} else {
				return false;
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Async
	public void createTenantInTenantDB(Tenant tenant) throws Exception {
		Connection conn = null;
		Statement s = null;
		PreparedStatement ps = null;
		ResultSet rs = null, rs1 = null, rs2 = null;
		try {
			conn = DriverManager.getConnection(url + tenant.getTenantId(), userName, password);
			s = conn.createStatement();

			Address address = tenant.getAddressId();

			String query = "INSERT INTO address (id, street1, street2, city, postal_code, state, province, country) VALUES(NULL,"
					+ getQueryValue(address.getStreet1()) + "," + getQueryValue(address.getStreet2()) + ","
					+ getQueryValue(address.getCity()) + "," + getQueryValue(address.getPostalCode()) + ","
					+ getQueryValue(address.getState()) + "," + getQueryValue(address.getProvince()) + ","
					+ getQueryValue(address.getCountry()) + ")";

			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.execute();
			rs = ps.getGeneratedKeys();
			Long generatedKey = null;
			if (rs.next()) {
				generatedKey = rs.getLong("id");
			}
			Long addressId = generatedKey;

			rs1 = s.executeQuery(
					"SELECT id FROM assessment_type WHERE name = '" + tenant.getAssessmentTypeId().getName() + "'");
			if (rs1.next()) {
				generatedKey = rs1.getLong("id");
			}
			Long assessmentTypeId = generatedKey;

			rs2 = s.executeQuery("SELECT id FROM industry WHERE name = '" + tenant.getIndustryId().getName() + "'");
			if (rs2.next()) {
				generatedKey = rs2.getLong("id");
			}
			Long industryId = generatedKey;

			ps.executeQuery(
					"INSERT INTO tenant (id, company_url, contact_person_deskphone, contact_person_email, contact_person_mobilephone, contact_person_name, create_date, name, company_abbreviation, company_phone_number, email, service_start_date, tenant_id, address_id, assessment_type_id, industry_id, deleted) VALUES(NULL,"
							+ getQueryValue(tenant.getCompanyUrl()) + ","
							+ getQueryValue(tenant.getContactPersonDeskphone()) + ","
							+ getQueryValue(tenant.getContactPersonEmail()) + ","
							+ getQueryValue(tenant.getContactPersonMobilephone()) + ","
							+ getQueryValue(tenant.getContactPersonName()) + ","
							+ getQueryValue(tenant.getCreateDate().toString()) + "," + getQueryValue(tenant.getName())
							+ "," + getQueryValue(tenant.getCompanyAbbreviation()) + ","
							+ getQueryValue(tenant.getCompanyPhoneNumber()) + "," + getQueryValue(tenant.getEmail())
							+ "," + getQueryValue(tenant.getServiceStartDate().toString()) + ", "
							+ getQueryValue(tenant.getTenantId()) + ", " + addressId + ", " + assessmentTypeId + ", "
							+ industryId + ", " + tenant.isDeleted() + ")");
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (rs != null) {
				rs.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
			if (rs2 != null) {
				rs2.close();
			}
		}
	}

	@Async
	public void updateTenantInTenantDB(Tenant tenant, String tenantId) throws Exception {
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null, rs1 = null, rs2 = null, rs3 = null;
		try {
			conn = DriverManager.getConnection(url + tenant.getTenantId(), userName, password);
			s = conn.createStatement();

			Address address = tenant.getAddressId();
			rs = s.executeQuery("SELECT address_id FROM tenant WHERE tenant_id =" + getQueryValue(tenantId));
			Long generatedKey = null;
			if (rs.next()) {
				generatedKey = rs.getLong("address_id");
			}
			Long addressId = generatedKey;

			s.executeQuery("UPDATE address SET street1 =" + getQueryValue(address.getStreet1()) + ", street2 ="
					+ getQueryValue(address.getStreet2()) + ", city =" + getQueryValue(address.getCity())
					+ ", postal_code =" + getQueryValue(address.getPostalCode()) + ", state ="
					+ getQueryValue(address.getState()) + ", province =" + getQueryValue(address.getProvince())
					+ ", country =" + getQueryValue(address.getCountry())
					+ " WHERE id = " + addressId);

			rs1 = s.executeQuery("SELECT id FROM assessment_type WHERE name ="
					+ getQueryValue(tenant.getAssessmentTypeId().getName()));
			if (rs1.next()) {
				generatedKey = rs1.getLong("id");
			}
			Long assessmentTypeId = generatedKey;

			rs2 = s.executeQuery(
					"SELECT id FROM industry WHERE name =" + getQueryValue(tenant.getIndustryId().getName()));
			if (rs2.next()) {
				generatedKey = rs2.getLong("id");
			}
			Long industryId = generatedKey;

			rs3 = s.executeQuery("SELECT id FROM tenant WHERE tenant_id =" + getQueryValue(tenantId));
			if (rs3.next()) {
				generatedKey = rs3.getLong("id");
			}
			Long curId = generatedKey;

			s.executeQuery("UPDATE tenant SET company_url =" + getQueryValue(tenant.getCompanyUrl())
					+ ", contact_person_deskphone =" + getQueryValue(tenant.getContactPersonDeskphone())
					+ ", contact_person_email =" + getQueryValue(tenant.getContactPersonEmail())
					+ ", contact_person_mobilephone =" + getQueryValue(tenant.getContactPersonMobilephone())
					+ ", contact_person_name =" + getQueryValue(tenant.getContactPersonName()) + ", create_date ="
					+ getQueryValue(tenant.getCreateDate().toString()) + ", name =" + getQueryValue(tenant.getName())
					+ ", company_abbreviation =" + getQueryValue(tenant.getCompanyAbbreviation())
					+ ", company_phone_number =" + getQueryValue(tenant.getCompanyPhoneNumber()) + ", email ="
					+ getQueryValue(tenant.getEmail()) + ", service_start_date ="
					+ getQueryValue(tenant.getServiceStartDate().toString()) + ", tenant_id ="
					+ getQueryValue(tenant.getTenantId()) + ", address_id = " + addressId + ", assessment_type_id = "
					+ assessmentTypeId + ", industry_id = " + industryId + " WHERE id = " + curId);
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
			if (rs != null) {
				rs.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
			if (rs2 != null) {
				rs2.close();
			}
			if (rs3 != null) {
				rs3.close();
			}
		}
	}

	@Async
	public void deleteTenantInTenantDB(String tenantId) throws SQLException {
		Connection conn = null;
		Statement s = null;
		try {
			conn = DriverManager.getConnection(url + tenantId, userName, password);
			s = conn.createStatement();
			s.executeQuery("Update tenant SET deleted = true WHERE tenant_id = '" + tenantId + "'");
		} finally {
			if (conn != null) {
				conn.close();
			}
			if (s != null) {
				s.close();
			}
		}
	}

	private String getQueryValue(String field) {
		if (field != null) {
			return " '" + field + "' ";
		}
		return field;
	}
}
