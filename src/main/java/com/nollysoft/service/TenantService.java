package com.nollysoft.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.Tenant;
import com.nollysoft.repository.TenantRepository;

/**
 * Service Implementation for managing Tenant.
 */
@Service
@Transactional
public class TenantService {

	private final Logger log = LoggerFactory.getLogger(TenantService.class);

	private final TenantRepository tenantRepository;

	public TenantService(TenantRepository tenantRepository) {
		this.tenantRepository = tenantRepository;
	}

	/**
	 * Save a tenant.
	 *
	 * @param tenant
	 *            the entity to save
	 * @return the persisted entity
	 */
	public Tenant save(Tenant tenant) {
		log.debug("Request to save Tenant : {}", tenant);
		Tenant curTenant = tenant;
		curTenant.setDeleted(false);
		return tenantRepository.save(tenant);
	}

	/**
	 * Get all the tenants.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<Tenant> findAll(Pageable pageable) {
		log.debug("Request to get all Tenants");
		return tenantRepository.findByDeletedIsFalse(pageable);
	}

	/**
	 * Get one tenant by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Tenant findOne(Long id) {
		log.debug("Request to get Tenant : {}", id);
		return tenantRepository.findOneByIdAndDeletedIsFalse(id);
	}

	/**
	 * Get domain of tenant by tenantId.
	 *
	 * @param tenantId
	 *            the tenantId of the entity
	 * @return the domain
	 */
	public String getDomain(String tenantId) {
		log.debug("Request to get Tenant domain : {}", tenantId);
		Tenant tenant = tenantRepository.findOneByTenantIdAndDeletedIsFalse(tenantId);
		String domain = null;
		if (tenant != null) {
			String url = tenant.getCompanyUrl();
			domain = url.substring(url.indexOf("://") + 3);
		}
		return domain;
	}

	/**
	 * Delete the tenant by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Tenant : {}", id);
		Tenant curTenant = tenantRepository.findOne(id);
		curTenant.setDeleted(true);
		tenantRepository.save(curTenant);
	}
}
