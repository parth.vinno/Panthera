package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.TestedRequirementHeader;
import com.nollysoft.repository.TestedRequirementHeaderRepository;
import com.nollysoft.service.dto.TestedRequirementHeaderDTO;

/**
 * Service Implementation for managing TestedRequirementHeader.
 */
@Service
@Transactional
public class TestedRequirementHeaderService {

	private final Logger log = LoggerFactory.getLogger(TestedRequirementHeaderService.class);

	private final TestedRequirementHeaderRepository testedRequirementHeaderRepository;

	public TestedRequirementHeaderService(TestedRequirementHeaderRepository testedRequirementHeaderRepository) {
		this.testedRequirementHeaderRepository = testedRequirementHeaderRepository;
	}

	/**
	 * Save a testedRequirementHeader.
	 *
	 * @param testedRequirementHeader
	 *            the entity to save
	 * @return the persisted entity
	 */
	public TestedRequirementHeader save(TestedRequirementHeader testedRequirementHeader) {
		log.debug("Request to save TestedRequirementHeader : {}", testedRequirementHeader);
		return testedRequirementHeaderRepository.save(testedRequirementHeader);
	}

	/**
	 * Get all the testedRequirementHeaders.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<TestedRequirementHeaderDTO> findAll() {
		log.debug("Request to get all TestedRequirementHeaders");
		List<TestedRequirementHeaderDTO> result = new ArrayList<>();
		testedRequirementHeaderRepository.findAll().stream().forEach(header -> {
			result.add(new TestedRequirementHeaderDTO(header));
		});
		return result;
	}

	/**
	 * Get one testedRequirementHeader by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public TestedRequirementHeader findOne(Long id) {
		log.debug("Request to get TestedRequirementHeader : {}", id);
		return testedRequirementHeaderRepository.findOne(id);
	}

	/**
	 * Delete the testedRequirementHeader by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete TestedRequirementHeader : {}", id);
		testedRequirementHeaderRepository.delete(id);
	}
}
