package com.nollysoft.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.domain.TestedRequirementSummary;
import com.nollysoft.repository.TestedRequirementSummaryRepository;
import com.nollysoft.service.dto.TestedRequirementSummaryDTO;
import com.nollysoft.service.dto.TestedRequirementSummaryPageDTO;


/**
 * Service Implementation for managing TestedRequirementSummary.
 */
@Service
@Transactional
public class TestedRequirementSummaryService {

    private final Logger log = LoggerFactory.getLogger(TestedRequirementSummaryService.class);

    private final TestedRequirementSummaryRepository testedRequirementSummaryRepository;

    public TestedRequirementSummaryService(TestedRequirementSummaryRepository testedRequirementSummaryRepository) {
        this.testedRequirementSummaryRepository = testedRequirementSummaryRepository;
    }

    /**
     * Save a testedRequirementSummary.
     *
     * @param testedRequirementSummary the entity to save
     * @return the persisted entity
     */
    public TestedRequirementSummary save(TestedRequirementSummary testedRequirementSummary) {
        log.debug("Request to save TestedRequirementSummary : {}", testedRequirementSummary);
        return testedRequirementSummaryRepository.save(testedRequirementSummary);
    }

    /**
     * Save a list of testedRequirementSummary.
     *
     * @param list of testedRequirementSummary to save
     * @return the list of persisted entities
     */
	public List<TestedRequirementSummaryDTO> create(List<TestedRequirementSummary> testedRequirementsSummary) {
		List<TestedRequirementSummaryDTO> testedRequirementSummarylist = new ArrayList<>();
		testedRequirementsSummary.stream().forEach(requirement -> {
			testedRequirementSummarylist
					.add(new TestedRequirementSummaryDTO(testedRequirementSummaryRepository.save(requirement)));
		});
		return testedRequirementSummarylist;
	}

    /**
     *  Get all the testedRequirementSummaries.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
	@Transactional(readOnly = true)
	public TestedRequirementSummaryPageDTO findAll(Pageable pageable) {
		log.debug("Request to get all TestedRequirementSummaries");
		Page<TestedRequirementSummary> result = testedRequirementSummaryRepository.findAll(pageable);
		List<TestedRequirementSummaryDTO> TestedRequirementSummaries = new ArrayList<>();
		result.getContent().stream().forEach(summary -> {
			TestedRequirementSummaries.add(new TestedRequirementSummaryDTO(summary));
		});
		return new TestedRequirementSummaryPageDTO(TestedRequirementSummaries, pageable, result.getTotalElements());
	}

    /**
     *  Get one testedRequirementSummary by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TestedRequirementSummary findOne(Long id) {
        log.debug("Request to get TestedRequirementSummary : {}", id);
        return testedRequirementSummaryRepository.findOne(id);
    }

    /**
     *  Delete the  testedRequirementSummary by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestedRequirementSummary : {}", id);
        testedRequirementSummaryRepository.delete(id);
    }
}
