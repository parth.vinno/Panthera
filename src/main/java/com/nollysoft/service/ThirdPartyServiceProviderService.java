package com.nollysoft.service;

import com.nollysoft.domain.ThirdPartyServiceProvider;
import com.nollysoft.repository.ThirdPartyServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ThirdPartyServiceProvider.
 */
@Service
@Transactional
public class ThirdPartyServiceProviderService {

    private final Logger log = LoggerFactory.getLogger(ThirdPartyServiceProviderService.class);

    private final ThirdPartyServiceProviderRepository thirdPartyServiceProviderRepository;

    public ThirdPartyServiceProviderService(ThirdPartyServiceProviderRepository thirdPartyServiceProviderRepository) {
        this.thirdPartyServiceProviderRepository = thirdPartyServiceProviderRepository;
    }

    /**
     * Save a thirdPartyServiceProvider.
     *
     * @param thirdPartyServiceProvider the entity to save
     * @return the persisted entity
     */
    public ThirdPartyServiceProvider save(ThirdPartyServiceProvider thirdPartyServiceProvider) {
        log.debug("Request to save ThirdPartyServiceProvider : {}", thirdPartyServiceProvider);
        return thirdPartyServiceProviderRepository.save(thirdPartyServiceProvider);
    }

    /**
     *  Get all the thirdPartyServiceProviders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ThirdPartyServiceProvider> findAll(Pageable pageable) {
        log.debug("Request to get all ThirdPartyServiceProviders");
        return thirdPartyServiceProviderRepository.findAll(pageable);
    }

    /**
     *  Get one thirdPartyServiceProvider by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ThirdPartyServiceProvider findOne(Long id) {
        log.debug("Request to get ThirdPartyServiceProvider : {}", id);
        return thirdPartyServiceProviderRepository.findOne(id);
    }

    /**
     *  Delete the  thirdPartyServiceProvider by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ThirdPartyServiceProvider : {}", id);
        thirdPartyServiceProviderRepository.delete(id);
    }
}
