package com.nollysoft.service;

import com.nollysoft.domain.UserGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing UserGroup.
 */
public interface UserGroupService {

    /**
     * Save a userGroup.
     *
     * @param userGroup the entity to save
     * @return the persisted entity
     */
    UserGroup save(UserGroup userGroup);

    /**
     *  Get all the userGroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UserGroup> findAll(Pageable pageable);

    /**
     *  Get the "id" userGroup.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UserGroup findOne(Long id);

    /**
     *  Delete the "id" userGroup.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     *  Get all the userGroups except admin.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
	Page<UserGroup> findAllExceptAdmin(Pageable pageable);
}
