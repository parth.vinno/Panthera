package com.nollysoft.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.config.Constants;
import com.nollysoft.domain.Authority;
import com.nollysoft.domain.Tenant;
import com.nollysoft.domain.User;
import com.nollysoft.domain.User.GENDER;
import com.nollysoft.domain.UserGroup;
import com.nollysoft.domain.enumeration.UserRole;
import com.nollysoft.repository.AuthorityRepository;
import com.nollysoft.repository.TenantRepository;
import com.nollysoft.repository.UserGroupRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.security.SecurityUtils;
import com.nollysoft.service.dto.AdminCreationDTO;
import com.nollysoft.service.dto.AdminUpdateDTO;
import com.nollysoft.service.dto.TenantAdminCreationDTO;
import com.nollysoft.service.dto.UserCreationDTO;
import com.nollysoft.service.dto.UserDTO;
import com.nollysoft.service.dto.UserOutputDTO;
import com.nollysoft.service.dto.UserUpdateDTO;
import com.nollysoft.service.util.RandomUtil;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final AuthorityRepository authorityRepository;

	@Autowired
	private UserGroupRepository userGroupRepository;

	@Autowired
	private TenantRepository tenantRepository;

	@Autowired
	private ModelMapper modelMapper;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public boolean isValidEmail(String email, String tenantId) {
		Tenant tenant = tenantRepository.findOneByTenantIdAndDeletedIsFalse(tenantId);
		if (tenant != null) {
			String companyUrl = tenantRepository.findOneByTenantIdAndDeletedIsFalse(tenantId).getCompanyUrl();
			String userDomain = email.substring(email.indexOf("@") + 1);
			if (companyUrl.contains(userDomain))
				return true;
			else
				return false;
		} else
			return false;
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key)
				.filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400))).map(user -> {
					user.setPassword(passwordEncoder.encode(newPassword));
					user.setResetKey(null);
					user.setResetDate(null);
					return user;
				});
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findOneByEmail(mail).filter(User::getActivated).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(Instant.now());
			return user;
		});
	}

	public User createAdmin(AdminCreationDTO adminCreationDTO, String password, Set<UserGroup> userGroups) {

		User newUser = modelMapper.map(adminCreationDTO, User.class);
		String encryptedPassword = passwordEncoder.encode(password);
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		// new user is not active
		newUser.setActivated(false);
		newUser.setUserGroups(userGroups);
		newUser.setDeleted(false);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createTenantAdmin(TenantAdminCreationDTO tenantAdminCreationDTO, String password,
			Set<UserGroup> groups) {

		User newUser = modelMapper.map(tenantAdminCreationDTO, User.class);
		String encryptedPassword = passwordEncoder.encode(password);
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		// new user is not active
		newUser.setActivated(false);
		newUser.setUserGroups(groups);
		newUser.setDeleted(false);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createTenantUser(UserCreationDTO userCreationDTO, String password, String tenantId) {

		if (userCreationDTO.getUserGroups() == null) {
			Set<UserGroup> userGroups = new HashSet<>();
			userGroups.add(userGroupRepository.findOneByName("DEFAULT"));
			userCreationDTO.setUserGroups(userGroups);
		}
		User newUser = modelMapper.map(userCreationDTO, User.class);
		String encryptedPassword = passwordEncoder.encode(password);
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		// new user is not active
		newUser.setActivated(false);
		newUser.setTenantId(tenantId);
		newUser.setDeleted(false);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createUser(UserDTO userDTO) {
		User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		user.setImageUrl(userDTO.getImageUrl());
		if (userDTO.getLangKey() == null) {
			user.setLangKey("en"); // default language
		} else {
			user.setLangKey(userDTO.getLangKey());
		}
		/*
		 * if (userDTO.getAuthorities() != null) { Set<Authority> authorities =
		 * new HashSet<>(); userDTO.getAuthorities().forEach(authority ->
		 * authorities.add(authorityRepository.findByName(authority)));
		 * user.setAuthorities(authorities); }
		 */
		user.setUserGroups(userDTO.getUserGroups());
		String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
		user.setPassword(encryptedPassword);
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		user.setActivated(true);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param firstName
	 *            first name of user
	 * @param lastName
	 *            last name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 * @param imageUrl
	 *            image URL of user
	 */
	public User updateUser(String curlogin, String login, String firstName, String lastName, String title,
			String salutation, GENDER gender, String phone, String email, String langKey, String imageUrl,
			Set<UserGroup> groups) {
		User user = new User();
		if (userRepository.findOneByLogin(curlogin).isPresent()) {
			user = userRepository.findOneByLogin(curlogin).get();
			user.setLogin(login);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setTitle(title);
			user.setSalutation(salutation);
			user.setGender(gender);
			user.setPhone(phone);
			user.setEmail(email);
			user.setLangKey(langKey);
			user.setImageUrl(imageUrl);
			user.setUserGroups(groups);
		}
		return userRepository.save(user);
	}

	/**
	 * Update basic information (first name, last name, email, language) for
	 * the, and return the modified user.
	 *
	 * @param AdminUpdateDTO
	 *            user to update
	 * @return updated user
	 */
	public User updateAdmin(AdminUpdateDTO userDTO) {
		User curUser = userRepository.findOne(userDTO.getId());
		modelMapper.map(userDTO, curUser);
		return userRepository.save(curUser);
	}

	/**
	 * Update basic information (first name, last name, email, language) for
	 * the, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public User updateUser(UserUpdateDTO userDTO) {
		User curUser = userRepository.findOne(userDTO.getId());
		modelMapper.map(userDTO, curUser);
		return userRepository.save(curUser);
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			userRepository.delete(user);
			log.debug("Deleted User: {}", user);
		});
	}

	public void changePassword(String password) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			log.debug("Changed password for User: {}", user);
		});
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithUserGroupsByLogin(login);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities(Long id) {
		return userRepository.findOneWithUserGroupsById(id);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities() {
		return userRepository.findOneWithUserGroupsByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
	}

	/**
	 * Get all the tenant admins
	 *
	 * @param pageable
	 * @param role
	 * @return the list of entities
	 */
	public Page<UserDTO> getAllAdmins(Pageable pageable) {
		return userRepository
				.findAllByUserGroupsNameOrUserGroupsNameAndDeletedIsFalse(pageable, "ADMIN_GROUP", "TENANT_ADMIN_GROUP")
				.map(UserDTO::new);
	}

	/**
	 * Get all the users by tenantId
	 *
	 * @param pageable
	 * @param tenantId
	 * @return the list of entities
	 */
	public Page<UserDTO> getAllUsers(Pageable pageable, String tenantId) {
		return userRepository.findAllByTenantIdAndDeletedIsFalse(pageable, tenantId).map(UserDTO::new);
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void removeNotActivatedUsers() {
		List<User> users = userRepository
				.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
		for (User user : users) {
			log.debug("Deleting not activated user {}", user.getLogin());
			userRepository.delete(user);
		}
	}

	/**
	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}

	public List<UserGroup> getUserGroups() {
		return userGroupRepository.findAll();
	}

	/**
	 * @return a list of all the users
	 */
	public Page<UserDTO> findAll(Pageable pageable) {
		return userRepository.findAll(pageable).map(UserDTO::new);
	}

	public void deleteByTenantId(String tenantId) {
		List<User> users = userRepository.findAllByTenantId(tenantId);
		users.stream().forEach(user -> {
			user.setDeleted(true);
			userRepository.save(user);
		});
	}

	public List<UserOutputDTO> findAllByRole(String role) {
		log.debug("Request to get all QA Notifications");
		List<UserOutputDTO> dtos = new ArrayList<>();
		userRepository.findByRole(UserRole.valueOf(role)).stream().forEach(user -> {
			dtos.add(new UserOutputDTO(user));
		});
		return dtos;
	}

	public UserRole[] findAllRoles() {
		return UserRole.values();
	}

	/**
	 * @param userGroup
	 * @return true if there is any user belongs to the given userGroup else
	 *         false
	 */
	public boolean isUserPresent(String userGroup) {
		return userRepository.countByUserGroupsName(userGroup) > 0;
	}
}
