package com.nollysoft.service.dto;

import com.nollysoft.domain.AOCLegalExceptionDetails;

public class AOCLegalExceptionDetailsDTO {

	private Long id;

	private String legalConstraintDetail;

	private PcidssRequirementAdminDTO pcidssRequirementId;

	public AOCLegalExceptionDetailsDTO() {
	}

	public AOCLegalExceptionDetailsDTO(AOCLegalExceptionDetails aocLegalExceptionDetails) {
		this.id = aocLegalExceptionDetails.getId();
		this.legalConstraintDetail = aocLegalExceptionDetails.getLegalConstraintDetail();
		this.pcidssRequirementId = new PcidssRequirementAdminDTO(aocLegalExceptionDetails.getPcidssRequirementId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLegalConstraintDetail() {
		return legalConstraintDetail;
	}

	public void setLegalConstraintDetail(String legalConstraintDetail) {
		this.legalConstraintDetail = legalConstraintDetail;
	}

	public PcidssRequirementAdminDTO getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public void setPcidssRequirementId(PcidssRequirementAdminDTO pcidssRequirementId) {
		this.pcidssRequirementId = pcidssRequirementId;
	}

}
