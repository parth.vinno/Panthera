package com.nollysoft.service.dto;

import com.nollysoft.domain.CompensatingControlWorksheet;
import com.nollysoft.domain.CompensatingControlWorksheetResponse;

public class CompensatingControlWorkSheetResponseDTO {

	private Long id;

	private String explanation;

	private CompensatingControlWorksheet compensatingControlWorksheetId;

	public CompensatingControlWorkSheetResponseDTO() {}

	public CompensatingControlWorkSheetResponseDTO(
			CompensatingControlWorksheetResponse compensatingControlWorkSheetResponse) {
		this.id = compensatingControlWorkSheetResponse.getId();
		this.explanation = compensatingControlWorkSheetResponse.getExplanation();
		this.compensatingControlWorksheetId = compensatingControlWorkSheetResponse.getCompensatingControlWorksheetId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public CompensatingControlWorksheet getCompensatingControlWorksheetId() {
		return compensatingControlWorksheetId;
	}

	public void setCompensatingControlWorksheetId(CompensatingControlWorksheet compensatingControlWorksheetId) {
		this.compensatingControlWorksheetId = compensatingControlWorksheetId;
	}
}
