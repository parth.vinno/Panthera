package com.nollysoft.service.dto;

import com.nollysoft.domain.enumeration.Section;

public class ExecutiveSummaryStatusDTO {

	public static enum STATUS {
		START, RESUME, REVISIT;
	}

	private STATUS status;

	private Section lastUpdatedSection;

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public Section getLastUpdatedSection() {
		return lastUpdatedSection;
	}

	public void setLastUpdatedSection(Section lastUpdatedSection) {
		this.lastUpdatedSection = lastUpdatedSection;
	}
}
