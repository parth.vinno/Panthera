package com.nollysoft.service.dto;

import com.nollysoft.domain.NonApplicabilityExplanation;

public class NonApplicabilityExplanationDTO {

	private Long id;

	private String reasonRequirementNotApplicable;

	public NonApplicabilityExplanationDTO() {

	}

	public NonApplicabilityExplanationDTO(NonApplicabilityExplanation nonApplicabilityExplanation) {
		this.id = nonApplicabilityExplanation.getId();
		this.reasonRequirementNotApplicable = nonApplicabilityExplanation.getReasonRequirementNotApplicable();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReasonRequirementNotApplicable() {
		return reasonRequirementNotApplicable;
	}

	public void setReasonRequirementNotApplicable(String reasonRequirementNotApplicable) {
		this.reasonRequirementNotApplicable = reasonRequirementNotApplicable;
	}
}
