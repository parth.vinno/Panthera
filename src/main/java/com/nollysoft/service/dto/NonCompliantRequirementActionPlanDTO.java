package com.nollysoft.service.dto;

import java.time.LocalDate;

import com.nollysoft.domain.NonCompliantRequirementActionPlan;

public class NonCompliantRequirementActionPlanDTO {

	private Long id;

	private boolean compliant;

	private String remediationPlan;

	private LocalDate remediationDate;

	private PcidssRequirementAdminDTO pcidssRequirementId;

	public NonCompliantRequirementActionPlanDTO() {
	}

	public NonCompliantRequirementActionPlanDTO(NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan) {
		this.id = nonCompliantRequirementActionPlan.getId();
		this.compliant = nonCompliantRequirementActionPlan.isCompliant();
		this.remediationPlan = nonCompliantRequirementActionPlan.getRemediationPlan();
		this.remediationDate = nonCompliantRequirementActionPlan.getRemediationDate();
		this.pcidssRequirementId = new PcidssRequirementAdminDTO(
				nonCompliantRequirementActionPlan.getPcidssRequirementId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isCompliant() {
		return compliant;
	}

	public void setCompliant(boolean compliant) {
		this.compliant = compliant;
	}

	public String getRemediationPlan() {
		return remediationPlan;
	}

	public void setRemediationPlan(String remediationPlan) {
		this.remediationPlan = remediationPlan;
	}

	public LocalDate getRemediationDate() {
		return remediationDate;
	}

	public void setRemediationDate(LocalDate remediationDate) {
		this.remediationDate = remediationDate;
	}

	public PcidssRequirementAdminDTO getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public void setPcidssRequirementId(PcidssRequirementAdminDTO pcidssRequirementId) {
		this.pcidssRequirementId = pcidssRequirementId;
	}

}
