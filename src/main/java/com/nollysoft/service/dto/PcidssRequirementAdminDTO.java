package com.nollysoft.service.dto;

import com.nollysoft.domain.PcidssRequirement;

public class PcidssRequirementAdminDTO {
	
	private Long id;

	private String requirement;

	private String requirementNumber;
	
	private RequirementCategoryAdminDTO requirementCategoryId;
	
	public PcidssRequirementAdminDTO() {
	}
	
	public PcidssRequirementAdminDTO(PcidssRequirement pcidssRequirement) {
		if (pcidssRequirement != null) {
			this.id = pcidssRequirement.getId();
			this.requirement = pcidssRequirement.getRequirement();
			this.requirementNumber = pcidssRequirement.getRequirementNumber();
			if (pcidssRequirement.getRequirementCategoryId() != null) {
				pcidssRequirement.getRequirementCategoryId().setPcidssRequirements(null);
			}
			RequirementCategoryAdminDTO category = new RequirementCategoryAdminDTO(pcidssRequirement.getRequirementCategoryId());
			this.requirementCategoryId = category;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getRequirementNumber() {
		return requirementNumber;
	}

	public void setRequirementNumber(String requirementNumber) {
		this.requirementNumber = requirementNumber;
	}

	public RequirementCategoryAdminDTO getRequirementCategoryId() {
		return requirementCategoryId;
	}

	public void setRequirementCategoryId(RequirementCategoryAdminDTO requirementCategoryId) {
		this.requirementCategoryId = requirementCategoryId;
	}
}
