package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class PcidssRequirementPageDTO extends PageImpl<PcidssRequirementAdminDTO> {
	
	private static final long serialVersionUID = 1L;

	public PcidssRequirementPageDTO(List<PcidssRequirementAdminDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
