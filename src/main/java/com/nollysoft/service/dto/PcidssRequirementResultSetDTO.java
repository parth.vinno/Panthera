package com.nollysoft.service.dto;

public class PcidssRequirementResultSetDTO {

	private long id;

	private long questionCount;

	private long responseCount;

	public PcidssRequirementResultSetDTO() {
	}
	
	public PcidssRequirementResultSetDTO(long id) {
		this.id = id;
	}
	
	public PcidssRequirementResultSetDTO(long id, long questionCount) {
		this.id = id;
		this.questionCount = questionCount;
	}

	public PcidssRequirementResultSetDTO(long id, long questionCount, long responseCount) {
		this.id = id;
		this.questionCount = questionCount;
		this.responseCount = responseCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(long questionCount) {
		this.questionCount = questionCount;
	}

	public long getResponseCount() {
		return responseCount;
	}

	public void setResponseCount(long responseCount) {
		this.responseCount = responseCount;
	}
}
