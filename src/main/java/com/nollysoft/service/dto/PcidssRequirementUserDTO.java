package com.nollysoft.service.dto;

import com.nollysoft.domain.PcidssRequirement;

public class PcidssRequirementUserDTO {

	public static enum REQUIREMENT_STATUS {
		TAKE, RESUME, REVISIT;
	}

	private Long id;

	private String requirement;

	private String requirementNumber;

	private REQUIREMENT_STATUS requirementStatus = REQUIREMENT_STATUS.TAKE;

	private boolean available = false;

	private Long[] questionIds;
	
	private Long lastRespondedQuestionId;

	private RequirementCategoryUserDTO requirementCategoryId;

	public PcidssRequirementUserDTO() {
	}
	
	public PcidssRequirementUserDTO(PcidssRequirement pcidssRequirement) {
		this.id = pcidssRequirement.getId();
		this.requirement = pcidssRequirement.getRequirement();
		this.requirementNumber = pcidssRequirement.getRequirementNumber();
		this.requirementCategoryId = new RequirementCategoryUserDTO(pcidssRequirement.getRequirementCategoryId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getRequirementNumber() {
		return requirementNumber;
	}

	public void setRequirementNumber(String requirementNumber) {
		this.requirementNumber = requirementNumber;
	}

	public REQUIREMENT_STATUS getRequirementStatus() {
		return requirementStatus;
	}

	public void setRequirementStatus(REQUIREMENT_STATUS requirementStatus) {
		this.requirementStatus = requirementStatus;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Long[] getQuestionIds() {
		return questionIds;
	}

	public void setQuestionIds(Long[] questionIds) {
		this.questionIds = questionIds;
	}

	public Long getLastRespondedQuestionId() {
		return lastRespondedQuestionId;
	}

	public void setLastRespondedQuestionId(Long lastRespondedQuestionId) {
		this.lastRespondedQuestionId = lastRespondedQuestionId;
	}

	public RequirementCategoryUserDTO getRequirementCategoryId() {
		return requirementCategoryId;
	}

	public void setRequirementCategoryId(RequirementCategoryUserDTO requirementCategoryId) {
		this.requirementCategoryId = requirementCategoryId;
	}
}
