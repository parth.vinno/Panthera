package com.nollysoft.service.dto;

import java.time.LocalDate;

import com.nollysoft.domain.User;
import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;

public class QANotificationDTO {

	private Long id;
	
	private LocalDate notificationDate;
	
	private LocalDate correctionDate;
	
	private NOTIFICATION_STATUS status;
	
	private User qsaId;
	
	private SaqQuestionUserDTO lastReviewedQuestion;
	
	public QANotificationDTO(){
	}
	
	public QANotificationDTO(QANotification qaNotification){
		this.id = qaNotification.getId();
		this.notificationDate = qaNotification.getNotificationDate();
		this.correctionDate = qaNotification.getCorrectionDate();
		this.status = qaNotification.getStatusType();
		this.qsaId = qaNotification.getQsaId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(LocalDate notificationDate) {
		this.notificationDate = notificationDate;
	}

	public LocalDate getCorrectionDate() {
		return correctionDate;
	}

	public void setCorrectionDate(LocalDate correctionDate) {
		this.correctionDate = correctionDate;
	}

	public NOTIFICATION_STATUS getStatus() {
		return status;
	}

	public void setStatus(NOTIFICATION_STATUS status) {
		this.status = status;
	}

	public User getQsaId() {
		return qsaId;
	}

	public void setQsaId(User userId) {
		this.qsaId = userId;
	}

	public SaqQuestionUserDTO getLastReviewedQuestion() {
		return lastReviewedQuestion;
	}

	public void setLastReviewedQuestion(SaqQuestionUserDTO lastReviewedQuestion) {
		this.lastReviewedQuestion = lastReviewedQuestion;
	}
	
}
