package com.nollysoft.service.dto;

import java.util.LinkedHashSet;
import java.util.Set;

import com.nollysoft.domain.RequirementCategory;

public class RequirementCategoryAdminDTO {

	private Long id;

	private String categoryName;

	private Set<PcidssRequirementAdminDTO> pcidssRequirements = new LinkedHashSet<>();

	public RequirementCategoryAdminDTO() {
	}

	public RequirementCategoryAdminDTO(RequirementCategory requirementCategory) {
		if (requirementCategory != null) {
			this.id = requirementCategory.getId();
			this.categoryName = requirementCategory.getCategoryName();
			if (requirementCategory.getPcidssRequirements() != null
					&& !requirementCategory.getPcidssRequirements().isEmpty()) {
				requirementCategory.getPcidssRequirements().stream().forEach(requirement -> {
					addPcidssRequirement(new PcidssRequirementAdminDTO(requirement));
				});
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Set<PcidssRequirementAdminDTO> getPcidssRequirements() {
		return pcidssRequirements;
	}

	public void setPcidssRequirements(Set<PcidssRequirementAdminDTO> pcidssRequirements) {
		this.pcidssRequirements = pcidssRequirements;
	}

	public void addPcidssRequirement(PcidssRequirementAdminDTO pcidssRequirement) {
		this.pcidssRequirements.add(pcidssRequirement);
	}
}
