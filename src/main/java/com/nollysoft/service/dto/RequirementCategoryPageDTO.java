package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class RequirementCategoryPageDTO extends PageImpl<RequirementCategoryAdminDTO> {
	
	private static final long serialVersionUID = 1L;

	public RequirementCategoryPageDTO(List<RequirementCategoryAdminDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
