package com.nollysoft.service.dto;

import com.nollysoft.domain.RequirementCategory;

public class RequirementCategoryUserDTO {
	private Long id;

	private String categoryName;

	private boolean available;

	public RequirementCategoryUserDTO() {
	}

	public RequirementCategoryUserDTO(RequirementCategory requirementCategory) {
		if (requirementCategory != null) {
			this.id = requirementCategory.getId();
			this.categoryName = requirementCategory.getCategoryName();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
