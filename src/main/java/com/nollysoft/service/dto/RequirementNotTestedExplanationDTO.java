package com.nollysoft.service.dto;

import com.nollysoft.domain.RequirementNotTestedExplanation;

public class RequirementNotTestedExplanationDTO {

	private Long id;

	private String requirementPartNotTested;

	private String reason;

	public RequirementNotTestedExplanationDTO() {

	}

	public RequirementNotTestedExplanationDTO(RequirementNotTestedExplanation requirementNotTestedExplantion) {
		this.id = requirementNotTestedExplantion.getId();
		this.requirementPartNotTested = requirementNotTestedExplantion.getRequirementPartNotTested();
		this.reason = requirementNotTestedExplantion.getReason();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequirementPartNotTested() {
		return requirementPartNotTested;
	}

	public void setRequirementPartNotTested(String requirementPartNotTested) {
		this.requirementPartNotTested = requirementPartNotTested;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
