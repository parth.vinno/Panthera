package com.nollysoft.service.dto;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.nollysoft.domain.ExpectedTest;
import com.nollysoft.domain.SaqQuestion;

public class SaqQuestionAdminDTO {

	private Long id;

	private String questionNumber;

	private boolean responseRequired;

	private String questionText;

	private boolean part;

	private Set<ExpectedTest> expectedTests;

	Set<SaqQuestionAdminDTO> subQuestions = new LinkedHashSet<>();

	private PcidssRequirementAdminDTO pcidssRequirementId;

	public SaqQuestionAdminDTO() {
	}

	public SaqQuestionAdminDTO(SaqQuestion saqQuestion) {
		this.id = saqQuestion.getId();
		this.questionNumber = saqQuestion.getQuestionNumber();
		this.questionText = saqQuestion.getQuestionText();
		this.part = saqQuestion.isPart();
		this.expectedTests = saqQuestion.getExpectedTests();
		if (saqQuestion.getSubQuestions() != null && !saqQuestion.getSubQuestions().isEmpty()) {
			Set<SaqQuestionAdminDTO> subQuestions = new HashSet<>();
			saqQuestion.getSubQuestions().stream().forEach(question -> {
				subQuestions.add(new SaqQuestionAdminDTO(question));
			});
			this.subQuestions = subQuestions;
		}
		this.pcidssRequirementId = new PcidssRequirementAdminDTO(saqQuestion.getPcidssRequirementId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}

	public boolean isResponseRequired() {
		return responseRequired;
	}

	public void setResponseRequired(boolean responseRequired) {
		this.responseRequired = responseRequired;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public boolean isPart() {
		return part;
	}

	public void setPart(boolean part) {
		this.part = part;
	}

	public Set<ExpectedTest> getExpectedTests() {
		return expectedTests;
	}

	public void setExpectedTests(Set<ExpectedTest> expectedTests) {
		this.expectedTests = expectedTests;
	}

	public Set<SaqQuestionAdminDTO> getSubQuestions() {
		return subQuestions;
	}

	public void setSubQuestions(Set<SaqQuestionAdminDTO> subQuestions) {
		this.subQuestions = subQuestions;
	}

	public PcidssRequirementAdminDTO getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public void setPcidssRequirementId(PcidssRequirementAdminDTO pcidssRequirementId) {
		this.pcidssRequirementId = pcidssRequirementId;
	}
}
