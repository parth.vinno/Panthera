package com.nollysoft.service.dto;

import java.util.Set;

import com.nollysoft.domain.ExpectedTest;
import com.nollysoft.domain.SaqQuestion;

public class SaqQuestionUserDTO {

	private Long id;

	private String questionText;
	
	private String questionNumber;

	private SaqQuestionUserDTO parentQuestion;

	private Set<ExpectedTest> expectedTests;
	
	private PcidssRequirementAdminDTO pcidssRequirementId;

	public SaqQuestionUserDTO() {

	}

	public SaqQuestionUserDTO(SaqQuestion saqQuestion) {
		this.id = saqQuestion.getId();
		this.questionText = saqQuestion.getQuestionText();
		this.questionNumber = saqQuestion.getQuestionNumber();
		if (saqQuestion.getParentId() != null) {
			this.parentQuestion = new SaqQuestionUserDTO(saqQuestion.getParentId());
		}
		this.expectedTests = saqQuestion.getExpectedTests();
		this.pcidssRequirementId = new PcidssRequirementAdminDTO(saqQuestion.getPcidssRequirementId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}

	public SaqQuestionUserDTO getParentQuestion() {
		return parentQuestion;
	}

	public void setParentQuestion(SaqQuestionUserDTO parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	public Set<ExpectedTest> getExpectedTests() {
		return expectedTests;
	}

	public void setExpectedTests(Set<ExpectedTest> expectedTests) {
		this.expectedTests = expectedTests;
	}

	public PcidssRequirementAdminDTO getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public void setPcidssRequirementId(PcidssRequirementAdminDTO pcidssRequirementId) {
		this.pcidssRequirementId = pcidssRequirementId;
	}
}
