package com.nollysoft.service.dto;

import java.util.List;

import com.nollysoft.domain.SaqResponse.QUESTION_RESPONSE;

/**
 * A DTO representing a saqResponse, with appendixes.
 */
public class SaqResponseDTO {

	private Long id;

	private QUESTION_RESPONSE questionResponse;

	private SaqQuestionUserDTO saqQuestion;

	private NonApplicabilityExplanationDTO nonApplicabilityExplanation;

	private RequirementNotTestedExplanationDTO requirementNotTestedExplanation;

	private List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponses;

	public SaqResponseDTO() {

	}

	public SaqResponseDTO(NonApplicabilityExplanationDTO nonApplicabilityExplanation,
			RequirementNotTestedExplanationDTO requirementNotTestedExplanation,
			List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponses) {
		this.nonApplicabilityExplanation = nonApplicabilityExplanation;
		this.requirementNotTestedExplanation = requirementNotTestedExplanation;
		this.compensatingControlWorksheetResponses = compensatingControlWorksheetResponses;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public QUESTION_RESPONSE getQuestionResponse() {
		return questionResponse;
	}

	public void setQuestionResponse(QUESTION_RESPONSE questionResponse) {
		this.questionResponse = questionResponse;
	}

	public SaqQuestionUserDTO getSaqQuestion() {
		return saqQuestion;
	}

	public void setSaqQuestion(SaqQuestionUserDTO saqQuestion) {
		this.saqQuestion = saqQuestion;
	}

	public NonApplicabilityExplanationDTO getNonApplicabilityExplanation() {
		return nonApplicabilityExplanation;
	}

	public void setNonApplicabilityExplanation(NonApplicabilityExplanationDTO nonApplicabilityExplanation) {
		this.nonApplicabilityExplanation = nonApplicabilityExplanation;
	}

	public RequirementNotTestedExplanationDTO getRequirementNotTestedExplanation() {
		return requirementNotTestedExplanation;
	}

	public void setRequirementNotTestedExplanation(RequirementNotTestedExplanationDTO requirementNotTestedExplanation) {
		this.requirementNotTestedExplanation = requirementNotTestedExplanation;
	}

	public List<CompensatingControlWorkSheetResponseDTO> getCompensatingControlWorksheetResponses() {
		return compensatingControlWorksheetResponses;
	}

	public void setCompensatingControlWorksheetResponses(
			List<CompensatingControlWorkSheetResponseDTO> compensatingControlWorksheetResponses) {
		this.compensatingControlWorksheetResponses = compensatingControlWorksheetResponses;
	}
}
