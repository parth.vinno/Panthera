package com.nollysoft.service.dto;

import java.time.LocalDateTime;
import java.util.Set;

import com.nollysoft.domain.SaqReview;
import com.nollysoft.domain.SaqReview.REVIEW_STATUS;
import com.nollysoft.domain.SaqReviewComment;

public class SaqReviewDTO {

	private Long id;

	private Set<SaqReviewComment> comments;

	private REVIEW_STATUS status;

	private LocalDateTime createdDate;

	private SaqQuestionUserDTO saqQuestion;

	public SaqReviewDTO() {
	}

	public SaqReviewDTO(SaqReview saqReview) {
		this.id = saqReview.getId();
		this.comments = saqReview.getComments();
		this.status = REVIEW_STATUS.valueOf(saqReview.getStatus());
		this.createdDate = saqReview.getCreatedDate();
		this.saqQuestion = new SaqQuestionUserDTO(saqReview.getSaqQuestionId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<SaqReviewComment> getComments() {
		return comments;
	}

	public void setComments(Set<SaqReviewComment> comments) {
		this.comments = comments;
	}

	public REVIEW_STATUS getStatus() {
		return status;
	}

	public void setStatus(REVIEW_STATUS status) {
		this.status = status;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public SaqQuestionUserDTO getSaqQuestion() {
		return saqQuestion;
	}

	public void setSaqQuestion(SaqQuestionUserDTO saqQuestion) {
		this.saqQuestion = saqQuestion;
	}

}
