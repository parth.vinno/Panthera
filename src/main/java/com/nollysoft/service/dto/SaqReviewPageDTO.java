package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class SaqReviewPageDTO extends PageImpl<SaqReviewDTO> {

	private static final long serialVersionUID = 1L;

	public SaqReviewPageDTO(List<SaqReviewDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
