package com.nollysoft.service.dto;

import com.nollysoft.domain.SelectedServiceType;

public class SelectedServiceTypeDTO {

	private Long id;

	private ServiceTypeDTO serviceTypeId;

	public SelectedServiceTypeDTO() {
	}

	public SelectedServiceTypeDTO(SelectedServiceType selectedServiceType) {
		this.id = selectedServiceType.getId();
		this.serviceTypeId = new ServiceTypeDTO(selectedServiceType.getServiceTypeId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceTypeDTO getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(ServiceTypeDTO serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}
}
