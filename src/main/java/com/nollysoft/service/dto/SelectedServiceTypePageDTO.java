package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class SelectedServiceTypePageDTO extends PageImpl<SelectedServiceTypeDTO> {

	private static final long serialVersionUID = 7600090604075718993L;

	public SelectedServiceTypePageDTO(List<SelectedServiceTypeDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
