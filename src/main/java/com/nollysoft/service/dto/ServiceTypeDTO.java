package com.nollysoft.service.dto;

import com.nollysoft.domain.ServiceCategory;
import com.nollysoft.domain.ServiceType;

public class ServiceTypeDTO {

	private Long id;

	private String serviceType;

	private ServiceCategory serviceCategoryId;

	public ServiceTypeDTO() {
	}

	public ServiceTypeDTO(ServiceType serviceType) {
		this.id = serviceType.getId();
		this.serviceType = serviceType.getServiceType();
		this.serviceCategoryId = serviceType.getServiceCategoryId();
		this.serviceCategoryId.setServiceTypes(null);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public ServiceCategory getServiceCategoryId() {
		return serviceCategoryId;
	}

	public void setServiceCategoryId(ServiceCategory serviceCategoryId) {
		this.serviceCategoryId = serviceCategoryId;
	}

}
