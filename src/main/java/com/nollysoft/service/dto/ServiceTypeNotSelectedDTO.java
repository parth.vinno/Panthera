package com.nollysoft.service.dto;

import com.nollysoft.domain.ServiceTypeNotSelected;

public class ServiceTypeNotSelectedDTO {

	private Long id;

	private ServiceTypeDTO serviceTypeId;

	public ServiceTypeNotSelectedDTO() {
	}

	public ServiceTypeNotSelectedDTO(ServiceTypeNotSelected serviceTypeNotSelected) {
		this.id = serviceTypeNotSelected.getId();
		this.serviceTypeId = new ServiceTypeDTO(serviceTypeNotSelected.getServiceTypeId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceTypeDTO getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(ServiceTypeDTO serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}
}
