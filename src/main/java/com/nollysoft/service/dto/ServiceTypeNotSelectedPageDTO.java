package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class ServiceTypeNotSelectedPageDTO extends PageImpl<ServiceTypeNotSelectedDTO> {

	private static final long serialVersionUID = 7600090604075718993L;

	public ServiceTypeNotSelectedPageDTO(List<ServiceTypeNotSelectedDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
