package com.nollysoft.service.dto;

import java.time.LocalDate;

import com.nollysoft.domain.Address;
import com.nollysoft.domain.Industry;

public class TenantUpdateDTO {

	private Long id;

    private String companyUrl;

    private String contactPersonDeskphone;

    private String contactPersonMobilephone;

    private String contactPersonName;

    private LocalDate createDate;

    private String name;

    private String companyPhoneNumber;

    private String email;

    private LocalDate serviceStartDate;

    private Address addressId;

    private Industry industryId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyUrl() {
		return companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	public String getContactPersonDeskphone() {
		return contactPersonDeskphone;
	}

	public void setContactPersonDeskphone(String contactPersonDeskphone) {
		this.contactPersonDeskphone = contactPersonDeskphone;
	}

	public String getContactPersonMobilephone() {
		return contactPersonMobilephone;
	}

	public void setContactPersonMobilephone(String contactPersonMobilephone) {
		this.contactPersonMobilephone = contactPersonMobilephone;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public LocalDate getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompanyPhoneNumber() {
		return companyPhoneNumber;
	}

	public void setCompanyPhoneNumber(String companyPhoneNumber) {
		this.companyPhoneNumber = companyPhoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(LocalDate serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}

	public Address getAddressId() {
		return addressId;
	}

	public void setAddressId(Address addressId) {
		this.addressId = addressId;
	}

	public Industry getIndustryId() {
		return industryId;
	}

	public void setIndustryId(Industry industryId) {
		this.industryId = industryId;
	}
}
