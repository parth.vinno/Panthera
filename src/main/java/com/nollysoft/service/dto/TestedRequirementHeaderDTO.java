package com.nollysoft.service.dto;

import java.util.HashSet;
import java.util.Set;

import com.nollysoft.domain.TestedRequirementHeader;

public class TestedRequirementHeaderDTO {

	public Long id;

	private String serviceAssessedName;

	private Set<TestedRequirementSummaryDTO> testedRequirementSummaries = new HashSet<>();

	public TestedRequirementHeaderDTO() {
	}

	public TestedRequirementHeaderDTO(TestedRequirementHeader testedRequirementHeader) {
		this.id = testedRequirementHeader.getId();
		this.serviceAssessedName = testedRequirementHeader.getServiceAssessedName();
		if (testedRequirementHeader.getTestedRequirementSummaries() != null) {
			testedRequirementHeader.getTestedRequirementSummaries().forEach(summary -> {
				this.testedRequirementSummaries.add(new TestedRequirementSummaryDTO(summary));
			});
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceAssessedName() {
		return serviceAssessedName;
	}

	public void setServiceAssessedName(String serviceAssessedName) {
		this.serviceAssessedName = serviceAssessedName;
	}

	public Set<TestedRequirementSummaryDTO> getTestedRequirementSummaries() {
		return testedRequirementSummaries;
	}

	public void setTestedRequirementSummaries(Set<TestedRequirementSummaryDTO> testedRequirementSummaries) {
		this.testedRequirementSummaries = testedRequirementSummaries;
	}
}
