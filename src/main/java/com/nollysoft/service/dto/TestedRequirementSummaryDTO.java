package com.nollysoft.service.dto;

import com.nollysoft.domain.TestedRequirementSummary;
import com.nollysoft.domain.TestedRequirementSummary.ASSESSED;

public class TestedRequirementSummaryDTO {

	private Long id;

	private ASSESSED assessed;

	private String justification;
	
	private PcidssRequirementAdminDTO pcidssRequirementId;

	public TestedRequirementSummaryDTO() {
	}

	public TestedRequirementSummaryDTO(TestedRequirementSummary testedRequirementSummary) {
		this.id = testedRequirementSummary.getId();
		this.assessed = testedRequirementSummary.getAssessed();
		this.justification = testedRequirementSummary.getJustification();
		this.pcidssRequirementId = new PcidssRequirementAdminDTO(testedRequirementSummary.getPcidssRequirementId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ASSESSED getAssessed() {
		return assessed;
	}

	public void setAssessed(ASSESSED assessed) {
		this.assessed = assessed;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public PcidssRequirementAdminDTO getPcidssRequirementId() {
		return pcidssRequirementId;
	}

	public void setPcidssRequirementId(PcidssRequirementAdminDTO pcidssRequirementId) {
		this.pcidssRequirementId = pcidssRequirementId;
	}
}
