package com.nollysoft.service.dto;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class TestedRequirementSummaryPageDTO extends PageImpl<TestedRequirementSummaryDTO> {

	private static final long serialVersionUID = 1L;

	public TestedRequirementSummaryPageDTO(List<TestedRequirementSummaryDTO> content, Pageable pageable, long total) {
		super(content, pageable, total);
	}

}
