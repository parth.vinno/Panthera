package com.nollysoft.service.dto;

import java.util.Set;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.nollysoft.config.Constants;
import com.nollysoft.domain.User;
import com.nollysoft.domain.User.GENDER;
import com.nollysoft.domain.UserGroup;
import com.nollysoft.domain.enumeration.UserRole;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserOutputDTO {

	private Long id;

	@NotBlank
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String login;

	@Size(max = 50)
	private String firstName;

	@Size(max = 50)
	private String lastName;

	private String title;

	private String salutation;

	private GENDER gender;

	private UserRole role;

	private String phone;

	@Email
	@Size(min = 5, max = 100)
	private String email;

	@Size(max = 256)
	private String imageUrl;

	private boolean activated = false;

	@Size(min = 2, max = 5)
	private String langKey;

	private Set<UserGroup> userGroups;

	private String tenantId;

	public UserOutputDTO() {
		// Empty constructor needed for Jackson.
	}

	public UserOutputDTO(User user) {
		this(user.getId(), user.getLogin(), user.getFirstName(), user.getLastName(), user.getTitle(),
				user.getSalutation(), user.getGender(), user.getRoleType(), user.getPhone(), user.getEmail(), user.getActivated(),
				user.getImageUrl(), user.getLangKey(), user.getUserGroups(), user.getTenantId());
		this.setUserGroups(user.getUserGroups());
	}

	public UserOutputDTO(Long id, String login, String firstName, String lastName, String title, String salutation,
			GENDER gender, UserRole role, String phone, String email, boolean activated, String imageUrl, String langKey,
			Set<UserGroup> groups, String tenantId) {

		this.id = id;
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.salutation = salutation;
		this.gender = gender;
		this.role = role;
		this.phone = phone;
		this.email = email;
		this.activated = activated;
		this.imageUrl = imageUrl;
		this.langKey = langKey;
		this.userGroups = groups;
		this.tenantId = tenantId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutaion) {
		this.salutation = salutaion;
	}

	public GENDER getGender() {
		return gender;
	}

	public void setGender(GENDER gender) {
		this.gender = gender;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public boolean isActivated() {
		return activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(Set<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		return "UserDTO{" + "login='" + login + '\'' + ", firstName='" + firstName + '\'' + ", lastName='" + lastName
				+ '\'' + ", email='" + email + '\'' + ", imageUrl='" + imageUrl + '\'' + ", activated=" + activated
				+ ", langKey='" + langKey + "}";
	}

}
