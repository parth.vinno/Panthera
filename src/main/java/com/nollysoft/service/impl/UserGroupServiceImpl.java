package com.nollysoft.service.impl;

import com.nollysoft.service.UserGroupService;
import com.nollysoft.domain.UserGroup;
import com.nollysoft.repository.UserGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserGroup.
 */
@Service
@Transactional
public class UserGroupServiceImpl implements UserGroupService{

    private final Logger log = LoggerFactory.getLogger(UserGroupServiceImpl.class);

    private final UserGroupRepository userGroupRepository;

    public UserGroupServiceImpl(UserGroupRepository userGroupRepository) {
        this.userGroupRepository = userGroupRepository;
    }

    /**
     * Save a userGroup.
     *
     * @param userGroup the entity to save
     * @return the persisted entity
     */
    @Override
    public UserGroup save(UserGroup userGroup) {
        log.debug("Request to save UserGroup : {}", userGroup);
        return userGroupRepository.save(userGroup);
    }

    /**
     *  Get all the userGroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserGroup> findAll(Pageable pageable) {
        log.debug("Request to get all UserGroups");
        return userGroupRepository.findAll(pageable);
    }

    /**
     *  Get all the userGroups except admin.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
	public Page<UserGroup> findAllExceptAdmin(Pageable pageable) {
        log.debug("Request to get all UserGroups");
        return userGroupRepository.findAllByNameNot(pageable, "ADMIN_GROUP");
	};

    /**
     *  Get one userGroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserGroup findOne(Long id) {
        log.debug("Request to get UserGroup : {}", id);
        return userGroupRepository.findOneWithEagerRelationships(id);
    }

    /**
     *  Delete the  userGroup by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserGroup : {}", id);
        userGroupRepository.delete(id);
    }
}
