/**
 * 
 */
package com.nollysoft.tenancy.config;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import com.nollysoft.config.Constants;
import com.nollysoft.config.MessageSource;
import com.nollysoft.tenancy.user.TenantUser;

/**
 * @author bhavinsuhas
 * 
 * This class is used by Hibernate to resolve current tenant.
 */
public class CurrentTenantIdentifyResolverImpl implements CurrentTenantIdentifierResolver{
	
	 private final Logger log = LoggerFactory.getLogger(CurrentTenantIdentifyResolverImpl.class);

	@Override
	public String resolveCurrentTenantIdentifier() {
		if(SecurityContextHolder.getContext().getAuthentication() != null
	            && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null){
	            Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	            if(userDetails instanceof TenantUser){
	                TenantUser tenantUser = ((TenantUser) userDetails);
	                return tenantUser.getTenantId();
	            }
	        }
			// Provide DEFAULT_DATABASE if no user authentication found. 
	        return MessageSource.getMsg(Constants.DEFAULT_DATABASE);
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		// TODO Auto-generated method stub
		return false;
	}

}
