/**
 * 
 */
package com.nollysoft.tenancy.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.hibernate.service.spi.ServiceRegistryAwareService;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nollysoft.config.Constants;
import com.nollysoft.config.MessageSource;

/**
 * @author bhavinsuhas
 * 
 * This class is required by Hibernate to provide the connection to the
 *  context. Fetching the connection from {@link DataSource} and setting
 * the schema based on the Tenant.
 *
 */
public class MultiTenantSchemaConnectionProviderImpl
		implements ServiceRegistryAwareService, MultiTenantConnectionProvider {

	private static final long serialVersionUID = 1L;

	private final Logger log = LoggerFactory.getLogger(MultiTenantSchemaConnectionProviderImpl.class);
    DataSource dataSource;

	/* (non-Javadoc)
	 * @see org.hibernate.service.spi.Wrapped#isUnwrappableAs(java.lang.Class)
	 */
	@Override
	public boolean isUnwrappableAs(Class arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.service.spi.Wrapped#unwrap(java.lang.Class)
	 */
	@Override
	public <T> T unwrap(Class<T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@link DataSource} will provide new connection from configured connection pool.
	 */
	@Override
	public Connection getAnyConnection() throws SQLException {
		return this.dataSource.getConnection();
	}

	/**
	 * Obtain new connection from configured connection pool and switch to the schema based on the TenantId.  
	 */
	@Override
	public Connection getConnection(String tenantId) throws SQLException {
		log.warn("Get Connection for TenantId is:"+tenantId);
        final Connection connection = getAnyConnection();
        try {
            connection.setCatalog(tenantId);
        }
        catch (SQLException e) {
            throw new HibernateException("Could not alter JDBC connection to specified schema [" + tenantId + "]", e);
        }
        return connection;
	}

	/**
	 * Release the connection.
	 */
	@Override
	public void releaseAnyConnection(Connection connection) throws SQLException {
		String defaultDatabase = MessageSource.getMsg(Constants.DEFAULT_DATABASE);
		try {
            connection.createStatement().execute("USE `"+ defaultDatabase +"`;");
        }
        catch (SQLException e) {
            throw new HibernateException("Could not alter JDBC connection to specified schema [public]", e);
        }
        connection.close();

	}

	/* (non-Javadoc)
	 * @see org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider#releaseConnection(java.lang.String, java.sql.Connection)
	 */
	@Override
	public void releaseConnection(String tenantId, Connection connection) throws SQLException {
        connection.close();
	}

	/* (non-Javadoc)
	 * @see org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider#supportsAggressiveRelease()
	 */
	@Override
	public boolean supportsAggressiveRelease() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.service.spi.ServiceRegistryAwareService#injectServices(org.hibernate.service.spi.ServiceRegistryImplementor)
	 */
	@Override
	public void injectServices(ServiceRegistryImplementor serviceRegistry) {
		// Fetch Datasource from the configuration service.
        DataSource localDs =  (DataSource) serviceRegistry.getService(ConfigurationService.class).getSettings().get("hibernate.connection.datasource");
        dataSource = localDs;

	}

}
