/**
 * 
 */
package com.nollysoft.tenancy.user;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author bhavinsuhas
 * 
 *         This class is meant for extending the {@link User} to make aware of
 *         TenantId which will be return by Spring after successful
 *         authentication.
 */
public class TenantUser extends User {

	private String tenantId;

	private static final long serialVersionUID = 1L;

	public TenantUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
