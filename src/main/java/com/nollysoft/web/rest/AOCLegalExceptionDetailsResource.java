package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AOCLegalExceptionDetails;
import com.nollysoft.service.AOCLegalExceptionDetailsService;
import com.nollysoft.service.dto.AOCLegalExceptionDetailsDTO;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AOCLegalExceptionDetails.
 */
@RestController
@RequestMapping("/api")
public class AOCLegalExceptionDetailsResource {

	private final Logger log = LoggerFactory.getLogger(AOCLegalExceptionDetailsResource.class);

	private static final String ENTITY_NAME = "aocLegalExceptionDetails";

	private final AOCLegalExceptionDetailsService aocLegalExceptionDetailsService;

	public AOCLegalExceptionDetailsResource(AOCLegalExceptionDetailsService aocLegalExceptionDetailsService) {
		this.aocLegalExceptionDetailsService = aocLegalExceptionDetailsService;
	}

	/**
	 * POST /aoc-legal-exception-details/service-provider : Create a new
	 * aocLegalExceptionDetails for service provider)
	 *
	 * @param aocLegalExceptionDetails
	 *            the aocLegalExceptionDetails to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new aocLegalExceptionDetails, or with status 400 (Bad Request) if
	 *         the aocLegalExceptionDetails has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/aoc-legal-exception-details/service-provider")
	@Timed
	public ResponseEntity<AOCLegalExceptionDetailsDTO> createAOCLegalExceptionDetailsForServiceProvider(
			@RequestBody AOCLegalExceptionDetails aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to save AOCLegalExceptionDetails for service provider: {}", aocLegalExceptionDetails);
		if (aocLegalExceptionDetails.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new aocLegalExceptionDetails cannot already have an ID")).body(null);
		}
		AOCLegalExceptionDetails result = aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);
		return ResponseEntity.created(new URI("/api/aoc-legal-exception-details/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new AOCLegalExceptionDetailsDTO(result));
	}

	/**
	 * POST /aoc-legal-exception-details : Create a new aocLegalExceptionDetails
	 * for merchant.
	 *
	 * @param aocLegalExceptionDetails
	 *            the aocLegalExceptionDetails to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new aocLegalExceptionDetails, or with status 400 (Bad Request) if
	 *         the aocLegalExceptionDetails has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/aoc-legal-exception-details")
	@Timed
	public ResponseEntity<AOCLegalExceptionDetailsDTO> createAOCLegalExceptionDetailsForMerchant(
			@RequestBody AOCLegalExceptionDetails aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to save AOCLegalExceptionDetails for merchant: {}", aocLegalExceptionDetails);
		if (aocLegalExceptionDetails.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new aocLegalExceptionDetails cannot already have an ID")).body(null);
		}
		AOCLegalExceptionDetails result = aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);
		return ResponseEntity.created(new URI("/api/aoc-legal-exception-details/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new AOCLegalExceptionDetailsDTO(result));
	}

	/**
	 * POST /list/aoc-legal-exception-details/service-provider : Create list
	 * aocLegalExceptionDetails for service provider.
	 *
	 * @param list
	 *            of aocLegalExceptionDetails the aocLegalExceptionDetails to
	 *            create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of aocLegalExceptionDetails
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/list/aoc-legal-exception-details/service-provider")
	@Timed
	public ResponseEntity<List<AOCLegalExceptionDetailsDTO>> createOrUpdateAOCLegalExceptionDetailsForServiceProvider(
			@RequestBody List<AOCLegalExceptionDetails> aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to save AOCLegalExceptionDetails for merchant: {}", aocLegalExceptionDetails);
		List<AOCLegalExceptionDetailsDTO> result = aocLegalExceptionDetailsService
				.saveOrUpdate(aocLegalExceptionDetails);
		return ResponseEntity.created(new URI("/api/list/aoc-legal-exception-details/service-provider"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * POST /list/aoc-legal-exception-details : Create list
	 * aocLegalExceptionDetails for merchant.
	 *
	 * @param list
	 *            of aocLegalExceptionDetails the aocLegalExceptionDetails to
	 *            create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of aocLegalExceptionDetails
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/list/aoc-legal-exception-details")
	@Timed
	public ResponseEntity<List<AOCLegalExceptionDetailsDTO>> createOrUpdateAOCLegalExceptionDetailsForMerchant(
			@RequestBody List<AOCLegalExceptionDetails> aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to save AOCLegalExceptionDetails for merchant: {}", aocLegalExceptionDetails);
		List<AOCLegalExceptionDetailsDTO> result = aocLegalExceptionDetailsService
				.saveOrUpdate(aocLegalExceptionDetails);
		return ResponseEntity.created(new URI("/api/list/aoc-legal-exception-details"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /aoc-legal-exception-details/service-provider : Updates an existing
	 * aocLegalExceptionDetails for service provider.
	 *
	 * @param aocLegalExceptionDetails
	 *            the aocLegalExceptionDetails to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         aocLegalExceptionDetails, or with status 400 (Bad Request) if the
	 *         aocLegalExceptionDetails is not valid, or with status 500
	 *         (Internal Server Error) if the aocLegalExceptionDetails couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/aoc-legal-exception-details/service-provider")
	@Timed
	public ResponseEntity<AOCLegalExceptionDetailsDTO> updateAOCLegalExceptionDetailsForServiceProvider(
			@RequestBody AOCLegalExceptionDetails aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to update AOCLegalExceptionDetails for service provider : {}",
				aocLegalExceptionDetails);
		if (aocLegalExceptionDetails.getId() == null) {
			return createAOCLegalExceptionDetailsForServiceProvider(aocLegalExceptionDetails);
		}

		AOCLegalExceptionDetails result = aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aocLegalExceptionDetails.getId().toString()))
				.body(new AOCLegalExceptionDetailsDTO(result));
	}

	/**
	 * PUT /aoc-legal-exception-details : Updates an existing
	 * aocLegalExceptionDetails for merchant.
	 *
	 * @param aocLegalExceptionDetails
	 *            the aocLegalExceptionDetails to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         aocLegalExceptionDetails, or with status 400 (Bad Request) if the
	 *         aocLegalExceptionDetails is not valid, or with status 500
	 *         (Internal Server Error) if the aocLegalExceptionDetails couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/aoc-legal-exception-details")
	@Timed
	public ResponseEntity<AOCLegalExceptionDetailsDTO> updateAOCLegalExceptionDetailsForMerchant(
			@RequestBody AOCLegalExceptionDetails aocLegalExceptionDetails) throws URISyntaxException {
		log.debug("REST request to update AOCLegalExceptionDetails for merchant : {}", aocLegalExceptionDetails);
		if (aocLegalExceptionDetails.getId() == null) {
			return createAOCLegalExceptionDetailsForMerchant(aocLegalExceptionDetails);
		}
		AOCLegalExceptionDetails result = aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aocLegalExceptionDetails.getId().toString()))
				.body(new AOCLegalExceptionDetailsDTO(result));
	}

	/**
	 * GET /aoc-legal-exception-details/service-provider : get all the
	 * aocLegalExceptionDetails for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         aocLegalExceptionDetailss in body
	 */
	@GetMapping("/aoc-legal-exception-details/service-provider")
	@Timed
	public ResponseEntity<List<AOCLegalExceptionDetailsDTO>> getAllAOCLegalExceptionDetailsesForServiceProvider() {
		log.debug("REST request to get a page of AOCLegalExceptionDetailses for service provider");
		List<AOCLegalExceptionDetailsDTO> list = aocLegalExceptionDetailsService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /aoc-legal-exception-details : get all the aocLegalExceptionDetailses
	 * for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         aocLegalExceptionDetailses in body
	 */
	@GetMapping("/aoc-legal-exception-details")
	@Timed
	public ResponseEntity<List<AOCLegalExceptionDetailsDTO>> getAllAOCLegalExceptionDetailsesForMerchant() {
		log.debug("REST request to get a page of AOCLegalExceptionDetailses for merchant");
		List<AOCLegalExceptionDetailsDTO> list = aocLegalExceptionDetailsService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /aoc-legal-exception-details/service-provider/:id : delete the
	 * "id" aocLegalExceptionDetails for service provider.
	 *
	 * @param id
	 *            the id of the aocLegalExceptionDetails to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/aoc-legal-exception-details/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAOCLegalExceptionDetailsForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete AOCLegalExceptionDetails for service provider : {}", id);
		aocLegalExceptionDetailsService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /aoc-legal-exception-details/:id : delete the "id"
	 * aocLegalExceptionDetails for merchant. (Part 1B))
	 *
	 * @param id
	 *            the id of the aocLegalExceptionDetails to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/aoc-legal-exception-details/{id}")
	@Timed
	public ResponseEntity<Void> deleteAOCLegalExceptionDetailsForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete AOCLegalExceptionDetails for merchant : {}", id);
		aocLegalExceptionDetailsService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
