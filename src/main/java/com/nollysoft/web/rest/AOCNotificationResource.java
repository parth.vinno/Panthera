package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AOCNotification;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;
import com.nollysoft.domain.User;
import com.nollysoft.repository.AssessmentInfoRepository;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.security.SecurityUtils;
import com.nollysoft.service.AOCNotificationService;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AOCNotification.
 */
@RestController
@RequestMapping("/api")
public class AOCNotificationResource {

	private final Logger log = LoggerFactory.getLogger(AOCNotificationResource.class);

	private static final String ENTITY_NAME = "aocNotification";

	private final AOCNotificationService aocNotificationService;

	private final QANotificationService qaNotificationService;

	private final UserRepository userRepository;

	private final MailService mailService;

	private final AssessmentInfoRepository assessmentInfoRepository;

	private final OrganizationRepository organizationRepository;

	public AOCNotificationResource(AOCNotificationService aocNotificationService, UserRepository userRepository,
			MailService mailService, QANotificationService qaNotificationService,
			AssessmentInfoRepository assessmentInfoRepository, OrganizationRepository organizationRepository) {
		this.aocNotificationService = aocNotificationService;
		this.userRepository = userRepository;
		this.mailService = mailService;
		this.qaNotificationService = qaNotificationService;
		this.assessmentInfoRepository = assessmentInfoRepository;
		this.organizationRepository = organizationRepository;
	}

	/**
	 * POST /aoc-notifications : Create a new aocNotification.
	 *
	 * @param aocNotification
	 *            the aocNotification to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new aocNotification, or with status 400 (Bad Request) if the
	 *         aocNotification has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/aoc-notifications")
	@Timed
	public ResponseEntity<AOCNotification> createAOCNotification(@Valid @RequestBody AOCNotification aocNotification)
			throws URISyntaxException {
		log.debug("REST request to save AOCNotification : {}", aocNotification);
		if (aocNotification.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new aocNotification cannot already have an ID")).body(null);
		}
		AOCNotification result = aocNotificationService.save(aocNotification);
		return ResponseEntity.created(new URI("/api/aoc-notifications/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /aoc-notifications : Updates an existing aocNotification.
	 *
	 * @param aocNotification
	 *            the aocNotification to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         aocNotification, or with status 400 (Bad Request) if the
	 *         aocNotification is not valid, or with status 500 (Internal Server
	 *         Error) if the aocNotification couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/aoc-notifications")
	@Timed
	public ResponseEntity<AOCNotification> updateAOCNotification(@Valid @RequestBody AOCNotification aocNotification)
			throws URISyntaxException {
		log.debug("REST request to update AOCNotification : {}", aocNotification);
		if (aocNotification.getId() == null) {
			return createAOCNotification(aocNotification);
		}
		AOCNotification result = aocNotificationService.save(aocNotification);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aocNotification.getId().toString()))
				.body(result);
	}

	/**
	 * GET /aoc-notifications : get all the aocNotifications.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         aocNotifications in body
	 */
	@GetMapping("/aoc-notifications")
	@Timed
	public List<AOCNotification> getAllAOCNotifications() {
		log.debug("REST request to get all AOCNotifications");
		return aocNotificationService.findAll();
	}

	/**
	 * GET /aoc-notifications/:id : get the "id" aocNotification.
	 *
	 * @param id
	 *            the id of the aocNotification to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         aocNotification, or with status 404 (Not Found)
	 */
	@GetMapping("/aoc-notifications/{id}")
	@Timed
	public ResponseEntity<AOCNotification> getAOCNotification(@PathVariable Long id) {
		log.debug("REST request to get AOCNotification : {}", id);
		AOCNotification aocNotification = aocNotificationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(aocNotification));
	}

	/**
	 * DELETE /aoc-notifications/:id : delete the "id" aocNotification.
	 *
	 * @param id
	 *            the id of the aocNotification to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/aoc-notifications/{id}")
	@Timed
	public ResponseEntity<Void> deleteAOCNotification(@PathVariable Long id) {
		log.debug("REST request to delete AOCNotification : {}", id);
		aocNotificationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /aoc-notifications/send-notification/service-provider : send email notification to QA
	 * for service provider.
	 *
	 * @param notification
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new qANotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/aoc-notifications/send-notification/service-provider")
	@Timed
	public ResponseEntity<List<AOCNotification>> sendNotificationForServiceProvider(
			@RequestBody List<User> aocUsers) throws URISyntaxException {
		List<AOCNotification> result = new ArrayList<>();
		Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		aocUsers.stream().forEach(aocUser -> {
			result.add(aocNotificationService.create(aocUser, user.get()));
		});
		qaNotificationService.updateStatus(NOTIFICATION_STATUS.SENT_TO_AOC);
		LocalDate assessmentDate = assessmentInfoRepository.findAll().get(0).getAssessmentEndDate();
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToAOC(user.get(), aocUsers, assessmentDate, companyName);
		return ResponseEntity.created(new URI("/api/aoc-notifications/send-notification/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}
	
	/**
	 * POST /aoc-notifications/send-notification : send email notification to QA
	 * for merchant.
	 *
	 * @param notification
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new qANotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/aoc-notifications/send-notification")
	@Timed
	public ResponseEntity<List<AOCNotification>> sendNotificationForMerchant(
			@RequestBody List<User> aocUsers) throws URISyntaxException {
		List<AOCNotification> result = new ArrayList<>();
		Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		aocUsers.stream().forEach(aocUser -> {
			result.add(aocNotificationService.create(aocUser, user.get()));
		});
		qaNotificationService.updateStatus(NOTIFICATION_STATUS.SENT_TO_AOC);
		LocalDate assessmentDate = assessmentInfoRepository.findAll().get(0).getAssessmentEndDate();
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToAOC(user.get(), aocUsers, assessmentDate, companyName);
		return ResponseEntity.created(new URI("/api/aoc-notifications/send-notification/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}
}
