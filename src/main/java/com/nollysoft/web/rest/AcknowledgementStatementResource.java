package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AcknowledgementStatement;
import com.nollysoft.service.AcknowledgementStatementService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AcknowledgementStatement.
 */
@RestController
@RequestMapping("/api")
public class AcknowledgementStatementResource {
	private final Logger log = LoggerFactory.getLogger(AcknowledgementStatementResource.class);

	private static final String ENTITY_NAME = "acknowledgementStatement";

	private final AcknowledgementStatementService acknowledgementStatementService;

	public AcknowledgementStatementResource(AcknowledgementStatementService acknowledgementStatementService) {
		this.acknowledgementStatementService = acknowledgementStatementService;
	}

	/**
	 * POST /acknowledgement-statements/service-provider : Create a new
	 * acknowledgementStatement for service provider)
	 *
	 * @param acknowledgementStatement
	 *            the acknowledgementStatement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         acknowledgementStatement, or with status 400 (Bad Request) if the
	 *         acknowledgementStatement has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/acknowledgement-statements/service-provider")
	@Timed
	public ResponseEntity<AcknowledgementStatement> createAcknowledgementStatementForServiceProvider(
			@RequestBody AcknowledgementStatement acknowledgementStatement) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatement for service provider: {}", acknowledgementStatement);
		if (acknowledgementStatement.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new acknowledgementStatement cannot already have an ID")).body(null);
		}
		AcknowledgementStatement result = acknowledgementStatementService.save(acknowledgementStatement);
		return ResponseEntity.created(new URI("/api/acknowledgement-statements/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /acknowledgement-statements : Create a new acknowledgementStatement for
	 * merchant.
	 *
	 * @param acknowledgementStatement
	 *            the acknowledgementStatement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         acknowledgementStatement, or with status 400 (Bad Request) if the
	 *         acknowledgementStatement has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/acknowledgement-statements")
	@Timed
	public ResponseEntity<AcknowledgementStatement> createAcknowledgementStatementForMerchant(
			@RequestBody AcknowledgementStatement acknowledgementStatement) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatement for merchant: {}", acknowledgementStatement);
		if (acknowledgementStatement.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new acknowledgementStatement cannot already have an ID")).body(null);
		}
		AcknowledgementStatement result = acknowledgementStatementService.save(acknowledgementStatement);
		return ResponseEntity.created(new URI("/api/acknowledgement-statements/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /acknowledgement-statements/service-provider : Updates an existing
	 * acknowledgementStatement for service provider.
	 *
	 * @param acknowledgementStatement
	 *            the acknowledgementStatement to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         acknowledgementStatement, or with status 400 (Bad Request) if the
	 *         acknowledgementStatement is not valid, or with status 500 (Internal
	 *         Server Error) if the acknowledgementStatement couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/acknowledgement-statements/service-provider")
	@Timed
	public ResponseEntity<AcknowledgementStatement> updateAcknowledgementStatementForServiceProvider(
			@RequestBody AcknowledgementStatement acknowledgementStatement) throws URISyntaxException {
		log.debug("REST request to update AcknowledgementStatement for service provider : {}",
				acknowledgementStatement);
		if (acknowledgementStatement.getId() == null) {
			return createAcknowledgementStatementForServiceProvider(acknowledgementStatement);
		}

		AcknowledgementStatement result = acknowledgementStatementService.save(acknowledgementStatement);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, acknowledgementStatement.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /acknowledgement-statements : Updates an existing
	 * acknowledgementStatement for merchant.
	 *
	 * @param acknowledgementStatement
	 *            the acknowledgementStatement to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         acknowledgementStatement, or with status 400 (Bad Request) if the
	 *         acknowledgementStatement is not valid, or with status 500 (Internal
	 *         Server Error) if the acknowledgementStatement couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/acknowledgement-statements")
	@Timed
	public ResponseEntity<AcknowledgementStatement> updateAcknowledgementStatementForMerchant(
			@RequestBody AcknowledgementStatement acknowledgementStatement) throws URISyntaxException {
		log.debug("REST request to update AcknowledgementStatement for merchant : {}", acknowledgementStatement);
		if (acknowledgementStatement.getId() == null) {
			return createAcknowledgementStatementForMerchant(acknowledgementStatement);
		}
		AcknowledgementStatement result = acknowledgementStatementService.save(acknowledgementStatement);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, acknowledgementStatement.getId().toString()))
				.body(result);
	}

	/**
	 * GET /acknowledgement-statements/service-provider : get all the
	 * acknowledgementStatement for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         acknowledgementStatements in body
	 */
	@GetMapping("/acknowledgement-statements/service-provider")
	@Timed
	public ResponseEntity<List<AcknowledgementStatement>> getAllAcknowledgementStatementesForServiceProvider() {
		log.debug("REST request to get a page of AcknowledgementStatementes for service provider");
		List<AcknowledgementStatement> list = acknowledgementStatementService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /acknowledgement-statements : get all the acknowledgementStatementes for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         acknowledgementStatementes in body
	 */
	@GetMapping("/acknowledgement-statements")
	@Timed
	public ResponseEntity<List<AcknowledgementStatement>> getAllAcknowledgementStatementesForMerchant() {
		log.debug("REST request to get a page of AcknowledgementStatementes for merchant");
		List<AcknowledgementStatement> list = acknowledgementStatementService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /acknowledgement-statements/service-provider/:id : delete the "id"
	 * acknowledgementStatement for service provider.
	 *
	 * @param id
	 *            the id of the acknowledgementStatement to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/acknowledgement-statements/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAcknowledgementStatementForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete AcknowledgementStatement for service provider : {}", id);
		acknowledgementStatementService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /acknowledgement-statements/:id : delete the "id"
	 * acknowledgementStatement for merchant.
	 * 
	 * @param id
	 *            the id of the acknowledgementStatement to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/acknowledgement-statements/{id}")
	@Timed
	public ResponseEntity<Void> deleteAcknowledgementStatementForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete AcknowledgementStatement for merchant : {}", id);
		acknowledgementStatementService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
