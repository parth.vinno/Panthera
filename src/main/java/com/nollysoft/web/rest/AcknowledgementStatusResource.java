package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AcknowledgementStatus;
import com.nollysoft.service.AcknowledgementStatusService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AcknowledgementStatus.
 */
@RestController
@RequestMapping("/api")
public class AcknowledgementStatusResource {
	private final Logger log = LoggerFactory.getLogger(AcknowledgementStatusResource.class);

	private static final String ENTITY_NAME = "acknowledgementStatus";

	private final AcknowledgementStatusService acknowledgementStatusService;

	public AcknowledgementStatusResource(AcknowledgementStatusService acknowledgementStatusService) {
		this.acknowledgementStatusService = acknowledgementStatusService;
	}

	/**
	 * POST /acknowledgement-statuses/service-provider : Create a new
	 * acknowledgementStatus for service provider)
	 *
	 * @param acknowledgementStatus
	 *            the acknowledgementStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         acknowledgementStatus, or with status 400 (Bad Request) if the
	 *         acknowledgementStatus has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/acknowledgement-statuses/service-provider")
	@Timed
	public ResponseEntity<AcknowledgementStatus> createAcknowledgementStatusForServiceProvider(
			@RequestBody AcknowledgementStatus acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatus for service provider: {}", acknowledgementStatus);
		if (acknowledgementStatus.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new acknowledgementStatus cannot already have an ID")).body(null);
		}
		AcknowledgementStatus result = acknowledgementStatusService.save(acknowledgementStatus);
		return ResponseEntity.created(new URI("/api/acknowledgement-statuses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /acknowledgement-statuses : Create a new acknowledgementStatus for
	 * merchant.
	 *
	 * @param acknowledgementStatus
	 *            the acknowledgementStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         acknowledgementStatus, or with status 400 (Bad Request) if the
	 *         acknowledgementStatus has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/acknowledgement-statuses")
	@Timed
	public ResponseEntity<AcknowledgementStatus> createAcknowledgementStatusForMerchant(
			@RequestBody AcknowledgementStatus acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatus for merchant: {}", acknowledgementStatus);
		if (acknowledgementStatus.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new acknowledgementStatus cannot already have an ID")).body(null);
		}
		AcknowledgementStatus result = acknowledgementStatusService.save(acknowledgementStatus);
		return ResponseEntity.created(new URI("/api/acknowledgement-statuses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /list/acknowledgement-statuses/service-provider : Create a new list of
	 * acknowledgementStatus for merchant.
	 *
	 * @param list
	 *            of acknowledgementStatus the acknowledgementStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         list of acknowledgementStatus
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/list/acknowledgement-statuses/service-provider")
	@Timed
	public ResponseEntity<List<AcknowledgementStatus>> createOrUpdateAcknowledgementStatusForServiceProvider(
			@RequestBody List<AcknowledgementStatus> acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatus for merchant: {}", acknowledgementStatus);
		List<AcknowledgementStatus> result = acknowledgementStatusService.saveOrUpdate(acknowledgementStatus);
		return ResponseEntity.created(new URI("/api/list/acknowledgement-statuses/service-provider"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}
	
	/**
	 * POST /list/acknowledgement-statuses : Create a new list of
	 * acknowledgementStatus for merchant.
	 *
	 * @param list
	 *            of acknowledgementStatus the acknowledgementStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         list of acknowledgementStatus
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/list/acknowledgement-statuses")
	@Timed
	public ResponseEntity<List<AcknowledgementStatus>> createOrUpdateAcknowledgementStatusForMerchant(
			@RequestBody List<AcknowledgementStatus> acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to save AcknowledgementStatus for merchant: {}", acknowledgementStatus);
		List<AcknowledgementStatus> result = acknowledgementStatusService.saveOrUpdate(acknowledgementStatus);
		return ResponseEntity.created(new URI("/api/list/acknowledgement-statuses"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /acknowledgement-statuses/service-provider : Updates an existing
	 * acknowledgementStatus for service provider.
	 *
	 * @param acknowledgementStatus
	 *            the acknowledgementStatus to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         acknowledgementStatus, or with status 400 (Bad Request) if the
	 *         acknowledgementStatus is not valid, or with status 500 (Internal
	 *         Server Error) if the acknowledgementStatus couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/acknowledgement-statuses/service-provider")
	@Timed
	public ResponseEntity<AcknowledgementStatus> updateAcknowledgementStatusForServiceProvider(
			@RequestBody AcknowledgementStatus acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to update AcknowledgementStatus for service provider : {}", acknowledgementStatus);
		if (acknowledgementStatus.getId() == null) {
			return createAcknowledgementStatusForServiceProvider(acknowledgementStatus);
		}
		AcknowledgementStatus result = acknowledgementStatusService.save(acknowledgementStatus);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, acknowledgementStatus.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /acknowledgement-statuses : Updates an existing acknowledgementStatus for
	 * merchant.
	 *
	 * @param acknowledgementStatus
	 *            the acknowledgementStatus to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         acknowledgementStatus, or with status 400 (Bad Request) if the
	 *         acknowledgementStatus is not valid, or with status 500 (Internal
	 *         Server Error) if the acknowledgementStatus couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/acknowledgement-statuses")
	@Timed
	public ResponseEntity<AcknowledgementStatus> updateAcknowledgementStatusForMerchant(
			@RequestBody AcknowledgementStatus acknowledgementStatus) throws URISyntaxException {
		log.debug("REST request to update AcknowledgementStatus for merchant : {}", acknowledgementStatus);
		if (acknowledgementStatus.getId() == null) {
			return createAcknowledgementStatusForMerchant(acknowledgementStatus);
		}
		AcknowledgementStatus result = acknowledgementStatusService.save(acknowledgementStatus);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, acknowledgementStatus.getId().toString()))
				.body(result);
	}

	/**
	 * GET /acknowledgement-statuses/service-provider : get all the
	 * acknowledgementStatus for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         acknowledgementStatuss in body
	 */
	@GetMapping("/acknowledgement-statuses/service-provider")
	@Timed
	public ResponseEntity<List<AcknowledgementStatus>> getAllAcknowledgementStatusesForServiceProvider() {
		log.debug("REST request to get a page of AcknowledgementStatuses for service provider");
		List<AcknowledgementStatus> list = acknowledgementStatusService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /acknowledgement-statuses : get all the acknowledgementStatuses for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         acknowledgementStatuses in body
	 */
	@GetMapping("/acknowledgement-statuses")
	@Timed
	public ResponseEntity<List<AcknowledgementStatus>> getAllAcknowledgementStatusesForMerchant() {
		log.debug("REST request to get a page of AcknowledgementStatuses for merchant");
		List<AcknowledgementStatus> list = acknowledgementStatusService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /acknowledgement-statuses/service-provider/:id : delete the "id"
	 * acknowledgementStatus for service provider.
	 *
	 * @param id
	 *            the id of the acknowledgementStatus to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/acknowledgement-statuses/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAcknowledgementStatusForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete AcknowledgementStatus for service provider : {}", id);
		acknowledgementStatusService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /acknowledgement-statuses/:id : delete the "id" acknowledgementStatus
	 * for merchant.
	 * 
	 * @param id
	 *            the id of the acknowledgementStatus to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/acknowledgement-statuses/{id}")
	@Timed
	public ResponseEntity<Void> deleteAcknowledgementStatusForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete AcknowledgementStatus for merchant : {}", id);
		acknowledgementStatusService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
