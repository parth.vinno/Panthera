package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.Address;
import com.nollysoft.service.AddressService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Address.
 */
@RestController
@RequestMapping("/api")
public class AddressResource {

    private final Logger log = LoggerFactory.getLogger(AddressResource.class);

    private static final String ENTITY_NAME = "address";

    private final AddressService addressService;

    public AddressResource(AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * POST  /addresses/service-provider : Create a new address for service provider. Don't use this service directly (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param address the address to create
     * @return the ResponseEntity with status 201 (Created) and with body the new address, or with status 400 (Bad Request) if the address has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/addresses/service-provider")
    @Timed
    public ResponseEntity<Address> createAddressForServiceProvider(@RequestBody Address address) throws URISyntaxException {
        log.debug("REST request to save Address for service provider: {}", address);
        if (address.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new address cannot already have an ID")).body(null);
        }
        Address result = addressService.save(address);
        return ResponseEntity.created(new URI("/api/addresses/service-provider/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * POST  /addresses : Create a new address for merchant. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param address the address to create
     * @return the ResponseEntity with status 201 (Created) and with body the new address, or with status 400 (Bad Request) if the address has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/addresses")
    @Timed
    public ResponseEntity<Address> createAddressForMerchant(@RequestBody Address address) throws URISyntaxException {
        log.debug("REST request to save Address for merchant: {}", address);
        if (address.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new address cannot already have an ID")).body(null);
        }
        Address result = addressService.save(address);
        return ResponseEntity.created(new URI("/api/addresses/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /addresses/service-provider : Updates an existing address for service provider. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param address the address to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated address,
     * or with status 400 (Bad Request) if the address is not valid,
     * or with status 500 (Internal Server Error) if the address couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/addresses/service-provider")
    @Timed
    public ResponseEntity<Address> updateAddressForServiceProvider(@RequestBody Address address) throws URISyntaxException {
        log.debug("REST request to update Address for service provider : {}", address);
        if (address.getId() == null) {
            return createAddressForServiceProvider(address);
        }

        Address result = addressService.save(address);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, address.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /addresses : Updates an existing address for merchant. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param address the address to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated address,
     * or with status 400 (Bad Request) if the address is not valid,
     * or with status 500 (Internal Server Error) if the address couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/addresses")
    @Timed
    public ResponseEntity<Address> updateAddressForMerchant(@RequestBody Address address) throws URISyntaxException {
        log.debug("REST request to update Address for merchant : {}", address);
        if (address.getId() == null) {
            return createAddressForMerchant(address);
        }
        Address result = addressService.save(address);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, address.getId().toString()))
                .body(result);
    }

    /**
     * GET  /addresses/service-provider : get all the addresses for service provider. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of addresses in body
     */
    @GetMapping("/addresses/service-provider")
    @Timed
    public ResponseEntity<List<Address>> getAllAddressesForServiceProvider(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Addresses for service provider");
        Page<Address> page = addressService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addresses/service-provider");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /addresses : get all the addresses for merchant. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of addresses in body
     */
    @GetMapping("/addresses")
    @Timed
    public ResponseEntity<List<Address>> getAllAddressesForMerchant(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Addresses for merchant");
        Page<Address> page = addressService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addresses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /addresses/service-provider/:id : get the "id" address for service provider. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param id the id of the address to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the address, or with status 404 (Not Found)
     */
    @GetMapping("/addresses/service-provider/{id}")
    @Timed
    public ResponseEntity<Address> getAddressForServiceProvider(@PathVariable Long id) {
        log.debug("REST request to get Address for service provider: {}", id);
        DbContextHolder.setDbType(DbType.dbserviceprovider);
        Address address = addressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(address));

    }

    /**
     * GET  /addresses/:id : get the "id" address for merchant. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param id the id of the address to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the address, or with status 404 (Not Found)
     */
    @GetMapping("/addresses/{id}")
    @Timed
    public ResponseEntity<Address> getAddressForMerchant(@PathVariable Long id) {
        log.debug("REST request to get Address for merchant : {}", id);
        DbContextHolder.setDbType(DbType.dbmerchant);
        Address address = addressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(address));

    }

    /**
     * DELETE  /addresses/service-provider/:id : delete the "id" address for service provider. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param id the id of the address to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/addresses/service-provider/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddressForServiceProvider(@PathVariable Long id) {
        log.debug("REST request to delete Address for service provider : {}", id);
        addressService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * DELETE  /addresses/:id : delete the "id" address for merchant. (use service from Organization Resource (Part 1A) or Accessor Company Resource (Part 1B))
     *
     * @param id the id of the address to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/addresses/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddressForMerchant(@PathVariable Long id) {
        log.debug("REST request to delete Address for merchant : {}", id);
        addressService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
