package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AssessmentInfo;
import com.nollysoft.service.AssessmentInfoService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AssessmentInfo.
 */
@RestController
@RequestMapping("/api")
public class AssessmentInfoResource {

	private final Logger log = LoggerFactory
			.getLogger(AssessmentInfoResource.class);

	private static final String ENTITY_NAME = "assessmentInfo";

	private final AssessmentInfoService assessmentInfoService;

	public AssessmentInfoResource(AssessmentInfoService assessmentInfoService) {
		this.assessmentInfoService = assessmentInfoService;
	}

	/**
	 * POST /assessment-infos/service-provider : Create a new assessmentInfo.
	 *
	 * @param assessmentInfo
	 *            the assessmentInfo to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new assessmentInfo, or with status 400 (Bad Request) if the
	 *         assessmentInfo has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/assessment-infos/service-provider")
	@Timed
	public ResponseEntity<AssessmentInfo> createAssessmentInfoForServiceProvider(
			@RequestBody AssessmentInfo assessmentInfo)
			throws URISyntaxException {
		log.debug("REST request to save AssessmentInfo : {}", assessmentInfo);
		if (assessmentInfo.getId() != null) {
			return ResponseEntity
					.badRequest()
					.headers(
							HeaderUtil
									.createFailureAlert(ENTITY_NAME,
											"idexists",
											"A new AssessmentInfo cannot already have an ID"))
					.body(null);
		}
		AssessmentInfo result = assessmentInfoService.save(assessmentInfo);
		return ResponseEntity
				.created(
						new URI("/api/assessment-infos/service-provider/"
								+ result.getId()))
				.headers(
						HeaderUtil.createEntityCreationAlert(ENTITY_NAME,
								result.getId().toString())).body(result);
	}

	/**
	 * POST /assessment-infos : Create a new assessmentInfo for merchant.
	 *
	 * @param assessmentInfo
	 *            the assessmentInfo to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new assessmentInfo, or with status 400 (Bad Request) if the
	 *         assessmentInfo has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/assessment-infos")
	@Timed
	public ResponseEntity<AssessmentInfo> createAssessmentInfoForMerchant(
			@RequestBody AssessmentInfo assessmentInfo)
			throws URISyntaxException {
		log.debug("REST request to save AssessmentInfo : {}", assessmentInfo);
		if (assessmentInfo.getId() != null) {
			return ResponseEntity
					.badRequest()
					.headers(
							HeaderUtil
									.createFailureAlert(ENTITY_NAME,
											"idexists",
											"A new AssessmentInfo cannot already have an ID"))
					.body(null);
		}
		AssessmentInfo result = assessmentInfoService.save(assessmentInfo);
		return ResponseEntity
				.created(new URI("/api/assessment-infos/" + result.getId()))
				.headers(
						HeaderUtil.createEntityCreationAlert(ENTITY_NAME,
								result.getId().toString())).body(result);
	}

	/**
	 * POST /assessment-infos/assessment/start : Create a new assessmentInfo.
	 *
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         assessmentInfo, or body of
	 *         assessmentInfo if already created
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/assessment-infos/assessment/start")
	@Timed
	public ResponseEntity<AssessmentInfo> createAssessmentStartInfo() throws URISyntaxException {
		log.debug("REST request to save AssessmentInfo : {}");
		AssessmentInfo result = assessmentInfoService.saveAssessmentStartInfo();
		return ResponseEntity.created(new URI("/api/assessment-infos/assessment/start" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /assessment-infos/assessment/end : update assessmentInfo.
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessmentInfo, or with status 400 (Bad Request) if the
	 *         assessmentInfo is not valid, or with status 500 (Internal Server
	 *         Error) if the assessmentInfo couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PutMapping("/assessment-infos/assessment/end")
	@Timed
	public ResponseEntity<AssessmentInfo> createAssessmentEndInfo() throws URISyntaxException {
		log.debug("REST request to update AssessmentInfo : {}");
		AssessmentInfo result = assessmentInfoService.saveAssessmentEndInfo();

		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /assessment-infos/service-provider : Updates an existing
	 * assessmentInfo for service provider.
	 *
	 * @param assessmentInfo
	 *            the assessmentInfo to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessmentInfo, or with status 400 (Bad Request) if the
	 *         assessmentInfo is not valid, or with status 500 (Internal Server
	 *         Error) if the assessmentInfo couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/assessment-infos/service-provider")
	@Timed
	public ResponseEntity<AssessmentInfo> updateAssessmentInfo(
			@RequestBody AssessmentInfo assessmentInfo)
			throws URISyntaxException {
		log.debug("REST request to update AssessmentInfo : {}", assessmentInfo);
		if (assessmentInfo.getId() == null) {
			return createAssessmentInfoForServiceProvider(assessmentInfo);
		}
		AssessmentInfo result = assessmentInfoService.save(assessmentInfo);
		return ResponseEntity
				.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,
								assessmentInfo.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /assessment-infos : Updates an existing assessmentInfo for merchant.
	 *
	 * @param assessmentInfo
	 *            the assessmentInfo to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessmentInfo, or with status 400 (Bad Request) if the
	 *         assessmentInfo is not valid, or with status 500 (Internal Server
	 *         Error) if the assessmentInfo couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/assessment-infos")
	@Timed
	public ResponseEntity<AssessmentInfo> updateAssessmentInfoForMerchant(
			@RequestBody AssessmentInfo assessmentInfo)
			throws URISyntaxException {
		log.debug("REST request to update AssessmentInfo : {}", assessmentInfo);
		if (assessmentInfo.getId() == null) {
			return createAssessmentInfoForMerchant(assessmentInfo);
		}
		AssessmentInfo result = assessmentInfoService.save(assessmentInfo);
		return ResponseEntity
				.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,
								assessmentInfo.getId().toString()))
				.body(result);
	}

	/**
	 * GET /assessment-infos/service-provider : get all the assessmentInfos for
	 * service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         assessmentInfos in body
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/assessment-infos/service-provider")
	@Timed
	public List<AssessmentInfo> getAllAssessmentInfosForServiceProvider() {
		log.debug("REST request to get all AssessmentInfos");
		return assessmentInfoService.findAll();
	}

	/**
	 * GET /assessment-infos : get all the assessmentInfos for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         assessmentInfos in body
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/assessment-infos")
	@Timed
	public List<AssessmentInfo> getAllAssessmentInfosFroMerchant() {
		log.debug("REST request to get all AssessmentInfos");
		return assessmentInfoService.findAll();
	}

	/**
	 * GET /assessment-infos/service-provider/:id : get the "id" assessmentInfo
	 * for service provider.
	 *
	 * @param id
	 *            the id of the assessmentInfo to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         assessmentInfo, or with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/assessment-infos/service-provider/{id}")
	@Timed
	public ResponseEntity<AssessmentInfo> getAssessmentInfoForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get AssessmentInfo : {}", id);
		AssessmentInfo assessmentInfo = assessmentInfoService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(assessmentInfo));
	}

	/**
	 * GET /assessment-infos/:id : get the "id" assessmentInfo for merchant.
	 *
	 * @param id
	 *            the id of the assessmentInfo to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         assessmentInfo, or with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/assessment-infos/{id}")
	@Timed
	public ResponseEntity<AssessmentInfo> getAssessmentInfoForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get AssessmentInfo : {}", id);
		AssessmentInfo assessmentInfo = assessmentInfoService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(assessmentInfo));
	}

	/**
	 * DELETE /assessment-infos/:id : delete the "id" assessmentInfo for service
	 * provider.
	 *
	 * @param id
	 *            the id of the assessmentInfo to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/assessment-infos/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAssessmentInfoForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to delete AssessmentInfo : {}", id);
		assessmentInfoService.delete(id);
		return ResponseEntity
				.ok()
				.headers(
						HeaderUtil.createEntityDeletionAlert(ENTITY_NAME,
								id.toString())).build();
	}

	/**
	 * DELETE /assessment-infos/:id : delete the "id" assessmentInfo for
	 * merchant.
	 *
	 * @param id
	 *            the id of the assessmentInfo to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/assessment-infos/{id}")
	@Timed
	public ResponseEntity<Void> deleteAssessmentInfoForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to delete AssessmentInfo : {}", id);
		assessmentInfoService.delete(id);
		return ResponseEntity
				.ok()
				.headers(
						HeaderUtil.createEntityDeletionAlert(ENTITY_NAME,
								id.toString())).build();
	}
}
