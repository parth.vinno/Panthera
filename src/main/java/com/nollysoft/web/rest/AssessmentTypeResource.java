package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AssessmentType;
import com.nollysoft.service.AssessmentTypeService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AssessmentType.
 */
@RestController
@RequestMapping("/api")
public class AssessmentTypeResource {

	private final Logger log = LoggerFactory.getLogger(AssessmentTypeResource.class);

	private static final String ENTITY_NAME = "assessmentType";

	private final AssessmentTypeService assessmentTypeService;

	public AssessmentTypeResource(AssessmentTypeService assessmentTypeService) {
		this.assessmentTypeService = assessmentTypeService;
	}

	/**
	 * POST /assessment-types : Create a new assessmentType.
	 *
	 * @param assessmentType
	 *            the assessmentType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         assessmentType, or with status 400 (Bad Request) if the
	 *         assessmentType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/assessment-types")
	@Timed
	public ResponseEntity<AssessmentType> createAssessmentType(@Valid @RequestBody AssessmentType assessmentType)
			throws URISyntaxException {
		log.debug("REST request to save AssessmentType : {}", assessmentType);
		if (assessmentType.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new assessmentType cannot already have an ID")).body(null);
		}
		AssessmentType result = assessmentTypeService.save(assessmentType);
		return ResponseEntity.created(new URI("/api/assessment-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /assessment-types : Updates an existing assessmentType.
	 *
	 * @param assessmentType
	 *            the assessmentType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessmentType, or with status 400 (Bad Request) if the
	 *         assessmentType is not valid, or with status 500 (Internal Server
	 *         Error) if the assessmentType couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/assessment-types")
	@Timed
	public ResponseEntity<AssessmentType> updateAssessmentType(@Valid @RequestBody AssessmentType assessmentType)
			throws URISyntaxException {
		log.debug("REST request to update AssessmentType : {}", assessmentType);
		if (assessmentType.getId() == null) {
			return createAssessmentType(assessmentType);
		}
		AssessmentType result = assessmentTypeService.save(assessmentType);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, assessmentType.getId().toString()))
				.body(result);
	}

	/**
	 * GET /assessment-types : get all the assessmentTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         assessmentTypes in body
	 */
	@GetMapping("/assessment-types")
	@Timed
	public ResponseEntity<List<AssessmentType>> getAllAssessmentTypes(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of AssessmentTypes");
		Page<AssessmentType> page = assessmentTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assessment-types");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /assessment-types/:id : get the "id" assessmentType.
	 *
	 * @param id
	 *            the id of the assessmentType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         assessmentType, or with status 404 (Not Found)
	 */
	@GetMapping("/assessment-types/{id}")
	@Timed
	public ResponseEntity<AssessmentType> getAssessmentType(@PathVariable Long id) {
		log.debug("REST request to get AssessmentType : {}", id);
		AssessmentType assessmentType = assessmentTypeService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(assessmentType));
	}

	/**
	 * DELETE /assessment-types/:id : delete the "id" assessmentType.
	 *
	 * @param id
	 *            the id of the assessmentType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/assessment-types/{id}")
	@Timed
	public ResponseEntity<Void> deleteAssessmentType(@PathVariable Long id) {
		log.debug("REST request to delete AssessmentType : {}", id);
		assessmentTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
