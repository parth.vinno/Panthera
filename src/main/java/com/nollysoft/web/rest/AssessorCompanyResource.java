package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.AssessorCompany;
import com.nollysoft.domain.enumeration.OrganizationSection;
import com.nollysoft.service.AssessorCompanyService;
import com.nollysoft.service.OrganizationSummaryStatusService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AssessorCompany.
 */
@RestController
@RequestMapping("/api")
public class AssessorCompanyResource {

	private final Logger log = LoggerFactory.getLogger(AssessorCompanyResource.class);

	private static final String ENTITY_NAME = "assessorCompany";

	private final AssessorCompanyService assessorCompanyService;
	private final OrganizationSummaryStatusService organizationSummaryStatusService;

	public AssessorCompanyResource(AssessorCompanyService assessorCompanyService, OrganizationSummaryStatusService organizationSummaryStatusService) {
		this.assessorCompanyService = assessorCompanyService;
		this.organizationSummaryStatusService = organizationSummaryStatusService;
	}

	/**
	 * POST /assessor-companies/service-provider : Create a new assessorCompany
	 * for service provider. This is for module part 1B, Use this if only
	 * applicable
	 *
	 * @param assessorCompany
	 *            the assessorCompany to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new assessorCompany, or with status 400 (Bad Request) if the
	 *         assessorCompany has already an ID, requires authorization or else
	 *         return with status 401 (Un-Authorized)
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/assessor-companies/service-provider")
	@Timed
	public ResponseEntity<AssessorCompany> createAssessorCompanyForServiceProvider(
			@RequestBody AssessorCompany assessorCompany) throws URISyntaxException {
		log.debug("REST request to save AssessorCompany for service provider : {}", assessorCompany);
		if (assessorCompany.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new assessorCompany cannot already have an ID")).body(null);
		}
		AssessorCompany result = assessorCompanyService.save(assessorCompany);
		organizationSummaryStatusService.createOrUpdate(OrganizationSection.ASSESSOR);
		return ResponseEntity.created(new URI("/api/assessor-companies/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /assessor-companies : Create a new assessorCompany for merchant.
	 * This is for module part 1B, Use this if only applicable
	 *
	 * @param assessorCompany
	 *            the assessorCompany to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new assessorCompany, or with status 400 (Bad Request) if the
	 *         assessorCompany has already an ID, requires authorization or else
	 *         return with status 401 (Un-Authorized)
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/assessor-companies")
	@Timed
	public ResponseEntity<AssessorCompany> createAssessorCompanyForMerchant(
			@RequestBody AssessorCompany assessorCompany) throws URISyntaxException {
		log.debug("REST request to save AssessorCompany for merchant : {}", assessorCompany);
		if (assessorCompany.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new assessorCompany cannot already have an ID")).body(null);
		}
		AssessorCompany result = assessorCompanyService.save(assessorCompany);
		organizationSummaryStatusService.createOrUpdate(OrganizationSection.ASSESSOR);
		return ResponseEntity.created(new URI("/api/assessor-companies/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /assessor-companies/service-provider : Updates an existing
	 * assessorCompany for service provider. This is for module part 1B, Use
	 * this if only applicable
	 *
	 * @param assessorCompany
	 *            the assessorCompany to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessorCompany, or with status 400 (Bad Request) if the
	 *         assessorCompany is not valid, or with status 401 (Un-Authorized)
	 *         if un-authorized, or with status 500 (Internal Server Error) if
	 *         the assessorCompany couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/assessor-companies/service-provider")
	@Timed
	public ResponseEntity<AssessorCompany> updateAssessorCompanyForServiceProvider(
			@RequestBody AssessorCompany assessorCompany) throws URISyntaxException {
		log.debug("REST request to update AssessorCompany for service provider : {}", assessorCompany);
		if (assessorCompany.getId() == null) {
			return createAssessorCompanyForServiceProvider(assessorCompany);
		}
		AssessorCompany result = assessorCompanyService.save(assessorCompany);
		organizationSummaryStatusService.createOrUpdate(OrganizationSection.ASSESSOR);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, assessorCompany.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /assessor-companies : Updates an existing assessorCompany for
	 * merchant. This is for module part 1B, Use this if only applicable
	 *
	 * @param assessorCompany
	 *            the assessorCompany to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         assessorCompany, or with status 400 (Bad Request) if the
	 *         assessorCompany is not valid, or with status 401 (Un-Authorized)
	 *         if un-authorized, or with status 500 (Internal Server Error) if
	 *         the assessorCompany couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/assessor-companies")
	@Timed
	public ResponseEntity<AssessorCompany> updateAssessorCompanyForMerchant(
			@RequestBody AssessorCompany assessorCompany) throws URISyntaxException {
		log.debug("REST request to update AssessorCompany for merchant: {}", assessorCompany);
		if (assessorCompany.getId() == null) {
			return createAssessorCompanyForMerchant(assessorCompany);
		}
		AssessorCompany result = assessorCompanyService.save(assessorCompany);
		organizationSummaryStatusService.createOrUpdate(OrganizationSection.ASSESSOR);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, assessorCompany.getId().toString()))
				.body(result);
	}

	/**
	 * GET /assessor-companies/service-provider : get all the assessorCompanies
	 * for service provider. This is for module part 1B, Use this if only
	 * applicable
	 *
	 * @param pageable
	 *            the pagination information, default page number is 0 and limit
	 *            is 20
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         assessorCompanies in body
	 */
	@GetMapping("/assessor-companies/service-provider")
	@Timed
	public ResponseEntity<List<AssessorCompany>> getAllAssessorCompaniesForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of AssessorCompanies for service provider");
		Page<AssessorCompany> page = assessorCompanyService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/assessor-companies/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /assessor-companies : get all the assessorCompanies for merchant.
	 * This is for module part 1B, Use this if only applicable
	 *
	 * @param pageable
	 *            the pagination information, default page number is 0 and limit
	 *            is 20
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         assessorCompanies in body
	 */
	@GetMapping("/assessor-companies")
	@Timed
	public ResponseEntity<List<AssessorCompany>> getAllAssessorCompaniesForMerchant(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of AssessorCompanies for merchant");
		Page<AssessorCompany> page = assessorCompanyService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assessor-companies");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /assessor-companies/service-provider/:id : get the "id"
	 * assessorCompany for service provider. This is for module part 1B, Use
	 * this if only applicable
	 *
	 * @param id
	 *            the id of the assessorCompany to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         assessorCompany, or with status 404 (Not Found), or with 401
	 *         (un-authorised) if unauthorized
	 */
	@GetMapping("/assessor-companies/service-provider/{id}")
	@Timed
	public ResponseEntity<AssessorCompany> getAssessorCompanyForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get AssessorCompany for service provider: {}", id);
		AssessorCompany assessorCompany = assessorCompanyService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(assessorCompany));
	}

	/**
	 * GET /assessor-companies/:id : get the "id" assessorCompany for merchant.
	 * This is for module part 1B, Use this if only applicable
	 *
	 * @param id
	 *            the id of the assessorCompany to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         assessorCompany, or with status 404 (Not Found), or with 401
	 *         (un-authorised) if unauthorized
	 */
	@GetMapping("/assessor-companies/{id}")
	@Timed
	public ResponseEntity<AssessorCompany> getAssessorCompanyForMerchant(@PathVariable Long id) {
		log.debug("REST request to get AssessorCompany for merchant: {}", id);
		AssessorCompany assessorCompany = assessorCompanyService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(assessorCompany));
	}

	/**
	 * DELETE /assessor-companies/service-provider/:id : delete the "id"
	 * assessorCompany for service provider. This is for module part 1B, Use
	 * this if only applicable
	 *
	 * @param id
	 *            the id of the assessorCompany to delete
	 * @return the ResponseEntity with status 200 (OK) or with 401
	 *         (un-authorised) if unauthorized, or with 500 (ISE) if failed to
	 *         delete
	 */
	@DeleteMapping("/assessor-companies/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAssessorCompanyForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete AssessorCompany for service provider : {}", id);
		assessorCompanyService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /assessor-companies/:id : delete the "id" assessorCompany for
	 * merchant. This is for module part 1B, Use this if only applicable
	 *
	 * @param id
	 *            the id of the assessorCompany to delete
	 * @return the ResponseEntity with status 200 (OK) or with 401
	 *         (un-authorised) if unauthorized, or with 500 (ISE) if failed to
	 *         delete
	 */
	@DeleteMapping("/assessor-companies/{id}")
	@Timed
	public ResponseEntity<Void> deleteAssessorCompanyForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete AssessorCompany for merchant : {}", id);
		assessorCompanyService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
