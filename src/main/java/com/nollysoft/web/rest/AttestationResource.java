package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.Attestation;
import com.nollysoft.service.AttestationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Attestation.
 */
@RestController
@RequestMapping("/api")
public class AttestationResource {
	private final Logger log = LoggerFactory.getLogger(AttestationResource.class);

	private static final String ENTITY_NAME = "attestation";

	private final AttestationService attestationService;

	public AttestationResource(AttestationService attestationService) {
		this.attestationService = attestationService;
	}

	/**
	 * POST /attestations/service-provider : Create a new attestation for service
	 * provider)
	 *
	 * @param attestation
	 *            the attestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         attestation, or with status 400 (Bad Request) if the attestation has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/attestations/service-provider")
	@Timed
	public ResponseEntity<Attestation> createAttestationForServiceProvider(@RequestBody Attestation attestation)
			throws URISyntaxException {
		log.debug("REST request to save Attestation for service provider: {}", attestation);
		if (attestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new attestation cannot already have an ID")).body(null);
		}
		Attestation result = attestationService.save(attestation);
		return ResponseEntity.created(new URI("/api/attestations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /attestations : Create a new attestation for merchant.
	 *
	 * @param attestation
	 *            the attestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         attestation, or with status 400 (Bad Request) if the attestation has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/attestations")
	@Timed
	public ResponseEntity<Attestation> createAttestationForMerchant(@RequestBody Attestation attestation)
			throws URISyntaxException {
		log.debug("REST request to save Attestation for merchant: {}", attestation);
		if (attestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new attestation cannot already have an ID")).body(null);
		}
		Attestation result = attestationService.save(attestation);
		return ResponseEntity.created(new URI("/api/attestations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /attestations/service-provider : Updates an existing attestation for
	 * service provider.
	 *
	 * @param attestation
	 *            the attestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         attestation, or with status 400 (Bad Request) if the attestation is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         attestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/attestations/service-provider")
	@Timed
	public ResponseEntity<Attestation> updateAttestationForServiceProvider(@RequestBody Attestation attestation)
			throws URISyntaxException {
		log.debug("REST request to update Attestation for service provider : {}", attestation);
		if (attestation.getId() == null) {
			return createAttestationForServiceProvider(attestation);
		}

		Attestation result = attestationService.save(attestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attestation.getId().toString())).body(result);
	}

	/**
	 * PUT /attestations : Updates an existing attestation for merchant.
	 *
	 * @param attestation
	 *            the attestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         attestation, or with status 400 (Bad Request) if the attestation is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         attestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/attestations")
	@Timed
	public ResponseEntity<Attestation> updateAttestationForMerchant(@RequestBody Attestation attestation)
			throws URISyntaxException {
		log.debug("REST request to update Attestation for merchant : {}", attestation);
		if (attestation.getId() == null) {
			return createAttestationForMerchant(attestation);
		}
		Attestation result = attestationService.save(attestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attestation.getId().toString())).body(result);
	}

	/**
	 * GET /attestations/service-provider : get all the attestation for service
	 * provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of attestations
	 *         in body
	 */
	@GetMapping("/attestations/service-provider")
	@Timed
	public ResponseEntity<List<Attestation>> getAllAttestationesForServiceProvider() {
		log.debug("REST request to get a page of Attestations for service provider");
		List<Attestation> list = attestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /attestations : get all the attestationes for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of attestationes
	 *         in body
	 */
	@GetMapping("/attestations")
	@Timed
	public ResponseEntity<List<Attestation>> getAllAttestationesForMerchant() {
		log.debug("REST request to get a page of Attestations for merchant");
		List<Attestation> list = attestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /attestations/service-provider/:id : delete the "id" attestation for
	 * service provider.
	 *
	 * @param id
	 *            the id of the attestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/attestations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteAttestationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete Attestation for service provider : {}", id);
		attestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /attestations/:id : delete the "id" attestation for merchant.
	 * 
	 * @param id
	 *            the id of the attestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/attestations/{id}")
	@Timed
	public ResponseEntity<Void> deleteAttestationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete Attestation for merchant : {}", id);
		attestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
