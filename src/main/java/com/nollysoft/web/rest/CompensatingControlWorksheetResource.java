package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.CompensatingControlWorksheet;
import com.nollysoft.service.CompensatingControlWorksheetService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing CompensatingControlWorksheet.
 */
@RestController
@RequestMapping("/api")
public class CompensatingControlWorksheetResource {

	private final Logger log = LoggerFactory.getLogger(CompensatingControlWorksheetResource.class);

	private static final String ENTITY_NAME = "compensatingControlWorksheet";

	private final CompensatingControlWorksheetService compensatingControlWorksheetService;

	public CompensatingControlWorksheetResource(
			CompensatingControlWorksheetService compensatingControlWorksheetService) {
		this.compensatingControlWorksheetService = compensatingControlWorksheetService;
	}

	/**
	 * POST /compensating-control-worksheets/service-provider : Create a new
	 * compensatingControlWorksheet for service provider.
	 *
	 * @param compensatingControlWorksheet
	 *            the compensatingControlWorksheet to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compensatingControlWorksheet, or with status 400 (Bad Request) if the
	 *         compensatingControlWorksheet has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/compensating-control-worksheets/service-provider")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> createCompensatingControlWorksheetForServiceProvider(
			@RequestBody CompensatingControlWorksheet compensatingControlWorksheet) throws URISyntaxException {
		log.debug("REST request to save CompensatingControlWorksheet : {}", compensatingControlWorksheet);
		if (compensatingControlWorksheet.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new compensatingControlWorksheet cannot already have an ID")).body(null);
		}
		CompensatingControlWorksheet result = compensatingControlWorksheetService.save(compensatingControlWorksheet);
		return ResponseEntity
				.created(new URI("/api/compensating-control-worksheets/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /compensating-control-worksheets : Create a new
	 * compensatingControlWorksheet for merchant.
	 *
	 * @param compensatingControlWorksheet
	 *            the compensatingControlWorksheet to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compensatingControlWorksheet, or with status 400 (Bad Request) if the
	 *         compensatingControlWorksheet has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/compensating-control-worksheets")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> createCompensatingControlWorksheetForMerchant(
			@RequestBody CompensatingControlWorksheet compensatingControlWorksheet) throws URISyntaxException {
		log.debug("REST request to save CompensatingControlWorksheet : {}", compensatingControlWorksheet);
		if (compensatingControlWorksheet.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new compensatingControlWorksheet cannot already have an ID")).body(null);
		}
		CompensatingControlWorksheet result = compensatingControlWorksheetService.save(compensatingControlWorksheet);
		return ResponseEntity.created(new URI("/api/compensating-control-worksheets/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /compensating-control-worksheets/service-provider : Updates an existing
	 * compensatingControlWorksheet for service provider.
	 *
	 * @param compensatingControlWorksheet
	 *            the compensatingControlWorksheet to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         compensatingControlWorksheet, or with status 400 (Bad Request) if the
	 *         compensatingControlWorksheet is not valid, or with status 500
	 *         (Internal Server Error) if the compensatingControlWorksheet couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/compensating-control-worksheets/service-provider")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> updateCompensatingControlWorksheetForServiceProvider(
			@RequestBody CompensatingControlWorksheet compensatingControlWorksheet) throws URISyntaxException {
		log.debug("REST request to update CompensatingControlWorksheet : {}", compensatingControlWorksheet);
		if (compensatingControlWorksheet.getId() == null) {
			return createCompensatingControlWorksheetForServiceProvider(compensatingControlWorksheet);
		}
		CompensatingControlWorksheet result = compensatingControlWorksheetService.save(compensatingControlWorksheet);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, compensatingControlWorksheet.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /compensating-control-worksheets : Updates an existing
	 * compensatingControlWorksheet for merchant.
	 *
	 * @param compensatingControlWorksheet
	 *            the compensatingControlWorksheet to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         compensatingControlWorksheet, or with status 400 (Bad Request) if the
	 *         compensatingControlWorksheet is not valid, or with status 500
	 *         (Internal Server Error) if the compensatingControlWorksheet couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/compensating-control-worksheets")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> updateCompensatingControlWorksheetForMerchant(
			@RequestBody CompensatingControlWorksheet compensatingControlWorksheet) throws URISyntaxException {
		log.debug("REST request to update CompensatingControlWorksheet : {}", compensatingControlWorksheet);
		if (compensatingControlWorksheet.getId() == null) {
			return createCompensatingControlWorksheetForMerchant(compensatingControlWorksheet);
		}
		CompensatingControlWorksheet result = compensatingControlWorksheetService.save(compensatingControlWorksheet);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, compensatingControlWorksheet.getId().toString()))
				.body(result);
	}

	/**
	 * GET /compensating-control-worksheets/service-provider : get all the
	 * compensatingControlWorksheets for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         compensatingControlWorksheets in body
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/compensating-control-worksheets/service-provider")
	@Timed
	public ResponseEntity<List<CompensatingControlWorksheet>> getAllCompensatingControlWorksheetsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of CompensatingControlWorksheets");
		Page<CompensatingControlWorksheet> page = compensatingControlWorksheetService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/compensating-control-worksheets");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /compensating-control-worksheets : get all the
	 * compensatingControlWorksheets for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         compensatingControlWorksheets in body
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/compensating-control-worksheets")
	@Timed
	public ResponseEntity<List<CompensatingControlWorksheet>> getAllCompensatingControlWorksheetsForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of CompensatingControlWorksheets");
		Page<CompensatingControlWorksheet> page = compensatingControlWorksheetService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/compensating-control-worksheets");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /compensating-control-worksheets/service-provider/:id : get the "id"
	 * compensatingControlWorksheet for service provider.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheet to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         compensatingControlWorksheet, or with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/compensating-control-worksheets/service-provider/{id}")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> getCompensatingControlWorksheetForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get CompensatingControlWorksheet : {}", id);
		CompensatingControlWorksheet compensatingControlWorksheet = compensatingControlWorksheetService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(compensatingControlWorksheet));
	}

	/**
	 * GET /compensating-control-worksheets/:id : get the "id"
	 * compensatingControlWorksheet for merchant.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheet to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         compensatingControlWorksheet, or with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/compensating-control-worksheets/{id}")
	@Timed
	public ResponseEntity<CompensatingControlWorksheet> getCompensatingControlWorksheetForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get CompensatingControlWorksheet : {}", id);
		CompensatingControlWorksheet compensatingControlWorksheet = compensatingControlWorksheetService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(compensatingControlWorksheet));
	}

	/**
	 * DELETE /compensating-control-worksheets/service-provider/:id : delete the
	 * "id" compensatingControlWorksheet for service provider.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheet to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/compensating-control-worksheets/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteCompensatingControlWorksheetForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete CompensatingControlWorksheet : {}", id);
		compensatingControlWorksheetService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /compensating-control-worksheets/:id : delete the "id"
	 * compensatingControlWorksheet for merchant.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheet to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/compensating-control-worksheets/{id}")
	@Timed
	public ResponseEntity<Void> deleteCompensatingControlWorksheetForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete CompensatingControlWorksheet : {}", id);
		compensatingControlWorksheetService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
