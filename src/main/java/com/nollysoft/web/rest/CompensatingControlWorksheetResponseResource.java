package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.CompensatingControlWorksheetResponse;
import com.nollysoft.service.CompensatingControlWorksheetResponseService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing CompensatingControlWorksheetResponse.
 */
@RestController
@RequestMapping("/api")
public class CompensatingControlWorksheetResponseResource {

	private final Logger log = LoggerFactory.getLogger(CompensatingControlWorksheetResponseResource.class);

	private static final String ENTITY_NAME = "compensatingControlWorksheetResponse";

	private final CompensatingControlWorksheetResponseService compensatingControlWorksheetResponseService;

	public CompensatingControlWorksheetResponseResource(
			CompensatingControlWorksheetResponseService compensatingControlWorksheetResponseService) {
		this.compensatingControlWorksheetResponseService = compensatingControlWorksheetResponseService;
	}

	/**
	 * POST /compensating-control-worksheet-responses/service-provider : Create a
	 * new compensatingControlWorksheetResponse for service provider.
	 *
	 * @param compensatingControlWorksheetResponse
	 *            the compensatingControlWorksheetResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compensatingControlWorksheetResponse, or with status 400 (Bad
	 *         Request) if the compensatingControlWorksheetResponse has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/compensating-control-worksheet-responses/service-provider")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> createCompensatingControlWorksheetResponseForServiceProvider(
			@Valid @RequestBody CompensatingControlWorksheetResponse compensatingControlWorksheetResponse)
			throws URISyntaxException {
		log.debug("REST request to save CompensatingControlWorksheetResponse : {}",
				compensatingControlWorksheetResponse);
		if (compensatingControlWorksheetResponse.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new compensatingControlWorksheetResponse cannot already have an ID")).body(null);
		}
		CompensatingControlWorksheetResponse result = compensatingControlWorksheetResponseService
				.save(compensatingControlWorksheetResponse);
		return ResponseEntity
				.created(new URI("/api/compensating-control-worksheet-responses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /compensating-control-worksheet-responses : Create a new
	 * compensatingControlWorksheetResponse for merchant.
	 *
	 * @param compensatingControlWorksheetResponse
	 *            the compensatingControlWorksheetResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compensatingControlWorksheetResponse, or with status 400 (Bad
	 *         Request) if the compensatingControlWorksheetResponse has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/compensating-control-worksheet-responses")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> createCompensatingControlWorksheetResponseForMerchant(
			@Valid @RequestBody CompensatingControlWorksheetResponse compensatingControlWorksheetResponse)
			throws URISyntaxException {
		log.debug("REST request to save CompensatingControlWorksheetResponse : {}",
				compensatingControlWorksheetResponse);
		if (compensatingControlWorksheetResponse.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new compensatingControlWorksheetResponse cannot already have an ID")).body(null);
		}
		CompensatingControlWorksheetResponse result = compensatingControlWorksheetResponseService
				.save(compensatingControlWorksheetResponse);
		return ResponseEntity.created(new URI("/api/compensating-control-worksheet-responses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /compensating-control-worksheet-responses/service-provider : Updates an
	 * existing compensatingControlWorksheetResponse for service provider.
	 *
	 * @param compensatingControlWorksheetResponse
	 *            the compensatingControlWorksheetResponse to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         compensatingControlWorksheetResponse, or with status 400 (Bad
	 *         Request) if the compensatingControlWorksheetResponse is not valid, or
	 *         with status 500 (Internal Server Error) if the
	 *         compensatingControlWorksheetResponse couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/compensating-control-worksheet-responses/service-provider")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> updateCompensatingControlWorksheetResponseServiceProvider(
			@Valid @RequestBody CompensatingControlWorksheetResponse compensatingControlWorksheetResponse)
			throws URISyntaxException {
		log.debug("REST request to update CompensatingControlWorksheetResponse : {}",
				compensatingControlWorksheetResponse);
		if (compensatingControlWorksheetResponse.getId() == null) {
			return createCompensatingControlWorksheetResponseForServiceProvider(compensatingControlWorksheetResponse);
		}
		CompensatingControlWorksheetResponse result = compensatingControlWorksheetResponseService
				.save(compensatingControlWorksheetResponse);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,
				compensatingControlWorksheetResponse.getId().toString())).body(result);
	}

	/**
	 * PUT /compensating-control-worksheet-responses : Updates an existing
	 * compensatingControlWorksheetResponse for merchant.
	 *
	 * @param compensatingControlWorksheetResponse
	 *            the compensatingControlWorksheetResponse to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         compensatingControlWorksheetResponse, or with status 400 (Bad
	 *         Request) if the compensatingControlWorksheetResponse is not valid, or
	 *         with status 500 (Internal Server Error) if the
	 *         compensatingControlWorksheetResponse couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/compensating-control-worksheet-responses")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> updateCompensatingControlWorksheetResponseForMerchant(
			@Valid @RequestBody CompensatingControlWorksheetResponse compensatingControlWorksheetResponse)
			throws URISyntaxException {
		log.debug("REST request to update CompensatingControlWorksheetResponse : {}",
				compensatingControlWorksheetResponse);
		if (compensatingControlWorksheetResponse.getId() == null) {
			return createCompensatingControlWorksheetResponseForMerchant(compensatingControlWorksheetResponse);
		}
		CompensatingControlWorksheetResponse result = compensatingControlWorksheetResponseService
				.save(compensatingControlWorksheetResponse);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,
				compensatingControlWorksheetResponse.getId().toString())).body(result);
	}

	/**
	 * GET /compensating-control-worksheet-responses/service-provider : get all the
	 * compensatingControlWorksheetResponses for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         compensatingControlWorksheetResponses in body
	 */
	@GetMapping("/compensating-control-worksheet-responses/service-provider")
	@Timed
	public List<CompensatingControlWorksheetResponse> getAllCompensatingControlWorksheetResponsesForServiceProvider() {
		log.debug("REST request to get all CompensatingControlWorksheetResponses");
		return compensatingControlWorksheetResponseService.findAll();
	}

	/**
	 * GET /compensating-control-worksheet-responses : get all the
	 * compensatingControlWorksheetResponses for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         compensatingControlWorksheetResponses in body
	 */
	@GetMapping("/compensating-control-worksheet-responses")
	@Timed
	public List<CompensatingControlWorksheetResponse> getAllCompensatingControlWorksheetResponsesForMerchant() {
		log.debug("REST request to get all CompensatingControlWorksheetResponses");
		return compensatingControlWorksheetResponseService.findAll();
	}

	/**
	 * GET /compensating-control-worksheet-responses/service-provider/:id : get the
	 * "id" compensatingControlWorksheetResponse for service provider.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheetResponse to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         compensatingControlWorksheetResponse, or with status 404 (Not Found)
	 */
	@GetMapping("/compensating-control-worksheet-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> getCompensatingControlWorksheetResponseForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get CompensatingControlWorksheetResponse : {}", id);
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse = compensatingControlWorksheetResponseService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(compensatingControlWorksheetResponse));
	}

	/**
	 * GET /compensating-control-worksheet-responses/:id : get the "id"
	 * compensatingControlWorksheetResponse for merchant.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheetResponse to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         compensatingControlWorksheetResponse, or with status 404 (Not Found)
	 */
	@GetMapping("/compensating-control-worksheet-responses/{id}")
	@Timed
	public ResponseEntity<CompensatingControlWorksheetResponse> getCompensatingControlWorksheetResponseForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get CompensatingControlWorksheetResponse : {}", id);
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse = compensatingControlWorksheetResponseService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(compensatingControlWorksheetResponse));
	}

	/**
	 * DELETE /compensating-control-worksheet-responses/service-provider/:id :
	 * delete the "id" compensatingControlWorksheetResponse for service provider.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheetResponse to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compensating-control-worksheet-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteCompensatingControlWorksheetResponseForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete CompensatingControlWorksheetResponse : {}", id);
		compensatingControlWorksheetResponseService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /compensating-control-worksheet-responses/:id : delete the "id"
	 * compensatingControlWorksheetResponse for merchant.
	 *
	 * @param id
	 *            the id of the compensatingControlWorksheetResponse to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compensating-control-worksheet-responses/{id}")
	@Timed
	public ResponseEntity<Void> deleteCompensatingControlWorksheetResponseForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete CompensatingControlWorksheetResponse : {}", id);
		compensatingControlWorksheetResponseService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
