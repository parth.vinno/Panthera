package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ComplianceAttestation;
import com.nollysoft.service.ComplianceAttestationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.undertow.util.BadRequestException;

/**
 * REST controller for managing ComplianceAttestation.
 */
@RestController
@RequestMapping("/api")
public class ComplianceAttestationResource {

	private final Logger log = LoggerFactory.getLogger(ComplianceAttestationResource.class);

	private static final String ENTITY_NAME = "complianceAttestation";

	private final ComplianceAttestationService complianceAttestationService;

	public ComplianceAttestationResource(ComplianceAttestationService complianceAttestationService) {
		this.complianceAttestationService = complianceAttestationService;
	}

	/**
	 * POST /compliance-attestations/service-provider : Create a new
	 * complianceAttestation for service provider)
	 *
	 * @param complianceAttestation
	 *            the complianceAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         complianceAttestation, or with status 400 (Bad Request) if the
	 *         complianceAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/compliance-attestations/service-provider")
	@Timed
	public ResponseEntity<ComplianceAttestation> createComplianceAttestationForServiceProvider(
			@RequestBody ComplianceAttestation complianceAttestation) throws URISyntaxException, BadRequestException {
		log.debug("REST request to save ComplianceAttestation for service provider: {}", complianceAttestation);
		if (complianceAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new complianceAttestation cannot already have an ID")).body(null);
		}
		ComplianceAttestation result = complianceAttestationService.save(complianceAttestation);
		return ResponseEntity.created(new URI("/api/compliance-attestations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /compliance-attestations : Create a new complianceAttestation for
	 * merchant.
	 *
	 * @param complianceAttestation
	 *            the complianceAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         complianceAttestation, or with status 400 (Bad Request) if the
	 *         complianceAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/compliance-attestations")
	@Timed
	public ResponseEntity<ComplianceAttestation> createComplianceAttestationForMerchant(
			@RequestBody ComplianceAttestation complianceAttestation) throws URISyntaxException, BadRequestException {
		log.debug("REST request to save ComplianceAttestation for merchant: {}", complianceAttestation);
		if (complianceAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new complianceAttestation cannot already have an ID")).body(null);
		}
		ComplianceAttestation result = complianceAttestationService.save(complianceAttestation);
		return ResponseEntity.created(new URI("/api/compliance-attestations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /compliance-attestations/service-provider : Updates an existing
	 * complianceAttestation for service provider.
	 *
	 * @param complianceAttestation
	 *            the complianceAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         complianceAttestation, or with status 400 (Bad Request) if the
	 *         complianceAttestation is not valid, or with status 500 (Internal
	 *         Server Error) if the complianceAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PutMapping("/compliance-attestations/service-provider")
	@Timed
	public ResponseEntity<ComplianceAttestation> updateComplianceAttestationForServiceProvider(
			@RequestBody ComplianceAttestation complianceAttestation) throws URISyntaxException, BadRequestException {
		log.debug("REST request to update ComplianceAttestation for service provider : {}", complianceAttestation);
		if (complianceAttestation.getId() == null) {
			return createComplianceAttestationForServiceProvider(complianceAttestation);
		}
		ComplianceAttestation result = complianceAttestationService.save(complianceAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, complianceAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /compliance-attestations : Updates an existing complianceAttestation for
	 * merchant.
	 *
	 * @param complianceAttestation
	 *            the complianceAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         complianceAttestation, or with status 400 (Bad Request) if the
	 *         complianceAttestation is not valid, or with status 500 (Internal
	 *         Server Error) if the complianceAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PutMapping("/compliance-attestations")
	@Timed
	public ResponseEntity<ComplianceAttestation> updateComplianceAttestationForMerchant(
			@RequestBody ComplianceAttestation complianceAttestation) throws URISyntaxException, BadRequestException {
		log.debug("REST request to update ComplianceAttestation for merchant : {}", complianceAttestation);
		if (complianceAttestation.getId() == null) {
			return createComplianceAttestationForMerchant(complianceAttestation);
		}
		ComplianceAttestation result = complianceAttestationService.save(complianceAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, complianceAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * GET /compliance-attestations/service-provider : get all the
	 * complianceAttestation for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         complianceAttestations in body
	 */
	@GetMapping("/compliance-attestations/service-provider")
	@Timed
	public ResponseEntity<List<ComplianceAttestation>> getAllComplianceAttestationesForServiceProvider() {
		log.debug("REST request to get a page of ComplianceAttestationes for service provider");
		List<ComplianceAttestation> list = complianceAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /compliance-attestations : get all the complianceAttestationes for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         complianceAttestationes in body
	 */
	@GetMapping("/compliance-attestations")
	@Timed
	public ResponseEntity<List<ComplianceAttestation>> getAllComplianceAttestationesForMerchant() {
		log.debug("REST request to get a page of ComplianceAttestationes for merchant");
		List<ComplianceAttestation> list = complianceAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /compliance-attestations/service-provider/:id : delete the "id"
	 * complianceAttestation for service provider.
	 *
	 * @param id
	 *            the id of the complianceAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compliance-attestations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteComplianceAttestationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete ComplianceAttestation for service provider : {}", id);
		complianceAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /compliance-attestations/:id : delete the "id" complianceAttestation
	 * for merchant. (Part 1B))
	 *
	 * @param id
	 *            the id of the complianceAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compliance-attestations/{id}")
	@Timed
	public ResponseEntity<Void> deleteComplianceAttestationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete ComplianceAttestation for merchant : {}", id);
		complianceAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
