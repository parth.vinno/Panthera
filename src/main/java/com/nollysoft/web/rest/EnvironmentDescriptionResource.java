package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.EnvironmentDescription;
import com.nollysoft.service.EnvironmentDescriptionService;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EnvironmentDescription.
 */
@RestController
@RequestMapping("/api")
public class EnvironmentDescriptionResource {

	private final Logger log = LoggerFactory.getLogger(EnvironmentDescriptionResource.class);

	private static final String ENTITY_NAME = "environmentDescription";

	private final EnvironmentDescriptionService environmentDescriptionService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public EnvironmentDescriptionResource(EnvironmentDescriptionService environmentDescriptionService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.environmentDescriptionService = environmentDescriptionService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /environment-descriptions/service-provider : Create a new
	 * environmentDescription for service provider.
	 *
	 * @param environmentDescription
	 *            the environmentDescription to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new environmentDescription, or with status 400 (Bad Request) if
	 *         the environmentDescription has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/environment-descriptions/service-provider")
	@Timed
	public ResponseEntity<EnvironmentDescription> createEnvironmentDescriptionForServiceProvider(
			@Valid @RequestBody EnvironmentDescription environmentDescription) throws URISyntaxException {
		log.debug("REST request to save EnvironmentDescription : {}", environmentDescription);
		if (environmentDescription.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new environmentDescription cannot already have an ID")).body(null);
		}
		EnvironmentDescription result = environmentDescriptionService.save(environmentDescription);
		executiveSummaryStatusService.createOrUpdate("ENV_DESCRIPTION");
		return ResponseEntity.created(new URI("/api/environment-descriptions/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /environment-descriptions/service-provider : Updates an existing
	 * environmentDescription for service provider.
	 *
	 * @param environmentDescription
	 *            the environmentDescription to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         environmentDescription, or with status 400 (Bad Request) if the
	 *         environmentDescription is not valid, or with status 500 (Internal
	 *         Server Error) if the environmentDescription couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/environment-descriptions/service-provider")
	@Timed
	public ResponseEntity<EnvironmentDescription> updateEnvironmentDescriptionForServiceProvider(
			@Valid @RequestBody EnvironmentDescription environmentDescription) throws URISyntaxException {
		log.debug("REST request to update EnvironmentDescription : {}", environmentDescription);
		if (environmentDescription.getId() == null) {
			return createEnvironmentDescriptionForServiceProvider(environmentDescription);
		}
		EnvironmentDescription result = environmentDescriptionService.save(environmentDescription);
		executiveSummaryStatusService.createOrUpdate("ENV_DESCRIPTION");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, environmentDescription.getId().toString()))
				.body(result);
	}

	/**
	 * GET /environment-descriptions/service-provider : get all the
	 * environmentDescriptions for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         environmentDescriptions in body
	 */
	@GetMapping("/environment-descriptions/service-provider")
	@Timed
	public ResponseEntity<List<EnvironmentDescription>> getAllEnvironmentDescriptionsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of EnvironmentDescriptions");
		Page<EnvironmentDescription> page = environmentDescriptionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/environment-descriptions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /environment-descriptions/service-provider/:id : get the "id"
	 * environmentDescription for service provider.
	 *
	 * @param id
	 *            the id of the environmentDescription to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         environmentDescription, or with status 404 (Not Found)
	 */
	@GetMapping("/environment-descriptions/service-provider/{id}")
	@Timed
	public ResponseEntity<EnvironmentDescription> getEnvironmentDescriptionForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get EnvironmentDescription : {}", id);
		EnvironmentDescription environmentDescription = environmentDescriptionService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(environmentDescription));
	}

	/**
	 * DELETE /environment-descriptions/service-provider/:id : delete the "id"
	 * environmentDescription for service provider.
	 *
	 * @param id
	 *            the id of the environmentDescription to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/environment-descriptions/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteEnvironmentDescriptionForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete EnvironmentDescription : {}", id);
		environmentDescriptionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /environment-descriptions : Create a new environmentDescription for
	 * merchant.
	 *
	 * @param environmentDescription
	 *            the environmentDescription to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new environmentDescription, or with status 400 (Bad Request) if
	 *         the environmentDescription has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/environment-descriptions")
	@Timed
	public ResponseEntity<EnvironmentDescription> createEnvironmentDescriptionForMerchant(
			@Valid @RequestBody EnvironmentDescription environmentDescription) throws URISyntaxException {
		log.debug("REST request to save EnvironmentDescription : {}", environmentDescription);
		if (environmentDescription.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new environmentDescription cannot already have an ID")).body(null);
		}
		EnvironmentDescription result = environmentDescriptionService.save(environmentDescription);
		executiveSummaryStatusService.createOrUpdate("ENV_DESCRIPTION");
		return ResponseEntity.created(new URI("/api/environment-descriptions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /environment-descriptions : Updates an existing
	 * environmentDescription for merchant.
	 *
	 * @param environmentDescription
	 *            the environmentDescription to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         environmentDescription, or with status 400 (Bad Request) if the
	 *         environmentDescription is not valid, or with status 500 (Internal
	 *         Server Error) if the environmentDescription couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/environment-descriptions")
	@Timed
	public ResponseEntity<EnvironmentDescription> updateEnvironmentDescriptionForMerchant(
			@Valid @RequestBody EnvironmentDescription environmentDescription) throws URISyntaxException {
		log.debug("REST request to update EnvironmentDescription : {}", environmentDescription);
		if (environmentDescription.getId() == null) {
			return createEnvironmentDescriptionForMerchant(environmentDescription);
		}
		EnvironmentDescription result = environmentDescriptionService.save(environmentDescription);
		executiveSummaryStatusService.createOrUpdate("ENV_DESCRIPTION");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, environmentDescription.getId().toString()))
				.body(result);
	}

	/**
	 * GET /environment-descriptions : get all the environmentDescriptions for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         environmentDescriptions in body
	 */
	@GetMapping("/environment-descriptions")
	@Timed
	public ResponseEntity<List<EnvironmentDescription>> getAllEnvironmentDescriptionsForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of EnvironmentDescriptions");
		Page<EnvironmentDescription> page = environmentDescriptionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/environment-descriptions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /environment-descriptions/:id : get the "id" environmentDescription
	 * for merchant.
	 *
	 * @param id
	 *            the id of the environmentDescription to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         environmentDescription, or with status 404 (Not Found)
	 */
	@GetMapping("/environment-descriptions/{id}")
	@Timed
	public ResponseEntity<EnvironmentDescription> getEnvironmentDescriptionForMerchant(@PathVariable Long id) {
		log.debug("REST request to get EnvironmentDescription : {}", id);
		EnvironmentDescription environmentDescription = environmentDescriptionService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(environmentDescription));
	}

	/**
	 * DELETE /environment-descriptions/:id : delete the "id"
	 * environmentDescription for merchant.
	 *
	 * @param id
	 *            the id of the environmentDescription to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/environment-descriptions/{id}")
	@Timed
	public ResponseEntity<Void> deleteEnvironmentDescriptionForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete EnvironmentDescription : {}", id);
		environmentDescriptionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
