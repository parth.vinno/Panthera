package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ExecutiveSummaryStatus;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.dto.ExecutiveSummaryStatusDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ExecutiveSummaryStatus.
 */
@RestController
@RequestMapping("/api")
public class ExecutiveSummaryStatusResource {

	private final Logger log = LoggerFactory.getLogger(ExecutiveSummaryStatusResource.class);

	private static final String ENTITY_NAME = "executiveSummaryStatus";

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public ExecutiveSummaryStatusResource(ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /executive-summary-statuses : Create a new executiveSummaryStatus.
	 *
	 * @param executiveSummaryStatus
	 *            the executiveSummaryStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         executiveSummaryStatus, or with status 400 (Bad Request) if the
	 *         executiveSummaryStatus has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/executive-summary-statuses")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatus> createExecutiveSummaryStatus(
			@Valid @RequestBody ExecutiveSummaryStatus executiveSummaryStatus) throws URISyntaxException {
		log.debug("REST request to save ExecutiveSummaryStatus : {}", executiveSummaryStatus);
		if (executiveSummaryStatus.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new executiveSummaryStatus cannot already have an ID")).body(null);
		}
		ExecutiveSummaryStatus result = executiveSummaryStatusService.save(executiveSummaryStatus);
		return ResponseEntity.created(new URI("/api/executive-summary-statuses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /executive-summary-statuses : Create a new executiveSummaryStatus.
	 *
	 * @param executiveSummaryStatus
	 *            the executiveSummaryStatus to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         executiveSummaryStatus, or with status 400 (Bad Request) if the
	 *         executiveSummaryStatus has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/executive-summary-statuses/{section}")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatus> createOrUpdateExecutiveSummaryStatus(
			@PathVariable("section") String section) throws URISyntaxException {
		log.debug("REST request to save ExecutiveSummaryStatus : {}", section);
		ExecutiveSummaryStatus result = executiveSummaryStatusService.createOrUpdate(section);
		return ResponseEntity.created(new URI("/api/executive-summary-statuses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /executive-summary-statuses : Updates an existing executiveSummaryStatus.
	 *
	 * @param executiveSummaryStatus
	 *            the executiveSummaryStatus to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         executiveSummaryStatus, or with status 400 (Bad Request) if the
	 *         executiveSummaryStatus is not valid, or with status 500 (Internal
	 *         Server Error) if the executiveSummaryStatus couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/executive-summary-statuses")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatus> updateExecutiveSummaryStatus(
			@Valid @RequestBody ExecutiveSummaryStatus executiveSummaryStatus) throws URISyntaxException {
		log.debug("REST request to update ExecutiveSummaryStatus : {}", executiveSummaryStatus);
		if (executiveSummaryStatus.getId() == null) {
			return createExecutiveSummaryStatus(executiveSummaryStatus);
		}
		ExecutiveSummaryStatus result = executiveSummaryStatusService.save(executiveSummaryStatus);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, executiveSummaryStatus.getId().toString()))
				.body(result);
	}

	/**
	 * GET /executive-summary-statuses : get all the executiveSummaryStatuses.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         executiveSummaryStatuses in body
	 */
	@GetMapping("/executive-summary-statuses")
	@Timed
	public List<ExecutiveSummaryStatus> getAllExecutiveSummaryStatuses() {
		log.debug("REST request to get all ExecutiveSummaryStatuses");
		return executiveSummaryStatusService.findAll();
	}

	/**
	 * GET /executive-summary-statuses/:id : get the "id" executiveSummaryStatus.
	 *
	 * @param id
	 *            the id of the executiveSummaryStatus to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         executiveSummaryStatus, or with status 404 (Not Found)
	 */
	@GetMapping("/executive-summary-statuses/{id}")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatus> getExecutiveSummaryStatus(@PathVariable Long id) {
		log.debug("REST request to get ExecutiveSummaryStatus : {}", id);
		ExecutiveSummaryStatus executiveSummaryStatus = executiveSummaryStatusService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(executiveSummaryStatus));
	}

	/**
	 * GET /executive-summary-statuses/status : get status for executiveSummaryStatus.
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         executiveSummaryStatusDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/executive-summary-statuses/status/service-provider")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatusDTO> getStatusForServiceProvider() {
		log.debug("REST request to get ExecutiveSummaryStatus : {}");
		ExecutiveSummaryStatusDTO executiveSummaryStatus = executiveSummaryStatusService.getStatus(9);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(executiveSummaryStatus));
	}

	/**
	 * GET /executive-summary-statuses/status : get status for executiveSummaryStatus.
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         executiveSummaryStatusDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/executive-summary-statuses/status")
	@Timed
	public ResponseEntity<ExecutiveSummaryStatusDTO> getStatusForMerchant() {
		log.debug("REST request to get ExecutiveSummaryStatus : {}");
		ExecutiveSummaryStatusDTO executiveSummaryStatus = executiveSummaryStatusService.getStatus(7);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(executiveSummaryStatus));
	}

	/**
	 * DELETE /executive-summary-statuses/:id : delete the "id"
	 * executiveSummaryStatus.
	 *
	 * @param id
	 *            the id of the executiveSummaryStatus to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/executive-summary-statuses/{id}")
	@Timed
	public ResponseEntity<Void> deleteExecutiveSummaryStatus(@PathVariable Long id) {
		log.debug("REST request to delete ExecutiveSummaryStatus : {}", id);
		executiveSummaryStatusService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
