package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.ExpectedTest;
import com.nollysoft.service.ExpectedTestService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ExpectedTest.
 */
@RestController
@RequestMapping("/api")
public class ExpectedTestResource {

	private final Logger log = LoggerFactory.getLogger(ExpectedTestResource.class);

	private static final String ENTITY_NAME = "expectedTest";

	private final ExpectedTestService expectedTestService;

	public ExpectedTestResource(ExpectedTestService expectedTestService) {
		this.expectedTestService = expectedTestService;
	}

	/**
	 * POST /expected-tests/service-provider : Create a new expectedTest for service
	 * provider.
	 *
	 * @param expectedTest
	 *            the expectedTest to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         expectedTest, or with status 400 (Bad Request) if the expectedTest
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/expected-tests/service-provider")
	@Timed
	public ResponseEntity<ExpectedTest> createExpectedTestForServiceProvider(
			@Valid @RequestBody ExpectedTest expectedTest) throws URISyntaxException {
		log.debug("REST request to save ExpectedTest : {}", expectedTest);
		if (expectedTest.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new expectedTest cannot already have an ID")).body(null);
		}
		ExpectedTest result = expectedTestService.save(expectedTest);
		return ResponseEntity.created(new URI("/api/expected-tests/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /expected-tests : Create a new expectedTest for merchant.
	 *
	 * @param expectedTest
	 *            the expectedTest to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         expectedTest, or with status 400 (Bad Request) if the expectedTest
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/expected-tests")
	@Timed
	public ResponseEntity<ExpectedTest> createExpectedTestForMerchant(@Valid @RequestBody ExpectedTest expectedTest)
			throws URISyntaxException {
		log.debug("REST request to save ExpectedTest : {}", expectedTest);
		if (expectedTest.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new expectedTest cannot already have an ID")).body(null);
		}
		ExpectedTest result = expectedTestService.save(expectedTest);
		return ResponseEntity.created(new URI("/api/expected-tests/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /expected-tests/service-provider : Updates an existing expectedTest for
	 * service provider.
	 *
	 * @param expectedTest
	 *            the expectedTest to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         expectedTest, or with status 400 (Bad Request) if the expectedTest is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         expectedTest couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/expected-tests/service-provider")
	@Timed
	public ResponseEntity<ExpectedTest> updateExpectedTestForServiceProvider(
			@Valid @RequestBody ExpectedTest expectedTest) throws URISyntaxException {
		log.debug("REST request to update ExpectedTest : {}", expectedTest);
		if (expectedTest.getId() == null) {
			return createExpectedTestForServiceProvider(expectedTest);
		}
		ExpectedTest result = expectedTestService.save(expectedTest);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, expectedTest.getId().toString())).body(result);
	}

	/**
	 * PUT /expected-tests : Updates an existing expectedTest for merchant.
	 *
	 * @param expectedTest
	 *            the expectedTest to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         expectedTest, or with status 400 (Bad Request) if the expectedTest is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         expectedTest couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/expected-tests")
	@Timed
	public ResponseEntity<ExpectedTest> updateExpectedTestForMerchant(@Valid @RequestBody ExpectedTest expectedTest)
			throws URISyntaxException {
		log.debug("REST request to update ExpectedTest : {}", expectedTest);
		if (expectedTest.getId() == null) {
			return createExpectedTestForMerchant(expectedTest);
		}
		ExpectedTest result = expectedTestService.save(expectedTest);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, expectedTest.getId().toString())).body(result);
	}

	/**
	 * GET /expected-tests/service-provider : get all the expectedTests for service
	 * provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of expectedTests
	 *         in body
	 */
	@GetMapping("/expected-tests/service-provider")
	@Timed
	public List<ExpectedTest> getAllExpectedTestsForServiceProvider() {
		log.debug("REST request to get all ExpectedTests");
		return expectedTestService.findAll();
	}

	/**
	 * GET /expected-tests : get all the expectedTests for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of expectedTests
	 *         in body
	 */
	@GetMapping("/expected-tests")
	@Timed
	public List<ExpectedTest> getAllExpectedTestsForMerchant() {
		log.debug("REST request to get all ExpectedTests");
		return expectedTestService.findAll();
	}

	@GetMapping("/expected-tests-pageable")
	@Timed
	public ResponseEntity<List<ExpectedTest>> getAllExpectedTestingsForMerchant(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ExpectedTests");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			Page<ExpectedTest> page = expectedTestService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/merchant/expectedTests");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}
	
	/**
	 * GET /expected-tests/use-count/service-provider/:id : get use count of the
	 * expectedTest for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the count in body
	 */
	@GetMapping("/expected-tests/use-count/service-provider/{id}")
	@Timed
	public int getExpectedTestsUseCountForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get use count of ExpectedTests : {}", id);
		return expectedTestService.getUseCount(id);
	}

	/**
	 * GET /expected-tests/use-count/:id : get use count of the expectedTest for
	 * merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the count in body
	 */
	@GetMapping("/expected-tests/use-count/{id}")
	@Timed
	public int getExpectedTestsUseCountForMerchant(@PathVariable Long id) {
		log.debug("REST request to get use count of ExpectedTests : {}", id);
		return expectedTestService.getUseCount(id);
	}

	/**
	 * GET /expected-tests/service-provider/:id : get the "id" expectedTest for
	 * service provider.
	 *
	 * @param id
	 *            the id of the expectedTest to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         expectedTest, or with status 404 (Not Found)
	 */
	@GetMapping("/expected-tests/service-provider/{id}")
	@Timed
	public ResponseEntity<ExpectedTest> getExpectedTestForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get ExpectedTest : {}", id);
		ExpectedTest expectedTest = expectedTestService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(expectedTest));
	}

	/**
	 * GET /expected-tests/:id : get the "id" expectedTest for merchant.
	 *
	 * @param id
	 *            the id of the expectedTest to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         expectedTest, or with status 404 (Not Found)
	 */
	@GetMapping("/expected-tests/{id}")
	@Timed
	public ResponseEntity<ExpectedTest> getExpectedTestForMerchant(@PathVariable Long id) {
		log.debug("REST request to get ExpectedTest : {}", id);
		ExpectedTest expectedTest = expectedTestService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(expectedTest));
	}

	/**
	 * DELETE /expected-tests/service-provider/:id : delete the "id" expectedTest
	 * for service provider.
	 *
	 * @param id
	 *            the id of the expectedTest to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/expected-tests/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteExpectedTestForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete ExpectedTest : {}", id);
		expectedTestService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /expected-tests/:id : delete the "id" expectedTest for merchant.
	 *
	 * @param id
	 *            the id of the expectedTest to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/expected-tests/{id}")
	@Timed
	public ResponseEntity<Void> deleteExpectedTestForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete ExpectedTest : {}", id);
		expectedTestService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
