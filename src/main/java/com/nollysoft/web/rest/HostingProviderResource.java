package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.HostingProvider;
import com.nollysoft.service.HostingProviderService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing HostingProvider.
 */
@RestController
@RequestMapping("/api")
public class HostingProviderResource {

    private final Logger log = LoggerFactory.getLogger(HostingProviderResource.class);

    private static final String ENTITY_NAME = "hostingProvider";

    private final HostingProviderService hostingProviderService;

    public HostingProviderResource(HostingProviderService hostingProviderService) {
        this.hostingProviderService = hostingProviderService;
    }

    /**
     * POST  /hosting-providers : Create a new hostingProvider.
     *
     * @param hostingProvider the hostingProvider to create
     * @return the ResponseEntity with status 201 (Created) and with body the new hostingProvider, or with status 400 (Bad Request) if the hostingProvider has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/hosting-providers")
    @Timed
    public ResponseEntity<HostingProvider> createHostingProvider(@RequestBody HostingProvider hostingProvider) throws URISyntaxException {
        log.debug("REST request to save HostingProvider : {}", hostingProvider);
        if (hostingProvider.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new hostingProvider cannot already have an ID")).body(null);
        }
        HostingProvider result = hostingProviderService.save(hostingProvider);
        return ResponseEntity.created(new URI("/api/hosting-providers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hosting-providers : Updates an existing hostingProvider.
     *
     * @param hostingProvider the hostingProvider to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated hostingProvider,
     * or with status 400 (Bad Request) if the hostingProvider is not valid,
     * or with status 500 (Internal Server Error) if the hostingProvider couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/hosting-providers")
    @Timed
    public ResponseEntity<HostingProvider> updateHostingProvider(@RequestBody HostingProvider hostingProvider) throws URISyntaxException {
        log.debug("REST request to update HostingProvider : {}", hostingProvider);
        if (hostingProvider.getId() == null) {
            return createHostingProvider(hostingProvider);
        }
        HostingProvider result = hostingProviderService.save(hostingProvider);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hostingProvider.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hosting-providers : get all the hostingProviders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of hostingProviders in body
     */
    @GetMapping("/hosting-providers")
    @Timed
    public ResponseEntity<List<HostingProvider>> getAllHostingProviders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of HostingProviders");
        Page<HostingProvider> page = hostingProviderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hosting-providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hosting-providers/:id : get the "id" hostingProvider.
     *
     * @param id the id of the hostingProvider to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the hostingProvider, or with status 404 (Not Found)
     */
    @GetMapping("/hosting-providers/{id}")
    @Timed
    public ResponseEntity<HostingProvider> getHostingProvider(@PathVariable Long id) {
        log.debug("REST request to get HostingProvider : {}", id);
        HostingProvider hostingProvider = hostingProviderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hostingProvider));
    }

    /**
     * DELETE  /hosting-providers/:id : delete the "id" hostingProvider.
     *
     * @param id the id of the hostingProvider to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/hosting-providers/{id}")
    @Timed
    public ResponseEntity<Void> deleteHostingProvider(@PathVariable Long id) {
        log.debug("REST request to delete HostingProvider : {}", id);
        hostingProviderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
