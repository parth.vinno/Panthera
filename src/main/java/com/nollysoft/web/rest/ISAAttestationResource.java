package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ISAAttestation;
import com.nollysoft.service.ISAAttestationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing ISAAttestation.
 */
@RestController
@RequestMapping("/api")
public class ISAAttestationResource {

	private final Logger log = LoggerFactory.getLogger(ISAAttestationResource.class);

	private static final String ENTITY_NAME = "isaAttestation";
	private final ISAAttestationService isaAttestationService;

	public ISAAttestationResource(ISAAttestationService isaAttestationService) {
		this.isaAttestationService = isaAttestationService;
	}

	/**
	 * POST /isa-attestations/service-provider : Create a new isaAttestation for
	 * service provider)
	 *
	 * @param isaAttestation
	 *            the isaAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         isaAttestation, or with status 400 (Bad Request) if the
	 *         isaAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/isa-attestations/service-provider")
	@Timed
	public ResponseEntity<ISAAttestation> createISAAttestationForServiceProvider(
			@RequestBody ISAAttestation isaAttestation) throws URISyntaxException {
		log.debug("REST request to save ISAAttestation for service provider: {}", isaAttestation);
		if (isaAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new isaAttestation cannot already have an ID")).body(null);
		}
		ISAAttestation result = isaAttestationService.save(isaAttestation);
		return ResponseEntity.created(new URI("/api/isa-attestations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /isa-attestations : Create a new isaAttestation for merchant.
	 *
	 * @param isaAttestation
	 *            the isaAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         isaAttestation, or with status 400 (Bad Request) if the
	 *         isaAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/isa-attestations")
	@Timed
	public ResponseEntity<ISAAttestation> createISAAttestationForMerchant(@RequestBody ISAAttestation isaAttestation)
			throws URISyntaxException {
		log.debug("REST request to save ISAAttestation for merchant: {}", isaAttestation);
		if (isaAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new isaAttestation cannot already have an ID")).body(null);
		}
		ISAAttestation result = isaAttestationService.save(isaAttestation);
		return ResponseEntity.created(new URI("/api/isa-attestations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /isa-attestations/service-provider : Updates an existing isaAttestation
	 * for service provider.
	 *
	 * @param isaAttestation
	 *            the isaAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         isaAttestation, or with status 400 (Bad Request) if the
	 *         isaAttestation is not valid, or with status 500 (Internal Server
	 *         Error) if the isaAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/isa-attestations/service-provider")
	@Timed
	public ResponseEntity<ISAAttestation> updateISAAttestationForServiceProvider(
			@RequestBody ISAAttestation isaAttestation) throws URISyntaxException {
		log.debug("REST request to update ISAAttestation for service provider : {}", isaAttestation);
		if (isaAttestation.getId() == null) {
			return createISAAttestationForServiceProvider(isaAttestation);
		}
		ISAAttestation result = isaAttestationService.save(isaAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, isaAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /isa-attestations : Updates an existing isaAttestation for merchant.
	 *
	 * @param isaAttestation
	 *            the isaAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         isaAttestation, or with status 400 (Bad Request) if the
	 *         isaAttestation is not valid, or with status 500 (Internal Server
	 *         Error) if the isaAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/isa-attestations")
	@Timed
	public ResponseEntity<ISAAttestation> updateISAAttestationForMerchant(@RequestBody ISAAttestation isaAttestation)
			throws URISyntaxException {
		log.debug("REST request to update ISAAttestation for merchant : {}", isaAttestation);
		if (isaAttestation.getId() == null) {
			return createISAAttestationForMerchant(isaAttestation);
		}
		ISAAttestation result = isaAttestationService.save(isaAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, isaAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * GET /isa-attestations/service-provider : get all the isaAttestation for
	 * service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         isaAttestations in body
	 */
	@GetMapping("/isa-attestations/service-provider")
	@Timed
	public ResponseEntity<List<ISAAttestation>> getAllISAAttestationesForServiceProvider() {
		log.debug("REST request to get a page of ISAAttestations for service provider");
		List<ISAAttestation> list = isaAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /isaAttestations : get all the isaAttestationes for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         isaAttestationes in body
	 */
	@GetMapping("/isa-attestations")
	@Timed
	public ResponseEntity<List<ISAAttestation>> getAllISAAttestationesForMerchant() {
		log.debug("REST request to get a page of ISAAttestationes for merchant");
		List<ISAAttestation> list = isaAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /isa-attestations/service-provider/:id : delete the "id"
	 * isaAttestation for service provider.
	 *
	 * @param id
	 *            the id of the isaAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/isa-attestations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteISAAttestationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete ISAAttestation for service provider : {}", id);
		isaAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /isa-attestations/:id : delete the "id" isaAttestation for merchant.
	 * (Part 1B))
	 *
	 * @param id
	 *            the id of the isaAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/isa-attestations/{id}")
	@Timed
	public ResponseEntity<Void> deleteISAAttestationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete ISAAttestation for merchant : {}", id);
		isaAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
