package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.Location;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.LocationService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Location.
 */
@RestController
@RequestMapping("/api")
public class LocationResource {

	private final Logger log = LoggerFactory.getLogger(LocationResource.class);

	private static final String ENTITY_NAME = "location";

	private final LocationService locationService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public LocationResource(LocationService locationService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.locationService = locationService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /locations/service-provider : Create a new location for service
	 * provider.
	 *
	 * @param location
	 *            the location to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new location, or with status 400 (Bad Request) if the location
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/locations/service-provider")
	@Timed
	public ResponseEntity<Location> createLocationForServiceProvider(@Valid @RequestBody Location location)
			throws URISyntaxException {
		log.debug("REST request to save Location : {}", location);
		if (location.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new location cannot already have an ID"))
					.body(null);
		}
		Location result = locationService.save(location);
		return ResponseEntity.created(new URI("/api/locations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /locations : Create a new location for merchant.
	 *
	 * @param location
	 *            the location to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new location, or with status 400 (Bad Request) if the location
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/locations")
	@Timed
	public ResponseEntity<Location> createLocationForMerchant(@Valid @RequestBody Location location)
			throws URISyntaxException {
		log.debug("REST request to save Location : {}", location);
		if (location.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new location cannot already have an ID"))
					.body(null);
		}
		Location result = locationService.save(location);
		return ResponseEntity.created(new URI("/api/locations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /locations/list/service-provider : Create a list of location for
	 * service provider.
	 *
	 * @param location
	 *            the location to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of location
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/locations/list/service-provider")
	@Timed
	public ResponseEntity<List<Location>> createLocationsForServiceProvider(@Valid @RequestBody List<Location> location)
			throws URISyntaxException {
		log.debug("REST request to save Location : {}", location);
		List<Location> result = locationService.create(location);
		executiveSummaryStatusService.createOrUpdate("LOCATIONS");
		return ResponseEntity.created(new URI("/api/locations/list/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * POST /locations/list : Create a list of location for merchant.
	 *
	 * @param location
	 *            the location to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of location
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/locations/list")
	@Timed
	public ResponseEntity<List<Location>> createLocationsForMerchant(@Valid @RequestBody List<Location> location)
			throws URISyntaxException {
		log.debug("REST request to save Location : {}", location);
		List<Location> result = locationService.create(location);
		executiveSummaryStatusService.createOrUpdate("LOCATIONS");
		return ResponseEntity.created(new URI("/api/locations/list/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /locations/service-provider : Updates an existing location for
	 * service provider.
	 *
	 * @param location
	 *            the location to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         location, or with status 400 (Bad Request) if the location is not
	 *         valid, or with status 500 (Internal Server Error) if the location
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/locations/service-provider")
	@Timed
	public ResponseEntity<Location> updateLocationForServiceProvider(@Valid @RequestBody Location location)
			throws URISyntaxException {
		log.debug("REST request to update Location : {}", location);
		if (location.getId() == null) {
			return createLocationForServiceProvider(location);
		}
		Location result = locationService.save(location);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, location.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /locations : Updates an existing location for merchant.
	 *
	 * @param location
	 *            the location to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         location, or with status 400 (Bad Request) if the location is not
	 *         valid, or with status 500 (Internal Server Error) if the location
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/locations")
	@Timed
	public ResponseEntity<Location> updateLocationForMerchant(@Valid @RequestBody Location location)
			throws URISyntaxException {
		log.debug("REST request to update Location : {}", location);
		if (location.getId() == null) {
			return createLocationForMerchant(location);
		}
		Location result = locationService.save(location);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, location.getId().toString()))
				.body(result);
	}

	/**
	 * GET /locations/service-provider : get all the locations for service
	 * provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of locations
	 *         in body
	 */
	@GetMapping("/locations/service-provider")
	@Timed
	public ResponseEntity<List<Location>> getAllLocationsForServiceProvider(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Locations");
		Page<Location> page = locationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/locations/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /locations : get all the locations for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of locations
	 *         in body
	 */
	@GetMapping("/locations")
	@Timed
	public ResponseEntity<List<Location>> getAllLocationsForMerchant(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Locations");
		Page<Location> page = locationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/locations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /locations/service-provider/:id : get the "id" location for service
	 * provider.
	 *
	 * @param id
	 *            the id of the location to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         location, or with status 404 (Not Found)
	 */
	@GetMapping("/locations/service-provider/{id}")
	@Timed
	public ResponseEntity<Location> getLocationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get Location : {}", id);
		Location location = locationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(location));
	}

	/**
	 * GET /locations/:id : get the "id" location for merchant.
	 *
	 * @param id
	 *            the id of the location to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         location, or with status 404 (Not Found)
	 */
	@GetMapping("/locations/{id}")
	@Timed
	public ResponseEntity<Location> getLocationForMerchant(@PathVariable Long id) {
		log.debug("REST request to get Location : {}", id);
		Location location = locationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(location));
	}

	/**
	 * DELETE /locations/service-provider/:id : delete the "id" location for
	 * service provider.
	 *
	 * @param id
	 *            the id of the location to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/locations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteLocationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete Location : {}", id);
		locationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /locations/:id : delete the "id" location for merchant.
	 *
	 * @param id
	 *            the id of the location to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/locations/{id}")
	@Timed
	public ResponseEntity<Void> deleteLocationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete Location : {}", id);
		locationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
