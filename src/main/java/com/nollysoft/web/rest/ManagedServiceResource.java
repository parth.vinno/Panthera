package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ManagedService;
import com.nollysoft.service.ManagedServiceService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ManagedService.
 */
@RestController
@RequestMapping("/api")
public class ManagedServiceResource {

    private final Logger log = LoggerFactory.getLogger(ManagedServiceResource.class);

    private static final String ENTITY_NAME = "managedService";

    private final ManagedServiceService managedServiceService;

    public ManagedServiceResource(ManagedServiceService managedServiceService) {
        this.managedServiceService = managedServiceService;
    }

    /**
     * POST  /managed-services : Create a new managedService.
     *
     * @param managedService the managedService to create
     * @return the ResponseEntity with status 201 (Created) and with body the new managedService, or with status 400 (Bad Request) if the managedService has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/managed-services")
    @Timed
    public ResponseEntity<ManagedService> createManagedService(@Valid @RequestBody ManagedService managedService) throws URISyntaxException {
        log.debug("REST request to save ManagedService : {}", managedService);
        if (managedService.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new managedService cannot already have an ID")).body(null);
        }
        ManagedService result = managedServiceService.save(managedService);
        return ResponseEntity.created(new URI("/api/managed-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /managed-services : Updates an existing managedService.
     *
     * @param managedService the managedService to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated managedService,
     * or with status 400 (Bad Request) if the managedService is not valid,
     * or with status 500 (Internal Server Error) if the managedService couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/managed-services")
    @Timed
    public ResponseEntity<ManagedService> updateManagedService(@Valid @RequestBody ManagedService managedService) throws URISyntaxException {
        log.debug("REST request to update ManagedService : {}", managedService);
        if (managedService.getId() == null) {
            return createManagedService(managedService);
        }
        ManagedService result = managedServiceService.save(managedService);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, managedService.getId().toString()))
            .body(result);
    }

    /**
     * GET  /managed-services : get all the managedServices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of managedServices in body
     */
    @GetMapping("/managed-services")
    @Timed
    public ResponseEntity<List<ManagedService>> getAllManagedServices(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ManagedServices");
        Page<ManagedService> page = managedServiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/managed-services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /managed-services/:id : get the "id" managedService.
     *
     * @param id the id of the managedService to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the managedService, or with status 404 (Not Found)
     */
    @GetMapping("/managed-services/{id}")
    @Timed
    public ResponseEntity<ManagedService> getManagedService(@PathVariable Long id) {
        log.debug("REST request to get ManagedService : {}", id);
        ManagedService managedService = managedServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(managedService));
    }

    /**
     * DELETE  /managed-services/:id : delete the "id" managedService.
     *
     * @param id the id of the managedService to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/managed-services/{id}")
    @Timed
    public ResponseEntity<Void> deleteManagedService(@PathVariable Long id) {
        log.debug("REST request to delete ManagedService : {}", id);
        managedServiceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
