package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.MerchantBusinessType;
import com.nollysoft.service.MerchantBusinessTypeService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing MerchantBusinessType.
 */
@RestController
@RequestMapping("/api")
public class MerchantBusinessTypeResource {

	private final Logger log = LoggerFactory.getLogger(MerchantBusinessTypeResource.class);

	private static final String ENTITY_NAME = "merchantBusinessType";

	private final MerchantBusinessTypeService merchantBusinessTypeService;

	public MerchantBusinessTypeResource(MerchantBusinessTypeService merchantBusinessTypeService) {
		this.merchantBusinessTypeService = merchantBusinessTypeService;
	}

	/**
	 * POST /merchant-business-types : Create a new merchantBusinessType for
	 * merchant.
	 *
	 * @param merchantBusinessType
	 *            the merchantBusinessType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new merchantBusinessType, or with status 400 (Bad Request) if the
	 *         merchantBusinessType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/merchant-business-types")
	@Timed
	public ResponseEntity<MerchantBusinessType> createMerchantBusinessTypeForMerchant(
			@Valid @RequestBody MerchantBusinessType merchantBusinessType) throws URISyntaxException {
		log.debug("REST request to save MerchantBusinessType : {}", merchantBusinessType);
		if (merchantBusinessType.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new merchantBusinessType cannot already have an ID")).body(null);
		}
		MerchantBusinessType result = merchantBusinessTypeService.save(merchantBusinessType);
		return ResponseEntity.created(new URI("/api/merchant-business-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /merchant-business-types : Updates an existing merchantBusinessType
	 * for merchant.
	 *
	 * @param merchantBusinessType
	 *            the merchantBusinessType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         merchantBusinessType, or with status 400 (Bad Request) if the
	 *         merchantBusinessType is not valid, or with status 500 (Internal
	 *         Server Error) if the merchantBusinessType couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/merchant-business-types")
	@Timed
	public ResponseEntity<MerchantBusinessType> updateMerchantBusinessTypeForMerchant(
			@Valid @RequestBody MerchantBusinessType merchantBusinessType) throws URISyntaxException {
		log.debug("REST request to update MerchantBusinessType : {}", merchantBusinessType);
		if (merchantBusinessType.getId() == null) {
			return createMerchantBusinessTypeForMerchant(merchantBusinessType);
		}
		MerchantBusinessType result = merchantBusinessTypeService.save(merchantBusinessType);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, merchantBusinessType.getId().toString()))
				.body(result);
	}

	/**
	 * GET /merchant-business-types : get all the merchantBusinessTypes for
	 * merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         merchantBusinessTypes in body
	 */
	@GetMapping("/merchant-business-types")
	@Timed
	public List<MerchantBusinessType> getAllMerchantBusinessTypesForMerchant() {
		log.debug("REST request to get all MerchantBusinessTypes");
		return merchantBusinessTypeService.findAll();
	}

	/**
	 * GET /merchant-business-types : get all the
	 * merchantBusinessTypes for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         merchantBusinessTypes in body
	 */
	@GetMapping("/merchant-business-types-pageable")
	@Timed
	public ResponseEntity<List<MerchantBusinessType>> getAllMerchantBusinessTypes(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of MerchantBusinessTypes");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			Page<MerchantBusinessType> page = merchantBusinessTypeService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/merchant-business-types");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /merchant-business-types/:id : get the "id" merchantBusinessType for
	 * merchant.
	 *
	 * @param id
	 *            the id of the merchantBusinessType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         merchantBusinessType, or with status 404 (Not Found)
	 */
	@GetMapping("/merchant-business-types/{id}")
	@Timed
	public ResponseEntity<MerchantBusinessType> getMerchantBusinessTypeForMerchant(@PathVariable Long id) {
		log.debug("REST request to get MerchantBusinessType : {}", id);
		MerchantBusinessType merchantBusinessType = merchantBusinessTypeService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(merchantBusinessType));
	}

	/**
	 * DELETE /merchant-business-types/:id : delete the "id"
	 * merchantBusinessType for merchant.
	 *
	 * @param id
	 *            the id of the merchantBusinessType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/merchant-business-types/{id}")
	@Timed
	public ResponseEntity<Void> deleteMerchantBusinessTypeForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete MerchantBusinessType : {}", id);
		merchantBusinessTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
