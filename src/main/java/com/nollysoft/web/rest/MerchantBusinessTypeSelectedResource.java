package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.MerchantBusinessTypeSelected;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.MerchantBusinessTypeSelectedService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing MerchantBusinessTypeSelected.
 */
@RestController
@RequestMapping("/api")
public class MerchantBusinessTypeSelectedResource {

	private final Logger log = LoggerFactory.getLogger(MerchantBusinessTypeSelectedResource.class);

	private static final String ENTITY_NAME = "merchantBusinessTypeSelected";

	private final MerchantBusinessTypeSelectedService merchantBusinessTypeSelectedService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public MerchantBusinessTypeSelectedResource(
			MerchantBusinessTypeSelectedService merchantBusinessTypeSelectedService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.merchantBusinessTypeSelectedService = merchantBusinessTypeSelectedService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /merchant-business-type-selected : Create a new
	 * merchantBusinessTypeSelected for merchant.
	 *
	 * @param merchantBusinessTypeSelected
	 *            the merchantBusinessTypeSelected to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new merchantBusinessTypeSelected, or with status 400 (Bad
	 *         Request) if the merchantBusinessTypeSelected has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/merchant-business-type-selected")
	@Timed
	public ResponseEntity<MerchantBusinessTypeSelected> createMerchantBusinessTypeSelectedForMerchant(
			@RequestBody MerchantBusinessTypeSelected merchantBusinessTypeSelected) throws URISyntaxException {
		log.debug("REST request to save MerchantBusinessTypeSelected : {}", merchantBusinessTypeSelected);
		if (merchantBusinessTypeSelected.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new merchantBusinessTypeSelected cannot already have an ID")).body(null);
		}
		MerchantBusinessTypeSelected result = merchantBusinessTypeSelectedService.save(merchantBusinessTypeSelected);
		return ResponseEntity.created(new URI("/api/merchant-business-type-selected/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}
	
	/**
	 * POST /merchant-business-type-selected/list : Create list of
	 * merchantBusinessTypeSelected for service provider.
	 *
	 * @param list of merchantBusinessTypeSelected to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of merchantBusinessTypesSelected
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/merchant-business-type-selected/list")
	@Timed
	public ResponseEntity<List<MerchantBusinessTypeSelected>> createMerchantBusinessTypesSelectedForServiceProvider(
			@RequestBody List<MerchantBusinessTypeSelected> merchantBusinessTypesSelected) throws URISyntaxException {
		log.debug("REST request to save MerchantBusinessTypeSelected : {}", merchantBusinessTypesSelected);
		List<MerchantBusinessTypeSelected> result = merchantBusinessTypeSelectedService.create(merchantBusinessTypesSelected);
		executiveSummaryStatusService.createOrUpdate("MERCHANT_BUSINESS");
		return ResponseEntity
				.created(new URI("/api/merchant-business-type-selected/list/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /merchant-business-type-selected : Updates an existing
	 * merchantBusinessTypeSelected for merchant.
	 *
	 * @param merchantBusinessTypeSelected
	 *            the merchantBusinessTypeSelected to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         merchantBusinessTypeSelected, or with status 400 (Bad Request) if
	 *         the merchantBusinessTypeSelected is not valid, or with status 500
	 *         (Internal Server Error) if the merchantBusinessTypeSelected
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/merchant-business-type-selected")
	@Timed
	public ResponseEntity<MerchantBusinessTypeSelected> updateMerchantBusinessTypeSelectedForMerchant(
			@RequestBody MerchantBusinessTypeSelected merchantBusinessTypeSelected) throws URISyntaxException {
		log.debug("REST request to update MerchantBusinessTypeSelected : {}", merchantBusinessTypeSelected);
		if (merchantBusinessTypeSelected.getId() == null) {
			return createMerchantBusinessTypeSelectedForMerchant(merchantBusinessTypeSelected);
		}
		MerchantBusinessTypeSelected result = merchantBusinessTypeSelectedService.save(merchantBusinessTypeSelected);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, merchantBusinessTypeSelected.getId().toString()))
				.body(result);
	}

	/**
	 * GET /merchant-business-type-selected : get all the
	 * merchantBusinessTypeSelected for merchant.
	 *
	 * @param filter
	 *            the filter of the request
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         merchantBusinessTypeSelecteds in body
	 */
	@GetMapping("/merchant-business-type-selected")
	@Timed
	public List<MerchantBusinessTypeSelected> getAllMerchantBusinessTypesSelectedForMerchant(
			@RequestParam(required = false) String filter) {
		if ("merchantbusinesstypeid-is-null".equals(filter)) {
			log.debug("REST request to get all MerchantBusinessTypeSelecteds where merchantBusinessTypeId is null");
			return merchantBusinessTypeSelectedService.findAllWhereMerchantBusinessTypeIdIsNull();
		}
		log.debug("REST request to get all MerchantBusinessTypeSelecteds");
		return merchantBusinessTypeSelectedService.findAll();
	}

	/**
	 * GET /merchant-business-type-selected/:id : get the "id"
	 * merchantBusinessTypeSelected for merchant.
	 *
	 * @param id
	 *            the id of the merchantBusinessTypeSelected to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         merchantBusinessTypeSelected, or with status 404 (Not Found)
	 */
	@GetMapping("/merchant-business-type-selected/{id}")
	@Timed
	public ResponseEntity<MerchantBusinessTypeSelected> getMerchantBusinessTypeSelectedForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get MerchantBusinessTypeSelected : {}", id);
		MerchantBusinessTypeSelected merchantBusinessTypeSelected = merchantBusinessTypeSelectedService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(merchantBusinessTypeSelected));
	}

	/**
	 * DELETE /merchant-business-type-selected/:id : delete the "id"
	 * merchantBusinessTypeSelected for merchant.
	 *
	 * @param id
	 *            the id of the merchantBusinessTypeSelected to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/merchant-business-type-selected/{id}")
	@Timed
	public ResponseEntity<Void> deleteMerchantBusinessTypeSelectedForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete MerchantBusinessTypeSelected : {}", id);
		merchantBusinessTypeSelectedService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
