package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.NonApplicabilityExplanation;
import com.nollysoft.service.NonApplicabilityExplanationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing NonApplicabilityExplanation.
 */
@RestController
@RequestMapping("/api")
public class NonApplicabilityExplanationResource {

	private final Logger log = LoggerFactory.getLogger(NonApplicabilityExplanationResource.class);

	private static final String ENTITY_NAME = "nonApplicabilityExplanation";

	private final NonApplicabilityExplanationService nonApplicabilityExplanationService;

	public NonApplicabilityExplanationResource(NonApplicabilityExplanationService nonApplicabilityExplanationService) {
		this.nonApplicabilityExplanationService = nonApplicabilityExplanationService;
	}

	/**
	 * POST /non-applicability-explanations/service-provider : Create a new
	 * nonApplicabilityExplanation for service provider.
	 *
	 * @param nonApplicabilityExplanation
	 *            the nonApplicabilityExplanation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         nonApplicabilityExplanation, or with status 400 (Bad Request) if the
	 *         nonApplicabilityExplanation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/non-applicability-explanations/service-provider")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> createNonApplicabilityExplanationForServiceProvider(
			@Valid @RequestBody NonApplicabilityExplanation nonApplicabilityExplanation) throws URISyntaxException {
		log.debug("REST request to save NonApplicabilityExplanation : {}", nonApplicabilityExplanation);
		if (nonApplicabilityExplanation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new nonApplicabilityExplanation cannot already have an ID")).body(null);
		}
		NonApplicabilityExplanation result = nonApplicabilityExplanationService.save(nonApplicabilityExplanation);
		return ResponseEntity.created(new URI("/api/non-applicability-explanations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}
	
	/**
	 * POST /non-applicability-explanations : Create a new
	 * nonApplicabilityExplanation for merchant.
	 *
	 * @param nonApplicabilityExplanation
	 *            the nonApplicabilityExplanation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         nonApplicabilityExplanation, or with status 400 (Bad Request) if the
	 *         nonApplicabilityExplanation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/non-applicability-explanations")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> createNonApplicabilityExplanationForMerchant(
			@Valid @RequestBody NonApplicabilityExplanation nonApplicabilityExplanation) throws URISyntaxException {
		log.debug("REST request to save NonApplicabilityExplanation : {}", nonApplicabilityExplanation);
		if (nonApplicabilityExplanation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new nonApplicabilityExplanation cannot already have an ID")).body(null);
		}
		NonApplicabilityExplanation result = nonApplicabilityExplanationService.save(nonApplicabilityExplanation);
		return ResponseEntity.created(new URI("/api/non-applicability-explanations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /non-applicability-explanations/service-provider : Updates an existing
	 * nonApplicabilityExplanation for service provider.
	 *
	 * @param nonApplicabilityExplanation
	 *            the nonApplicabilityExplanation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         nonApplicabilityExplanation, or with status 400 (Bad Request) if the
	 *         nonApplicabilityExplanation is not valid, or with status 500
	 *         (Internal Server Error) if the nonApplicabilityExplanation couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/non-applicability-explanations/service-provider")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> updateNonApplicabilityExplanationForServiceProvider(
			@Valid @RequestBody NonApplicabilityExplanation nonApplicabilityExplanation) throws URISyntaxException {
		log.debug("REST request to update NonApplicabilityExplanation : {}", nonApplicabilityExplanation);
		if (nonApplicabilityExplanation.getId() == null) {
			return createNonApplicabilityExplanationForServiceProvider(nonApplicabilityExplanation);
		}
		NonApplicabilityExplanation result = nonApplicabilityExplanationService.save(nonApplicabilityExplanation);
		return ResponseEntity.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nonApplicabilityExplanation.getId().toString()))
				.body(result);
	}
	
	/**
	 * PUT /non-applicability-explanations : Updates an existing
	 * nonApplicabilityExplanation for merchant.
	 *
	 * @param nonApplicabilityExplanation
	 *            the nonApplicabilityExplanation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         nonApplicabilityExplanation, or with status 400 (Bad Request) if the
	 *         nonApplicabilityExplanation is not valid, or with status 500
	 *         (Internal Server Error) if the nonApplicabilityExplanation couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/non-applicability-explanations")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> updateNonApplicabilityExplanationForMerchant(
			@Valid @RequestBody NonApplicabilityExplanation nonApplicabilityExplanation) throws URISyntaxException {
		log.debug("REST request to update NonApplicabilityExplanation : {}", nonApplicabilityExplanation);
		if (nonApplicabilityExplanation.getId() == null) {
			return createNonApplicabilityExplanationForMerchant(nonApplicabilityExplanation);
		}
		NonApplicabilityExplanation result = nonApplicabilityExplanationService.save(nonApplicabilityExplanation);
		return ResponseEntity.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nonApplicabilityExplanation.getId().toString()))
				.body(result);
	}
	
	/**
	 * GET /non-applicability-explanations/service-provider : get all the
	 * nonApplicabilityExplanations for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         nonApplicabilityExplanations in body
	 */
	@GetMapping("/non-applicability-explanations/service-provider")
	@Timed
	public List<NonApplicabilityExplanation> getAllNonApplicabilityExplanationsForServiceProvider() {
		log.debug("REST request to get all NonApplicabilityExplanations");
		return nonApplicabilityExplanationService.findAll();
	}
	
	/**
	 * GET /non-applicability-explanations : get all the
	 * nonApplicabilityExplanations for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         nonApplicabilityExplanations in body
	 */
	@GetMapping("/non-applicability-explanations")
	@Timed
	public List<NonApplicabilityExplanation> getAllNonApplicabilityExplanationsForMerchant() {
		log.debug("REST request to get all NonApplicabilityExplanations");
		return nonApplicabilityExplanationService.findAll();
	}

	/**
	 * GET /non-applicability-explanations/service-provider/:id : get the "id"
	 * nonApplicabilityExplanation for service provider.
	 *
	 * @param id
	 *            the id of the nonApplicabilityExplanation to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         nonApplicabilityExplanation, or with status 404 (Not Found)
	 */
	@GetMapping("/non-applicability-explanations/service-provider/{id}")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> getNonApplicabilityExplanationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get NonApplicabilityExplanation : {}", id);
		NonApplicabilityExplanation nonApplicabilityExplanation = nonApplicabilityExplanationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(nonApplicabilityExplanation));
	}
	
	/**
	 * GET /non-applicability-explanations/:id : get the "id"
	 * nonApplicabilityExplanation for merchant.
	 *
	 * @param id
	 *            the id of the nonApplicabilityExplanation to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         nonApplicabilityExplanation, or with status 404 (Not Found)
	 */
	@GetMapping("/non-applicability-explanations/{id}")
	@Timed
	public ResponseEntity<NonApplicabilityExplanation> getNonApplicabilityExplanationForMerchant(@PathVariable Long id) {
		log.debug("REST request to get NonApplicabilityExplanation : {}", id);
		NonApplicabilityExplanation nonApplicabilityExplanation = nonApplicabilityExplanationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(nonApplicabilityExplanation));
	}

	/**
	 * DELETE /non-applicability-explanations/service-provider/:id : delete the "id"
	 * nonApplicabilityExplanation for service provider.
	 *
	 * @param id
	 *            the id of the nonApplicabilityExplanation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/non-applicability-explanations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteNonApplicabilityExplanationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete NonApplicabilityExplanation : {}", id);
		nonApplicabilityExplanationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	
	/**
	 * DELETE /non-applicability-explanations/:id : delete the "id"
	 * nonApplicabilityExplanation for merchant.
	 *
	 * @param id
	 *            the id of the nonApplicabilityExplanation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/non-applicability-explanations/{id}")
	@Timed
	public ResponseEntity<Void> deleteNonApplicabilityExplanationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete NonApplicabilityExplanation : {}", id);
		nonApplicabilityExplanationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
