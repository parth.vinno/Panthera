package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.NonCompliantRequirementActionPlan;
import com.nollysoft.service.NonCompliantRequirementActionPlanService;
import com.nollysoft.service.dto.NonCompliantRequirementActionPlanDTO;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.undertow.util.BadRequestException;

/**
 * REST controller for managing NonCompliantRequirementActionPlan.
 */
@RestController
@RequestMapping("/api")
public class NonCompliantRequirementActionPlanResource {
	private final Logger log = LoggerFactory.getLogger(NonCompliantRequirementActionPlanResource.class);

	private static final String ENTITY_NAME = "nonCompliantRequirementActionPlan";

	private final NonCompliantRequirementActionPlanService nonCompliantRequirementActionPlanService;

	public NonCompliantRequirementActionPlanResource(
			NonCompliantRequirementActionPlanService nonCompliantRequirementActionPlanService) {
		this.nonCompliantRequirementActionPlanService = nonCompliantRequirementActionPlanService;
	}

	/**
	 * POST /non-compliant-requirement-action-plans/service-provider : Create a new
	 * nonCompliantRequirementActionPlan for service provider)
	 *
	 * @param nonCompliantRequirementActionPlan
	 *            the nonCompliantRequirementActionPlan to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         nonCompliantRequirementActionPlan, or with status 400 (Bad Request)
	 *         if the nonCompliantRequirementActionPlan has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/non-compliant-requirement-action-plans/service-provider")
	@Timed
	public ResponseEntity<NonCompliantRequirementActionPlanDTO> createNonCompliantRequirementActionPlanForServiceProvider(
			@RequestBody NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to save NonCompliantRequirementActionPlan for service provider: {}",
				nonCompliantRequirementActionPlan);
		if (nonCompliantRequirementActionPlan.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new nonCompliantRequirementActionPlan cannot already have an ID")).body(null);
		}
		NonCompliantRequirementActionPlan result = nonCompliantRequirementActionPlanService
				.save(nonCompliantRequirementActionPlan);
		return ResponseEntity
				.created(new URI("/api/non-compliant-requirement-action-plans/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new NonCompliantRequirementActionPlanDTO(result));
	}

	/**
	 * POST /non-compliant-requirement-action-plans : Create a new
	 * nonCompliantRequirementActionPlan for merchant.
	 *
	 * @param nonCompliantRequirementActionPlan
	 *            the nonCompliantRequirementActionPlan to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         nonCompliantRequirementActionPlan, or with status 400 (Bad Request)
	 *         if the nonCompliantRequirementActionPlan has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/non-compliant-requirement-action-plans")
	@Timed
	public ResponseEntity<NonCompliantRequirementActionPlanDTO> createNonCompliantRequirementActionPlanForMerchant(
			@RequestBody NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to save NonCompliantRequirementActionPlan for merchant: {}",
				nonCompliantRequirementActionPlan);
		if (nonCompliantRequirementActionPlan.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new nonCompliantRequirementActionPlan cannot already have an ID")).body(null);
		}
		NonCompliantRequirementActionPlan result = nonCompliantRequirementActionPlanService
				.save(nonCompliantRequirementActionPlan);
		return ResponseEntity.created(new URI("/api/non-compliant-requirement-action-plans/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new NonCompliantRequirementActionPlanDTO(result));
	}

	/**
	 * POST /list/non-compliant-requirement-action-plans/service-provider : Create
	 * list of nonCompliantRequirementActionPlan for service provider)
	 *
	 * @param list
	 *            of nonCompliantRequirementActionPlan the
	 *            nonCompliantRequirementActionPlan to create
	 * @return the ResponseEntity with status 201 (Created) and with body the list
	 *         of nonCompliantRequirementActionPlan, 
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/list/non-compliant-requirement-action-plans/service-provider")
	@Timed
	public ResponseEntity<List<NonCompliantRequirementActionPlanDTO>> createNonCompliantRequirementActionPlansForServiceProvider(
			@RequestBody List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlans)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to save NonCompliantRequirementActionPlan for service provider: {}",
				nonCompliantRequirementActionPlans);
		List<NonCompliantRequirementActionPlanDTO> result = nonCompliantRequirementActionPlanService
				.saveOrUpdate(nonCompliantRequirementActionPlans);
		return ResponseEntity.created(new URI("/api/list/non-compliant-requirement-action-plans/service-provider"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body((result));
	}
	
	/**
	 * POST /list/non-compliant-requirement-action-plans : Create
	 * list of nonCompliantRequirementActionPlan for merchant)
	 *
	 * @param list
	 *            of nonCompliantRequirementActionPlan the
	 *            nonCompliantRequirementActionPlan to create
	 * @return the ResponseEntity with status 201 (Created) and with body the list
	 *         of nonCompliantRequirementActionPlan, 
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PostMapping("/list/non-compliant-requirement-action-plans")
	@Timed
	public ResponseEntity<List<NonCompliantRequirementActionPlanDTO>> createNonCompliantRequirementActionPlansForMerchant(
			@RequestBody List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlans)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to save NonCompliantRequirementActionPlan for service provider: {}",
				nonCompliantRequirementActionPlans);
		List<NonCompliantRequirementActionPlanDTO> result = nonCompliantRequirementActionPlanService
				.saveOrUpdate(nonCompliantRequirementActionPlans);
		return ResponseEntity.created(new URI("/api/list/non-compliant-requirement-action-plans"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /non-compliant-requirement-action-plans/service-provider : Updates an
	 * existing nonCompliantRequirementActionPlan for service provider.
	 *
	 * @param nonCompliantRequirementActionPlan
	 *            the nonCompliantRequirementActionPlan to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         nonCompliantRequirementActionPlan, or with status 400 (Bad Request)
	 *         if the nonCompliantRequirementActionPlan is not valid, or with status
	 *         500 (Internal Server Error) if the nonCompliantRequirementActionPlan
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PutMapping("/non-compliant-requirement-action-plans/service-provider")
	@Timed
	public ResponseEntity<NonCompliantRequirementActionPlanDTO> updateNonCompliantRequirementActionPlanForServiceProvider(
			@RequestBody NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to update NonCompliantRequirementActionPlan for service provider : {}",
				nonCompliantRequirementActionPlan);
		if (nonCompliantRequirementActionPlan.getId() == null) {
			return createNonCompliantRequirementActionPlanForServiceProvider(nonCompliantRequirementActionPlan);
		}
		NonCompliantRequirementActionPlan result = nonCompliantRequirementActionPlanService
				.save(nonCompliantRequirementActionPlan);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nonCompliantRequirementActionPlan.getId().toString()))
				.body(new NonCompliantRequirementActionPlanDTO(result));
	}

	/**
	 * PUT /non-compliant-requirement-action-plans : Updates an existing
	 * nonCompliantRequirementActionPlan for merchant.
	 *
	 * @param nonCompliantRequirementActionPlan
	 *            the nonCompliantRequirementActionPlan to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         nonCompliantRequirementActionPlan, or with status 400 (Bad Request)
	 *         if the nonCompliantRequirementActionPlan is not valid, or with status
	 *         500 (Internal Server Error) if the nonCompliantRequirementActionPlan
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestException
	 */
	@PutMapping("/non-compliant-requirement-action-plans")
	@Timed
	public ResponseEntity<NonCompliantRequirementActionPlanDTO> updateNonCompliantRequirementActionPlanForMerchant(
			@RequestBody NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan)
			throws URISyntaxException, BadRequestException {
		log.debug("REST request to update NonCompliantRequirementActionPlan for merchant : {}",
				nonCompliantRequirementActionPlan);
		if (nonCompliantRequirementActionPlan.getId() == null) {
			return createNonCompliantRequirementActionPlanForMerchant(nonCompliantRequirementActionPlan);
		}
		NonCompliantRequirementActionPlan result = nonCompliantRequirementActionPlanService
				.save(nonCompliantRequirementActionPlan);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nonCompliantRequirementActionPlan.getId().toString()))
				.body(new NonCompliantRequirementActionPlanDTO(result));
	}

	/**
	 * GET /non-compliant-requirement-action-plans/service-provider : get all the
	 * nonCompliantRequirementActionPlan for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         nonCompliantRequirementActionPlans in body
	 */
	@GetMapping("/non-compliant-requirement-action-plans/service-provider")
	@Timed
	public ResponseEntity<List<NonCompliantRequirementActionPlanDTO>> getAllNonCompliantRequirementActionPlanesForServiceProvider() {
		log.debug("REST request to get a page of NonCompliantRequirementActionPlans for service provider");
		List<NonCompliantRequirementActionPlanDTO> list = nonCompliantRequirementActionPlanService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /non-compliant-requirement-action-plans : get all the
	 * nonCompliantRequirementActionPlanes for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         nonCompliantRequirementActionPlanes in body
	 */
	@GetMapping("/non-compliant-requirement-action-plans")
	@Timed
	public ResponseEntity<List<NonCompliantRequirementActionPlanDTO>> getAllNonCompliantRequirementActionPlanesForMerchant() {
		log.debug("REST request to get a page of NonCompliantRequirementActionPlans for merchant");
		List<NonCompliantRequirementActionPlanDTO> list = nonCompliantRequirementActionPlanService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /non-compliant-requirement-action-plans/service-provider/:id : delete
	 * the "id" nonCompliantRequirementActionPlan for service provider.
	 *
	 * @param id
	 *            the id of the nonCompliantRequirementActionPlan to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/non-compliant-requirement-action-plans/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteNonCompliantRequirementActionPlanForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete NonCompliantRequirementActionPlan for service provider : {}", id);
		nonCompliantRequirementActionPlanService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /non-compliant-requirement-action-plans/:id : delete the "id"
	 * nonCompliantRequirementActionPlan for merchant.
	 * 
	 * @param id
	 *            the id of the nonCompliantRequirementActionPlan to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/non-compliant-requirement-action-plans/{id}")
	@Timed
	public ResponseEntity<Void> deleteNonCompliantRequirementActionPlanForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete NonCompliantRequirementActionPlan for merchant : {}", id);
		nonCompliantRequirementActionPlanService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
