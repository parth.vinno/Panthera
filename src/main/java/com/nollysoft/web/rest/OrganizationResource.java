package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.Organization;
import com.nollysoft.domain.enumeration.OrganizationSection;
import com.nollysoft.service.OrganizationService;
import com.nollysoft.service.OrganizationSummaryStatusService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Organization.
 */
@RestController
@RequestMapping("/api")
public class OrganizationResource {

    private final Logger log = LoggerFactory.getLogger(OrganizationResource.class);

    private static final String ENTITY_NAME = "organization";

    private final OrganizationService organizationService;
    private final OrganizationSummaryStatusService organizationSummaryStatusService;

    public OrganizationResource(OrganizationService organizationService, OrganizationSummaryStatusService organizationSummaryStatusService) {
        this.organizationService = organizationService;
        this.organizationSummaryStatusService = organizationSummaryStatusService;
    }

    /**
     * POST  /organizations/service-provider : Create a new organization for service provider. This is for module part 1A.
     *
     * @param organization the organization to create
     * @return the ResponseEntity with status 201 (Created) and with body the new organization, or with status 400 (Bad Request) if the organization has already an ID, this end-point requires authorization or else return with status 401 (Un-Authorized)
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organizations/service-provider")
    @Timed
    public ResponseEntity<Organization> createOrganizationForServiceProvider(@RequestBody Organization organization) throws URISyntaxException {
        log.debug("REST request to save Organization for service provider : {}", organization);
        if (organization.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new organization cannot already have an ID")).body(null);
        }
        Organization result = organizationService.save(organization);
        organizationSummaryStatusService.createOrUpdate(OrganizationSection.ORGANIZATION);
            return ResponseEntity.created(new URI("/api/organizations/service-provider" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                    .body(result);
    }

    /**
     * POST  /organizations : Create a new organization for merchant. This is for module part 1A.
     *
     * @param organization the organization to create
     * @return the ResponseEntity with status 201 (Created) and with body the new organization, or with status 400 (Bad Request) if the organization has already an ID, this end-point requires authorization or else return with status 401 (Un-Authorized)
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organizations")
    @Timed
    public ResponseEntity<Organization> createOrganizationForMerchant(@RequestBody Organization organization) throws URISyntaxException {
        log.debug("REST request to save Organization for merchant: {}", organization);
        if (organization.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new organization cannot already have an ID")).body(null);
        }
        Organization result = organizationService.save(organization);
        organizationSummaryStatusService.createOrUpdate(OrganizationSection.ORGANIZATION);
        return ResponseEntity.created(new URI("/api/organizations" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);

    }

    /**
     * PUT  /organizations/service-provider : Updates an existing organization for service provider. This is for module part 1A.
     *
     * @param organization the organization to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated organization,
     * or with status 400 (Bad Request) if the organization is not valid,
     * or with status 401 (Un-Authorized) if un-authorized,
     * or with status 500 (Internal Server Error) if the organization couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/organizations/service-provider")
    @Timed
    public ResponseEntity<Organization> updateOrganizationForServiceProvider(@RequestBody Organization organization) throws URISyntaxException {
        log.debug("REST request to update Organization for service provider: {}", organization);
        if (organization.getId() == null) {
            return createOrganizationForServiceProvider(organization);
        }
        Organization result = organizationService.save(organization);
        organizationSummaryStatusService.createOrUpdate(OrganizationSection.ORGANIZATION);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, organization.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /organizations : Updates an existing organization for merchant. This is for module part 1A.
     *
     * @param organization the organization to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated organization,
     * or with status 400 (Bad Request) if the organization is not valid,
     * or with status 401 (Un-Authorized) if un-authorized,
     * or with status 500 (Internal Server Error) if the organization couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/organizations")
    @Timed
    public ResponseEntity<Organization> updateOrganizationForMerchant(@RequestBody Organization organization) throws URISyntaxException {
        log.debug("REST request to update Organization for merchant : {}", organization);
        if (organization.getId() == null) {
            return createOrganizationForMerchant(organization);
        }
        Organization result = organizationService.save(organization);
        organizationSummaryStatusService.createOrUpdate(OrganizationSection.ORGANIZATION);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, organization.getId().toString()))
                .body(result);
    }

    /**
     * GET  /organizations/service-provider : get all the organizations for service provider. This is for module part 1A.
     *
     * @param pageable the pagination information, default page number is 0 and limit is 20
     * @return the ResponseEntity with status 200 (OK) and the list of organizations in body
     */
    @GetMapping("/organizations/service-provider")
    @Timed
    public ResponseEntity<List<Organization>> getAllOrganizationsForServiceProvider(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Organizations for service provider");
        Page<Organization> page = organizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/organizations/service-provider");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /organizations : get all the organizations for merchant. This is for module part 1A.
     *
     * @param pageable the pagination information, default page number is 0 and limit is 20
     * @return the ResponseEntity with status 200 (OK) and the list of organizations in body
     */
    @GetMapping("/organizations")
    @Timed
    public ResponseEntity<List<Organization>> getAllOrganizationsForMerchant(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Organizations for merchant");
        Page<Organization> page = organizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /organizations/service-provider/:id : get the "id" organization for service provider. This is for module part 1A.
     *
     * @param id the id of the organization to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the organization, or with status 404 (Not Found) or with 401 (un-authorised) if unauthorized
     */
    @GetMapping("/organizations/service-provider/{id}")
    @Timed
    public ResponseEntity<Organization> getOrganizationForServiceProvider(@PathVariable Long id) {
        log.debug("REST request to get Organization for service provider: {}", id);
            Organization organization = organizationService.findOne(id);
            return ResponseUtil.wrapOrNotFound(Optional.ofNullable(organization));
    }

    /**
     * GET  /organizations/:id : get the "id" organization for merchant. This is for module part 1A.
     *
     * @param id the id of the organization to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the organization, or with status 404 (Not Found) or with 401 (un-authorised) if unauthorized
     */
    @GetMapping("/organizations/{id}")
    @Timed
    public ResponseEntity<Organization> getOrganizationForMerchant(@PathVariable Long id) {
        log.debug("REST request to get Organization for merchant : {}", id);
        Organization organization = organizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(organization));

    }

    /**
     * DELETE  /organizations/service-provider/:id : delete the "id" organization for service provider. This is for module part 1A.
     *
     * @param id the id of the organization to delete
     * @return the ResponseEntity with status 200 (OK), or with status 404 (Not Found) or with 401 (un-authorised) if unauthorized, or with 500 (ISE) if failed to delete
     */
    @DeleteMapping("/organizations/service-provider/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrganizationForServiceProvider(@PathVariable Long id) {
        log.debug("REST request to delete Organization for service provider: {}", id);
        organizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * DELETE  /organizations/:id : delete the "id" organization for merchant. This is for module part 1A.
     *
     * @param id the id of the organization to delete
     * @return the ResponseEntity with status 200 (OK), or with status 404 (Not Found) or with 401 (un-authorised) if unauthorized, or with 500 (ISE) if failed to delete
     */
    @DeleteMapping("/organizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrganizationForMerchant(@PathVariable Long id) {
        log.debug("REST request to delete Organization for merchant : {}", id);
        organizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

    }
}
