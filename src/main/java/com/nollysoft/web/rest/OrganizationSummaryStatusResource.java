package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.OrganizationSummaryStatus;
import com.nollysoft.domain.enumeration.OrganizationSection;
import com.nollysoft.service.OrganizationSummaryStatusService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing OrganizationSummaryStatus.
 */
@RestController
@RequestMapping("/api")
public class OrganizationSummaryStatusResource {

    private final Logger log = LoggerFactory.getLogger(OrganizationSummaryStatusResource.class);

    private static final String ENTITY_NAME = "organizationSummaryStatus";

    private final OrganizationSummaryStatusService organizationSummaryStatusService;

    public OrganizationSummaryStatusResource(OrganizationSummaryStatusService organizationSummaryStatusService) {
        this.organizationSummaryStatusService = organizationSummaryStatusService;
    }

    /**
	 * POST /organization-summary-status : Create a new OrganizationSummaryStatus.
	 *
	 * @param section
	 *            the OrganizationSummaryStatusSection to create / update
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         OrganizationSummaryStatus
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/organization-summary-status/{section}")
	@Timed
	public ResponseEntity<OrganizationSummaryStatus> createOrUpdateExecutiveSummaryStatus(
			@PathVariable("section") String section) throws URISyntaxException {
		log.debug("REST request to save ExecutiveSummaryStatus : {}", section);
		OrganizationSummaryStatus result = organizationSummaryStatusService.createOrUpdate(OrganizationSection.valueOf(section));
		return ResponseEntity.created(new URI("/api/executive-summary-statuses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

    /**
     * GET  /organization-summary-status : get the organizationSummaryStatus.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the organizationSummaryStatus
     */
    @GetMapping("/organization-summary-status")
    @Timed
    public ResponseEntity<OrganizationSummaryStatus> getOrganizationSummaryStatus() {
        OrganizationSummaryStatus organizationSummaryStatus = organizationSummaryStatusService.getOrganizationSummaryStatus();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(organizationSummaryStatus));
    }

    /**
	 * GET /organization-summary-status : get all the organizationSummaryStatus.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         organizationSummaryStatus in body
	 */
	@GetMapping("/organization-summary-statuses")
	@Timed
	public List<OrganizationSummaryStatus> getAllOrganizationSummaryStatuses() {
		log.debug("REST request to get all OrganizationSummaryStatus");
		return organizationSummaryStatusService.findAll();
	}
}
