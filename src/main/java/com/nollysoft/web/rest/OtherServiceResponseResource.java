package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.OtherServiceResponse;
import com.nollysoft.service.OtherServiceResponseService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing OtherServiceResponse.
 */
@RestController
@RequestMapping("/api")
public class OtherServiceResponseResource {

	private final Logger log = LoggerFactory.getLogger(OtherServiceResponseResource.class);

	private static final String ENTITY_NAME = "otherServiceResponse";

	private final OtherServiceResponseService otherServiceResponseService;

	public OtherServiceResponseResource(OtherServiceResponseService otherServiceResponseService) {
		this.otherServiceResponseService = otherServiceResponseService;
	}

	/**
	 * POST /other-service-responses/service-provider : Create a new
	 * otherServiceResponse for service provider.
	 *
	 * @param otherServiceResponse
	 *            the otherServiceResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new otherServiceResponse, or with status 400 (Bad Request) if the
	 *         otherServiceResponse has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/other-service-responses/service-provider")
	@Timed
	public ResponseEntity<OtherServiceResponse> createOtherServiceResponseForServiceProvider(
			@Valid @RequestBody OtherServiceResponse otherServiceResponse) throws URISyntaxException {
		log.debug("REST request to save OtherServiceResponse : {}", otherServiceResponse);
		if (otherServiceResponse.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new otherServiceResponse cannot already have an ID")).body(null);
		}
		OtherServiceResponse result = otherServiceResponseService.save(otherServiceResponse);
		return ResponseEntity.created(new URI("/api/other-service-responses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /selected-service-types/list/service-provider : Create a list of
	 * selectedServiceType for service provider.
	 *
	 * @param list
	 *            of selectedServiceType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of selectedServiceType
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/other-service-responses/list/service-provider/{scope}")
	@Timed
	public ResponseEntity<List<OtherServiceResponse>> createOtherServiceResponseForServiceProvider(
			@RequestBody List<OtherServiceResponse> otherServiceResponse, @PathVariable("scope") String scope)
			throws URISyntaxException {
		log.debug("REST request to save SelectedServiceType : {}", otherServiceResponse);
		List<OtherServiceResponse> result = otherServiceResponseService.create(otherServiceResponse, scope);
		return ResponseEntity.created(new URI("/api/other-service-responses/list/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}
	
	/**
	 * PUT /other-service-responses/service-provider : Updates an existing
	 * otherServiceResponse for service provider.
	 *
	 * @param otherServiceResponse
	 *            the otherServiceResponse to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         otherServiceResponse, or with status 400 (Bad Request) if the
	 *         otherServiceResponse is not valid, or with status 500 (Internal
	 *         Server Error) if the otherServiceResponse couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/other-service-responses/service-provider")
	@Timed
	public ResponseEntity<OtherServiceResponse> updateOtherServiceResponseForServiceProvider(
			@Valid @RequestBody OtherServiceResponse otherServiceResponse) throws URISyntaxException {
		log.debug("REST request to update OtherServiceResponse : {}", otherServiceResponse);
		if (otherServiceResponse.getId() == null) {
			return createOtherServiceResponseForServiceProvider(otherServiceResponse);
		}
		OtherServiceResponse result = otherServiceResponseService.save(otherServiceResponse);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, otherServiceResponse.getId().toString()))
				.body(result);
	}

	/**
	 * GET /other-service-responses/service-provider : get all the
	 * otherServiceResponses for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         otherServiceResponses in body
	 */
	@GetMapping("/other-service-responses/service-provider")
	@Timed
	public List<OtherServiceResponse> getAllOtherServiceResponsesForServiceProvider() {
		log.debug("REST request to get all OtherServiceResponses");
		return otherServiceResponseService.findAll();
	}

	/**
	 * GET /other-service-responses/service-provider/:id : get the "id"
	 * otherServiceResponse for service provider.
	 *
	 * @param id
	 *            the id of the otherServiceResponse to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         otherServiceResponse, or with status 404 (Not Found)
	 */
	@GetMapping("/other-service-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<OtherServiceResponse> getOtherServiceResponseForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get OtherServiceResponse : {}", id);
		OtherServiceResponse otherServiceResponse = otherServiceResponseService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(otherServiceResponse));
	}

	/**
	 * DELETE /other-service-responses/service-provider/:id : delete the "id"
	 * otherServiceResponse for service provider.
	 *
	 * @param id
	 *            the id of the otherServiceResponse to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/other-service-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteOtherServiceResponseForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete OtherServiceResponse : {}", id);
		otherServiceResponseService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
