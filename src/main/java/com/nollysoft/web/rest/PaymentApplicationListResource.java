package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentApplicationList;
import com.nollysoft.service.PaymentApplicationListService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing PaymentApplicationList.
 */
@RestController
@RequestMapping("/api")
public class PaymentApplicationListResource {

	private final Logger log = LoggerFactory.getLogger(PaymentApplicationListResource.class);

	private static final String ENTITY_NAME = "paymentApplicationList";

	private final PaymentApplicationListService paymentApplicationListService;

	public PaymentApplicationListResource(PaymentApplicationListService paymentApplicationListService) {
		this.paymentApplicationListService = paymentApplicationListService;
	}

	/**
	 * POST /payment-application-lists/service-provider : Create a new
	 * paymentApplicationList for service provider.
	 *
	 * @param paymentApplicationList
	 *            the paymentApplicationList to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentApplicationList, or with status 400 (Bad Request) if
	 *         the paymentApplicationList has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-application-lists/service-provider")
	@Timed
	public ResponseEntity<PaymentApplicationList> createPaymentApplicationListForServiceProvider(
			@Valid @RequestBody PaymentApplicationList paymentApplicationList) throws URISyntaxException {
		log.debug("REST request to save PaymentApplicationList : {}", paymentApplicationList);
		if (paymentApplicationList.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentApplicationList cannot already have an ID")).body(null);
		}
		PaymentApplicationList result = paymentApplicationListService.save(paymentApplicationList);
		return ResponseEntity.created(new URI("/api/payment-application-lists/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-application-lists/service-provider : Updates an existing
	 * paymentApplicationList for service provider.
	 *
	 * @param paymentApplicationList
	 *            the paymentApplicationList to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentApplicationList, or with status 400 (Bad Request) if the
	 *         paymentApplicationList is not valid, or with status 500 (Internal
	 *         Server Error) if the paymentApplicationList couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-application-lists/service-provider")
	@Timed
	public ResponseEntity<PaymentApplicationList> updatePaymentApplicationListForServiceProvider(
			@Valid @RequestBody PaymentApplicationList paymentApplicationList) throws URISyntaxException {
		log.debug("REST request to update PaymentApplicationList : {}", paymentApplicationList);
		if (paymentApplicationList.getId() == null) {
			return createPaymentApplicationListForServiceProvider(paymentApplicationList);
		}
		PaymentApplicationList result = paymentApplicationListService.save(paymentApplicationList);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentApplicationList.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-application-lists/service-provider : get all the
	 * paymentApplicationLists for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentApplicationLists in body
	 */
	@GetMapping("/payment-application-lists/service-provider")
	@Timed
	public ResponseEntity<List<PaymentApplicationList>> getAllPaymentApplicationListsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentApplicationLists");
		Page<PaymentApplicationList> page = paymentApplicationListService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/payment-application-lists/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-application-lists/service-provider/:id : get the "id"
	 * paymentApplicationList for service provider.
	 *
	 * @param id
	 *            the id of the paymentApplicationList to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentApplicationList, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-application-lists/service-provider/{id}")
	@Timed
	public ResponseEntity<PaymentApplicationList> getPaymentApplicationListForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get PaymentApplicationList : {}", id);
		PaymentApplicationList paymentApplicationList = paymentApplicationListService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentApplicationList));
	}

	/**
	 * DELETE /payment-application-lists/service-provider/:id : delete the "id"
	 * paymentApplicationList for service provider.
	 *
	 * @param id
	 *            the id of the paymentApplicationList to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-application-lists/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentApplicationListForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete PaymentApplicationList : {}", id);
		paymentApplicationListService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /payment-application-lists : Create a new paymentApplicationList for
	 * merchant.
	 *
	 * @param paymentApplicationList
	 *            the paymentApplicationList to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentApplicationList, or with status 400 (Bad Request) if
	 *         the paymentApplicationList has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-application-lists")
	@Timed
	public ResponseEntity<PaymentApplicationList> createPaymentApplicationListForMerchant(
			@Valid @RequestBody PaymentApplicationList paymentApplicationList) throws URISyntaxException {
		log.debug("REST request to save PaymentApplicationList : {}", paymentApplicationList);
		if (paymentApplicationList.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentApplicationList cannot already have an ID")).body(null);
		}
		PaymentApplicationList result = paymentApplicationListService.save(paymentApplicationList);
		return ResponseEntity.created(new URI("/api/payment-application-lists/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-application-lists : Updates an existing
	 * paymentApplicationList for merchant.
	 *
	 * @param paymentApplicationList
	 *            the paymentApplicationList to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentApplicationList, or with status 400 (Bad Request) if the
	 *         paymentApplicationList is not valid, or with status 500 (Internal
	 *         Server Error) if the paymentApplicationList couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-application-lists")
	@Timed
	public ResponseEntity<PaymentApplicationList> updatePaymentApplicationListForMerchant(
			@Valid @RequestBody PaymentApplicationList paymentApplicationList) throws URISyntaxException {
		log.debug("REST request to update PaymentApplicationList : {}", paymentApplicationList);
		if (paymentApplicationList.getId() == null) {
			return createPaymentApplicationListForMerchant(paymentApplicationList);
		}
		PaymentApplicationList result = paymentApplicationListService.save(paymentApplicationList);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentApplicationList.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-application-lists : get all the paymentApplicationLists for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentApplicationLists in body
	 */
	@GetMapping("/payment-application-lists")
	@Timed
	public ResponseEntity<List<PaymentApplicationList>> getAllPaymentApplicationListsForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentApplicationLists");
		Page<PaymentApplicationList> page = paymentApplicationListService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-application-lists");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-application-lists/:id : get the "id" paymentApplicationList
	 * for merchant.
	 *
	 * @param id
	 *            the id of the paymentApplicationList to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentApplicationList, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-application-lists/{id}")
	@Timed
	public ResponseEntity<PaymentApplicationList> getPaymentApplicationListForMerchant(@PathVariable Long id) {
		log.debug("REST request to get PaymentApplicationList : {}", id);
		PaymentApplicationList paymentApplicationList = paymentApplicationListService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentApplicationList));
	}

	/**
	 * DELETE /payment-application-lists/:id : delete the "id"
	 * paymentApplicationList for merchant.
	 *
	 * @param id
	 *            the id of the paymentApplicationList to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-application-lists/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentApplicationListForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentApplicationList : {}", id);
		paymentApplicationListService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
