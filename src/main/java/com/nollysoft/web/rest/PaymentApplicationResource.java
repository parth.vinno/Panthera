package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentApplication;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentApplicationService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing PaymentApplication.
 */
@RestController
@RequestMapping("/api")
public class PaymentApplicationResource {

	private final Logger log = LoggerFactory.getLogger(PaymentApplicationResource.class);

	private static final String ENTITY_NAME = "paymentApplication";

	private final PaymentApplicationService paymentApplicationService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public PaymentApplicationResource(PaymentApplicationService paymentApplicationService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.paymentApplicationService = paymentApplicationService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /payment-applications/service-provider : Create a new
	 * paymentApplication for service provider.
	 *
	 * @param paymentApplication
	 *            the paymentApplication to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentApplication, or with status 400 (Bad Request) if the
	 *         paymentApplication has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-applications/service-provider")
	@Timed
	public ResponseEntity<PaymentApplication> createPaymentApplicationForServiceProvider(
			@Valid @RequestBody PaymentApplication paymentApplication)
			throws URISyntaxException {
		log.debug("REST request to save PaymentApplication : {}", paymentApplication);
		if (paymentApplication.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentApplication cannot already have an ID")).body(null);
		}
		PaymentApplication result = paymentApplicationService.save(paymentApplication);
		executiveSummaryStatusService.createOrUpdate("PAYMENT_APPLICATIONS");
		return ResponseEntity.created(new URI("/api/payment-applications/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-applications/service-provider : Updates an existing
	 * paymentApplication for service provider.
	 *
	 * @param paymentApplication
	 *            the paymentApplication to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentApplication, or with status 400 (Bad Request) if the
	 *         paymentApplication is not valid, or with status 500 (Internal
	 *         Server Error) if the paymentApplication couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-applications/service-provider")
	@Timed
	public ResponseEntity<PaymentApplication> updatePaymentApplicationForServiceProvider(
			@Valid @RequestBody PaymentApplication paymentApplication)
			throws URISyntaxException {
		log.debug("REST request to update PaymentApplication : {}", paymentApplication);
		if (paymentApplication.getId() == null) {
			return createPaymentApplicationForServiceProvider(paymentApplication);
		}
		PaymentApplication result = paymentApplicationService.save(paymentApplication);
		executiveSummaryStatusService.createOrUpdate("PAYMENT_APPLICATIONS");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentApplication.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-applications/service-provider : get all the
	 * paymentApplications for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentApplications in body
	 */
	@GetMapping("/payment-applications/service-provider")
	@Timed
	public ResponseEntity<List<PaymentApplication>> getAllPaymentApplicationsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentApplications");
		Page<PaymentApplication> page = paymentApplicationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/payment-applications/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-applications/service-provider/:id : get the "id"
	 * paymentApplication for service provider.
	 *
	 * @param id
	 *            the id of the paymentApplication to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentApplication, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-applications/service-provider/{id}")
	@Timed
	public ResponseEntity<PaymentApplication> getPaymentApplicationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get PaymentApplication : {}", id);
		PaymentApplication paymentApplication = paymentApplicationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentApplication));
	}

	/**
	 * DELETE /payment-applications/service-provider/:id : delete the "id"
	 * paymentApplication for service provider.
	 *
	 * @param id
	 *            the id of the paymentApplication to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-applications/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentApplicationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete PaymentApplication : {}", id);
		paymentApplicationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /payment-applications : Create a new paymentApplication for
	 * merchant.
	 *
	 * @param paymentApplication
	 *            the paymentApplication to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentApplication, or with status 400 (Bad Request) if the
	 *         paymentApplication has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-applications")
	@Timed
	public ResponseEntity<PaymentApplication> createPaymentApplicationForMerchant(
			@Valid @RequestBody PaymentApplication paymentApplication)
			throws URISyntaxException {
		log.debug("REST request to save PaymentApplication : {}", paymentApplication);
		if (paymentApplication.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentApplication cannot already have an ID")).body(null);
		}
		PaymentApplication result = paymentApplicationService.save(paymentApplication);
		executiveSummaryStatusService.createOrUpdate("PAYMENT_APPLICATIONS");
		return ResponseEntity.created(new URI("/api/payment-applications/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-applications : Updates an existing paymentApplication for
	 * merchant.
	 *
	 * @param paymentApplication
	 *            the paymentApplication to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentApplication, or with status 400 (Bad Request) if the
	 *         paymentApplication is not valid, or with status 500 (Internal
	 *         Server Error) if the paymentApplication couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-applications")
	@Timed
	public ResponseEntity<PaymentApplication> updatePaymentApplicationForMerchant(
			@Valid @RequestBody PaymentApplication paymentApplication)
			throws URISyntaxException {
		log.debug("REST request to update PaymentApplication : {}", paymentApplication);
		if (paymentApplication.getId() == null) {
			return createPaymentApplicationForMerchant(paymentApplication);
		}
		PaymentApplication result = paymentApplicationService.save(paymentApplication);
		executiveSummaryStatusService.createOrUpdate("PAYMENT_APPLICATIONS");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentApplication.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-applications : get all the paymentApplications for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentApplications in body
	 */
	@GetMapping("/payment-applications")
	@Timed
	public ResponseEntity<List<PaymentApplication>> getAllPaymentApplicationsForMerchant(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentApplications");
		Page<PaymentApplication> page = paymentApplicationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-applications");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-applications/:id : get the "id" paymentApplication for
	 * merchant.
	 *
	 * @param id
	 *            the id of the paymentApplication to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentApplication, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-applications/{id}")
	@Timed
	public ResponseEntity<PaymentApplication> getPaymentApplicationForMerchant(@PathVariable Long id) {
		log.debug("REST request to get PaymentApplication : {}", id);
		PaymentApplication paymentApplication = paymentApplicationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentApplication));
	}

	/**
	 * DELETE /payment-applications/:id : delete the "id" paymentApplication for
	 * merchant.
	 *
	 * @param id
	 *            the id of the paymentApplication to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-applications/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentApplicationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentApplication : {}", id);
		paymentApplicationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
