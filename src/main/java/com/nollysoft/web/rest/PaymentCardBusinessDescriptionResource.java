package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentCardBusinessDescription;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentCardBusinessDescriptionService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing PaymentCardBusinessDescription.
 */
@RestController
@RequestMapping("/api")
public class PaymentCardBusinessDescriptionResource {

	private final Logger log = LoggerFactory.getLogger(PaymentCardBusinessDescriptionResource.class);

	private static final String ENTITY_NAME = "paymentCardBusinessDescription";

	private final PaymentCardBusinessDescriptionService paymentCardBusinessDescriptionService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public PaymentCardBusinessDescriptionResource(
			PaymentCardBusinessDescriptionService paymentCardBusinessDescriptionService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.paymentCardBusinessDescriptionService = paymentCardBusinessDescriptionService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /payment-card-business-descriptions/service-provider : Create a new
	 * paymentCardBusinessDescription for service provider.
	 *
	 * @param paymentCardBusinessDescription
	 *            the paymentCardBusinessDescription to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentCardBusinessDescription, or with status 400 (Bad
	 *         Request) if the paymentCardBusinessDescription has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-card-business-descriptions/service-provider")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> createPaymentCardBusinessDescriptionForServiceProvider(
			@Valid @RequestBody PaymentCardBusinessDescription paymentCardBusinessDescription) throws URISyntaxException {
		log.debug("REST request to save PaymentCardBusinessDescription : {}", paymentCardBusinessDescription);
		if (paymentCardBusinessDescription.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentCardBusinessDescription cannot already have an ID")).body(null);
		}
		PaymentCardBusinessDescription result = paymentCardBusinessDescriptionService
				.save(paymentCardBusinessDescription);
		executiveSummaryStatusService.createOrUpdate("CARD_DESCRIPTION");
		return ResponseEntity
				.created(new URI("/api/payment-card-business-descriptions/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /payment-card-business-descriptions : Create a new
	 * paymentCardBusinessDescription for merchant.
	 *
	 * @param paymentCardBusinessDescription
	 *            the paymentCardBusinessDescription to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentCardBusinessDescription, or with status 400 (Bad
	 *         Request) if the paymentCardBusinessDescription has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-card-business-descriptions")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> createPaymentCardBusinessDescriptionForMerchant(
			@Valid @RequestBody PaymentCardBusinessDescription paymentCardBusinessDescription)
			throws URISyntaxException {
		log.debug("REST request to save PaymentCardBusinessDescription : {}", paymentCardBusinessDescription);
		if (paymentCardBusinessDescription.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentCardBusinessDescription cannot already have an ID")).body(null);
		}
		PaymentCardBusinessDescription result = paymentCardBusinessDescriptionService
				.save(paymentCardBusinessDescription);
		executiveSummaryStatusService.createOrUpdate("CARD_DESCRIPTION");
		return ResponseEntity.created(new URI("/api/payment-card-business-descriptions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-card-business-descriptions/service-provider : Updates an
	 * existing paymentCardBusinessDescription for service provider.
	 *
	 * @param paymentCardBusinessDescription
	 *            the paymentCardBusinessDescription to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentCardBusinessDescription, or with status 400 (Bad Request)
	 *         if the paymentCardBusinessDescription is not valid, or with
	 *         status 500 (Internal Server Error) if the
	 *         paymentCardBusinessDescription couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-card-business-descriptions/service-provider")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> updatePaymentCardBusinessDescriptionForServiceProvider(
			@Valid @RequestBody PaymentCardBusinessDescription paymentCardBusinessDescription)
			throws URISyntaxException {
		log.debug("REST request to update PaymentCardBusinessDescription : {}", paymentCardBusinessDescription);
		if (paymentCardBusinessDescription.getId() == null) {
			return createPaymentCardBusinessDescriptionForServiceProvider(paymentCardBusinessDescription);
		}
		PaymentCardBusinessDescription result = paymentCardBusinessDescriptionService
				.save(paymentCardBusinessDescription);
		executiveSummaryStatusService.createOrUpdate("CARD_DESCRIPTION");
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentCardBusinessDescription.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /payment-card-business-descriptions : Updates an existing
	 * paymentCardBusinessDescription for merchant.
	 *
	 * @param paymentCardBusinessDescription
	 *            the paymentCardBusinessDescription to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentCardBusinessDescription, or with status 400 (Bad Request)
	 *         if the paymentCardBusinessDescription is not valid, or with
	 *         status 500 (Internal Server Error) if the
	 *         paymentCardBusinessDescription couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-card-business-descriptions")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> updatePaymentCardBusinessDescriptionForMerchant(
			@Valid @RequestBody PaymentCardBusinessDescription paymentCardBusinessDescription)
			throws URISyntaxException {
		log.debug("REST request to update PaymentCardBusinessDescription : {}", paymentCardBusinessDescription);
		if (paymentCardBusinessDescription.getId() == null) {
			return createPaymentCardBusinessDescriptionForMerchant(paymentCardBusinessDescription);
		}
		PaymentCardBusinessDescription result = paymentCardBusinessDescriptionService
				.save(paymentCardBusinessDescription);
		executiveSummaryStatusService.createOrUpdate("CARD_DESCRIPTION");
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentCardBusinessDescription.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-card-business-descriptions/service-provider : get all the
	 * paymentCardBusinessDescriptions for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentCardBusinessDescriptions in body
	 */
	@GetMapping("/payment-card-business-descriptions/service-provider")
	@Timed
	public ResponseEntity<List<PaymentCardBusinessDescription>> getAllPaymentCardBusinessDescriptionsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentCardBusinessDescriptions");
		Page<PaymentCardBusinessDescription> page = paymentCardBusinessDescriptionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/payment-card-business-descriptions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-card-business-descriptions : get all the
	 * paymentCardBusinessDescriptions for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentCardBusinessDescriptions in body
	 */
	@GetMapping("/payment-card-business-descriptions")
	@Timed
	public ResponseEntity<List<PaymentCardBusinessDescription>> getAllPaymentCardBusinessDescriptionsForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PaymentCardBusinessDescriptions");
		Page<PaymentCardBusinessDescription> page = paymentCardBusinessDescriptionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/payment-card-business-descriptions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-card-business-descriptions/service-provider/:id : get the
	 * "id" paymentCardBusinessDescription for service provider.
	 *
	 * @param id
	 *            the id of the paymentCardBusinessDescription to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentCardBusinessDescription, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-card-business-descriptions/service-provider/{id}")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> getPaymentCardBusinessDescriptionForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get PaymentCardBusinessDescription : {}", id);
		PaymentCardBusinessDescription paymentCardBusinessDescription = paymentCardBusinessDescriptionService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentCardBusinessDescription));
	}

	/**
	 * GET /payment-card-business-descriptions/:id : get the "id"
	 * paymentCardBusinessDescription for merchant.
	 *
	 * @param id
	 *            the id of the paymentCardBusinessDescription to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentCardBusinessDescription, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-card-business-descriptions/{id}")
	@Timed
	public ResponseEntity<PaymentCardBusinessDescription> getPaymentCardBusinessDescriptionForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get PaymentCardBusinessDescription : {}", id);
		PaymentCardBusinessDescription paymentCardBusinessDescription = paymentCardBusinessDescriptionService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentCardBusinessDescription));
	}

	/**
	 * DELETE /payment-card-business-descriptions/service-provider/:id : delete
	 * the "id" paymentCardBusinessDescription for service provider.
	 *
	 * @param id
	 *            the id of the paymentCardBusinessDescription to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-card-business-descriptions/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentCardBusinessDescriptionForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete PaymentCardBusinessDescription : {}", id);
		paymentCardBusinessDescriptionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /payment-card-business-descriptions/:id : delete the "id"
	 * paymentCardBusinessDescription for merchant.
	 *
	 * @param id
	 *            the id of the paymentCardBusinessDescription to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-card-business-descriptions/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentCardBusinessDescriptionForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentCardBusinessDescription : {}", id);
		paymentCardBusinessDescriptionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
