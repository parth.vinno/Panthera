package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentChannelBusinessServes;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentChannelBusinessServesService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PaymentChannelBusinessServes.
 */
@RestController
@RequestMapping("/api")
public class PaymentChannelBusinessServesResource {

	private final Logger log = LoggerFactory.getLogger(PaymentChannelBusinessServesResource.class);

	private static final String ENTITY_NAME = "paymentChannelBusinessServes";

	private final PaymentChannelBusinessServesService paymentChannelBusinessServesService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public PaymentChannelBusinessServesResource(
			PaymentChannelBusinessServesService paymentChannelBusinessServesService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.paymentChannelBusinessServesService = paymentChannelBusinessServesService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /payment-channel-business-serves : Create a new
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @param paymentChannelBusinessServes
	 *            the paymentChannelBusinessServes to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentChannelBusinessServes, or with status 400 (Bad
	 *         Request) if the paymentChannelBusinessServes has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-channel-business-serves")
	@Timed
	public ResponseEntity<PaymentChannelBusinessServes> createPaymentChannelBusinessServesForMerchant(
			@Valid @RequestBody PaymentChannelBusinessServes paymentChannelBusinessServes) throws URISyntaxException {
		log.debug("REST request to save PaymentChannelBusinessServes : {}", paymentChannelBusinessServes);
		if (paymentChannelBusinessServes.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentChannelBusinessServes cannot already have an ID")).body(null);
		}
		PaymentChannelBusinessServes result = paymentChannelBusinessServesService.save(paymentChannelBusinessServes);
		return ResponseEntity.created(new URI("/api/payment-channel-business-serves/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /payment-channel-business-serves/list : Create a list of
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @param paymentchannelsSaqCovers
	 *            the paymentChannelBusinessServes to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of paymentChannelBusinessServes
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-channel-business-serves/list")
	@Timed
	public ResponseEntity<List<PaymentChannelBusinessServes>> createPaymentchannelsSaqCoversForMerchant(
			@Valid @RequestBody List<PaymentChannelBusinessServes> paymentChannelBusinessServes)
			throws URISyntaxException {
		log.debug("REST request to save PaymentchannelSaqCovers : {}", paymentChannelBusinessServes);
		List<PaymentChannelBusinessServes> result = paymentChannelBusinessServesService
				.create(paymentChannelBusinessServes);
		executiveSummaryStatusService.createOrUpdate("MERCHANT_BUSINESS");
		return ResponseEntity.created(new URI("/api/payment-channel-business-serves/list/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /payment-channel-business-serves : Updates an existing
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @param paymentChannelBusinessServes
	 *            the paymentChannelBusinessServes to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentChannelBusinessServes, or with status 400 (Bad Request) if
	 *         the paymentChannelBusinessServes is not valid, or with status 500
	 *         (Internal Server Error) if the paymentChannelBusinessServes
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-channel-business-serves")
	@Timed
	public ResponseEntity<PaymentChannelBusinessServes> updatePaymentChannelBusinessServesForMerchant(
			@Valid @RequestBody PaymentChannelBusinessServes paymentChannelBusinessServes) throws URISyntaxException {
		log.debug("REST request to update PaymentChannelBusinessServes : {}", paymentChannelBusinessServes);
		if (paymentChannelBusinessServes.getId() == null) {
			return createPaymentChannelBusinessServesForMerchant(paymentChannelBusinessServes);
		}
		PaymentChannelBusinessServes result = paymentChannelBusinessServesService.save(paymentChannelBusinessServes);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentChannelBusinessServes.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-channel-business-serves : get all the
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentChannelBusinessServes in body
	 */
	@GetMapping("/payment-channel-business-serves")
	@Timed
	public List<PaymentChannelBusinessServes> getAllPaymentChannelBusinessServesForMerchant() {
		log.debug("REST request to get all PaymentChannelBusinessServes");
		return paymentChannelBusinessServesService.findAll();
	}

	/**
	 * GET /payment-channel-business-serves/:id : get the "id"
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @param id
	 *            the id of the paymentChannelBusinessServes to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentChannelBusinessServes, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-channel-business-serves/{id}")
	@Timed
	public ResponseEntity<PaymentChannelBusinessServes> getPaymentChannelBusinessServesForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get PaymentChannelBusinessServes : {}", id);
		PaymentChannelBusinessServes paymentChannelBusinessServes = paymentChannelBusinessServesService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentChannelBusinessServes));
	}

	/**
	 * DELETE /payment-channel-business-serves/:id : delete the "id"
	 * paymentChannelBusinessServes for merchant.
	 *
	 * @param id
	 *            the id of the paymentChannelBusinessServes to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-channel-business-serves/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentChannelBusinessServesForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentChannelBusinessServes : {}", id);
		paymentChannelBusinessServesService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
