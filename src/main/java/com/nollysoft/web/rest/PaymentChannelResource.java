package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.PaymentChannel;
import com.nollysoft.service.PaymentChannelService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing PaymentChannel.
 */
@RestController
@RequestMapping("/api")
public class PaymentChannelResource {

	private final Logger log = LoggerFactory.getLogger(PaymentChannelResource.class);

	private static final String ENTITY_NAME = "paymentChannel";

	private final PaymentChannelService paymentChannelService;

	public PaymentChannelResource(PaymentChannelService paymentChannelService) {
		this.paymentChannelService = paymentChannelService;
	}

	/**
	 * POST /payment-channels : Create a new paymentChannel for merchant.
	 *
	 * @param paymentChannel
	 *            the paymentChannel to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentChannel, or with status 400 (Bad Request) if the
	 *         paymentChannel has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-channels")
	@Timed
	public ResponseEntity<PaymentChannel> createPaymentChannelForMerchant(
			@Valid @RequestBody PaymentChannel paymentChannel) throws URISyntaxException {
		log.debug("REST request to save PaymentChannel : {}", paymentChannel);
		if (paymentChannel.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentChannel cannot already have an ID")).body(null);
		}
		PaymentChannel result = paymentChannelService.save(paymentChannel);
		return ResponseEntity.created(new URI("/api/payment-channels/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /payment-channels : Updates an existing paymentChannel for merchant.
	 *
	 * @param paymentChannel
	 *            the paymentChannel to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentChannel, or with status 400 (Bad Request) if the
	 *         paymentChannel is not valid, or with status 500 (Internal Server
	 *         Error) if the paymentChannel couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-channels")
	@Timed
	public ResponseEntity<PaymentChannel> updatePaymentChannelForMerchant(
			@Valid @RequestBody PaymentChannel paymentChannel) throws URISyntaxException {
		log.debug("REST request to update PaymentChannel : {}", paymentChannel);
		if (paymentChannel.getId() == null) {
			return createPaymentChannelForMerchant(paymentChannel);
		}
		PaymentChannel result = paymentChannelService.save(paymentChannel);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentChannel.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-channels : get all the paymentChannels for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentChannels in body
	 */
	@GetMapping("/payment-channels")
	@Timed
	public List<PaymentChannel> getAllPaymentChannelsForMerchant() {
		log.debug("REST request to get all PaymentChannels");
		return paymentChannelService.findAll();
	}

	/**
	 * GET /merchant-business-types : get all the
	 * merchantBusinessTypes for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         merchantBusinessTypes in body
	 */
	@GetMapping("/payment-channels-pageable")
	@Timed
	public ResponseEntity<List<PaymentChannel>> getAllPaymentChannels(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Payment Channel");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			Page<PaymentChannel> page = paymentChannelService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-channels");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}


	/**
	 * GET /payment-channels/:id : get the "id" paymentChannel for merchant.
	 *
	 * @param id
	 *            the id of the paymentChannel to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentChannel, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-channels/{id}")
	@Timed
	public ResponseEntity<PaymentChannel> getPaymentChannelForMerchant(@PathVariable Long id) {
		log.debug("REST request to get PaymentChannel : {}", id);
		PaymentChannel paymentChannel = paymentChannelService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentChannel));
	}

	/**
	 * DELETE /payment-channels/:id : delete the "id" paymentChannel for
	 * merchant.
	 *
	 * @param id
	 *            the id of the paymentChannel to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-channels/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentChannelForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentChannel : {}", id);
		paymentChannelService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
