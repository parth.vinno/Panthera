package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentChannelSaqCovers;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentChannelSaqCoversService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PaymentchannelSaqCovers.
 */
@RestController
@RequestMapping("/api")
public class PaymentChannelSaqCoversResource {

	private final Logger log = LoggerFactory.getLogger(PaymentChannelSaqCoversResource.class);

	private static final String ENTITY_NAME = "paymentChannelSaqCovers";

	private final PaymentChannelSaqCoversService paymentChannelSaqCoversService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public PaymentChannelSaqCoversResource(PaymentChannelSaqCoversService paymentChannelSaqCoversService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.paymentChannelSaqCoversService = paymentChannelSaqCoversService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /payment-channel-saq-covers : Create a new paymentChannelSaqCovers
	 * for merchant.
	 *
	 * @param paymentChannelSaqCovers
	 *            the paymentChannelSaqCovers to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentChannelSaqCovers, or with status 400 (Bad Request) if
	 *         the paymentChannelSaqCovers has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-channel-saq-covers")
	@Timed
	public ResponseEntity<PaymentChannelSaqCovers> createPaymentchannelSaqCoversForMerchant(
			@Valid @RequestBody PaymentChannelSaqCovers paymentChannelSaqCovers) throws URISyntaxException {
		log.debug("REST request to save PaymentchannelSaqCovers : {}", paymentChannelSaqCovers);
		if (paymentChannelSaqCovers.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new paymentChannelSaqCovers cannot already have an ID")).body(null);
		}
		PaymentChannelSaqCovers result = paymentChannelSaqCoversService.save(paymentChannelSaqCovers);
		return ResponseEntity.created(new URI("/api/payment-channel-saq-covers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /payment-channel-saq-covers/list : Create a list of
	 * paymentChannelSaqCovers for merchant.
	 *
	 * @param paymentchannelsSaqCovers
	 *            the paymentChannelSaqCovers to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of paymentChannelSaqCovers
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-channel-saq-covers/list")
	@Timed
	public ResponseEntity<List<PaymentChannelSaqCovers>> createPaymentchannelsSaqCoversForMerchant(
			@Valid @RequestBody List<PaymentChannelSaqCovers> paymentchannelsSaqCovers) throws URISyntaxException {
		log.debug("REST request to save PaymentchannelSaqCovers : {}", paymentchannelsSaqCovers);
		List<PaymentChannelSaqCovers> result = paymentChannelSaqCoversService.create(paymentchannelsSaqCovers);
		executiveSummaryStatusService.createOrUpdate("MERCHANT_BUSINESS");
		return ResponseEntity.created(new URI("/api/payment-channel-saq-covers/list/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /payment-channel-saq-covers : Updates an existing
	 * paymentChannelSaqCovers for merchant.
	 *
	 * @param paymentChannelSaqCovers
	 *            the paymentChannelSaqCovers to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentChannelSaqCovers, or with status 400 (Bad Request) if the
	 *         paymentChannelSaqCovers is not valid, or with status 500
	 *         (Internal Server Error) if the paymentChannelSaqCovers couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-channel-saq-covers")
	@Timed
	public ResponseEntity<PaymentChannelSaqCovers> updatePaymentchannelSaqCoversForMerchant(
			@Valid @RequestBody PaymentChannelSaqCovers paymentChannelSaqCovers) throws URISyntaxException {
		log.debug("REST request to update PaymentchannelSaqCovers : {}", paymentChannelSaqCovers);
		if (paymentChannelSaqCovers.getId() == null) {
			return createPaymentchannelSaqCoversForMerchant(paymentChannelSaqCovers);
		}
		PaymentChannelSaqCovers result = paymentChannelSaqCoversService.save(paymentChannelSaqCovers);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentChannelSaqCovers.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-channel-saq-covers : get all the paymentChannelSaqCovers for
	 * merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentChannelSaqCovers in body
	 */
	@GetMapping("/payment-channel-saq-covers")
	@Timed
	public List<PaymentChannelSaqCovers> getAllPaymentchannelSaqCoversForMerchant() {
		log.debug("REST request to get all PaymentchannelSaqCovers");
		return paymentChannelSaqCoversService.findAll();
	}

	/**
	 * GET /payment-channel-saq-covers/:id : get the "id"
	 * paymentChannelSaqCovers for merchant.
	 *
	 * @param id
	 *            the id of the paymentChannelSaqCovers to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentChannelSaqCovers, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-channel-saq-covers/{id}")
	@Timed
	public ResponseEntity<PaymentChannelSaqCovers> getPaymentchannelSaqCoversForMerchant(@PathVariable Long id) {
		log.debug("REST request to get PaymentchannelSaqCovers : {}", id);
		PaymentChannelSaqCovers paymentChannelSaqCovers = paymentChannelSaqCoversService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentChannelSaqCovers));
	}

	/**
	 * DELETE /payment-channel-saq-covers/:id : delete the "id"
	 * paymentChannelSaqCovers for merchant.
	 *
	 * @param id
	 *            the id of the paymentChannelSaqCovers to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-channel-saq-covers/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentchannelSaqCoversForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PaymentchannelSaqCovers : {}", id);
		paymentChannelSaqCoversService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
