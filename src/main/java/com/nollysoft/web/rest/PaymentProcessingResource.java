package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.PaymentProcessing;
import com.nollysoft.service.PaymentProcessingService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PaymentProcessing.
 */
@RestController
@RequestMapping("/api")
public class PaymentProcessingResource {

    private final Logger log = LoggerFactory.getLogger(PaymentProcessingResource.class);

    private static final String ENTITY_NAME = "paymentProcessing";

    private final PaymentProcessingService paymentProcessingService;

    public PaymentProcessingResource(PaymentProcessingService paymentProcessingService) {
        this.paymentProcessingService = paymentProcessingService;
    }

    /**
     * POST  /payment-processings : Create a new paymentProcessing.
     *
     * @param paymentProcessing the paymentProcessing to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentProcessing, or with status 400 (Bad Request) if the paymentProcessing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-processings")
    @Timed
    public ResponseEntity<PaymentProcessing> createPaymentProcessing(@Valid @RequestBody PaymentProcessing paymentProcessing) throws URISyntaxException {
        log.debug("REST request to save PaymentProcessing : {}", paymentProcessing);
        if (paymentProcessing.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new paymentProcessing cannot already have an ID")).body(null);
        }
        PaymentProcessing result = paymentProcessingService.save(paymentProcessing);
        return ResponseEntity.created(new URI("/api/payment-processings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /payment-processings : Updates an existing paymentProcessing.
     *
     * @param paymentProcessing the paymentProcessing to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentProcessing,
     * or with status 400 (Bad Request) if the paymentProcessing is not valid,
     * or with status 500 (Internal Server Error) if the paymentProcessing couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-processings")
    @Timed
    public ResponseEntity<PaymentProcessing> updatePaymentProcessing(@Valid @RequestBody PaymentProcessing paymentProcessing) throws URISyntaxException {
        log.debug("REST request to update PaymentProcessing : {}", paymentProcessing);
        if (paymentProcessing.getId() == null) {
            return createPaymentProcessing(paymentProcessing);
        }
        PaymentProcessing result = paymentProcessingService.save(paymentProcessing);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentProcessing.getId().toString()))
            .body(result);
    }

    /**
     * GET  /payment-processings : get all the paymentProcessings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paymentProcessings in body
     */
    @GetMapping("/payment-processings")
    @Timed
    public ResponseEntity<List<PaymentProcessing>> getAllPaymentProcessings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PaymentProcessings");
        Page<PaymentProcessing> page = paymentProcessingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-processings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payment-processings/:id : get the "id" paymentProcessing.
     *
     * @param id the id of the paymentProcessing to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentProcessing, or with status 404 (Not Found)
     */
    @GetMapping("/payment-processings/{id}")
    @Timed
    public ResponseEntity<PaymentProcessing> getPaymentProcessing(@PathVariable Long id) {
        log.debug("REST request to get PaymentProcessing : {}", id);
        PaymentProcessing paymentProcessing = paymentProcessingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentProcessing));
    }

    /**
     * DELETE  /payment-processings/:id : delete the "id" paymentProcessing.
     *
     * @param id the id of the paymentProcessing to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-processings/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentProcessing(@PathVariable Long id) {
        log.debug("REST request to delete PaymentProcessing : {}", id);
        paymentProcessingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
