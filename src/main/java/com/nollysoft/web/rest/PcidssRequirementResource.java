package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.service.PcidssRequirementService;
import com.nollysoft.service.dto.PcidssRequirementAdminDTO;
import com.nollysoft.service.dto.PcidssRequirementPageDTO;
import com.nollysoft.service.dto.PcidssRequirementUserDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing PcidssRequirement.
 */
@RestController
@RequestMapping("/api")
public class PcidssRequirementResource {

	private final Logger log = LoggerFactory.getLogger(PcidssRequirementResource.class);

	private static final String ENTITY_NAME = "pcidssRequirement";

	private final PcidssRequirementService pcidssRequirementService;

	public PcidssRequirementResource(PcidssRequirementService pcidssRequirementService) {
		this.pcidssRequirementService = pcidssRequirementService;
	}

	/**
	 * POST /pcidss-requirements/service-provider : Create a new
	 * pcidssRequirement for service provider.
	 *
	 * @param pcidssRequirement
	 *            the pcidssRequirement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new pcidssRequirement, or with status 400 (Bad Request) if the
	 *         pcidssRequirement has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/pcidss-requirements/service-provider")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> createPcidssRequirementForServiceProvider(
			@RequestBody PcidssRequirement pcidssRequirement) throws URISyntaxException {
		log.debug("REST request to save PcidssRequirement : {}", pcidssRequirement);
		if (pcidssRequirement.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new pcidssRequirement cannot already have an ID")).body(null);
		}
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirement result = pcidssRequirementService.save(pcidssRequirement);
			return ResponseEntity.created(new URI("/api/serviceProvider/pcidssRequirements/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
					.body(new PcidssRequirementAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * POST /pcidss-requirements : Create a new pcidssRequirement for merchant.
	 *
	 * @param pcidssRequirement
	 *            the pcidssRequirement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new pcidssRequirement, or with status 400 (Bad Request) if the
	 *         pcidssRequirement has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/pcidss-requirements")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> createPcidssRequirementForMerchant(
			@RequestBody PcidssRequirement pcidssRequirement) throws URISyntaxException {
		log.debug("REST request to save PcidssRequirement : {}", pcidssRequirement);
		if (pcidssRequirement.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new pcidssRequirement cannot already have an ID")).body(null);
		}
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirement result = pcidssRequirementService.save(pcidssRequirement);
			return ResponseEntity.created(new URI("/api/merchant/pcidssRequirements/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
					.body(new PcidssRequirementAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}
	
	/**
	 * POST /pcidss-requirements/list/service-provider/{categoryId} : Create a list of pcidssRequirement for service provider.
	 *
	 * @param List of pcidssRequirement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of pcidssRequirement
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/pcidss-requirements/list/service-provider/{categoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> createPcidssRequirementsForServiceProvider(
			@RequestBody List<PcidssRequirement> pcidssRequirements, @PathVariable Long categoryId) throws URISyntaxException {
		log.debug("REST request to save list of PcidssRequirement : {}", pcidssRequirements);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			List<PcidssRequirementAdminDTO> result = pcidssRequirementService.saveRequirements(pcidssRequirements, categoryId);
			return ResponseEntity.created(new URI("/api/merchant/pcidssRequirements/"))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString()))
					.body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}
	
	/**
	 * POST /pcidss-requirements/list/{categoryId} : Create a list of pcidssRequirement for merchant.
	 *
	 * @param List of pcidssRequirement to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of pcidssRequirement
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/pcidss-requirements/list/{categoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> createPcidssRequirementsForMerchant(
			@RequestBody List<PcidssRequirement> pcidssRequirements, @PathVariable Long categoryId) throws URISyntaxException {
		log.debug("REST request to save list of PcidssRequirement : {}", pcidssRequirements);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementAdminDTO> result = pcidssRequirementService.saveRequirements(pcidssRequirements, categoryId);
			return ResponseEntity.created(new URI("/api/merchant/pcidssRequirements/"))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString()))
					.body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * PUT /pcidss-requirements/service-provider : Updates an existing
	 * pcidssRequirement for service provider.
	 *
	 * @param pcidssRequirement
	 *            the pcidssRequirement to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         pcidssRequirement, or with status 400 (Bad Request) if the
	 *         pcidssRequirement is not valid, or with status 500 (Internal
	 *         Server Error) if the pcidssRequirement couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/pcidss-requirements/service-provider")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> updatePcidssRequirementForServiceProvider(
			@RequestBody PcidssRequirement pcidssRequirement) throws URISyntaxException {
		log.debug("REST request to update PcidssRequirement : {}", pcidssRequirement);
		if (pcidssRequirement.getId() == null) {
			return createPcidssRequirementForServiceProvider(pcidssRequirement);
		}
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirement result = pcidssRequirementService.save(pcidssRequirement);
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pcidssRequirement.getId().toString()))
					.body(new PcidssRequirementAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * PUT /pcidss-requirements : Updates an existing pcidssRequirement for
	 * merchant.
	 *
	 * @param pcidssRequirement
	 *            the pcidssRequirement to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         pcidssRequirement, or with status 400 (Bad Request) if the
	 *         pcidssRequirement is not valid, or with status 500 (Internal
	 *         Server Error) if the pcidssRequirement couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/pcidss-requirements")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> updatePcidssRequirementForMerchant(
			@RequestBody PcidssRequirement pcidssRequirement) throws URISyntaxException {
		log.debug("REST request to update PcidssRequirement : {}", pcidssRequirement);
		if (pcidssRequirement.getId() == null) {
			return createPcidssRequirementForMerchant(pcidssRequirement);
		}
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirement result = pcidssRequirementService.save(pcidssRequirement);
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pcidssRequirement.getId().toString()))
					.body(new PcidssRequirementAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/service-provider : get all the
	 * pcidssRequirements for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/service-provider")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> getAllPcidssRequirementsForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirementPageDTO page = pcidssRequirementService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/serviceProvider/pcidssRequirements");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements : get all the pcidssRequirements for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> getAllPcidssRequirementsForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirementPageDTO page = pcidssRequirementService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/merchant/pcidssRequirements");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-status/service-provider : get all the
	 * pcidssRequirements for service provider.
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status/service-provider")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithStatusForServiceProvider() {
		log.debug("REST request to get PcidssRequirements with status");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService.findAllWithStatusAndAvailability();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-status : get all the pcidssRequirements for
	 * merchant.
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithStatusForMerchant() {
		log.debug("REST request to get PcidssRequirements with status");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService.findAllWithStatusAndAvailability();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET
	 * /pcidss-requirements/with-status/fixed/service-provider/category-id/:categoryId
	 * : get all the pcidssRequirements with corrected responses for a category
	 * for service provider.
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status/fixed/service-provider/category-id/{categoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithCorrectedSaqQuestionsForServiceProvider(
			@PathVariable Long categoryId) {
		log.debug("REST request to get PcidssRequirements with status");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService
					.findAllWithCorrectedSaqResponses(categoryId);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-status/fixed/category-id/:categoryId : get
	 * all the pcidssRequirements with corrected responses for a category for
	 * merchant.
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status/fixed/category-id/{categoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithCorrectedSaqQuestionsForMerchant(
			@PathVariable Long categoryId) {
		log.debug("REST request to get PcidssRequirements with status");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService
					.findAllWithCorrectedSaqResponses(categoryId);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/service-provider/:id : get the "id"
	 * pcidssRequirement for service provider.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/service-provider/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> getPcidssRequirementForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirement pcidssRequirement = pcidssRequirementService.findOne(id);
			return ResponseUtil.wrapOrNotFound(Optional
					.ofNullable(pcidssRequirement != null ? new PcidssRequirementAdminDTO(pcidssRequirement) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/:id : get the "id" pcidssRequirement for
	 * merchant.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> getPcidssRequirementForMerchant(@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirement pcidssRequirement = pcidssRequirementService.findOne(id);
			return ResponseUtil.wrapOrNotFound(Optional
					.ofNullable(pcidssRequirement != null ? new PcidssRequirementAdminDTO(pcidssRequirement) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-saq-question-ids/service-provider/:id : get
	 * the "id" pcidssRequirement for service provider.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/with-saq-question-ids/service-provider/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementUserDTO> getPcidssRequirementWithSaqQuestionIdsForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirementUserDTO pcidssRequirement = pcidssRequirementService.findOneWithSaqQuestionIds(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pcidssRequirement));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-saq-question-ids/:id : get the "id"
	 * pcidssRequirement for merchant.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/with-saq-question-ids/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementUserDTO> getPcidssRequirementWithSaqQuestionIdsForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirementUserDTO pcidssRequirement = pcidssRequirementService.findOneWithSaqQuestionIds(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pcidssRequirement));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-saq-question-ids/fixed/service-provider/:id
	 * : get the "id" pcidssRequirement for service provider.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/with-saq-question-ids/fixed/service-provider/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementUserDTO> getPcidssRequirementWithCorrectedSaqQuestionIdsForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirementUserDTO pcidssRequirement = pcidssRequirementService
					.findOneWithCorrectedSaqQuestionIds(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pcidssRequirement));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/with-saq-question-ids/fixed/:id : get the "id"
	 * pcidssRequirement for merchant.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         pcidssRequirement, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/with-saq-question-ids/fixed/{id}")
	@Timed
	public ResponseEntity<PcidssRequirementUserDTO> getPcidssRequirementWithCorrectedSaqQuestionIdsForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirementUserDTO pcidssRequirement = pcidssRequirementService
					.findOneWithCorrectedSaqQuestionIds(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pcidssRequirement));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * DELETE /pcidss-requirements/service-provider/:id : delete the "id"
	 * pcidssRequirement for service provider.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/pcidss-requirements/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deletePcidssRequirementForServicerProvider(@PathVariable Long id) {
		log.debug("REST request to delete PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			pcidssRequirementService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * DELETE /pcidss-requirements/:id : delete the "id" pcidssRequirement for
	 * merchant.
	 *
	 * @param id
	 *            the id of the pcidssRequirement to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/pcidss-requirements/{id}")
	@Timed
	public ResponseEntity<Void> deletePcidssRequirementForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete PcidssRequirement : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			pcidssRequirementService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /pcidss-requirements/service-provider/category-id/:categoryId : get
	 * all the pcidssRequirements by categoryId for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/service-provider/category-id/{requirementCategoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> getAllPcidssRequirementsByRequirementCategoryForServiceProvider(
			@PathVariable Long requirementCategoryId, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirementPageDTO page = pcidssRequirementService
					.findPcidssRequirementByRequirmentCategory(requirementCategoryId, pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/serviceProvider/pcidssRequirementsByRequirementCategory/" + requirementCategoryId);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}

	}

	/**
	 * GET /pcidss-requirements/service-provider/:categoryId : get all the
	 * pcidssRequirements by categoryId for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/category-id/{requirementCategoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementAdminDTO>> getAllPcidssRequirementsByRequirementCategoryForMerchant(
			@PathVariable Long requirementCategoryId, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			PcidssRequirementPageDTO page = pcidssRequirementService
					.findPcidssRequirementByRequirmentCategory(requirementCategoryId, pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/merchant/pcidssRequirementsByRequirementCategory/" + requirementCategoryId);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET
	 * /pcidss-requirements/with-status/service-provider/category-id/:categoryId
	 * : get all the pcidssRequirements by categoryId for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status/service-provider/category-id/{requirementCategoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithStatusByRequirementCategoryForServiceProvider(
			@PathVariable Long requirementCategoryId, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService
					.findPcidssRequirementWithStatusByRequirmentCategory(requirementCategoryId);
			return ResponseEntity.status(HttpStatus.OK).body(result);

		} finally {
			DbContextHolder.clearDbType();
		}

	}

	/**
	 * GET /pcidss-requirements/with-status/service-provider/:categoryId : get
	 * all the pcidssRequirements by categoryId for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         pcidssRequirements in body
	 */
	@GetMapping("/pcidss-requirements/with-status/category-id/{requirementCategoryId}")
	@Timed
	public ResponseEntity<List<PcidssRequirementUserDTO>> getAllPcidssRequirementsWithStatusByRequirementCategoryForMerchant(
			@PathVariable Long requirementCategoryId) {
		log.debug("REST request to get a page of PcidssRequirements");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<PcidssRequirementUserDTO> result = pcidssRequirementService
					.findPcidssRequirementWithStatusByRequirmentCategory(requirementCategoryId);
			return ResponseEntity.status(HttpStatus.OK).body(result);

		} finally {
			DbContextHolder.clearDbType();
		}
	}
	
	/**
	 * GET /pcidss-requirements/by-requirement-number : get the
	 * "requirementNumber" requirements for merchant.
	 *
	 * @param requirementNumber
	 *            the questionNumber of the requirements to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirements, or with status 404 (Not Found)
	 */
	@GetMapping("/pcidss-requirements/by-requirement-number")
	@Timed
	public ResponseEntity<PcidssRequirementAdminDTO> getRequirementByRequirementNumberForMerchant(
			@RequestParam(value = "requirementNumber", required = true) String requirementNumber) {
		log.debug("REST request to get Pcidss Requirement : {}", requirementNumber);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			PcidssRequirement pcidssRequirement = pcidssRequirementService.findOneByRequirementNumber(requirementNumber);
			return ResponseUtil.wrapOrNotFound(Optional
					.ofNullable(pcidssRequirement != null ? new PcidssRequirementAdminDTO(pcidssRequirement) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}
}

