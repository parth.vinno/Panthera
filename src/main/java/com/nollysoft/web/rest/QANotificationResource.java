package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.User;
import com.nollysoft.repository.AssessmentInfoRepository;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.security.SecurityUtils;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.service.dto.QANotificationDTO;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing QANotification.
 */
@RestController
@RequestMapping("/api")
public class QANotificationResource {

	private final Logger log = LoggerFactory.getLogger(QANotificationResource.class);

	private static final String ENTITY_NAME = "qaNotification";

	private final QANotificationService qaNotificationService;

	private final MailService mailService;

	private final UserRepository userRepository;

	private final AssessmentInfoRepository assessmentInfoRepository;

	private final OrganizationRepository organizationRepository;

	public QANotificationResource(QANotificationService qaNotificationService, MailService mailService,
			UserRepository userRepository, AssessmentInfoRepository assessmentInfoRepository,
			OrganizationRepository organizationRepository) {
		this.qaNotificationService = qaNotificationService;
		this.mailService = mailService;
		this.userRepository = userRepository;
		this.assessmentInfoRepository = assessmentInfoRepository;
		this.organizationRepository = organizationRepository;
	}

	/**
	 * POST /qa-notifications/service-provider : Create a new qaNotification for
	 * service provider.
	 *
	 * @param qaNotification
	 *            the qaNotification to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qaNotification, or with status 400 (Bad Request) if the
	 *         qaNotification has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/qa-notifications/service-provider")
	@Timed
	public ResponseEntity<QANotification> createQANotificationForServiceProvider(
			@Valid @RequestBody QANotification qaNotification) throws URISyntaxException {
		log.debug("REST request to save QANotification : {}", qaNotification);
		if (qaNotification.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new qaNotification cannot already have an ID")).body(null);
		}
		QANotification result = qaNotificationService.save(qaNotification);
		return ResponseEntity.created(new URI("/api/qa-notifications/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /qa-notifications : Create a new qaNotification for merchant.
	 *
	 * @param qaNotification
	 *            the qaNotification to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qaNotification, or with status 400 (Bad Request) if the
	 *         qaNotification has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/qa-notifications")
	@Timed
	public ResponseEntity<QANotification> createQANotificationForMerchant(
			@Valid @RequestBody QANotification qaNotification) throws URISyntaxException {
		log.debug("REST request to save QANotification : {}", qaNotification);
		if (qaNotification.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new qaNotification cannot already have an ID")).body(null);
		}
		QANotification result = qaNotificationService.save(qaNotification);
		return ResponseEntity.created(new URI("/api/qa-notifications/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /qa-notifications/service-provider : Updates an existing qaNotification
	 * for service provider.
	 *
	 * @param qaNotification
	 *            the qaNotification to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         qaNotification, or with status 400 (Bad Request) if the
	 *         qaNotification is not valid, or with status 500 (Internal Server
	 *         Error) if the qaNotification couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/qa-notifications/service-provider")
	@Timed
	public ResponseEntity<QANotification> updateQANotificationForServiceProvider(
			@Valid @RequestBody QANotification qaNotification) throws URISyntaxException {
		log.debug("REST request to update QANotification : {}", qaNotification);
		if (qaNotification.getId() == null) {
			return createQANotificationForServiceProvider(qaNotification);
		}
		QANotification result = qaNotificationService.save(qaNotification);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qaNotification.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /qa-notifications : Updates an existing qaNotification for merchant.
	 *
	 * @param qaNotification
	 *            the qaNotification to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         qaNotification, or with status 400 (Bad Request) if the
	 *         qaNotification is not valid, or with status 500 (Internal Server
	 *         Error) if the qaNotification couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/qa-notifications")
	@Timed
	public ResponseEntity<QANotification> updateQANotificationForMerchant(
			@Valid @RequestBody QANotification qaNotification) throws URISyntaxException {
		log.debug("REST request to update QANotification : {}", qaNotification);
		if (qaNotification.getId() == null) {
			return createQANotificationForMerchant(qaNotification);
		}
		QANotification result = qaNotificationService.save(qaNotification);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qaNotification.getId().toString()))
				.body(result);
	}

	/**
	 * GET /qa-notifications/service-provider : get all the qaNotifications for
	 * service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qaNotifications in body
	 */
	@GetMapping("/qa-notifications/service-provider")
	@Timed
	public List<QANotification> getAllQANotificationsForServiceProvider() {
		log.debug("REST request to get all QANotifications");
		return qaNotificationService.findAll();
	}

	/**
	 * GET /qa-notifications : get all the qaNotifications for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qaNotifications in body
	 */
	@GetMapping("/qa-notifications")
	@Timed
	public List<QANotification> getAllQANotificationsForMerchant() {
		log.debug("REST request to get all QANotifications");
		return qaNotificationService.findAll();
	}
	
	/**
	 * GET /qa-notifications/with-status/service-provider : get the
	 * qaNotifications for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qaNotifications in body
	 */
	@GetMapping("/qa-notifications/with-status/service-provider")
	@Timed
	public ResponseEntity<QANotificationDTO> getQANotificationWithstatusForServiceProvider() {
		log.debug("REST request to get all QANotifications");
		QANotificationDTO qaNotification = qaNotificationService.findWithstatus();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qaNotification));
	}
	
	/**
	 * GET /qa-notifications/with-status : get the qaNotifications for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qaNotifications in body
	 */
	@GetMapping("/qa-notifications/with-status")
	@Timed
	public ResponseEntity<QANotificationDTO> getQANotificationWithstatusForMerchant() {
		log.debug("REST request to get all QANotifications");
		QANotificationDTO qaNotification = qaNotificationService.findWithstatus();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qaNotification));
	}

	/**
	 * GET /qa-notifications/service-provider/:id : get the "id" qaNotification for
	 * service provider.
	 *
	 * @param id
	 *            the id of the qaNotification to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         qaNotification, or with status 404 (Not Found)
	 */
	@GetMapping("/qa-notifications/service-provider/{id}")
	@Timed
	public ResponseEntity<QANotification> getQANotificationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get QANotification : {}", id);
		QANotification qaNotification = qaNotificationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qaNotification));
	}

	/**
	 * GET /qa-notifications/:id : get the "id" qaNotification for merchant.
	 *
	 * @param id
	 *            the id of the qaNotification to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         qaNotification, or with status 404 (Not Found)
	 */
	@GetMapping("/qa-notifications/{id}")
	@Timed
	public ResponseEntity<QANotification> getQANotificationForMerchant(@PathVariable Long id) {
		log.debug("REST request to get QANotification : {}", id);
		QANotification qaNotification = qaNotificationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qaNotification));
	}

	/**
	 * DELETE /qa-notifications/service-provider/:id : delete the "id"
	 * qaNotification for service provider.
	 *
	 * @param id
	 *            the id of the qaNotification to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/qa-notifications/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteQANotificationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete QANotification : {}", id);
		qaNotificationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /qa-notifications/:id : delete the "id" qaNotification for merchant.
	 *
	 * @param id
	 *            the id of the qaNotification to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/qa-notifications/{id}")
	@Timed
	public ResponseEntity<Void> deleteQANotificationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete QANotification : {}", id);
		qaNotificationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /qa-notifications/send-notification/service-provider : send email
	 * notification to QA for service provider.
	 *
	 * @param notification
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qaNotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/qa-notifications/send-notification/service-provider")
	@Timed
	public ResponseEntity<QANotification> sendNotificationForServiceProvider(@RequestBody User qaUser)
			throws URISyntaxException {
		Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		QANotification result = qaNotificationService.saveWithNotification(qaUser, user.get());
		LocalDate assessmentDate = assessmentInfoRepository.findAll().get(0).getAssessmentEndDate();
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToQA(user.get(), qaUser, assessmentDate, companyName);
		return ResponseEntity
				.created(new URI("/api/qa-notifications/send-notification/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /qa-notifications/send-notification : send email notification to QA for
	 * merchant.
	 *
	 * @param notification
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qaNotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/qa-notifications/send-notification")
	@Timed
	public ResponseEntity<QANotification> sendNotificationForMerchant(@RequestBody User qaUser)
			throws URISyntaxException {
		Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		QANotification result = qaNotificationService.saveWithNotification(qaUser, user.get());
		LocalDate assessmentDate = assessmentInfoRepository.findAll().get(0).getAssessmentEndDate();
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToQA(user.get(), qaUser, assessmentDate, companyName);
		return ResponseEntity
				.created(new URI("/api/qa-notifications/send-notification/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}
}
