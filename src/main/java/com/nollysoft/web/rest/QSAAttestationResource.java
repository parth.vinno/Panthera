package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.QSAAttestation;
import com.nollysoft.service.QSAAttestationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing QSAAttestation.
 */
@RestController
@RequestMapping("/api")
public class QSAAttestationResource {

	private final Logger log = LoggerFactory.getLogger(QSAAttestationResource.class);

	private static final String ENTITY_NAME = "qsaAttestation";

	private final QSAAttestationService qsaAttestationService;

	public QSAAttestationResource(QSAAttestationService qsaAttestationService) {
		this.qsaAttestationService = qsaAttestationService;
	}

	/**
	 * POST /qsa-attestations/service-provider : Create a new qsaAttestation for
	 * service provider)
	 *
	 * @param qsaAttestation
	 *            the qsaAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qsaAttestation, or with status 400 (Bad Request) if the
	 *         qsaAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/qsa-attestations/service-provider")
	@Timed
	public ResponseEntity<QSAAttestation> createQSAAttestationForServiceProvider(
			@RequestBody QSAAttestation qsaAttestation) throws URISyntaxException {
		log.debug("REST request to save QSAAttestation for service provider: {}", qsaAttestation);
		if (qsaAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new qsaAttestation cannot already have an ID")).body(null);
		}
		QSAAttestation result = qsaAttestationService.save(qsaAttestation);
		return ResponseEntity.created(new URI("/api/qsa-attestations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /qsa-attestations : Create a new qsaAttestation for merchant.
	 *
	 * @param qsaAttestation
	 *            the qsaAttestation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         qsaAttestation, or with status 400 (Bad Request) if the
	 *         qsaAttestation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/qsa-attestations")
	@Timed
	public ResponseEntity<QSAAttestation> createQSAAttestationForMerchant(@RequestBody QSAAttestation qsaAttestation)
			throws URISyntaxException {
		log.debug("REST request to save QSAAttestation for merchant: {}", qsaAttestation);
		if (qsaAttestation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new qsaAttestation cannot already have an ID")).body(null);
		}
		QSAAttestation result = qsaAttestationService.save(qsaAttestation);
		return ResponseEntity.created(new URI("/api/qsa-attestations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /qsa-attestations/service-provider : Updates an existing qsaAttestation
	 * for service provider.
	 *
	 * @param qsaAttestation
	 *            the qsaAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         qsaAttestation, or with status 400 (Bad Request) if the
	 *         qsaAttestation is not valid, or with status 500 (Internal Server
	 *         Error) if the qsaAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/qsa-attestations/service-provider")
	@Timed
	public ResponseEntity<QSAAttestation> updateQSAAttestationForServiceProvider(
			@RequestBody QSAAttestation qsaAttestation) throws URISyntaxException {
		log.debug("REST request to update QSAAttestation for service provider : {}", qsaAttestation);
		if (qsaAttestation.getId() == null) {
			return createQSAAttestationForServiceProvider(qsaAttestation);
		}
		QSAAttestation result = qsaAttestationService.save(qsaAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qsaAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /qsa-attestations : Updates an existing qsaAttestation for merchant.
	 *
	 * @param qsaAttestation
	 *            the qsaAttestation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         qsaAttestation, or with status 400 (Bad Request) if the
	 *         qsaAttestation is not valid, or with status 500 (Internal Server
	 *         Error) if the qsaAttestation couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/qsa-attestations")
	@Timed
	public ResponseEntity<QSAAttestation> updateQSAAttestationForMerchant(@RequestBody QSAAttestation qsaAttestation)
			throws URISyntaxException {
		log.debug("REST request to update QSAAttestation for merchant : {}", qsaAttestation);
		if (qsaAttestation.getId() == null) {
			return createQSAAttestationForMerchant(qsaAttestation);
		}
		QSAAttestation result = qsaAttestationService.save(qsaAttestation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qsaAttestation.getId().toString()))
				.body(result);
	}

	/**
	 * GET /qsa-attestations/service-provider : get all the qsaAttestation for
	 * service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qsaAttestations in body
	 */
	@GetMapping("/qsa-attestations/service-provider")
	@Timed
	public ResponseEntity<List<QSAAttestation>> getAllQSAAttestationesForServiceProvider() {
		log.debug("REST request to get a page of QSAAttestationes for service provider");
		List<QSAAttestation> list = qsaAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * GET /qsa-attestations : get all the qsaAttestationes for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qsaAttestationes in body
	 */
	@GetMapping("/qsa-attestations")
	@Timed
	public ResponseEntity<List<QSAAttestation>> getAllQSAAttestationesForMerchant() {
		log.debug("REST request to get a page of QSAAttestationes for merchant");
		List<QSAAttestation> list = qsaAttestationService.get();
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
	}

	/**
	 * DELETE /qsa-attestations/service-provider/:id : delete the "id"
	 * qsaAttestation for service provider.
	 *
	 * @param id
	 *            the id of the qsaAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/qsa-attestations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteQSAAttestationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete QSAAttestation for service provider : {}", id);
		qsaAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /qsa-attestations/:id : delete the "id" qsaAttestation for merchant.
	 * (Part 1B))
	 *
	 * @param id
	 *            the id of the qsaAttestation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/qsa-attestations/{id}")
	@Timed
	public ResponseEntity<Void> deleteQSAAttestationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete QSAAttestation for merchant : {}", id);
		qsaAttestationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
