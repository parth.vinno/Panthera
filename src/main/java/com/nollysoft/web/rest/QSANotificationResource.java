package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;
import com.nollysoft.domain.QSANotification;
import com.nollysoft.domain.User;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.QANotificationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.security.SecurityUtils;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.service.QSANotificationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing QSANotification.
 */
@RestController
@RequestMapping("/api")
public class QSANotificationResource {

	private final Logger log = LoggerFactory.getLogger(QSANotificationResource.class);

	private static final String ENTITY_NAME = "qsaNotification";

	private final QSANotificationService qsaNotificationService;

	private final QANotificationService qaNotificationService;

	private final QANotificationRepository qaNotificationRepository;

	private final UserRepository userRepository;

	private final MailService mailService;

	private final OrganizationRepository organizationRepository;

	public QSANotificationResource(QSANotificationService qsaNotificationService,
			QANotificationRepository qaNotificationRepository, UserRepository userRepository, MailService mailService,
			QANotificationService qaNotificationService, OrganizationRepository organizationRepository) {
		this.qsaNotificationService = qsaNotificationService;
		this.userRepository = userRepository;
		this.mailService = mailService;
		this.qaNotificationService = qaNotificationService;
		this.qaNotificationRepository = qaNotificationRepository;
		this.organizationRepository = organizationRepository;
	}

	/**
	 * POST /qsa-notifications : Create a new qsaNotification.
	 *
	 * @param qsaNotification
	 *            the qsaNotification to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new qsaNotification, or with status 400 (Bad Request) if the
	 *         qsaNotification has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/qsa-notifications")
	@Timed
	public ResponseEntity<QSANotification> createQSANotification(@Valid @RequestBody QSANotification qsaNotification)
			throws URISyntaxException {
		log.debug("REST request to save QSANotification : {}", qsaNotification);
		if (qsaNotification.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new qsaNotification cannot already have an ID")).body(null);
		}
		QSANotification result = qsaNotificationService.save(qsaNotification);
		return ResponseEntity.created(new URI("/api/qsa-notifications/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /qsa-notifications : Updates an existing qsaNotification.
	 *
	 * @param qsaNotification
	 *            the qsaNotification to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         qsaNotification, or with status 400 (Bad Request) if the
	 *         qsaNotification is not valid, or with status 500 (Internal Server
	 *         Error) if the qsaNotification couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/qsa-notifications")
	@Timed
	public ResponseEntity<QSANotification> updateQSANotification(@Valid @RequestBody QSANotification qsaNotification)
			throws URISyntaxException {
		log.debug("REST request to update QSANotification : {}", qsaNotification);
		if (qsaNotification.getId() == null) {
			return createQSANotification(qsaNotification);
		}
		QSANotification result = qsaNotificationService.save(qsaNotification);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qsaNotification.getId().toString()))
				.body(result);
	}

	/**
	 * GET /qsa-notifications : get all the qsaNotifications.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         qsaNotifications in body
	 */
	@GetMapping("/qsa-notifications")
	@Timed
	public List<QSANotification> getAllQSANotifications() {
		log.debug("REST request to get all QSANotifications");
		return qsaNotificationService.findAll();
	}

	/**
	 * GET /qsa-notifications/:id : get the "id" qsaNotification.
	 *
	 * @param id
	 *            the id of the qsaNotification to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         qsaNotification, or with status 404 (Not Found)
	 */
	@GetMapping("/qsa-notifications/{id}")
	@Timed
	public ResponseEntity<QSANotification> getQSANotification(@PathVariable Long id) {
		log.debug("REST request to get QSANotification : {}", id);
		QSANotification qsaNotification = qsaNotificationService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qsaNotification));
	}

	/**
	 * DELETE /qsa-notifications/:id : delete the "id" qsaNotification.
	 *
	 * @param id
	 *            the id of the qsaNotification to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/qsa-notifications/{id}")
	@Timed
	public ResponseEntity<Void> deleteQSANotification(@PathVariable Long id) {
		log.debug("REST request to delete QSANotification : {}", id);
		qsaNotificationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /qsa-notifications/send-notification/service-provider : send email
	 * notification to QSA for service provider.
	 *
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new qANotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/qsa-notifications/send-notification/service-provider")
	@Timed
	public ResponseEntity<QSANotification> sendNotificationForServiceProvider() throws URISyntaxException {
		Optional<User> qaUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		User qsaUser = qaNotificationRepository.findByQaId(qaUser.get()).getQsaId();
		QSANotification result = qsaNotificationService.create(qaUser.get(), qsaUser);
		qaNotificationService.updateStatus(NOTIFICATION_STATUS.RESPONSE_REQUIRED);
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToQSA(qaUser.get(), qsaUser, null, companyName);
		return ResponseEntity
				.created(new URI("/api/qsa-notifications/send-notification/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /qsa-notifications/send-notification : send email notification to
	 * QSA for merchant.
	 *
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new qANotification
	 * @throws URISyntaxException
	 */
	@PostMapping("/qsa-notifications/send-notification")
	@Timed
	public ResponseEntity<QSANotification> sendNotificationForMerchant() throws URISyntaxException {
		Optional<User> qaUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		User qsaUser = qaNotificationRepository.findByQaId(qaUser.get()).getQsaId();
		QSANotification result = qsaNotificationService.create(qaUser.get(), qsaUser);
		qaNotificationService.updateStatus(NOTIFICATION_STATUS.RESPONSE_REQUIRED);
		String companyName = organizationRepository.findAll().get(0).getCompanyName();
		mailService.sendNotificationMailToQSA(qaUser.get(), qsaUser, null, companyName);
		return ResponseEntity.created(new URI("/api/qsa-notifications/send-notification/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}
}
