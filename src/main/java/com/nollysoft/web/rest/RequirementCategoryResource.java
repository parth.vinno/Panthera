package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.RequirementCategory;
import com.nollysoft.service.RequirementCategoryService;
import com.nollysoft.service.dto.RequirementCategoryAdminDTO;
import com.nollysoft.service.dto.RequirementCategoryPageDTO;
import com.nollysoft.service.dto.RequirementCategoryUserDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing RequirementCategory.
 */
@RestController
@RequestMapping("/api")
public class RequirementCategoryResource {

	private final Logger log = LoggerFactory.getLogger(RequirementCategoryResource.class);

	private static final String ENTITY_NAME = "requirementCategory";

	private final RequirementCategoryService requirementCategoryService;

	public RequirementCategoryResource(RequirementCategoryService requirementCategoryService) {
		this.requirementCategoryService = requirementCategoryService;
	}

	/**
	 * POST /requirement-categories/service-provider : Create a new
	 * requirementCategory for service provider.
	 *
	 * @param requirementCategory
	 *            the requirementCategory to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new requirementCategory, or with status 400 (Bad Request) if the
	 *         requirementCategory has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/requirement-categories/service-provider")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> createRequirementCategoryForServiceProvider(
			@RequestBody RequirementCategory requirementCategory) throws URISyntaxException {
		log.debug("REST request to save RequirementCategory : {}", requirementCategory);
		if (requirementCategory.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new requirementCategory cannot already have an ID")).body(null);
		}
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			RequirementCategory result = requirementCategoryService.save(requirementCategory);
			return ResponseEntity.created(new URI("/api/requirement-categories/service-provider/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
					.body(new RequirementCategoryAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * POST /requirement-categories : Create a new requirementCategory for
	 * merchant.
	 *
	 * @param requirementCategory
	 *            the requirementCategory to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new requirementCategory, or with status 400 (Bad Request) if the
	 *         requirementCategory has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/requirement-categories")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> createRequirementCategoryForMerchant(
			@RequestBody RequirementCategory requirementCategory) throws URISyntaxException {
		log.debug("REST request to save RequirementCategory : {}", requirementCategory);
		if (requirementCategory.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new requirementCategory cannot already have an ID")).body(null);
		}
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			RequirementCategory result = requirementCategoryService.save(requirementCategory);
			return ResponseEntity.created(new URI("/api/merchant/requirementCategories/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
					.body(new RequirementCategoryAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * PUT /requirement-categories/service-provider : Updates an existing
	 * requirementCategory.
	 *
	 * @param requirementCategory
	 *            the requirementCategory to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         requirementCategory, or with status 400 (Bad Request) if the
	 *         requirementCategory is not valid, or with status 500 (Internal
	 *         Server Error) if the requirementCategory couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/requirement-categories/service-provider")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> updateRequirementCategoryForServiceProvider(
			@RequestBody RequirementCategory requirementCategory) throws URISyntaxException {
		log.debug("REST request to update RequirementCategory : {}", requirementCategory);
		if (requirementCategory.getId() == null) {
			return createRequirementCategoryForServiceProvider(requirementCategory);
		}
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			RequirementCategory result = requirementCategoryService.save(requirementCategory);
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementCategory.getId().toString()))
					.body(new RequirementCategoryAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * PUT /requirement-categories/service-provider : Updates an existing
	 * requirementCategory for merchant.
	 *
	 * @param requirementCategory
	 *            the requirementCategory to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         requirementCategory, or with status 400 (Bad Request) if the
	 *         requirementCategory is not valid, or with status 500 (Internal
	 *         Server Error) if the requirementCategory couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/requirement-categories")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> updateRequirementCategoryForMerchant(
			@RequestBody RequirementCategory requirementCategory) throws URISyntaxException {
		log.debug("REST request to update RequirementCategory : {}", requirementCategory);
		if (requirementCategory.getId() == null) {
			return createRequirementCategoryForMerchant(requirementCategory);
		}
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			RequirementCategory result = requirementCategoryService.save(requirementCategory);
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementCategory.getId().toString()))
					.body(new RequirementCategoryAdminDTO(result));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/service-provider : get all the
	 * requirementCategories for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories/service-provider")
	@Timed
	public ResponseEntity<List<RequirementCategoryAdminDTO>> getAllRequirementCategoriesForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			RequirementCategoryPageDTO page = requirementCategoryService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/serviceProvider/requirementCategories");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories : get all the requirementCategories for
	 * merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories")
	@Timed
	public ResponseEntity<List<RequirementCategoryAdminDTO>> getAllRequirementCategoriesForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			RequirementCategoryPageDTO page = requirementCategoryService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-categories");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/available/service-provider : get the
	 * requirementCategories with available as true for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories/available/service-provider")
	@Timed
	public ResponseEntity<List<RequirementCategoryUserDTO>> getRequirementCategoriesWithAvailableAsTrueForServiceProvider() {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			List<RequirementCategoryUserDTO> result = requirementCategoryService.findAllWithAvailability();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "list")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/available : get the requirementCategories
	 * with available as true for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories/available")
	@Timed
	public ResponseEntity<List<RequirementCategoryUserDTO>> getRequirementCategoriesWithAvailableAsTrueForMerchant() {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<RequirementCategoryUserDTO> result = requirementCategoryService.findAllWithAvailability();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "list")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/fixed/available/service-provider : get the
	 * requirementCategories having corrected saqResponses for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories/fixed/available/service-provider")
	@Timed
	public ResponseEntity<List<RequirementCategoryUserDTO>> getRequirementCategoriesWithCorrectedSaqResponseForServiceProvider() {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			List<RequirementCategoryUserDTO> result = requirementCategoryService.findAllWithCorrectedSaqResponse();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "list")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/fixed/available : get the
	 * requirementCategories having corrected saqResponses for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementCategories in body
	 */
	@GetMapping("/requirement-categories/fixed/available")
	@Timed
	public ResponseEntity<List<RequirementCategoryUserDTO>> getRequirementCategoriesWithCorrectedSaqResponseForMerchant() {
		log.debug("REST request to get a page of RequirementCategories");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			List<RequirementCategoryUserDTO> result = requirementCategoryService.findAllWithCorrectedSaqResponse();
			return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "list")).body(result);
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/service-provider/:id : get the "id"
	 * requirementCategory for service provider.
	 *
	 * @param id
	 *            the id of the requirementCategory to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirementCategory, or with status 404 (Not Found)
	 */
	@GetMapping("/requirement-categories/service-provider/{id}")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> getRequirementCategoryForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get RequirementCategory : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			RequirementCategory requirementCategory = requirementCategoryService.findOne(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(
					requirementCategory != null ? new RequirementCategoryAdminDTO(requirementCategory) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * GET /requirement-categories/:id : get the "id" requirementCategory for
	 * merchant.
	 *
	 * @param id
	 *            the id of the requirementCategory to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirementCategory, or with status 404 (Not Found)
	 */
	@GetMapping("/requirement-categories/{id}")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> getRequirementCategoryForMerchant(@PathVariable Long id) {
		log.debug("REST request to get RequirementCategory : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			RequirementCategory requirementCategory = requirementCategoryService.findOne(id);
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(
					requirementCategory != null ? new RequirementCategoryAdminDTO(requirementCategory) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * DELETE /requirement-categories/service-provider/:id : delete the "id"
	 * requirementCategory for service provider.
	 *
	 * @param id
	 *            the id of the requirementCategory to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/requirement-categories/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteRequirementCategoryForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete RequirementCategory : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbserviceprovider);
			requirementCategoryService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();
		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * DELETE /requirement-categories/:id : delete the "id" requirementCategory
	 * for merchant.
	 *
	 * @param id
	 *            the id of the requirementCategory to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/requirement-categories/{id}")
	@Timed
	public ResponseEntity<Void> deleteRequirementCategoryForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete RequirementCategory : {}", id);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			requirementCategoryService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();
		} finally {
			DbContextHolder.clearDbType();
		}
	}
	
	/**
	 * GET /requirement-categories/by-requirement : get the
	 * "requirement" requirementCategories for merchant.
	 *
	 * @param requirement
	 *            the requirement of the requirementCategories to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirementCategories, or with status 404 (Not Found)
	 */
	@GetMapping("/requirement-categories/by-category-name")
	@Timed
	public ResponseEntity<RequirementCategoryAdminDTO> getRequirementByRequirementForMerchant(
			@RequestParam(value = "categoryName", required = true) String categoryName) {
		log.debug("REST request to get Requirement : {}", categoryName);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			RequirementCategory requirementCategory = requirementCategoryService.findOneByCategoryName(categoryName);
			return ResponseUtil.wrapOrNotFound(Optional
					.ofNullable(requirementCategory != null ? new RequirementCategoryAdminDTO(requirementCategory) : null));
		} finally {
			DbContextHolder.clearDbType();
		}
	}
}