package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.RequirementNotTestedExplanation;
import com.nollysoft.service.RequirementNotTestedExplanationService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing RequirementNotTestedExplanation.
 */
@RestController
@RequestMapping("/api")
public class RequirementNotTestedExplanationResource {

	private final Logger log = LoggerFactory.getLogger(RequirementNotTestedExplanationResource.class);

	private static final String ENTITY_NAME = "requirementNotTestedExplanation";

	private final RequirementNotTestedExplanationService requirementNotTestedExplanationService;

	public RequirementNotTestedExplanationResource(
			RequirementNotTestedExplanationService requirementNotTestedExplanationService) {
		this.requirementNotTestedExplanationService = requirementNotTestedExplanationService;
	}

	/**
	 * POST /requirement-not-tested-explanations/service-provider : Create a new
	 * requirementNotTestedExplanation for service provider.
	 *
	 * @param requirementNotTestedExplanation
	 *            the requirementNotTestedExplanation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         requirementNotTestedExplanation, or with status 400 (Bad Request) if
	 *         the requirementNotTestedExplanation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/requirement-not-tested-explanations/service-provider")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> createRequirementNotTestedExplanationForServiceProvider(
			@Valid @RequestBody RequirementNotTestedExplanation requirementNotTestedExplanation)
			throws URISyntaxException {
		log.debug("REST request to save RequirementNotTestedExplanation : {}", requirementNotTestedExplanation);
		if (requirementNotTestedExplanation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new requirementNotTestedExplanation cannot already have an ID")).body(null);
		}
		RequirementNotTestedExplanation result = requirementNotTestedExplanationService
				.save(requirementNotTestedExplanation);
		return ResponseEntity
				.created(new URI("/api/requirement-not-tested-explanations/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /requirement-not-tested-explanations : Create a new
	 * requirementNotTestedExplanation for merchant.
	 *
	 * @param requirementNotTestedExplanation
	 *            the requirementNotTestedExplanation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         requirementNotTestedExplanation, or with status 400 (Bad Request) if
	 *         the requirementNotTestedExplanation has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/requirement-not-tested-explanations")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> createRequirementNotTestedExplanationForMerchant(
			@Valid @RequestBody RequirementNotTestedExplanation requirementNotTestedExplanation)
			throws URISyntaxException {
		log.debug("REST request to save RequirementNotTestedExplanation : {}", requirementNotTestedExplanation);
		if (requirementNotTestedExplanation.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new requirementNotTestedExplanation cannot already have an ID")).body(null);
		}
		RequirementNotTestedExplanation result = requirementNotTestedExplanationService
				.save(requirementNotTestedExplanation);
		return ResponseEntity.created(new URI("/api/requirement-not-tested-explanations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /requirement-not-tested-explanations/service-provider : Updates an
	 * existing requirementNotTestedExplanation for service provider.
	 *
	 * @param requirementNotTestedExplanation
	 *            the requirementNotTestedExplanation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         requirementNotTestedExplanation, or with status 400 (Bad Request) if
	 *         the requirementNotTestedExplanation is not valid, or with status 500
	 *         (Internal Server Error) if the requirementNotTestedExplanation
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/requirement-not-tested-explanations/service-provider")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> updateRequirementNotTestedExplanationForServiceProvider(
			@Valid @RequestBody RequirementNotTestedExplanation requirementNotTestedExplanation)
			throws URISyntaxException {
		log.debug("REST request to update RequirementNotTestedExplanation : {}", requirementNotTestedExplanation);
		if (requirementNotTestedExplanation.getId() == null) {
			return createRequirementNotTestedExplanationForServiceProvider(requirementNotTestedExplanation);
		}
		RequirementNotTestedExplanation result = requirementNotTestedExplanationService
				.save(requirementNotTestedExplanation);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementNotTestedExplanation.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /requirement-not-tested-explanations : Updates an existing
	 * requirementNotTestedExplanation for merchant.
	 *
	 * @param requirementNotTestedExplanation
	 *            the requirementNotTestedExplanation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         requirementNotTestedExplanation, or with status 400 (Bad Request) if
	 *         the requirementNotTestedExplanation is not valid, or with status 500
	 *         (Internal Server Error) if the requirementNotTestedExplanation
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/requirement-not-tested-explanations")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> updateRequirementNotTestedExplanationForMerchant(
			@Valid @RequestBody RequirementNotTestedExplanation requirementNotTestedExplanation)
			throws URISyntaxException {
		log.debug("REST request to update RequirementNotTestedExplanation : {}", requirementNotTestedExplanation);
		if (requirementNotTestedExplanation.getId() == null) {
			return createRequirementNotTestedExplanationForMerchant(requirementNotTestedExplanation);
		}
		RequirementNotTestedExplanation result = requirementNotTestedExplanationService
				.save(requirementNotTestedExplanation);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementNotTestedExplanation.getId().toString()))
				.body(result);
	}

	/**
	 * GET /requirement-not-tested-explanations/service-provider : get all the
	 * requirementNotTestedExplanations for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementNotTestedExplanations in body
	 */
	@GetMapping("/requirement-not-tested-explanations/service-provider")
	@Timed
	public List<RequirementNotTestedExplanation> getAllRequirementNotTestedExplanationsForServiceProvider() {
		log.debug("REST request to get all RequirementNotTestedExplanations");
		return requirementNotTestedExplanationService.findAll();
	}

	/**
	 * GET /requirement-not-tested-explanations : get all the
	 * requirementNotTestedExplanations for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         requirementNotTestedExplanations in body
	 */
	@GetMapping("/requirement-not-tested-explanations")
	@Timed
	public List<RequirementNotTestedExplanation> getAllRequirementNotTestedExplanationsForMerchant() {
		log.debug("REST request to get all RequirementNotTestedExplanations");
		return requirementNotTestedExplanationService.findAll();
	}

	/**
	 * GET /requirement-not-tested-explanations/service-provider/:id : get the "id"
	 * requirementNotTestedExplanation for service provider.
	 *
	 * @param id
	 *            the id of the requirementNotTestedExplanation to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirementNotTestedExplanation, or with status 404 (Not Found)
	 */
	@GetMapping("/requirement-not-tested-explanations/service-provider/{id}")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> getRequirementNotTestedExplanationForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get RequirementNotTestedExplanation : {}", id);
		RequirementNotTestedExplanation requirementNotTestedExplanation = requirementNotTestedExplanationService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementNotTestedExplanation));
	}

	/**
	 * GET /requirement-not-tested-explanations/:id : get the "id"
	 * requirementNotTestedExplanation for merchant.
	 *
	 * @param id
	 *            the id of the requirementNotTestedExplanation to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         requirementNotTestedExplanation, or with status 404 (Not Found)
	 */
	@GetMapping("/requirement-not-tested-explanations/{id}")
	@Timed
	public ResponseEntity<RequirementNotTestedExplanation> getRequirementNotTestedExplanationForMerchant(
			@PathVariable Long id) {
		log.debug("REST request to get RequirementNotTestedExplanation : {}", id);
		RequirementNotTestedExplanation requirementNotTestedExplanation = requirementNotTestedExplanationService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementNotTestedExplanation));
	}

	/**
	 * DELETE /requirement-not-tested-explanations/service-provider/:id : delete the
	 * "id" requirementNotTestedExplanation for service provider.
	 *
	 * @param id
	 *            the id of the requirementNotTestedExplanation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/requirement-not-tested-explanations/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteRequirementNotTestedExplanationForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete RequirementNotTestedExplanation : {}", id);
		requirementNotTestedExplanationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /requirement-not-tested-explanations/:id : delete the "id"
	 * requirementNotTestedExplanation for merchant.
	 *
	 * @param id
	 *            the id of the requirementNotTestedExplanation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/requirement-not-tested-explanations/{id}")
	@Timed
	public ResponseEntity<Void> deleteRequirementNotTestedExplanationForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete RequirementNotTestedExplanation : {}", id);
		requirementNotTestedExplanationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
