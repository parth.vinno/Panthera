package com.nollysoft.web.rest;

import io.github.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.SaqInfo;
import com.nollysoft.service.SaqInfoService;
import com.nollysoft.web.rest.util.HeaderUtil;

/**
 * REST controller for managing SaqInfo.
 */
@RestController
@RequestMapping("/api")
public class SaqInfoResource {

	private final Logger log = LoggerFactory.getLogger(SaqInfoResource.class);

	private static final String ENTITY_NAME = "saqInfo";

	private final SaqInfoService saqInfoService;

	public SaqInfoResource(SaqInfoService saqInfoService) {
		this.saqInfoService = saqInfoService;
	}

	/**
	 * POST /saq-infos/service-provider : Create a new saqInfo for service provider.
	 *
	 * @param saqInfo
	 *            the saqInfo to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqInfo, or with status 400 (Bad Request) if the saqInfo has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-infos/service-provider")
	@Timed
	public ResponseEntity<SaqInfo> createSaqInfoForServiceProvider(@RequestBody SaqInfo saqInfo)
			throws URISyntaxException {
		log.debug("REST request to save SaqInfo : {}", saqInfo);
		if (saqInfo.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new SaqInfo cannot already have an ID"))
					.body(null);
		}
		SaqInfo result = saqInfoService.save(saqInfo);
		return ResponseEntity.created(new URI("/api/saq-infos/service-provider" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /saq-infos : Create a new saqInfo for merchant
	 *
	 * @param saqInfo
	 *            the saqInfo to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqInfo, or with status 400 (Bad Request) if the saqInfo has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-infos")
	@Timed
	public ResponseEntity<SaqInfo> createSaqInfoForMerchant(@RequestBody SaqInfo saqInfo) throws URISyntaxException {
		log.debug("REST request to save SaqInfo : {}", saqInfo);
		if (saqInfo.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new SaqInfo cannot already have an ID"))
					.body(null);
		}
		SaqInfo result = saqInfoService.save(saqInfo);
		return ResponseEntity.created(new URI("/api/saq-infos" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /saq-infos/service-provider : Updates an existing saqInfo for service
	 * provider.
	 *
	 * @param saqInfo
	 *            the saqInfo to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqInfo, or with status 400 (Bad Request) if the saqInfo is not
	 *         valid, or with status 500 (Internal Server Error) if the saqInfo
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-infos/service-provider")
	@Timed
	public ResponseEntity<SaqInfo> updateSaqInfoForServiceProvider(@RequestBody SaqInfo saqInfo)
			throws URISyntaxException {
		log.debug("REST request to update SaqInfo : {}", saqInfo);
		if (saqInfo.getId() == null) {
			return createSaqInfoForServiceProvider(saqInfo);
		}
		SaqInfo result = saqInfoService.save(saqInfo);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqInfo.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /saq-infos : Updates an existing saqInfo for merchant.
	 *
	 * @param saqInfo
	 *            the saqInfo to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqInfo, or with status 400 (Bad Request) if the saqInfo is not
	 *         valid, or with status 500 (Internal Server Error) if the saqInfo
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-infos")
	@Timed
	public ResponseEntity<SaqInfo> updateSaqInfoForMerchant(@RequestBody SaqInfo saqInfo) throws URISyntaxException {
		log.debug("REST request to update SaqInfo : {}", saqInfo);
		if (saqInfo.getId() == null) {
			return createSaqInfoForMerchant(saqInfo);
		}
		SaqInfo result = saqInfoService.save(saqInfo);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqInfo.getId().toString()))
				.body(result);
	}

	/**
	 * GET /saq-infos/service-provider : get all the saqInfos for service provider.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of saqInfos in
	 *         body
	 */
	@GetMapping("/saq-infos/service-provider")
	@Timed
	public List<SaqInfo> getAllSaqInfosServiceProvider() {
		log.debug("REST request to get all SaqInfos");
		return saqInfoService.findAll();
	}

	/**
	 * GET /saq-infos : get all the saqInfos for merchant.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of saqInfos in
	 *         body
	 */
	@GetMapping("/saq-infos")
	@Timed
	public List<SaqInfo> getAllSaqInfosForMerchant() {
		log.debug("REST request to get all SaqInfos");
		return saqInfoService.findAll();
	}

	/**
	 * GET /saq-infos/service-provider:/id : get the "id" saqInfo for service
	 * provider.
	 *
	 * @param id
	 *            the id of the saqInfo to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the saqInfo, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/saq-infos/service-provider/{id}")
	@Timed
	public ResponseEntity<SaqInfo> getSaqInfoServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get SaqInfo : {}", id);
		SaqInfo saqInfo = saqInfoService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqInfo));
	}

	/**
	 * GET /saq-infos/:id : get the "id" saqInfo for merchant.
	 *
	 * @param id
	 *            the id of the saqInfo to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the saqInfo, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/saq-infos/{id}")
	@Timed
	public ResponseEntity<SaqInfo> getSaqInfoForMerchant(@PathVariable Long id) {
		log.debug("REST request to get SaqInfo : {}", id);
		SaqInfo saqInfo = saqInfoService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqInfo));
	}

	/**
	 * DELETE /saq-infos/service-provider/:id : delete the "id" saqInfo for service
	 * provider.
	 *
	 * @param id
	 *            the id of the saqInfo to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-infos/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqInfoServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete SaqInfo : {}", id);
		saqInfoService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /saq-infos/:id : delete the "id" saqInfo for merchant.
	 *
	 * @param id
	 *            the id of the saqInfo to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-infos/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqInfoForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete SaqInfo : {}", id);
		saqInfoService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
