package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.service.SaqQuestionService;
import com.nollysoft.service.dto.SaqQuestionAdminDTO;
import com.nollysoft.service.dto.SaqQuestionPageDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing SaqQuestion.
 */
@RestController
@RequestMapping("/api")
public class SaqQuestionResource {

	private final Logger log = LoggerFactory.getLogger(SaqQuestionResource.class);

	private static final String ENTITY_NAME = "saqQuestion";

	private final SaqQuestionService saqQuestionService;

	public SaqQuestionResource(SaqQuestionService saqQuestionService) {
		this.saqQuestionService = saqQuestionService;
	}

	/**
	 * POST /saq-questions/service-provider : Create a new saqQuestion for service
	 * provider.
	 *
	 * @param saqQuestion
	 *            the saqQuestion to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqQuestion, or with status 400 (Bad Request) if the saqQuestion has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-questions/service-provider")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> createSaqQuestionForServiceProvider(
			@Valid @RequestBody SaqQuestion saqQuestion) throws URISyntaxException {
		log.debug("REST request to save SaqQuestion : {}", saqQuestion);
		if (saqQuestion.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new saqQuestion cannot already have an ID")).body(null);
		}
		SaqQuestion result = saqQuestionService.save(saqQuestion);
		return ResponseEntity.created(new URI("/api/saq-questions/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqQuestionAdminDTO(result));
	}

	/**
	 * POST /saq-questions : Create a new saqQuestion for merchant.
	 *
	 * @param saqQuestion
	 *            the saqQuestion to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqQuestion, or with status 400 (Bad Request) if the saqQuestion has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-questions")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> createSaqQuestionForMerchant(@Valid @RequestBody SaqQuestion saqQuestion)
			throws URISyntaxException {
		log.debug("REST request to save SaqQuestion : {}", saqQuestion);
		if (saqQuestion.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new saqQuestion cannot already have an ID")).body(null);
		}
		SaqQuestion result = saqQuestionService.save(saqQuestion);
		return ResponseEntity.created(new URI("/api/saq-questions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqQuestionAdminDTO(result));
	}

	/**
	 * PUT /saq-questions/service-provider : Updates an existing saqQuestion for
	 * service provider.
	 *
	 * @param saqQuestion
	 *            the saqQuestion to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqQuestion, or with status 400 (Bad Request) if the saqQuestion is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         saqQuestion couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-questions/service-provider")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> updateSaqQuestionForServiceProvider(
			@Valid @RequestBody SaqQuestion saqQuestion) throws URISyntaxException {
		log.debug("REST request to update SaqQuestion : {}", saqQuestion);
		if (saqQuestion.getId() == null) {
			return createSaqQuestionForServiceProvider(saqQuestion);
		}
		SaqQuestion result = saqQuestionService.save(saqQuestion);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqQuestion.getId().toString()))
				.body(new SaqQuestionAdminDTO(result));
	}

	/**
	 * PUT /saq-questions : Updates an existing saqQuestion for merchant.
	 *
	 * @param saqQuestion
	 *            the saqQuestion to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqQuestion, or with status 400 (Bad Request) if the saqQuestion is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         saqQuestion couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-questions")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> updateSaqQuestionForMerchant(@Valid @RequestBody SaqQuestion saqQuestion)
			throws URISyntaxException {
		log.debug("REST request to update SaqQuestion : {}", saqQuestion);
		if (saqQuestion.getId() == null) {
			return createSaqQuestionForServiceProvider(saqQuestion);
		}
		SaqQuestion result = saqQuestionService.save(saqQuestion);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqQuestion.getId().toString()))
				.body(new SaqQuestionAdminDTO(result));
	}

	/**
	 * GET /saq-questions/service-provider : get all the saqQuestions for service
	 * provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of saqQuestions
	 *         in body
	 */
	@GetMapping("/saq-questions/service-provider")
	@Timed
	public ResponseEntity<List<SaqQuestionAdminDTO>> getAllSaqQuestionsForServiceProvider(Pageable pageable) {
		log.debug("REST request to get a page of SaqQuestions");
		SaqQuestionPageDTO page = saqQuestionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/saq-questions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /saq-questions : get all the saqQuestions for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of saqQuestions
	 *         in body
	 */
	@GetMapping("/saq-questions")
	@Timed
	public ResponseEntity<List<SaqQuestionAdminDTO>> getAllSaqQuestionsForMerchant(Pageable pageable) {
		log.debug("REST request to get a page of SaqQuestions");
		SaqQuestionPageDTO page = saqQuestionService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/saq-questions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /saq-questions/service-provider/:id : get the "id" saqQuestion for
	 * service provider.
	 *
	 * @param id
	 *            the id of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqQuestion, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-questions/service-provider/{id}")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> getSaqQuestionForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get SaqQuestion : {}", id);
		SaqQuestion saqQuestion = saqQuestionService.findOne(id);
		return ResponseUtil
				.wrapOrNotFound(Optional.ofNullable(saqQuestion != null ? new SaqQuestionAdminDTO(saqQuestion) : null));
	}

	/**
	 * GET /saq-questions/:id : get the "id" saqQuestion for merchant.
	 *
	 * @param id
	 *            the id of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqQuestion, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-questions/{id}")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> getSaqQuestionForMerchant(@PathVariable Long id) {
		log.debug("REST request to get SaqQuestion : {}", id);
		SaqQuestion saqQuestion = saqQuestionService.findOne(id);
		return ResponseUtil
				.wrapOrNotFound(Optional.ofNullable(saqQuestion != null ? new SaqQuestionAdminDTO(saqQuestion) : null));
	}

	/**
	 * GET /saq-questions/by-question-number/service-provider : get the
	 * "questionNumber" saqQuestion for service provider.
	 *
	 * @param questionNumber
	 *            the questionNumber of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqQuestion, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-questions/by-question-number/service-provider")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> getSaqQuestionByQuestionNumberForServiceProvider(
			@RequestParam(value = "questionNumber", required = true) String questionNumber) {
		log.debug("REST request to get SaqQuestion : {}", questionNumber);
		SaqQuestion saqQuestion = saqQuestionService.findOneByQuestionNumber(questionNumber);
		return ResponseUtil
				.wrapOrNotFound(Optional.ofNullable(saqQuestion != null ? new SaqQuestionAdminDTO(saqQuestion) : null));
	}
	
	/**
	 * GET /saq-questions/by-question-number : get the
	 * "questionNumber" saqQuestion for merchant.
	 *
	 * @param questionNumber
	 *            the questionNumber of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqQuestion, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-questions/by-question-number")
	@Timed
	public ResponseEntity<SaqQuestionAdminDTO> getSaqQuestionByQuestionNumberForMerchant(
			@RequestParam(value = "questionNumber", required = true) String questionNumber) {
		log.debug("REST request to get SaqQuestion : {}", questionNumber);
		SaqQuestion saqQuestion = saqQuestionService.findOneByQuestionNumber(questionNumber);
		return ResponseUtil
				.wrapOrNotFound(Optional.ofNullable(saqQuestion != null ? new SaqQuestionAdminDTO(saqQuestion) : null));
	}

	/**
	 * GET /saq-questions/:id : get the "pcidssrequirementId" saqQuestion for merchant.
	 *
	 * @param id
	 *            the id of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqQuestion, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-questions/questionByPcidssRequirementId/{pcidssRequirementId}")
	@Timed
	public ResponseEntity<List<SaqQuestionAdminDTO>> getAllSaqQuestionByPcidssRequirementForMerchant(
			@PathVariable Long pcidssRequirementId, @ApiParam Pageable pageable) {
		log.debug("REST request to get SaqQuestion : {}", pcidssRequirementId);
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			SaqQuestionPageDTO page = saqQuestionService.findSaqQuestionByPcidssRequirement(pcidssRequirementId, pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"/api/saq-questions/questionByPcidssRequirementId/" + pcidssRequirementId);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

		} finally {
			DbContextHolder.clearDbType();
		}
	}

	/**
	 * DELETE /saq-questions/service-provider/:id : delete the "id" saqQuestion for
	 * service provider.
	 *
	 * @param id
	 *            the id of the saqQuestion to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-questions/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqQuestionForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete SaqQuestion : {}", id);
		saqQuestionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /saq-questions/:id : delete the "id" saqQuestion for merchant.
	 *
	 * @param id
	 *            the id of the saqQuestion to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-questions/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqQuestionForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete SaqQuestion : {}", id);
		saqQuestionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}