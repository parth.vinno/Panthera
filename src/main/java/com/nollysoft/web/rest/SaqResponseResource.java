package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.SaqResponse;
import com.nollysoft.service.SaqResponseService;
import com.nollysoft.service.dto.SaqResponseDTO;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing SaqResponse.
 */
@RestController
@RequestMapping("/api")
public class SaqResponseResource {

	private final Logger log = LoggerFactory.getLogger(SaqResponseResource.class);

	private static final String ENTITY_NAME = "saqResponse";

	private final SaqResponseService saqResponseService;

	public SaqResponseResource(SaqResponseService saqResponseService) {
		this.saqResponseService = saqResponseService;
	}

	/**
	 * POST /saq-responses/service-provider : Create a new saqResponse for service
	 * provider.
	 *
	 * @param saqResponse
	 *            the saqResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-responses/service-provider")
	@Timed
	public ResponseEntity<SaqResponse> createSaqResponseForServiceProvider(@Valid @RequestBody SaqResponse saqResponse)
			throws URISyntaxException {
		log.debug("REST request to save SaqResponse : {}", saqResponse);
		if (saqResponse.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new saqResponse cannot already have an ID")).body(null);
		}
		SaqResponse result = saqResponseService.save(saqResponse);
		return ResponseEntity.created(new URI("/api/saq-responses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /saq-responses : Create a new saqResponse for merchant.
	 *
	 * @param saqResponse
	 *            the saqResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-responses")
	@Timed
	public ResponseEntity<SaqResponse> createSaqResponseForMerchant(@Valid @RequestBody SaqResponse saqResponse)
			throws URISyntaxException {
		log.debug("REST request to save SaqResponse : {}", saqResponse);
		if (saqResponse.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new saqResponse cannot already have an ID")).body(null);
		}
		SaqResponse result = saqResponseService.save(saqResponse);
		return ResponseEntity.created(new URI("/api/saq-responses/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /saq-responses/with-appendixes/service-provider : Create a new
	 * saqResponse for service provider.
	 *
	 * @param saqResponse
	 *            the saqResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-responses/with-appendixes/service-provider")
	@Timed
	public ResponseEntity<SaqResponseDTO> createSaqResponseWithAppendixesForServiceProvider(
			@RequestBody SaqResponseDTO saqResponseDTO) throws URISyntaxException {
		SaqResponseDTO result = saqResponseService.saveWithAppendixes(saqResponseDTO);
		return ResponseEntity.created(new URI("/api/saq-responses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /saq-responses/with-appendixes : Create a new saqResponse for merchant.
	 *
	 * @param saqResponse
	 *            the saqResponse to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-responses/with-appendixes")
	@Timed
	public ResponseEntity<SaqResponseDTO> createSaqResponseWithAppendixesForMerchant(
			@RequestBody SaqResponseDTO saqResponseDTO) throws URISyntaxException {
		SaqResponseDTO result = saqResponseService.saveWithAppendixes(saqResponseDTO);
		return ResponseEntity.created(new URI("/api/saq-responses/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /saq-responses/service-provider : Updates an existing saqResponse for
	 * service provider.
	 *
	 * @param saqResponse
	 *            the saqResponse to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         saqResponse couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-responses/service-provider")
	@Timed
	public ResponseEntity<SaqResponse> updateSaqResponseForServiceProvider(@Valid @RequestBody SaqResponse saqResponse)
			throws URISyntaxException {
		log.debug("REST request to update SaqResponse : {}", saqResponse);
		if (saqResponse.getId() == null) {
			return createSaqResponseForServiceProvider(saqResponse);
		}
		SaqResponse result = saqResponseService.save(saqResponse);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqResponse.getId().toString())).body(result);
	}

	/**
	 * PUT /saq-responses : Updates an existing saqResponse for merchant.
	 *
	 * @param saqResponse
	 *            the saqResponse to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqResponse, or with status 400 (Bad Request) if the saqResponse is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         saqResponse couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-responses")
	@Timed
	public ResponseEntity<SaqResponse> updateSaqResponseForMerchant(@Valid @RequestBody SaqResponse saqResponse)
			throws URISyntaxException {
		log.debug("REST request to update SaqResponse : {}", saqResponse);
		if (saqResponse.getId() == null) {
			return createSaqResponseForMerchant(saqResponse);
		}
		SaqResponse result = saqResponseService.save(saqResponse);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqResponse.getId().toString())).body(result);
	}

	/**
	 * GET /saq-responses/service-provider : get all the saqResponses for service
	 * provider.
	 *
	 * @param filter
	 *            the filter of the request
	 * @return the ResponseEntity with status 200 (OK) and the list of saqResponses
	 *         in body
	 */
	@GetMapping("/saq-responses/service-provider")
	@Timed
	public List<SaqResponse> getAllSaqResponsesForServiceProvider(@RequestParam(required = false) String filter) {
		if ("saqquestionid-is-null".equals(filter)) {
			log.debug("REST request to get all SaqResponses where saqQuestionId is null");
			return saqResponseService.findAllWhereSaqQuestionIdIsNull();
		}
		log.debug("REST request to get all SaqResponses");
		return saqResponseService.findAll();
	}

	/**
	 * GET /saq-responses : get all the saqResponses for merchant.
	 *
	 * @param filter
	 *            the filter of the request
	 * @return the ResponseEntity with status 200 (OK) and the list of saqResponses
	 *         in body
	 */
	@GetMapping("/saq-responses")
	@Timed
	public List<SaqResponse> getAllSaqResponsesForMerchant(@RequestParam(required = false) String filter) {
		if ("saqquestionid-is-null".equals(filter)) {
			log.debug("REST request to get all SaqResponses where saqQuestionId is null");
			return saqResponseService.findAllWhereSaqQuestionIdIsNull();
		}
		log.debug("REST request to get all SaqResponses");
		return saqResponseService.findAll();
	}

	/**
	 * GET /saq-responses/service-provider/:id : get the "id" saqResponse for
	 * service provider.
	 *
	 * @param id
	 *            the id of the saqResponse to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqResponse, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<SaqResponse> getSaqResponseForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get SaqResponse : {}", id);
		SaqResponse saqResponse = saqResponseService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqResponse));
	}

	/**
	 * GET /saq-responses/:id : get the "id" saqResponse for merchant.
	 *
	 * @param id
	 *            the id of the saqResponse to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqResponse, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-responses/{id}")
	@Timed
	public ResponseEntity<SaqResponse> getSaqResponseForMerchant(@PathVariable Long id) {
		log.debug("REST request to get SaqResponse : {}", id);
		SaqResponse saqResponse = saqResponseService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqResponse));
	}

	/**
	 * GET /saq-responses/with-appendixes/service-provider/:saqQuestionId : get the
	 * saqResponseDTO for merchant by saqQuestionId.
	 *
	 * @param id
	 *            the id of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqResponseDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-responses/with-appendixes/service-provider/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqResponseDTO> getSaqResponseBySaqQuestionIdForServiceProvider(
			@PathVariable Long saqQuestionId) {
		log.debug("REST request to get SaqResponse : {}", saqQuestionId);
		SaqResponseDTO saqResponseDTO = saqResponseService.findOneBySaqQuestionId(saqQuestionId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqResponseDTO));
	}

	/**
	 * GET /saq-responses/with-appendixes/:saqQuestionId : get the saqResponseDTO
	 * for merchant by saqQuestionId.
	 *
	 * @param id
	 *            the id of the saqQuestion to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqResponseDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-responses/with-appendixes/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqResponseDTO> getSaqResponseBySaqQuestionIdForMerchant(@PathVariable Long saqQuestionId) {
		log.debug("REST request to get SaqResponse : {}", saqQuestionId);
		SaqResponseDTO saqResponseDTO = saqResponseService.findOneBySaqQuestionId(saqQuestionId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqResponseDTO));
	}

	/**
	 * DELETE /saq-responses/service-provider/:id : delete the "id" saqResponse.
	 *
	 * @param id
	 *            the id of the saqResponse to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-responses/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqResponse(@PathVariable Long id) {
		log.debug("REST request to delete SaqResponse : {}", id);
		saqResponseService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /saq-responses/:id : delete the "id" saqResponse.
	 *
	 * @param id
	 *            the id of the saqResponse to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-responses/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqResponseForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete SaqResponse : {}", id);
		saqResponseService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
