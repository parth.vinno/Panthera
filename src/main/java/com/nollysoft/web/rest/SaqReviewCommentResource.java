package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.SaqReviewComment;
import com.nollysoft.service.SaqReviewCommentService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing SaqReviewComment.
 */
@RestController
@RequestMapping("/api")
public class SaqReviewCommentResource {

	private final Logger log = LoggerFactory.getLogger(SaqReviewCommentResource.class);

	private static final String ENTITY_NAME = "saqReviewComment";

	private final SaqReviewCommentService saqReviewCommentService;

	public SaqReviewCommentResource(SaqReviewCommentService saqReviewCommentService) {
		this.saqReviewCommentService = saqReviewCommentService;
	}

	/**
	 * POST /saq-review-comments : Create a new saqReviewComment.
	 *
	 * @param saqReviewComment
	 *            the saqReviewComment to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new saqReviewComment, or with status 400 (Bad Request) if the
	 *         saqReviewComment has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-review-comments")
	@Timed
	public ResponseEntity<SaqReviewComment> createSaqReviewComment(@RequestBody SaqReviewComment saqReviewComment) throws URISyntaxException {
		log.debug("REST request to save SaqReviewComment : {}", saqReviewComment);
		if (saqReviewComment.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new saqReviewComment cannot already have an ID")).body(null);
		}
		SaqReviewComment result = saqReviewCommentService.save(saqReviewComment);
		return ResponseEntity.created(new URI("/api/saq-review-comments/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /saq-review-comments : Updates an existing saqReviewComment.
	 *
	 * @param saqReviewComment
	 *            the saqReviewComment to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqReviewComment, or with status 400 (Bad Request) if the
	 *         saqReviewComment is not valid, or with status 500 (Internal
	 *         Server Error) if the saqReviewComment couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-review-comments")
	@Timed
	public ResponseEntity<SaqReviewComment> updateSaqReviewComment(
			@Valid @RequestBody SaqReviewComment saqReviewComment) throws URISyntaxException {
		log.debug("REST request to update SaqReviewComment : {}", saqReviewComment);
		if (saqReviewComment.getId() == null) {
			return createSaqReviewComment(saqReviewComment);
		}
		SaqReviewComment result = saqReviewCommentService.save(saqReviewComment);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqReviewComment.getId().toString()))
				.body(result);
	}

	/**
	 * GET /saq-review-comments : get all the saqReviewComments.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         saqReviewComments in body
	 */
	@GetMapping("/saq-review-comments")
	@Timed
	public List<SaqReviewComment> getAllSaqReviewComments() {
		log.debug("REST request to get all SaqReviewComments");
		return saqReviewCommentService.findAll();
	}

	/**
	 * GET /saq-review-comments/:id : get the "id" saqReviewComment.
	 *
	 * @param id
	 *            the id of the saqReviewComment to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqReviewComment, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-review-comments/{id}")
	@Timed
	public ResponseEntity<SaqReviewComment> getSaqReviewComment(@PathVariable Long id) {
		log.debug("REST request to get SaqReviewComment : {}", id);
		SaqReviewComment saqReviewComment = saqReviewCommentService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqReviewComment));
	}

	/**
	 * DELETE /saq-review-comments/:id : delete the "id" saqReviewComment.
	 *
	 * @param id
	 *            the id of the saqReviewComment to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-review-comments/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqReviewComment(@PathVariable Long id) {
		log.debug("REST request to delete SaqReviewComment : {}", id);
		saqReviewCommentService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
