package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.SaqReview;
import com.nollysoft.service.SaqReviewService;
import com.nollysoft.service.dto.SaqReviewDTO;
import com.nollysoft.service.dto.SaqReviewPageDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing SaqReview.
 */
@RestController
@RequestMapping("/api")
public class SaqReviewResource {

	private final Logger log = LoggerFactory.getLogger(SaqReviewResource.class);

	private static final String ENTITY_NAME = "saqReview";

	private final SaqReviewService saqReviewService;

	public SaqReviewResource(SaqReviewService saqReviewService) {
		this.saqReviewService = saqReviewService;
	}

	/**
	 * POST /saq-reviews : Create a new saqReview.
	 *
	 * @param saqReview
	 *            the saqReview to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new saqReview, or with status 400 (Bad Request) if the saqReview
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-reviews")
	@Timed
	public ResponseEntity<SaqReviewDTO> createSaqReview(@Valid @RequestBody SaqReview saqReview)
			throws URISyntaxException {
		log.debug("REST request to save SaqReview : {}", saqReview);
		if (saqReview.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new saqReview cannot already have an ID"))
					.body(null);
		}
		SaqReview result = saqReviewService.save(saqReview);
		return ResponseEntity.created(new URI("/api/saq-reviews/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * POST /saq-reviews/with-question-id/service-provider : Create a new
	 * saqReview for service provider.
	 *
	 * @param saqQuestionId
	 * @param saqReview
	 *            the saqReview to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new saqReview
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-reviews/with-question-id/service-provider")
	@Timed
	public ResponseEntity<SaqReviewDTO> createSaqReviewForServiceProvider(
			@RequestParam(value = "saqQuestionId") Long saqQuestionId, @RequestParam(value = "status") String status,
			@RequestBody SaqReview saqReview) throws URISyntaxException {
		log.debug("REST request to save SaqReview : {}", saqReview);
		SaqReview result = saqReviewService.create(saqQuestionId, status, saqReview);
		return ResponseEntity.created(new URI("/api/saq-reviews/with-question-id/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * POST /saq-reviews/with-question-id : Create a new saqReview for merchant.
	 *
	 * @param saqQuestionId
	 * @param saqReview
	 *            the saqReview to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new saqReview
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/saq-reviews/with-question-id")
	@Timed
	public ResponseEntity<SaqReviewDTO> createSaqReviewForMerchant(
			@RequestParam(value = "saqQuestionId") Long saqQuestionId, @RequestParam(value = "status") String status,
			@RequestBody SaqReview saqReview) throws URISyntaxException {
		log.debug("REST request to save SaqReview : {}", saqReview);
		SaqReview result = saqReviewService.create(saqQuestionId, status, saqReview);
		return ResponseEntity.created(new URI("/api/saq-reviews/with-question-id/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * PUT /saq-reviews : Updates an existing saqReview.
	 *
	 * @param saqReview
	 *            the saqReview to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqReview, or with status 400 (Bad Request) if the saqReview is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         saqReview couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-reviews")
	@Timed
	public ResponseEntity<SaqReviewDTO> updateSaqReview(@Valid @RequestBody SaqReview saqReview)
			throws URISyntaxException {
		log.debug("REST request to update SaqReview : {}", saqReview);
		if (saqReview.getId() == null) {
			return createSaqReview(saqReview);
		}
		SaqReview result = saqReviewService.save(saqReview);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saqReview.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * PUT /saq-reviews/corrected/service-provider/{saqQuestionId} : Updates an
	 * existing saqReview for service provider.
	 *
	 * @param saqQuestionId
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqReview, or with status 500 (Internal Server Error) if the
	 *         saqReview couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-reviews/corrected/service-provider/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqReviewDTO> updateSaqReviewCorrectedForServiceProvider(@PathVariable Long saqQuestionId)
			throws URISyntaxException {
		log.debug("REST request to update SaqReview : {}", saqQuestionId);
		SaqReview result = saqReviewService.update(saqQuestionId);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * PUT /saq-reviews/corrected/{saqQuestionId} : Updates an existing
	 * saqReview for merchant.
	 *
	 * @param saqQuestionId
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         saqReview, or with status 500 (Internal Server Error) if the
	 *         saqReview couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/saq-reviews/corrected/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqReviewDTO> updateSaqReviewCorrectedForMerchant(@PathVariable Long saqQuestionId)
			throws URISyntaxException {
		log.debug("REST request to update SaqReview : {}", saqQuestionId);
		SaqReview result = saqReviewService.update(saqQuestionId);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(new SaqReviewDTO(result));
	}

	/**
	 * GET /saq-questions/service-provider : get all the saqReviews for service
	 * provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         saqReviews in body
	 */

	@GetMapping("/saq-reviews/service-provider")
	@Timed
	public ResponseEntity<List<SaqReviewDTO>> getAllSaqReviewsForServiceProvider(
			@RequestParam(value = "status", required = false) String status, Pageable pageable) {
		log.debug("REST request to get a page of SaqReviews");
		SaqReviewPageDTO page = saqReviewService.findAll(status, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/saq-questions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /saq-questions : get all the saqReviews for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         saqReviews in body
	 */
	@GetMapping("/saq-reviews")
	@Timed
	public ResponseEntity<List<SaqReviewDTO>> getAllSaqReviewsForMerchant(
			@RequestParam(value = "status", required = false) String status, Pageable pageable) {
		log.debug("REST request to get a page of SaqReviews");
		SaqReviewPageDTO page = saqReviewService.findAll(status, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/saq-questions/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /saq-reviews/:id : get the "id" saqReview.
	 *
	 * @param id
	 *            the id of the saqReview to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqReview, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-reviews/{id}")
	@Timed
	public ResponseEntity<SaqReviewDTO> getSaqReview(@PathVariable Long id) {
		log.debug("REST request to get SaqReview : {}", id);
		SaqReview saqReview = saqReviewService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqReview != null ? new SaqReviewDTO(saqReview) : null));
	}

	/**
	 * GET /saq-reviews/saq-question/service-provider/:saqQuestionId : get the
	 * "saqQuestionId" saqReview.
	 *
	 * @param saqQuestionId
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqReview, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-reviews/saq-question/service-provider/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqReviewDTO> getSaqReviewBySaqQuestionIdForServiceProvider(
			@PathVariable Long saqQuestionId) {
		log.debug("REST request to get SaqReview : {}", saqQuestionId);
		SaqReview saqReview = saqReviewService.findBySaqQuestionId(saqQuestionId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqReview != null ? new SaqReviewDTO(saqReview) : null));
	}

	/**
	 * GET /saq-reviews/saq-question/:saqQuestionId : get the "saqQuestionId"
	 * saqReview.
	 *
	 * @param saqQuestionId
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         saqReview, or with status 404 (Not Found)
	 */
	@GetMapping("/saq-reviews/saq-question/{saqQuestionId}")
	@Timed
	public ResponseEntity<SaqReviewDTO> getSaqReviewBySaqQuestionIdForMerchant(@PathVariable Long saqQuestionId) {
		log.debug("REST request to get SaqReview : {}", saqQuestionId);
		SaqReview saqReview = saqReviewService.findBySaqQuestionId(saqQuestionId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saqReview != null ? new SaqReviewDTO(saqReview) : null));
	}

	/**
	 * DELETE /saq-reviews/service-provider/:id : delete the "id" saqReview.
	 *
	 * @param id
	 *            the id of the saqReview to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-reviews/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqReviewForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete SaqReview : {}", id);
		saqReviewService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * DELETE /saq-reviews/:id : delete the "id" saqReview.
	 *
	 * @param id
	 *            the id of the saqReview to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/saq-reviews/{id}")
	@Timed
	public ResponseEntity<Void> deleteSaqReviewForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete SaqReview : {}", id);
		saqReviewService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
