package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ScopeVerificationNotSelectedService;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ScopeVerificationNotSelectedServiceService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ScopeVerificationNotSelectedService.
 */
@RestController
@RequestMapping("/api")
public class ScopeVerificationNotSelectedServiceResource {

	private final Logger log = LoggerFactory.getLogger(ScopeVerificationNotSelectedServiceResource.class);

	private static final String ENTITY_NAME = "scopeVerificationNotSelectedService";

	private final ScopeVerificationNotSelectedServiceService scopeVerificationNotSelectedServiceService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public ScopeVerificationNotSelectedServiceResource(
			ScopeVerificationNotSelectedServiceService scopeVerificationNotSelectedServiceService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.scopeVerificationNotSelectedServiceService = scopeVerificationNotSelectedServiceService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /scope-verification-not-selected-services/service-provider : Create
	 * a new scopeVerificationNotSelectedService for service provider.
	 *
	 * @param scopeVerificationNotSelectedService
	 *            the scopeVerificationNotSelectedService to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new scopeVerificationNotSelectedService, or with status 400 (Bad
	 *         Request) if the scopeVerificationNotSelectedService has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/scope-verification-not-selected-services/service-provider")
	@Timed
	public ResponseEntity<ScopeVerificationNotSelectedService> createScopeVerificationNotSelectedService(
			@RequestBody ScopeVerificationNotSelectedService scopeVerificationNotSelectedService) throws URISyntaxException {
		log.debug("REST request to save ScopeVerificationNotSelectedService : {}", scopeVerificationNotSelectedService);
		if (scopeVerificationNotSelectedService.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new scopeVerificationNotSelectedService cannot already have an ID")).body(null);
		}
		ScopeVerificationNotSelectedService result = scopeVerificationNotSelectedServiceService
				.save(scopeVerificationNotSelectedService);
		executiveSummaryStatusService.createOrUpdate("NOT_SELECTED_SERVICES");
		return ResponseEntity
				.created(new URI(
						"/api/scope-verification-not-selected-services/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /scope-verification-not-selected-services/service-provider : Updates
	 * an existing scopeVerificationNotSelectedService for service provider.
	 *
	 * @param scopeVerificationNotSelectedService
	 *            the scopeVerificationNotSelectedService to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         scopeVerificationNotSelectedService, or with status 400 (Bad
	 *         Request) if the scopeVerificationNotSelectedService is not valid,
	 *         or with status 500 (Internal Server Error) if the
	 *         scopeVerificationNotSelectedService couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/scope-verification-not-selected-services/service-provider")
	@Timed
	public ResponseEntity<ScopeVerificationNotSelectedService> updateScopeVerificationNotSelectedService(
			@RequestBody ScopeVerificationNotSelectedService scopeVerificationNotSelectedService) throws URISyntaxException {
		log.debug("REST request to update ScopeVerificationNotSelectedService : {}",
				scopeVerificationNotSelectedService);
		if (scopeVerificationNotSelectedService.getId() == null) {
			return createScopeVerificationNotSelectedService(scopeVerificationNotSelectedService);
		}
		ScopeVerificationNotSelectedService result = scopeVerificationNotSelectedServiceService
				.save(scopeVerificationNotSelectedService);
		executiveSummaryStatusService.createOrUpdate("NOT_SELECTED_SERVICES");
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, scopeVerificationNotSelectedService.getId().toString()))
				.body(result);
	}

	/**
	 * GET /scope-verification-not-selected-services/service-provider : get all
	 * the scopeVerificationNotSelectedServices for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         scopeVerificationNotSelectedServices in body
	 */
	@GetMapping("/scope-verification-not-selected-services/service-provider")
	@Timed
	public ResponseEntity<List<ScopeVerificationNotSelectedService>> getAllScopeVerificationNotSelectedServices(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ScopeVerificationNotSelectedServices");
		Page<ScopeVerificationNotSelectedService> page = scopeVerificationNotSelectedServiceService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/scope-verification-not-selected-services/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /scope-verification-not-selected-services/service-provider/:id : get
	 * the "id" scopeVerificationNotSelectedService for service provider.
	 *
	 * @param id
	 *            the id of the scopeVerificationNotSelectedService to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         scopeVerificationNotSelectedService, or with status 404 (Not
	 *         Found)
	 */
	@GetMapping("/scope-verification-not-selected-services/service-provider/{id}")
	@Timed
	public ResponseEntity<ScopeVerificationNotSelectedService> getScopeVerificationNotSelectedService(
			@PathVariable Long id) {
		log.debug("REST request to get ScopeVerificationNotSelectedService : {}", id);
		ScopeVerificationNotSelectedService scopeVerificationNotSelectedService = scopeVerificationNotSelectedServiceService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(scopeVerificationNotSelectedService));
	}

	/**
	 * DELETE /scope-verification-not-selected-services/service-provider/:id :
	 * delete the "id" scopeVerificationNotSelectedService for service provider.
	 *
	 * @param id
	 *            the id of the scopeVerificationNotSelectedService to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/scope-verification-not-selected-services/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteScopeVerificationNotSelectedService(@PathVariable Long id) {
		log.debug("REST request to delete ScopeVerificationNotSelectedService : {}", id);
		scopeVerificationNotSelectedServiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
