package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ScopeVerificationSelectedService;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ScopeVerificationSelectedServiceService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ScopeVerificationSelectedService.
 */
@RestController
@RequestMapping("/api")
public class ScopeVerificationSelectedServiceResource {

	private final Logger log = LoggerFactory.getLogger(ScopeVerificationSelectedServiceResource.class);

	private static final String ENTITY_NAME = "scopeVerificationSelectedService";

	private final ScopeVerificationSelectedServiceService scopeVerificationSelectedServiceService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public ScopeVerificationSelectedServiceResource(
			ScopeVerificationSelectedServiceService scopeVerificationSelectedServiceService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.scopeVerificationSelectedServiceService = scopeVerificationSelectedServiceService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /scope-verification-selected-services/service-provider : Create a
	 * new scopeVerificationSelectedService for service provider.
	 *
	 * @param scopeVerificationSelectedService
	 *            the scopeVerificationSelectedService to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new scopeVerificationSelectedService, or with status 400 (Bad
	 *         Request) if the scopeVerificationSelectedService has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/scope-verification-selected-services/service-provider")
	@Timed
	public ResponseEntity<ScopeVerificationSelectedService> createScopeVerificationSelectedService(
			@RequestBody ScopeVerificationSelectedService scopeVerificationSelectedService) throws URISyntaxException {
		log.debug("REST request to save ScopeVerificationSelectedService : {}", scopeVerificationSelectedService);
		if (scopeVerificationSelectedService.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new scopeVerificationSelectedService cannot already have an ID")).body(null);
		}
		ScopeVerificationSelectedService result = scopeVerificationSelectedServiceService
				.save(scopeVerificationSelectedService);
		executiveSummaryStatusService.createOrUpdate("SELECTED_SERVICES");
		return ResponseEntity
				.created(new URI("/api/scope-verification-selected-services/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /scope-verification-selected-services/service-provider : Updates an
	 * existing scopeVerificationSelectedService for service provider.
	 *
	 * @param scopeVerificationSelectedService
	 *            the scopeVerificationSelectedService to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         scopeVerificationSelectedService, or with status 400 (Bad
	 *         Request) if the scopeVerificationSelectedService is not valid, or
	 *         with status 500 (Internal Server Error) if the
	 *         scopeVerificationSelectedService couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/scope-verification-selected-services/service-provider")
	@Timed
	public ResponseEntity<ScopeVerificationSelectedService> updateScopeVerificationSelectedService(
			@RequestBody ScopeVerificationSelectedService scopeVerificationSelectedService) throws URISyntaxException {
		log.debug("REST request to update ScopeVerificationSelectedService : {}", scopeVerificationSelectedService);
		if (scopeVerificationSelectedService.getId() == null) {
			return createScopeVerificationSelectedService(scopeVerificationSelectedService);
		}
		ScopeVerificationSelectedService result = scopeVerificationSelectedServiceService
				.save(scopeVerificationSelectedService);
		executiveSummaryStatusService.createOrUpdate("SELECTED_SERVICES");
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, scopeVerificationSelectedService.getId().toString()))
				.body(result);
	}

	/**
	 * GET /scope-verification-selected-services/service-provider : get all the
	 * scopeVerificationSelectedServices for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         scopeVerificationSelectedServices in body
	 */
	@GetMapping("/scope-verification-selected-services/service-provider")
	@Timed
	public ResponseEntity<List<ScopeVerificationSelectedService>> getAllScopeVerificationSelectedServices(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ScopeVerificationSelectedServices");
		Page<ScopeVerificationSelectedService> page = scopeVerificationSelectedServiceService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/scope-verification-selected-services/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /scope-verification-selected-services/service-provider/:id : get the
	 * "id" scopeVerificationSelectedService for service provider.
	 *
	 * @param id
	 *            the id of the scopeVerificationSelectedService to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         scopeVerificationSelectedService, or with status 404 (Not Found)
	 */
	@GetMapping("/scope-verification-selected-services/service-provider/{id}")
	@Timed
	public ResponseEntity<ScopeVerificationSelectedService> getScopeVerificationSelectedService(@PathVariable Long id) {
		log.debug("REST request to get ScopeVerificationSelectedService : {}", id);
		ScopeVerificationSelectedService scopeVerificationSelectedService = scopeVerificationSelectedServiceService
				.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(scopeVerificationSelectedService));
	}

	/**
	 * DELETE /scope-verification-selected-services/service-provider/:id :
	 * delete the "id" scopeVerificationSelectedService for service provider.
	 *
	 * @param id
	 *            the id of the scopeVerificationSelectedService to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/scope-verification-selected-services/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteScopeVerificationSelectedService(@PathVariable Long id) {
		log.debug("REST request to delete ScopeVerificationSelectedService : {}", id);
		scopeVerificationSelectedServiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
