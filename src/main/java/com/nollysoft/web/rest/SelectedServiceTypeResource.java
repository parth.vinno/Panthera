package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.SelectedServiceType;
import com.nollysoft.service.SelectedServiceTypeService;
import com.nollysoft.service.dto.SelectedServiceTypeDTO;
import com.nollysoft.service.dto.SelectedServiceTypePageDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing SelectedServiceType.
 */
@RestController
@RequestMapping("/api")
public class SelectedServiceTypeResource {

	private final Logger log = LoggerFactory.getLogger(SelectedServiceTypeResource.class);

	private static final String ENTITY_NAME = "selectedServiceType";

	private final SelectedServiceTypeService selectedServiceTypeService;

	public SelectedServiceTypeResource(SelectedServiceTypeService selectedServiceTypeService) {
		this.selectedServiceTypeService = selectedServiceTypeService;
	}

	/**
	 * POST /selected-service-types/service-provider : Create a new
	 * selectedServiceType for service provider.
	 *
	 * @param selectedServiceType
	 *            the selectedServiceType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new selectedServiceType, or with status 400 (Bad Request) if the
	 *         selectedServiceType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/selected-service-types/service-provider")
	@Timed
	public ResponseEntity<SelectedServiceType> createSelectedServiceType(
			@RequestBody SelectedServiceType selectedServiceType) throws URISyntaxException {
		log.debug("REST request to save SelectedServiceType : {}", selectedServiceType);
		if (selectedServiceType.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new selectedServiceType cannot already have an ID")).body(null);
		}
		SelectedServiceType result = selectedServiceTypeService.save(selectedServiceType);
		return ResponseEntity.created(new URI("/api/selected-service-types/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /selected-service-types/list/service-provider : Create a list of
	 * selectedServiceType for service provider.
	 *
	 * @param list
	 *            of selectedServiceType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of selectedServiceType
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/selected-service-types/list/service-provider")
	@Timed
	public ResponseEntity<List<SelectedServiceType>> createSelectedServiceTypes(
			@RequestBody List<SelectedServiceType> selectedServiceTypes) throws URISyntaxException {
		log.debug("REST request to save SelectedServiceType : {}", selectedServiceTypes);
		List<SelectedServiceType> result = selectedServiceTypeService.create(selectedServiceTypes);
		return ResponseEntity.created(new URI("/api/selected-service-types/list/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /selected-service-types/service-provider : Updates an existing
	 * selectedServiceType for service provider.
	 *
	 * @param selectedServiceType
	 *            the selectedServiceType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         selectedServiceType, or with status 400 (Bad Request) if the
	 *         selectedServiceType is not valid, or with status 500 (Internal
	 *         Server Error) if the selectedServiceType couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/selected-service-types/service-provider")
	@Timed
	public ResponseEntity<SelectedServiceType> updateSelectedServiceType(
			@RequestBody SelectedServiceType selectedServiceType) throws URISyntaxException {
		log.debug("REST request to update SelectedServiceType : {}", selectedServiceType);
		if (selectedServiceType.getId() == null) {
			return createSelectedServiceType(selectedServiceType);
		}
		SelectedServiceType result = selectedServiceTypeService.save(selectedServiceType);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, selectedServiceType.getId().toString()))
				.body(result);
	}

	/**
	 * GET /selected-service-types/service-provider : get all the
	 * selectedServiceTypes for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         selectedServiceTypes in body
	 */
	@GetMapping("/selected-service-types/service-provider")
	@Timed
	public ResponseEntity<List<SelectedServiceTypeDTO>> getAllSelectedServiceTypes(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of SelectedServiceTypes");
		SelectedServiceTypePageDTO page = selectedServiceTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/selected-service-types/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /selected-service-types/service-provider/:id : get the "id"
	 * selectedServiceType for service provider.
	 *
	 * @param id
	 *            the id of the selectedServiceType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         selectedServiceType, or with status 404 (Not Found)
	 */
	@GetMapping("/selected-service-types/service-provider/{id}")
	@Timed
	public ResponseEntity<SelectedServiceTypeDTO> getSelectedServiceType(@PathVariable Long id) {
		log.debug("REST request to get SelectedServiceType : {}", id);
		SelectedServiceType selectedServiceType = selectedServiceTypeService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional
				.ofNullable(selectedServiceType != null ? new SelectedServiceTypeDTO(selectedServiceType) : null));
	}

	/**
	 * DELETE /selected-service-types/service-provider/:id : delete the "id"
	 * selectedServiceType for service provider.
	 *
	 * @param id
	 *            the id of the selectedServiceType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/selected-service-types/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteSelectedServiceType(@PathVariable Long id) {
		log.debug("REST request to delete SelectedServiceType : {}", id);
		selectedServiceTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
