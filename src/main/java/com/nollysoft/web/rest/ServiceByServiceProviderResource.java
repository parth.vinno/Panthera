package com.nollysoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ServiceByServiceProvider;
import com.nollysoft.service.ServiceByServiceProviderService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceByServiceProvider.
 */
@RestController
@RequestMapping("/api")
public class ServiceByServiceProviderResource {

	private final Logger log = LoggerFactory.getLogger(ServiceByServiceProviderResource.class);

	private static final String ENTITY_NAME = "serviceByServiceProvider";

	private final ServiceByServiceProviderService serviceByServiceProviderService;

	public ServiceByServiceProviderResource(ServiceByServiceProviderService serviceByServiceProviderService) {
		this.serviceByServiceProviderService = serviceByServiceProviderService;
	}

	/**
	 * POST /service-by-service-providers/service-provider : Create a new
	 * serviceByServiceProvider for service provider.
	 *
	 * @param serviceByServiceProvider
	 *            the serviceByServiceProvider to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new serviceByServiceProvider, or with status 400 (Bad Request) if
	 *         the serviceByServiceProvider has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-by-service-providers/service-provider")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> createServiceByServiceProviderForServiceProvider(
			@Valid @RequestBody ServiceByServiceProvider serviceByServiceProvider) throws URISyntaxException {
		log.debug("REST request to save ServiceByServiceProvider : {}", serviceByServiceProvider);
		if (serviceByServiceProvider.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new serviceByServiceProvider cannot already have an ID")).body(null);
		}
		ServiceByServiceProvider result = serviceByServiceProviderService.save(serviceByServiceProvider);
		return ResponseEntity.created(new URI("/api/service-by-service-providers/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /service-by-service-providers/service-provider : Updates an existing
	 * serviceByServiceProvider for service provider.
	 *
	 * @param serviceByServiceProvider
	 *            the serviceByServiceProvider to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         serviceByServiceProvider, or with status 400 (Bad Request) if the
	 *         serviceByServiceProvider is not valid, or with status 500
	 *         (Internal Server Error) if the serviceByServiceProvider couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/service-by-service-providers/service-provider")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> updateServiceByServiceProviderForServiceProvider(
			@Valid @RequestBody ServiceByServiceProvider serviceByServiceProvider) throws URISyntaxException {
		log.debug("REST request to update ServiceByServiceProvider : {}", serviceByServiceProvider);
		if (serviceByServiceProvider.getId() == null) {
			return createServiceByServiceProviderForServiceProvider(serviceByServiceProvider);
		}
		ServiceByServiceProvider result = serviceByServiceProviderService.save(serviceByServiceProvider);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceByServiceProvider.getId().toString()))
				.body(result);
	}

	/**
	 * GET /service-by-service-providers/service-provider : get all the
	 * serviceByServiceProviders for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceByServiceProviders in body
	 */
	@GetMapping("/service-by-service-providers/service-provider")
	@Timed
	public ResponseEntity<List<ServiceByServiceProvider>> getAllServiceByServiceProvidersForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ServiceByServiceProviders");
		Page<ServiceByServiceProvider> page = serviceByServiceProviderService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/service-by-service-providers/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /service-by-service-providers/service-provider/:id : get the "id"
	 * serviceByServiceProvider for service provider.
	 *
	 * @param id
	 *            the id of the serviceByServiceProvider to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         serviceByServiceProvider, or with status 404 (Not Found)
	 */
	@GetMapping("/service-by-service-providers/service-provider/{id}")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> getServiceByServiceProviderForServiceProvider(
			@PathVariable Long id) {
		log.debug("REST request to get ServiceByServiceProvider : {}", id);
		ServiceByServiceProvider serviceByServiceProvider = serviceByServiceProviderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceByServiceProvider));
	}

	/**
	 * DELETE /service-by-service-providers/service-provider/:id : delete the
	 * "id" serviceByServiceProvider for service provider.
	 *
	 * @param id
	 *            the id of the serviceByServiceProvider to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/service-by-service-providers/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteServiceByServiceProviderForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete ServiceByServiceProvider : {}", id);
		serviceByServiceProviderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /service-by-service-providers : Create a new
	 * serviceByServiceProvider for merchant.
	 *
	 * @param serviceByServiceProvider
	 *            the serviceByServiceProvider to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new serviceByServiceProvider, or with status 400 (Bad Request) if
	 *         the serviceByServiceProvider has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-by-service-providers")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> createServiceByServiceProviderForMerchant(
			@Valid @RequestBody ServiceByServiceProvider serviceByServiceProvider) throws URISyntaxException {
		log.debug("REST request to save ServiceByServiceProvider : {}", serviceByServiceProvider);
		if (serviceByServiceProvider.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new serviceByServiceProvider cannot already have an ID")).body(null);
		}
		ServiceByServiceProvider result = serviceByServiceProviderService.save(serviceByServiceProvider);
		return ResponseEntity.created(new URI("/api/service-by-service-providers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /service-by-service-providers : Updates an existing
	 * serviceByServiceProvider for merchant.
	 *
	 * @param serviceByServiceProvider
	 *            the serviceByServiceProvider to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         serviceByServiceProvider, or with status 400 (Bad Request) if the
	 *         serviceByServiceProvider is not valid, or with status 500
	 *         (Internal Server Error) if the serviceByServiceProvider couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/service-by-service-providers")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> updateServiceByServiceProviderForMerchant(
			@Valid @RequestBody ServiceByServiceProvider serviceByServiceProvider) throws URISyntaxException {
		log.debug("REST request to update ServiceByServiceProvider : {}", serviceByServiceProvider);
		if (serviceByServiceProvider.getId() == null) {
			return createServiceByServiceProviderForMerchant(serviceByServiceProvider);
		}
		ServiceByServiceProvider result = serviceByServiceProviderService.save(serviceByServiceProvider);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceByServiceProvider.getId().toString()))
				.body(result);
	}

	/**
	 * GET /service-by-service-providers : get all the serviceByServiceProviders
	 * for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceByServiceProviders in body
	 */
	@GetMapping("/service-by-service-providers")
	@Timed
	public ResponseEntity<List<ServiceByServiceProvider>> getAllServiceByServiceProvidersForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ServiceByServiceProviders");
		Page<ServiceByServiceProvider> page = serviceByServiceProviderService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/service-by-service-providers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /service-by-service-providers/:id : get the "id"
	 * serviceByServiceProvider for merchant.
	 *
	 * @param id
	 *            the id of the serviceByServiceProvider to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         serviceByServiceProvider, or with status 404 (Not Found)
	 */
	@GetMapping("/service-by-service-providers/{id}")
	@Timed
	public ResponseEntity<ServiceByServiceProvider> getServiceByServiceProviderForMerchant(@PathVariable Long id) {
		log.debug("REST request to get ServiceByServiceProvider : {}", id);
		ServiceByServiceProvider serviceByServiceProvider = serviceByServiceProviderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceByServiceProvider));
	}

	/**
	 * DELETE /service-by-service-providers/:id : delete the "id"
	 * serviceByServiceProvider for merchant.
	 *
	 * @param id
	 *            the id of the serviceByServiceProvider to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/service-by-service-providers/{id}")
	@Timed
	public ResponseEntity<Void> deleteServiceByServiceProviderForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete ServiceByServiceProvider : {}", id);
		serviceByServiceProviderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
