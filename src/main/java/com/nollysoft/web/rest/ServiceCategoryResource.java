package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.DbContextHolder;
import com.nollysoft.config.DbType;
import com.nollysoft.domain.ServiceCategory;
import com.nollysoft.service.ServiceCategoryService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ServiceCategory.
 */
@RestController
@RequestMapping("/api")
public class ServiceCategoryResource {

	private final Logger log = LoggerFactory.getLogger(ServiceCategoryResource.class);

	private static final String ENTITY_NAME = "serviceCategory";

	private final ServiceCategoryService serviceCategoryService;

	public ServiceCategoryResource(ServiceCategoryService serviceCategoryService) {
		this.serviceCategoryService = serviceCategoryService;
	}

	/**
	 * POST /service-categories/service-provider : Create a new serviceCategory.
	 *
	 * @param serviceCategory
	 *            the serviceCategory to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new serviceCategory, or with status 400 (Bad Request) if the
	 *         serviceCategory has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-categories/service-provider")
	@Timed
	public ResponseEntity<ServiceCategory> createServiceCategory(@Valid @RequestBody ServiceCategory serviceCategory)
			throws URISyntaxException {
		log.debug("REST request to save ServiceCategory : {}", serviceCategory);
		if (serviceCategory.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new serviceCategory cannot already have an ID")).body(null);
		}
		ServiceCategory result = serviceCategoryService.save(serviceCategory);
		return ResponseEntity.created(new URI("/api/service-categories/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /service-categories/service-provider : Updates an existing
	 * serviceCategory.
	 *
	 * @param serviceCategory
	 *            the serviceCategory to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         serviceCategory, or with status 400 (Bad Request) if the
	 *         serviceCategory is not valid, or with status 500 (Internal Server
	 *         Error) if the serviceCategory couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/service-categories/service-provider")
	@Timed
	public ResponseEntity<ServiceCategory> updateServiceCategory(@Valid @RequestBody ServiceCategory serviceCategory)
			throws URISyntaxException {
		log.debug("REST request to update ServiceCategory : {}", serviceCategory);
		if (serviceCategory.getId() == null) {
			return createServiceCategory(serviceCategory);
		}
		ServiceCategory result = serviceCategoryService.save(serviceCategory);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceCategory.getId().toString()))
				.body(result);
	}

	/**
	 * GET /service-categories/service-provider : get all the serviceCategories.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceCategories in body
	 */
	@GetMapping("/service-categories/service-provider")
	@Timed
	public List<ServiceCategory> getAllServiceCategories() {
		log.debug("REST request to get all ServiceCategories");
		return serviceCategoryService.findAll();
	}
	
	/**
	 * GET /service-categories : get all the
	 * serviceCategories for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceCategories in body
	 */
	@GetMapping("/service-categories-pageable/service-provider")
	@Timed
	public ResponseEntity<List<ServiceCategory>> getAllServiceCategory(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Payment Channel");
		try {
			DbContextHolder.setDbType(DbType.dbmerchant);
			Page<ServiceCategory> page = serviceCategoryService.findAll(pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-channels");
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} finally {
			DbContextHolder.clearDbType();
		}
	}
	/**
	 * GET /service-categories/service-provider/:id : get the "id"
	 * serviceCategory.
	 *
	 * @param id
	 *            the id of the serviceCategory to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         serviceCategory, or with status 404 (Not Found)
	 */
	@GetMapping("/service-categories/service-provider/{id}")
	@Timed
	public ResponseEntity<ServiceCategory> getServiceCategory(@PathVariable Long id) {
		log.debug("REST request to get ServiceCategory : {}", id);
		ServiceCategory serviceCategory = serviceCategoryService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceCategory));
	}

	/**
	 * DELETE /service-categories/service-provider/:id : delete the "id"
	 * serviceCategory.
	 *
	 * @param id
	 *            the id of the serviceCategory to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/service-categories/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteServiceCategory(@PathVariable Long id) {
		log.debug("REST request to delete ServiceCategory : {}", id);
		serviceCategoryService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
