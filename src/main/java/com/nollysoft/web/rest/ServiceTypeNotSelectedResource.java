package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ServiceTypeNotSelected;
import com.nollysoft.service.ServiceTypeNotSelectedService;
import com.nollysoft.service.dto.ServiceTypeNotSelectedDTO;
import com.nollysoft.service.dto.ServiceTypeNotSelectedPageDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ServiceTypeNotSelected.
 */
@RestController
@RequestMapping("/api")
public class ServiceTypeNotSelectedResource {

	private final Logger log = LoggerFactory.getLogger(ServiceTypeNotSelectedResource.class);

	private static final String ENTITY_NAME = "serviceTypeNotSelected";

	private final ServiceTypeNotSelectedService serviceTypeNotSelectedService;

	public ServiceTypeNotSelectedResource(ServiceTypeNotSelectedService serviceTypeNotSelectedService) {
		this.serviceTypeNotSelectedService = serviceTypeNotSelectedService;
	}

	/**
	 * POST /service-type-not-selected/service-provider : Create a new
	 * serviceTypeNotSelected for service provider.
	 *
	 * @param serviceTypeNotSelected
	 *            the serviceTypeNotSelected to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new serviceTypeNotSelected, or with status 400 (Bad Request) if
	 *         the serviceTypeNotSelected has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-type-not-selected/service-provider")
	@Timed
	public ResponseEntity<ServiceTypeNotSelected> createServiceTypeNotSelected(
			@RequestBody ServiceTypeNotSelected serviceTypeNotSelected) throws URISyntaxException {
		log.debug("REST request to save ServiceTypeNotSelected : {}", serviceTypeNotSelected);
		if (serviceTypeNotSelected.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new serviceTypeNotSelected cannot already have an ID")).body(null);
		}
		ServiceTypeNotSelected result = serviceTypeNotSelectedService.save(serviceTypeNotSelected);
		return ResponseEntity.created(new URI("/api/service-type-not-selected/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /service-type-not-selected/list/service-provider : Create a list
	 * serviceTypeNotSelected for service provider.
	 *
	 * @param list
	 *            of serviceTypeNotSelected to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         list of serviceTypeNotSelected
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-type-not-selected/list/service-provider")
	@Timed
	public ResponseEntity<List<ServiceTypeNotSelected>> createServiceTypesNotSelected(
			@RequestBody List<ServiceTypeNotSelected> serviceTypesNotSelected) throws URISyntaxException {
		log.debug("REST request to save ServiceTypeNotSelected : {}", serviceTypesNotSelected);
		List<ServiceTypeNotSelected> result = serviceTypeNotSelectedService.create(serviceTypesNotSelected);
		return ResponseEntity.created(new URI("/api/service-type-not-selected/list/service-provider/"))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
	}

	/**
	 * PUT /service-type-not-selected/service-provider : Updates an existing
	 * serviceTypeNotSelected for service provider.
	 *
	 * @param serviceTypeNotSelected
	 *            the serviceTypeNotSelected to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         serviceTypeNotSelected, or with status 400 (Bad Request) if the
	 *         serviceTypeNotSelected is not valid, or with status 500 (Internal
	 *         Server Error) if the serviceTypeNotSelected couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/service-type-not-selected/service-provider")
	@Timed
	public ResponseEntity<ServiceTypeNotSelected> updateServiceTypeNotSelected(
			@RequestBody ServiceTypeNotSelected serviceTypeNotSelected) throws URISyntaxException {
		log.debug("REST request to update ServiceTypeNotSelected : {}", serviceTypeNotSelected);
		if (serviceTypeNotSelected.getId() == null) {
			return createServiceTypeNotSelected(serviceTypeNotSelected);
		}
		ServiceTypeNotSelected result = serviceTypeNotSelectedService.save(serviceTypeNotSelected);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceTypeNotSelected.getId().toString()))
				.body(result);
	}

	/**
	 * GET /service-type-not-selected/service-provider : get all the
	 * serviceTypeNotSelecteds for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceTypeNotSelecteds in body
	 */
	@GetMapping("/service-type-not-selected/service-provider")
	@Timed
	public ResponseEntity<List<ServiceTypeNotSelectedDTO>> getAllServiceTypeNotSelecteds(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ServiceTypeNotSelecteds");
		ServiceTypeNotSelectedPageDTO page = serviceTypeNotSelectedService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/service-type-not-selected/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /service-type-not-selected/service-provider/:id : get the "id"
	 * serviceTypeNotSelected for service provider.
	 *
	 * @param id
	 *            the id of the serviceTypeNotSelected to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         serviceTypeNotSelected, or with status 404 (Not Found)
	 */
	@GetMapping("/service-type-not-selected/service-provider/{id}")
	@Timed
	public ResponseEntity<ServiceTypeNotSelectedDTO> getServiceTypeNotSelected(@PathVariable Long id) {
		log.debug("REST request to get ServiceTypeNotSelected : {}", id);
		ServiceTypeNotSelected serviceTypeNotSelected = serviceTypeNotSelectedService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(
				serviceTypeNotSelected != null ? new ServiceTypeNotSelectedDTO(serviceTypeNotSelected) : null));
	}

	/**
	 * DELETE /service-type-not-selected/service-provider/:id : delete the "id"
	 * serviceTypeNotSelected for service provider.
	 *
	 * @param id
	 *            the id of the serviceTypeNotSelected to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/service-type-not-selected/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteServiceTypeNotSelected(@PathVariable Long id) {
		log.debug("REST request to delete ServiceTypeNotSelected : {}", id);
		serviceTypeNotSelectedService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
