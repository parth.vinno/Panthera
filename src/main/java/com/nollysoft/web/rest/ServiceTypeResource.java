package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ServiceType;
import com.nollysoft.service.ServiceTypeService;
import com.nollysoft.service.dto.ServiceTypeDTO;
import com.nollysoft.service.dto.ServiceTypePageDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ServiceType.
 */
@RestController
@RequestMapping("/api")
public class ServiceTypeResource {

	private final Logger log = LoggerFactory.getLogger(ServiceTypeResource.class);

	private static final String ENTITY_NAME = "serviceType";

	private final ServiceTypeService serviceTypeService;

	public ServiceTypeResource(ServiceTypeService serviceTypeService) {
		this.serviceTypeService = serviceTypeService;
	}

	/**
	 * POST /service-types/service-provider : Create a new serviceType for
	 * service provider.
	 *
	 * @param serviceType
	 *            the serviceType to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new serviceType, or with status 400 (Bad Request) if the
	 *         serviceType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/service-types/service-provider")
	@Timed
	public ResponseEntity<ServiceType> createServiceType(@RequestBody ServiceType serviceType)
			throws URISyntaxException {
		log.debug("REST request to save ServiceType : {}", serviceType);
		if (serviceType.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new serviceType cannot already have an ID")).body(null);
		}
		ServiceType result = serviceTypeService.save(serviceType);
		return ResponseEntity.created(new URI("/api/service-types/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /service-types/service-provider : Updates an existing serviceType for
	 * service provider.
	 *
	 * @param serviceType
	 *            the serviceType to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         serviceType, or with status 400 (Bad Request) if the serviceType
	 *         is not valid, or with status 500 (Internal Server Error) if the
	 *         serviceType couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/service-types/service-provider")
	@Timed
	public ResponseEntity<ServiceType> updateServiceType(@RequestBody ServiceType serviceType)
			throws URISyntaxException {
		log.debug("REST request to update ServiceType : {}", serviceType);
		if (serviceType.getId() == null) {
			return createServiceType(serviceType);
		}
		ServiceType result = serviceTypeService.save(serviceType);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceType.getId().toString())).body(result);
	}

	/**
	 * GET /service-types/service-provider : get all the serviceTypes for
	 * service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         serviceTypes in body
	 */
	@GetMapping("/service-types/service-provider")
	@Timed
	public ResponseEntity<List<ServiceTypeDTO>> getAllServiceTypes(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ServiceTypes");
		ServiceTypePageDTO page = serviceTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/service-types/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /service-types/service-provider/:id : get the "id" serviceType for
	 * service provider.
	 *
	 * @param id
	 *            the id of the serviceType to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         serviceType, or with status 404 (Not Found)
	 */
	@GetMapping("/service-types/service-provider/{id}")
	@Timed
	public ResponseEntity<ServiceTypeDTO> getServiceType(@PathVariable Long id) {
		log.debug("REST request to get ServiceType : {}", id);
		ServiceType serviceType = serviceTypeService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceType != null ? new ServiceTypeDTO(serviceType) : null));
	}

	/**
	 * DELETE /service-types/service-provider/:id : delete the "id" serviceType
	 * for service provider.
	 *
	 * @param id
	 *            the id of the serviceType to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/service-types/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteServiceType(@PathVariable Long id) {
		log.debug("REST request to delete ServiceType : {}", id);
		serviceTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
