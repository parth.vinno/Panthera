package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.State;
import com.nollysoft.service.StateService;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing State.
 */
@RestController
@RequestMapping("/api")
public class StateResource {

    private final Logger log = LoggerFactory.getLogger(StateResource.class);

    private static final String ENTITY_NAME = "state";

    private final StateService stateService;

    public StateResource(StateService stateService) {
        this.stateService = stateService;
    }

    /**
     * POST  /states : Create a new state.
     *
     * @param state the state to create
     * @return the ResponseEntity with status 201 (Created) and with body the new state, or with status 400 (Bad Request) if the state has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/states")
    @Timed
    public ResponseEntity<State> createState(@RequestBody State state) throws URISyntaxException {
        log.debug("REST request to save State : {}", state);
        State result = stateService.save(state);
        return ResponseEntity.created(new URI("/api/states/" + result.getLongName()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getShortName()))
            .body(result);
    }

    /**
     * PUT  /states : Updates an existing state.
     *
     * @param state the state to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated state,
     * or with status 400 (Bad Request) if the state is not valid,
     * or with status 500 (Internal Server Error) if the state couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/states")
    @Timed
    public ResponseEntity<State> updateState(@RequestBody State state) throws URISyntaxException {
        log.debug("REST request to update State : {}", state);
        State result = stateService.save(state);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, state.getShortName()))
            .body(result);
    }

    /**
     * GET  /states : get all the states.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of states in body
     */
    @GetMapping("/states")
    @Timed
    public ResponseEntity<List<State>> getAllStates() {
        log.debug("REST request to get a page of States");
        List<State> page = stateService.findAll();
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

	@GetMapping("/states/country/{countryCode}")
	@Timed
	public ResponseEntity<List<State>> getAllStates(@PathVariable("countryCode") String countryCode) {
		log.debug("REST request to get a page of States");
		List<State> page = stateService.findAllByCountryCode(countryCode);
		return new ResponseEntity<>(page, HttpStatus.OK);
	}

    /**
     * GET  /states/:shortName : get the "shortName" state.
     *
     * @param shortName the id of the state to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the state, or with status 404 (Not Found)
     */
    @GetMapping("/states/{shortName}")
    @Timed
    public ResponseEntity<State> getState(@PathVariable("shortName") String shortName) {
        log.debug("REST request to get State : {}", shortName);
        State state = stateService.findOne(shortName);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(state));
    }

    /**
     * DELETE  /states/:shortName : delete the "shortName" state.
     *
     * @param shortName the id of the state to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/states/{id}")
    @Timed
    public ResponseEntity<Void> deleteState(@PathVariable("shortName") String shortName) {
        log.debug("REST request to delete State : {}", shortName);
        stateService.delete(shortName);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, shortName)).build();
    }
}
