package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.Tenant;
import com.nollysoft.repository.TenantRepository;
import com.nollysoft.service.TenancyService;
import com.nollysoft.service.TenantService;
import com.nollysoft.service.UserService;
import com.nollysoft.service.dto.TenantUpdateDTO;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Tenant.
 */
@RestController
@RequestMapping("/api")
public class TenantResource {

	private final Logger log = LoggerFactory.getLogger(TenantResource.class);

	private static final String ENTITY_NAME = "tenant";

	private final TenantService tenantService;

	private final TenantRepository tenantRepository;

	private final UserService userService;

	private final TenancyService tenancyService;

	public TenantResource(TenantService tenantService, TenantRepository tenantRepository, UserService userService,
			TenancyService tenancyService) {
		this.tenantService = tenantService;
		this.tenantRepository = tenantRepository;
		this.tenancyService = tenancyService;
		this.userService = userService;
	}

	/**
	 * POST /tenants : Create a new tenant.
	 *
	 * @param tenant
	 *            the tenant to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         tenant, or with status 400 (Bad Request) if the tenant has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping("/tenants")
	@Timed
	public ResponseEntity<?> createTenant(@RequestBody Tenant tenant) throws URISyntaxException {
		log.debug("REST request to save Tenant : {}", tenant);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
		Tenant existingTenant = tenantRepository.findOneByCompanyAbbreviation(tenant.getCompanyAbbreviation());
		if (existingTenant != null) {
			return new ResponseEntity<>("error.abbreviation", textPlainHeaders, HttpStatus.BAD_REQUEST);
		} else if (tenant.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tenant cannot already have an ID"))
					.body(null);
		} else if (!tenant.getCompanyUrl()
				.contains(tenant.getContactPersonEmail().substring(tenant.getContactPersonEmail().indexOf("@") + 1))) {
			return new ResponseEntity<>("error.invalidCompanyDomain", textPlainHeaders,
					HttpStatus.BAD_REQUEST);
		}
		Tenant result = tenantService.save(tenant);
		try {
			tenancyService.createTenantDB(tenant.getTenantId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			tenancyService.createTenantInTenantDB(tenant);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.created(new URI("/api/tenants/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /tenants : Updates an existing tenant.
	 *
	 * @param tenantDTO
	 *            the tenant to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         tenant, or with status 400 (Bad Request) if the tenant is not valid,
	 *         or with status 500 (Internal Server Error) if the tenant couldn't be
	 *         updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/tenants")
	@Timed
	public ResponseEntity<?> updateTenant(@RequestBody TenantUpdateDTO tenantDTO) throws URISyntaxException {
		log.debug("REST request to update Tenant : {}", tenantDTO);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
		Tenant curTenant = tenantRepository.findOne(tenantDTO.getId());

		@SuppressWarnings("rawtypes")
		Condition skipIds = new Condition() {
		    public boolean applies(MappingContext context) {
		        return !context.getMapping().getLastDestinationProperty().getName().equals("tenantId");
		    }
		};
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setPropertyCondition(skipIds);
		modelMapper.map(tenantDTO, curTenant);
		if (curTenant.isDeleted()) {
			return ResponseUtil.wrapOrNotFound(Optional.ofNullable(null));
		}
		Tenant result = tenantService.save(curTenant);

		try {
			tenancyService.updateTenantInTenantDB(result, curTenant.getTenantId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, curTenant.getId().toString())).body(result);
	}

	/**
	 * GET /tenants : get all the tenants.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of tenants in
	 *         body
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/tenants")
	@Timed
	public ResponseEntity<List<Tenant>> getAllTenants(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Tenants");
		Page<Tenant> page = tenantService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tenants");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /tenants/:id : get the "id" tenant.
	 *
	 * @param id
	 *            the id of the tenant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the tenant, or
	 *         with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/tenants/{id}")
	@Timed
	public ResponseEntity<Tenant> getTenant(@PathVariable Long id) {
		log.debug("REST request to get Tenant : {}", id);
		Tenant tenant = tenantService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tenant));
	}

	/**
	 * GET /tenants/domain/:tenantId : get the "id" tenant.
	 *
	 * @param id
	 *            the id of the tenant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the tenant, or
	 *         with status 404 (Not Found)
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/tenants/domain/{tenantId}")
	@Timed
	public ResponseEntity<String> getDomain(@PathVariable String tenantId) {
		log.debug("REST request to get Tenant domain : {}", tenantId);
		String domain = tenantService.getDomain(tenantId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(domain));
	}

	/**
	 * DELETE /tenants/:id : delete the "id" tenant.
	 *
	 * @param id
	 *            the id of the tenant to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/tenants/{id}")
	@Timed
	@Transactional
	public ResponseEntity<Void> deleteTenant(@PathVariable Long id) {
		log.debug("REST request to delete Tenant : {}", id);
		String tenantId = tenantRepository.findOne(id).getTenantId();
		tenantService.delete(id);
		userService.deleteByTenantId(tenantId);
		try {
			tenancyService.deleteTenantInTenantDB(tenantId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
