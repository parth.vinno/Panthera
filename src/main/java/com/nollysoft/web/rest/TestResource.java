package com.nollysoft.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.service.ResourceService;

/**
 * REST controller for managing Resource.
 */
@RestController
@RequestMapping("/api")
public class TestResource {

    private final Logger log = LoggerFactory.getLogger(TestResource.class);

    private static final String ENTITY_NAME = "resource";

    private final ResourceService resourceService;

    public TestResource(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    
    /**
     * GET  /resources : get all the resources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of resources in body
     */
    @GetMapping("/one")
    @Timed
    public ResponseEntity<String> getAllResources() {
        log.debug("REST request to get a page of Resources");
    
        return new ResponseEntity<>("test", HttpStatus.OK);
    }

    /**
     * GET  /resources : get all the resources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of resources in body
     */
    @GetMapping("/two")
    @Timed
    public ResponseEntity<String> two() {
        log.debug("REST request to get a page of Resources");
    
        return new ResponseEntity<>("test", HttpStatus.OK);
    }

    
}
