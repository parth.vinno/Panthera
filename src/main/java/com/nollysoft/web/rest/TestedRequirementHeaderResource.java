package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.TestedRequirementHeader;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.TestedRequirementHeaderService;
import com.nollysoft.service.dto.TestedRequirementHeaderDTO;
import com.nollysoft.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing TestedRequirementHeader.
 */
@RestController
@RequestMapping("/api")
public class TestedRequirementHeaderResource {

	private final Logger log = LoggerFactory.getLogger(TestedRequirementHeaderResource.class);

	private static final String ENTITY_NAME = "testedRequirementHeader";

	private final TestedRequirementHeaderService testedRequirementHeaderService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public TestedRequirementHeaderResource(TestedRequirementHeaderService testedRequirementHeaderService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.testedRequirementHeaderService = testedRequirementHeaderService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /tested-requirement-header/service-provider : Create a new
	 * testedRequirementHeader.
	 *
	 * @param testedRequirementHeader
	 *            the testedRequirementHeader to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new testedRequirementHeader, or with status 400 (Bad Request) if
	 *         the testedRequirementHeader has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/tested-requirement-header/service-provider")
	@Timed
	public ResponseEntity<TestedRequirementHeaderDTO> createTestedRequirementHeader(
			@Valid @RequestBody TestedRequirementHeader testedRequirementHeader) throws URISyntaxException {
		log.debug("REST request to save TestedRequirementHeader : {}", testedRequirementHeader);
		if (testedRequirementHeader.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new TestedRequirementHeader cannot already have an ID")).body(null);
		}
		TestedRequirementHeader result = testedRequirementHeaderService.save(testedRequirementHeader);
		executiveSummaryStatusService.createOrUpdate("TESTED_REQUIREMENTS");
		return ResponseEntity.created(new URI("/api/tested-requirement-header/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(new TestedRequirementHeaderDTO(result));
	}

	/**
	 * PUT /tested-requirement-header/service-provider : Updates an existing
	 * testedRequirementHeader.
	 *
	 * @param testedRequirementHeader
	 *            the testedRequirementHeader to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         testedRequirementHeader, or with status 400 (Bad Request) if the
	 *         testedRequirementHeader is not valid, or with status 500
	 *         (Internal Server Error) if the testedRequirementHeader couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/tested-requirement-header/service-provider")
	@Timed
	public ResponseEntity<TestedRequirementHeaderDTO> updateTestedRequirementHeader(
			@Valid @RequestBody TestedRequirementHeader testedRequirementHeader) throws URISyntaxException {
		log.debug("REST request to update TestedRequirementHeader : {}", testedRequirementHeader);
		if (testedRequirementHeader.getId() == null) {
			return createTestedRequirementHeader(testedRequirementHeader);
		}
		TestedRequirementHeader result = testedRequirementHeaderService.save(testedRequirementHeader);
		executiveSummaryStatusService.createOrUpdate("TESTED_REQUIREMENTS");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, testedRequirementHeader.getId().toString()))
				.body(new TestedRequirementHeaderDTO(result));
	}

	/**
	 * GET /tested-requirement-header/service-provider : get all the
	 * testedRequirementHeaders.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         testedRequirementHeaders in body
	 */
	@GetMapping("/tested-requirement-header/service-provider")
	@Timed
	public List<TestedRequirementHeaderDTO> getAllTestedRequirementHeaders() {
		log.debug("REST request to get all TestedRequirementHeaders");
		return testedRequirementHeaderService.findAll();
	}

	/**
	 * GET /tested-requirement-header/service-provider/:id : get the "id"
	 * TestedRequirementHeader.
	 *
	 * @param id
	 *            the id of the TestedRequirementHeader to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         testedRequirementHeader, or with status 404 (Not Found)
	 */
	@GetMapping("/tested-requirement-header/service-provider/{id}")
	@Timed
	public ResponseEntity<TestedRequirementHeaderDTO> getTestedRequirementHeader(@PathVariable Long id) {
		log.debug("REST request to get testedRequirementHeader : {}", id);
		TestedRequirementHeader testedRequirementHeader = testedRequirementHeaderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(
				testedRequirementHeader != null ? new TestedRequirementHeaderDTO(testedRequirementHeader) : null));
	}

	/**
	 * DELETE /tested-requirement-header/service-provider/:id : delete the "id"
	 * TestedRequirementHeader.
	 *
	 * @param id
	 *            the id of the TestedRequirementHeader to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/tested-requirement-header/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteTestedRequirementHeader(@PathVariable Long id) {
		log.debug("REST request to delete testedRequirementHeader : {}", id);
		testedRequirementHeaderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
