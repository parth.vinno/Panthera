package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.ThirdPartyServiceProvider;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ThirdPartyServiceProviderService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ThirdPartyServiceProvider.
 */
@RestController
@RequestMapping("/api")
public class ThirdPartyServiceProviderResource {

	private final Logger log = LoggerFactory.getLogger(ThirdPartyServiceProviderResource.class);

	private static final String ENTITY_NAME = "thirdPartyServiceProvider";

	private final ThirdPartyServiceProviderService thirdPartyServiceProviderService;

	private final ExecutiveSummaryStatusService executiveSummaryStatusService;

	public ThirdPartyServiceProviderResource(ThirdPartyServiceProviderService thirdPartyServiceProviderService,
			ExecutiveSummaryStatusService executiveSummaryStatusService) {
		this.thirdPartyServiceProviderService = thirdPartyServiceProviderService;
		this.executiveSummaryStatusService = executiveSummaryStatusService;
	}

	/**
	 * POST /third-party-service-providers/service-provider : Create a new
	 * thirdPartyServiceProvider for service provider.
	 *
	 * @param thirdPartyServiceProvider
	 *            the thirdPartyServiceProvider to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new thirdPartyServiceProvider, or with status 400 (Bad Request)
	 *         if the thirdPartyServiceProvider has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/third-party-service-providers/service-provider")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> createThirdPartyServiceProviderForServiceProvider(
			@Valid @RequestBody ThirdPartyServiceProvider thirdPartyServiceProvider) throws URISyntaxException {
		log.debug("REST request to save ThirdPartyServiceProvider : {}", thirdPartyServiceProvider);
		if (thirdPartyServiceProvider.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new thirdPartyServiceProvider cannot already have an ID")).body(null);
		}
		ThirdPartyServiceProvider result = thirdPartyServiceProviderService.save(thirdPartyServiceProvider);
		executiveSummaryStatusService.createOrUpdate("SERVICE_PROVIDERS");
		return ResponseEntity.created(new URI("/api/third-party-service-providers/service-provider/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /third-party-service-providers/service-provider : Updates an existing
	 * thirdPartyServiceProvider for service provider.
	 *
	 * @param thirdPartyServiceProvider
	 *            the thirdPartyServiceProvider to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         thirdPartyServiceProvider, or with status 400 (Bad Request) if
	 *         the thirdPartyServiceProvider is not valid, or with status 500
	 *         (Internal Server Error) if the thirdPartyServiceProvider couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/third-party-service-providers/service-provider")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> updateThirdPartyServiceProviderForServiceProvider(
			@Valid @RequestBody ThirdPartyServiceProvider thirdPartyServiceProvider) throws URISyntaxException {
		log.debug("REST request to update ThirdPartyServiceProvider : {}", thirdPartyServiceProvider);
		if (thirdPartyServiceProvider.getId() == null) {
			return createThirdPartyServiceProviderForServiceProvider(thirdPartyServiceProvider);
		}
		ThirdPartyServiceProvider result = thirdPartyServiceProviderService.save(thirdPartyServiceProvider);
		executiveSummaryStatusService.createOrUpdate("SERVICE_PROVIDERS");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, thirdPartyServiceProvider.getId().toString()))
				.body(result);
	}

	/**
	 * GET /third-party-service-providers/service-provider : get all the
	 * thirdPartyServiceProviders for service provider.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         thirdPartyServiceProviders in body
	 */
	@GetMapping("/third-party-service-providers/service-provider")
	@Timed
	public ResponseEntity<List<ThirdPartyServiceProvider>> getAllThirdPartyServiceProvidersForServiceProvider(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ThirdPartyServiceProviders");
		Page<ThirdPartyServiceProvider> page = thirdPartyServiceProviderService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/third-party-service-providers/service-provider");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /third-party-service-providers/service-provider/:id : get the "id"
	 * thirdPartyServiceProvider for service provider.
	 *
	 * @param id
	 *            the id of the thirdPartyServiceProvider to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         thirdPartyServiceProvider, or with status 404 (Not Found)
	 */
	@GetMapping("/third-party-service-providers/service-provider/{id}")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> getThirdPartyServiceProviderForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to get ThirdPartyServiceProvider : {}", id);
		ThirdPartyServiceProvider thirdPartyServiceProvider = thirdPartyServiceProviderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(thirdPartyServiceProvider));
	}

	/**
	 * DELETE /third-party-service-providers/service-provider/:id : delete the "id"
	 * thirdPartyServiceProvider for service provider.
	 *
	 * @param id
	 *            the id of the thirdPartyServiceProvider to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/third-party-service-providers/service-provider/{id}")
	@Timed
	public ResponseEntity<Void> deleteThirdPartyServiceProviderForServiceProvider(@PathVariable Long id) {
		log.debug("REST request to delete ThirdPartyServiceProvider : {}", id);
		thirdPartyServiceProviderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /third-party-service-providers : Create a new
	 * thirdPartyServiceProvider for merchant.
	 *
	 * @param thirdPartyServiceProvider
	 *            the thirdPartyServiceProvider to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new thirdPartyServiceProvider, or with status 400 (Bad Request)
	 *         if the thirdPartyServiceProvider has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/third-party-service-providers")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> createThirdPartyServiceProviderForMerchant(
			@Valid @RequestBody ThirdPartyServiceProvider thirdPartyServiceProvider) throws URISyntaxException {
		log.debug("REST request to save ThirdPartyServiceProvider : {}", thirdPartyServiceProvider);
		if (thirdPartyServiceProvider.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new thirdPartyServiceProvider cannot already have an ID")).body(null);
		}
		ThirdPartyServiceProvider result = thirdPartyServiceProviderService.save(thirdPartyServiceProvider);
		executiveSummaryStatusService.createOrUpdate("SERVICE_PROVIDERS");
		return ResponseEntity.created(new URI("/api/third-party-service-providers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /third-party-service-providers : Updates an existing
	 * thirdPartyServiceProvider for merchant.
	 *
	 * @param thirdPartyServiceProvider
	 *            the thirdPartyServiceProvider to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         thirdPartyServiceProvider, or with status 400 (Bad Request) if
	 *         the thirdPartyServiceProvider is not valid, or with status 500
	 *         (Internal Server Error) if the thirdPartyServiceProvider couldn't
	 *         be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/third-party-service-providers")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> updateThirdPartyServiceProviderForMerchant(
			@Valid @RequestBody ThirdPartyServiceProvider thirdPartyServiceProvider) throws URISyntaxException {
		log.debug("REST request to update ThirdPartyServiceProvider : {}", thirdPartyServiceProvider);
		if (thirdPartyServiceProvider.getId() == null) {
			return createThirdPartyServiceProviderForMerchant(thirdPartyServiceProvider);
		}
		ThirdPartyServiceProvider result = thirdPartyServiceProviderService.save(thirdPartyServiceProvider);
		executiveSummaryStatusService.createOrUpdate("SERVICE_PROVIDERS");
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, thirdPartyServiceProvider.getId().toString()))
				.body(result);
	}

	/**
	 * GET /third-party-service-providers : get all the
	 * thirdPartyServiceProviders for merchant.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         thirdPartyServiceProviders in body
	 */
	@GetMapping("/third-party-service-providers")
	@Timed
	public ResponseEntity<List<ThirdPartyServiceProvider>> getAllThirdPartyServiceProvidersForMerchant(
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of ThirdPartyServiceProviders");
		Page<ThirdPartyServiceProvider> page = thirdPartyServiceProviderService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/third-party-service-providers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /third-party-service-providers/:id : get the "id"
	 * thirdPartyServiceProvider for merchant.
	 *
	 * @param id
	 *            the id of the thirdPartyServiceProvider to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         thirdPartyServiceProvider, or with status 404 (Not Found)
	 */
	@GetMapping("/third-party-service-providers/{id}")
	@Timed
	public ResponseEntity<ThirdPartyServiceProvider> getThirdPartyServiceProviderForMerchant(@PathVariable Long id) {
		log.debug("REST request to get ThirdPartyServiceProvider : {}", id);
		ThirdPartyServiceProvider thirdPartyServiceProvider = thirdPartyServiceProviderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(thirdPartyServiceProvider));
	}

	/**
	 * DELETE /third-party-service-providers/:id : delete the "id"
	 * thirdPartyServiceProvider for merchant.
	 *
	 * @param id
	 *            the id of the thirdPartyServiceProvider to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/third-party-service-providers/{id}")
	@Timed
	public ResponseEntity<Void> deleteThirdPartyServiceProviderForMerchant(@PathVariable Long id) {
		log.debug("REST request to delete ThirdPartyServiceProvider : {}", id);
		thirdPartyServiceProviderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
