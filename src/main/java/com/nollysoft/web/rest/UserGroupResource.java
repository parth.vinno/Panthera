package com.nollysoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.domain.UserGroup;
import com.nollysoft.service.UserGroupService;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing UserGroup.
 */
@RestController
@RequestMapping("/api")
public class UserGroupResource {

	private final Logger log = LoggerFactory.getLogger(UserGroupResource.class);

	private static final String ENTITY_NAME = "userGroup";

	private final UserGroupService userGroupService;

	public UserGroupResource(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	/**
	 * POST /user-groups : Create a new userGroup.
	 *
	 * @param userGroup
	 *            the userGroup to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         userGroup, or with status 400 (Bad Request) if the userGroup has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/user-groups")
	@Timed
	public ResponseEntity<UserGroup> createUserGroup(@Valid @RequestBody UserGroup userGroup)
			throws URISyntaxException {
		log.debug("REST request to save UserGroup : {}", userGroup);
		if (userGroup.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new userGroup cannot already have an ID"))
					.body(null);
		}
		UserGroup result = userGroupService.save(userGroup);
		return ResponseEntity.created(new URI("/api/user-groups/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /user-groups : Updates an existing userGroup.
	 *
	 * @param userGroup
	 *            the userGroup to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         userGroup, or with status 400 (Bad Request) if the userGroup is not
	 *         valid, or with status 500 (Internal Server Error) if the userGroup
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/user-groups")
	@Timed
	public ResponseEntity<UserGroup> updateUserGroup(@Valid @RequestBody UserGroup userGroup)
			throws URISyntaxException {
		log.debug("REST request to update UserGroup : {}", userGroup);
		if (userGroup.getId() == null) {
			return createUserGroup(userGroup);
		}
		UserGroup result = userGroupService.save(userGroup);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userGroup.getId().toString())).body(result);
	}

	/**
	 * GET /user-groups : get all the userGroups.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of userGroups in
	 *         body
	 */
	@GetMapping("/user-groups")
	@Timed
	public ResponseEntity<List<UserGroup>> getAllUserGroups(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of UserGroups");
		Page<UserGroup> page = userGroupService.findAllExceptAdmin(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-groups");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /user-groups/:id : get the "id" userGroup.
	 *
	 * @param id
	 *            the id of the userGroup to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the userGroup,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/user-groups/{id}")
	@Timed
	public ResponseEntity<UserGroup> getUserGroup(@PathVariable Long id) {
		log.debug("REST request to get UserGroup : {}", id);
		UserGroup userGroup = userGroupService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userGroup));
	}

	/**
	 * DELETE /user-groups/:id : delete the "id" userGroup.
	 *
	 * @param id
	 *            the id of the userGroup to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/user-groups/{id}")
	@Timed
	public ResponseEntity<Void> deleteUserGroup(@PathVariable Long id) {
		log.debug("REST request to delete UserGroup : {}", id);
		userGroupService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
