package com.nollysoft.web.rest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.nollysoft.config.Constants;
import com.nollysoft.constant.UserConstant;
import com.nollysoft.domain.User;
import com.nollysoft.domain.UserGroup;
import com.nollysoft.repository.UserGroupRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.service.MailService;
import com.nollysoft.service.TenancyService;
import com.nollysoft.service.UserService;
import com.nollysoft.service.dto.AdminCreationDTO;
import com.nollysoft.service.dto.AdminUpdateDTO;
import com.nollysoft.service.dto.TenantAdminCreationDTO;
import com.nollysoft.service.dto.UserCreationDTO;
import com.nollysoft.service.dto.UserDTO;
import com.nollysoft.service.dto.UserUpdateDTO;
import com.nollysoft.service.util.RandomUtil;
import com.nollysoft.web.rest.util.HeaderUtil;
import com.nollysoft.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Tenant Users.
 */
@RestController
@RequestMapping("/api")
public class UserManagementResource {

	private final Logger log = LoggerFactory.getLogger(UserManagementResource.class);

	private final UserRepository userRepository;

	private final UserService userService;

	private final MailService mailService;

	private final TenancyService tenancyService;

	@Autowired
	private UserGroupRepository userGroupRepository;

	public UserManagementResource(UserRepository userRepository, UserService userService, MailService mailService,
			TenancyService tenancyService) {
		this.userRepository = userRepository;
		this.userService = userService;
		this.mailService = mailService;
		this.tenancyService = tenancyService;
	}

	/**
	 * POST /admin/admin/register : register the admin.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already
	 *         in use
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping(path = "/admin/admin/register", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	@Timed
	public ResponseEntity<?> registerAdminByAdmin(@Valid @RequestBody AdminCreationDTO adminCreationDTO) {
		log.debug("REST request to save User : {}", adminCreationDTO);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
		if (!adminCreationDTO.getEmail().substring(adminCreationDTO.getEmail().indexOf("@") + 1)
				.equals(UserConstant.DEFAULT_DOMAIN)) {
			return new ResponseEntity<>("error.invalidCompanyDomain", textPlainHeaders,
					HttpStatus.BAD_REQUEST);
		}
		String password = RandomUtil.generatePassword();
		Set<UserGroup> groups = new HashSet<>(Arrays.asList(userGroupRepository.findOneByName("ADMIN_GROUP")));
		return userRepository.findOneByLogin(adminCreationDTO.getLogin().toLowerCase())
				.map(user -> new ResponseEntity<>("error.userexists", textPlainHeaders, HttpStatus.BAD_REQUEST))
				.orElseGet(() -> userRepository.findOneByEmail(adminCreationDTO.getEmail())
						.map(user -> new ResponseEntity<>("error.emailexists", textPlainHeaders,
								HttpStatus.BAD_REQUEST))
						.orElseGet(() -> {
							User user = userService.createAdmin(adminCreationDTO, password, groups);

							mailService.sendCreationEmail(user, password);

							return new ResponseEntity<>(HttpStatus.CREATED);
						}));
	}

	/**
	 * POST /admin/tenant-admin/register : register the tenant admin.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping(path = "/admin/tenant-admin/register", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	@Timed
	public ResponseEntity<?> registerTenantAdminByAdmin(
			@Valid @RequestBody TenantAdminCreationDTO tenantAdminCreationDTO) {
		log.debug("REST request to save User : {}", tenantAdminCreationDTO);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
		if (!tenantAdminCreationDTO.getTenantId().isEmpty()) {
			if (!userService.isValidEmail(tenantAdminCreationDTO.getEmail(), tenantAdminCreationDTO.getTenantId())) {
				return new ResponseEntity<>("error.invalidCompanyDomain", textPlainHeaders,
						HttpStatus.BAD_REQUEST);
			}
		}
		String password = RandomUtil.generatePassword();
		Set<UserGroup> groups = new HashSet<>(Arrays.asList(userGroupRepository.findOneByName("TENANT_ADMIN_GROUP")));
		return userRepository.findOneByLogin(tenantAdminCreationDTO.getLogin().toLowerCase())
				.map(user -> new ResponseEntity<>("error.userexists", textPlainHeaders, HttpStatus.BAD_REQUEST))
				.orElseGet(() -> userRepository.findOneByEmail(tenantAdminCreationDTO.getEmail()).map(
						user -> new ResponseEntity<>("error.emailexists", textPlainHeaders, HttpStatus.BAD_REQUEST))
						.orElseGet(() -> {
							User user = userService.createTenantAdmin(tenantAdminCreationDTO, password, groups);

							mailService.sendCreationEmail(user, password);

							try {
								tenancyService.createUserInTenantDB(user);
							} catch (Exception e) {
								e.printStackTrace();
							}

							return new ResponseEntity<>(HttpStatus.CREATED);
						}));
	}

	/**
	 * PUT /admin/admin : update admin information.
	 *
	 * @param userDTO
	 *            the current user information
	 * @return the ResponseEntity with status 200 (OK), or status 400 (Bad
	 *         Request) or 500 (Internal Server Error) if the user couldn't be
	 *         updated
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/admin/admin")
	@Timed
	public ResponseEntity<?> adminUpdatedByAdmin(@Valid @RequestBody AdminUpdateDTO userDTO) {
		log.debug("REST request to update User : {}", userDTO);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

		User curUser = userRepository.findOne(userDTO.getId());
		final String curLogin = curUser.getLogin();
		return userRepository.findOneByLogin(curLogin).map(u -> {
			userService.updateAdmin(userDTO);

			return new ResponseEntity<>(HttpStatus.OK);
		}).orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));

	}

	/**
	 * PUT /admin/tenant-admin : update the tenant admin information.
	 *
	 * @param userDTO
	 *            the current user information
	 * @return the ResponseEntity with status 200 (OK), or status 400 (Bad
	 *         Request) or 500 (Internal Server Error) if the user couldn't be
	 *         updated
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/admin/tenant-admin")
	@Timed
	public ResponseEntity<?> tenantAdminUpdatedByAdmin(@Valid @RequestBody UserUpdateDTO userDTO) {
		log.debug("REST request to update User : {}", userDTO);
		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

		User curUser = userRepository.findOne(userDTO.getId());
		final String curLogin = curUser.getLogin();
		return userRepository.findOneByLogin(curLogin).map(u -> {
			User updatedUser = userService.updateUser(userDTO);

			try {
				tenancyService.updateUserInTenantDB(updatedUser, curLogin);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return new ResponseEntity<>(HttpStatus.OK);
		}).orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));

	}

	/**
	 * GET /admin/admin : get all admins.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/admin/admin")
	@Timed
	public ResponseEntity<List<UserDTO>> getAllAdmins(@ApiParam Pageable pageable) {
		log.debug("REST request to get all Admins : {}");
		final Page<UserDTO> page = userService.getAllAdmins(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/admin/admin");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * DELETE /admin/admin/:login : delete the "login" User.
	 *
	 * @param login
	 *            the login of the user to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/admin/admin/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<Void> deleteAdminByAdmin(@PathVariable String login) {
		log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
	}

	/**
	 * DELETE /admin/tenant-admin/:login : delete the "login" User.
	 *
	 * @param login
	 *            the login of the user to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/admin/tenant-admin/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<Void> deleteTenantAdminByAdmin(@PathVariable String login) {
		log.debug("REST request to delete User: {}", login);
        User user = userService.getUserWithAuthoritiesByLogin(login).get();
        userService.deleteUser(login);
        try {
			tenancyService.deleteUserFromTenantDB(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
	}

	/**
	 * POST /tenant/users/register : register the user.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already
	 *         in use
	 */
	@PreAuthorize("hasRole('CREATE')")
	@PostMapping(path = "/tenant/user/register", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })
	@Timed
	public ResponseEntity<?> registerAccountByTenantAdmin(@Valid @RequestBody UserCreationDTO userCreationDTO) {
		log.debug("REST request to save User : {}", userCreationDTO);

		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
		String tenantId = userService.getUserWithAuthorities().getTenantId();
		if (!tenantId.isEmpty()) {
			if (!userService.isValidEmail(userCreationDTO.getEmail(), tenantId)) {
				return new ResponseEntity<>("error.invalidCompanyDomain", textPlainHeaders,
						HttpStatus.BAD_REQUEST);
			}
		}
		try {
			if (tenancyService.userWithLoginPresent(userCreationDTO.getLogin())) {
				return new ResponseEntity<>("error.userexists", textPlainHeaders, HttpStatus.BAD_REQUEST);
			}else if (tenancyService.userWithEmailPresent(userCreationDTO.getEmail())) {
				return new ResponseEntity<>("error.emailexists", textPlainHeaders, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		String password = RandomUtil.generatePassword();
		return userRepository.findOneByLogin(userCreationDTO.getLogin().toLowerCase())
				.map(user -> new ResponseEntity<>("error.userexists", textPlainHeaders, HttpStatus.BAD_REQUEST))
				.orElseGet(() -> userRepository.findOneByEmail(userCreationDTO.getEmail())
						.map(user -> new ResponseEntity<>("error.emailexists", textPlainHeaders,
								HttpStatus.BAD_REQUEST))
						.orElseGet(() -> {
							User user = userService.createTenantUser(userCreationDTO, password, tenantId);

							mailService.sendCreationEmail(user, password);
							try {
								tenancyService.createUserInDefaultDB(user);
							} catch (Exception e) {
								e.printStackTrace();
							}
							return new ResponseEntity<>(HttpStatus.CREATED);
						}));
	}

	/**
	 * PUT /tenant/user : update the current user information.
	 *
	 * @param userDTO
	 *            the current user information
	 * @return the ResponseEntity with status 200 (OK), or status 400 (Bad
	 *         Request) or 500 (Internal Server Error) if the user couldn't be
	 *         updated
	 */
	@PreAuthorize("hasRole('UPDATE')")
	@PutMapping("/tenant/user")
	@Timed
	public ResponseEntity<?> userUpdatedByTenantAdmin(@Valid @RequestBody UserUpdateDTO userDTO) {
		log.debug("REST request to update User : {}", userDTO);

		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
			User curUser = userRepository.findOne(userDTO.getId());
			final String curLogin = curUser.getLogin();
			return userRepository.findOneByLogin(curLogin).map(u -> {
				User updatedUser = userService.updateUser(userDTO);

					try {
						tenancyService.updateUserInDefaultDB(updatedUser, curLogin);
					} catch (Exception e) {
						e.printStackTrace();
					}

				return new ResponseEntity<>(HttpStatus.OK);
			}).orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
	}

	/**
	 * GET /tenant/user : get all tenant users.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@PreAuthorize("hasRole('VIEW')")
	@GetMapping("/tenant/user")
	@Timed
	public ResponseEntity<List<UserDTO>> getAllTenantUsers(@ApiParam Pageable pageable) {
		log.debug("REST request to get all tenant Users : {}");
		final Page<UserDTO> page = userService.getAllUsers(pageable,
				userService.getUserWithAuthorities().getTenantId());
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tenant/user");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * DELETE /tenant/user/:login : delete the "login" User.
	 *
	 * @param login
	 *            the login of the user to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@PreAuthorize("hasRole('DELETE')")
	@DeleteMapping("/tenant/user/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<String> deleteTenantUserByTenantAdmin(@PathVariable String login) {
		log.debug("REST request to delete User: {}", login);
		String curTenantId = userService.getUserWithAuthorities().getTenantId();

		HttpHeaders textPlainHeaders = new HttpHeaders();
		textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

		if (curTenantId.equals(userService.getUserWithAuthoritiesByLogin(login).get().getTenantId())) {
			userService.deleteUser(login);
			tenancyService.deleteUserFromDefaultDB(login);
			return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
		}

		return new ResponseEntity<>("only tenant admin can delete a tenant user", textPlainHeaders,
				HttpStatus.UNAUTHORIZED);
	}

	/**
	 * GET /user/:login : get the "login" user.
	 *
	 * @param login
	 *            the login of the user to find
	 * @return the ResponseEntity with status 200 (OK) and with body the "login"
	 *         user, or with status 404 (Not Found)
	 */
	@GetMapping("/user/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
		log.debug("REST request to get User : {}", login);
		return ResponseUtil.wrapOrNotFound(userService.getUserWithAuthoritiesByLogin(login).map(UserDTO::new));
	}

}
