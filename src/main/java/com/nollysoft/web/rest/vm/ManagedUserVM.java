package com.nollysoft.web.rest.vm;

import com.nollysoft.domain.UserGroup;
import com.nollysoft.domain.User.GENDER;
import com.nollysoft.domain.enumeration.UserRole;
import com.nollysoft.service.dto.UserDTO;
import javax.validation.constraints.Size;

import java.time.Instant;
import java.util.Set;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

	public ManagedUserVM(Long id, String login, String password, String firstName, String lastName, String title,
			String salutation, GENDER gender, UserRole role, String phone, String email, boolean activated, String imageUrl,
			String langKey, String createdBy, Instant createdDate, String lastModifiedBy, Instant lastModifiedDate,
			Set<UserGroup> userGroups, String tenantId) {

		super(id, login, firstName, lastName, title, salutation, gender, role, phone, email, activated, imageUrl, langKey,
				createdBy, createdDate, lastModifiedBy, lastModifiedDate, userGroups, tenantId);

		this.password = password;
	}

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "ManagedUserVM{" +
            "} " + super.toString();
    }
}
