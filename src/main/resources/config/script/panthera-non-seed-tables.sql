-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: gss_with_no_seed
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acknowledgement_status`
--

DROP TABLE IF EXISTS `acknowledgement_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acknowledgement_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_response` bit(1) DEFAULT NULL,
  `acknowledgement_statement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acknowledgement_statement_acknowledgement_status_id` (`acknowledgement_statement_id`),
  CONSTRAINT `fk_acknowledgement_statement_acknowledgement_status_id` FOREIGN KEY (`acknowledgement_statement_id`) REFERENCES `acknowledgement_statement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street1` varchar(255) DEFAULT NULL,
  `street2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `province` varchar(64) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aoc_legal_exception_detail`
--

DROP TABLE IF EXISTS `aoc_legal_exception_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aoc_legal_exception_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `legal_constraint_detail` varchar(255) DEFAULT NULL,
  `pcidss_requirement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_aoclegal_exception_details_pcidss_requirement_id` (`pcidss_requirement_id`),
  CONSTRAINT `fk_aoclegal_exception_details_pcidss_requirement_id` FOREIGN KEY (`pcidss_requirement_id`) REFERENCES `pcidss_requirement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aoc_notification`
--

DROP TABLE IF EXISTS `aoc_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aoc_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_date` date NOT NULL,
  `aoc_id` bigint(20) NOT NULL,
  `qa_notification_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_aocnotification_aoc_id` (`aoc_id`),
  KEY `fk_aocnotification_qa_notification_id` (`qa_notification_id`),
  CONSTRAINT `fk_aocnotification_aoc_id` FOREIGN KEY (`aoc_id`) REFERENCES `jhi_user` (`id`),
  CONSTRAINT `fk_aocnotification_qa_notification_id` FOREIGN KEY (`qa_notification_id`) REFERENCES `qa_notification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assessed_service`
--

DROP TABLE IF EXISTS `assessed_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessed_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assessment_info`
--

DROP TABLE IF EXISTS `assessment_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessment_type` varchar(255) DEFAULT NULL,
  `assessment_start_date` date DEFAULT NULL,
  `assessment_end_date` date DEFAULT NULL,
  `onsite_time_description` varchar(255) DEFAULT NULL,
  `assessor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table tracks assessment information including the name of the person (assessor) who conducts the assessment ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assessor_company`
--

DROP TABLE IF EXISTS `assessor_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessor_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `lead_qsa_contact_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  `company_url` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assessor_company_address_id` (`address_id`),
  CONSTRAINT `fk_assessor_company_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attestation`
--

DROP TABLE IF EXISTS `attestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attestation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attestor_title` varchar(255) NOT NULL,
  `attestor_name` varchar(255) NOT NULL,
  `attestor_signature` text NOT NULL,
  `attestation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `compensating_control_worksheet_response`
--

DROP TABLE IF EXISTS `compensating_control_worksheet_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compensating_control_worksheet_response` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `explanation` varchar(255) NOT NULL,
  `compensating_control_worksheet_id` bigint(20) NOT NULL,
  `saq_question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_compensating_control_worksheet_id` (`compensating_control_worksheet_id`),
  KEY `fk_saq_question_id` (`saq_question_id`),
  CONSTRAINT `fk_compensating_control_worksheet_id` FOREIGN KEY (`compensating_control_worksheet_id`) REFERENCES `compensating_control_worksheet` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_saq_question_id` FOREIGN KEY (`saq_question_id`) REFERENCES `saq_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `compliance_attestation`
--

DROP TABLE IF EXISTS `compliance_attestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compliance_attestation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compliant_statement` varchar(255) NOT NULL,
  `compliance_target_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `environment_description`
--

DROP TABLE IF EXISTS `environment_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `environment_description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `network_segmented` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `environment_description_overview`
--

DROP TABLE IF EXISTS `environment_description_overview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `environment_description_overview` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `people` varchar(255) NOT NULL,
  `processes` varchar(255) NOT NULL,
  `technologies` varchar(255) NOT NULL,
  `locations` varchar(255) NOT NULL,
  `others` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `executive_summary_status`
--

DROP TABLE IF EXISTS `executive_summary_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `executive_summary_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `section` varchar(45) NOT NULL,
  `last_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hosting_provider`
--

DROP TABLE IF EXISTS `hosting_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hosting_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `isa_attestation`
--

DROP TABLE IF EXISTS `isa_attestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isa_attestation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isa_attestor_name` varchar(255) NOT NULL,
  `isa_attestor_title` varchar(255) NOT NULL,
  `isa_attestor_company` varchar(255) NOT NULL,
  `attestation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `salutation` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(5) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  `tenant_id` varchar(128) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `idx_user_login` (`login`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jhi_user_user_group`
--

DROP TABLE IF EXISTS `jhi_user_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user_user_group` (
  `jhi_user_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`jhi_user_id`,`user_group_id`),
  KEY `FKbx5sv8ddu5asphnmu01jc72v3` (`user_group_id`),
  CONSTRAINT `FK2l802wrnufg6lrk0o0vyl2iyv` FOREIGN KEY (`jhi_user_id`) REFERENCES `jhi_user` (`id`),
  CONSTRAINT `FKbx5sv8ddu5asphnmu01jc72v3` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `facility_type` varchar(255) NOT NULL,
  `number_of_facility_type` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `managed_service`
--

DROP TABLE IF EXISTS `managed_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managed_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchant_business_type_selected`
--

DROP TABLE IF EXISTS `merchant_business_type_selected`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant_business_type_selected` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `specification` varchar(255) DEFAULT NULL,
  `merchant_business_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_merchant_business_type_selected_merchant_business_type_i_idx` (`merchant_business_type_id`),
  CONSTRAINT `fk_merchant_business_type_selected_merchant_business_type_id` FOREIGN KEY (`merchant_business_type_id`) REFERENCES `merchant_business_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `non_applicability_explanation`
--

DROP TABLE IF EXISTS `non_applicability_explanation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_applicability_explanation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reason_requirement_not_applicable` varchar(255) NOT NULL,
  `saq_question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_non_applicability_explanation_saq_question_id` (`saq_question_id`),
  CONSTRAINT `fk_non_applicability_explanation_saq_question_id` FOREIGN KEY (`saq_question_id`) REFERENCES `saq_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `non_compliant_requirement_action_plan`
--

DROP TABLE IF EXISTS `non_compliant_requirement_action_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_compliant_requirement_action_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compliant` bit(1) NOT NULL,
  `remediation_plan` varchar(255) DEFAULT NULL,
  `remediation_date` date DEFAULT NULL,
  `pcidss_requirement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_non_compliant_requirement_action_plan_pcidss_requirement_id` (`pcidss_requirement_id`),
  CONSTRAINT `fk_non_compliant_requirement_action_plan_pcidss_requirement_id` FOREIGN KEY (`pcidss_requirement_id`) REFERENCES `pcidss_requirement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  `jhi_dba` varchar(255) DEFAULT NULL,
  `company_url` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_organization_address_id` (`address_id`),
  CONSTRAINT `fk_organization_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organization_summary_status`
--

DROP TABLE IF EXISTS `organization_summary_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_summary_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `last_updated_at` datetime DEFAULT NULL,
  `section` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `other_service_response`
--

DROP TABLE IF EXISTS `other_service_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_service_response` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `other` varchar(512) DEFAULT NULL,
  `service_type_id` bigint(20) NOT NULL,
  `scope` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_other_service_response_service_type_id_idx` (`service_type_id`),
  CONSTRAINT `fk_other_service_response_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_application`
--

DROP TABLE IF EXISTS `payment_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_payment_application_more_than_one` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_application_list`
--

DROP TABLE IF EXISTS `payment_application_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_application_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_application_name` varchar(255) NOT NULL,
  `version_number` varchar(255) DEFAULT NULL,
  `application_vendor` varchar(255) NOT NULL,
  `is_application_padss_listed` bit(1) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `payment_application_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_application_list_payment_application_id` (`payment_application_id`),
  CONSTRAINT `fk_payment_application_list_payment_application_id` FOREIGN KEY (`payment_application_id`) REFERENCES `payment_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_card_business_description`
--

DROP TABLE IF EXISTS `payment_card_business_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_card_business_description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description_transmit` varchar(255) NOT NULL,
  `description_impact` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_channel_business_serves`
--

DROP TABLE IF EXISTS `payment_channel_business_serves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_channel_business_serves` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_channel_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_channel_business_serves_payment_channel_id_idx` (`payment_channel_id`),
  CONSTRAINT `fk_payment_channel_business_serves_payment_channel_id` FOREIGN KEY (`payment_channel_id`) REFERENCES `payment_channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_channel_saq_covers`
--

DROP TABLE IF EXISTS `payment_channel_saq_covers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_channel_saq_covers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_channel_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paymentchannel_saq_covers_paymentchannel_id_idx` (`payment_channel_id`),
  CONSTRAINT `fk_paymentchannel_saq_covers_payment_channel_id` FOREIGN KEY (`payment_channel_id`) REFERENCES `payment_channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_processing`
--

DROP TABLE IF EXISTS `payment_processing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_processing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pcb_description_overview`
--

DROP TABLE IF EXISTS `pcb_description_overview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcb_description_overview` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nature` varchar(255) NOT NULL,
  `how` varchar(255) NOT NULL,
  `why` varchar(255) NOT NULL,
  `types` varchar(255) NOT NULL,
  `others` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pcidss_requirement_summary`
--

DROP TABLE IF EXISTS `pcidss_requirement_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcidss_requirement_summary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compliance` varchar(45) NOT NULL,
  `pcidss_requirement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pcidss_requirement_summary_pcidss_requirement_id_idx` (`pcidss_requirement_id`),
  CONSTRAINT `fk_pcidss_requirement_summary_pcidss_requirement_id` FOREIGN KEY (`pcidss_requirement_id`) REFERENCES `pcidss_requirement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qa_notification`
--

DROP TABLE IF EXISTS `qa_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qa_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_date` date NOT NULL,
  `correction_date` date DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `qa_id` bigint(20) NOT NULL,
  `qsa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_id_UNIQUE` (`qa_id`),
  KEY `fk_qanotification_qsa_id` (`qsa_id`),
  CONSTRAINT `fk_qanotification_qa_id` FOREIGN KEY (`qa_id`) REFERENCES `jhi_user` (`id`),
  CONSTRAINT `fk_qanotification_qsa_id` FOREIGN KEY (`qsa_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qsa_attestation`
--

DROP TABLE IF EXISTS `qsa_attestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qsa_attestation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `qsa_attestor_name` varchar(255) NOT NULL,
  `qsa_attestor_signature` text NOT NULL,
  `qsa_attestor_company` varchar(255) NOT NULL,
  `attestation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qsa_notification`
--

DROP TABLE IF EXISTS `qsa_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qsa_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_date` varchar(225) NOT NULL,
  `qa_user_id` bigint(20) NOT NULL,
  `qsa_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_qsa_notification_qa_user_id_idx` (`qa_user_id`),
  KEY `fk_qsa_notification_qsa_user_id_idx` (`qsa_user_id`),
  CONSTRAINT `fk_qsa_notification_qa_user_id` FOREIGN KEY (`qa_user_id`) REFERENCES `jhi_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qsa_notification_qsa_user_id` FOREIGN KEY (`qsa_user_id`) REFERENCES `jhi_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `requirement_not_tested_explanation`
--

DROP TABLE IF EXISTS `requirement_not_tested_explanation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirement_not_tested_explanation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requirement_part_not_tested` varchar(255) DEFAULT NULL,
  `reason` varchar(255) NOT NULL,
  `saq_question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_requirement_not_tested_explanation_saq_question_id` (`saq_question_id`),
  CONSTRAINT `fk_requirement_not_tested_explanation_saq_question_id` FOREIGN KEY (`saq_question_id`) REFERENCES `saq_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resource_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource_user_group`
--

DROP TABLE IF EXISTS `resource_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_user_group` (
  `resource_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`resource_id`,`user_group_id`),
  KEY `FKlefwneeu12eno0hvwjg6nnpm1` (`user_group_id`),
  CONSTRAINT `FKlefwneeu12eno0hvwjg6nnpm1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`),
  CONSTRAINT `FKmiyd9nox168ws4y8ak4ijri42` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saq_response`
--

DROP TABLE IF EXISTS `saq_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saq_response` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_response` varchar(255) NOT NULL,
  `saq_question_id` bigint(20) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_saq_response_saq_question_id` (`saq_question_id`),
  CONSTRAINT `fk_saq_response_saq_question_id` FOREIGN KEY (`saq_question_id`) REFERENCES `saq_question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saq_review`
--

DROP TABLE IF EXISTS `saq_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saq_review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `saq_question_id` bigint(20) NOT NULL,
  `status` varchar(45) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_saq_review_saq_question_id_idx` (`saq_question_id`),
  CONSTRAINT `fk_saq_review_saq_question_id` FOREIGN KEY (`saq_question_id`) REFERENCES `saq_question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saq_review_comment`
--

DROP TABLE IF EXISTS `saq_review_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saq_review_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_updated_at` datetime NOT NULL,
  `saq_review_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_saq_review_comment_saq_review_id_idx` (`saq_review_id`),
  CONSTRAINT `fk_saq_review_comment_saq_review_id` FOREIGN KEY (`saq_review_id`) REFERENCES `saq_review` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scope_verification_not_selected_service`
--

DROP TABLE IF EXISTS `scope_verification_not_selected_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scope_verification_not_selected_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `not_assessed_service_name` varchar(255) NOT NULL,
  `not_assessed_service_type` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scope_verification_selected_service`
--

DROP TABLE IF EXISTS `scope_verification_selected_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scope_verification_selected_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessed_service_name` varchar(255) NOT NULL,
  `assessed_service_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selected_service_type`
--

DROP TABLE IF EXISTS `selected_service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selected_service_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_selected_service_type_service_type_id` (`service_type_id`),
  CONSTRAINT `fk_selected_service_type_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_by_service_provider`
--

DROP TABLE IF EXISTS `service_by_service_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_by_service_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(255) NOT NULL,
  `description_of_service_provider` varchar(255) NOT NULL,
  `third_party_service_provider_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_by_service_provider_third_party_service_provider_id` (`third_party_service_provider_id`),
  CONSTRAINT `fk_service_by_service_provider_third_party_service_provider_id` FOREIGN KEY (`third_party_service_provider_id`) REFERENCES `third_party_service_provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_type_not_selected`
--

DROP TABLE IF EXISTS `service_type_not_selected`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_type_not_selected` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_type_not_selected_service_type_id` (`service_type_id`),
  CONSTRAINT `fk_service_type_not_selected_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `services_by_qsa_company`
--

DROP TABLE IF EXISTS `services_by_qsa_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_by_qsa_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_description` varchar(255) NOT NULL,
  `effort_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_url` varchar(255) DEFAULT NULL,
  `contact_person_deskphone` varchar(32) DEFAULT NULL,
  `contact_person_email` varchar(64) DEFAULT NULL,
  `contact_person_mobilephone` varchar(32) DEFAULT NULL,
  `contact_person_name` varchar(128) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `company_abbreviation` varchar(45) DEFAULT NULL,
  `company_phone_number` varchar(32) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `tenant_id` varchar(128) DEFAULT NULL,
  `address_id` bigint(20) NOT NULL,
  `assessment_type_id` bigint(20) DEFAULT NULL,
  `industry_id` bigint(20) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tenant_address_id_idx` (`address_id`),
  KEY `fk_tenant_assessment_type_id_idx` (`assessment_type_id`),
  KEY `fk_tenant_industry_id_idx` (`industry_id`),
  CONSTRAINT `fk_tenant_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tenant_assessment_type_id` FOREIGN KEY (`assessment_type_id`) REFERENCES `assessment_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tenant_industry_id` FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tested_requirement_header`
--

DROP TABLE IF EXISTS `tested_requirement_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tested_requirement_header` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_assessed_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tested_requirement_summary`
--

DROP TABLE IF EXISTS `tested_requirement_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tested_requirement_summary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessed` varchar(45) NOT NULL,
  `justification` varchar(255) DEFAULT NULL,
  `pcidss_requirement_id` bigint(20) NOT NULL,
  `tested_requirement_header_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tested_requirement_summary_pcidss_requirement_id` (`pcidss_requirement_id`),
  KEY `fk_tested_requirement_summary_tested_requirement_header_id_idx` (`tested_requirement_header_id`),
  CONSTRAINT `fk_tested_requirement_summary_pcidss_requirement_id` FOREIGN KEY (`pcidss_requirement_id`) REFERENCES `pcidss_requirement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tested_requirement_summary_tested_requirement_header_id` FOREIGN KEY (`tested_requirement_header_id`) REFERENCES `tested_requirement_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `third_party_service_provider`
--

DROP TABLE IF EXISTS `third_party_service_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `third_party_service_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `qir_relationship_present` bit(1) NOT NULL,
  `qir_company_name` varchar(255) DEFAULT NULL,
  `qir_individual_name` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `service_provider_relation_ship_present` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-28 18:08:07
