import { Component, OnInit } from '@angular/core';

import { Principal, LoginService } from '../../shared';
import { PasswordService } from './password.service';
import { Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-password',
    templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    password: string;
    confirmPassword: string;

    constructor(
        private loginService: LoginService,
        private router: Router,
        private alertService: JhiAlertService,
        private passwordService: PasswordService,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    changePassword() {
        if (this.password !== this.confirmPassword) {
            this.error = null;
            this.success = null;
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.passwordService.save(this.password).subscribe(() => {
                this.error = null;
                this.success = 'OK';
                this.loginService.logout();
                this.router.navigate(['']);
                this.alertService.success('password.messages.success', null, null)
            }, () => {
                this.success = null;
                this.error = 'ERROR';
            });
        }
    }
}
