import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PcidssSharedModule } from '../shared';
import { PcidssSharedLibsModule } from '../shared';

import { PcidssUserGroupModule } from './user-group/user-group.module';
import { PcidssPrivilegeModule } from './privilege/privilege.module';
import { PcidssAuthorityModule } from './authority/authority.module';
import { PcidssResourceModule } from './resource/resource.module';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

import {
    adminState,
    AuditsComponent,
    UserManagementComponent,
    CreateUserComponent,
    UserListComponent,
    UserDeleteDialogComponent,
    UserMgmtDeleteDialogComponent,
    LeftNavComponent,
    ViewUserComponent,
    TenantListComponent,
    TenantManagementComponent,
    CreateTenantComponent,
    ViewTenantComponent,
    TenantDeleteDialogComponent,
    TenantMgmtDeleteDialogComponent,
    LogsComponent,
    JhiMetricsMonitoringModalComponent,
    JhiMetricsMonitoringComponent,
    JhiHealthModalComponent,
    JhiHealthCheckComponent,
    JhiConfigurationComponent,
    JhiDocsComponent,
    AuditsService,
    JhiConfigurationService,
    JhiHealthService,
    JhiMetricsService,
    LogsService,
    UserResolvePagingParams,
    UserResolve,
    UserModalService,
    TenantResolvePagingParams,
    TenantResolve,
    TenantModalService,
    CreateAdminComponent
} from './';

@NgModule({
    imports: [
        PcidssSharedModule,
        PcidssSharedLibsModule,
        RouterModule.forRoot(adminState, { useHash: true }),
        PcidssPrivilegeModule,
        PcidssAuthorityModule,
        PcidssUserGroupModule,
        PcidssResourceModule,
        FormsModule,
        ReactiveFormsModule,
        /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    ],
    declarations: [
        AuditsComponent,
        UserManagementComponent,
        CreateUserComponent,
        UserListComponent,
        ViewUserComponent,
        LeftNavComponent,
        UserDeleteDialogComponent,
        UserMgmtDeleteDialogComponent,
        CreateTenantComponent,
        TenantListComponent,
        ViewTenantComponent,
        TenantManagementComponent,
        TenantDeleteDialogComponent,
        TenantMgmtDeleteDialogComponent,
        LogsComponent,
        JhiConfigurationComponent,
        JhiHealthCheckComponent,
        JhiHealthModalComponent,
        JhiDocsComponent,
        JhiMetricsMonitoringComponent,
        JhiMetricsMonitoringModalComponent,
        CreateAdminComponent
    ],
    entryComponents: [
        UserMgmtDeleteDialogComponent,
        TenantMgmtDeleteDialogComponent,
        JhiHealthModalComponent,
        JhiMetricsMonitoringModalComponent,
    ],
    providers: [
        AuditsService,
        JhiConfigurationService,
        JhiHealthService,
        JhiMetricsService,
        LogsService,
        UserResolvePagingParams,
        UserResolve,
        UserModalService,
        TenantResolvePagingParams,
        TenantResolve,
        TenantModalService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssAdminModule { }
