import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Authority } from './authority.model';
import { AuthorityPopupService } from './authority-popup.service';
import { AuthorityService } from './authority.service';
import { Privilege, PrivilegeService } from '../privilege';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-authority-dialog',
    templateUrl: './authority-dialog.component.html'
})
export class AuthorityDialogComponent implements OnInit {

    authority: Authority;
    authorities: any[];
    isSaving: boolean;

    privileges: Privilege[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private authorityService: AuthorityService,
        private privilegeService: PrivilegeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.privilegeService.query()
            .subscribe((res: ResponseWrapper) => { this.privileges = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.authority.id !== undefined) {
            this.subscribeToSaveResponse(
                this.authorityService.update(this.authority));
        } else {
            this.subscribeToSaveResponse(
                this.authorityService.create(this.authority));
        }
    }

    private subscribeToSaveResponse(result: Observable<Authority>) {
        result.subscribe((res: Authority) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Authority) {
        this.eventManager.broadcast({ name: 'authorityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackPrivilegeById(index: number, item: Privilege) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-authority-popup',
    template: ''
})
export class AuthorityPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private authorityPopupService: AuthorityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.authorityPopupService
                    .open(AuthorityDialogComponent, params['id']);
            } else {
                this.modalRef = this.authorityPopupService
                    .open(AuthorityDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
