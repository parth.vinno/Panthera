export * from './privilege.model';
export * from './privilege-popup.service';
export * from './privilege.service';
export * from './privilege-dialog.component';
export * from './privilege-delete-dialog.component';
export * from './privilege-detail.component';
export * from './privilege.component';
export * from './privilege.route';
