import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Privilege } from './privilege.model';
import { PrivilegePopupService } from './privilege-popup.service';
import { PrivilegeService } from './privilege.service';

@Component({
    selector: 'jhi-privilege-delete-dialog',
    templateUrl: './privilege-delete-dialog.component.html'
})
export class PrivilegeDeleteDialogComponent {

    privilege: Privilege;

    constructor(
        private privilegeService: PrivilegeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.privilegeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'privilegeListModification',
                content: 'Deleted an privilege'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-privilege-delete-popup',
    template: ''
})
export class PrivilegeDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private privilegePopupService: PrivilegePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.privilegePopupService
                .open(PrivilegeDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
