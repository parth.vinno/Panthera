import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Privilege } from './privilege.model';
import { PrivilegeService } from './privilege.service';

@Component({
    selector: 'jhi-privilege-detail',
    templateUrl: './privilege-detail.component.html'
})
export class PrivilegeDetailComponent implements OnInit, OnDestroy {

    privilege: Privilege;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private privilegeService: PrivilegeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPrivileges();
    }

    load(id) {
        this.privilegeService.find(id).subscribe((privilege) => {
            this.privilege = privilege;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPrivileges() {
        this.eventSubscriber = this.eventManager.subscribe(
            'privilegeListModification',
            (response) => this.load(this.privilege.id)
        );
    }
}
