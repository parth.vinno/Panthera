import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Privilege } from './privilege.model';
import { PrivilegePopupService } from './privilege-popup.service';
import { PrivilegeService } from './privilege.service';

@Component({
    selector: 'jhi-privilege-dialog',
    templateUrl: './privilege-dialog.component.html'
})
export class PrivilegeDialogComponent implements OnInit {

    privilege: Privilege;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private privilegeService: PrivilegeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.privilege.id !== undefined) {
            this.subscribeToSaveResponse(
                this.privilegeService.update(this.privilege));
        } else {
            this.subscribeToSaveResponse(
                this.privilegeService.create(this.privilege));
        }
    }

    private subscribeToSaveResponse(result: Observable<Privilege>) {
        result.subscribe((res: Privilege) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Privilege) {
        this.eventManager.broadcast({ name: 'privilegeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-privilege-popup',
    template: ''
})
export class PrivilegePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private privilegePopupService: PrivilegePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.privilegePopupService
                    .open(PrivilegeDialogComponent, params['id']);
            } else {
                this.modalRef = this.privilegePopupService
                    .open(PrivilegeDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
