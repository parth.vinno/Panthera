import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Privilege } from './privilege.model';
import { PrivilegeService } from './privilege.service';

@Injectable()
export class PrivilegePopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private privilegeService: PrivilegeService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.privilegeService.find(id).subscribe((privilege) => {
                this.privilegeModalRef(component, privilege);
            });
        } else {
            setTimeout(() => this.privilegeModalRef(component, new Privilege()), 0);
        }
    }

    privilegeModalRef(component: Component, privilege: Privilege): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.privilege = privilege;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
