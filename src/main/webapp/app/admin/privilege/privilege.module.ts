import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import {
    PrivilegeService,
    PrivilegePopupService,
    PrivilegeComponent,
    PrivilegeDetailComponent,
    PrivilegeDialogComponent,
    PrivilegePopupComponent,
    PrivilegeDeletePopupComponent,
    PrivilegeDeleteDialogComponent,
    privilegeRoute,
    privilegePopupRoute,
    PrivilegeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...privilegeRoute,
    ...privilegePopupRoute,
];

@NgModule({
    imports: [
        PcidssSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PrivilegeComponent,
        PrivilegeDetailComponent,
        PrivilegeDialogComponent,
        PrivilegeDeleteDialogComponent,
        PrivilegePopupComponent,
        PrivilegeDeletePopupComponent,
    ],
    entryComponents: [
        PrivilegeComponent,
        PrivilegeDialogComponent,
        PrivilegePopupComponent,
        PrivilegeDeleteDialogComponent,
        PrivilegeDeletePopupComponent,
    ],
    providers: [
        PrivilegeService,
        PrivilegePopupService,
        PrivilegeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssPrivilegeModule {}
