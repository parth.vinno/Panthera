import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PrivilegeComponent } from './privilege.component';
import { PrivilegeDetailComponent } from './privilege-detail.component';
import { PrivilegePopupComponent } from './privilege-dialog.component';
import { PrivilegeDeletePopupComponent } from './privilege-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class PrivilegeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const privilegeRoute: Routes = [
    {
        path: 'privilege',
        component: PrivilegeComponent,
        resolve: {
            'pagingParams': PrivilegeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'pcidssApp.privilege.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'privilege/:id',
        component: PrivilegeDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'pcidssApp.privilege.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const privilegePopupRoute: Routes = [
    {
        path: 'privilege-new',
        component: PrivilegePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'pcidssApp.privilege.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'privilege/:id/edit',
        component: PrivilegePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'pcidssApp.privilege.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'privilege/:id/delete',
        component: PrivilegeDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'pcidssApp.privilege.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
