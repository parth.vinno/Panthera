import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Privilege } from './privilege.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PrivilegeService {

    private resourceUrl = 'api/privileges';

    constructor(private http: Http) { }

    create(privilege: Privilege): Observable<Privilege> {
        const copy = this.convert(privilege);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(privilege: Privilege): Observable<Privilege> {
        const copy = this.convert(privilege);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Privilege> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(privilege: Privilege): Privilege {
        const copy: Privilege = Object.assign({}, privilege);
        return copy;
    }
}
