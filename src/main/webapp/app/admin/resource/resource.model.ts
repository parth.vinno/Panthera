import { BaseEntity } from './../../shared';

export class Resource implements BaseEntity {
    constructor(
        public id?: number,
        public resourceURL?: string,
        public userGroups?: BaseEntity[],
    ) {
    }
}
