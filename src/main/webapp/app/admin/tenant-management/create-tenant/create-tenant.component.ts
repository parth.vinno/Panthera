import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {Address, AssessmentType, Country, Industry, JhiLanguageHelper, State, Tenant, TenantManagementService, UserService} from '../../../shared';

import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs/Subscription';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {NgForm} from '@angular/forms';

enum DROP_DOWN {
    COUNTRY,
    STATE,
    INDUSTRY
}

@Component({
    selector: 'jhi-create-tenant',
    templateUrl: './create-tenant.component.html',
    styleUrls: ['./create-tenant.component.scss']
})
export class CreateTenantComponent implements OnInit, OnDestroy {
    @ViewChild('tenantForm') public tenantForm: NgForm;

    tenant: Tenant;
    languages: any[];
    authorities: any[];
    tenants: Tenant[];
    isEditPage: Boolean;
    assessments: AssessmentType;
    industries: Industry[];
    countries: Country[];
    states: State[];
    datePicker;
    createDate;
    minDate;
    serviceStartDate;
    submitted: boolean;
    contactPersonDomainMatch;
    tenantEmailDomainMatch;

    /*DroDown settings*/
    dropdownSettings: any = {};
    countryDropdownSettings: any = {};
    stateDropdownSettings: any = {};
    DROP_DOWN = DROP_DOWN;

    selectedIndustry;
    selectedCountry;
    selectedState;

    private deleteSubscription: Subscription;
    private paramsSubscription: Subscription;

    constructor(
        private languageHelper: JhiLanguageHelper,
        private router: Router,
        private route: ActivatedRoute,
        private _dateFormatter: NgbDateParserFormatter,
        private tenantManagementService: TenantManagementService,
        private eventManager: JhiEventManager,
        private spinnerService: Ng4LoadingSpinnerService,
        private alertService: JhiAlertService
    ) {
        this.isEditPage = this.router.url.includes('edit');

        Observable.forkJoin(this.tenantManagementService.getAssessmentType(),
            this.tenantManagementService.getIndustryType(),
            this.tenantManagementService.getAllCountries(),
            this.languageHelper.getAll()).subscribe((results) => {
            const [assessment, industry, country, languages] = results;
            this.assessments = assessment.json;
            this.industries = industry.json;
            this.countries = country.json;
            this.languages = languages;
            if (this.isEditPage) {
                this.updateSelectedTenant();
            }
        });

        const curDate = new Date();
        this.minDate = {year: curDate.getFullYear(), month: curDate.getMonth() + 1, day: curDate.getDate()};
        this.tenant = new Tenant();
        this.tenant.addressId = new Address();
        this.tenant.assessmentTypeId = new AssessmentType();
        this.tenant.industryId = new Industry();
        this.tenant.createDate = this._dateFormatter.format(this.minDate);

        this.authorities = [];
    }

    ngOnInit() {
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'name',
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };

        this.countryDropdownSettings = {
            singleSelection: true,
            idField: 'countryCode',
            textField: 'countryName',
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };

        this.stateDropdownSettings = {
            singleSelection: true,
            idField: 'shortName',
            textField: 'longName',
            allowSearchFilter: true,
            closeDropDownOnSelection: true

        };
        this.subscribeToDeleteUser();
    }

    updateSelectedTenant() {
        this.paramsSubscription = this.route.params.subscribe((params) => {
            this.tenantManagementService.getTenantDetail(params['id']).subscribe(
                (res) => {
                    this.tenant = res.json;
                    this.selectedIndustry = [this.tenant.industryId];
                    this.selectedCountry = [this.countries.find((country) => {
                        return country.countryCode === this.tenant.addressId.country;
                    })];
                    this.tenantManagementService.getStates(this.tenant.addressId.country).subscribe((states) => {
                            this.states = states.json;
                            if (this.states && this.states.length) {
                                this.selectedState = [this.states.find((state) => {
                                    return state.shortName === this.tenant.addressId.state;
                                })]
                            }
                        });
                    this.serviceStartDate = this._dateFormatter.parse(this.tenant.serviceStartDate);
                    this.checkEmailDomain();
                });
        });
    }

    onItemSelect(type: DROP_DOWN, item: any) {
        switch (type) {
            case DROP_DOWN.COUNTRY :
                this.tenant.addressId.country = item.countryCode;
                this.getStates();
                break;

            case DROP_DOWN.INDUSTRY:
                this.tenant.industryId = this.industries.find((industry) => {
                    return industry.id === item.id;
                });
                break;

            case DROP_DOWN.STATE:
                this.tenant.addressId.state = item.shortName;
                break;

        }
    }

    onItemDeSelect(type: DROP_DOWN) {
        switch (type) {
            case DROP_DOWN.COUNTRY :
                this.tenant.addressId.country = undefined;
                this.getStates();
                break;

            case DROP_DOWN.INDUSTRY:
                this.tenant.industryId = undefined;
                break;

            case DROP_DOWN.STATE:
                this.tenant.addressId.state = undefined;
                break;

        }
    }

    getStates() {
        this.tenant.addressId.state = '';
        this.selectedState = [];
        if (this.tenant.addressId.country) {
            this.tenantManagementService.getStates(this.tenant.addressId.country).subscribe(
                (res) => {
                    this.states = res.json;
                }
            )
        }
    }

    getTenantId() {
        if (this.tenant.assessmentTypeId.name && this.tenant.companyAbbreviation) {
            this.tenant.tenantId = this.tenant.companyAbbreviation + '_' + this.tenant.assessmentTypeId.name + '_saqd';
        }
    }

    subscribeToDeleteUser() {
        this.deleteSubscription = this.eventManager.subscribe('tenantListModification', (response) => this.routeToHome());
    }

    private validateForm() {
        this.submitted = true;
        return (this.tenantForm && this.tenantForm.form.valid && this.tenant.assessmentTypeId && this.tenant.industryId && this.tenant.addressId.country
            && this.tenant.addressId.state && this.serviceStartDate && this.contactPersonDomainMatch && this.tenantEmailDomainMatch);
    }

    save() {
        if (this.validateForm()) {
            this.tenant.serviceStartDate = this._dateFormatter.format(this.serviceStartDate);
            this.spinnerService.show();
            if (this.tenant.id) {
                this.tenantManagementService.updateTenant(this.tenant).subscribe((response) => this.onUpdateSuccess(),
                    (error) => this.onSaveError(error));
            } else {
                this.tenantManagementService.createTenant(this.tenant).subscribe((response) => this.onSaveSuccess(response.json),
                    (error) => this.onSaveError(error));
            }
        }
    }

    onUpdateSuccess() {
        this.routeToHome();
        this.spinnerService.hide();
    }

    private onSaveSuccess(createdTenant: Tenant) {
        this.spinnerService.hide();
        this.router.navigate(['user-management/create'], {
            queryParams: {
                email: createdTenant.contactPersonEmail,
                login: createdTenant.contactPersonEmail,
                firstName: createdTenant.contactPersonName.indexOf(' ') !== -1 ? createdTenant.contactPersonName.substr(0, createdTenant.contactPersonName.indexOf(' '))
                    : createdTenant.contactPersonName,
                lastName: createdTenant.contactPersonName.indexOf(' ') === -1 ? '' : createdTenant.contactPersonName.substr(createdTenant.contactPersonName.indexOf(' ') + 1),
                phone: createdTenant.contactPersonMobilephone,
                tenantId: createdTenant.tenantId
            }
        });
    }

    isJson(error) {
        try {
            JSON.parse(error);
        } catch (e) {
            return error;
        }
        return JSON.parse(error).message;
    }

    private onSaveError(error) {
        this.spinnerService.hide();
        this.alertService.error(this.isJson(error._body), null, null);
    }

    checkEmailDomain() {
        if (this.tenant.companyUrl && this.tenant.contactPersonEmail && this.tenant.contactPersonEmail.indexOf('@') !== -1) {
            const domain = this.tenant.contactPersonEmail.replace(/.*@/, '');
            this.tenant.companyUrl.includes(domain) ? this.contactPersonDomainMatch = true : this.contactPersonDomainMatch = false;
        }

        if (this.tenant.companyUrl && this.tenant.email && this.tenant.email.indexOf('@') !== -1) {
            const domain = this.tenant.email.replace(/.*@/, '');
            this.tenant.companyUrl.includes(domain) ? this.tenantEmailDomainMatch = true : this.tenantEmailDomainMatch = false;
        }
    }

    routeToHome() {
        this.router.navigate(['tenant-management/tenants']);
    }

    cancel() {
        this.routeToHome();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.deleteSubscription);
        if (this.paramsSubscription) {
            this.paramsSubscription.unsubscribe();
        }
    }
}
