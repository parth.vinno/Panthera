import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Tenant, TenantManagementService } from '../../shared';
import { TenantModalService } from './tenant-modal.service';

@Component({
    selector: 'jhi-tenant-mgmt-delete-dialog',
    templateUrl: './tenant-management-delete-dialog.component.html'
})
export class TenantMgmtDeleteDialogComponent {

    tenant: Tenant;

    constructor(
        private tenantManagementService: TenantManagementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id) {
        this.tenantManagementService.deleteTenantAdmin(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'tenantListModification',
                content: 'Deleted a tenant'});
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tenant-delete-dialog',
    template: ''
})
export class TenantDeleteDialogComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tenantModalService: TenantModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.tenantModalService.open(TenantMgmtDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
