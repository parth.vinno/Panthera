import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TenantModalService } from './tenant-modal.service';
import { JhiLanguageHelper, User, UserService, TenantManagementService } from '../../shared';
import {Tenant} from '../../shared/apis/models/user.model';

@Component({
    selector: 'jhi-tenant-mgmt-dialog',
    templateUrl: './tenant-management-dialog.component.html',
    styleUrls: ['./tenant-management-dialog.component.scss']
})
export class TenantMgmtDialogComponent implements OnInit {

    user: User;
    languages: any[];
    authorities: any[];
    tenants: Tenant[];
    isSaving: Boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private languageHelper: JhiLanguageHelper,
        private userService: UserService,
        private tenantManagementService: TenantManagementService,
        private eventManager: JhiEventManager,
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.authorities = [];

        this.userService.getTenants().subscribe((res) => {
            this.tenants = res;
        });

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== null) {
            this.tenantManagementService.updateTenant(this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            this.tenantManagementService.createTenant(this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        }
    }

    private onSaveSuccess(result) {
        this.eventManager.broadcast({ name: 'tenantListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-tenant-dialog',
    template: ''
})
export class TenantDialogComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tenantModalService: TenantModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['login'] ) {
                this.modalRef = this.tenantModalService.open(TenantMgmtDialogComponent, params['id']);
            } else {
                this.modalRef = this.tenantModalService.open(TenantMgmtDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
