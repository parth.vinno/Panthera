import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-tenant-management',
    templateUrl: './tenant-management.component.html',
    styleUrls: ['./tenant-management.component.scss']
})
export class TenantManagementComponent implements OnInit {
    tenantType = 'Tenant';

    constructor() { }

    ngOnInit() {
    }

}
