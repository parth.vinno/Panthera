import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { JhiPaginationUtil } from 'ng-jhipster';

import {
    TenantManagementComponent,
    ViewTenantComponent,
    TenantListComponent,
    CreateTenantComponent,
    TenantDeleteDialogComponent
} from '../';

import { Principal } from '../../shared';

@Injectable()
export class TenantResolve implements CanActivate {

    constructor(private principal: Principal) { }

    canActivate() {
        return this.principal.identity().then((account) => this.principal.hasAnyAuthority(['ROLE_ADMIN']));
    }
}

@Injectable()
export class TenantResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const tenantMgmtRoute: Routes = [
    {
        path: 'tenant-management',
        component: TenantManagementComponent,
        data: {
            pageTitle: 'tenantManagement.home.title'
        },
        children: [
            {
                path: 'tenants',
                component: TenantListComponent,
                data: {
                    pageTitle: 'tenantManagement.home.title'
                },
                resolve: {
                    'pagingParams': TenantResolvePagingParams
                }
            },
            {
                path: 'create',
                component: CreateTenantComponent,
                data: {
                    pageTitle: 'tenantManagement.home.title'
                }
            },
            {
                path: 'view/:id',
                component: ViewTenantComponent,
                data: {
                    pageTitle: 'tenantManagement.home.title'
                }
            },
            {
                path: 'edit/:id',
                component: CreateTenantComponent,
                data: {
                    pageTitle: 'tenantManagement.home.title'
                }
            },
            {
                path: '',
                redirectTo: 'tenants',
                pathMatch: 'full'
            }
        ]
    }
];

export const tenantDialogRoute: Routes = [
    {
        path: 'tenant-management/:id/delete',
        component: TenantDeleteDialogComponent,
        outlet: 'popup'
    }
];
