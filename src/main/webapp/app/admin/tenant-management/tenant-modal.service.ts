import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { Tenant, TenantManagementService } from '../../shared';

@Injectable()
export class TenantModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private tenantManagementService: TenantManagementService
    ) {}

    open(component: Component, id?: string): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.tenantManagementService.getTenantDetail(id).subscribe((tenant) => this.tenantModalRef(component, tenant.json));
        } else {
            setTimeout(() => this.tenantModalRef(component, new Tenant()), 0);
        }
    }

    tenantModalRef(component: Component, tenant: Tenant): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tenant = tenant;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
