import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import {Tenant, TenantManagementService} from '../../../shared';

@Component({
    selector: 'jhi-view-tenant',
    templateUrl: './view-tenant.component.html',
    styleUrls: ['./view-tenant.component.scss']
})
export class ViewTenantComponent implements OnInit, OnDestroy {

    tenant = new Tenant();
    private subscription: Subscription;

    constructor(
        private tenantManagementService: TenantManagementService,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
    }

    load(id) {
        this.tenantManagementService.getTenantDetail(id).subscribe((tenant) => {
            this.tenant = tenant.json;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    cancel() {
        this.router.navigate(['tenant-management/tenants']);
    }
}
