import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {GENDER, JhiLanguageHelper, Principal, Tenant, User, UserManagementService, UserService} from '../../../shared';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'jhi-create-admin',
    templateUrl: './create-admin.component.html',
    styleUrls: ['./create-admin.component.scss']
})
export class CreateAdminComponent implements OnInit, OnDestroy {
    @ViewChild('adminForm') public adminForm: NgForm;

    user: User;
    tenants: Tenant;
    languages: any[];
    authorities: any[];
    currentAccount: User;
    isNollySoftAdmin: boolean;
    isEditPage: boolean;
    GENDER;
    submitted: boolean;
    tenantDomain = 'nollysoft.com';
    domainMatch: boolean;

    private deleteSubscription: Subscription;
    private queryParamsSubscription: Subscription;
    private pathParamsSubscription: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private principal: Principal,
        private userService: UserService,
        private eventManager: JhiEventManager,
        private languageHelper: JhiLanguageHelper,
        private userManagementService: UserManagementService,
        private alertService: JhiAlertService,
    ) {
        this.GENDER = GENDER;
        this.isEditPage = this.router.url.includes('edit');
        this.user = new User();
    }

    ngOnInit() {
        this.authorities = [];
        if (this.isEditPage) {
            this.pathParamsSubscription = this.route.params.subscribe((params) => {
                this.userManagementService.getUserDetail(params['login']).subscribe((user) => {
                    this.user = user.json;
                    this.checkEmailDomain();
                });
            });
        }
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.isNollySoftAdmin = this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1;
        });

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.subscribeToDeleteUser();
    }

    checkEmailDomain() {
        if (this.tenantDomain && this.user.email && this.user.email.indexOf('@') !== -1) {
            const domain = this.user.email.replace(/.*@/, '').toLowerCase();
            this.tenantDomain.includes(domain) ? this.domainMatch = true : this.domainMatch = false;
        }
    }

    private validateForm() {
        this.submitted = true;
        return (this.adminForm && this.adminForm.form.valid && this.user.langKey && this.domainMatch);
    }

    subscribeToDeleteUser() {
        this.deleteSubscription = this.eventManager.subscribe('userListModification', (response) => this.routeToHome());
    }

    save() {
        if (this.validateForm()) {
            if (this.user.id) {
                this.userManagementService.updateAdmin(this.user).subscribe((response) => this.onSaveSuccess(response), (error) => this.onSaveError(error));
            } else {
                this.userManagementService.registerAdmin(this.user).subscribe((response) => this.onSaveSuccess(response), (error) => this.onSaveError(error));
            }
        }
    }

    routeToHome() {
        this.eventManager.destroy(this.deleteSubscription);
        this.router.navigate(['user-management/users']);
    }

    cancel() {
        this.routeToHome();
    }

    isJson(error) {
        try {
            JSON.parse(error);
        } catch (e) {
            return error;
        }
        return JSON.parse(error).message;
    }

    private onSaveSuccess(result) {
        this.eventManager.broadcast({name: 'userListModification', content: 'OK'});

        this.routeToHome();
    }

    private onSaveError(error) {
        this.alertService.error(this.isJson(error._body), null, null);
    }

    ngOnDestroy() {
        if (this.deleteSubscription) {
            this.eventManager.destroy(this.deleteSubscription);
        }
        if (this.queryParamsSubscription) {
            this.queryParamsSubscription.unsubscribe();
        }
        if (this.pathParamsSubscription) {
            this.pathParamsSubscription.unsubscribe();
        }
    }

}
