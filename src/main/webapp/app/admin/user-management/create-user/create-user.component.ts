import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {GENDER, JhiLanguageHelper, Principal, Tenant, TenantManagementService, User, UserGroup, UserManagementService, UserService} from '../../../shared';
import {FormGroup, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'jhi-create-user',
    templateUrl: './create-user.component.html',
    styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {
    @ViewChild('tenantUserForm') public tenantUserForm: NgForm;

    user: User;
    tenants: Tenant[];
    languages: any[];
    authorities: any[];
    userGroups: UserGroup[];
    currentAccount: User;
    roles: string[];
    isNollySoftAdmin: boolean;
    isEditPage: boolean;
    GENDER;
    tenantDomain: string;
    domainMatch: boolean;
    isLastPageTenant: boolean;

    myForm: FormGroup;
    disabled = false;
    ShowFilter = false;
    dropdownSettings: any = {};
    singleDropdownSettings: any = {};
    roleDropdownSettings: any = {};
    submitted: boolean;
    selectedRole: string[];

    private deleteSubscription: Subscription;
    private queryParamsSubscription: Subscription;
    private pathParamsSubscription: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private principal: Principal,
        private userService: UserService,
        private eventManager: JhiEventManager,
        private languageHelper: JhiLanguageHelper,
        private userManagementService: UserManagementService,
        private alertService: JhiAlertService,
        private tenantManagementService: TenantManagementService
    ) {
        this.GENDER = GENDER;
        this.isEditPage = this.router.url.includes('edit');
        this.user = new User();
        this.user.userGroups = [];
    }

    ngOnInit() {
        this.authorities = [];
        if (this.isEditPage) {
            this.pathParamsSubscription = this.route.params.subscribe((params) => {
                this.userManagementService.getUserDetail(params['login']).subscribe((user) => {
                    this.user = user.json;
                    if (this.user.role) {
                        this.selectedRole = [this.user.role];
                    }
                    if (this.user.tenantId) {
                        this.tenantManagementService.getDomainByTenantId(this.user.tenantId).subscribe((domain) => {
                            this.tenantDomain = domain;
                            this.checkEmailDomain();
                        });
                    }
                });
            });
        } else {
            this.queryParamsSubscription = this.route.queryParams.subscribe((params) => {
                Object.assign(this.user, params);
                if (params['email']) {
                    this.isLastPageTenant = true;
                }
                if (this.user.tenantId) {
                    this.tenantManagementService.getDomainByTenantId(this.user.tenantId).subscribe((domain) => {
                        this.tenantDomain = domain;
                        this.checkEmailDomain();
                    });
                }
            });
        }
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.isNollySoftAdmin = this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1;
            if (this.isNollySoftAdmin) {
                this.userService.getTenants().subscribe((res) => this.tenants = res);
            } else {
                this.userService.userGroups().subscribe((res) => this.userGroups = res);
                this.user.tenantId = account.tenantId;
                if (this.user.tenantId) {
                    this.tenantManagementService.getDomainByTenantId(this.user.tenantId).subscribe((domain) => {
                        this.tenantDomain = domain;
                    });
                }            }
        });

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.userManagementService.getAllUsersRoles().subscribe(
            (roles) => {
                this.roles = roles.json;
            });

        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: this.ShowFilter,
        };

        this.singleDropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'tenantId',
            allowSearchFilter: true
        };

        this.roleDropdownSettings = {
            singleSelection: true,
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };

        this.subscribeToDeleteUser();
    }

    onTenantSelect(tenant: any) {
        this.user.tenantId = tenant.tenantId;
        this.tenantManagementService.getDomainByTenantId(tenant.tenantId).subscribe( (domain) => {
           this.tenantDomain = domain;
           this.checkEmailDomain();
        });
    }

    onTenantDeselect() {
        this.user.tenantId = undefined;
    }

    onRoleSelect(role: string) {
        this.user.role = role;
    }

    checkEmailDomain() {
        if (this.tenantDomain && this.user.email && this.user.email.indexOf('@') !== -1) {
            const domain = this.user.email.replace(/.*@/, '');
            this.tenantDomain.includes(domain) ? this.domainMatch = true : this.domainMatch = false;
        }
    }

    onItemSelect(item: any) {
        if (!this.user.userGroups) {
            this.user.userGroups = [];
        }
        const index = this.userGroups.map(function(x) {
            return x.id;
        }).indexOf(item.id);
        this.user.userGroups.push(this.userGroups[index]);
    }

    onDeSelect(item: any) {
        const index = this.user.userGroups.map(function(x) {
            return x.id;
        }).indexOf(item.id);
        this.user.userGroups.splice(index, 1);
    }

    onSelectAll() {
        this.user.userGroups = this.userGroups;
    }

    onDeselectAll() {
        this.user.userGroups = [];
    }

    subscribeToDeleteUser() {
        this.deleteSubscription = this.eventManager.subscribe('userListModification', (response) => this.routeToHome());
    }

    private validateForm() {
        this.submitted = true;
        return (this.tenantUserForm && this.tenantUserForm.form.valid && this.user.langKey &&
            this.domainMatch && (this.isNollySoftAdmin ? this.user.tenantId : this.user.userGroups.length));
    }

    save() {
        if (this.validateForm()) {
            if (this.user.id) {
                this.userManagementService.updateUser(this.isNollySoftAdmin, this.user).subscribe((response) => this.onSaveSuccess(response), (error) => this.onSaveError(error));
            } else {
                this.userManagementService.registerUser(this.isNollySoftAdmin, this.user).subscribe((response) => this.onSaveSuccess(response), (error) => this.onSaveError(error));
            }
        }
    }

    routeToHome() {
        this.eventManager.destroy(this.deleteSubscription);
        this.router.navigate(['user-management/users']);
    }

    cancel() {
        this.routeToHome();
    }

    isJson(error) {
        try {
            JSON.parse(error);
        } catch (e) {
            return error;
        }
        return JSON.parse(error).message;
    }

    private onSaveSuccess(result) {
        this.eventManager.broadcast({name: 'userListModification', content: 'OK'});

        this.routeToHome();
    }

    private onSaveError(error) {
        this.alertService.error(this.isJson(error._body), null, null);
    }

    ngOnDestroy() {
        if (this.deleteSubscription) {
            this.eventManager.destroy(this.deleteSubscription);
        }
        if (this.queryParamsSubscription) {
            this.queryParamsSubscription.unsubscribe();
        }
        if (this.pathParamsSubscription) {
            this.pathParamsSubscription.unsubscribe();
        }
    }
}
