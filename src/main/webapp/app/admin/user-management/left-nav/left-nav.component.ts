import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'jhi-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit {
    @Input() userType: string;

  constructor() { }

  ngOnInit() {
  }

}
