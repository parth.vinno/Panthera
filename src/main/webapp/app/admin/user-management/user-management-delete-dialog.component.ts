import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Principal, User, UserManagementService, UserGroup } from '../../shared';
import { UserModalService } from './user-modal.service';

@Component({
    selector: 'jhi-user-mgmt-delete-dialog',
    templateUrl: './user-management-delete-dialog.component.html'
})
export class UserMgmtDeleteDialogComponent implements OnInit {

    user: User;
    currentAccount: User;
    isNollySoftAdmin: boolean;

    constructor(
        private userManagementService: UserManagementService,
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.isNollySoftAdmin = this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1;
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    isTenantAdmin(userGroup: UserGroup[]) {
        for ( let i = 0; i < userGroup.length; i++) {
            return userGroup[i].name === 'TENANT_ADMIN_GROUP';
        }
    }

    confirmDelete(login) {
        this.userManagementService.deleteUser(this.isNollySoftAdmin, login, this.isTenantAdmin(this.user.userGroups)).subscribe((response) => {
            this.eventManager.broadcast({ name: 'userListModification',
                content: 'Deleted a user'});
            this.activeModal.dismiss(true);
        });
    }

}

@Component({
    selector: 'jhi-user-delete-dialog',
    template: ''
})
export class UserDeleteDialogComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userModalService: UserModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.userModalService.open(UserMgmtDeleteDialogComponent, params['login']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
