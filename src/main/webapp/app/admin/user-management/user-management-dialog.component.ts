import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserModalService } from './user-modal.service';
import { Principal, JhiLanguageHelper, User, UserGroup, UserService, UserManagementService } from '../../shared';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Tenant} from '../../shared/apis/models/user.model';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'jhi-user-mgmt-dialog',
    templateUrl: './user-management-dialog.component.html',
    styleUrls: ['./user-management-dialog.component.scss']
})
export class UserMgmtDialogComponent implements OnInit {

    user: User;
    tenants: Tenant;
    languages: any[];
    authorities: any[];
    userGroups: UserGroup[];
    isSaving: Boolean;
    currentAccount: User;
    isNollySoftAdmin: boolean;

    myForm: FormGroup;
    disabled = false;
    ShowFilter = false;
    dropdownSettings: any = {};

    constructor(
        private fb: FormBuilder,
        private principal: Principal,
        private userService: UserService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private languageHelper: JhiLanguageHelper,
        private userManagementService: UserManagementService,
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.authorities = [];

        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.isNollySoftAdmin = this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1;
        });

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        Observable.forkJoin(this.userService.getTenants(),
            this.userService.userGroups()).subscribe((results) => {
            const [tenants, userGroups] = results;
            this.tenants = tenants;
            this.userGroups = userGroups;
        });

        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 4,
            allowSearchFilter: this.ShowFilter,
        };
        this.myForm = this.fb.group({
            group: [this.user.userGroups]
        });
    }

    onItemSelect(item: any) {
        if (!this.user.userGroups) {
            this.user.userGroups = [];
        }
        const index = this.userGroups.map(function(x) {return x.id; }).indexOf(item.id);
        this.user.userGroups.push(this.userGroups[index]);
    }

    onDeSelect(item: any) {
        const index = this.userGroups.map(function(x) {return x.id; }).indexOf(item.id);
        this.userGroups.splice(index, 1);
    }

    onSelectAll() {
        this.user.userGroups = this.userGroups;
    }
    onDeselectAll() {
        this.user.userGroups = [];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== null) {
            this.userManagementService.updateUser(this.isNollySoftAdmin, this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            this.userManagementService.registerUser(this.isNollySoftAdmin , this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        }
    }

    private onSaveSuccess(result) {
        this.eventManager.broadcast({ name: 'userListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-user-dialog',
    template: ''
})
export class UserDialogComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userModalService: UserModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['login'] ) {
                this.modalRef = this.userModalService.open(UserMgmtDialogComponent, params['login']);
            } else {
                this.modalRef = this.userModalService.open(UserMgmtDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
