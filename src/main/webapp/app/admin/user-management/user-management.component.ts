import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'jhi-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
    usersType = 'Users';
    isView: boolean;
    isAdd: boolean;
    isEdit: boolean;

    constructor(router: Router) {
        this.isAdd = router.url.includes('create');
        this.isView = router.url.includes('view');
        this.isEdit = router.url.includes('edit');
    }

    ngOnInit() {
    }

}
