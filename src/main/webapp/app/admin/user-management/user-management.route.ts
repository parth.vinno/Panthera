import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { JhiPaginationUtil } from 'ng-jhipster';

import { UserManagementComponent } from './user-management.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserDeleteDialogComponent } from './user-management-delete-dialog.component';
import {UserListComponent} from './user-list/user-list.component';

import { Principal } from '../../shared';
import {CreateAdminComponent} from './create-admin/create-admin.component';

@Injectable()
export class UserResolve implements CanActivate {

    constructor(private principal: Principal) { }

    canActivate() {
        return this.principal.identity().then((account) => this.principal.hasAnyAuthority(['ROLE_ADMIN', 'ROLE_TENANT_ADMIN']));
    }
}

@Injectable()
export class UserResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const userMgmtRoute: Routes = [
    {
        path: 'user-management',
        component: UserManagementComponent,
        resolve: {
            'pagingParams': UserResolvePagingParams
        },
        data: {
            pageTitle: 'userManagement.home.title'
        },
        children: [
            {
                path: 'users',
                component: UserListComponent,
                resolve: {
                    'pagingParams': UserResolvePagingParams
                },
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: 'create',
                component: CreateUserComponent,
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: 'admin/create',
                component: CreateAdminComponent,
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: 'view/:login',
                component: ViewUserComponent,
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: 'edit/:login',
                component: CreateUserComponent,
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: 'admin/edit/:login',
                component: CreateAdminComponent,
                data: {
                    pageTitle: 'userManagement.home.title'
                }
            },
            {
                path: '',
                redirectTo: 'users',
                pathMatch: 'full'
            }
        ]
    }
];

export const userDialogRoute: Routes = [
    {
        path: 'user-management/:login/delete',
        component: UserDeleteDialogComponent,
        outlet: 'popup'
    }
];
