import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { User, UserGroup, UserManagementService, Principal } from '../../../shared';

@Component({
    selector: 'jhi-view-user',
    templateUrl: './view-user.component.html',
    styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit, OnDestroy {

    user: User;
    currentAccount: User;
    private subscription: Subscription;
    isNollySoftAdmin: boolean;
    isAdminUser;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private principal: Principal,
        private userManagementService: UserManagementService,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.userManagementService.getUserDetail(params['login']).subscribe((user) => {
                this.user = user.json;
                this.isAdminUser = this.isAdmin(this.user.userGroups);
            });
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.isNollySoftAdmin = this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1;
        });
    }

    isAdmin(userGroup: UserGroup[]) {
        for ( let i = 0; i < userGroup.length; i++) {
            if (userGroup[i].name === 'ADMIN_GROUP') {
                return true;
            }
        }
    }

    cancel() {
        this.router.navigate(['user-management/users']);
    }

    edit(user) {
        this.router.navigate([this.isAdminUser ? '/user-management/admin/edit/' + user.login :
        '/user-management/edit/' + user.login]);

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
