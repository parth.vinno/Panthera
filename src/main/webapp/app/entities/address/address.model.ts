import { BaseEntity } from './../../shared';

export class Address implements BaseEntity {
    constructor(
        public id?: number,
        public street1?: string,
        public street2?: string,
        public city?: string,
        public postalCode?: string,
        public state?: string,
        public province?: string,
        public country?: string,
        public url?: string,
    ) {
    }
}
