import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AccountService, SelfAssessmentService, QAReviewDetails, REVIEW_STATUS, StorageService, ResponseWrapper } from '../../shared/';
import { AssessmentHomeService } from './assessment-home.service';
import { ExSTATUS, EXECUTIVESTATUS, ASSESSMENT_INFO_STATUS } from './assessment-home.model';

@Component({
    selector: 'jhi-assessment-home.',
    templateUrl: './assessment-home.component.html',
    styleUrls: [ 'action.scss', 'assessment-home.component.scss' ]
})
export class AssessmentHomeComponent implements OnInit {

    reviewDetails: QAReviewDetails;
    summaryStatus: any;
    merchantSummaryStatus: any;
    assessmentInfoStatus: any;
    isQAUser: boolean;
    STATUS;
    ExSTATUS;
    EXECUTIVESTATUS;
    ASSESSMENT_INFO_STATUS;
    tenantId: any;
    exSummaryRoute: any;
    infoRoute: any;
    isAdmin: boolean;

    constructor(private selfAssessmentService: SelfAssessmentService,
            private router: Router,
            private accountService: AccountService,
            private _storageService: StorageService,
            private _ngZone: NgZone,
            private _assessmentHomeService: AssessmentHomeService) {
        this.tenantId = this._storageService.getTenantId();
        this.isAdmin = !this.tenantId;
        this.isQAUser = false;
        this.STATUS = REVIEW_STATUS;
        this.ExSTATUS = ExSTATUS;
        this.EXECUTIVESTATUS = EXECUTIVESTATUS;
        this.ASSESSMENT_INFO_STATUS = ASSESSMENT_INFO_STATUS;
        this._ngZone.run(() => {
            this._assessmentHomeService.query().subscribe(
                    (res: ResponseWrapper) => {
                        this.summaryStatus = res.json;
                        if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.SELECTED_SERVICES) {
                            this.exSummaryRoute = 'scope-verification-continue';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.NOT_SELECTED_SERVICES) {
                            this.exSummaryRoute = 'payment-card-business';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.CARD_DESCRIPTION) {
                            this.exSummaryRoute = 'locations';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.LOCATIONS) {
                            this.exSummaryRoute = 'payment-applications';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.PAYMENT_APPLICATIONS) {
                            this.exSummaryRoute = 'environment';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.ENV_DESCRIPTION) {
                            this.exSummaryRoute = 'third-party-service-providers';
                        } else if (this.summaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.SERVICE_PROVIDERS) {
                            this.exSummaryRoute = 'summary-of-requirements-tested';
                        }
                    },
                    (res: ResponseWrapper) => { console.log(res.json); });
            this._assessmentHomeService.queryMerchant().subscribe(
                    (res: ResponseWrapper) => {
                        this.merchantSummaryStatus = res.json;
                        if (this.merchantSummaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.MERCHANT_BUSINESS) {
                            this.exSummaryRoute = 'payment-card-business';
                        } else if (this.merchantSummaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.CARD_DESCRIPTION) {
                            this.exSummaryRoute = 'locations';
                        } else if (this.merchantSummaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.LOCATIONS) {
                            this.exSummaryRoute = 'payment-applications';
                        } else if (this.merchantSummaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.PAYMENT_APPLICATIONS) {
                            this.exSummaryRoute = 'environment';
                        } else if (this.merchantSummaryStatus.lastUpdatedSection === this.EXECUTIVESTATUS.ENV_DESCRIPTION) {
                            this.exSummaryRoute = 'third-party-service-providers';
                        }
                    },
                    (res: ResponseWrapper) => { console.log(res.json); });
            this._assessmentHomeService.queryInfo().subscribe(
                    (res: ResponseWrapper) => {
                        this.assessmentInfoStatus = res.json;
                        if (this.assessmentInfoStatus.lastUpdatedSectionEnum === this.ASSESSMENT_INFO_STATUS.ORGANIZATION) {
                            this.infoRoute = 'assessor-company';
                        }
                    },
                    (res: ResponseWrapper) => { console.log(res.json); });
        });
        Observable.forkJoin(accountService.get(),
                selfAssessmentService.getNotificationDetails()).subscribe((results) => {
                    const [account, reviewDetails] = results;
                    this.isQAUser = account.authorities.indexOf('ROLE_QA') !== -1;
                    this.reviewDetails = reviewDetails;
                });
    }

    ngOnInit() {
    }
}
