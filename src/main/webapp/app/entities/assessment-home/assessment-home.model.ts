import { BaseEntity } from './../../shared';

export class ExecutiveSummaryStatus implements BaseEntity {
    constructor(
        public id?: number,
        public section?: BaseEntity,
        public lastUpdatedAt?: BaseEntity
    ) {
    }
}

export enum ExSTATUS {
    START = <any>'START',
    RESUME= <any>'RESUME',
    REVISIT= <any>'REVISIT'
}

export enum EXECUTIVESTATUS {
    SELECTED_SERVICES = <any>'SELECTED_SERVICES',
    NOT_SELECTED_SERVICES = <any>'NOT_SELECTED_SERVICES',
    MERCHANT_BUSINESS = <any>'MERCHANT_BUSINESS',
    CARD_DESCRIPTION = <any>'CARD_DESCRIPTION',
    LOCATIONS = <any>'LOCATIONS',
    PAYMENT_APPLICATIONS = <any>'PAYMENT_APPLICATIONS',
    ENV_DESCRIPTION = <any>'ENV_DESCRIPTION',
    SERVICE_PROVIDERS = <any>'SERVICE_PROVIDERS',
    TESTED_REQUIREMENTS = <any>'TESTED_REQUIREMENTS',
    SUMMARY = <any>'SUMMARY',
}

export enum ASSESSMENT_INFO_STATUS {
    ORGANIZATION = <any>'ORGANIZATION',
    ASSESSOR = <any>'ASSESSOR',
    SUMMARY = <any>'SUMMARY'
}

export class AssessmentInfoStatus implements BaseEntity {
    constructor(
        public id?: number,
        public section?: BaseEntity,
        public lastUpdatedAt?: BaseEntity
    ) {
    }
}
