import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { PcidssSharedModule } from '../../shared';
import { AssessmentHomeComponent, AssessmentHomeService} from './';
import { assessmentHomeRoute } from './assessment-home.route';

const ENTITY_STATES = [
    ...assessmentHomeRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AssessmentHomeComponent
    ],
    entryComponents: [
        AssessmentHomeComponent
    ],
    providers: [
        AssessmentHomeService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssAssessmentHomeModule {}
