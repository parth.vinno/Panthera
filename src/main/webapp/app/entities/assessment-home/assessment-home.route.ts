import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AssessmentHomeComponent } from './assessment-home.component';

export const assessmentHomeRoute: Routes = [
    {
        path: 'saq/assessment-home',
        component: AssessmentHomeComponent,
        data: {
            pageTitle: 'pcidssApp.assessmentHome.mainTitle'
        },
        canActivate: [UserRouteAccessService]
    }
];
