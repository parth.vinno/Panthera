import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ExecutiveSummaryStatus, AssessmentInfoStatus } from './assessment-home.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AssessmentHomeService {

    private resourceUrlSp = 'api/executive-summary-statuses/status/service-provider';
    private resourceUrlMerchant = 'api/executive-summary-statuses/status';
    private resourceUrlInfo = 'api/organization-summary-status';

    constructor(private http: Http) { }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSp, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryMerchant(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlMerchant, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryInfo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlInfo, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(executiveSummaryStatus: ExecutiveSummaryStatus): ExecutiveSummaryStatus {
        const copy: ExecutiveSummaryStatus = Object.assign({}, executiveSummaryStatus);
        return copy;
    }
}
