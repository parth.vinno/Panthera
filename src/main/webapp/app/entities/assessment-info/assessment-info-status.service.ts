import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { AssessmentInfoStatus } from './assessment-info.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AssessmentInfoStatusService {

    private resourceUrl = 'api/organization-summary-status';
    private resourceUrlStatus = 'api/organization-summary-statuses';

    constructor(private http: Http) { }

    createAssessorInfo(assessmentInfoStatus: AssessmentInfoStatus, section: string): Observable<AssessmentInfoStatus> {
        return this.http.post(`${this.resourceUrl}/${section}`, assessmentInfoStatus).map((res: Response) => {
              return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryStatus(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlStatus, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(assessmentInfoStatus: AssessmentInfoStatus): AssessmentInfoStatus {
        const copy: AssessmentInfoStatus = Object.assign({}, assessmentInfoStatus);
        return copy;
    }
}
