import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';

import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-assessment-info',
    templateUrl: './assessment-info.component.html'
})
export class AssessmentInfoComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}
