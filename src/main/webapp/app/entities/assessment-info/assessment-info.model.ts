import { BaseEntity } from './../../shared';

export class Organization implements BaseEntity {
    constructor(
        public id?: number,
        public companyName?: string,
        public dba?: string,
        public contactName?: string,
        public title?: string,
        public telephone?: string,
        public email?: string,
        public url?: string,
        public address?: BaseEntity,
    ) {
    }
}

export class Address implements BaseEntity {
    constructor(
        public id?: number,
        public street1?: string,
        public street2?: string,
        public city?: string,
        public postalCode?: string,
        public state?: string,
        public province?: string,
        public country?: string
    ) {
    }
}

export class AssessorCompany implements BaseEntity {
    constructor(
        public id?: number,
        public companyName?: string,
        public leadQsaContactName?: string,
        public title?: string,
        public telephone?: string,
        public email?: string,
        public address?: BaseEntity,
    ) {
    }
}

export enum ASSESSMENTINFOSTATUS {
    ORGANIZATION = <any>'ORGANIZATION',
    ASSESSOR = <any>'ASSESSOR',
    SUMMARY = <any>'SUMMARY'
}

export class AssessmentInfoStatus implements BaseEntity {
    constructor(
        public id?: number,
        public section?: BaseEntity,
        public lastUpdatedAt?: BaseEntity
    ) {
    }
}
