import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { StoreModule } from '@ngrx/store';

import { PcidssSharedModule } from '../../shared';
import { DisplayOrganizationErrorsComponent } from '../form-validation-error-display/display-organization-error.component';
import { DisplayAssessorCompanyErrorsComponent } from '../form-validation-error-display/display-assessor-company-error.component';

import {
    AssessmentInfoComponent,
    OrganizationService,
    OrganizationComponent,
    assessmentInfoRoute,
    LeftNavComponent,
    AssessorCompanyComponent,
    AssessorCompanyService,
    SummaryComponent,
    SummaryService,
    AssessmentInfoStatusService
} from './';

const ENTITY_STATES = [
    ...assessmentInfoRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        BsDropdownModule,
        Ng2TelInputModule
    ],
    declarations: [
    AssessmentInfoComponent,
        OrganizationComponent,
        LeftNavComponent,
        AssessorCompanyComponent,
        SummaryComponent,
        DisplayOrganizationErrorsComponent,
        DisplayAssessorCompanyErrorsComponent
    ],
    entryComponents: [
        AssessmentInfoComponent
    ],
    providers: [
        OrganizationService,
        AssessorCompanyService,
        SummaryService,
        AssessmentInfoStatusService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssAssessmentInfoModule {}
