import {Routes} from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AssessmentInfoComponent } from './assessment-info.component';

import { OrganizationComponent } from './organization/organization.component';
import { AssessorCompanyComponent } from './assessor-company/assessor-company.component';
import { SummaryComponent } from './summary/summary.component';

export const assessmentInfoRoute: Routes = [
    {
        path: 'saq/assessment-info',
        component: AssessmentInfoComponent,
        data: {
            pageTitle: 'pcidssApp.assessmentInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
                   {
                       path: 'organization',
                       component: OrganizationComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'assessor-company',
                       component: AssessorCompanyComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'summary',
                       component: SummaryComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: '',
                       redirectTo: 'saq/assessment-info',
                       pathMatch: 'full'
                   }
        ]
    }
];
