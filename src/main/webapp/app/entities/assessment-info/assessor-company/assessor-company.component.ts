import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { AssessorCompany, AssessmentInfoStatus } from '../assessment-info.model';
import { AssessorCompanyService } from './assessor-company.service';
import { Country, CountryService } from '../../country';
import { State, StateService } from '../../state';
import { Address, AddressService } from '../../address';
import { ResponseWrapper, StorageService } from '../../../shared';
import { FormGroup, FormsModule, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CustomValidators } from './validators/custom-validators';
import { AssessmentInfoStatusService } from '../assessment-info-status.service';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-assessor-company',
    templateUrl: './assessor-company.component.html',
    styleUrls: [
                '../assessment-info.component.scss', '../fa-arrows.scss'
            ]
})
export class AssessorCompanyComponent implements OnInit, OnDestroy {

    assessorCompanies: AssessorCompany;
    authorities: any[];
    isSaving: boolean;
    addresses: Address[];
    states: State[];
    statesBasedOnCountry: State[];
    countries: Country[];
    selectedCountry: string;
    isStateOrProvince = true;
    isUSOrCanada = true;
    phoneObj: any;
    isMerchantSp: any;
    tenantId: any;
    assessorCompanyForm: FormGroup;
    infoStatus: any;

    constructor(
        private alertService: JhiAlertService,
        private _assessorCompanyService: AssessorCompanyService,
        private _addressService: AddressService,
        private _countryService: CountryService,
        private _stateService: StateService,
        private router: Router,
        private eventManager: JhiEventManager,
        private _fb: FormBuilder,
        private _ngZone: NgZone,
        private _bsModalService: BsModalService,
        private _storageService: StorageService,
        private _toastr: ToastrService,
        private _assessmentInfoStatusService: AssessmentInfoStatusService,
    ) {
        this.assessorCompanies = new AssessorCompany();
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        if (this.tenantId === 'nollysoft_serviceprovider_saqd') {
            this.isMerchantSp = 'Service Provider';
        } else if (this.tenantId === 'nollysoft_merchant_saqd') {
            this.isMerchantSp = 'Merchant';
        }
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.assessorCompanyForm = this._fb.group({
                id: null,
                companyName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(255), CustomValidators.companyName]],
                leadQsaContactName: [null, [Validators.minLength(2), Validators.maxLength(255), CustomValidators.qsaCompanyName]],
                title: [null, [Validators.minLength(2), Validators.maxLength(255), CustomValidators.title]],
                telephone: [null, [Validators.required, CustomValidators.telephone]],
                email: [null, [Validators.required, CustomValidators.email]],
                url: [null, [Validators.required, CustomValidators.url]],
                address: this._fb.group({
                    id: null,
                    street1: [null, [Validators.required, CustomValidators.street1]],
                    street2: [null],
                    city: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.city]],
                    postalCode: [null, [Validators.required, CustomValidators.postalCodeUS]],
                    state: [null],
                    province: [null],
                    country: ['US']
                })
        });
        this._ngZone.run(() => {
            this._countryService.query().subscribe(
                    (res: ResponseWrapper) => {
                        this.countries = res.json;
                        this._stateService.query().subscribe(
                                (res1: ResponseWrapper) => {
                                    this.states = res1.json;
                                    this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === 'US');
                                    this._assessorCompanyService.query().subscribe((res2: ResponseWrapper) => {
                                        const assessorInfo = res2.json;
                                        if (assessorInfo.length) {
                                            this.assessorCompanyForm.patchValue({
                                                id: assessorInfo[0].id,
                                                companyName: assessorInfo[0].companyName,
                                                leadQsaContactName: assessorInfo[0].leadQsaContactName,
                                                title: assessorInfo[0].title,
                                                telephone: assessorInfo[0].telephone,
                                                email: assessorInfo[0].email,
                                                url: assessorInfo[0].url
                                            });
                                            this.assessorCompanyForm.controls['address'].patchValue({
                                                id: assessorInfo[0].address.id,
                                                street1: assessorInfo[0].address.street1,
                                                street2: assessorInfo[0].address.street2,
                                                city: assessorInfo[0].address.city,
                                                postalCode: assessorInfo[0].address.postalCode,
                                                state: assessorInfo[0].address.state,
                                                province: assessorInfo[0].address.province,
                                                country: assessorInfo[0].address.country
                                            });
                                            this.onValueExist();
                                        }
                                    }, (res2: ResponseWrapper) => {
                                        this._toastr.error(`Error saving assessor company information: ${res2.json.message}`, `Error`);
                                    });
                                },
                                (res1: ResponseWrapper) => this.onError(res1.json));
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._assessmentInfoStatusService.query().subscribe(
                    (res: ResponseWrapper) => { this.infoStatus = res.json; },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    telInputObject(obj) {
        this.phoneObj = obj;
    }

    onSelectCountry(obj) {
        const countryCode = obj.iso2
        if ((obj.iso2).toUpperCase() !== this.assessorCompanyForm.get('address.country').value) {
            this.assessorCompanyForm.patchValue({
                telephone: null
            });
            this.assessorCompanyForm.get('address').patchValue({
                postalCode: null,
                state: null,
                province: null
            });
        }
        this.assessorCompanyForm.controls['address'].patchValue({
            country: countryCode.toUpperCase()
        })
        if (this.assessorCompanyForm.get('address.country').value === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.assessorCompanyForm.get('address.country').value);
            this.assessorCompanyForm.get('address.postalCode').setValidators([Validators.required, CustomValidators.postalCodeUS]);
            this.assessorCompanyForm.get('address').patchValue({
                province: null
              });
        } else if (this.assessorCompanyForm.get('address.country').value === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.assessorCompanyForm.get('address.country').value);
            this.assessorCompanyForm.get('address.postalCode').setValidators([CustomValidators.postalCodeCA]);
            this.assessorCompanyForm.get('address').patchValue({
                province: null
              });
        } else {
            this.isStateOrProvince = false;
            this.assessorCompanyForm.get('address.postalCode').setValidators([]);
        }
        this.assessorCompanyForm.get('address.postalCode').updateValueAndValidity();
    }

    onValueExist() {
        const existedCountry = this.assessorCompanyForm.get('address.country').value;
        this.phoneObj.intlTelInput('setCountry', existedCountry);
        if (existedCountry === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === existedCountry);
        } else if (existedCountry === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === existedCountry);
        } else {
            this.isStateOrProvince = false;
        }
    }

    onCountryChange() {
        this.assessorCompanyForm.patchValue({
            telephone: null
          });
        this.selectedCountry = this.assessorCompanyForm.get('address.country').value;
        this.phoneObj.intlTelInput('setCountry', this.selectedCountry);
        if (this.selectedCountry === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.selectedCountry);
            this.assessorCompanyForm.get('address.postalCode').setValidators([Validators.required, CustomValidators.postalCodeUS]);
            this.assessorCompanyForm.get('address').patchValue({
                province: null,
                postalCode: ''
              });
        } else if (this.selectedCountry === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.selectedCountry);
            this.assessorCompanyForm.get('address.postalCode').setValidators([CustomValidators.postalCodeCA]);
            this.assessorCompanyForm.get('address').patchValue({
                province: null,
                postalCode: '',
              });
        } else {
            this.isStateOrProvince = false;
            this.assessorCompanyForm.get('address.postalCode').setValidators([]);
            this.assessorCompanyForm.get('address').patchValue({
                postalCode: '',
                state: null,
                province: null
              });
        }
        this.assessorCompanyForm.get('address.postalCode').updateValueAndValidity();
    }

    saveAssessorCompany() {
        if (this.assessorCompanyForm.value.id !== undefined || this.assessorCompanyForm.value.id !== null) {
            return new Promise((resolve, reject) => {
               this._assessorCompanyService.update(this.assessorCompanyForm.value).subscribe(
                       (assessor) => {
                           resolve(assessor);
                       },
                       (error) => {
                           reject(error);
                       });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._assessorCompanyService.create(this.assessorCompanyForm.value).subscribe(
                        (assessor) => {
                            resolve(assessor);
                        },
                        (error) => {
                            reject(error);
                        });
             });
        }
    }

    routeToNext() {
        this.router.navigate(['/saq/assessment-info/summary']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/assessment-info/organization']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/assessment-info/summary']);
    }

    saveAndContinue() {
        this.saveAssessorCompany().then((assessor) => {
            if (this.infoStatus.statusEnum === 'RESUME') {
                this.routeToNext();
                this.subscribeToSaveResponse(this._assessmentInfoStatusService.createAssessorInfo({}, 'SUMMARY'));
                this._toastr.success(`The information has been successfully saved`, `Save`);
            } else if (this.infoStatus.statusEnum === 'REVISIT') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            }
        }).catch((error) => {
            this._toastr.error(`Error saving assessor company information: ${error.message}`, `Error`);
        });
    }

    private subscribeToSaveResponse(result: Observable<AssessmentInfoStatus>) {
        result.subscribe((res: AssessmentInfoStatus) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AssessmentInfoStatus) {
        this.eventManager.broadcast({ name: 'AssessmentInfoStatus', content: 'OK'});
    }

    private onSaveError(error) {
        try {
            error.json();
            } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    saveAndExit() {
        this.saveAssessorCompany().then((assessor) => {
            this._toastr.success(`The assessor company information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving assessor company information: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.saveAssessorCompany().then(
                (assessor) => {
                    this._toastr.success(`The assessor company information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving assessor company information: ${error.message}`, `Error`);
                });
    }

    back() {
        if (this.assessorCompanyForm && this.assessorCompanyForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                }
            }
        });
    }

    ngOnDestroy() {
    }
}
