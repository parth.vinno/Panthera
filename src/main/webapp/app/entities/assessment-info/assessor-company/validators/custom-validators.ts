import { FormArray, FormControl, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
    static companyName(c: FormControl): ValidationErrors {
        const isValidCompanyName = /^[a-zA-Z0-9& ]*$/.test(c.value);
        const message = {
                'companyName': {
                    'message': 'The company name must be valid.'
                }
        };
        return isValidCompanyName ? null : message;
    }

    static qsaCompanyName(c: FormControl): ValidationErrors {
        const isValidQsaContactName =  /^[a-zA-Z0-9 ]*$/.test(c.value);
        const message = {
                'leadQsaContactName': {
                    'message': 'The lead QSA contact name must be valid.'
                }
        };
        return isValidQsaContactName ? null : message;
    }

    static title(c: FormControl): ValidationErrors {
        const isValidTitle = /^[a-zA-Z ]*$/.test(c.value);
        const message = {
                'title': {
                    'message': 'The title must be valid.'
                }
        };
        return isValidTitle ? null : message;
    }

    static telephone(c: FormControl): ValidationErrors {
        const isValidPhoneNumber = /^[0-9-\s\(\) ]{0,20}$/.test(c.value);
        const message = {
                'telephone': {
                    'message': 'Invalid telephone number'
                }
        };
        return isValidPhoneNumber ? null : message;
    }

    static email(c: FormControl): ValidationErrors {
        const isValidEmail =
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            .test(c.value);
        const message = {
                'email': {
                    'message': 'The email must be valid.'
                }
        };
        return isValidEmail ? null : message;
    }

    static postalCodeUS(c: FormControl): ValidationErrors {
        const isValidPostalCode = /^\d{5}(-\d{4})?$/.test(c.value);
        const message = {
                'postalCode': {
                    'message': 'The postal code for United States must be valid (e.g., "95605-0277" or "95605").'
                }
        };
        return isValidPostalCode ? null : message;
    }

    static postalCodeCA(c: FormControl): ValidationErrors {
        const isValidPostalCode = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/.test(c.value);
        const message = {
                'postalCode': {
                    'message': 'The postal code for Canada must be valid (e.g., T2X 1V4" or "T2X1V4).'
                }
        };
        return isValidPostalCode ? null : message;
    }

    static city(c: FormControl): ValidationErrors {
        const isValidCity = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/.test(c.value);
        const message = {
                'city': {
                    'message': 'The city must be valid.'
                }
        };
        return isValidCity ? null : message;
    }

    static street1(c: FormControl): ValidationErrors {
        const isValidStreet1 = /^[a-zA-Z0-9\s,#'-]*$/.test(c.value);
        const message = {
                'street1': {
                    'message': 'The street must be valid.'
                }
        };
        return isValidStreet1 ? null : message;
    }

    static url(c: FormControl): ValidationErrors {
        const isValidUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/.test(c.value);
        const message = {
                'url': {
                    'message': 'The url must be valid "(e.g., http://www.example.net)".'
                }
        };
        return isValidUrl ? null : message;
    }
}
