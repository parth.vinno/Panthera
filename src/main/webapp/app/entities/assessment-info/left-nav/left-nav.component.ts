import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { ASSESSOR } from '../../executive-summary/ex-summary-reducer.service';

import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { AssessmentInfoStatusService } from '../assessment-info-status.service';

@Component({
  selector: 'jhi-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit {
    tenantId: any;
    infoStatus: any = '';
    state: Observable<string>;
    curStates: any = [];
    stateSubscription

    constructor(private _storageService: StorageService,
            private _ngZone: NgZone,
            private store: Store<string>,
            private _assessmentInfoStatusService: AssessmentInfoStatusService) {
        this.state = store.select('state');
        this.state.subscribe(
            (curStates) => {
                if (curStates === 'ASSESSOR') {
                    this.curStates = curStates;
                }
            })
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this._ngZone.run(() => {
            this._assessmentInfoStatusService.query().subscribe((res: ResponseWrapper) => {
                this.infoStatus = res.json;
                if (this.infoStatus.lastUpdatedSectionEnum === 'ORGANIZATION') {
                    this.store.dispatch({type: ASSESSOR});
                }
            },
            (res: ResponseWrapper) => {
                console.log(res.json);
            })
        });
    }
}
