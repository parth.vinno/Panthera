import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Store } from '@ngrx/store';
import { ASSESSOR } from '../../executive-summary/ex-summary-reducer.service';

import { Organization } from '../assessment-info.model';
import { OrganizationService } from './organization.service';
import { AssessmentInfoStatusService } from '../assessment-info-status.service';
import { Address, AddressService } from '../../address';
import { Country, CountryService } from '../../country';
import { State, StateService } from '../../state';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { DIRECTION, YesNoModalComponent } from '../../../shared';
import { FormGroup, FormsModule, FormBuilder, ReactiveFormsModule, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from './validators/custom-validators';

@Component({
    selector: 'jhi-organization',
    templateUrl: './organization.component.html',
    styleUrls: [
                '../assessment-info.component.scss', '../fa-arrows.scss'
            ]
})
export class OrganizationComponent implements OnInit, OnDestroy {

    organization: Organization;
    authorities: any[];
    isSaving: boolean;
    addresses: Address[];
    states: State[];
    statesBasedOnCountry: State[];
    countries: Country[];
    selectedCountry: string;
    isStateOrProvince = true;
    postalCodeCountry: string;
    isUSOrCanada = true;
    organizationForm: FormGroup;
    obj: any;
    isMerchantSp: any;
    tenantId: any;
    state: Observable<string>;
    curStates: string;
    stateSubscription;
    infoStatus: any = '';

    constructor(
        private alertService: JhiAlertService,
        private _organizationService: OrganizationService,
        private addressService: AddressService,
        private countryService: CountryService,
        private stateService: StateService,
        private router: Router,
        private eventManager: JhiEventManager,
        private _fb: FormBuilder,
        private _ngZone: NgZone,
        private _bsModalService: BsModalService,
        private _storageService: StorageService,
        private _toastr: ToastrService,
        private store: Store<string>,
        private _assessmentInfoStatusService: AssessmentInfoStatusService
    ) {
        this.organization = new Organization();
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        if (this.tenantId === 'nollysoft_serviceprovider_saqd') {
            this.isMerchantSp = 'Service Provider';
        } else if (this.tenantId === 'nollysoft_merchant_saqd') {
            this.isMerchantSp = 'Merchant';
        }
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.organizationForm = this._fb.group({
            id: null,
            companyName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(255), CustomValidators.companyName]],
            contactName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(255), CustomValidators.contactName]],
            dba: [null, [Validators.minLength(2), Validators.maxLength(255), CustomValidators.dba]],
            title: [null, [Validators.minLength(2), Validators.maxLength(255), CustomValidators.title]],
            telephone: [null, [Validators.required, CustomValidators.telephone]],
            email: [null, [Validators.required, CustomValidators.email]],
            url: [null, [Validators.required, CustomValidators.url]],
            address: this._fb.group({
                id: null,
                street1: [null, [Validators.required, CustomValidators.street1]],
                street2: [null],
                city: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.city]],
                postalCode: [null, [Validators.required, CustomValidators.postalCodeUS]],
                state: [null],
                province: [null],
                country: ['US']
            })
        });
        this._ngZone.run(() => {
            this.countryService.query().subscribe(
                    (res: ResponseWrapper) => {
                        this.countries = res.json;
                        this.stateService.query().subscribe(
                                (res1: ResponseWrapper) => {
                                    this.states = res1.json;
                                    this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === 'US');
                                    this._organizationService.query().subscribe((res2: ResponseWrapper) => {
                                        const orgInfo = res2.json;
                                        if (orgInfo.length) {
                                            this.organizationForm.patchValue({
                                                id: orgInfo[0].id,
                                                companyName: orgInfo[0].companyName,
                                                contactName: orgInfo[0].contactName,
                                                dba: orgInfo[0].dba,
                                                title: orgInfo[0].title,
                                                telephone: orgInfo[0].telephone,
                                                email: orgInfo[0].email,
                                                url: orgInfo[0].url
                                            });
                                            this.organizationForm.controls['address'].patchValue({
                                                id: orgInfo[0].address.id,
                                                street1: orgInfo[0].address.street1,
                                                street2: orgInfo[0].address.street2,
                                                city: orgInfo[0].address.city,
                                                postalCode: orgInfo[0].address.postalCode,
                                                state: orgInfo[0].address.state,
                                                province: orgInfo[0].address.province,
                                                country: orgInfo[0].address.country
                                            });
                                            this.onValueExist();
                                        }
                                    }, (res2: ResponseWrapper) =>
                                    this.onError(res2.json));
                                },
                                (res1: ResponseWrapper) => this.onError(res1.json));
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._assessmentInfoStatusService.query().subscribe(
                    (res: ResponseWrapper) => { this.infoStatus = res.json; },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    onValueExist() {
        const existedCountry = this.organizationForm.get('address.country').value;
        this.obj.intlTelInput('setCountry', existedCountry);
        if (existedCountry === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === existedCountry);
        } else if (existedCountry === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === existedCountry);
        } else {
            this.isStateOrProvince = false;
        }
    }

    telInputObject(obj) {
        this.obj = obj;
    }

    onSelectCountry(obj) {
        const countryCode = obj.iso2
        if ((obj.iso2).toUpperCase() !== this.organizationForm.get('address.country').value) {
            this.organizationForm.patchValue({
                telephone: null
            });
            this.organizationForm.get('address').patchValue({
                postalCode: null,
                state: null,
                province: null
            });
        }
        this.organizationForm.controls['address'].patchValue({
            country: countryCode.toUpperCase()
        })
        if (this.organizationForm.get('address.country').value === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.organizationForm.get('address.country').value);
            this.organizationForm.get('address.postalCode').setValidators([Validators.required, CustomValidators.postalCodeUS]);
            this.organizationForm.get('address').patchValue({
                province: null,
                postalCode: ''
              });
        } else if (this.organizationForm.get('address.country').value === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.organizationForm.get('address.country').value);
            this.organizationForm.get('address.postalCode').setValidators([CustomValidators.postalCodeCA]);
            this.organizationForm.get('address').patchValue({
                province: null,
                postalCode: ''
              });
        } else {
            this.isStateOrProvince = false;
            this.organizationForm.get('address.postalCode').setValidators([]);
        }
        this.organizationForm.get('address.postalCode').updateValueAndValidity();
    }

    onCountryChanges() {
        this.organizationForm.patchValue({
            telephone: null
          });
        this.selectedCountry = this.organizationForm.get('address.country').value;
        this.obj.intlTelInput('setCountry', this.selectedCountry);
        if (this.selectedCountry === 'US') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = true;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.selectedCountry);
            this.organizationForm.get('address.postalCode').setValidators([Validators.required, CustomValidators.postalCodeUS]);
            this.organizationForm.get('address').patchValue({
                province: null
              });
        } else if (this.selectedCountry === 'CA') {
            this.isStateOrProvince = true;
            this.isUSOrCanada = false;
            this.statesBasedOnCountry = this.states.filter((state) => state.countryCode === this.selectedCountry);
            this.organizationForm.get('address.postalCode').setValidators([CustomValidators.postalCodeCA]);
            this.organizationForm.get('address').patchValue({
                province: null
            });
        } else {
            this.isStateOrProvince = false;
            this.organizationForm.get('address.postalCode').setValidators([]);
            this.organizationForm.get('address').patchValue({
                postalCode: '',
                state: null,
                province: null
              });
        }
        this.organizationForm.get('address.postalCode').updateValueAndValidity();
    }

    saveOrganization() {
        if (this.organizationForm.value.id !== undefined || this.organizationForm.value.id !== null) {
            return new Promise((resolve, reject) => {
               this._organizationService.update(this.organizationForm.value).subscribe(
                       (org) => {
                           resolve(org);
                       },
                       (error) => {
                           reject(error);
                       });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._organizationService.create(this.organizationForm.value).subscribe(
                        (org) => {
                            resolve(org);
                        },
                        (error) => {
                            reject(error);
                        });
             });
        }
    }

    routeToNext() {
        this.router.navigate(['/saq/assessment-info/assessor-company']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/assessment-info/summary']);
    }

    saveAndContinue() {
        this.saveOrganization().then((org) => {
            if (this.infoStatus.statusEnum === 'RESUME' || this.infoStatus.statusEnum === 'START') {
                this.routeToNext();
                this.store.dispatch({type: ASSESSOR});
                this._toastr.success(`The information has been successfully saved`, `Save`);
            } else if (this.infoStatus.statusEnum === 'REVISIT') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            }
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndExit() {
        this.saveOrganization().then((org) => {
            this._toastr.success(`The organization information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving organization information: ${error.message}`, `Error`);
        });
    }

    forward() {
        if (this.organizationForm && this.organizationForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.saveAndContinue();
            } else if (result === false) {
                this.routeToNext();
            }
        });
    }

    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
