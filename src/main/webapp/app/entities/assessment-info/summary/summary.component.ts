import { Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { NgForm } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

// import { ORGANIZATION, ASSESSOR, INFO_SUMMARY } from '../assessment-info-reducer.service';
import { SummaryService } from './summary.service';
import { Organization, AssessorCompany} from '../assessment-info.model';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';

import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-org-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss', '../fa-arrows.scss']
})
export class SummaryComponent implements OnInit, OnDestroy {

    organization: Organization;
    assessorCompany: AssessorCompany;
    orgAdderss: any;
    assessorAdderss: any;
    tenantId: any;
    isMerchantSp: any;
    state: Observable<string>;
    curState: string;
    stateSubscription;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _summaryService: SummaryService,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService,
            private _storageService: StorageService,
            private store: Store<string>) {
            this.organization = new Organization();
            this.assessorCompany = new AssessorCompany();
//            this.state = store.select('state');
//            this.stateSubscription = this.state.subscribe(
//                (curState) => {
//                    this.curState = curState;
//                });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this.tenantId = this._storageService.getTenantId();
        if (this.tenantId === 'nollysoft_serviceprovider_saqd') {
            this.isMerchantSp = 'Service Provider';
        } else if (this.tenantId === 'nollysoft_merchant_saqd') {
            this.isMerchantSp = 'Merchant';
        }
        this._ngZone.run(() => {
            this._summaryService.queryOrganization().subscribe(
                    (res: ResponseWrapper) => {
                        const organization = res.json;
                        let stateOrProvince: any;
                        let street: any;
                        if (organization.length > 0) {
                            this.organization = organization[0];
                            if (organization[0].address.state !== null) {
                                stateOrProvince = organization[0].address.state;
                            } else if (organization[0].address.province !== null) {
                                stateOrProvince = organization[0].address.province;
                            }
                            if (organization[0].address.street2 === null) {
                                street = '';
                            } else {
                                street = organization[0].address.street2;
                            }
                            this.orgAdderss = `${organization[0].address.street1} ${street}
                            ${organization[0].address.city} ${stateOrProvince} ${organization[0].address.country}
                            ${organization[0].address.postalCode}`
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryAssessorCompany().subscribe(
                    (res: ResponseWrapper) => {
                        const assessorCompany = res.json
                        let stateOrProvince: any;
                        let street: any;
                        if (assessorCompany.length > 0) {
                            this.assessorCompany = assessorCompany[0];
                            if (assessorCompany[0].address.state !== null) {
                                stateOrProvince = assessorCompany[0].address.state;
                            } else if (assessorCompany[0].address.province !== null) {
                                stateOrProvince = assessorCompany[0].address.province;
                            }
                            if (assessorCompany[0].address.street2 === null) {
                                street = '';
                            } else {
                                street = assessorCompany[0].address.street2;
                            }
                            this.assessorAdderss = `${assessorCompany[0].address.street1} ${street}
                            ${assessorCompany[0].address.city} ${stateOrProvince} ${assessorCompany[0].address.country}
                            ${assessorCompany[0].address.postalCode}`
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    private onError(error) {
        console.log(error);
    }

    cancel() {
        this.router.navigate(['/saq/assessment-home']);
    }

    submit() {
        this.router.navigate(['/saq/assessment-home']);
    }

    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }

}
