import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Organization, AssessorCompany} from '../assessment-info.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class SummaryService {

    private getOrganizationUrl = 'api/organizations';
    private getAssessorCompanyUrl = 'api/assessor-companies';

    constructor(private http: Http) { }

    queryOrganization(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getOrganizationUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryAssessorCompany(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getAssessorCompanyUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }
}
