import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {
    AcknowledgementStatement, AcknowledgementStatementService, AcknowledgementStatus,
    AcknowledgementStatusService, YesNoModalComponent, DIRECTION
} from '../../../shared';
import {NgForm} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
    selector: 'jhi-acknowledgement-status',
    templateUrl: './acknowledgement-status.component.html',
    styleUrls: ['./acknowledgement-status.component.scss', '../fa-arrows.scss']
})
export class AcknowledgementStatusComponent implements OnInit {
    @ViewChild('ackStatusForm') public ackStatusForm: NgForm;

    statements: Array<AcknowledgementStatement>;
    statuses: Array<AcknowledgementStatus>;

    constructor(private router: Router, private _acknowledgementStatementService: AcknowledgementStatementService,
                private _ngZone: NgZone, private _acknowledgementStatusService: AcknowledgementStatusService,
                private _bsModalService: BsModalService) {
        this.statuses = [];
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this._acknowledgementStatementService.getAcknowledgementStatements().subscribe(
                (res) => {
                    this.statements = res;
                    this._acknowledgementStatusService.getAcknowledgementStatuses().subscribe(
                        (statuses) => {
                            if (statuses.length) {
                                this.constructViewData(statuses);
                            }
                        }, (error) => {
                            console.log(error);
                        }
                    )
                }
            );
        });
    }

    constructViewData(statuses: Array<AcknowledgementStatus>) {
        for (let i = 0, len = this.statements.length; i < len; i++) {
            const foundStatus = statuses.find((x) => x.acknowledgementStatementId.id === this.statements[i].id);
            this.statements[i].status = foundStatus ? foundStatus.statusResponse : false;
        }
    }

    constructServerData() {
        for (let i = 0, len = this.statements.length; i < len; i++) {
            const statement = this.statements[i];
            if (statement.status) {
                this.statuses.push(new AcknowledgementStatus(statement.status, statement));
            }
        }
    }

    selectAll() {
        for (let i = 0, len = this.statements.length; i < len; i++) {
            this.statements[i].status = true;
        }
    }

    saveAcknowledgement() {
        this.constructServerData();
        return new Promise((resolve, reject) => {
            this._acknowledgementStatusService.saveAcknowledgementStatuses(this.statuses).subscribe(
                (resp) => {
                    resolve(resp);
                }, (error) => {
                    reject(error);
                });
        });
    }

    saveAndContinue() {
        this.saveAcknowledgement().then(
            (resp) => {
                this.routeToNext();
            }, (error) => {
                console.log(error);
                alert('Error saving acknowledgements');
            });
    }

    saveAndExit() {
        this.saveAcknowledgement().then(
            (resp) => {
                this.home();
            }, (error) => {
                console.log(error);
                alert('Error saving acknowledgements');
            });
    }

    saveAndPrev() {
        this.saveAcknowledgement().then(
            (resp) => {
                this.routeToPrev();
            }, (error) => {
                console.log(error);
                alert('Error saving Attestation : ' + error.message);
            });
    }

    routeToNext() {
        this.router.navigate(['/attestation/final-attestation']);
    }

    routeToPrev() {
        this.router.navigate(['/attestation/pcidss-validation']);
    }

    home() {
        this.router.navigate(['/saq/assessment-home']);
    }

    back() {
        if (this.ackStatusForm && this.ackStatusForm.form.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.ackStatusForm && this.ackStatusForm.form.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }
}
