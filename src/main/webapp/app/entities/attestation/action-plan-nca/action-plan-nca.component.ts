import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

import {RequirementActionPlan, RequirementsService, PciDssRequirement} from '../../../shared';

@Component({
    selector: 'jhi-action-plan-nca',
    templateUrl: './action-plan-nca.component.html',
    styleUrls: ['./action-plan-nca.component.scss', '../fa-arrows.scss']
})
export class ActionPlanNcaComponent implements OnInit {
    datePicker;
    minDate;
    actionPlans: Array<RequirementActionPlan>;

    constructor(private router: Router, private _requirementsService: RequirementsService,
                private _ngZone: NgZone, private _dateFormatter: NgbDateParserFormatter) {
        // Date has to be the current date or after
        const curDate = new Date();
        this.minDate = {year: curDate.getFullYear(), month: curDate.getMonth() + 1, day: curDate.getDate()};
        this.actionPlans = [];
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this._requirementsService.getRequirementActionPlans().subscribe(
                (plans) => {
                    // Parse dates to the date picker format
                    if (plans && plans.length) {
                        this.parseDates(plans);
                        this.actionPlans = plans;
                    } else {
                        this._requirementsService.getPciDssRequirements().subscribe(
                            (requiremenst) => {
                                this.constructActionPlans(requiremenst);
                            });
                    }
                }, (error) => {
                    console.error(error);
                }
            )
        });
    }

    private constructActionPlans(requirements: Array<PciDssRequirement>) {
        for (let i = 0, len = requirements.length; i < len; i++) {
            this.actionPlans.push(new RequirementActionPlan(requirements[i], true));
        }
    }

    private parseDates(plans: RequirementActionPlan[]) {
        for (let i = 0, len = plans.length; i < len; i++) {
            const plan = plans[i];
            if (plan.remediationDate) {
                plan.dpDate = this._dateFormatter.parse(plan.remediationDate);
            }
        }
    }

    private formatDates(plans: RequirementActionPlan[]) {
        for (let i = 0, len = plans.length; i < len; i++) {
            const plan = plans[i];
            if (plan.dpDate) {
                plan.remediationDate = this._dateFormatter.format(plan.dpDate);
            }
        }
    }

    private saveActionPlans() {
        return new Promise((resolve, reject) => {
            this.formatDates(this.actionPlans);
            this._requirementsService.saveRequirementActionPlans(this.actionPlans).subscribe(
                (plans) => {
                    resolve(plans);
                },
                (error) => {
                    this.parseDates(this.actionPlans);
                    reject(error);
                });
        });
    }

    submit() {
        this.saveActionPlans().then((plans) => {
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            console.log('error');
            alert('Error saving Attestation : ' + error.message);
        });
        this.router.navigate(['/saq/assessment-home']);
    }

    back() {
        this.router.navigate(['/attestation/isa-final-attestation']);
    }
}
