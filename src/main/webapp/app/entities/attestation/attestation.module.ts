import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import {
    AttestationComponent,
    AcknowledgementStatusComponent,
    PcidssValidationComponent,
    LeftNavComponent,
    QsaFinalAttestationComponent,
    IsaFinalAttestationComponent,
    ActionPlanNcaComponent,
    FinalAttestationComponent,
    attestationRoute
} from './';

const ENTITY_STATES = [
    ...attestationRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AttestationComponent,
        AcknowledgementStatusComponent,
        PcidssValidationComponent,
        LeftNavComponent,
        QsaFinalAttestationComponent,
        IsaFinalAttestationComponent,
        ActionPlanNcaComponent,
        FinalAttestationComponent
    ],
    entryComponents: [
        AttestationComponent,
    ],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssAttestationModule {}
