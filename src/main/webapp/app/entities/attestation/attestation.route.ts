import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import {
    AttestationComponent,
    AcknowledgementStatusComponent,
    PcidssValidationComponent,
    QsaFinalAttestationComponent,
    IsaFinalAttestationComponent,
    ActionPlanNcaComponent,
    FinalAttestationComponent
} from './';

export const attestationRoute: Routes = [
    {
        path: 'attestation',
        component: AttestationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.attestation.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: 'pcidss-validation',
                component: PcidssValidationComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: 'acknowledgement-status',
                component: AcknowledgementStatusComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: 'final-attestation',
                component: FinalAttestationComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: 'qsa-final-attestation',
                component: QsaFinalAttestationComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: 'isa-final-attestation',
                component: IsaFinalAttestationComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: 'action-plan-for-nca',
                component: ActionPlanNcaComponent,
                data: {
                    pageTitle: 'pcidssApp.attestation.home.title'
                }
            },
            {
                path: '',
                redirectTo: 'pcidss-validation',
                pathMatch: 'full'
            }
        ]
    }
];
