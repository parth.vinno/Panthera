import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';

import {AttestationService, DIRECTION, FinalAttestation, YesNoModalComponent} from '../../../shared';

@Component({
    selector: 'jhi-final-attestation',
    templateUrl: './final-attestation.component.html',
    styleUrls: ['./final-attestation.component.scss', '../fa-arrows.scss']
})
export class FinalAttestationComponent implements OnInit {
    @ViewChild('finalAttestationForm') public finalAttestationForm: NgForm;

    currentDate;
    finalAttestation: FinalAttestation;
    submitted = false;

    constructor(private router: Router, private _attestationService: AttestationService,
                private _ngZone: NgZone, private _bsModalService: BsModalService) {
        this.currentDate = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        this.finalAttestation = new FinalAttestation();
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this._attestationService.getFinalAttestations().subscribe(
                (attestations) => {
                    if (attestations.length) {
                        this.finalAttestation = attestations[0];
                        this.currentDate = this.finalAttestation.attestationDate.split('T')[0];
                    }
                }, (error) => {
                    console.error(error);
                });
        });
    }

    private saveAttestation() {
        return new Promise((resolve, reject) => {
            this._attestationService.saveFinalAttestation(this.finalAttestation).subscribe(
                (attestation) => {
                    resolve(attestation);
                },
                (error) => {
                    reject(error);
                });
        });
    }

    private validateForm() {
        this.submitted = true;
        return this.finalAttestationForm && this.finalAttestationForm.form.valid && this.finalAttestation.attestorSignature;
    }

    saveAndContinue() {
        if (this.validateForm()) {
            this.saveAttestation().then((attestation) => {
                this.routeToNext();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndExit() {
        if (this.validateForm()) {
            this.saveAttestation().then((attestation) => {
                this.home();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndPrev() {
        if (this.validateForm()) {
            this.saveAttestation().then(
                (resp) => {
                    this.routeToPrev();
                }, (error) => {
                    console.log(error);
                    alert('Error saving Attestation : ' + error.message);
                });
        }
    }

    routeToPrev() {
        this.router.navigate(['/attestation/acknowledgement-status']);
    }

    routeToNext() {
        this.router.navigate(['/attestation/qsa-final-attestation']);
    }

    home() {
        this.router.navigate(['/saq/assessment-home']);
    }

    back() {
        if (this.finalAttestationForm && this.finalAttestationForm.form.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.finalAttestationForm && this.finalAttestationForm.form.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }
}
