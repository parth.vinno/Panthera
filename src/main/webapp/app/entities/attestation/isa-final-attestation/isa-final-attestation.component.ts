import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';

import {AttestationService, IsaAttestation, COMPLIANT_STATEMENT, DIRECTION, YesNoModalComponent} from '../../../shared';

@Component({
    selector: 'jhi-isa-final-attestation',
    templateUrl: './isa-final-attestation.component.html',
    styleUrls: ['./isa-final-attestation.component.scss', '../fa-arrows.scss']
})
export class IsaFinalAttestationComponent implements OnInit {

    @ViewChild('attestationForm') public attestationForm: NgForm;

    currentDate;
    isaAttestation: IsaAttestation;
    submitted = false;
    complianceType: COMPLIANT_STATEMENT;
    COMPLIANT_STATEMENT;
    enableForm;
    prevAttestationId;

    constructor(private router: Router, private _attestationService: AttestationService,
                private _ngZone: NgZone, private _bsModalService: BsModalService) {
        this.currentDate = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        this.isaAttestation = new IsaAttestation();
        this.COMPLIANT_STATEMENT = COMPLIANT_STATEMENT;
        this.enableForm = false;
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this._attestationService.getIsaAttestations().subscribe(
                (attestations) => {
                    if (attestations.length) {
                        this.isaAttestation = attestations[0];
                        this.prevAttestationId = this.isaAttestation.id;
                        this.currentDate = this.isaAttestation.attestationDate.split('T')[0];
                        this.enableForm = true;
                    } else {
                        this.toggleForm();
                    }
                }, (error) => {
                    console.error(error);
                });

            this._attestationService.getAttestation().subscribe(
                (attestations) => {
                    if (attestations.length) {
                        this.complianceType = attestations[0].compliantStatement;
                    }
                }
            )
        });
    }

    toggleForm() {
        if (this.enableForm) {
            this.attestationForm.form.controls.oName.enable(true);
            this.attestationForm.form.controls.titleName.enable(true);
            this.attestationForm.form.controls.companyName.enable(true);
        } else {
            this.initAttestation();
            this.attestationForm.form.controls.oName.disable(true);
            this.attestationForm.form.controls.titleName.disable(true);
            this.attestationForm.form.controls.companyName.disable(true);
        }
    }

    initAttestation() {
        this.isaAttestation = new IsaAttestation();
    }

    private saveAttestation() {
        return new Promise((resolve, reject) => {
            if (!this.enableForm) {
                this.deleteAttestation().then(() => {
                    resolve();
                }, (error) => {
                    reject(error);
                });
            } else {
                this._attestationService.saveIsaAttestation(this.isaAttestation).subscribe((attestation) => {
                    resolve(attestation);
                }, (error) => {
                    reject(error);
                });
            }
        });
    }

    private deleteAttestation() {
        return new Promise((resolve, reject) => {
            this._attestationService.deleteIsaAttestation(this.prevAttestationId).subscribe(
                () => {
                    resolve();
                },
                (error) => {
                    reject(error);
                });
        });
    }

    private validateForm() {
        this.submitted = true;
        return (this.prevAttestationId && !this.enableForm) || (this.attestationForm && this.attestationForm.form.valid);
    }

    private isFormDirty(): boolean {
        return (this.prevAttestationId && !this.enableForm) || (this.attestationForm && this.attestationForm.form.dirty);
    }

    saveAndContinue() {
        if (this.validateForm()) {
            this.saveAttestation().then(() => {
                this.routeToNext();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndExit() {
        if (this.validateForm()) {
            this.saveAttestation().then((attestation) => {
                this.home();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndPrev() {
        if (this.validateForm()) {
            this.saveAttestation().then(
                (resp) => {
                    this.routeToPrev();
                }, (error) => {
                    console.log(error);
                    alert('Error saving Attestation : ' + error.message);
                });
        }
    }

    routeToPrev() {
        this.router.navigate(['/attestation/qsa-final-attestation']);
    }

    routeToNext() {
        if (this.complianceType === COMPLIANT_STATEMENT.NON_COMPLIANT) {
            this.router.navigate(['/attestation/action-plan-for-nca']);
        } else {
            this.home();
        }
    }

    home() {
        this.router.navigate(['/saq/assessment-home']);
    }

    back() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    showSaveBtn(): boolean {
        return typeof this.prevAttestationId !== 'undefined' || this.enableForm;
    }
}
