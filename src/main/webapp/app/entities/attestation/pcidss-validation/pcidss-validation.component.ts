import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';

import {
    RequirementsService, PciDssRequirement, AttestationModel, AttestationService,
    COMPLIANT_STATEMENT, AocLegalExceptionDetail, AssessmentInfo, OrganizationsService,
    OrganizationModel, AssessmentService, DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-pcidss-validation',
    templateUrl: './pcidss-validation.component.html',
    styleUrls: ['./pcidss-validation.component.scss', '../responsive.scss', '../fa-arrows.scss']
})
export class PcidssValidationComponent implements OnInit {
    @ViewChild('pcidssForm') public pcidssForm: NgForm;

    attestation: AttestationModel = new AttestationModel();
    public selectedRequirements: Array<AocLegalExceptionDetail> = [];
    requirements: Array<PciDssRequirement>;
    datePicker;
    selectedDate: NgbDateStruct;
    COMPLIANT_STATEMENT;
    minDate;
    companyName;
    completionDate;
    organization: OrganizationModel = new OrganizationModel();
    assessment: AssessmentInfo = new AssessmentInfo();

    constructor(private router: Router, private _requirementsService: RequirementsService,
                private _ngZone: NgZone, private _attestationService: AttestationService,
                private _dateFormatter: NgbDateParserFormatter, private _organizationService: OrganizationsService,
                private _assessmentService: AssessmentService, private _bsModalService: BsModalService) {
        this._requirementsService.getPciDssRequirements().subscribe(
            (res) => this.requirements = res);

        // Date has to be the current date or after
        const curDate = new Date();
        this.minDate = {year: curDate.getFullYear(), month: curDate.getMonth() + 1, day: curDate.getDate()};
        this.COMPLIANT_STATEMENT = COMPLIANT_STATEMENT;
    }

    ngOnInit() {
        this._ngZone.run(() => {
            Observable.forkJoin(this._attestationService.getAttestation(),
                this._organizationService.getCompanyName(),
                this._assessmentService.getAssessmentDetails()).subscribe((results) => {
                    const [attestation, organization, assessment] = results;
                    if (attestation.length) {
                        this.attestation = attestation[0];
                        this.selectedDate = this._dateFormatter.parse(this.attestation.complianceTargetDate);
                        if (this.attestation.compliantStatement === this.COMPLIANT_STATEMENT.COMPLIANT_WITH_LEGAL_EXCEPTION) {
                            this._requirementsService.getAocLegalExceptions().subscribe(
                                (legalExceptions) => {
                                    this.selectedRequirements = legalExceptions;
                                });
                        }
                    }
                    if (organization.length) {
                        this.organization = organization[0];
                        this.companyName = this.organization.companyName;
                    }
                    if (assessment.length) {
                        this.assessment = assessment[0];
                        this.completionDate = this.assessment.assessmentEndDate;
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
        });
    }

    isSelected(req: PciDssRequirement) {
        for (let i = 0; i < this.selectedRequirements.length; i++) {
            if (this.selectedRequirements[i].pcidssRequirementId && this.selectedRequirements[i].pcidssRequirementId.id === req.id) {
                return true;
            }
        }
        return false;
    }

    deleteRequirement(index) {
        this.selectedRequirements.splice(index, 1);
    }

    addRequirement() {
        this.selectedRequirements.push(new AocLegalExceptionDetail());
    }

    initLegalExceptions() {
        if (this.selectedRequirements.length === 0) {
            this.addRequirement();
        }
    }

    saveAttestation() {
        this.attestation.complianceTargetDate = this._dateFormatter.format(this.selectedDate);
        return new Promise((resolve, reject) => {
            this._attestationService.saveAttestation(this.attestation).subscribe(
                (attestation) => {
                    if (this.attestation.compliantStatement === COMPLIANT_STATEMENT.COMPLIANT_WITH_LEGAL_EXCEPTION) {
                        this._requirementsService.saveAocLegalExceptions(this.selectedRequirements).subscribe(
                            () => {
                                resolve(attestation);
                            }, (error) => {
                                reject(error);
                            });
                    } else {
                        resolve(attestation);
                    }
                },
                (error) => {
                    reject(error);
                });
        });
    }

    saveAndContinue() {
        this.saveAttestation().then((attestation) => {
            this.routeToNext();
        }).catch((error) => {
            console.log('error');
            alert('Error saving Attestation : ' + error.message);
        });
    }

    saveAndExit() {
        this.saveAttestation().then((attestation) => {
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            console.log('error');
            alert('Error saving Attestation : ' + error.message);
        });
    }

    routeToNext() {
        this.router.navigate(['/attestation/acknowledgement-status']);
    }

    forward() {
        if (this.pcidssForm && this.pcidssForm.form.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.saveAndContinue();
            } else if (result === false) {
                this.routeToNext();
            }
        });
    }
}
