import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';

import {AttestationService, DIRECTION, QsaAttestation, YesNoModalComponent} from '../../../shared';

@Component({
    selector: 'jhi-final-attestation',
    templateUrl: './qsa-final-attestation.component.html',
    styleUrls: ['./qsa-final-attestation.component.scss', '../fa-arrows.scss']
})
export class QsaFinalAttestationComponent implements OnInit {
    @ViewChild('qsaAttestationForm') public qsaAttestationForm: NgForm;

    currentDate;
    qsaAttestation: QsaAttestation;
    submitted: boolean;
    enableForm: boolean;
    prevAttestationId;

    constructor(private router: Router, private _attestationService: AttestationService,
                private _ngZone: NgZone, private _bsModalService: BsModalService) {
        this.currentDate = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        this.qsaAttestation = new QsaAttestation(null);
        this.submitted = false;
        this.enableForm = false;
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this._attestationService.getQsaAttestations().subscribe(
                (attestations) => {
                    if (attestations.length) {
                        this.qsaAttestation = attestations[0];
                        this.prevAttestationId = this.qsaAttestation.id;
                        this.currentDate = this.qsaAttestation.attestationDate.split('T')[0];
                        this.enableForm = true;
                    } else {
                        this.toggleForm();
                    }
                }, (error) => {
                    console.error(error);
                });
        });
    }

    toggleForm() {
        if (this.enableForm) {
            this.qsaAttestationForm.form.controls.officerName.enable(true);
            this.qsaAttestationForm.form.controls.companyName.enable(true);
        } else {
            this.initAttestation();
            this.qsaAttestationForm.form.controls.officerName.disable(true);
            this.qsaAttestationForm.form.controls.companyName.disable(true);
        }
    }

    initAttestation() {
        // Assigning CLEAR to clear the signature field manually
        this.qsaAttestation = new QsaAttestation('CLEAR')
    }

    saveAttestation() {
        return new Promise((resolve, reject) => {
            if (!this.enableForm) {
                this.deleteAttestation().then(() => {
                    resolve();
                }, (error) => {
                    reject(error);
                });
            } else {
                this._attestationService.saveQsaAttestation(this.qsaAttestation).subscribe((attestation) => {
                    resolve(attestation);
                }, (error) => {
                    reject(error);
                });
            }
        });
    }

    private deleteAttestation() {
        return new Promise((resolve, reject) => {
            this._attestationService.deleteQsaAttestation(this.prevAttestationId).subscribe(() => {
                resolve();
            }, (error) => {
                reject(error);
            });
        });
    }

    private validateForm() {
        this.submitted = true;
        return (this.prevAttestationId && !this.enableForm) || (this.qsaAttestationForm && this.qsaAttestationForm.form.valid && this.qsaAttestation.qsaAttestorSignature);
    }

    private isFormDirty(): boolean {
        return (this.prevAttestationId && !this.enableForm) || (this.qsaAttestationForm && this.qsaAttestationForm.form.dirty);
    }

    saveAndContinue() {
        if (this.validateForm()) {
            this.saveAttestation().then((attestation) => {
                this.routeToNext();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndExit() {
        if (this.validateForm()) {
            this.saveAttestation().then((attestation) => {
                this.home();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Attestation : ' + error.message);
            });
        }
    }

    saveAndPrev() {
        if (this.validateForm()) {
            this.saveAttestation().then(
                (resp) => {
                    this.routeToPrev();
                }, (error) => {
                    console.log(error);
                    alert('Error saving Attestation : ' + error.message);
                });
        }
    }

    routeToPrev() {
        this.router.navigate(['/attestation/final-attestation']);
    }

    routeToNext() {
        this.router.navigate(['/attestation/isa-final-attestation']);
    }

    home() {
        this.router.navigate(['/saq/assessment-home']);
    }

    back() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    showSaveBtn(): boolean {
        return typeof this.prevAttestationId !== 'undefined' || this.enableForm;
    }
}
