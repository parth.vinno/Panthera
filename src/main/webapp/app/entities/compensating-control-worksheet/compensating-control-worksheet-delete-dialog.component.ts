import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CompensatingControlWorksheet } from './compensating-control-worksheet.model';
import { CompensatingControlWorksheetPopupService } from './compensating-control-worksheet-popup.service';
import { CompensatingControlWorksheetService } from './compensating-control-worksheet.service';

@Component({
    selector: 'jhi-compensating-control-worksheet-delete-dialog',
    templateUrl: './compensating-control-worksheet-delete-dialog.component.html'
})
export class CompensatingControlWorksheetDeleteDialogComponent {

    compensatingControlWorksheet: CompensatingControlWorksheet;

    constructor(
        private compensatingControlWorksheetService: CompensatingControlWorksheetService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.compensatingControlWorksheetService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'compensatingControlWorksheetListModification',
                content: 'Deleted an compensatingControlWorksheet'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-compensating-control-worksheet-delete-popup',
    template: ''
})
export class CompensatingControlWorksheetDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private compensatingControlWorksheetPopupService: CompensatingControlWorksheetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.compensatingControlWorksheetPopupService
                .open(CompensatingControlWorksheetDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
