import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { CompensatingControlWorksheet } from './compensating-control-worksheet.model';
import { CompensatingControlWorksheetService } from './compensating-control-worksheet.service';

@Component({
    selector: 'jhi-compensating-control-worksheet-detail',
    templateUrl: './compensating-control-worksheet-detail.component.html'
})
export class CompensatingControlWorksheetDetailComponent implements OnInit, OnDestroy {

    compensatingControlWorksheet: CompensatingControlWorksheet;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private compensatingControlWorksheetService: CompensatingControlWorksheetService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCompensatingControlWorksheets();
    }

    load(id) {
        this.compensatingControlWorksheetService.find(id).subscribe((compensatingControlWorksheet) => {
            this.compensatingControlWorksheet = compensatingControlWorksheet;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCompensatingControlWorksheets() {
        this.eventSubscriber = this.eventManager.subscribe(
            'compensatingControlWorksheetListModification',
            (response) => this.load(this.compensatingControlWorksheet.id)
        );
    }
}
