import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CompensatingControlWorksheet } from './compensating-control-worksheet.model';
import { CompensatingControlWorksheetPopupService } from './compensating-control-worksheet-popup.service';
import { CompensatingControlWorksheetService } from './compensating-control-worksheet.service';

@Component({
    selector: 'jhi-compensating-control-worksheet-dialog',
    templateUrl: './compensating-control-worksheet-dialog.component.html'
})
export class CompensatingControlWorksheetDialogComponent implements OnInit {

    compensatingControlWorksheet: CompensatingControlWorksheet;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private compensatingControlWorksheetService: CompensatingControlWorksheetService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.compensatingControlWorksheet.id !== undefined) {
            this.subscribeToSaveResponse(
                this.compensatingControlWorksheetService.update(this.compensatingControlWorksheet));
        } else {
            this.subscribeToSaveResponse(
                this.compensatingControlWorksheetService.create(this.compensatingControlWorksheet));
        }
    }

    private subscribeToSaveResponse(result: Observable<CompensatingControlWorksheet>) {
        result.subscribe((res: CompensatingControlWorksheet) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CompensatingControlWorksheet) {
        this.eventManager.broadcast({ name: 'compensatingControlWorksheetListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-compensating-control-worksheet-popup',
    template: ''
})
export class CompensatingControlWorksheetPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private compensatingControlWorksheetPopupService: CompensatingControlWorksheetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.compensatingControlWorksheetPopupService
                    .open(CompensatingControlWorksheetDialogComponent, params['id']);
            } else {
                this.modalRef = this.compensatingControlWorksheetPopupService
                    .open(CompensatingControlWorksheetDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
