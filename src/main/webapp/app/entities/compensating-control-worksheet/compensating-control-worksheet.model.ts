import { BaseEntity } from './../../shared';

export class CompensatingControlWorksheet implements BaseEntity {
    constructor(
        public id?: number,
        public requiredInformation?: string,
        public worksheetType?: string,
    ) {
    }
}
