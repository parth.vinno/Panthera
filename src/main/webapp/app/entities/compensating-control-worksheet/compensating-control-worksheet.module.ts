import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import {
    CompensatingControlWorksheetService,
    CompensatingControlWorksheetPopupService,
    CompensatingControlWorksheetComponent,
    CompensatingControlWorksheetDetailComponent,
    CompensatingControlWorksheetDialogComponent,
    CompensatingControlWorksheetPopupComponent,
    CompensatingControlWorksheetDeletePopupComponent,
    CompensatingControlWorksheetDeleteDialogComponent,
    compensatingControlWorksheetRoute,
    compensatingControlWorksheetPopupRoute,
    CompensatingControlWorksheetResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...compensatingControlWorksheetRoute,
    ...compensatingControlWorksheetPopupRoute,
];

@NgModule({
    imports: [
        PcidssSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CompensatingControlWorksheetComponent,
        CompensatingControlWorksheetDetailComponent,
        CompensatingControlWorksheetDialogComponent,
        CompensatingControlWorksheetDeleteDialogComponent,
        CompensatingControlWorksheetPopupComponent,
        CompensatingControlWorksheetDeletePopupComponent,
    ],
    entryComponents: [
        CompensatingControlWorksheetComponent,
        CompensatingControlWorksheetDialogComponent,
        CompensatingControlWorksheetPopupComponent,
        CompensatingControlWorksheetDeleteDialogComponent,
        CompensatingControlWorksheetDeletePopupComponent,
    ],
    providers: [
        CompensatingControlWorksheetService,
        CompensatingControlWorksheetPopupService,
        CompensatingControlWorksheetResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssCompensatingControlWorksheetModule {}
