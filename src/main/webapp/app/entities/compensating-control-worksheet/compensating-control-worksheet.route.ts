import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CompensatingControlWorksheetComponent } from './compensating-control-worksheet.component';
import { CompensatingControlWorksheetDetailComponent } from './compensating-control-worksheet-detail.component';
import { CompensatingControlWorksheetPopupComponent } from './compensating-control-worksheet-dialog.component';
import { CompensatingControlWorksheetDeletePopupComponent } from './compensating-control-worksheet-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class CompensatingControlWorksheetResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const compensatingControlWorksheetRoute: Routes = [
    {
        path: 'compensating-control-worksheet',
        component: CompensatingControlWorksheetComponent,
        resolve: {
            'pagingParams': CompensatingControlWorksheetResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.compensatingControlWorksheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'compensating-control-worksheet/:id',
        component: CompensatingControlWorksheetDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.compensatingControlWorksheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const compensatingControlWorksheetPopupRoute: Routes = [
    {
        path: 'compensating-control-worksheet-new',
        component: CompensatingControlWorksheetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.compensatingControlWorksheet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'compensating-control-worksheet/:id/edit',
        component: CompensatingControlWorksheetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.compensatingControlWorksheet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'compensating-control-worksheet/:id/delete',
        component: CompensatingControlWorksheetDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.compensatingControlWorksheet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
