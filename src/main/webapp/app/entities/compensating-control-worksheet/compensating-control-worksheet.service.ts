import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { CompensatingControlWorksheet } from './compensating-control-worksheet.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CompensatingControlWorksheetService {

    private resourceUrl = 'api/compensating-control-worksheets';

    constructor(private http: Http) { }

    create(compensatingControlWorksheet: CompensatingControlWorksheet): Observable<CompensatingControlWorksheet> {
        const copy = this.convert(compensatingControlWorksheet);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(compensatingControlWorksheet: CompensatingControlWorksheet): Observable<CompensatingControlWorksheet> {
        const copy = this.convert(compensatingControlWorksheet);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<CompensatingControlWorksheet> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(compensatingControlWorksheet: CompensatingControlWorksheet): CompensatingControlWorksheet {
        const copy: CompensatingControlWorksheet = Object.assign({}, compensatingControlWorksheet);
        return copy;
    }
}
