import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PcidssSaqResponseModule } from './saq-response/saq-response.module';
import { PcidssAddressModule } from './address/address.module';
import { PcidssRequirementWorkStatusModule } from './requirement-work-status/requirement-work-status.module';
import { PcidssCompensatingControlWorksheetModule } from './compensating-control-worksheet/compensating-control-worksheet.module';
import { PcidssCountryModule } from './country/country.module';
import { PcidssStateModule } from './state/state.module';
import { PcidssAttestationModule } from './attestation/attestation.module';
import { PcidssSelfAssessmentModule } from './self-assessment/self-assessment.module';
import { PcidssSaqAdminModule } from './saq-admin/saq-admin.module';
import { PcidssExecutiveSummaryModule } from './executive-summary/executive-summary.module';
import { PcidssAssessmentHomeModule } from './assessment-home/assessment-home.module';
import { PcidssAssessmentInfoModule } from './assessment-info/assessment-info.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        PcidssSaqResponseModule,
        PcidssAddressModule,
        PcidssRequirementWorkStatusModule,
        PcidssCompensatingControlWorksheetModule,
        PcidssCountryModule,
        PcidssStateModule,
        PcidssAttestationModule,
        PcidssSelfAssessmentModule,
        PcidssSaqAdminModule,
        PcidssExecutiveSummaryModule,
        PcidssAssessmentHomeModule,
        PcidssAssessmentInfoModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssEntityModule {}
