import { Action } from '@ngrx/store';

export const SELECTED_SERVICES = 'SELECTED_SERVICES';
export const NOT_SELECTED_SERVICES = 'NOT_SELECTED_SERVICES';
export const MERCHANT_BUSINESS = 'MERCHANT_BUSINESS';
export const CARD_DESCRIPTION = 'CARD_DESCRIPTION';
export const LOCATIONS = 'LOCATIONS';
export const PAYMENT_APPLICATIONS = 'PAYMENT_APPLICATIONS';
export const ENV_DESCRIPTION = 'ENV_DESCRIPTION';
export const SERVICE_PROVIDERS = 'SERVICE_PROVIDERS';
export const TESTED_REQUIREMENTS = 'TESTED_REQUIREMENTS';
export const SUMMARY = 'SUMMARY';
export const ASSESSOR = 'ASSESSOR';

export function exSummaryReducer(state: string , action: Action) {
    switch (action.type) {
        case SELECTED_SERVICES:
            return 'SELECTED_SERVICES';

        case NOT_SELECTED_SERVICES:
            return 'NOT_SELECTED_SERVICES';

        case MERCHANT_BUSINESS:
            return 'MERCHANT_BUSINESS';

        case CARD_DESCRIPTION:
            return 'CARD_DESCRIPTION';

        case LOCATIONS:
            return 'LOCATIONS';

        case PAYMENT_APPLICATIONS:
            return 'PAYMENT_APPLICATIONS';

        case ENV_DESCRIPTION:
            return 'ENV_DESCRIPTION';

        case SERVICE_PROVIDERS:
            return 'SERVICE_PROVIDERS';

        case TESTED_REQUIREMENTS:
            return 'TESTED_REQUIREMENTS';

        case ASSESSOR:
            return 'ASSESSOR';

        case SUMMARY:
            return 'SUMMARY';

        default:
            return 'SELECTED_SERVICES';
    }
}
