import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';

import { ExecutiveSummaryStatus } from './executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ExecutiveSummaryStatusService {

    private resourceUrl = 'api/executive-summary-statuses';
    private resourceUrlSp = 'api/executive-summary-statuses/status/service-provider';
    private resourceUrlMerchant = 'api/executive-summary-statuses/status';

    constructor(private http: Http) { }

    createSummary(executiveSummaryStatus: ExecutiveSummaryStatus, section: string): Observable<ExecutiveSummaryStatus> {
        return this.http.post(`${this.resourceUrl}/${section}`, executiveSummaryStatus).map((res: Response) => {
              return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySp(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSp, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryMerchant(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlMerchant, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(executiveSummaryStatus: ExecutiveSummaryStatus): ExecutiveSummaryStatus {
        const copy: ExecutiveSummaryStatus = Object.assign({}, executiveSummaryStatus);
        return copy;
    }
}
