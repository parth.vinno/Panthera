import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiAlertService } from 'ng-jhipster';

import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-executive-summary',
    templateUrl: './executive-summary.component.html'
})
export class ExecutiveSummaryComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}
