import { BaseEntity } from './../../shared';
// import { PcidssRequirement} from '../saq-admin/pcidss-requirement';

export class ScopeVerificationSelectedService implements BaseEntity {
    constructor(
        public id?: number,
        public assessedServiceName?: string,
        public assessedServiceType?: string,
        public selectedServiceTypeId?: BaseEntity
    ) {
    }
}

export class HostingProvider implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string
    ) {
    }
}

export class ManagedService implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string
    ) {
    }
}

export class PaymentProcessing implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string
    ) {}
}

export enum SCOPE {
    SELECTED = <any>'SELECTED',
    NOT_SELECTED= <any>'NOT_SELECTED'
}

export class ServiceCategory implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public serviceTypes?: BaseEntity[]
    ) {
    }
}

export class ServiceType implements BaseEntity {
    constructor(
        public id?: number,
        public serviceType?: string,
        public serviceCategoryId?: BaseEntity,
        public checked?: boolean
    ) {
        this.checked = false;
    }
}

export class SelectedServiceType implements BaseEntity {
    constructor(
        public id?: number,
        public serviceTypeId?: BaseEntity,
    ) {
    }
}

export class OtherServiceResponse implements BaseEntity {
    constructor(
        public id?: number,
        public other?: string,
        public serviceTypeId?: BaseEntity,
        public scope?: SCOPE
    ) {
    }
}
export class ScopeVerificationNotSelectedService implements BaseEntity {
    constructor(
        public id?: number,
        public notAssessedServiceName?: string,
        public notAssessedServiceType?: string,
        public comment?: string
    ) {
    }
}

export class ServiceTypeNotSelected implements BaseEntity {
    constructor(
        public id?: number,
        public serviceTypeId?: BaseEntity,
    ) {
    }
}

export class MerchantBusinessType implements BaseEntity {
    constructor(
        public id?: number,
        public businessType?: string,
        public checked?: boolean
    ) {
        this.checked = false;
    }
}

export class MerchantBusinessTypeSelected implements BaseEntity {
    constructor(
        public id?: number,
        public specification?: string,
        public merchantBusinessTypeId?: BaseEntity
    ) {
    }
}

export class PaymentChannel implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public checked?: boolean
    ) {
        this.checked = false;
    }
}

export class PaymentChannelBusinessServes implements BaseEntity {
    constructor(
        public id?: number,
        public paymentChannelId?: BaseEntity
    ) {
    }
}

export class PaymentChannelSaqCovers implements BaseEntity {
    constructor(
        public id?: number,
        public paymentChannelId?: BaseEntity
    ) {
    }
}

export class PaymentCardBusinessDescription implements BaseEntity {
    constructor(
        public id?: number,
        public descriptionTransmit?: string,
        public descriptionImpact?: string,
    ) {}
}

export class Location implements BaseEntity {
    constructor(
        public id?: number,
        public facilityType?: string,
        public numberOfFacilityType?: number,
        public location?: string,
    ) {}
}

export class PaymentApplication implements BaseEntity {
    constructor(
        public id?: number,
        public isPaymentApplicationMoreThanOne?: boolean,
        public paymentApplicationLists?: BaseEntity[],
    ) {
        this.isPaymentApplicationMoreThanOne = false;
    }
}

export class PaymentApplicationList implements BaseEntity {
    constructor(
        public id?: number,
        public paymentApplicationName?: string,
        public versionNumber?: string,
        public applicationVendor?: string,
        public isApplicationPadssListed?: boolean,
        public expiryDate?: any,
        public paymentApplication?: BaseEntity,
    ) {
        this.isApplicationPadssListed = false;
    }
}

export enum PAYMENTAPPLICATION_RADIO {
    YES = <any>'true',
    NO = <any>'false'
}

export class EnvironmentDescription implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public networkSegmented?: boolean,
    ) {
        this.networkSegmented = false;
    }
}

export class ThirdPartyServiceProvider implements BaseEntity {
    constructor(
        public id?: number,
        public qirRelationshipPresent?: boolean,
        public qirCompanyName?: string,
        public qirIndividualName?: string,
        public serviceDescription?: string,
        public serviceProviderRelationShipPresent?: boolean,
        public serviceByServiceProviders?: BaseEntity[],
    ) {
        this.qirRelationshipPresent = false;
        this.serviceProviderRelationShipPresent = false;
    }
}

export class ServiceByServiceProvider implements BaseEntity {
    constructor(
        public id?: number,
        public providerName?: string,
        public descriptionOfServiceProvider?: string,
     //   public thirdPartyServiceProvider?: BaseEntity,
    ) {
    }
}

export enum ASSESSED {
    FULL = <any>'FULL',
    PARTIAL = <any>'PARTIAL',
    NONE = <any>'NONE'
}

export class TestedRequirementHeader {
    constructor(
        public id?: number,
        public serviceAssessedName?: string,
        public testedRequirementSummaries?: BaseEntity[]
    ) {}
}

export class TestedRequirementSummary implements BaseEntity {
    constructor(
        public id?: number,
        public assessed?: ASSESSED,
        public justification?: string,
        public pcidssRequirementId?: BaseEntity,
        public ReqOrAppendix?: string,
        public testedRequirementHeaderId?: BaseEntity
    ) {}
}

export class PcidssRequirement implements BaseEntity {
    constructor(
        public id?: number,
        public requirementNumber?: number,
        public requirement?: string,
        public requirementCategoryId?: BaseEntity
    ) {
    }
}

export enum EXECUTIVESTATUS {
    SELECTED_SERVICES = <any>'SELECTED_SERVICES',
    NOT_SELECTED_SERVICES = <any>'NOT_SELECTED_SERVICES',
    MERCHANT_BUSINESS = <any>'MERCHANT_BUSINESS',
    CARD_DESCRIPTION = <any>'CARD_DESCRIPTION',
    LOCATIONS = <any>'LOCATIONS',
    PAYMENT_APPLICATIONS = <any>'PAYMENT_APPLICATIONS',
    ENV_DESCRIPTION = <any>'ENV_DESCRIPTION',
    SERVICE_PROVIDERS = <any>'SERVICE_PROVIDERS',
    TESTED_REQUIREMENTS = <any>'TESTED_REQUIREMENTS',
    SUMMARY = <any>'SUMMARY',
}

export class ExecutiveSummaryStatus implements BaseEntity {
    constructor(
        public id?: number,
        public section?: BaseEntity,
        public lastUpdatedAt?: BaseEntity
    ) {
    }
}
