import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from 'ng2-ckeditor';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../shared';
import { exSummaryReducer } from './ex-summary-reducer.service';

import {
    ExecutiveSummaryComponent,
    executiveSummaryRoute,
    LeftNavComponent,
    HostingProviderService,
    ManagedServiceService,
    PaymentProcessingService,
    Part2aParentComponent,
    Part2aComponent,
    Part2aService,
    Part2aContinueComponent,
    Part2aContinueService,
    Part2aMerchantComponent,
    Part2aMerchantService,
    Part2bParentComponent,
    Part2bComponent,
    Part2bService,
    Part2bMerchantComponent,
    Part2bMerchantService,
    Part2cComponent,
    Part2cService,
    Part2dComponent,
    Part2dService,
    PaymentApplicationListService,
    Part2eComponent,
    Part2eService,
    Part2fComponent,
    Part2fService,
    Part2gComponent,
    Part2gService,
    SummaryComponent,
    SummaryService,
    ExecutiveSummaryStatusService
} from './';
import { DisplayPart2ErrorsComponent } from '../form-validation-error-display/display-part-2-error.component';

const ENTITY_STATES = [
    ...executiveSummaryRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        StoreModule.provideStore({state: exSummaryReducer})
    ],
    declarations: [
        ExecutiveSummaryComponent,
        LeftNavComponent,
        Part2aParentComponent,
        Part2aComponent,
        Part2aContinueComponent,
        Part2aMerchantComponent,
        Part2bParentComponent,
        Part2bComponent,
        Part2bMerchantComponent,
        Part2cComponent,
        Part2dComponent,
        Part2eComponent,
        Part2fComponent,
        Part2gComponent,
        SummaryComponent,
        DisplayPart2ErrorsComponent
    ],
    entryComponents: [
        ExecutiveSummaryComponent
    ],
    providers: [
                HostingProviderService,
                ManagedServiceService,
                PaymentProcessingService,
                Part2aService,
                Part2aContinueService,
                Part2aMerchantService,
                Part2bService,
                Part2bMerchantService,
                Part2cService,
                Part2dService,
                PaymentApplicationListService,
                Part2eService,
                Part2fService,
                Part2gService,
                SummaryService,
                ExecutiveSummaryStatusService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssExecutiveSummaryModule {}
