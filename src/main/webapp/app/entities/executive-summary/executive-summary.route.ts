import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import { ExecutiveSummaryComponent } from './executive-summary.component';
import { Part2aComponent } from './part-2a/part-2a.component';
import { Part2aContinueComponent } from './part-2a-continue/part-2a-continue.component';
import { Part2aMerchantComponent } from './part-2a-merchant/part-2a-merchant.component';
import { Part2bComponent } from './part-2b/part-2b.component';
import { Part2bMerchantComponent } from './part-2b-merchant/part-2b-merchant.component';
import { Part2cComponent } from './part-2c/part-2c.component';
import { Part2dComponent } from './part-2d/part-2d.component';
import { Part2eComponent } from './part-2e/part-2e.component';
import { Part2fComponent } from './part-2f/part-2f.component';
import { Part2gComponent } from './part-2g/part-2g.component';
import { Part2aParentComponent } from './part-2a-parent/part-2a-parent.component';
import { Part2bParentComponent } from './part-2b-parent/part-2b-parent.component';
import { SummaryComponent } from './summary/summary.component';

export const executiveSummaryRoute: Routes = [
    {
        path: 'saq/executive-summary',
        component: ExecutiveSummaryComponent,
        data: {
            pageTitle: 'pcidssApp.assessmentInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
                   {
                       path: 'scope-verification',
                       component: Part2aParentComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   },
                   {
                       path: 'sp-scope-verification',
                       component: Part2aComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'scope-verification-continue',
                       component: Part2aContinueComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchant-scope-verification',
                       component: Part2aMerchantComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-card-business',
                       component: Part2bParentComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'sp-payment-card-business',
                       component: Part2bComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchnat-payment-card-business',
                       component: Part2bMerchantComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'locations',
                       component: Part2cComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-applications',
                       component: Part2dComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'environment',
                       component: Part2eComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'third-party-service-providers',
                       component: Part2fComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'summary-of-requirements-tested',
                       component: Part2gComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'summary',
                       component: SummaryComponent,
                       data: {
                           authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.assessmentInfo.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: '',
                       redirectTo: 'scope-verification',
                       pathMatch: 'full'
                   }
        ]
    }
];
