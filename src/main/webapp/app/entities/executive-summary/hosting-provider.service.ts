import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { HostingProvider } from './executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class HostingProviderService {

    private resourceUrl = 'api/hosting-providers';

    constructor(private http: Http) { }

    create(hostingProvider: HostingProvider): Observable<HostingProvider> {
        const copy = this.convert(hostingProvider);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(hostingProvider: HostingProvider): Observable<HostingProvider> {
        const copy = this.convert(hostingProvider);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<HostingProvider> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(hostingProvider: HostingProvider): HostingProvider {
        const copy: HostingProvider = Object.assign({}, hostingProvider);
        return copy;
    }
}
