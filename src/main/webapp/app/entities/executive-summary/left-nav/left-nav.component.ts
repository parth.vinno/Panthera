import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS, SUMMARY } from '../ex-summary-reducer.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { ExecutiveSummaryStatus, EXECUTIVESTATUS} from '../executive-summary.model';
import { ExecutiveSummaryStatusService } from '../executive-summary-status.service';

@Component({
  selector: 'jhi-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit, OnDestroy  {
    tenantId: any;
    availableExSummary:  any= [];
    state: Observable<string>;
    curStates: any = [];
    statesSubscription
    EXECUTIVESTATUS;
    summaryStatus: any;
    SpSummaryStatus: any;
    merchantSummaryStatus: any;
    summaryStatusObj = {'SELECTED_SERVICES': '',
                        'NOT_SELECTED_SERVICES': '',
                        'MERCHANT_BUSINESS': '',
                        'CARD_DESCRIPTION': '',
                        'LOCATIONS' : '',
                        'PAYMENT_APPLICATIONS' : '',
                        'ENV_DESCRIPTION' : '',
                        'SERVICE_PROVIDERS' : '',
                        'TESTED_REQUIREMENTS' : '',
                        'SUMMARY': ''
    }

    constructor(private _storageService: StorageService,
                private _ngZone: NgZone,
                private store: Store<string>,
                private _executiveSummaryStatusService: ExecutiveSummaryStatusService) {
        this.EXECUTIVESTATUS = EXECUTIVESTATUS;
        this.curStates = [];
        this.state = store.select('state');
        this.state.subscribe(
            (curStates) => {
                if (curStates === 'SELECTED_SERVICES') {
                    this.summaryStatusObj.SELECTED_SERVICES = curStates;
                }
                if (curStates === 'NOT_SELECTED_SERVICES') {
                    this.summaryStatusObj.NOT_SELECTED_SERVICES = curStates;
                }
                if (curStates === 'MERCHANT_BUSINESS') {
                    this.summaryStatusObj.MERCHANT_BUSINESS = curStates;
                }
                if (curStates === 'CARD_DESCRIPTION') {
                    this.summaryStatusObj.CARD_DESCRIPTION = curStates;
                }
                if (curStates === 'LOCATIONS') {
                    this.summaryStatusObj.LOCATIONS = curStates;
                }
                if (curStates === 'PAYMENT_APPLICATIONS') {
                    this.summaryStatusObj.PAYMENT_APPLICATIONS = curStates;
                }
                if (curStates === 'ENV_DESCRIPTION') {
                    this.summaryStatusObj.ENV_DESCRIPTION = curStates;
                }
                if (curStates === 'SERVICE_PROVIDERS') {
                    this.summaryStatusObj.SERVICE_PROVIDERS = curStates;
                }
                if (curStates === 'TESTED_REQUIREMENTS') {
                    this.summaryStatusObj.TESTED_REQUIREMENTS = curStates;
                }
                if (curStates === 'SUMMARY') {
                    this.summaryStatusObj.SUMMARY = curStates;
                }
            }
        )
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        Observable.forkJoin(
                this._executiveSummaryStatusService.query(),
                this._executiveSummaryStatusService.queryMerchant(),
                this._executiveSummaryStatusService.querySp()).subscribe((results) => {
                    const [summaryStatus, merchantSummaryStatus, SpSummaryStatus] = results;
                    this.summaryStatus = summaryStatus.json;
                    this.merchantSummaryStatus = merchantSummaryStatus.json;
                    this.SpSummaryStatus = SpSummaryStatus.json;
                    if (this.tenantId.includes('serviceprovider_saqd') && this.SpSummaryStatus.status === 'START') {
                        this.store.dispatch({type: SELECTED_SERVICES});
                    }
                    if (this.tenantId.includes('merchant_saqd') && this.merchantSummaryStatus.status === 'START') {
                        this.store.dispatch({type: MERCHANT_BUSINESS});
                    }
                        this.summaryStatus.map((status) => {
                          if (this.tenantId.includes('serviceprovider_saqd')) {
                              if (status.section === this.EXECUTIVESTATUS.SELECTED_SERVICES) {
                                  this.store.dispatch({type: NOT_SELECTED_SERVICES});
                              } else if (status.section === this.EXECUTIVESTATUS.NOT_SELECTED_SERVICES) {
                                  this.store.dispatch({type: CARD_DESCRIPTION});
                              } else if (status.section === this.EXECUTIVESTATUS.MERCHANT_BUSINESS) {
                                  this.store.dispatch({type: CARD_DESCRIPTION});
                              } else if (status.section === this.EXECUTIVESTATUS.CARD_DESCRIPTION) {
                                  this.store.dispatch({type: LOCATIONS});
                              } else if (status.section === this.EXECUTIVESTATUS.LOCATIONS) {
                                  this.store.dispatch({type: PAYMENT_APPLICATIONS});
                              } else if (status.section === this.EXECUTIVESTATUS.PAYMENT_APPLICATIONS) {
                                  this.store.dispatch({type: ENV_DESCRIPTION});
                              } else if (status.section === this.EXECUTIVESTATUS.ENV_DESCRIPTION) {
                                  this.store.dispatch({type: SERVICE_PROVIDERS});
                              } else if (status.section === this.EXECUTIVESTATUS.SERVICE_PROVIDERS) {
                                  this.store.dispatch({type: TESTED_REQUIREMENTS});
                              } else if (status.section === this.EXECUTIVESTATUS.TESTED_REQUIREMENTS) {
                                  this.store.dispatch({type: SUMMARY});
                              }
                          } else if (this.tenantId.includes('merchant_saqd')) {
                              if (status.section === this.EXECUTIVESTATUS.MERCHANT_BUSINESS) {
                                  this.store.dispatch({type: CARD_DESCRIPTION});
                              } else if (status.section === this.EXECUTIVESTATUS.CARD_DESCRIPTION) {
                                  this.store.dispatch({type: LOCATIONS});
                              } else if (status.section === this.EXECUTIVESTATUS.LOCATIONS) {
                                  this.store.dispatch({type: PAYMENT_APPLICATIONS});
                              } else if (status.section === this.EXECUTIVESTATUS.PAYMENT_APPLICATIONS) {
                                  this.store.dispatch({type: ENV_DESCRIPTION});
                              } else if (status.section === this.EXECUTIVESTATUS.ENV_DESCRIPTION) {
                                  this.store.dispatch({type: SERVICE_PROVIDERS});
                              } else if (status.section === this.EXECUTIVESTATUS.SERVICE_PROVIDERS) {
                                  this.store.dispatch({type: SUMMARY});
                              }
                          }
                        });
                });
    }

    ngOnDestroy() {
        if (this.statesSubscription) {
            this.statesSubscription.unsubscribe();
        }
    }
}
