import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { ScopeVerificationNotSelectedService, ServiceType, SelectedServiceType, ServiceTypeNotSelected,
    OtherServiceResponse, SCOPE} from '../executive-summary.model';
import { Part2aContinueService } from './part-2a-continue.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-part-2a-continue',
    templateUrl: './part-2a-continue.component.html',
    styleUrls: ['./part-2a-continue.component.scss', '../fa-arrows.scss']
})
export class Part2aContinueComponent implements OnInit, OnDestroy {
    part2aContinueForm: FormGroup;
    part2aContinue: ScopeVerificationNotSelectedService;
    notSelectedServiceType: ServiceTypeNotSelected[];
    selectedServiceType: SelectedServiceType[];
    otherServiceResponse: OtherServiceResponse[];
    hostingProvider: ServiceType[];
    managedService: ServiceType[];
    paymentProcessing: ServiceType[];
    serviceType: ServiceType[];
    tenantId: any;
    totalItems: any;
    queryCount: any;
    specify = false;
    specifyManage = false;
    specifyPayment = false;
    specifyHost = false;
    controlOther;
    SCOPE;
    state: Observable<string>;
    curStates: string;
    statesSubscription;

    constructor(private _part2aContinueService: Part2aContinueService,
            private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _bsModalService: BsModalService,
            private _storageService: StorageService,
            private _toastr: ToastrService,
            private store: Store<string>) {
        this.part2aContinue = new ScopeVerificationNotSelectedService();
        this.SCOPE = SCOPE;
        this.state = store.select('state');
        this.statesSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this._ngZone.run(() => {
            this._part2aContinueService.querySelectedType().subscribe((res: ResponseWrapper) => {
                this.totalItems = res.headers.get('X-Total-Count');
                this.queryCount = this.totalItems;
            this._part2aContinueService.querySelectedType({size: this.queryCount}).subscribe((res2a: ResponseWrapper) => {
                this.selectedServiceType = res2a.json;
                this._part2aContinueService.queryCategory().subscribe((resCategory: ResponseWrapper) => {
                    const serviceCategory = resCategory.json;
                    let categories: any;
                    if (serviceCategory.length) {
                        serviceCategory.map((category) => {
                            categories = (category.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                            if (categories === 'Hosting Provider'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                this.hostingProvider = category.serviceTypes.filter((array_el) => {
                                    return this.selectedServiceType.filter((anotherOne_el) => {
                                        return anotherOne_el.serviceTypeId.id === array_el.id;
                                    }).length === 0
                                });
                                this.hostingProvider.map((x, i) => {
                                    this.hostingProvider[i] = {};
                                    this.hostingProvider[i].id = x.id;
                                    this.hostingProvider[i].serviceType = x.serviceType;
                                    this.hostingProvider[i].checked = false;
                                });
                                this.sortServiceType(this.hostingProvider);
                            } else if (categories === 'Managed Services (specify)'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                this.managedService = category.serviceTypes.filter((array_el) => {
                                    return this.selectedServiceType.filter((anotherOne_el) => {
                                        return anotherOne_el.serviceTypeId.id === array_el.id;
                                    }).length === 0
                                });
                                this.managedService.map((x, i) => {
                                    this.managedService[i] = {};
                                    this.managedService[i].id = x.id;
                                    this.managedService[i].serviceType = x.serviceType;
                                    this.managedService[i].checked = false;
                                });
                                this.sortServiceType(this.managedService);
                            } else if (categories === 'Payment Processing'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                this.paymentProcessing = category.serviceTypes.filter((array_el) => {
                                    return this.selectedServiceType.filter((anotherOne_el) => {
                                        return anotherOne_el.serviceTypeId.id === array_el.id;
                                    }).length === 0
                                });
                                this.paymentProcessing.map((x, i) => {
                                    this.paymentProcessing[i] = {};
                                    this.paymentProcessing[i].id = x.id;
                                    this.paymentProcessing[i].serviceType = x.serviceType;
                                    this.paymentProcessing[i].checked = false;
                                });
                                this.sortServiceType(this.paymentProcessing);
                            }
                        })
                    }
                }, (resCategory: ResponseWrapper) => {
                    this.onError(resCategory.json);
                });
                this._part2aContinueService.query().subscribe((res2aService: ResponseWrapper) => {
                    const scopeVerify = res2aService.json;
                    if (scopeVerify.length) {
                        this.part2aContinueForm.patchValue({
                            id: scopeVerify[0].id,
                            notAssessedServiceName: scopeVerify[0].notAssessedServiceName,
                            notAssessedServiceType: scopeVerify[0].notAssessedServiceType,
                            comment: scopeVerify[0].comment
                        })
                    }
                    this._part2aContinueService.queryNotSelectedType().subscribe((resType: ResponseWrapper) => {
                        const control = <FormArray>this.part2aContinueForm.controls['notSelectedServiceType'];
                        const types = resType.json;
                        const selected: any = [];
                        if (types.length) {
                            types.map((stype) => {
                                control.push(this._fb.group({
                                    id: stype.id,
                                    serviceTypeId: stype.serviceTypeId
                                }))
                                selected.push(stype.serviceTypeId);
                            });
                        }
                        this.isTypesChecked(this.hostingProvider, selected);
                        this.isTypesChecked(this.managedService, selected);
                        this.isTypesChecked(this.paymentProcessing, selected);
                        this._part2aContinueService.queryOther().subscribe(
                                (resOther: ResponseWrapper) => {
//                                    const controlOther = <FormArray>this.part2aContinueForm.controls['otherServiceResponse'];
                                    const others = resOther.json;
                                    let otherValue: any;
                                    others.map((other) => {
                                        otherValue = (other.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                        if (otherValue === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                && other.scope === this.SCOPE.NOT_SELECTED) {
                                            this.part2aContinueForm.patchValue({
                                                specificationHost: other.other
                                            })
                                            this.specifyHost = true;
                                        } else if (otherValue === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                && other.scope === this.SCOPE.NOT_SELECTED) {
                                            this.part2aContinueForm.patchValue({
                                                specificationOther: other.other
                                            })
                                            this.specify = true;
                                        } else if (otherValue === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                && other.scope === this.SCOPE.NOT_SELECTED) {
                                            this.part2aContinueForm.patchValue({
                                                specificationManage: other.other
                                            })
                                            this.specifyManage = true;
                                        } else if (otherValue === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                && other.scope === this.SCOPE.NOT_SELECTED) {
                                            this.part2aContinueForm.patchValue({
                                                specificationProcess: other.other
                                            })
                                            this.specifyPayment = true;
                                        }
                                    })
                                },
                                (resOther: ResponseWrapper) => {
                                    this.onError(resOther.json);
                                });
                    }, (resType: ResponseWrapper) => {
                        this.onError(resType.json);
                    })
                }, (res2aService: ResponseWrapper) => {
                    this.onError(res2aService.json);
                })
            },
            (res2a: ResponseWrapper) => {
                this.onError(res2a.json);
            });
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            });
        });
        this.part2aContinueForm = this._fb.group({
            id: null,
            notAssessedServiceName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.notAssessedServiceName]],
            notAssessedServiceType: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.notAssessedServiceType]],
            comment: [null, [Validators.required, CustomValidators.comment]],
            specificationOther: [null, [CustomValidators.specificationOther]],
            specificationManage: [null, [CustomValidators.specificationManage]],
            specificationHost: [null, [CustomValidators.specificationHost]],
            specificationProcess: [null, [CustomValidators.specificationProcess]],
            notSelectedServiceType: this._fb.array([]),
            otherServiceResponse: this._fb.array([])
        })
    }

    isTypesChecked(channel, selected) {
        if (selected.length > 0) {
            for (let i = 0; i < selected.length; i++ ) {
                for (let j = 0; j < channel.length; j++ ) {
                    if (channel[j].id === selected[i].id) {
                        channel[j].checked = true;
                    }
                }
            }
        }
    }

    sortServiceType(types) {
        types.sort( function(t1, t2) {
            if ( t1.id < t2.id ) {
                return -1;
            } else if ( t1.id > t2.id ) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    onHostChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aContinueForm.controls['notSelectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
        if (isChecked) {
            control.push(this._fb.group({
                serviceTypeId: types
            }))
            if (typeValue  === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyHost = true;
            }
            if (typeValue  === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specify = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue  === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyHost = false;
                this.part2aContinueForm.patchValue({
                    specificationHost: null
                })
            }
            if (typeValue  === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specify = false;
                this.part2aContinueForm.patchValue({
                    specificationOther: null
                })
            }
        }
    }

    onManageChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aContinueForm.controls['notSelectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
        if (isChecked) {
            control.push(this._fb.group({
                serviceTypeId: types
            }))
            if (typeValue  === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyManage = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue  === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyManage = false;
                this.part2aContinueForm.patchValue({
                    specificationManage: null
                })
            }
        }
    }

    onPaymentChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aContinueForm.controls['notSelectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
        if (isChecked) {
            control.push(this._fb.group({
                serviceTypeId: types
            }))
            if (typeValue  === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyPayment = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue  === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyPayment = false;
                this.part2aContinueForm.patchValue({
                    specificationProcess: null
                })
            }
        }
    }

    savepart2aContinue() {
        this.setOtherResponse();
        this.part2aContinue.id = this.part2aContinueForm.value.id;
        this.part2aContinue.notAssessedServiceName = this.part2aContinueForm.value.notAssessedServiceName;
        this.part2aContinue.notAssessedServiceType = this.part2aContinueForm.value.notAssessedServiceType;
        this.part2aContinue.comment = this.part2aContinueForm.value.comment;
        return new Promise((resolve, reject) => {
            this._part2aContinueService.update(this.part2aContinue).subscribe(
                    (part2aContinue) => {
                        if (this.part2aContinueForm.value.notSelectedServiceType !== null) {
                            this._part2aContinueService.createNotTypeList(this.part2aContinueForm.value.notSelectedServiceType).subscribe(
                                    () => {
                                        this._part2aContinueService.createOtherList(this.part2aContinueForm.value.otherServiceResponse, this.SCOPE.NOT_SELECTED).subscribe(
                                                () => {
                                                    resolve(part2aContinue);
                                                }, (error) => {
                                                    reject(error);
                                                });
                                    }, (error) => {
                                        reject(error);
                                    });
                        } else {
                            resolve(part2aContinue);
                        }
                    },
                    (error) => {
                        reject(error);
                    });
        });
    }

    setOtherResponse() {
        let typeValue: any;
        this.part2aContinueForm.setControl('otherServiceResponse', this._fb.array([]));
        this.controlOther = <FormArray>this.part2aContinueForm.controls['otherServiceResponse'];
        this.part2aContinueForm.value.notSelectedServiceType.map((types) => {
            typeValue = (types.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
            if (typeValue  === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                    && this.part2aContinueForm.value.specificationHost !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aContinueForm.value.specificationHost
            }))
            }
            if (typeValue  === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                    && this.part2aContinueForm.value.specificationOther !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aContinueForm.value.specificationOther
            }))
            }
            if (typeValue  === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                    && this.part2aContinueForm.value.specificationManage !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aContinueForm.value.specificationManage
            }))
            }
            if (typeValue  === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                    && this.part2aContinueForm.value.specificationProcess !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aContinueForm.value.specificationProcess
            }))
            }
        })
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    routeToNext() {
        this.router.navigate(['/saq/executive-summary/payment-card-business']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/scope-verification']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    saveAndContinue() {
        this.savepart2aContinue().then((part2aContinue) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this.routeToNext();
                this.store.dispatch({type: CARD_DESCRIPTION});
                this._toastr.success(`The scope verification has been successfully saved`, `Save`);
            }
        }).catch((error) => {
            this._toastr.error(`Error saving scope verification: ${error.message}`, `Error`);
        });
    }

    saveAndExit() {
        this.savepart2aContinue().then((part2aContinue) => {
            this._toastr.success(`The scope verification has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving scope verification: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.savepart2aContinue().then(
                (part2aContinue) => {
                    this._toastr.success(`The scope verification has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving scope verification: ${error.message}`, `Error`);
                });
    }

    forward() {
        if (this.part2aContinueForm && this.part2aContinueForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    back() {
        if (this.part2aContinueForm && this.part2aContinueForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }
    ngOnDestroy() {
        if (this.statesSubscription) {
            this.statesSubscription.unsubscribe();
        }
    }
}
