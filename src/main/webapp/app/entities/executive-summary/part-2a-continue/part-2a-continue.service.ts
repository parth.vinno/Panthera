import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ScopeVerificationNotSelectedService, ServiceType, ServiceTypeNotSelected, OtherServiceResponse, SCOPE } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2aContinueService {

    private resourceUrl = 'api/scope-verification-not-selected-services/service-provider';
    private resourceTypesUrl = 'api/service-categories/service-provider';
    private resourceNotSelectedTypesUrl = 'api/service-type-not-selected/list/service-provider';
    private resourceSelectedTypesReadUrl = 'api/selected-service-types/service-provider';
    private resourceSelectedOtherUrl = 'api/other-service-responses/list/service-provider';
    private resourceNotSelectedTypesReadUrl = 'api/service-type-not-selected/service-provider';
    private resourceOtherReadUrl = 'api/other-service-responses/service-provider';

    constructor(private http: Http) { }

    create(scopeVerificationNotSelectedService: ScopeVerificationNotSelectedService):
        Observable<ScopeVerificationNotSelectedService> {
        const copy = this.convert(scopeVerificationNotSelectedService);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createNotTypeList(serviceTypeNotSelected: ServiceTypeNotSelected[]):
        Observable<ServiceTypeNotSelected[]> {
        return this.http.post(this.resourceNotSelectedTypesUrl, serviceTypeNotSelected).map((res: Response) => {
            return res.json();
        });
    }

    createOtherList(otherServiceResponse: OtherServiceResponse[], scope: SCOPE):
        Observable<OtherServiceResponse[]> {
        return this.http.post(`${this.resourceSelectedOtherUrl}/${scope}`, otherServiceResponse).map((res: Response) => {
            return res.json();
        });
    }

    update(scopeVerificationNotSelectedService: ScopeVerificationNotSelectedService):
        Observable<ScopeVerificationNotSelectedService> {
        const copy = this.convert(scopeVerificationNotSelectedService);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ScopeVerificationNotSelectedService> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSelectedTypesReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryNotSelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceNotSelectedTypesReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryOther(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceOtherReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCategory(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceTypesUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(scopeVerificationNotSelectedService: ScopeVerificationNotSelectedService): ScopeVerificationNotSelectedService {
        const copy: ScopeVerificationNotSelectedService = Object.assign({}, scopeVerificationNotSelectedService);
        return copy;
    }
}
