import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { ToastrService } from 'ngx-toastr';
import {BsModalService} from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';

import { MerchantBusinessType, MerchantBusinessTypeSelected, PaymentChannel, PaymentChannelSaqCovers, PaymentChannelBusinessServes } from '../executive-summary.model';
import { Part2aMerchantService } from './part-2a-merchant.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2a-merchant',
    templateUrl: './part-2a-merchant.component.html',
    styleUrls: ['./part-2a-merchant.scss', '../fa-arrows.scss']
})
export class Part2aMerchantComponent implements OnInit, OnDestroy {
    part2aMerchantForm: FormGroup;
    businessType: MerchantBusinessType[];
    paymentChannel: PaymentChannel[] = [];
    paymentChannelForServe: PaymentChannel[] = [];
    paymentChannelForCover: PaymentChannel[] = [];
    merchantBusinessTypeSelected: MerchantBusinessTypeSelected[];
    paymentChannelBusinessServes: PaymentChannelBusinessServes[];
    paymentChannelSaqCover: PaymentChannelSaqCovers[];
    typeSelected: any = [];
    selected: any = [];
    tenantId: any;
    specify = false;
    state: Observable<string>;
    curStates: string;
    stateSubscription;

constructor(private _part2aMerchantService: Part2aMerchantService,
        private _fb: FormBuilder,
        private router: Router,
        private _ngZone: NgZone,
        private _bsModalService: BsModalService,
        private _storageService: StorageService,
        private _toastr: ToastrService,
        private store: Store<string>) {
    this.state = store.select('state');
    this.stateSubscription = this.state.subscribe(
        (curStates) => {
            this.curStates = curStates;
        });
}

ngOnInit() {
    this.part2aMerchantForm = this._fb.group({
        specification: [null, [CustomValidators.specification]],
        merchantBusinessTypeSelected: this._fb.array([]),
        paymentChannelBusinessServes: this._fb.array([]),
        paymentChannelSaqCovers: this._fb.array([])
    })
    this.tenantId = this._storageService.getTenantId();
//  console.log('this.tenant', this.tenantId);
    this._ngZone.run(() => {
        this._part2aMerchantService.queryBusinessType().subscribe((res: ResponseWrapper) => {
            const businessType = res.json;
            if (businessType.length) {
                this.businessType = businessType;
                businessType.map((x, i) => {
                    this.businessType[i] = {};
                    this.businessType[i].id = x.id
                    this.businessType[i].businessType = x.businessType
                    this.businessType[i].checked = false;
                })
                this._part2aMerchantService.query().subscribe((resM: ResponseWrapper) => {
                    const control = <FormArray>this.part2aMerchantForm.controls['merchantBusinessTypeSelected'];
                    const selected: any = [];
                    const typeSelected = resM.json;
                    if (typeSelected.length) {
                        typeSelected.map((types) => {
                            const merchantType = types.merchantBusinessTypeId.businessType.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                            if (merchantType === 'Others (please specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                this.part2aMerchantForm.patchValue({
                                    specification: types.specification
                                })
                            }
                            control.push(this._fb.group({
                                id: types.id,
                                merchantBusinessTypeId: types.merchantBusinessTypeId,
                                specification: types.specification
                            }))
                            selected.push(types.merchantBusinessTypeId);
                        });
                        if (selected.length > 0) {
                            for (let i = 0; i < selected.length; i++ ) {
                                const selectedMerchantType = (selected[i].businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                if (selectedMerchantType === 'Others (please specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                    this.specify = true;
                                }
                                for (let j = 0; j < this.businessType.length; j++ ) {
                                    if (this.businessType[j].id === selected[i].id) {
                                        this.businessType[j].checked = true;
                                    }
                                }
                            }
                        };
                    }
                }, (resM: ResponseWrapper) => {
                    this.onError(resM.json);
                })
            }
        }, (res: ResponseWrapper) => {
            this.onError(res.json);
        })
        this._part2aMerchantService.queryChannel().subscribe((res: ResponseWrapper) => {
            const channel = res.json;
            if (channel.length) {
                channel.map((x, i) => {
                    this.paymentChannelForServe[i] = {};
                    this.paymentChannelForServe[i].id = x.id
                    this.paymentChannelForServe[i].name = x.name
                    this.paymentChannelForServe[i].checked = false;
                    this.paymentChannelForCover[i] = {};
                    this.paymentChannelForCover[i].id = x.id
                    this.paymentChannelForCover[i].name = x.name
                    this.paymentChannelForCover[i].checked = false;
                })
                this._part2aMerchantService.queryServe().subscribe((resServe: ResponseWrapper) => {
                    const servecontrol = <FormArray>this.part2aMerchantForm.controls['paymentChannelBusinessServes'];
                    const serves = resServe.json;
                    const selected: any = [];
                    if (serves.length) {
                        serves.map((serve) => {
                            servecontrol.push(this._fb.group({
                                id: serve.id,
                                paymentChannelId: serve.paymentChannelId
                            }))
                            selected.push(serve.paymentChannelId);
                        });
                    }
                    this.isChannelChecked(this.paymentChannelForServe, selected)
                }, (resServe: ResponseWrapper) => {
                    this.onError(resServe.json);
                })
                this._part2aMerchantService.queryCover().subscribe((resCover: ResponseWrapper) => {
                    const convercontrol = <FormArray>this.part2aMerchantForm.controls['paymentChannelSaqCovers'];
                    const covers = resCover.json;
                    const selected: any = [];
                    if (covers.length) {
                        covers.map((cover) => {
                            convercontrol.push(this._fb.group({
                                id: cover.id,
                                paymentChannelId: cover.paymentChannelId
                            }))
                            selected.push(cover.paymentChannelId);
                        });
                    }
                    this.isChannelChecked(this.paymentChannelForCover, selected)
                }, (resCover: ResponseWrapper) => {
                    this.onError(resCover.json);
                })
            }
        }, (res: ResponseWrapper) => {
            this.onError(res.json);
        })
    });
}

private onError(error) {
    this._toastr.error(`${error.message}`, `Error`);
}

isChannelChecked(channel, selected) {
    if (selected.length > 0) {
        for (let i = 0; i < selected.length; i++ ) {
            for (let j = 0; j < channel.length; j++ ) {
                if (channel[j].id === selected[i].id) {
                    channel[j].checked = true;
                }
            }
        }
    }
}

onTypeChange(types, isChecked: boolean) {
    const control = <FormArray>this.part2aMerchantForm.controls['merchantBusinessTypeSelected'];
    let merchantType = '';
    if (isChecked) {
        control.push(this._fb.group({
            merchantBusinessTypeId: types,
            specification: null
        }))
        merchantType = (types.businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        if (merchantType === 'Others (please specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
            this.specify = true;
        }
    } else {
        const index = control.controls.findIndex((x) => x.value.merchantBusinessTypeId.id === types.id);
        control.removeAt(index);
        merchantType = (types.businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        if (merchantType === 'Others (please specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
            this.specify = false;
        }
    }
}

onChannelChange(channel, isChecked: boolean) {
    const control = <FormArray>this.part2aMerchantForm.controls['paymentChannelBusinessServes'];
    if (isChecked) {
        control.push(this._fb.group({
            paymentChannelId: channel
        }))
    } else {
        const index = control.controls.findIndex((x) => x.value.paymentChannelId.id === channel.id);
        control.removeAt(index);
    }
}

onChannelCoversChange(channel, isChecked: boolean) {
    const control = <FormArray>this.part2aMerchantForm.controls['paymentChannelSaqCovers'];
    if (isChecked) {
        control.push(this._fb.group({
            paymentChannelId: channel
        }))
    } else {
        const index = control.controls.findIndex((x) => x.value.paymentChannelId.id === channel.id);
        control.removeAt(index);
    }
}

savePartMerchant2a() {
    this.part2aMerchantForm.value.merchantBusinessTypeSelected.map((types) => {
        const merchantType = (types.merchantBusinessTypeId.businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        if (merchantType === 'Others (please specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase() && this.part2aMerchantForm.value.specification !== null) {
            types.specification = this.part2aMerchantForm.value.specification;
        }
    })
    if (this.part2aMerchantForm.value.merchantBusinessTypeSelected !== null && this.part2aMerchantForm.value.merchantBusinessTypeSelected.length > 0) {
        return new Promise((resolve, reject) => {
            this._part2aMerchantService.createList(this.part2aMerchantForm.value.merchantBusinessTypeSelected).subscribe(
                    (part2a) => {
                        if (this.part2aMerchantForm.value.paymentChannelBusinessServes !== null && this.part2aMerchantForm.value.paymentChannelBusinessServes.length > 0) {
                            this._part2aMerchantService.createServeList(this.part2aMerchantForm.value.paymentChannelBusinessServes).subscribe(
                                    () => {
                                        if (this.part2aMerchantForm.value.paymentChannelSaqCovers !== null && this.part2aMerchantForm.value.paymentChannelSaqCovers.length > 0) {
                                            this._part2aMerchantService.createCoverList(this.part2aMerchantForm.value.paymentChannelSaqCovers).subscribe(
                                                    () => {
                                                        resolve(part2a);
                                                    }, (error) => {
                                                        reject(error);
                                                    });
                                        } else {
                                            resolve(part2a);
                                        }
                                    }, (error) => {
                                        reject(error);
                                    });
                        } else {
                            resolve(part2a);
                        }
                    },
                    (error) => {
                        reject(error);
                    });
        });
    }
}

routeToNext() {
    this.router.navigate(['/saq/executive-summary/payment-card-business']);
}

routeToSummary() {
    this.router.navigate(['/saq/executive-summary/summary']);
}

saveAndContinue() {
    this.savePartMerchant2a().then((part2a) => {
        if (this.curStates === 'SUMMARY') {
            this.routeToSummary();
            this._toastr.success(`The information has been successfully updated`, `Update`);
        } else {
            this.store.dispatch({type: CARD_DESCRIPTION});
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.routeToNext();
        }
    }).catch((error) => {
        this._toastr.error(`Error saving information: ${error.message}`, `Error`);
    });
}

saveAndExit() {
    this.savePartMerchant2a().then((part2a) => {
        this._toastr.success(`The information has been successfully saved`, `Save`);
        this.router.navigate(['/saq/assessment-home']);
    }).catch((error) => {
        this._toastr.error(`Error saving information: ${error.message}`, `Error`);
    });
}

forward() {
    if (this.part2aMerchantForm && this.part2aMerchantForm.dirty) {
        this.openConfirmationModal(DIRECTION.FORWARD);
    } else {
        this.routeToNext();
    }
}

openConfirmationModal(direction: DIRECTION) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.saveAndContinue();
        } else if (result === false) {
            this.routeToNext();
        }
    });
}

ngOnDestroy() {
    if (this.stateSubscription) {
        this.stateSubscription.unsubscribe();
    }
}
}
