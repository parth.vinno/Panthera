import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { MerchantBusinessType, MerchantBusinessTypeSelected, PaymentChannel, PaymentChannelBusinessServes, PaymentChannelSaqCovers } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2aMerchantService {

    private resourceUrl = 'api/merchant-business-type-selected';
    private resourceTypeUrl = 'api/merchant-business-types';
    private resourceChannelUrl = 'api/payment-channels';
    private resourceChannelServeUrl = 'api/payment-channel-business-serves';
    private resourceChannelCoverUrl = 'api/payment-channel-saq-covers';

    constructor(private http: Http) { }

    create(merchantBusinessTypeSelected: MerchantBusinessTypeSelected):
        Observable<MerchantBusinessTypeSelected> {
        const copy = this.convert(merchantBusinessTypeSelected);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createList(merchantBusinessTypeSelected: MerchantBusinessTypeSelected[]):
        Observable<MerchantBusinessTypeSelected[]> {
        return this.http.post(`${this.resourceUrl}/list`, merchantBusinessTypeSelected).map((res: Response) => {
            return res.json();
        });
    }

    createServeList(paymentChannelBusinessServes: PaymentChannelBusinessServes[]):
        Observable<PaymentChannelBusinessServes[]> {
        return this.http.post(`${this.resourceChannelServeUrl}/list`, paymentChannelBusinessServes).map((res: Response) => {
            return res.json();
        });
    }

    createCoverList(paymentChannelSaqCovers: PaymentChannelSaqCovers[]):
        Observable<PaymentChannelSaqCovers[]> {
        return this.http.post(`${this.resourceChannelCoverUrl}/list`, paymentChannelSaqCovers).map((res: Response) => {
            return res.json();
        });
    }

    update(merchantBusinessTypeSelected: MerchantBusinessTypeSelected):
        Observable<MerchantBusinessTypeSelected> {
        const copy = this.convert(merchantBusinessTypeSelected);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<MerchantBusinessTypeSelected> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    queryBusinessType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceTypeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryChannel(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceChannelUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryServe(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceChannelServeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCover(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceChannelCoverUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(merchantBusinessTypeSelected: MerchantBusinessTypeSelected): MerchantBusinessTypeSelected {
        const copy: MerchantBusinessTypeSelected = Object.assign({}, merchantBusinessTypeSelected);
        return copy;
    }
}
