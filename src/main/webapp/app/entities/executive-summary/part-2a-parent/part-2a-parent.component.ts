import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiAlertService } from 'ng-jhipster';

import { Principal, ResponseWrapper, StorageService } from '../../../shared';

@Component({
    selector: 'jhi-part-2a-parent',
    templateUrl: './part-2a-parent.component.html'
})
export class Part2aParentComponent implements OnInit {
    tenantId: any;
    constructor(private _storageService: StorageService) {
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
    }
}
