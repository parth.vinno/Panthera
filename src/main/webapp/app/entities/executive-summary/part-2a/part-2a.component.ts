import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { ScopeVerificationSelectedService, HostingProvider, ManagedService, ExecutiveSummaryStatus,
    PaymentProcessing, ServiceType, SelectedServiceType, ServiceCategory, OtherServiceResponse, ServiceTypeNotSelected, SCOPE} from '../executive-summary.model';
import { Part2aService } from './part-2a.service';
import { CustomValidators } from '../validators/custom-validators';
import { ExecutiveSummaryStatusService } from '../executive-summary-status.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-part-2a',
    templateUrl: './part-2a.component.html',
    styleUrls: ['./part-2a.component.scss', '../fa-arrows.scss']
})
export class Part2aComponent implements OnInit, OnDestroy {
    part2aForm: FormGroup;
    part2a: ScopeVerificationSelectedService;
    selectedServiceType: SelectedServiceType[];
    otherServiceResponse: OtherServiceResponse[];
    hostingProvider: ServiceType[];
    managedService: ServiceType[];
    paymentProcessing: ServiceType[];
    NotSelectedServiceType: ServiceTypeNotSelected[];
    tenantId: any;
    totalItems: any;
    queryCount: any;
    specify = false;
    specifyManage = false;
    specifyPayment = false;
    specifyHost = false;
    controlOther;
    state: Observable<string>;
    curStates: string;
    statesSubscription;
    SCOPE;

    constructor(private _part2aService: Part2aService,
            private _executiveSummaryStatusService: ExecutiveSummaryStatusService,
            private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _bsModalService: BsModalService,
            private _storageService: StorageService,
            private _toastr: ToastrService,
            private store: Store<string>) {
        this.part2a = new ScopeVerificationSelectedService();
        this.SCOPE = SCOPE;
        this.state = store.select('state');
        this.statesSubscription = this.state.subscribe(
                (curStates) => {
                    this.curStates = curStates;
                });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this._ngZone.run(() => {
            this._part2aService.queryNotSelectedType().subscribe((resNotSelected: ResponseWrapper) => {
                this.totalItems = resNotSelected.headers.get('X-Total-Count');
                this.queryCount = this.totalItems;
                this._part2aService.queryNotSelectedType({size: this.queryCount}).subscribe((resNotSelected2a: ResponseWrapper) => {
                    this.NotSelectedServiceType = resNotSelected2a.json;
                    this._part2aService.queryCategory().subscribe((res: ResponseWrapper) => {
                        const serviceCategory = res.json;
                        let categories: any;
                        if (serviceCategory.length) {
                            serviceCategory.map((category) => {
                                categories = (category.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                if (categories === 'Hosting Provider'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                    // this.hostingProvider = category.serviceTypes;
                                    this.hostingProvider = category.serviceTypes.filter((array_el) => {
                                        return this.NotSelectedServiceType.filter((anotherOne_el) => {
                                            return anotherOne_el.serviceTypeId.id === array_el.id;
                                        }).length === 0
                                    });
                                    this.hostingProvider.map((x, i) => {
                                        this.hostingProvider[i] = {};
                                        this.hostingProvider[i].id = x.id;
                                        this.hostingProvider[i].serviceType = x.serviceType;
                                        this.hostingProvider[i].checked = false;
                                    })
                                    this.sortServiceType(this.hostingProvider);
                                } else if (categories === 'Managed Services (specify)'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                    this.managedService = category.serviceTypes.filter((array_el) => {
                                        return this.NotSelectedServiceType.filter((anotherOne_el) => {
                                            return anotherOne_el.serviceTypeId.id === array_el.id;
                                        }).length === 0
                                    });
                                    this.managedService.map((x, i) => {
                                        this.managedService[i] = {};
                                        this.managedService[i].id = x.id;
                                        this.managedService[i].serviceType = x.serviceType;
                                        this.managedService[i].checked = false;
                                    })
                                    this.sortServiceType(this.managedService);
                                } else if (categories === 'Payment Processing'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                    this.paymentProcessing = category.serviceTypes.filter((array_el) => {
                                        return this.NotSelectedServiceType.filter((anotherOne_el) => {
                                            return anotherOne_el.serviceTypeId.id === array_el.id;
                                        }).length === 0
                                    });
                                    this.paymentProcessing.map((x, i) => {
                                        this.paymentProcessing[i] = {};
                                        this.paymentProcessing[i].id = x.id;
                                        this.paymentProcessing[i].serviceType = x.serviceType;
                                        this.paymentProcessing[i].checked = false;
                                    })
                                    this.sortServiceType(this.paymentProcessing);
                                }
                            })
                        }
                    }, (res: ResponseWrapper) => {
                        this.onError(res.json)
                    });
                    this._part2aService.query().subscribe((res2a: ResponseWrapper) => {
                        const scopeVerify = res2a.json;
                        this.part2a = scopeVerify;
                        if (scopeVerify.length) {
                            this.part2aForm.patchValue({
                                id: scopeVerify[0].id,
                                assessedServiceName: scopeVerify[0].assessedServiceName,
                                assessedServiceType: scopeVerify[0].assessedServiceType
                            });
                        }
                        this._part2aService.querySelectedType().subscribe((resType: ResponseWrapper) => {
                            this.totalItems = resType.headers.get('X-Total-Count');
                            this.queryCount = this.totalItems;
                            this._part2aService.querySelectedType({size: this.queryCount}).subscribe((resTypes: ResponseWrapper) => {
                                const control = <FormArray>this.part2aForm.controls['selectedServiceType'];
                                const types = resTypes.json;
                                const selected: any = [];
                                if (types.length > 0) {
                                    types.map((stype) => {
                                        control.push(this._fb.group({
                                            id: stype.id,
                                            serviceTypeId: stype.serviceTypeId
                                        }))
                                        selected.push(stype.serviceTypeId);
                                    });
                                }
                                this.isTypesChecked(this.hostingProvider, selected);
                                this.isTypesChecked(this.managedService, selected);
                                this.isTypesChecked(this.paymentProcessing, selected);
                                this._part2aService.queryOther().subscribe(
                                        (resOther: ResponseWrapper) => {
                                            const controlOther = <FormArray>this.part2aForm.controls['otherServiceResponse'];
                                            const others = resOther.json;
                                            let otherValue: any;
                                            others.map((other) => {
                                                otherValue = (other.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                                if (otherValue === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                        && other.scope === this.SCOPE.SELECTED) {
                                                    this.part2aForm.patchValue({
                                                        specificationHost: other.other
                                                    })
                                                    this.specifyHost = true;
                                                } else if (otherValue === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                        && other.scope === this.SCOPE.SELECTED) {
                                                    this.part2aForm.patchValue({
                                                        specificationOther: other.other
                                                    })
                                                    this.specify = true;
                                                } else if (otherValue === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                        && other.scope === this.SCOPE.SELECTED) {
                                                    this.part2aForm.patchValue({
                                                        specificationManage: other.other
                                                    })
                                                    this.specifyManage = true;
                                                } else if (otherValue === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
                                                        && other.scope === this.SCOPE.SELECTED) {
                                                    this.part2aForm.patchValue({
                                                        specificationProcess: other.other
                                                    })
                                                    this.specifyPayment = true;
                                                }
                                                controlOther.push(this._fb.group({
                                                    id: other.id,
                                                    other: other.other,
                                                    serviceTypeId: other.serviceTypeId
                                                }))
                                            })
                                        },
                                        (resOther: ResponseWrapper) => this.onError(resOther.json));
                            }, (resTypes: ResponseWrapper) => this.onError(resTypes.json)) },
                            (resType: ResponseWrapper) => this.onError(resType.json))
                    }, (res2a: ResponseWrapper) => this.onError(res2a.json))
                }, (resNotSelected2a: ResponseWrapper) => {
                    this.onError(resNotSelected2a.json)
                });
            }, (resNotSelected: ResponseWrapper) => {
                this.onError(resNotSelected.json)
            });
        });
        this.part2aForm = this._fb.group({
            id: null,
            assessedServiceName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.assessedServiceName]],
            assessedServiceType: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.assessedServiceType]],
            specificationOther: [null, [CustomValidators.specificationOther]],
            specificationManage: [null, [CustomValidators.specificationManage]],
            specificationHost: [null, [CustomValidators.specificationHost]],
            specificationProcess: [null, [CustomValidators.specificationProcess]],
            selectedServiceType: this._fb.array([]),
            otherServiceResponse: this._fb.array([])
        })
    }

    isTypesChecked(channel, selected) {
        if (selected.length > 0) {
            for (let i = 0; i < selected.length; i++ ) {
                for (let j = 0; j < channel.length; j++ ) {
                    if (channel[j].id === selected[i].id) {
                        channel[j].checked = true;
                    }
                }
            }
        }
    }

    sortServiceType(types) {
        types.sort( function(t1, t2) {
            if ( t1.id < t2.id ) {
                return -1;
            } else if ( t1.id > t2.id ) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    onHostChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aForm.controls['selectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        if (isChecked) {
            control.push(this._fb.group({
                id: null,
                serviceTypeId: types
            }))
            if (typeValue === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyHost = true;
            }
            if (typeValue === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specify = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyHost = false;
                this.part2aForm.patchValue({
                    specificationHost: null
                })
            }
            if (typeValue === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specify = false;
                this.part2aForm.patchValue({
                    specificationOther: null
                })
            }
        }
    }

    onManageChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aForm.controls['selectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
        if (isChecked) {
            control.push(this._fb.group({
                id: null,
                serviceTypeId: types
            }))
            if (typeValue === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyManage = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyManage = false;
                this.part2aForm.patchValue({
                    specificationManage: null
                })
            }
        }
    }

    onPaymentChange(types, isChecked: boolean) {
        const control = <FormArray>this.part2aForm.controls['selectedServiceType'];
        const typeValue = (types.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()
        if (isChecked) {
            control.push(this._fb.group({
                id: null,
                serviceTypeId: types
            }))
            if (typeValue === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyPayment = true;
            }
        } else {
            const index = control.controls.findIndex((x) => x.value.serviceTypeId.id === types.id);
            control.removeAt(index);
            if (typeValue === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                this.specifyPayment = false;
                this.part2aForm.patchValue({
                    specificationProcess: null
                })
            }
        }
    }

    savePart2a() {
        this.setOtherResponse();
        this.part2a.id = this.part2aForm.value.id;
        this.part2a.assessedServiceName = this.part2aForm.value.assessedServiceName;
        this.part2a.assessedServiceType = this.part2aForm.value.assessedServiceType;
            return new Promise((resolve, reject) => {
                this._part2aService.update(this.part2a).subscribe(
                        (part2a) => {
                            if (this.part2aForm.value.selectedServiceType !== null) {
                                this._part2aService.createTypeList(this.part2aForm.value.selectedServiceType).subscribe(
                                        () => {
                                            this._part2aService.createOtherList(this.part2aForm.value.otherServiceResponse, this.SCOPE.SELECTED).subscribe(
                                                    () => {
                                                        resolve(part2a);
                                                    }, (error) => {
                                                        reject(error);
                                                    });
                                        }, (error) => {
                                            reject(error);
                                        });
                            } else {
                                resolve(part2a);
                            }
                        },
                        (error) => {
                            reject(error);
                        });
            });
    }

    setOtherResponse() {
        this.part2aForm.setControl('otherServiceResponse', this._fb.array([]));
        this.controlOther = <FormArray>this.part2aForm.controls['otherServiceResponse'];
        let typeValue: any;
        this.part2aForm.value.selectedServiceType.map((types) => {
            typeValue = (types.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
            if (typeValue === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase() && this.part2aForm.value.specificationHost !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aForm.value.specificationHost
            }))
            }
            if (typeValue === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase() && this.part2aForm.value.specificationOther !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aForm.value.specificationOther
            }))
            }
            if (typeValue === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase() && this.part2aForm.value.specificationManage !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aForm.value.specificationManage
            }))
            }
            if (typeValue === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase() && this.part2aForm.value.specificationProcess !== null) {
                this.controlOther.push(this._fb.group({
                    serviceTypeId: types.serviceTypeId,
                    other: this.part2aForm.value.specificationProcess
            }))
            }
        })
    }

    routeToNext() {
        this.router.navigate(['/saq/executive-summary/scope-verification-continue']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    saveAndContinue() {
        this.savePart2a().then((part2a) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this.store.dispatch({type: NOT_SELECTED_SERVICES});
                this.routeToNext();
                this._toastr.success(`The scope verification has been successfully saved`, `Save`);
            }
        }).catch((error) => {
            this._toastr.error(`Error saving scope verification: ${error.message}`, `Error`);
        });
    }

    saveAndExit() {
        this.savePart2a().then((part2a) => {
            this._toastr.success(`The scope verification has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving scope verification: ${error.message}`, `Error`);
        });
    }

    forward() {
        if (this.part2aForm && this.part2aForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.saveAndContinue();
            } else if (result === false) {
                this.routeToNext();
            }
        });
    }

    ngOnDestroy() {
        if (this.statesSubscription) {
            this.statesSubscription.unsubscribe();
        }
    }
}
