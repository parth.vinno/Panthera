import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ScopeVerificationSelectedService, ServiceType, SelectedServiceType, ServiceTypeNotSelected, OtherServiceResponse, SCOPE } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2aService {

    private resourceUrlSp = 'api/executive-summary-statuses/status/service-provider';
    private resourceUrl = 'api/scope-verification-selected-services/service-provider';
    private resourceTypesUrl = 'api/service-categories/service-provider';
    private resourceSelectedTypesUrl = 'api/selected-service-types/list/service-provider';
    private resourceNotSelectedTypesUrl = 'api/service-type-not-selected/list/service-provider';
    private resourceSelectedOtherUrl = 'api/other-service-responses/list/service-provider';
    private resourceSelectedTypesReadUrl = 'api/selected-service-types/service-provider';
    private resourceOtherReadUrl = 'api/other-service-responses/service-provider';
    private resourceServiceTypeReadUrl = 'api/service-types/service-provider';
    private resourceNotSelectedTypesReadUrl = 'api/service-type-not-selected/service-provider';

    constructor(private http: Http) { }

    create(scopeVerificationSelectedService: ScopeVerificationSelectedService):
        Observable<ScopeVerificationSelectedService> {
        const copy = this.convert(scopeVerificationSelectedService);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createTypeList(selectedServiceType: SelectedServiceType[]):
        Observable<SelectedServiceType[]> {
        return this.http.post(this.resourceSelectedTypesUrl, selectedServiceType).map((res: Response) => {
            return res.json();
        });
    }

    createOtherList(otherServiceResponse: OtherServiceResponse[], scope: SCOPE):
        Observable<OtherServiceResponse[]> {
        return this.http.post(`${this.resourceSelectedOtherUrl}/${scope}`, otherServiceResponse).map((res: Response) => {
            return res.json();
        });
    }

    update(scopeVerificationSelectedService: ScopeVerificationSelectedService):
        Observable<ScopeVerificationSelectedService> {
        const copy = this.convert(scopeVerificationSelectedService);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    UpdateOtherList(otherServiceResponse: OtherServiceResponse[]):
        Observable<OtherServiceResponse[]> {
        const copy = this.convert(otherServiceResponse);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ScopeVerificationSelectedService> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryStatus(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSp, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSelectedTypesReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryOther(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceOtherReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCategory(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceTypesUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryTypes(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceServiceTypeReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryNotSelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceNotSelectedTypesReadUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(scopeVerificationSelectedService: ScopeVerificationSelectedService): ScopeVerificationSelectedService {
        const copy: ScopeVerificationSelectedService = Object.assign({}, scopeVerificationSelectedService);
        return copy;
    }
}
