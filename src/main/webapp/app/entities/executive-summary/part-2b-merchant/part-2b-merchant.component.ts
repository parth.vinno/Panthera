import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { PaymentCardBusinessDescription } from '../executive-summary.model';
import { Part2bMerchantService } from './part-2b-merchant.service';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import {
     DIRECTION, YesNoModalComponent, StorageService
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2b-merchant',
    templateUrl: './part-2b-merchant.component.html',
    styleUrls: ['../executive-summary-shared.component.scss', '../fa-arrows.scss']
})
export class Part2bMerchantComponent implements OnInit, OnDestroy {
    tenantId: any;
    part2bMerchnatForm: FormGroup;
state: Observable<string>;
curStates: string;
stateSubscription;

    constructor(private router: Router,
            private _part2bMerchantService: Part2bMerchantService,
            private _ngZone: NgZone,
            private _fb: FormBuilder,
            private _bsModalService: BsModalService,
            private _storageService: StorageService,
            private _toastr: ToastrService,
            private store: Store<string>) {
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this.part2bMerchnatForm = this._fb.group({
            id: null,
            descriptionTransmit: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.descriptionTransmit]]
        })
        this._ngZone.run(() => {
            this._part2bMerchantService.queryMerchant().subscribe(
                    (res: ResponseWrapper) => {
                        const part2b = res.json;
                        if (part2b.length) {
                            this.part2bMerchnatForm.setValue({
                                id: part2b[0].id,
                                descriptionTransmit:  part2b[0].descriptionTransmit
                            });
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    savePart2b() {
        if (this.part2bMerchnatForm.value.id !== undefined || this.part2bMerchnatForm.value.id !== null) {
            return new Promise((resolve, reject) => {
               this._part2bMerchantService.update(this.part2bMerchnatForm.value).subscribe(
                       (part2b) => {
                           resolve(part2b);
                       },
                       (error) => {
                           reject(error);
                       });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._part2bMerchantService.create(this.part2bMerchnatForm.value).subscribe(
                        (part2b) => {
                            resolve(part2b);
                        },
                        (error) => {
                            reject(error);
                        });
             });
        }
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    routeToNext() {
        this.router.navigate(['/saq/executive-summary/locations']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/scope-verification']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    saveAndContinue() {
        this.savePart2b().then((part2b) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this.store.dispatch({type: LOCATIONS});
                this._toastr.success(`The information has been successfully saved`, `Save`);
                this.routeToNext();
            }
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndExit() {
        this.savePart2b().then((part2b) => {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            console.log('error');
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    forward() {
        if (this.part2bMerchnatForm && this.part2bMerchnatForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    saveAndPrev() {
        this.savePart2b().then(
                (part2b) => {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving information: ${error.message}`, `Error`);
                });
    }

    back() {
        if (this.part2bMerchnatForm && this.part2bMerchnatForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }
    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
