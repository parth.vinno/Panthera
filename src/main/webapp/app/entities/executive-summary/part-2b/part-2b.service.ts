import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PaymentCardBusinessDescription } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2bService {

    private resourceUrl = 'api/payment-card-business-descriptions';
    private resourceUrlSP = 'api/payment-card-business-descriptions/service-provider';

    constructor(private http: Http) { }

    create(paymentCardBusinessDescription: PaymentCardBusinessDescription): Observable<PaymentCardBusinessDescription> {
        const copy = this.convert(paymentCardBusinessDescription);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createSP(paymentCardBusinessDescription: PaymentCardBusinessDescription): Observable<PaymentCardBusinessDescription> {
        const copy = this.convert(paymentCardBusinessDescription);
        return this.http.post(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(paymentCardBusinessDescription: PaymentCardBusinessDescription): Observable<PaymentCardBusinessDescription> {
        const copy = this.convert(paymentCardBusinessDescription);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateSP(paymentCardBusinessDescription: PaymentCardBusinessDescription): Observable<PaymentCardBusinessDescription> {
        const copy = this.convert(paymentCardBusinessDescription);
        return this.http.put(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PaymentCardBusinessDescription> {
        return this.http.get(`${this.resourceUrlSP}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

//    query(req?: any): Observable<ResponseWrapper> {
//        const options = createRequestOption(req);
//        return this.http.get(this.resourceUrl, options)
//            .map((res: Response) => this.convertResponse(res));
//    }

    querySP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSP, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlSP}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(paymentCardBusinessDescription: PaymentCardBusinessDescription): PaymentCardBusinessDescription {
        const copy: PaymentCardBusinessDescription = Object.assign({}, paymentCardBusinessDescription);
        return copy;
    }
}
