import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { NgForm } from '@angular/forms';
import { BsModalService} from 'ngx-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import {Store} from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { Location } from '../executive-summary.model';
import { Part2cService } from './part-2c.service';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2c',
    templateUrl: './part-2c.component.html',
    styleUrls: ['./part-2c.component.scss', '../fa-arrows.scss']
})
export class Part2cComponent implements OnInit, OnDestroy {

    part2cForm: FormGroup;
    part2c: Location;
    state: Observable<string>;
    curStates: string;
    stateSubscription;

constructor(private _fb: FormBuilder,
        private router: Router,
        private _ngZone: NgZone,
        private _part2cService: Part2cService,
        private _bsModalService: BsModalService,
        private _eventManager: BsModalService,
        private _toastr: ToastrService,
        private store: Store<string>) {
    this.state = store.select('state');
    this.stateSubscription = this.state.subscribe(
        (curStates) => {
            this.curStates = curStates;
        });
}

ngOnInit() {
    this.part2cForm = this._fb.group({
        locations: this._fb.array([])
    })
    this._ngZone.run(() => {
        this._part2cService.querySP().subscribe(
                (res: ResponseWrapper) => {
                    const control = <FormArray>this.part2cForm.controls['locations'];
                    const part2c = res.json;
                    if (part2c.length) {
                        part2c.map((location) => {
                            control.push(this._fb.group({
                                id: location.id,
                                facilityType: [location.facilityType, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.facilityType]],
                                numberOfFacilityType: [location.numberOfFacilityType, [Validators.required, CustomValidators.numberOfFacilityType]],
                                location: [location.location, [Validators.required, Validators.minLength(3), Validators.maxLength(128),  CustomValidators.location]]
                            }))
                        });
                    }
                },
                (res: ResponseWrapper) => this.onError(res.json));
    });
}

get locations(): FormArray {
    return this.part2cForm.get('locations') as FormArray;
};

initLocationRows() {
    return this._fb.group({
        id: null,
        facilityType: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.facilityType]],
        numberOfFacilityType: [null, [Validators.required, CustomValidators.numberOfFacilityType]],
        location: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128),  CustomValidators.location]]
    });
}

deleteLocation(index) {
    this.openConfirmationModalToDelete(index);
}

addLocation() {
    const control = <FormArray>this.part2cForm.controls['locations'];
    control.push(this.initLocationRows());
}

savePart2c() {
    if (this.part2cForm.value.locations.id !== undefined) {
        return new Promise((resolve, reject) => {
            this._part2cService.updateSPList(this.part2cForm.value.locations).subscribe(
                    (part2c) => {
                        resolve(part2c);
                    },
                    (error) => {
                        reject(error);
                    });
        });
    } else {
        return new Promise((resolve, reject) => {
            this._part2cService.createSPList(this.part2cForm.value.locations).subscribe(
                    (part2c) => {
                        resolve(part2c);
                    },
                    (error) => {
                        reject(error);
                    });
        });
    }
}

private onError(error) {
    this._toastr.error(`${error.message}`, `Error`);
}

routeToNext() {
    this.router.navigate(['/saq/executive-summary/payment-applications']);
}

routeToPrev() {
    this.router.navigate(['/saq/executive-summary/payment-card-business']);
}

routeToSummary() {
    this.router.navigate(['/saq/executive-summary/summary']);
}

saveAndContinue() {
    this.savePart2c().then((part2c) => {
        if (this.curStates === 'SUMMARY') {
            this.routeToSummary();
            this._toastr.success(`The information has been successfully updated`, `Update`);
        } else {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.routeToNext();
            this.store.dispatch({type: PAYMENT_APPLICATIONS});
        }
    }).catch((error) => {
        this._toastr.error(`Error saving information: ${error.message}`, `Error`);
    });
}

saveAndExit() {
    this.savePart2c().then((part2c) => {
        this._toastr.success(`The information has been successfully saved`, `Save`);
        this.router.navigate(['/saq/assessment-home']);
    }).catch((error) => {
        this._toastr.error(`Error saving information: ${error.message}`, `Error`);
    });
}

saveAndPrev() {
    this.savePart2c().then(
            (part2c) => {
                this._toastr.success(`The information has been successfully saved`, `Save`);
                this.routeToPrev();
            }, (error) => {
                this._toastr.error(`Error saving information: ${error.message}`, `Error`);
            });
}

forward() {
    if (this.part2cForm && this.part2cForm.dirty) {
        this.openConfirmationModal(DIRECTION.FORWARD);
    } else {
        this.routeToNext();
    }
}

back() {
    if (this.part2cForm && this.part2cForm.dirty) {
        this.openConfirmationModal(DIRECTION.BACK);
    } else {
        this.routeToPrev();
    }
}

openConfirmationModal(direction: DIRECTION) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm',
            'You have unsaved changes. Do you want to save?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            if (direction === DIRECTION.BACK) {
                this.saveAndPrev();
            } else if (direction === DIRECTION.FORWARD) {
                this.saveAndContinue();
            }
        } else if (result === false) {
            if (direction === DIRECTION.BACK) {
                this.routeToPrev();
            } else if (direction === DIRECTION.FORWARD) {
                this.routeToNext();
            }
        }
    });
}

openConfirmationModalToDelete(index) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm',
            'Are you sure you want to delete the location?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            const control = <FormArray>this.part2cForm.controls['locations'];
            if (this.part2cForm.value.locations[index].id === null) {
                control.removeAt(index);
            } else {
                this._part2cService.querySP().subscribe((res: ResponseWrapper) => {
                    const part2cToDelete = res.json;
                    if (part2cToDelete.length) {
                        control.removeAt(index);
                        this._part2cService.delete(part2cToDelete[index].id).subscribe((response) => {
                            this._toastr.success('the Location has been removed', 'Delete');
                        });
                    }
                });
            }
        } else if (result === false) {
            return false;
        }
    });
}

ngOnDestroy() {
    if (this.stateSubscription) {
        this.stateSubscription.unsubscribe();
    }
}
}
