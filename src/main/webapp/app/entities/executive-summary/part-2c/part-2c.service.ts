import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Location } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2cService {

    private resourceUrl = 'api/locations';
    private resourceUrlList = 'api/locations/list/service-provider';
    private resourceUrlSP = 'api/locations/service-provider';
    private resourceUrlSPList = 'api/locations/list/service-provider';

    constructor(private http: Http) { }

    create(location: Location): Observable<Location> {
        const copy = this.convert(location);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(location: Location): Observable<Location> {
        const copy = this.convert(location);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createSPList(location: Location[]): Observable<Location[]> {
        return this.http.post(this.resourceUrlSPList, location).map((res: Response) => {
            return res.json();
        });
    }

    updateSPList(location: Location[]): Observable<Location[]> {
        const copy = this.convert(location);
        return this.http.put(this.resourceUrlSPList, location).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Location> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSP, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlSP}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(location: Location): Location {
        const copy: Location = Object.assign({}, location);
        return copy;
    }
}
