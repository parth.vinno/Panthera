import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import {Store} from '@ngrx/store';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { PaymentApplication, PaymentApplicationList, PAYMENTAPPLICATION_RADIO } from '../executive-summary.model';
import { Part2dService } from './part-2d.service';
import { PaymentApplicationListService } from '../payment-application-list.service';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2d',
    templateUrl: './part-2d.component.html',
    styleUrls: ['./part-2d.component.scss', '../fa-arrows.scss']
})
export class Part2dComponent implements OnInit, OnDestroy {
    part2dForm: FormGroup;
    part2d: PaymentApplication;
    datePicker;
    selectedDate: NgbDateStruct;
    minDate;
    PAYMENTAPPLICATION_RADIO;
    state: Observable<string>;
    curStates: string;
    stateSubscription;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _part2dService: Part2dService,
            private _paymentApplicationListService: PaymentApplicationListService,
            private _toastr: ToastrService,
            private store: Store<string>) {
        this.part2d = new PaymentApplication();
        const curDate = new Date();
        this.minDate = {year: curDate.getFullYear(), month: curDate.getMonth() + 1, day: curDate.getDate()};
        this.PAYMENTAPPLICATION_RADIO = PAYMENTAPPLICATION_RADIO;
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.part2dForm = this._fb.group({
            id: null,
            isPaymentApplicationMoreThanOne: false,
            paymentApplicationLists: this._fb.array([])
        });
        this._ngZone.run(() => {
            this._part2dService.querySP().subscribe(
                    (res: ResponseWrapper) => {
                        const control = <FormArray>this.part2dForm.controls['paymentApplicationLists'];
                        const part2d = res.json;
                        if (part2d.length) {
                            this.part2dForm.patchValue({
                                id: part2d[0].id,
                                isPaymentApplicationMoreThanOne: part2d[0].isPaymentApplicationMoreThanOne
                            })
                            part2d[0].paymentApplicationLists.map((payment) => {
                                const date = this._dateFormatter.parse(payment.expiryDate);
                                control.push(this._fb.group({
                                    id: payment.id,
                                    paymentApplicationName: [payment.paymentApplicationName, [Validators.minLength(3), Validators.maxLength(128),
                                                                                              CustomValidators.paymentApplicationName]],
                                    versionNumber: [payment.versionNumber, [Validators.minLength(1), Validators.maxLength(255), CustomValidators.versionNumber]],
                                    applicationVendor: [payment.applicationVendor, [Validators.minLength(3), Validators.maxLength(255), CustomValidators.applicationVendor]],
                                    isApplicationPadssListed: payment.isApplicationPadssListed,
                                    expiryDate: date
                                }))
                            });
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    get paymentApplicationLists(): FormArray {
        return this.part2dForm.get('paymentApplicationLists') as FormArray;
    };

    initPaymentApplicationRows() {
        return this._fb.group({
                    id: null,
                    paymentApplicationName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.paymentApplicationName]],
                    versionNumber: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(255), CustomValidators.versionNumber]],
                    applicationVendor: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(255), CustomValidators.applicationVendor]],
                    isApplicationPadssListed: false,
                    expiryDate: {year: null, month: null, day: null}
        });
    }

    deletePaymentApplication(index) {
        this.openConfirmationModalToDelete(index);
    }

    addPaymentApplication() {
        const control = <FormArray>this.part2dForm.controls['paymentApplicationLists'];
        control.push(this.initPaymentApplicationRows());
    }

    savePart2d() {
        this.updateValue();
        if (this.part2dForm.value.id !== undefined) {
            return new Promise((resolve, reject) => {
                this._part2dService.updateSP(this.part2dForm.value).subscribe(
                        (part2d) => {
                            resolve(part2d);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        } else {
            return new Promise((resolve, reject) => {
                    this._part2dService.createSP(this.part2dForm.value).subscribe(
                        (part2d) => {
                                resolve(part2d);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        }
    }

    updateValue() {
        const controlD = <FormArray>this.part2dForm.controls['paymentApplicationLists'];
        controlD.controls.map(
                (control, i) => {
                    control.patchValue({
                        expiryDate: this._dateFormatter.format(this.part2dForm.value.paymentApplicationLists[i].expiryDate)
                    });
                }
        )
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    routeToNext() {
        this.router.navigate(['/saq/executive-summary/environment']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/locations']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    saveAndContinue() {
        this.savePart2d().then((part2d) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this._toastr.success(`The information has been successfully saved`, `Save`);
                this.routeToNext();
                this.store.dispatch({type: ENV_DESCRIPTION});
            }
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndExit() {
        this.savePart2d().then((part2d) => {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.savePart2d().then(
                (part2d) => {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving information: ${error.message}`, `Error`);
                });
    }

    forward() {
        if (this.part2dForm && this.part2dForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    back() {
      if (this.part2dForm && this.part2dForm.dirty) {
          this.openConfirmationModal(DIRECTION.BACK);
      } else {
          this.routeToPrev();
      }
  }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    openConfirmationModalToDelete(index) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'Are you sure you want to delete the payment application information?'
        );
        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                const control = <FormArray>this.part2dForm.controls['paymentApplicationLists'];
                control.removeAt(index);
                this._toastr.warning('Please save your changes to reflect deletion', 'Warning');
            } else if (result === false) {
                return false;
            }
        });
    }
    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
