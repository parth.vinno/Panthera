import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PaymentApplication, PaymentApplicationList } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2dService {

    private resourceUrl = 'api/payment-applications';
    private resourceUrlSP = 'api/payment-applications/service-provider';

    constructor(private http: Http) { }

    create(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
        const copy = this.convert(paymentApplication);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
        const copy = this.convert(paymentApplication);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createSP(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
        const copy = this.convert(paymentApplication);
        return this.http.post(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateSP(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
        const copy = this.convert(paymentApplication);
        return this.http.put(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PaymentApplication> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSP, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlSP}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(paymentApplication: PaymentApplication): PaymentApplication {
        const copy: PaymentApplication = Object.assign({}, paymentApplication);
        return copy;
    }
}
