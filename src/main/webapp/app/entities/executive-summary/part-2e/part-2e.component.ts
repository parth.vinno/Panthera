import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { EnvironmentDescription } from '../executive-summary.model';
import { Part2eService } from './part-2e.service';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2e',
    templateUrl: './part-2e.component.html',
    styleUrls: ['./part-2e.component.scss', '../fa-arrows.scss']
})
export class Part2eComponent implements OnInit, OnDestroy {
    part2eForm: FormGroup;
    part2e: EnvironmentDescription;
state: Observable<string>;
curStates: string;
stateSubscription;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _part2eService: Part2eService,
            private _ngZone: NgZone,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService,
            private store: Store<string>) {
        this.part2e = new EnvironmentDescription();
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.part2eForm = this._fb.group({
            id: null,
            description: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(225), CustomValidators.description]],
            networkSegmented: false
        });
        this._ngZone.run(() => {
            this._part2eService.query().subscribe(
                    (res: ResponseWrapper) => {
                        const part2e = res.json;
                        if (part2e.length) {
                            this.part2eForm.patchValue({
                                id: part2e[0].id,
                                description: part2e[0].description,
                                networkSegmented: part2e[0].networkSegmented
                            })
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    savePart2e() {
        if (this.part2eForm.value.id !== undefined) {
            return new Promise((resolve, reject) => {
                this._part2eService.update(this.part2eForm.value).subscribe(
                        (part2e) => {
                            resolve(part2e);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._part2eService.create(this.part2eForm.value).subscribe(
                        (part2e) => {
                            resolve(part2e);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        }
    }

    routeToNext() {
        this.router.navigate(['/saq/executive-summary/third-party-service-providers']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/payment-applications']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    saveAndContinue() {
        this.savePart2e().then((part2e) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this._toastr.success(`The information has been successfully saved`, `Save`);
                this.routeToNext();
                this.store.dispatch({type: SERVICE_PROVIDERS});
            }
        }).catch((error) => {
            console.log('error');
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.savePart2e().then(
                (part2e) => {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving information: ${error.message}`, `Error`);
                });
    }

    saveAndExit() {
        this.savePart2e().then((part2e) => {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    forward() {
        if (this.part2eForm && this.part2eForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    back() {
        if (this.part2eForm && this.part2eForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm!!!',
                'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }
    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
