import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS, SUMMARY } from '../ex-summary-reducer.service';

import { ThirdPartyServiceProvider } from '../executive-summary.model';
import { ServiceByServiceProvider } from '../executive-summary.model';
import { Part2fService } from './part-2f.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { ExecutiveSummaryStatus, EXECUTIVESTATUS} from '../executive-summary.model';
import { ExecutiveSummaryStatusService } from '../executive-summary-status.service';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2f',
    templateUrl: './part-2f.component.html',
    styleUrls: ['./part-2f.component.scss', '../fa-arrows.scss']
})
export class Part2fComponent implements OnInit, OnDestroy {
    part2fForm: FormGroup;
    part2f: ThirdPartyServiceProvider;
    tenantId: any;
    state: Observable<string>;
    curStates: string;
    stateSubscription;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _part2fService: Part2fService,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService,
            private _storageService: StorageService,
            private store: Store<string>,
            private _executiveSummaryStatusService: ExecutiveSummaryStatusService) {
        this.part2f = new ThirdPartyServiceProvider();
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this.part2fForm = this._fb.group({
            id: null,
            qirRelationshipPresent: false,
            qirCompanyName: [null, [Validators.minLength(3), Validators.maxLength(128), CustomValidators.qirCompanyName]],
            qirIndividualName: [null, [Validators.minLength(3), Validators.maxLength(128), CustomValidators.qirIndividualName]],
            serviceDescription: [null, [Validators.minLength(3), Validators.maxLength(128), CustomValidators.serviceDescription]],
            serviceProviderRelationShipPresent: false,
            serviceByServiceProviders: this._fb.array([])
        });
        this._ngZone.run(() => {
            this._part2fService.query().subscribe(
                    (res: ResponseWrapper) => {
                        const part2f = res.json;
                        const control = <FormArray>this.part2fForm.controls['serviceByServiceProviders'];
                        if (part2f.length) {
                            this.part2f = part2f[0];
                            this.part2fForm.patchValue({
                                id: part2f[0].id,
                                qirRelationshipPresent: part2f[0].qirRelationshipPresent,
                                qirCompanyName: part2f[0].qirCompanyName,
                                qirIndividualName: part2f[0].qirIndividualName,
                                serviceDescription: part2f[0].serviceDescription,
                                serviceProviderRelationShipPresent: part2f[0].serviceProviderRelationShipPresent
                            });
                                part2f[0].serviceByServiceProviders.map((sp) => {
                                    control.push(this._fb.group({
                                        id: sp.id,
                                        providerName: [sp.providerName, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.providerName]],
                                        descriptionOfServiceProvider: [sp.descriptionOfServiceProvider, [Validators.required, Validators.minLength(3), Validators.maxLength(128),
                                                                                                         CustomValidators.providerName]]
                                    }))
                                });
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
        });
        this.part2fForm.controls['qirRelationshipPresent'].valueChanges.subscribe((value) => {
            if (value === false) {
                this.part2fForm.patchValue({
                    qirCompanyName: null,
                    qirIndividualName: null,
                    serviceDescription: null,
                });
            } else {
                if (this.part2f) {
                    this.part2fForm.patchValue({
                        qirCompanyName: this.part2f.qirCompanyName,
                        qirIndividualName: this.part2f.qirIndividualName,
                        serviceDescription: this.part2f.serviceDescription,
                    });
                }
            }
        });
    }

    private onError(error) {
        this._toastr.error(`${error.message}`, `Error`);
    }

    get serviceByServiceProviders(): FormArray {
        return this.part2fForm.get('serviceByServiceProviders') as FormArray;
    };

    initServiceByServiceProviderRows() {
        return this._fb.group({
            id: null,
            providerName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.providerName]],
            descriptionOfServiceProvider: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.descriptionOfServiceProvider]]
        });
    }

    deleteServiceByServiceProvider(index) {
        this.openConfirmationModalToDelete(index);
    }

    addServiceByServiceProvider() {
        const control = <FormArray>this.part2fForm.controls['serviceByServiceProviders'];
        control.push(this.initServiceByServiceProviderRows());
    }

    savePart2f() {
        if (this.part2fForm.value.id !== undefined) {
            return new Promise((resolve, reject) => {
                this._part2fService.update(this.part2fForm.value).subscribe(
                        (part2f) => {
                            resolve(part2f);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._part2fService.create(this.part2fForm.value).subscribe(
                        (part2f) => {
                            resolve(part2f);
                        },
                        (error) => {
                            reject(error);
                        });
            });
        }
    }

    routeToNext() {
        if (this.tenantId.includes('merchant_saqd')) {
            this.router.navigate(['/saq/executive-summary/summary']);
        } else if (this.tenantId.includes('serviceprovider_saqd')) {
            this.router.navigate(['/saq/executive-summary/summary-of-requirements-tested']);
        }
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/environment']);
    }

    routeToSummary() {
        this.router.navigate(['/saq/executive-summary/summary']);
    }

    saveAndContinue() {
        this.savePart2f().then((part2f) => {
            if (this.tenantId.includes('merchant_saqd')) {
                if (this.curStates === 'SUMMARY') {
                    this.routeToSummary();
                    this._toastr.success(`The information has been successfully updated`, `Update`);
                } else {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.subscribeToSaveResponse(this._executiveSummaryStatusService.createSummary({}, 'SUMMARY'));
                    this.store.dispatch({type: SUMMARY});
                    this.routeToSummary();
                }
            } else if (this.tenantId.includes('serviceprovider_saqd')) {
                if (this.curStates === 'SUMMARY') {
                    this.routeToNext();
                    this._toastr.success(`The information has been successfully updated`, `Update`);
                } else {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToNext();
                    this.store.dispatch({type: TESTED_REQUIREMENTS});
                }
            }
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    private subscribeToSaveResponse(result: Observable<ExecutiveSummaryStatus>) {
        result.subscribe((res: ExecutiveSummaryStatus) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ExecutiveSummaryStatus) {
    }

    private onSaveError(error) {
        try {
            error.json();
            } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    saveAndExit() {
        this.savePart2f().then((part2f) => {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.savePart2f().then(
                (part2f) => {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving information: ${error.message}`, `Error`);
                });
    }

    forward() {
        if (this.part2fForm && this.part2fForm.dirty) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    back() {
        if (this.part2fForm && this.part2fForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    openConfirmationModalToDelete(index) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'Are you sure you want to delete the payment application information?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                const control = <FormArray>this.part2fForm.controls['serviceByServiceProviders'];
                control.removeAt(index);
                this._toastr.warning('Please save your changes to reflect deletion', 'Warning');
            } else if (result === false) {
                return false;
            }
        });
    }
    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
