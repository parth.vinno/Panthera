import {Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS, SUMMARY } from '../ex-summary-reducer.service';

import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { ASSESSED, TestedRequirementSummary, TestedRequirementHeader, PcidssRequirement } from '../executive-summary.model';
import { Part2gService } from './part-2g.service';
import { PcidssRequirementService } from '../../saq-admin/pcidss-requirement';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import { ExecutiveSummaryStatus, EXECUTIVESTATUS} from '../executive-summary.model';
import { ExecutiveSummaryStatusService } from '../executive-summary-status.service';
import {
    DIRECTION, YesNoModalComponent, StorageService
} from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-part-2g',
    templateUrl: './part-2g.component.html',
    styleUrls: ['./part-2g.component.scss', '../fa-arrows.scss']
})
export class Part2gComponent implements OnInit, OnDestroy {
    part2gForm: FormGroup;
    part2g: TestedRequirementHeader;
    testedRequirement: Array<TestedRequirementSummary> = [];
    ASSESSED;
    state: Observable<string>;
    curStates: string;
    testedSummary: any = [];
    stateSubscription;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private store: Store<string>,
            private _part2gService: Part2gService,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService,
            private _pcidssRequirementService: PcidssRequirementService,
            private _storageService: StorageService,
            private _executiveSummaryStatusService: ExecutiveSummaryStatusService) {
         this.ASSESSED = ASSESSED;
         this.state = store.select('state');
         this.stateSubscription = this.state.subscribe(
             (curStates) => {
                 this.curStates = curStates;
             });
    }

    ngOnInit() {
        this.part2gForm = this._fb.group({
            id: null,
            serviceAssessedName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(128), CustomValidators.serviceAssessedName]],
            testedRequirementSummaries: this._fb.array([])
        });
        this._ngZone.run(() => {
            this._part2gService.queryHeader().subscribe(
                    (res: ResponseWrapper) => {
                        const testedHeader = res.json;
                       if (testedHeader.length) {
                           this.part2gForm.patchValue({
                               id: testedHeader[0].id,
                               serviceAssessedName: testedHeader[0].serviceAssessedName
                           });
                           if (testedHeader[0].testedRequirementSummaries.length > 0) {
                               this.part2gForm.setControl('testedRequirementSummaries', this._fb.array([]));
                               const control = <FormArray>this.part2gForm.controls['testedRequirementSummaries'];
                               this.sortSummary(testedHeader[0].testedRequirementSummaries);
                               testedHeader[0].testedRequirementSummaries.map((summary) => {
                                   if (/[A-Z]/g.test(summary.pcidssRequirementId.requirementNumber)) {
                                       summary.ReqOrAppendix = 'Appendix';
                                   } else {
                                       summary.ReqOrAppendix = 'Requirement';
                                   }
                                   control.push(this._fb.group({
                                       id: summary.id,
                                       assessed: summary.assessed,
                                       justification: summary.justification,
                                       pcidssRequirementId: summary.pcidssRequirementId,
                                       ReqOrAppendix: summary.ReqOrAppendix
                                   }))
                               });
                           }
                       } else {
                           this._pcidssRequirementService.query().subscribe(
                                   (resReq: ResponseWrapper) => {
                                       const pcidssRequirement = resReq.json;
                                       const control = <FormArray>this.part2gForm.controls['testedRequirementSummaries'];
                                       for ( let i = 0; i < pcidssRequirement.length; i++) {
                                           this.testedRequirement[i] = {};
                                           this.testedRequirement[i].pcidssRequirementId = pcidssRequirement[i];
                                           if (/[A-Z]/g.test(pcidssRequirement[i].requirementNumber)) {
                                               this.testedRequirement[i].ReqOrAppendix = 'Appendix';
                                           } else {
                                               this.testedRequirement[i].ReqOrAppendix = 'Requirement';
                                           }
                                           control.push(this._fb.group({
                                               assessed: [this.testedRequirement[i].assessed],
                                               justification: this.testedRequirement[i].justification,
                                               pcidssRequirementId: this.testedRequirement[i].pcidssRequirementId,
                                               ReqOrAppendix: this.testedRequirement[i].ReqOrAppendix
                                           }))
                                       }
                                   },
                                   (resReq: ResponseWrapper) => this.onError(resReq.json));
                       }
                       },
                       (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    sortSummary(summary) {
        summary.sort( function(t1, t2) {
            if ( t1.pcidssRequirementId.id < t2.pcidssRequirementId.id ) {
                return -1;
            } else if ( t1.pcidssRequirementId.id > t2.pcidssRequirementId.id ) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    private onError(error) {
        alert('Error retrieving tested requiremnet : ' + error.message);
    }

    get testedRequirementSummaries(): FormArray {
        return this.part2gForm.get('testedRequirementSummaries') as FormArray;
    };

    onChangeFull(req, isChecked: boolean) {
        if (isChecked) {
            req.controls['justification'].setValue(null);
        }
    }

    onChangePartial(req, isChecked: boolean) {
        if (isChecked) {
            req.controls['justification'].setValue(null);
        }
    }

    onChangeNone(req, isChecked: boolean) {
        if (isChecked) {
            req.controls['justification'].setValue(null);
        }
    }

    savePart2g() {
       if (this.part2gForm.value.id !== undefined) {
           return new Promise((resolve, reject) => {
               this._part2gService.updateHeader(this.part2gForm.value).subscribe(
                       (part2g) => {
                           resolve(part2g);
                       },
                       (error) => {
                           reject(error);
                       });
           });
       } else {
           return new Promise((resolve, reject) => {
               this._part2gService.createHeader(this.part2gForm.value).subscribe(
                       (part2g) => {
                           resolve(part2g);
                       },
                       (error) => {
                           reject(error);
                       });
           });
       }
    }

    routeToSummary() {
       this.router.navigate(['/saq/executive-summary/summary']);
    }

    routeToPrev() {
        this.router.navigate(['/saq/executive-summary/third-party-service-providers']);
    }

    saveAndContinue() {
        this.savePart2g().then((part2g) => {
            if (this.curStates === 'SUMMARY') {
                this.routeToSummary();
                this._toastr.success(`The information has been successfully updated`, `Update`);
            } else {
                this._toastr.success(`The information has been successfully saved`, `Save`);
                this.subscribeToSaveResponse(this._executiveSummaryStatusService.createSummary({}, 'SUMMARY'));
                this.store.dispatch({type: SUMMARY});
                this.routeToSummary();
            }
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    private subscribeToSaveResponse(result: Observable<ExecutiveSummaryStatus>) {
        result.subscribe((res: ExecutiveSummaryStatus) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ExecutiveSummaryStatus) {
    }

    private onSaveError(error) {
        try {
            error.json();
            } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    saveAndExit() {
        this.savePart2g().then((part2g) => {
            this._toastr.success(`The information has been successfully saved`, `Save`);
            this.router.navigate(['/saq/assessment-home']);
        }).catch((error) => {
            this._toastr.error(`Error saving information: ${error.message}`, `Error`);
        });
    }

    saveAndPrev() {
        this.savePart2g().then(
                (part2g) => {
                    this._toastr.success(`The information has been successfully saved`, `Save`);
                    this.routeToPrev();
                }, (error) => {
                    this._toastr.error(`Error saving information: ${error.message}`, `Error`);
                });
    }

    back() {
        if (this.part2gForm && this.part2gForm.dirty) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );
        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                }
            }
        });
    }
    ngOnDestroy() {
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
