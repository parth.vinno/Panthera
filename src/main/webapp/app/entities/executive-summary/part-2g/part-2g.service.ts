import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { TestedRequirementSummary, TestedRequirementHeader } from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class Part2gService {

    private resourceUrl = 'api/tested-requirement-summaries';
    private resourceHeaderUrl = 'api/tested-requirement-header/service-provider';

    constructor(private http: Http) { }

    createHeader(testedRequirementHeader: TestedRequirementHeader): Observable<TestedRequirementHeader> {
        const copy = this.convert(testedRequirementHeader);
        return this.http.post(this.resourceHeaderUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateHeader(testedRequirementHeader: TestedRequirementHeader): Observable<TestedRequirementHeader> {
        const copy = this.convert(testedRequirementHeader);
        return this.http.put(this.resourceHeaderUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    create(testedRequirementSummary: TestedRequirementSummary[]): Observable<TestedRequirementSummary> {
        const copy = this.convert(testedRequirementSummary);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(testedRequirementSummary: TestedRequirementSummary): Observable<TestedRequirementSummary> {
        const copy = this.convert(testedRequirementSummary);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<TestedRequirementSummary> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryHeader(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceHeaderUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(testedRequirementSummary: TestedRequirementSummary): TestedRequirementSummary {
        const copy: TestedRequirementSummary = Object.assign({}, testedRequirementSummary);
        return copy;
    }
}
