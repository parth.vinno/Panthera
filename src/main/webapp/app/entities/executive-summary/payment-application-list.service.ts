import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { PaymentApplicationList } from './executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentApplicationListService {

    private resourceUrl = 'api/payment-application-lists';
    private resourceUrlSP = 'api/payment-application-lists/service-provider';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(paymentApplicationList: PaymentApplicationList): Observable<PaymentApplicationList> {
        const copy = this.convert(paymentApplicationList);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createSP(paymentApplicationList: PaymentApplicationList): Observable<PaymentApplicationList> {
        const copy = this.convert(paymentApplicationList);
        return this.http.post(this.resourceUrlSP, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(paymentApplicationList: PaymentApplicationList): Observable<PaymentApplicationList> {
        const copy = this.convert(paymentApplicationList);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateSP(paymentApplicationList: PaymentApplicationList): Observable<PaymentApplicationList> {
        const copy = this.convert(paymentApplicationList);
        return this.http.put(this.resourceUrlSP, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PaymentApplicationList> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSP, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.expiryDate = this.dateUtils
            .convertLocalDateFromServer(entity.expiryDate);
    }

    private convert(paymentApplicationList: PaymentApplicationList): PaymentApplicationList {
        const copy: PaymentApplicationList = Object.assign({}, paymentApplicationList);
        copy.expiryDate = this.dateUtils
            .convertLocalDateToServer(paymentApplicationList.expiryDate);
        return copy;
    }
}
