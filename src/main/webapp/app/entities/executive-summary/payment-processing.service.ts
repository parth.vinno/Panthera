import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PaymentProcessing } from './executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentProcessingService {

    private resourceUrl = 'api/payment-processings';

    constructor(private http: Http) { }

    create(paymentProcessing: PaymentProcessing): Observable<PaymentProcessing> {
        const copy = this.convert(paymentProcessing);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(paymentProcessing: PaymentProcessing): Observable<PaymentProcessing> {
        const copy = this.convert(paymentProcessing);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PaymentProcessing> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(paymentProcessing: PaymentProcessing): PaymentProcessing {
        const copy: PaymentProcessing = Object.assign({}, paymentProcessing);
        return copy;
    }
}
