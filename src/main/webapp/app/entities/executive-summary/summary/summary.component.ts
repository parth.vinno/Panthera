import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {Store} from '@ngrx/store';
import {SELECTED_SERVICES, NOT_SELECTED_SERVICES, MERCHANT_BUSINESS, CARD_DESCRIPTION,
    LOCATIONS, PAYMENT_APPLICATIONS, ENV_DESCRIPTION, SERVICE_PROVIDERS, TESTED_REQUIREMENTS } from '../ex-summary-reducer.service';

import { SummaryService } from './summary.service';
import { ResponseWrapper, createRequestOption, StorageService } from '../../../shared';
import { ScopeVerificationSelectedService, SelectedServiceType, OtherServiceResponse, ScopeVerificationNotSelectedService,
    ThirdPartyServiceProvider, ServiceTypeNotSelected, MerchantBusinessTypeSelected, PaymentChannelBusinessServes, SCOPE,
    PaymentChannelSaqCovers, PaymentCardBusinessDescription, ServiceType, Location, PaymentApplication, EnvironmentDescription, ServiceCategory,
    TestedRequirementHeader, TestedRequirementSummary} from '../executive-summary.model';
import {
    DIRECTION, YesNoModalComponent
} from '../../../shared';

@Component({
    selector: 'jhi-ex-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss', '../fa-arrows.scss']
})
export class SummaryComponent implements OnInit {

    scopeVerificationSelectedService: ScopeVerificationSelectedService;
    scopeVerificationNotSelectedService: ScopeVerificationNotSelectedService;
    selectedServiceType: SelectedServiceType[];
    serviceCategory: ServiceCategory[];
    hostingProvider: ServiceType[];
    managedService: SelectedServiceType[];
    paymentProcessing: SelectedServiceType[];
    otherServiceResponse: OtherServiceResponse[];
    thirdPartyServiceProvider: ThirdPartyServiceProvider;
    serviceTypeNotSelected: ServiceTypeNotSelected[];
    merchantBusinessTypeSelected: MerchantBusinessTypeSelected[];
    paymentChannelBusinessServes: PaymentChannelBusinessServes[];
    paymentChannelSaqCovers: PaymentChannelSaqCovers[];
    paymentCardBusinessDescription: PaymentCardBusinessDescription;
    location: Location[];
    paymentApplication: PaymentApplication;
    environmentDescription: EnvironmentDescription;
    testedRequirementHeader: TestedRequirementHeader;
    testedRequirementSummary: TestedRequirementSummary[];
    selectedOtherResponse: any = [];
    notSelectedOtherResponse: any = [];
    tenantId: any;
    totalItems: any;
    queryCount: any;
    state: Observable<string>;
    curStates: string;
    stateSubscription;
    hostingProviderTypes = [];
    managedServiceTypes = [];
    paymentProcessTypes = [];
    notHostingProviderTypes = [];
    notManagedServiceTypes = [];
    notPaymentProcessTypes = [];
    SCOPE;

    constructor(private _fb: FormBuilder,
            private router: Router,
            private _ngZone: NgZone,
            private _summaryService: SummaryService,
            private _dateFormatter: NgbDateParserFormatter,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService,
            private _storageService: StorageService,
            private store: Store<string>) {
        this.SCOPE = SCOPE;
        this.state = store.select('state');
        this.stateSubscription = this.state.subscribe(
            (curStates) => {
                this.curStates = curStates;
            });
    }

    ngOnInit() {
        this.tenantId = this._storageService.getTenantId();
        this._ngZone.run(() => {
            if (this.tenantId.includes('serviceprovider_saqd')) {
            this._summaryService.queryScopeSelected().subscribe(
                    (res: ResponseWrapper) => {
                       const scopeVerificationSelectedService =  res.json
                       if (scopeVerificationSelectedService.length > 0) {
                           this.scopeVerificationSelectedService = scopeVerificationSelectedService[0];
                       }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryScopeNotSelected().subscribe(
                    (res: ResponseWrapper) => {
                        const scopeVerificationNotSelectedService = res.json
                        if (scopeVerificationNotSelectedService.length > 0) {
                            this.scopeVerificationNotSelectedService = scopeVerificationNotSelectedService[0];
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.querySelectedType().subscribe(
                  (resp1: ResponseWrapper) => {
                      this.totalItems = resp1.headers.get('X-Total-Count');
                      this.queryCount = this.totalItems;
                      this._summaryService.queryOtherService().subscribe(
                              (res: ResponseWrapper) => {
                                  const otherServiceResponse = res.json
                                  if (otherServiceResponse.length > 0) {
                                      this.otherServiceResponse = otherServiceResponse;
                                      this.otherServiceResponse.map((other) => {
                                          if (other.scope === this.SCOPE.SELECTED) {
                                              this.selectedOtherResponse.push(other);
                                          }
                                      });
                                  }
                              },
                              (res: ResponseWrapper) => this.onError(res.json));
                      this._summaryService.querySelectedType({size: this.queryCount}).subscribe(
                              (resp: ResponseWrapper) => {
                                  const selectedServiceType = resp.json;
                                  if (selectedServiceType.length) {
                                      selectedServiceType.map((types) => {
                                          const category = (types.serviceTypeId.serviceCategoryId.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                          const serviceTypeName = (types.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                          if (category === 'Hosting Provider'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                              if (serviceTypeName === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                  let otherHostResponse = [];
                                                  otherHostResponse = this.selectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                  if (otherHostResponse.length) {
                                                      this.hostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherHostResponse[0].other})
                                                  }
                                              } else if (serviceTypeName === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                  let otherResponse = [];
                                                  otherResponse = this.selectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                  if (otherResponse.length) {
                                                      this.hostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherResponse[0].other})
                                                  }
                                              } else {
                                                  this.hostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                              }
                                          } else if (category === 'Managed Services (specify)'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                              if (serviceTypeName === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                  let otherManageResponse = [];
                                                  otherManageResponse = this.selectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                  if (otherManageResponse.length) {
                                                      this.managedServiceTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherManageResponse[0].other});
                                                  }
                                              } else {
                                                  this.managedServiceTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                              }
                                          } else if (category === 'Payment Processing'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                              if (serviceTypeName === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                  let otherProcessResponse = [];
                                                  otherProcessResponse = this.selectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                  if (otherProcessResponse.length) {
                                                      this.paymentProcessTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherProcessResponse[0].other});
                                                  }
                                              } else {
                                                  this.paymentProcessTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                              }
                                          }
                                      });
                                  }
                              },   (resp: ResponseWrapper) => this.onError(resp.json));
                  },   (resp1: ResponseWrapper) => this.onError(resp1.json));
            this._summaryService.queryNotSelectedType().subscribe(
                    (resp1: ResponseWrapper) => {
                        this.totalItems = resp1.headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;
                        this._summaryService.queryOtherService().subscribe(
                                (res: ResponseWrapper) => {
                                    const otherServiceResponse = res.json
                                    if (otherServiceResponse.length > 0) {
                                        this.otherServiceResponse = otherServiceResponse;
                                        this.otherServiceResponse.map((other) => {
                                            if (other.scope === this.SCOPE.NOT_SELECTED) {
                                                this.notSelectedOtherResponse.push(other);
                                            }
                                        });
                                    }
                                },
                                (res: ResponseWrapper) => this.onError(res.json));
                        this._summaryService.queryNotSelectedType({size: this.queryCount}).subscribe(
                                (resp: ResponseWrapper) => {
                                    const notSelectedServiceType = resp.json;
                                    if (notSelectedServiceType.length) {
                                        notSelectedServiceType.map((types) => {
                                            const category = (types.serviceTypeId.serviceCategoryId.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                            const serviceTypeName = (types.serviceTypeId.serviceType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                                            if (category === 'Hosting Provider'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                if (serviceTypeName === 'Other Hosting (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                    let otherHostResponse = [];
                                                    otherHostResponse = this.notSelectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                    if (otherHostResponse.length) {
                                                        this.notHostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherHostResponse[0].other});
                                                    }
                                                } else if (serviceTypeName === 'Others (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                    let otherResponse = [];
                                                    otherResponse = this.notSelectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                    if (otherResponse.length) {
                                                        this.notHostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherResponse[0].other});
                                                    }
                                                } else {
                                                    this.notHostingProviderTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                                }
                                            } else if (category === 'Managed Services (specify)'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                if (serviceTypeName === 'Other services (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                    let otherManageResponse = [];
                                                    otherManageResponse = this.notSelectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                    if (otherManageResponse.length) {
                                                        this.notManagedServiceTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherManageResponse[0].other});
                                                    }
                                                } else {
                                                    this.notManagedServiceTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                                }
                                            } else if (category === 'Payment Processing'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                if (serviceTypeName === 'Other processing (specify):'.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase()) {
                                                    let otherProcessResponse = [];
                                                    otherProcessResponse = this.notSelectedOtherResponse.filter((m) => m.serviceTypeId.id === types.serviceTypeId.id);
                                                    if (otherProcessResponse.length) {
                                                        this.notPaymentProcessTypes.push({serciveType: types.serviceTypeId.serviceType, other: otherProcessResponse[0].other})
                                                    }
                                                    } else {
                                                    this.notPaymentProcessTypes.push({serciveType: types.serviceTypeId.serviceType, other: ''})
                                                }
                                            }
                                        });
                                    }
                                },   (resp: ResponseWrapper) => this.onError(resp.json));
                    },   (resp1: ResponseWrapper) => this.onError(resp1.json));
            this._summaryService.queryNotSelectedType().subscribe(
                    (res: ResponseWrapper) => {
                        const serviceTypeNotSelected = res.json
                        if (serviceTypeNotSelected.length > 0) {
                            this.serviceTypeNotSelected = serviceTypeNotSelected;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            }
            if (this.tenantId.includes('merchant_saqd')) {
            this._summaryService.queryBusinessType().subscribe(
                    (res: ResponseWrapper) => {
                        const merchantBusinessTypeSelected = res.json
                        if (merchantBusinessTypeSelected.length > 0) {
                            this.merchantBusinessTypeSelected = merchantBusinessTypeSelected;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryServe().subscribe(
                    (res: ResponseWrapper) => {
                        const paymentChannelBusinessServes = res.json
                        if (paymentChannelBusinessServes.length > 0) {
                            this.paymentChannelBusinessServes = paymentChannelBusinessServes;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryCover().subscribe(
                    (res: ResponseWrapper) => {
                        const paymentChannelSaqCovers = res.json
                        if (paymentChannelSaqCovers.length > 0) {
                            this.paymentChannelSaqCovers = paymentChannelSaqCovers;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            }
            this._summaryService.queryPaymentCard().subscribe(
                    (res: ResponseWrapper) => {
                        const paymentCardBusinessDescription = res.json
                        if (paymentCardBusinessDescription.length > 0) {
                            this.paymentCardBusinessDescription = paymentCardBusinessDescription;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryLocation().subscribe(
                    (res: ResponseWrapper) => {
                        const location = res.json
                        if (location.length > 0) {
                            this.location = location;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryPaymentApplications().subscribe(
                    (res: ResponseWrapper) => {
                        const paymentApplication = res.json
                        if (paymentApplication.length > 0) {
                            this.paymentApplication = paymentApplication;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryEnvironment().subscribe(
                    (res: ResponseWrapper) => {
                        const environmentDescription = res.json
                        if (environmentDescription.length > 0) {
                            this.environmentDescription = environmentDescription;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            this._summaryService.queryThirdPartySP().subscribe(
                    (res: ResponseWrapper) => {
                        const thirdPartyServiceProvider = res.json
                        if (thirdPartyServiceProvider.length > 0) {
                            this.thirdPartyServiceProvider = thirdPartyServiceProvider;
                        }
                    },
                    (res: ResponseWrapper) => this.onError(res.json));
            if (this.tenantId.includes('serviceprovider_saqd')) {
                this._summaryService.queryTestedHeader().subscribe(
                        (res: ResponseWrapper) => {
                            this.testedRequirementHeader = res.json;
                            if (this.testedRequirementHeader[0].testedRequirementSummaries) {
                                this.sortSummary(this.testedRequirementHeader[0].testedRequirementSummaries);
                            }
                        },
                        (res: ResponseWrapper) => this.onError(res.json));
            }
        });
    }

    private onError(error) {
        console.log(error);
    }

    sortServiceType(types) {
        types.sort( function(t1, t2) {
            if ( t1.id < t2.id ) {
                return -1;
            } else if ( t1.id > t2.id ) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    sortSummary(summary) {
        summary.sort( function(t1, t2) {
            if ( t1.pcidssRequirementId.id < t2.pcidssRequirementId.id ) {
                return -1;
            } else if ( t1.pcidssRequirementId.id > t2.pcidssRequirementId.id ) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    cancel() {
        this.router.navigate(['/saq/assessment-home']);
    }

    submit() {
        this.router.navigate(['/saq/assessment-home']);
    }

}
