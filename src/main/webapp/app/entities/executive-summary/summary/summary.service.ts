import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ScopeVerificationSelectedService, SelectedServiceType, OtherServiceResponse, ScopeVerificationNotSelectedService,
    ThirdPartyServiceProvider, ServiceTypeNotSelected, ServiceType, MerchantBusinessTypeSelected, PaymentChannelBusinessServes,
    PaymentChannelSaqCovers, PaymentCardBusinessDescription, Location, PaymentApplication, EnvironmentDescription,
    TestedRequirementHeader, TestedRequirementSummary} from '../executive-summary.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class SummaryService {

    private getScopeSelectedUrl = 'api/scope-verification-selected-services/service-provider';
    private getScopeNotSelectedUrl = 'api/scope-verification-not-selected-services/service-provider';
    private getServiceCategoryUrl = 'api/service-categories/service-provider';
    private getServiceTypeUrl = 'api/service-types/service-provider';
    private getSelectedTypeUrl = 'api/selected-service-types/service-provider';
    private getNotSelectedTypeUrl = 'api/service-type-not-selected/service-provider';
    private getOtherServiceUrl = 'api/other-service-responses/service-provider';
    private getBusinessTypeUrl = 'api/merchant-business-type-selected';
    private getServeUrl = 'api/payment-channel-business-serves';
    private getCoverUrl = 'api/payment-channel-saq-covers';
    private getPaymentCardUrl = 'api/payment-card-business-descriptions';
    private getLocationUrl = 'api/locations/service-provider';
    private getPaymentApplicationsUrl = 'api/payment-applications/service-provider';
    private getEnvironmentUrl = 'api/environment-descriptions/service-provider';
    private getThirdPartySPUrl = 'api/third-party-service-providers/service-provider';
    private getTestedHeaderUrl = 'api/tested-requirement-header/service-provider';
    private getTestedSummaryUrl = 'api/tested-requirement-summaries/service-provider';

    constructor(private http: Http) { }

    findServiceType(id: number): Observable<ServiceType> {
        return this.http.get(`${this.getServiceTypeUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    queryCategory(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getServiceCategoryUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryTypes(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getServiceTypeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryScopeSelected(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getScopeSelectedUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryScopeNotSelected(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getScopeNotSelectedUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getSelectedTypeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryNotSelectedType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getNotSelectedTypeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryOtherService(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getOtherServiceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryBusinessType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getBusinessTypeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryServe(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getServeUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCover(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getCoverUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPaymentCard(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getPaymentCardUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryLocation(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getLocationUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPaymentApplications(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getPaymentApplicationsUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryEnvironment(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getEnvironmentUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryThirdPartySP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getThirdPartySPUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryTestedHeader(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getTestedHeaderUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryTestedSummary(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.getTestedSummaryUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }
}
