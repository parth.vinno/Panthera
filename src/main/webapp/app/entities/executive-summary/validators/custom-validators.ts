import { FormArray, FormControl, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
    static assessedServiceName(c: FormControl): ValidationErrors {
        const isValidServiceName = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'assessedServiceName': {
                    'message': 'Enter valid service name'
                }
        };
        return isValidServiceName ? null : message;
    }

    static assessedServiceType(c: FormControl): ValidationErrors {
        const isValidNewType = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'assessedServiceType': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidNewType ? null : message;
    }

    static specificationOther(c: FormControl): ValidationErrors {
        const isValidSpecify = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'specificationOther': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidSpecify ? null : message;
    }

    static specification(c: FormControl): ValidationErrors {
        const isValidSpecify = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'specification': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidSpecify ? null : message;
    }

    static specificationManage(c: FormControl): ValidationErrors {
        const isValidSpecify = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'specificationManage': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidSpecify ? null : message;
    }

    static specificationHost(c: FormControl): ValidationErrors {
        const isValidSpecify = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'specificationHost': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidSpecify ? null : message;
    }

    static specificationProcess(c: FormControl): ValidationErrors {
        const isValidSpecify = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'specificationProcess': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidSpecify ? null : message;
    }

    static notAssessedServiceName(c: FormControl): ValidationErrors {
        const isValidServiceName = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'notAssessedServiceName': {
                    'message': 'Enter valid service name'
                }
        };
        return isValidServiceName ? null : message;
    }

    static notAssessedServiceType(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'notAssessedServiceType': {
                    'message': 'Enter valid service type'
                }
        };
        return isValid ? null : message;
    }

    static descriptionTransmit(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'descriptionTransmit': {
                    'message': 'Enter valid description'
                }
        };
        return isValid ? null : message;
    }

    static descriptionImpact(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'descriptionImpact': {
                    'message': 'Enter valid description'
                }
        };
        return isValid ? null : message;
    }

    static comment(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'comment': {
                    'message': 'Enter valid service type'
                }
        };
        return isValid ? null : message;
    }

    static facilityType(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'facilityType': {
                    'message': 'Enter valid facility type'
                }
        };
        return isValid ? null : message;
    }

    static numberOfFacilityType(c: FormControl): ValidationErrors {
        const isValid = /^[0-9]*$/.test(c.value);
        const message = {
                'numberOfFacilityType': {
                    'message': 'Enter numbers only'
                }
        };
        return isValid ? null : message;
    }

    static location(c: FormControl): ValidationErrors {
        const isValid = /^[A-Z,'\s()?]*$/.test(c.value);
        const message = {
                'location': {
                    'message': 'Enter valid location'
                }
        };
        return isValid ? null : message;
    }

    static paymentApplicationName(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'paymentApplicationName': {
                    'message': 'Enter valid payment application name'
                }
        };
        return isValid ? null : message;
    }

    static versionNumber(c: FormControl): ValidationErrors {
        const isValid = /^[0-9]*$/.test(c.value);
        const message = {
                'versionNumber': {
                    'message': 'Enter numbers only'
                }
        };
        return isValid ? null : message;
    }

    static applicationVendor(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'applicationVendor': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static description(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'description': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static qirCompanyName(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'qirCompanyName': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static qirIndividualName(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'qirIndividualName': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static serviceDescription(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'serviceDescription': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static providerName(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'providerName': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static descriptionOfServiceProvider(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'descriptionOfServiceProvider': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

    static serviceAssessedName(c: FormControl): ValidationErrors {
        const isValid = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'serviceAssessedName': {
                    'message': 'Enter valid application vendor'
                }
        };
        return isValid ? null : message;
    }

}
