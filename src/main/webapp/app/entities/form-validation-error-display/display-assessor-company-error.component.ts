import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
    selector: 'jhi-display-assessor-error',
    template: `
        <div class="alert" *ngIf="shouldShowErrors()">
            <ul>
                <li class="error-msg" *ngFor="let error of listOfErrors()">{{error}}</li>
            </ul>
        </div>
        `,
        styleUrls: ['./commonStyle.scss']
})
export class DisplayAssessorCompanyErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required.',
        'minlength': (params) => 'The min number of characters is ' + params.requiredLength + '.',
        'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength + '.',
        'companyName': (params) => params.message,
        'leadQsaContactName': (params) => params.message,
        'title': (params) => params.message,
        'street1': (params) => params.message,
        'postalCode': (params) => params.message,
        'telephone': (params) => params.message,
        'email': (params) => params.message,
        'city': (params) => params.message,
        'url': (params) => params.message
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    shouldShowErrors(): boolean {
        return this.control &&
        this.control.errors &&
        (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
            return Object.keys(this.control.errors)
            .map((field) => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return DisplayAssessorCompanyErrorsComponent.errorMessages[type](params);
    }
}
