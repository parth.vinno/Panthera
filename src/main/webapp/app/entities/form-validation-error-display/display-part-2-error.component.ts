import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
    selector: 'jhi-display-2-error',
    template: `
        <div class="alert" *ngIf="shouldShowErrors()">
        <ul>
        <li class="error-msg" *ngFor="let error of listOfErrors()">{{error}}</li>
        </ul></div>
        `,
        styleUrls: ['./commonStyle.scss']
})
export class DisplayPart2ErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required.',
        'minlength': (params) => 'The min number of characters is ' + params.requiredLength + '.',
        'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength + '.',
        'assessedServiceName': (params) => params.message,
        'assessedServiceType': (params) => params.message,
        'comment': (params) => params.message,
        'notAssessedServiceName': (params) => params.message,
        'notAssessedServiceType': (params) => params.message,
        'descriptionTransmit': (params) => params.message,
        'descriptionImpact': (params) => params.message,
        'facilityType': (params) => params.message,
        'numberOfFacilityType': (params) => params.message,
        'location': (params) => params.message,
        'paymentApplicationName': (params) => params.message,
        'versionNumber': (params) => params.message,
        'applicationVendor': (params) => params.message,
        'description': (params) => params.message,
        'qirCompanyName': (params) => params.message,
        'qirIndividualName': (params) => params.message,
        'serviceDescription': (params) => params.message,
        'providerName': (params) => params.message,
        'descriptionOfServiceProvider': (params) => params.message,
        'serviceAssessedName': (params) => params.message,
        'specification': (params) => params.message,
        'pattern': () => 'This field must be valid.'
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    shouldShowErrors(): boolean {
        return this.control &&
        this.control.errors &&
        (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
            return Object.keys(this.control.errors)
            .map((field) => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return DisplayPart2ErrorsComponent.errorMessages[type](params);
    }
}
