import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
    selector: 'jhi-display-saq-levels-error',
    template: `
        <div class="alert alert-danger" *ngIf="shouldShowErrors()">
        <ul>
        <li class="error-msg" *ngFor="let error of listOfErrors()">{{error}}</li>
        </ul></div>
        `,
        styleUrls: ['./commonStyle.scss']
})
export class DisplaySaqLevelsErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required.',
        'minlength': (params) => 'The min number of characters is ' + params.requiredLength + '.',
        'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength + '.',
        'level1QuestionNumber': (params) => params.message,
        'level2QuestionNumber': (params) => params.message,
        'pattern': () => 'This field must be valid.'
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    shouldShowErrors(): boolean {
        return this.control &&
        this.control.errors &&
        (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
            return Object.keys(this.control.errors)
            .map((field) => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return DisplaySaqLevelsErrorsComponent.errorMessages[type](params);
    }
}
