import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
    selector: 'jhi-display-category-error',
    template: `
        <div class="alert" *ngIf="shouldShowErrors()">
        <ul>
        <li class="error-msg" *ngFor="let error of listOfErrors()">{{error}}</li>
        </ul></div>
        `,
        styleUrls: ['./commonStyle.scss']
})
export class DisplayCategoryErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required.',
        'name': (params) => params.message,
        'serviceType': (params) => params.message,
        'pattern': () => 'This field must be valid.'
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    shouldShowErrors(): boolean {
        return this.control &&
        this.control.errors &&
        (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
            return Object.keys(this.control.errors)
            .map((field) => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return DisplayCategoryErrorsComponent.errorMessages[type](params);
    }
}
