import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RequirementWorkStatus } from './requirement-work-status.model';
import { RequirementWorkStatusPopupService } from './requirement-work-status-popup.service';
import { RequirementWorkStatusService } from './requirement-work-status.service';

@Component({
    selector: 'jhi-requirement-work-status-delete-dialog',
    templateUrl: './requirement-work-status-delete-dialog.component.html'
})
export class RequirementWorkStatusDeleteDialogComponent {

    requirementWorkStatus: RequirementWorkStatus;

    constructor(
        private requirementWorkStatusService: RequirementWorkStatusService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.requirementWorkStatusService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'requirementWorkStatusListModification',
                content: 'Deleted an requirementWorkStatus'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-requirement-work-status-delete-popup',
    template: ''
})
export class RequirementWorkStatusDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private requirementWorkStatusPopupService: RequirementWorkStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.requirementWorkStatusPopupService
                .open(RequirementWorkStatusDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
