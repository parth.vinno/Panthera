import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { RequirementWorkStatus } from './requirement-work-status.model';
import { RequirementWorkStatusService } from './requirement-work-status.service';

@Component({
    selector: 'jhi-requirement-work-status-detail',
    templateUrl: './requirement-work-status-detail.component.html'
})
export class RequirementWorkStatusDetailComponent implements OnInit, OnDestroy {

    requirementWorkStatus: RequirementWorkStatus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private requirementWorkStatusService: RequirementWorkStatusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRequirementWorkStatuses();
    }

    load(id) {
        this.requirementWorkStatusService.find(id).subscribe((requirementWorkStatus) => {
            this.requirementWorkStatus = requirementWorkStatus;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRequirementWorkStatuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'requirementWorkStatusListModification',
            (response) => this.load(this.requirementWorkStatus.id)
        );
    }
}
