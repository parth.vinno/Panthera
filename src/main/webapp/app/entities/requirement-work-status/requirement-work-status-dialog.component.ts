import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequirementWorkStatus } from './requirement-work-status.model';
import { RequirementWorkStatusPopupService } from './requirement-work-status-popup.service';
import { RequirementWorkStatusService } from './requirement-work-status.service';
import { User, UserService } from '../../shared';
import { PcidssRequirement, PcidssRequirementService } from '../saq-admin/pcidss-requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-requirement-work-status-dialog',
    templateUrl: './requirement-work-status-dialog.component.html'
})
export class RequirementWorkStatusDialogComponent implements OnInit {

    requirementWorkStatus: RequirementWorkStatus;
    authorities: any[];
    isSaving: boolean;

    users: User[];

    pcidssrequirements: PcidssRequirement[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private requirementWorkStatusService: RequirementWorkStatusService,
        private userService: UserService,
        private pcidssRequirementService: PcidssRequirementService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.pcidssRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.pcidssrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requirementWorkStatus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.requirementWorkStatusService.update(this.requirementWorkStatus));
        } else {
            this.subscribeToSaveResponse(
                this.requirementWorkStatusService.create(this.requirementWorkStatus));
        }
    }

    private subscribeToSaveResponse(result: Observable<RequirementWorkStatus>) {
        result.subscribe((res: RequirementWorkStatus) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: RequirementWorkStatus) {
        this.eventManager.broadcast({ name: 'requirementWorkStatusListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackPcidssRequirementById(index: number, item: PcidssRequirement) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-requirement-work-status-popup',
    template: ''
})
export class RequirementWorkStatusPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private requirementWorkStatusPopupService: RequirementWorkStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.requirementWorkStatusPopupService
                    .open(RequirementWorkStatusDialogComponent, params['id']);
            } else {
                this.modalRef = this.requirementWorkStatusPopupService
                    .open(RequirementWorkStatusDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
