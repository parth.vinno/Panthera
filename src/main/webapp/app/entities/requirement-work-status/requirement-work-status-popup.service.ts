import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequirementWorkStatus } from './requirement-work-status.model';
import { RequirementWorkStatusService } from './requirement-work-status.service';

@Injectable()
export class RequirementWorkStatusPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private requirementWorkStatusService: RequirementWorkStatusService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.requirementWorkStatusService.find(id).subscribe((requirementWorkStatus) => {
                this.requirementWorkStatusModalRef(component, requirementWorkStatus);
            });
        } else {
            return this.requirementWorkStatusModalRef(component, new RequirementWorkStatus());
        }
    }

    requirementWorkStatusModalRef(component: Component, requirementWorkStatus: RequirementWorkStatus): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.requirementWorkStatus = requirementWorkStatus;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
