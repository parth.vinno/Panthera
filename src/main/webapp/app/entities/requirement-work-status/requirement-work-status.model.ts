import { BaseEntity, User } from './../../shared';

export class RequirementWorkStatus implements BaseEntity {
    constructor(
        public id?: number,
        public status?: string,
        public assignById?: User,
        public assignToId?: User,
        public pcidssRequirementId?: BaseEntity,
    ) {
    }
}
