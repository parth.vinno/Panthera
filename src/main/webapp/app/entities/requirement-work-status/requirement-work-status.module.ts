import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import { PcidssAdminModule } from '../../admin/admin.module';
import {
    RequirementWorkStatusService,
    RequirementWorkStatusPopupService,
    RequirementWorkStatusComponent,
    RequirementWorkStatusDetailComponent,
    RequirementWorkStatusDialogComponent,
    RequirementWorkStatusPopupComponent,
    RequirementWorkStatusDeletePopupComponent,
    RequirementWorkStatusDeleteDialogComponent,
    requirementWorkStatusRoute,
    requirementWorkStatusPopupRoute,
    RequirementWorkStatusResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...requirementWorkStatusRoute,
    ...requirementWorkStatusPopupRoute,
];

@NgModule({
    imports: [
        PcidssSharedModule,
        PcidssAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RequirementWorkStatusComponent,
        RequirementWorkStatusDetailComponent,
        RequirementWorkStatusDialogComponent,
        RequirementWorkStatusDeleteDialogComponent,
        RequirementWorkStatusPopupComponent,
        RequirementWorkStatusDeletePopupComponent,
    ],
    entryComponents: [
        RequirementWorkStatusComponent,
        RequirementWorkStatusDialogComponent,
        RequirementWorkStatusPopupComponent,
        RequirementWorkStatusDeleteDialogComponent,
        RequirementWorkStatusDeletePopupComponent,
    ],
    providers: [
        RequirementWorkStatusService,
        RequirementWorkStatusPopupService,
        RequirementWorkStatusResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssRequirementWorkStatusModule {}
