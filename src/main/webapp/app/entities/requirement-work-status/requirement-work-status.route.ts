import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RequirementWorkStatusComponent } from './requirement-work-status.component';
import { RequirementWorkStatusDetailComponent } from './requirement-work-status-detail.component';
import { RequirementWorkStatusPopupComponent } from './requirement-work-status-dialog.component';
import { RequirementWorkStatusDeletePopupComponent } from './requirement-work-status-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class RequirementWorkStatusResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requirementWorkStatusRoute: Routes = [
    {
        path: 'requirement-work-status',
        component: RequirementWorkStatusComponent,
        resolve: {
            'pagingParams': RequirementWorkStatusResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.requirementWorkStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'requirement-work-status/:id',
        component: RequirementWorkStatusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.requirementWorkStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requirementWorkStatusPopupRoute: Routes = [
    {
        path: 'requirement-work-status-new',
        component: RequirementWorkStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.requirementWorkStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-work-status/:id/edit',
        component: RequirementWorkStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.requirementWorkStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-work-status/:id/delete',
        component: RequirementWorkStatusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.requirementWorkStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
