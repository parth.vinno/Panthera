import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { RequirementWorkStatus } from './requirement-work-status.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequirementWorkStatusService {

    private resourceUrl = 'api/requirement-work-statuses';

    constructor(private http: Http) { }

    create(requirementWorkStatus: RequirementWorkStatus): Observable<RequirementWorkStatus> {
        const copy = this.convert(requirementWorkStatus);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(requirementWorkStatus: RequirementWorkStatus): Observable<RequirementWorkStatus> {
        const copy = this.convert(requirementWorkStatus);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<RequirementWorkStatus> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(requirementWorkStatus: RequirementWorkStatus): RequirementWorkStatus {
        const copy: RequirementWorkStatus = Object.assign({}, requirementWorkStatus);
        return copy;
    }
}
