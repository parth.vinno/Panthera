import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ExpectedTest } from './expected-test.model';
import { ExpectedTestService } from './expected-test.service';

@Component({
    selector: 'jhi-expected-test-detail',
    templateUrl: './expected-test-detail.component.html',
    styleUrls: [
                'expected-test.component.scss'
            ]
})
export class ExpectedTestDetailComponent implements OnInit, OnDestroy {

    expectedTest: ExpectedTest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private _expectedTestService: ExpectedTestService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInExpectedTests();
    }

    load(id) {
        this._expectedTestService.find(id).subscribe((expectedTest) => {
            this.expectedTest = expectedTest;
        });
    }
    previousState() {
        this.router.navigate(['/saq/admin/expected-test']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInExpectedTests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'expectedTestListModification',
            (response) => this.load(this.expectedTest.id)
        );
    }
}
