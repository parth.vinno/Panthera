import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { YesNoModalComponent } from '../../../shared';
import { ExpectedTest } from './expected-test.model';
import { ExpectedTestService } from './expected-test.service';

@Component({
    selector: 'jhi-expected-test-edit',
    templateUrl: './expected-test-edit.component.html',
    styleUrls: [
                'expected-test.component.scss'
            ]
})
export class ExpectedTestEditComponent implements OnInit {

    expectedTest: ExpectedTest;
    authorities: any[];
    routeSub: any;

    constructor(
            private route: ActivatedRoute,
            private _alertService: JhiAlertService,
            private _expectedTestService: ExpectedTestService,
            private _eventManager: JhiEventManager,
            private _toastr: ToastrService,
            private _bsModalService: BsModalService,
            private router: Router
    ) {
    }

    ngOnInit() {
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this._expectedTestService.find(params['id']).subscribe((expectedTest) => {
                    this.expectedTest = expectedTest;
                    this._expectedTestService.findExpectedTestCount(this.expectedTest.id).subscribe((expTestCount) => {
                        if ( expTestCount === 0) {
                            this.expectedTest.isExpectedTestExist = true;
                        } else {
                            this.expectedTest.isExpectedTestExist = false;
                        }
                    });
                });
            }
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/expected-test']);
    }

    save() {
        if (this.expectedTest.id !== undefined) {
            this.subscribeToSaveResponse(
                this._expectedTestService.update(this.expectedTest));
        }
        this.previousState();
    }

    private subscribeToSaveResponse(result: Observable<ExpectedTest>) {
        result.subscribe((res: ExpectedTest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ExpectedTest) {
        this._toastr.success(`The expected test has been successfully Updated`, `Update`);
        this._eventManager.broadcast({ name: 'expectedTestListModification', content: 'OK'});
    }

    private onSaveError(error) {
        try {
            const err = error.json();
            this._toastr.error(`${err.description}`, `Error`);
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    private onError(error) {
        this._alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteExpectedTest(expTest) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete expected Test ' + expTest.id + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this._expectedTestService.delete(expTest.id).subscribe((response) => {
                    this._eventManager.broadcast({
                        name: 'expectedTestListModification',
                        content: 'Deleted an expectedTest'
                    });
                    this.router.navigate(['/saq/admin/expected-test']);
                    this._toastr.success(`The Expected Testing has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteExpectedTest(expTest) {
        this.openConfirmationModalToDeleteExpectedTest(expTest);
    }
}
