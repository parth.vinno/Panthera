import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { Pipe, PipeTransform } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { ExpectedTest } from './expected-test.model';
import { ExpectedTestService } from './expected-test.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

@Pipe({
    name: 'filterExpectedTest'
})
export class FilterExpectedTest implements PipeTransform {
    transform(items: any[], criteria: any): any {
        return items.filter((item) => {
           for (const key in item ) {
             if ( ('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
             }
           }
           return false;
        });
    }
}

@Component({
    selector: 'jhi-expected-test',
    templateUrl: './expected-test.component.html',
    styleUrls: [
                'expected-test.component.scss'
            ]
})
export class ExpectedTestComponent implements OnInit, OnDestroy {

currentAccount: any;
    expectedTests: ExpectedTest[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private _expectedTestService: ExpectedTestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private _bsModalService: BsModalService,
        private _toastr: ToastrService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() {
        this._expectedTestService.queryPageable({page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/saq/admin/expected-test'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/saq/admin/expected-test', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInExpectedTests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ExpectedTest) {
        return item.id;
    }
    registerChangeInExpectedTests() {
        this.eventSubscriber = this.eventManager.subscribe('expectedTestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.expectedTests = data;
        this.expectedTests.map( (exptest) => {
            this._expectedTestService.findExpectedTestCount(exptest.id).subscribe((expTestCount) => {
                if ( expTestCount === 0) {
                    exptest.isExpectedTestExist = true;
                } else {
                    exptest.isExpectedTestExist = false;
                }
            });
        });
    }

    openConfirmationModalToDeleteExpectedTest(expTest) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete expected Test ' + expTest.id + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this._expectedTestService.delete(expTest.id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'expectedTestListModification',
                        content: 'Deleted an expectedTest'
                    });
                    this.router.navigate(['/saq/admin/expected-test']);
                    this._toastr.success(`The Expected Testing has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteExpectedTest(expTest) {
        this.openConfirmationModalToDeleteExpectedTest(expTest);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
