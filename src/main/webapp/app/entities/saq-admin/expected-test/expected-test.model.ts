import { BaseEntity } from './../../../shared';

export class ExpectedTest implements BaseEntity {
    constructor(
        public id?: number,
        public testName?: string,
        public isExpectedTestExist?: boolean
    ) {
    }
}
