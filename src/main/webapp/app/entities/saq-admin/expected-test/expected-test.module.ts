import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    ExpectedTestService,
    ExpectedTestComponent,
    ExpectedTestDetailComponent,
    ExpectedTestEditComponent,
    FilterExpectedTest
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ExpectedTestComponent,
        ExpectedTestDetailComponent,
        ExpectedTestEditComponent,
        FilterExpectedTest
    ],
    entryComponents: [
        ExpectedTestComponent,
        ExpectedTestEditComponent
    ],
    providers: [
        ExpectedTestService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssExpectedTestModule {}
