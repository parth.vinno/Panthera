import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ExpectedTest } from './expected-test.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class ExpectedTestService {

    private resourceUrl = 'api/expected-tests';
    private resourceUrlPageable = 'api/expected-tests-pageable';

    constructor(private http: Http) { }

    create(expectedTest: ExpectedTest): Observable<ExpectedTest> {
        const copy = this.convert(expectedTest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(expectedTest: ExpectedTest): Observable<ExpectedTest> {
        const copy = this.convert(expectedTest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ExpectedTest> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findExpectedTestCount(id: number): Observable<ExpectedTest> {
        return this.http.get(`${this.resourceUrl}/use-count/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPageable(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlPageable, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(expectedTest: ExpectedTest): ExpectedTest {
        const copy: ExpectedTest = Object.assign({}, expectedTest);
        return copy;
    }
}
