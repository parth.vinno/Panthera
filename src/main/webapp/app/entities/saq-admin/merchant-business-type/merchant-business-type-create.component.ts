import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MerchantBusinessType } from '../saq-question.model';
import { MerchantBusinessTypeService } from './merchant-business-type.service';
import { Principal, ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-merchant-business-type-create',
    templateUrl: './merchant-business-type-create.component.html',
    styleUrls: [
                'merchant-business-type-create.component.scss'
            ]
})

export class MerchantBusinessTypeCreateComponent implements OnInit {

    merchantBusinessType: MerchantBusinessType;
    merchantBusinessTypeForm: FormGroup;
    authorities: any[];
    isSaving: boolean;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: JhiAlertService,
        private _MerchantBusinessTypeService: MerchantBusinessTypeService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.merchantBusinessTypeForm = new FormGroup ({
            businessType: new FormControl('', Validators.compose([
                                                                    Validators.required,
                                                                    Validators.minLength(5),
                                                                    Validators.maxLength(100),
                                                                    Validators.pattern('^[a-zA-Z0-9\,\.\!\-\:? ]*$')
                                                                    ]))
          });
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    previousState() {
        this.router.navigate(['/saq/admin/merchant-business-types']);
    }

    save(types) {
        const isDuplicateBusinessType = (types.businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isTypesExist = true;
        this._MerchantBusinessTypeService.query().subscribe(
            (res: ResponseWrapper) => {
                const typesList = res.json;
                for (let i = 0; i < typesList.length; i++) {
                    const modifiedTypes  = (typesList[i].businessType).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                    if (modifiedTypes === isDuplicateBusinessType) {
                        this._toastr.error(`The merchant Business Type aleady exists`, `Error`);
                        isTypesExist = false;
                        break;
                    }
                }
                if (isTypesExist) {
                    this.merchantBusinessType = types;
                     if (this.merchantBusinessType.id === undefined) {
                      this.subscribeToSaveResponse(
                          this._MerchantBusinessTypeService.create(this.merchantBusinessType))
                    }
                }
                },
            (res: ResponseWrapper) => {
                this.onError(res.json)}
        );
    }

    private subscribeToSaveResponse(result: Observable<MerchantBusinessType>) {
        result.subscribe((res: MerchantBusinessType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: MerchantBusinessType) {
        this._toastr.success(`The Merchant Business Type has been successfully Saved`, `Save`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this._toastr.error(`${error.message}`, `Error`);
        this.alertService.error(error.message, null, null);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
