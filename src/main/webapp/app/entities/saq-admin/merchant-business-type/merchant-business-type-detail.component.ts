import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { MerchantBusinessType } from '../saq-question.model';
import { MerchantBusinessTypeService } from './merchant-business-type.service';

@Component({
    selector: 'jhi-merchant-business-type-detail',
    templateUrl: './merchant-business-type-detail.component.html',
    styleUrls: [
                'merchant-business-type-create.component.scss'
            ]
})
export class MerchantBusinessTypeDetailComponent implements OnInit, OnDestroy {

    merchantBusinessType: MerchantBusinessType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private _merchantBusinessTypeService: MerchantBusinessTypeService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTypes();
    }

    load(id) {
        this._merchantBusinessTypeService.find(id).subscribe((types) => {
            this.merchantBusinessType = types;
        });
    }
    previousState() {
        this.router.navigate(['/saq/admin/merchant-business-types']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'TypesListModification',
            (response) => this.load(this.merchantBusinessType.id)
        );
    }
}
