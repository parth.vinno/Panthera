import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { YesNoModalComponent } from '../../../shared';
import { MerchantBusinessType } from '../saq-question.model';
import { MerchantBusinessTypeService } from './merchant-business-type.service';

@Component({
    selector: 'jhi-merchant-business-type-edit',
    templateUrl: './merchant-business-type-edit.component.html',
    styleUrls: [
                'merchant-business-type-create.component.scss'
            ]
})
export class MerchantBusinessTypeEditComponent implements OnInit {

    merchantBusinessType: MerchantBusinessType;
    authorities: any[];
    routeSub: any;

    constructor(
            private route: ActivatedRoute,
            private _alertService: JhiAlertService,
            private _merchantBusinessTypeService: MerchantBusinessTypeService,
            private _eventManager: JhiEventManager,
            private _toastr: ToastrService,
            private _bsModalService: BsModalService,
            private router: Router
    ) {
    }

    ngOnInit() {
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this._merchantBusinessTypeService.find(params['id']).subscribe((merchantBusinessType) => {
                    this.merchantBusinessType = merchantBusinessType;
                });
            }
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/merchant-business-types']);
    }

    save() {
        if (this.merchantBusinessType.id !== undefined) {
            this.subscribeToSaveResponse(
                this._merchantBusinessTypeService.update(this.merchantBusinessType));
        }
        this.previousState();
    }

    private subscribeToSaveResponse(result: Observable<MerchantBusinessType>) {
        result.subscribe((res: MerchantBusinessType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: MerchantBusinessType) {
        this._toastr.success(`The Merchant Business Type has been successfully Updated`, `Update`);
        this._eventManager.broadcast({ name: 'merchantBusinessTypeListModification', content: 'OK'});
    }

    private onSaveError(error) {
        try {
            const err = error.json();
            this._toastr.error(`${err.description}`, `Error`);
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    private onError(error) {
        this._alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteMerchantBusinessType(types) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete expected Test ' + types.id + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this._merchantBusinessTypeService.delete(types.id).subscribe((response) => {
                    this._eventManager.broadcast({
                        name: 'merchantBusinessTypeListModification',
                        content: 'Deleted Merchant Business Type'
                    });
                    this.router.navigate(['/saq/admin/merchant-business-types']);
                    this._toastr.success(`The Expected Testing has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteType(types) {
        this.openConfirmationModalToDeleteMerchantBusinessType(types);
    }
}
