import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { Pipe, PipeTransform } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { MerchantBusinessType } from '../saq-question.model';
import { MerchantBusinessTypeService } from './merchant-business-type.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

@Pipe({
    name: 'filterBusinessType'
})
export class FilterBusinessType implements PipeTransform {
    transform(items: any[], criteria: any): any {
        return items.filter((item) => {
           for (const key in item ) {
             if ( ('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
             }
           }
           return false;
        });
    }
}

@Component({
    selector: 'jhi-merchant-business-type',
    templateUrl: './merchant-business-type.component.html',
    styleUrls: [
                'merchant-business-type-create.component.scss'
            ]
})

export class MerchantBusinessTypeComponent implements OnInit, OnDestroy {
    currentAccount: any;
    merchantBusinessType: MerchantBusinessType[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    constructor(private _merchantBusinessTypeService: MerchantBusinessTypeService,
            private parseLinks: JhiParseLinks,
            private alertService: JhiAlertService,
            private principal: Principal,
            private activatedRoute: ActivatedRoute,
            private router: Router,
            private eventManager: JhiEventManager,
            private paginationUtil: JhiPaginationUtil,
            private paginationConfig: PaginationConfig,
            private _bsModalService: BsModalService,
            private _toastr: ToastrService ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
            console.log('this.page', this.page, this.reverse);
        });
    }

    loadAll() {
        this._merchantBusinessTypeService.query({page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.totalItems = (res.headers).get('X-Total-Count');
                this.queryCount = this.totalItems;
                this.merchantBusinessType = res.json;
                },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/saq/admin/merchant-business-types'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/saq/admin/merchant-business-types', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTypeTests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MerchantBusinessType) {
        return item.id;
    }

    registerChangeInTypeTests() {
        this.eventSubscriber = this.eventManager.subscribe('businessTypeListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.merchantBusinessType = data;
    }

    openConfirmationModalToDeleteTypeTest(types) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete business type ' + types.id + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this._merchantBusinessTypeService.delete(types.id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'TypeListModification',
                        content: 'Deleted an Type'
                    });
                    this._toastr.success(`The Merchant Business Type has been successfully Deleted`, `Delete`);
                    this.router.navigate(['/saq/admin/merchant-business-types']);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteType(types) {
        this.openConfirmationModalToDeleteTypeTest(types);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

}
