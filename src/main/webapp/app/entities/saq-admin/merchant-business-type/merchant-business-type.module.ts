import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    MerchantBusinessTypeCreateComponent,
    MerchantBusinessTypeService,
    MerchantBusinessTypeComponent,
    MerchantBusinessTypeEditComponent,
    MerchantBusinessTypeDetailComponent,
    FilterBusinessType
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MerchantBusinessTypeCreateComponent,
        MerchantBusinessTypeComponent,
        MerchantBusinessTypeEditComponent,
        MerchantBusinessTypeDetailComponent,
        FilterBusinessType
    ],
    entryComponents: [
        MerchantBusinessTypeCreateComponent
    ],
    providers: [
        MerchantBusinessTypeService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssBusinessTypeModule {}
