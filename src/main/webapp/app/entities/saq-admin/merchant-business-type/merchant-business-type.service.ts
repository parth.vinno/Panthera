import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { MerchantBusinessType } from '../saq-question.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class MerchantBusinessTypeService {

    private resourceUrl = 'api/merchant-business-types';
    private resourceUrlPageable = 'api/merchant-business-types-pageable';

    constructor(private http: Http) { }

    create(merchantBusinessType: MerchantBusinessType): Observable<MerchantBusinessType> {
        const copy = this.convert(merchantBusinessType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(merchantBusinessType: MerchantBusinessType): Observable<MerchantBusinessType> {
        const copy = this.convert(merchantBusinessType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<MerchantBusinessType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlPageable, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(merchantBusinessType: MerchantBusinessType): MerchantBusinessType {
        const copy: MerchantBusinessType = Object.assign({}, merchantBusinessType);
        return copy;
    }
}
