import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentChannel } from '../saq-question.model';
import { PaymentChannelService } from './payment-channel.service';
import { Principal, ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-payment-channel-create',
    templateUrl: './payment-channel-create.component.html',
    styleUrls: [
                'payment-channel.component.scss'
            ]
})

export class PaymentChannelCreateComponent implements OnInit {

    paymentChannel: PaymentChannel;
    paymentChannelForm: FormGroup;
    authorities: any[];
    isSaving: boolean;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: JhiAlertService,
        private _paymentChannelService: PaymentChannelService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.paymentChannelForm = new FormGroup ({
            name: new FormControl('', Validators.compose([
                                                                    Validators.required,
                                                                    Validators.minLength(5),
                                                                    Validators.maxLength(100),
                                                                    Validators.pattern('^[a-zA-Z0-9\,\.\!\-\:? ]*$')
                                                                    ]))
          });
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    previousState() {
        this.router.navigate(['/saq/admin/payment-channel']);
    }

    save(channel) {
        const isDuplicatePaymentChannel = (channel.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isNameExist = true;
        this._paymentChannelService.query().subscribe(
            (res: ResponseWrapper) => {
                const channelList = res.json;
                for (let i = 0; i < channelList.length; i++) {
                    const modifiedName  = (channelList[i].name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                    if (modifiedName === isDuplicatePaymentChannel) {
                        this._toastr.error(`The payment channel aleady exists`, `Error`);
                        isNameExist = false;
                        break;
                    }
                }
                if (isNameExist) {
                    this.paymentChannel = channel;
                     if (this.paymentChannel.id === undefined) {
                      this.subscribeToSaveResponse(
                          this._paymentChannelService.create(this.paymentChannel))
                    }
                }
                },
            (res: ResponseWrapper) => {
                this.onError(res.json)}
        );
    }

    private subscribeToSaveResponse(result: Observable<PaymentChannel>) {
        result.subscribe((res: PaymentChannel) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PaymentChannel) {
        this._toastr.success(`The payment channel has been successfully Saved`, `Save`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this._toastr.error(`${error.message}`, `Error`);
        this.alertService.error(error.message, null, null);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
