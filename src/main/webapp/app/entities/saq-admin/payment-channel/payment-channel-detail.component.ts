import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PaymentChannel } from '../saq-question.model';
import { PaymentChannelService } from './payment-channel.service';

@Component({
    selector: 'jhi-payment-channel-detail',
    templateUrl: './payment-channel-detail.component.html',
    styleUrls: [
                'payment-channel.component.scss'
            ]
})
export class PaymentChannelDetailComponent implements OnInit, OnDestroy {

    paymentChannel: PaymentChannel;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private _paymentChannelService: PaymentChannelService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTypes();
    }

    load(id) {
        this._paymentChannelService.find(id).subscribe((types) => {
            this.paymentChannel = types;
        });
    }
    previousState() {
        this.router.navigate(['/saq/admin/payment-channel']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'TypesListModification',
            (response) => this.load(this.paymentChannel.id)
        );
    }
}
