import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { YesNoModalComponent } from '../../../shared';
import { PaymentChannel } from '../saq-question.model';
import { PaymentChannelService } from './payment-channel.service';

@Component({
    selector: 'jhi-payment-channel-edit',
    templateUrl: './payment-channel-edit.component.html',
    styleUrls: [
                'payment-channel.component.scss'
            ]
})
export class PaymentChannelEditComponent implements OnInit {

    paymentChannel: PaymentChannel;
    authorities: any[];
    routeSub: any;

    constructor(
            private route: ActivatedRoute,
            private _alertService: JhiAlertService,
            private _paymentChannelService: PaymentChannelService,
            private _eventManager: JhiEventManager,
            private _toastr: ToastrService,
            private _bsModalService: BsModalService,
            private router: Router
    ) {
    }

    ngOnInit() {
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this._paymentChannelService.find(params['id']).subscribe((paymentChannel) => {
                    this.paymentChannel = paymentChannel;
                });
            }
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/payment-channel']);
    }

    save() {
        if (this.paymentChannel.id !== undefined) {
            this.subscribeToSaveResponse(
                this._paymentChannelService.update(this.paymentChannel));
        }
        this.previousState();
    }

    private subscribeToSaveResponse(result: Observable<PaymentChannel>) {
        result.subscribe((res: PaymentChannel) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PaymentChannel) {
        this._toastr.success(`The Payment Channel has been successfully Updated`, `Update`);
        this._eventManager.broadcast({ name: 'PaymentChannelListModification', content: 'OK'});
    }

    private onSaveError(error) {
        try {
            const err = error.json();
            this._toastr.error(`${err.description}`, `Error`);
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    private onError(error) {
        this._alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeletePaymentChannel(types) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete expected Test ' + types.id + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this._paymentChannelService.delete(types.id).subscribe((response) => {
                    this._eventManager.broadcast({
                        name: 'paymentChannelListModification',
                        content: 'Deleted Payment Channel'
                    });
                    this.router.navigate(['/saq/admin/payment-channel']);
                    this._toastr.success(`The Payment Channel has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteType(types) {
        this.openConfirmationModalToDeletePaymentChannel(types);
    }
}
