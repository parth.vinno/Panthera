import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    PaymentChannelCreateComponent,
    PaymentChannelService,
    PaymentChannelComponent,
    PaymentChannelEditComponent,
    PaymentChannelDetailComponent,
    FilterPaymentChannel
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PaymentChannelCreateComponent,
        PaymentChannelComponent,
        PaymentChannelEditComponent,
        PaymentChannelDetailComponent,
        FilterPaymentChannel
    ],
    entryComponents: [
        PaymentChannelCreateComponent
    ],
    providers: [
        PaymentChannelService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssPaymentChannelModule {}
