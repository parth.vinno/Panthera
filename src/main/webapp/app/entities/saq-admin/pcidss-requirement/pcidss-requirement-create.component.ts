 import { Component, OnInit, OnDestroy } from '@angular/core';
 import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
 import { NgFor } from '@angular/common';
 import { Response } from '@angular/http';
 import { ToastrService } from 'ngx-toastr';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
 import { Observable, Subscription } from 'rxjs/Rx';
 import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
 import { PcidssRequirement } from './pcidss-requirement.model';
 import { PcidssRequirementService } from './pcidss-requirement.service';
 import { RequirementCategory, RequirementCategoryService } from '../requirement-category';
 import { Principal, ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-pcidss-requirement-create',
    templateUrl: './pcidss-requirement-create.component.html',
    styleUrls: [
                'pcidss-requirement.scss'
            ]
})

export class PcidssRequirementCreateComponent implements OnInit, OnDestroy {

    pcidssRequirement: PcidssRequirement;
    requirementCategory: RequirementCategory;
    requirementCategories: RequirementCategory[];
    pcidssRequirementData: PcidssRequirement[];
    pcidssRequirementList= [];
    authorities: any[];
    currentAccount: any;
    isSaving: boolean;
    eventSubscriber: Subscription;
    newPcidssRequirement: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private principal: Principal,
        private alertService: JhiAlertService,
        private pcidssRequirementService: PcidssRequirementService,
        private requirementCategoryService: RequirementCategoryService,
        private eventManager: JhiEventManager,
        private _fb: FormBuilder,
        private _toastr: ToastrService
    ) {
        this.pcidssRequirement = new PcidssRequirement();
    }

    loadAllRequirementCategory() {
        this.requirementCategoryService.queryMerchant().subscribe(
            (res: ResponseWrapper) => this.onSuccessReq(res.json),
            (res: ResponseWrapper) => this.onErrorReq(res.json)
        );
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.loadAllRequirementCategory();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.newPcidssRequirement = this._fb.group({
            requirementCategoryId: null,
            newReqs: this._fb.array([this.initReqCategoryRows()])
          });
        this.registerChangeInPcidssRequirements();
    }

    initReqCategoryRows() {
        return this._fb.group({
            requirementNumber: [null, Validators.compose([
                                                          Validators.required,
                                                          Validators.minLength(1),
                                                          Validators.maxLength(2),
                                                          Validators.pattern('^[a-zA-Z0-9]*$')
                                                          ])],
            requirement: ['', Validators.compose([
                                                          Validators.required,
                                                          Validators.minLength(5),
                                                          Validators.maxLength(255),
                                                          Validators.pattern('^[a-zA-Z0-9\,\.\!\-\:? ]*$')
                                                          ])]
        });
    }

    addNewReqCategoryRow() {
        const control = <FormArray>this.newPcidssRequirement.controls['newReqs'];
        control.push(this.initReqCategoryRows());
    }

    deleteReqCategoryRow(index: number) {
        const control = <FormArray>this.newPcidssRequirement.controls['newReqs'];
        control.removeAt(index);
    }

    previousStateAfterSave(reqCategoryId) {
        const navigationExtras: NavigationExtras = {
                queryParams: {
                    requirementCategoryId : reqCategoryId.id,
                    requirementCategoryName : reqCategoryId.categoryName
                }
            };
        this.router.navigate(['/saq/admin/pcidss-requirement'], navigationExtras);
    }

    setRequirement() {
        const control = <FormArray>this.newPcidssRequirement.controls['newReqs'];
        control.controls = [];
        control.push(this.initReqCategoryRows());
    }

    checkForDuplicate(reqArr) {
        for (let i = 0; i < reqArr.length; i++) {
            for (let j = i; j < reqArr.length; j++) {
                if (i !== j && (reqArr[i].requirementNumber === reqArr[j].requirementNumber)) {
                    return true;
                }
            }
        }
        return false;
    }

    save() {
        if (this.newPcidssRequirement.value.requirementCategoryId === null) {
            this._toastr.warning(`Please select requirement category`, `Warning`);
        } else {
            let requirements = [];
            let isRequirementExist = true;
            this.pcidssRequirementList = this.newPcidssRequirement.value.newReqs;
            requirements = this.newPcidssRequirement.value.newReqs;
            const isDuplicate = this.checkForDuplicate(this.newPcidssRequirement.value.newReqs);
            if (isDuplicate) {
                this._toastr.warning(`Please remove duplicate entry`, `Warning`);
            } else {
                this.pcidssRequirementService.query().subscribe(
                        (res: ResponseWrapper) => {
                            this.pcidssRequirementData = res.json;
                            for (let j = 0; j < this.pcidssRequirementList.length; j++) {
                                for ( let k = 0; k < this.pcidssRequirementData.length; k++) {
                                    if (this.pcidssRequirementList[j].requirementNumber === this.pcidssRequirementData[k].requirementNumber) {
                                        isRequirementExist = false;
                                        this._toastr.error(`The requirement ${this.pcidssRequirementList[j].requirementNumber} aleady exists, Please Remove`, `Error`);
                                    }
                                }
                            }
                            if (isRequirementExist) {
                                if (requirements !== null && requirements.length > 0) {
                                    this.subscribeToSaveResponse(this.pcidssRequirementService.createList(requirements, this.newPcidssRequirement.value.requirementCategoryId.id));
                                }
                            }
                        },
                        (res: ResponseWrapper) => {
                            this.onError(res.json)
                        }
                )
            }
        }
    }

    onSelectCategory() {
        if (this.newPcidssRequirement.value.requirementCategoryId === 'null') {
            this.newPcidssRequirement.patchValue({
                requirementCategoryId: null
            });
        } else {
            this.setRequirement();
        }
    }

    previousState() {
        this.router.navigate(['/saq/admin/pcidss-requirement']);
    }

    private subscribeToSaveResponse(result: Observable<PcidssRequirement>) {
        result.subscribe((res: PcidssRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PcidssRequirement) {
        this._toastr.success(`The requirement has been successfully saved`, `Save`);
        this.eventManager.broadcast({ name: 'pcidssRequirementListModification', content: 'OK'});
        this.previousStateAfterSave(result[0].requirementCategoryId);
    }

    private onSaveError(error) {
        try {
            error.json();
            } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private onSuccessReq(data) {
        this.requirementCategories = data;
        this.requirementCategories.sort( function(req1, req2) {
          if ( req1.id < req2.id ) {
              return -1;
          } else if ( req1.id > req2.id ) {
              return 1;
          } else {
              return 0;
          }
      });
    }

    private onErrorReq(error) {
        this.alertService.error(error.message, null, null);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPcidssRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('pcidssRequirementListModification', (response) => this.loadAllRequirementCategory());
    }
}
