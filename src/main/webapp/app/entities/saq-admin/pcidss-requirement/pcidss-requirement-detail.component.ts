import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PcidssRequirement } from './pcidss-requirement.model';
import { PcidssRequirementService } from './pcidss-requirement.service';

@Component({
    selector: 'jhi-pcidss-requirement-detail',
    templateUrl: './pcidss-requirement-detail.component.html',
    styleUrls: [
                'pcidss-requirement.scss'
            ]
})
export class PcidssRequirementDetailComponent implements OnInit, OnDestroy {

    pcidssRequirement: PcidssRequirement;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private router: Router,
        private pcidssRequirementService: PcidssRequirementService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPcidssRequirements();
    }

    load(id) {
        this.pcidssRequirementService.find(id).subscribe((pcidssRequirement) => {
            this.pcidssRequirement = pcidssRequirement;
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/pcidss-requirement']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPcidssRequirements() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pcidssRequirementListModification',
            (response) => this.load(this.pcidssRequirement.id)
        );
    }
}
