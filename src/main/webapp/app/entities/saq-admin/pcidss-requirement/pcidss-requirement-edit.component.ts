import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { PcidssRequirement } from './pcidss-requirement.model';
import { BsModalService } from 'ngx-bootstrap';

import { PcidssRequirementService } from './pcidss-requirement.service';
import { RequirementCategory, RequirementCategoryService } from '../requirement-category';
import { ResponseWrapper, YesNoModalComponent } from '../../../shared';

@Component({
    selector: 'jhi-pcidss-requirement-edit',
    templateUrl: './pcidss-requirement-edit.component.html',
    styleUrls: [
                'pcidss-requirement.scss'
            ]
})
export class PcidssRequirementEditComponent implements OnInit {

    pcidssRequirement: PcidssRequirement;
    authorities: any[];
    routeSub: any;
    isSaving: boolean;
    requirementcategories: RequirementCategory[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: JhiAlertService,
        private pcidssRequirementService: PcidssRequirementService,
        private requirementCategoryService: RequirementCategoryService,
        private eventManager: JhiEventManager,
        private _bsModalService: BsModalService,
        private _toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.requirementCategoryService.queryMerchant()
        .subscribe((res: ResponseWrapper) => { this.requirementcategories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.pcidssRequirementService.find(params['id']).subscribe((pcidssRequirement) => {
                    this.pcidssRequirement = pcidssRequirement;
                });
            }
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/pcidss-requirement']);
    }

    save() {
        this.isSaving = true;
        if (this.pcidssRequirement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pcidssRequirementService.update(this.pcidssRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.pcidssRequirementService.create(this.pcidssRequirement));
        }
    }

    private subscribeToSaveResponse(result: Observable<PcidssRequirement>) {
        result.subscribe((res: PcidssRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PcidssRequirement) {
        this.eventManager.broadcast({ name: 'pcidssRequirementListModification', content: 'OK'});
        this.isSaving = false;
        this._toastr.success(`The requirement has been successfully updated`, `Update`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this._toastr.error(`The requirement cannot be updated`, `Error`);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackRequirementCategoryById(index: number, item: RequirementCategory) {
        return item.id;
    }

    previousStateAfterDelete(reqCategoryId) {
        const navigationExtras: NavigationExtras = {
                queryParams: {
                    requirementCategoryId : reqCategoryId.id,
                    requirementCategoryName : reqCategoryId.categoryName
                }
            };
        this.router.navigate(['/saq/admin/pcidss-requirement'], navigationExtras);
    }

    openConfirmationModalToDeleteRequirement(req) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete requirement number ' + req.requirementNumber + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.pcidssRequirementService.delete(req.id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'pcidssRequirementListModification',
                        content: 'Deleted an pcidssRequirement'
                    });
                    this.previousStateAfterDelete(req.requirementCategoryId);
                    this._toastr.success(`The requirement has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteRequirement(req) {
        this.openConfirmationModalToDeleteRequirement(req);
    }
}
