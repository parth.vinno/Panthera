import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Pipe, PipeTransform } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { PcidssRequirement } from './pcidss-requirement.model';
import { PcidssRequirementService } from './pcidss-requirement.service';
import { RequirementCategory } from '../requirement-category/requirement-category.model';
import { RequirementCategoryService } from '../requirement-category/requirement-category.service';
import { SaqQuestionService } from '../saq-question.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { BsModalService } from 'ngx-bootstrap';

@Pipe({
    name: 'filterRequirement'
})
export class FilterRequirement implements PipeTransform {
    transform(items: any[], criteria: any): any {
        return items.filter((item) => {
           for (const key in item ) {
             if ( ('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
             }
           }
           return false;
        });
    }
}

@Component({
    selector: 'jhi-pcidss-requirement',
    templateUrl: './pcidss-requirement.component.html',
    styleUrls: [
                'pcidss-requirement.scss'
            ]
})
export class PcidssRequirementComponent implements OnInit, OnDestroy {

    currentAccount: any;
    pcidssRequirements: PcidssRequirement[];
    requirementCategories: RequirementCategory[];
    requirementCategory: RequirementCategory;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedRequirementCategory;

    constructor(
        private requirementCategoryService: RequirementCategoryService,
        private pcidssRequirementService: PcidssRequirementService,
        private saqQuestionService: SaqQuestionService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private _bsModalService: BsModalService,
        private paginationConfig: PaginationConfig,
        private _toastr: ToastrService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.requirementCategory = new RequirementCategory();
        this.activatedRoute.queryParams.subscribe((params) => {
            this.requirementCategory.id = params['requirementCategoryId'];
            this.requirementCategory.categoryName = params['requirementCategoryName'];
        });
    }

    loadAllRequirementCategory() {
        this.requirementCategoryService.queryMerchant().subscribe(
            (res: ResponseWrapper) => this.onSuccessReq(res.json),
            (res: ResponseWrapper) => this.onErrorReq(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/saq/admin/pcidss-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.onSelectReqCategory(this.selectedRequirementCategory);
    }

    ngOnInit() {
        this.loadAllRequirementCategory();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPcidssRequirements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PcidssRequirement) {
        return item.id;
    }

    registerChangeInPcidssRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('pcidssRequirementListModification', (response) => this.onSelectReqCategory(this.selectedRequirementCategory));
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'requirementNumber') {
            result.push('requirementNumber');
        }
        return result;
    }

    onSelectReqCategory(reqCategoryValue) {
        this.pcidssRequirementService.findReqByReqCategory(reqCategoryValue.id, {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json));
    }

    private onSuccessReq(data) {
        this.requirementCategories = data;
        this.requirementCategories.sort( function(req1, req2) {
            if ( req1.id < req2.id ) {
                return -1;
            } else if ( req1.id > req2.id ) {
                return 1;
            } else {
                return 0;
            }
        });
        if ( typeof this.requirementCategory.id === 'undefined' ) {
            this.selectedRequirementCategory = this.requirementCategories[0];
            this.onSelectReqCategory(this.selectedRequirementCategory);
        } else {
            this.requirementCategories.map( (reqCat) => {
                if (reqCat.id === +this.requirementCategory.id) {
                    this.selectedRequirementCategory = reqCat;
                }
            });
            this.onSelectReqCategory(this.selectedRequirementCategory);
        }
    }

    private onErrorReq(error) {
        this.alertService.error(error.message, null, null);
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.pcidssRequirements = data;
        this.pcidssRequirements.map((requirement) => {
            this.saqQuestionService.findSaqLQuestionByRequirement(requirement.id).subscribe((res) => {
                if ( Object.keys(res.json).length > 0) {
                    requirement.isQuestionExist = true;
                } else {
                    requirement.isQuestionExist = false;
                }
            });
        });
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteRequirement(req) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete requirement number ' + req.requirementNumber + '?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.pcidssRequirementService.delete(req.id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'pcidssRequirementListModification',
                        content: 'Deleted an pcidssRequirement'
                    });
                    this.router.navigate(['saq/admin/pcidss-requirement']);
                    this._toastr.success(`The requirement has been successfully Deleted`, `Delete`);
                });
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteRequirement(req) {
        this.openConfirmationModalToDeleteRequirement(req);
    }
}
