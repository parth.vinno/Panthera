import { BaseEntity } from '../../../shared';

export class PcidssRequirement implements BaseEntity {
    constructor(
        public id?: number,
        public requirementNumber?: number,
        public requirement?: string,
        public requirementCategoryId?: BaseEntity,
        public isQuestionExist?: boolean
    ) {
    }
}
