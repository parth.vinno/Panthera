import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    PcidssRequirementService,
    PcidssRequirementComponent,
    PcidssRequirementDetailComponent,
    PcidssRequirementEditComponent,
    PcidssRequirementCreateComponent,
    FilterRequirement
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PcidssRequirementComponent,
        PcidssRequirementDetailComponent,
        PcidssRequirementEditComponent,
        PcidssRequirementCreateComponent,
        FilterRequirement
    ],
    entryComponents: [
        PcidssRequirementComponent,
        PcidssRequirementEditComponent,
        PcidssRequirementCreateComponent
    ],
    providers: [
        PcidssRequirementService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssPcidssRequirementModule {}
