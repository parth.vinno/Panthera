import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PcidssRequirement } from './pcidss-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import { BaseEntity } from '../../../shared';

@Injectable()
export class PcidssRequirementService {

    private resourceUrlSP = 'api/pcidss-requirements/service-provider';
    private resourceUrlMerchant = '/api/pcidss-requirements';
    private resourceUrlSPByReqCategory = '/api/pcidss-requirements/service-provider/category-id';
    private resourceUrlMerchantByReqCategory = '/api/pcidss-requirements/category-id';

    constructor(private http: Http) { }

    create(pcidssRequirement: PcidssRequirement): Observable<PcidssRequirement> {
        const copy = this.convert(pcidssRequirement);
        return this.http.post(this.resourceUrlMerchant, copy).map((res: Response) => {
            return res.json();
        });
    }

    createList(pcidssRequirement: PcidssRequirement[], categoryId: BaseEntity): Observable<PcidssRequirement[]> {
        return this.http.post(`${this.resourceUrlMerchant}/list/${categoryId}`, pcidssRequirement).map((res: Response) => {
            return res.json();
        });
    }

    update(pcidssRequirement: PcidssRequirement): Observable<PcidssRequirement> {
        const copy = this.convert(pcidssRequirement);
        return this.http.put(this.resourceUrlMerchant, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PcidssRequirement> {
        return this.http.get(`${this.resourceUrlMerchant}/${id}`).map((res: Response) => {
              return res.json();
        });
    }

    findByReqCategory(requirementCategoryId: BaseEntity): Observable<PcidssRequirement> {
        return this.http.get(`${this.resourceUrlMerchantByReqCategory}/${requirementCategoryId}`).map((res: Response) => {
            return res.json();
      });
    }

    findReqByReqCategory(requirementCategoryId: BaseEntity, req?: any): Observable<PcidssRequirement> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlMerchantByReqCategory}/${requirementCategoryId}`, options)
        .map((res: Response) => this.convertResponse(res));
    }

    findByRequirementNumber(requirementNumber: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrlMerchant}/by-requirement-number?requirementNumber=${requirementNumber}`)
        .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlMerchant, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlMerchant}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(pcidssRequirement: PcidssRequirement): PcidssRequirement {
        const copy: PcidssRequirement = Object.assign({}, pcidssRequirement);
        return copy;
    }
}
