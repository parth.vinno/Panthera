import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequirementCategory } from './requirement-category.model';
import { RequirementCategoryService } from './requirement-category.service';
import { Principal, ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-requirement-category-create',
    templateUrl: './requirement-category-create.component.html',
    styleUrls: [
                'requirement-category.scss'
            ]
})

export class RequirementCategoryCreateComponent implements OnInit {

    requirementCategory: RequirementCategory;
    categoryName: string;
    authorities: any[];
    isSaving: boolean;
    routeSub: any;
    requirementCategoryForm: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: JhiAlertService,
        private requirementCategoryService: RequirementCategoryService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
    ) {
        this.requirementCategory = new RequirementCategory();
    }

    ngOnInit() {
        this.requirementCategoryForm = new FormGroup ({
            categoryName: new FormControl('', Validators.compose([
                                                                  Validators.required,
                                                                  Validators.minLength(5),
                                                                  Validators.maxLength(100),
                                                                  Validators.pattern('^[a-zA-Z0-9,.!:? ]*$')
                                                                  ]))
          });
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    previousState() {
        this.router.navigate(['/saq/admin/requirement-category']);
    }

    save(reqCategory) {
        const isDuplicateCategory = (reqCategory.categoryName).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isCategoryExist = true;
        this.requirementCategoryService.queryMerchant().subscribe(
            (res: ResponseWrapper) => {
                const categoryList = res.json;
                for (let i = 0; i < categoryList.length; i++) {
                    const modifiedCategory  = (categoryList[i].categoryName).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                    if (modifiedCategory === isDuplicateCategory) {
                        this._toastr.error(`The requirement Category aleady exists`, `Error`);
                        isCategoryExist = false;
                        break;
                    }
                }
                if (isCategoryExist) {
                    this.requirementCategory = reqCategory;
                     if (this.requirementCategory.id === undefined) {
                      this.subscribeToSaveResponse(
                          this.requirementCategoryService.updateMerchant(this.requirementCategory))
                    }
                }
                },
            (res: ResponseWrapper) => {
                this.onError(res.json)}
        );
    }

    private subscribeToSaveResponse(result: Observable<RequirementCategory>) {
        result.subscribe((res: RequirementCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: RequirementCategory) {
        this.eventManager.broadcast({ name: 'requirementCategoryListModification', content: 'OK'});
        this._toastr.success(`The requirement Category has been successfully Saved`, `Save`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this._toastr.error(`${error.message}`, `Error`);
        this.alertService.error(error.message, null, null);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
