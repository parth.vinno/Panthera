import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { RequirementCategory } from './requirement-category.model';
import { RequirementCategoryService } from './requirement-category.service';

@Component({
    selector: 'jhi-requirement-category-detail',
    templateUrl: './requirement-category-detail.component.html',
    styleUrls: [
                'requirement-category.scss'
            ]
})
export class RequirementCategoryDetailComponent implements OnInit, OnDestroy {

    requirementCategory: RequirementCategory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private router: Router,
        private eventManager: JhiEventManager,
        private requirementCategoryService: RequirementCategoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRequirementCategories();
    }

    load(id) {
        this.requirementCategoryService.findMerchant(id).subscribe((requirementCategory) => {
            this.requirementCategory = requirementCategory;
        });
    }
    previousState() {
        this.router.navigate(['/saq/admin/requirement-category']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRequirementCategories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'requirementCategoryListModification',
            (response) => this.load(this.requirementCategory.id)
        );
    }
}
