import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { RequirementCategory } from './requirement-category.model';
import { RequirementCategoryService } from './requirement-category.service';
import { PcidssRequirementService } from '../pcidss-requirement/pcidss-requirement.service';
import { YesNoModalComponent } from '../../../shared';

@Component({
    selector: 'jhi-requirement-category-edit',
    templateUrl: './requirement-category-edit.component.html',
    styleUrls: [
                'requirement-category.scss'
            ]
})
export class RequirementCategoryEditComponent implements OnInit {

    requirementCategory: RequirementCategory;
    authorities: any[];
    isSaving: boolean;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: JhiAlertService,
        private requirementCategoryService: RequirementCategoryService,
        private pcidssRequirementService: PcidssRequirementService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
        private _bsModalService: BsModalService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.requirementCategoryService.findMerchant(params['id']).subscribe((requirementCategory) => {
                    this.pcidssRequirementService.findByReqCategory(requirementCategory.id).subscribe((req) => {
                        if ( Object.keys(req).length > 0) {
                            requirementCategory.isRequirementExist = true;
                        } else {
                            requirementCategory.isRequirementExist = false;
                        }
                    });
                    this.requirementCategory = requirementCategory;
                });
            }
        });
    }

    previousState() {
        this.router.navigate(['/saq/admin/requirement-category']);
    }

    save() {
        this.isSaving = true;
        if (this.requirementCategory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.requirementCategoryService.updateMerchant(this.requirementCategory));
        }
        this.previousState();
    }

    private subscribeToSaveResponse(result: Observable<RequirementCategory>) {
        result.subscribe((res: RequirementCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: RequirementCategory) {
        this._toastr.success(`The requirement Category has been successfully Updated`, `Update`);
        this.eventManager.broadcast({ name: 'requirementCategoryListModification', content: 'OK'});
        this.isSaving = false;
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this._toastr.error(`The requirement category cannot be updated`, `Error`);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteCategory(id) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete requirement category?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.requirementCategoryService.deleteMerchant(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'requirementCategoryListModification',
                        content: 'Deleted an requirementCategory'
                    });
                });
                this.router.navigate(['/saq/admin/requirement-category']);
                this._toastr.success(`The requirement Category has been successfully Deleted`, `Delete`);
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteExpectedTest(id) {
        this.openConfirmationModalToDeleteCategory(id);
    }
}
