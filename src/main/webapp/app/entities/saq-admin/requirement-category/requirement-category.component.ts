import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Pipe, PipeTransform } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { RequirementCategory } from './requirement-category.model';
import { RequirementCategoryService } from './requirement-category.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { PcidssRequirementService } from '../pcidss-requirement/pcidss-requirement.service';

@Pipe({
    name: 'filterRequirementCategory'
})
export class FilterRequirementCategory implements PipeTransform {
    transform(items: any[], criteria: any): any {
        return items.filter((item) => {
           for (const key in item ) {
             if ( ('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
             }
           }
           return false;
        });
    }
}

@Component({
    selector: 'jhi-requirement-category',
    templateUrl: './requirement-category.component.html',
    styleUrls: [
                'requirement-category.scss'
            ]
})
export class RequirementCategoryComponent implements OnInit, OnDestroy {

    currentAccount: any;
    requirementCategories: RequirementCategory[];
    requirementCategoryCopy: RequirementCategory[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private requirementCategoryService: RequirementCategoryService,
        private pcidssRequirementService: PcidssRequirementService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private _bsModalService: BsModalService,
        private _toastr: ToastrService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() {
        this.requirementCategoryService.queryMerchant({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers);
                this.requirementCategories = this.requirementCategoryCopy },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/saq/admin/requirement-category'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRequirementCategories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: RequirementCategory) {
        return item.id;
    }

    registerChangeInRequirementCategories() {
        this.eventSubscriber = this.eventManager.subscribe('requirementCategoryListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.requirementCategoryCopy = data;
        this.requirementCategoryCopy.map( (reqCat) => {
          this.pcidssRequirementService.findByReqCategory(reqCat.id).subscribe((req) => {
              if ( Object.keys(req).length > 0) {
                  reqCat.isRequirementExist = true;
              } else {
                  reqCat.isRequirementExist = false;
              }
          });
      });
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteCategory(id) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete requirement category?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.requirementCategoryService.deleteMerchant(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'requirementCategoryListModification',
                        content: 'Deleted an requirementCategory'
                    });
                });
                this.router.navigate(['/saq/admin/requirement-category']);
                this._toastr.success(`The requirement Category has been successfully Deleted`, `Delete`);
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteExpectedTest(id) {
        this.openConfirmationModalToDeleteCategory(id);
    }
}
