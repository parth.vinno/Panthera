import { BaseEntity } from './../../../shared';

export class RequirementCategory implements BaseEntity {
    constructor(
        public id?: number,
        public categoryName?: string,
        public isRequirementExist?: boolean
    ) {
    }
}
