import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    RequirementCategoryService,
    RequirementCategoryComponent,
    RequirementCategoryDetailComponent,
    RequirementCategoryEditComponent,
    RequirementCategoryCreateComponent,
    FilterRequirementCategory
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RequirementCategoryComponent,
        RequirementCategoryDetailComponent,
        RequirementCategoryEditComponent,
        RequirementCategoryCreateComponent,
        FilterRequirementCategory
    ],
    entryComponents: [
        RequirementCategoryComponent,
        RequirementCategoryEditComponent,
        RequirementCategoryCreateComponent
    ],
    providers: [
        RequirementCategoryService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssRequirementCategoryModule {}
