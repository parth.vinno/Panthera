import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { RequirementCategory } from './requirement-category.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class RequirementCategoryService {

    private resourceUrlSP = 'api/requirement-categories/service-provider';
    private resourceUrlMerchant = 'api/requirement-categories';
    private showId: number;

    constructor(private http: Http) { }

    createServiceProvider(requirementCategory: RequirementCategory): Observable<RequirementCategory> {
        const copy = this.convert(requirementCategory);
        return this.http.post(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    createMerchant(requirementCategory: RequirementCategory): Observable<RequirementCategory> {
        const copy = this.convert(requirementCategory);
        return this.http.post(this.resourceUrlMerchant, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateServiceProvider(requirementCategory: RequirementCategory): Observable<RequirementCategory> {
        const copy = this.convert(requirementCategory);
        return this.http.put(this.resourceUrlSP, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateMerchant(requirementCategory: RequirementCategory): Observable<RequirementCategory> {
        const copy = this.convert(requirementCategory);
        return this.http.put(this.resourceUrlMerchant, copy).map((res: Response) => {
            return res.json();
        });
    }

    findServiceProvider(id: number): Observable<RequirementCategory> {
        return this.http.get(`${this.resourceUrlSP}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findRequirementByCategoryName(categoryName: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrlMerchant}/by-category-name?categoryName=${categoryName}`)
        .map((res: Response) => this.convertResponse(res));
    }

    findMerchant(id: number): Observable<RequirementCategory> {
        return this.http.get(`${this.resourceUrlMerchant}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findMerchantByCategoryName(categoryName: string): Observable<RequirementCategory> {
        return this.http.get(`${this.resourceUrlMerchant}/${categoryName}`).map((res: Response) => {
            return res.json();
        });
    }

    queryServiceProvider(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlSP, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryMerchant(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlMerchant, options)
            .map((res: Response) => this.convertResponse(res));
    }

    deleteMerchant(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlMerchant}/${id}`);
    }

    deleteServiceProvider(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlSP}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(requirementCategory: RequirementCategory): RequirementCategory {
        const copy: RequirementCategory = Object.assign({}, requirementCategory);
        return copy;
    }
}
