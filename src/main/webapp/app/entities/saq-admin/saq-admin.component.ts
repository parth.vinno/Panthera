import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiAlertService } from 'ng-jhipster';

import { SaqQuestion } from './saq-question.model';
import { SaqQuestionService } from './saq-question.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-saq-admin',
    templateUrl: './saq-admin.component.html'
})
export class SaqAdminComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

}
