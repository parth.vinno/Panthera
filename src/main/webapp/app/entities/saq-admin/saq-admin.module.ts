import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from 'ng2-ckeditor';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../shared';
import { PcidssRequirementCategoryModule } from './requirement-category/requirement-category.module';
import { PcidssPcidssRequirementModule } from './pcidss-requirement/pcidss-requirement.module';
import { PcidssExpectedTestModule } from './expected-test/expected-test.module';
import { PcidssServiceCategoryModule } from './service-category/service-category.module';
import { PcidssBusinessTypeModule } from './merchant-business-type/merchant-business-type.module';
import { PcidssPaymentChannelModule } from './payment-channel/payment-channel.module';

import {
    SaqAdminComponent,
    saqAdminRoute,
    SaqQuestionService,
    LeftNavComponent,
    SaqQuestionCreateComponent,
    SaqQuestionEditComponent,
    RequirementCategoryResolvePagingParams,
    PcidssRequirementResolvePagingParams,
    ExpectedTestResolvePagingParams,
    ServiceCategoryResolvePagingParams,
    BusinessTypeResolvePagingParams,
    PaymentChannelResolvePagingParams,
    ServiceTypeService
} from './';
import { DisplaySaqQuestionErrorsComponent } from '../form-validation-error-display/display-saq-question-error.component';

const ENTITY_STATES = [
    ...saqAdminRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        CKEditorModule,
        CommonModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        PcidssRequirementCategoryModule,
        PcidssPcidssRequirementModule,
        PcidssExpectedTestModule,
        PcidssServiceCategoryModule,
        PcidssBusinessTypeModule,
        PcidssPaymentChannelModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SaqAdminComponent,
        SaqQuestionCreateComponent,
        SaqQuestionEditComponent,
        LeftNavComponent,
        DisplaySaqQuestionErrorsComponent
    ],
    entryComponents: [
        SaqAdminComponent
    ],
    providers: [
        SaqQuestionService,
        ServiceTypeService,
        RequirementCategoryResolvePagingParams,
        PcidssRequirementResolvePagingParams,
        ExpectedTestResolvePagingParams,
        ServiceCategoryResolvePagingParams,
        BusinessTypeResolvePagingParams,
        PaymentChannelResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssSaqAdminModule {}
