import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SaqAdminComponent } from './saq-admin.component';
import { SaqQuestionCreateComponent } from './saq-question-create/saq-question-create.component';
import { SaqQuestionEditComponent } from './saq-question-edit/saq-question-edit.component';
import { Principal } from '../../shared';
import { RequirementCategoryComponent } from './requirement-category/requirement-category.component';
import { RequirementCategoryDetailComponent } from './requirement-category/requirement-category-detail.component';
import { RequirementCategoryEditComponent } from './requirement-category/requirement-category-edit.component';
import { RequirementCategoryCreateComponent } from './requirement-category/requirement-category-create.component';
import { PcidssRequirementComponent } from './pcidss-requirement/pcidss-requirement.component';
import { PcidssRequirementDetailComponent } from './pcidss-requirement/pcidss-requirement-detail.component';
import { PcidssRequirementEditComponent } from './pcidss-requirement/pcidss-requirement-edit.component';
import { PcidssRequirementCreateComponent } from './pcidss-requirement/pcidss-requirement-create.component';
import { ExpectedTestComponent } from './expected-test/expected-test.component';
import { ExpectedTestDetailComponent } from './expected-test/expected-test-detail.component';
import { ExpectedTestEditComponent } from './expected-test/expected-test-edit.component';
import { MerchantBusinessTypeCreateComponent } from './merchant-business-type/merchant-business-type-create.component';
import { MerchantBusinessTypeComponent } from './merchant-business-type/merchant-business-type.component';
import { MerchantBusinessTypeEditComponent } from './merchant-business-type/merchant-business-type-edit.component';
import { MerchantBusinessTypeDetailComponent } from './merchant-business-type/merchant-business-type-detail.component';
import { PaymentChannelCreateComponent } from './payment-channel/payment-channel-create.component';
import { PaymentChannelDetailComponent } from './payment-channel/payment-channel-detail.component';
import { PaymentChannelEditComponent } from './payment-channel/payment-channel-edit.component';
import { PaymentChannelComponent } from './payment-channel/payment-channel.component';
import { ServiceCategoryCreateComponent } from './service-category/service-category-create.component';
import { ServiceCategoryListComponent } from './service-category/service-category-list.component';
import { ServiceCategoryDetailComponent } from './service-category/service-category-detail.component';
import { ServiceCategoryEditComponent } from './service-category/service-category-edit.component';

@Injectable()
export class RequirementCategoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class PcidssRequirementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'requirementNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class ExpectedTestResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class ServiceCategoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class BusinessTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class PaymentChannelResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const saqAdminRoute: Routes = [
    {
        path: 'saq/admin',
        component: SaqAdminComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
            pageTitle: 'pcidssApp.saqQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
                   {
                       path: 'requirement-category',
                       component: RequirementCategoryComponent,
                       resolve: {
                           'pagingParams': RequirementCategoryResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                    },
                    {
                        path: 'requirement-category/:id',
                        component: RequirementCategoryDetailComponent,
                        data: {
                            authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                            pageTitle: 'pcidssApp.saqQuestion.home.title'
                        },
                        canActivate: [UserRouteAccessService]
                    }, {
                        path: 'requirement-category/:id/edit',
                        component: RequirementCategoryEditComponent,
                        data: {
                            authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                            pageTitle: 'pcidssApp.saqQuestion.home.title'
                        },
                        canActivate: [UserRouteAccessService]
                    }, {
                        path: 'requirement-category-new',
                        component: RequirementCategoryCreateComponent,
                        data: {
                            authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                            pageTitle: 'pcidssApp.saqQuestion.home.title'
                        },
                        canActivate: [UserRouteAccessService],
                    }, {
                       path: 'saq-question-create',
                       component: SaqQuestionCreateComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       }
                   }, {
                       path: 'saq-question-edit',
                       component: SaqQuestionEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       }
                   }, {
                       path: 'pcidss-requirement',
                       component: PcidssRequirementComponent,
                       resolve: {
                           'pagingParams': PcidssRequirementResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'pcidss-requirement/:id',
                       component: PcidssRequirementDetailComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'pcidss-requirement/:id/edit',
                       component: PcidssRequirementEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService],
                   }, {
                       path: 'pcidss-requirement-new',
                       component: PcidssRequirementCreateComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService],
                   }, {
                       path: 'expected-test',
                       component: ExpectedTestComponent,
                       resolve: {
                           'pagingParams': ExpectedTestResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'expected-test/:id',
                       component: ExpectedTestDetailComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'expected-test/:id/edit',
                       component: ExpectedTestEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchant-business-types',
                       component: MerchantBusinessTypeComponent,
                       resolve: {
                           'pagingParams': BusinessTypeResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchant-business-types/:id/edit',
                       component: MerchantBusinessTypeEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchant-business-types-new',
                       component: MerchantBusinessTypeCreateComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'merchant-business-types/:id',
                       component: MerchantBusinessTypeDetailComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-channel-new',
                       component: PaymentChannelCreateComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-channel',
                       component: PaymentChannelComponent,
                       resolve: {
                           'pagingParams': PaymentChannelResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-channel/:id',
                       component: PaymentChannelDetailComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'payment-channel/:id/edit',
                       component: PaymentChannelEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'service-category-new',
                       component: ServiceCategoryCreateComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'service-category',
                       component: ServiceCategoryListComponent,
                       resolve: {
                           'pagingParams': ServiceCategoryResolvePagingParams
                       },
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'service-category/:id',
                       component: ServiceCategoryDetailComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: 'service-category/:id/edit',
                       component: ServiceCategoryEditComponent,
                       data: {
                           authorities: ['ROLE_ADMIN', 'ROLE_TENANT_ADMIN'],
                           pageTitle: 'pcidssApp.saqQuestion.home.title'
                       },
                       canActivate: [UserRouteAccessService]
                   }, {
                       path: '',
                       redirectTo: 'requirement-category',
                       pathMatch: 'full'
                   }
               ]
    }
];
