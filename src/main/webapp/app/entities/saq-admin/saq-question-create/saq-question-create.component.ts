import { Component, NgZone, OnInit, ViewChild, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { SaqQuestion } from '../saq-question.model';
import { SaqQuestionService } from '../saq-question.service';
import { PcidssRequirement, PcidssRequirementService } from '../pcidss-requirement';
import { RequirementCategory, RequirementCategoryService } from '../requirement-category';
import { ExpectedTest, ExpectedTestService } from '../expected-test';
import { Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-saq-question-create',
    templateUrl: './saq-question-create.component.html',
    styleUrls: [
                'saq-question-create.component.scss'
                ]
})

export class SaqQuestionCreateComponent implements OnInit, OnChanges {

saqQuestionForm: FormGroup;
saqQuestion: SaqQuestion;
expectedTest: ExpectedTest;
requirementCategories: RequirementCategory[];
pcidssRequirements: PcidssRequirement[];
requirementPerCategory: PcidssRequirement[];
existedExpectedTests: ExpectedTest[];
newExpectedTestsList: any = [];
newSectionExpectedTestsList: any = [];
newSubQuestionExpectedTestsList: any = [];
newSubSectionExpectedTestsList: any = [];
newSubSubQuestionExpectedTestsList: any = [];
newSubSubSectionExpectedTestsList: any = [];
newPointExpectedTestsList: any = [];
newSubPointExpectedTestsList: any = [];
newSubSubPointExpectedTestsList: any = [];
selectedCategory = '--Select Requirement Category--';
totalItems: any;
queryCount: any;
control: any;
subcontrol: any;
subQcontrol: any;
subSControl: any;
subsubcontrol: any;
subsubsubcontrol: any;
expcontrol: any;
isSection = false;
isSubQuestion = false;
disableSectionSubQuestionBtn= true;
commonExpectedTest: any;
isExpectedTestExist = false;
isExpTestExist = false;

constructor( private _requirementCategoryService: RequirementCategoryService,
        private _pcidssRequirementService: PcidssRequirementService,
        private _expectedTestService: ExpectedTestService,
        private _saqQuestionService: SaqQuestionService,
        private _ngZone: NgZone,
        private _alertService: JhiAlertService,
        private _fb: FormBuilder,
        private _bsModalService: BsModalService,
        private _toastr: ToastrService,
        private eventManager: JhiEventManager) {
    this.saqQuestion = new SaqQuestion();
    this.expectedTest = new ExpectedTest();
}

ngOnInit() {
    this._ngZone.run(() => {
        this._requirementCategoryService.queryMerchant().subscribe(
                (res: ResponseWrapper) => {
                    this.requirementCategories = res.json;
                },
                (res: ResponseWrapper) => this.onError(res.json)
        );
        this._pcidssRequirementService.query().subscribe(
                (res: ResponseWrapper) => {
                    this.pcidssRequirements = res.json;
                },
                (res: ResponseWrapper) => this.onError(res.json)
        );
        this.getExpectedTestsList();
    });
    this.createQuestionForm();
    this.saqQuestionForm.controls['questionNumber'].valueChanges.subscribe((value) => {
        if (value === null || value === '') {
            this.disableSectionSubQuestionBtn = true;
        } else {
            this.disableSectionSubQuestionBtn = false;
            if ((this.saqQuestionForm.value.sections).length > 0) {
                this.sections.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber:  value + '.' + (String.fromCharCode(97 + this.sections.controls.indexOf(control)))
                            });
                        }
                )
            }
            if ((this.saqQuestionForm.value.questions).length > 0) {
                this.questions.controls.map(
                        (control, index) => {
                            control.patchValue({
                                questionNumber:  value + '.' + (this.questions.controls.indexOf(control) + 1)
                            });
                            if ((control.value.sections).length > 0) {
                                this.subcontrol = this.saqQuestionForm.get(`questions.${index}.sections`) as FormArray;
                                this.subcontrol.controls.map(
                                        (subcontrol, indexj) => {
                                            subcontrol.patchValue({
                                                questionNumber: control.value.questionNumber + '.' +
                                                (String.fromCharCode(97 + indexj))
                                            });
                                        }
                                )
                            }
                            if ((control.value.questions).length > 0) {
                                this.subQcontrol = this.saqQuestionForm.get(`questions.${index}.questions`) as FormArray;
                                this.subQcontrol.controls.map(
                                        (subQcontrol, indexj) => {
                                            subQcontrol.patchValue({
                                                questionNumber: control.value.questionNumber + '.' +
                                                (indexj + 1)
                                            });
                                            if ((subQcontrol.value.sections).length > 0) {
                                                this.subSControl = this.saqQuestionForm.get(`questions.${index}.questions.${indexj}.sections`) as FormArray;
                                                this.subSControl.controls.map(
                                                        (subSControl, indexk) => {
                                                            subSControl.patchValue({
                                                                questionNumber: subQcontrol.value.questionNumber + '.'
                                                                + (String.fromCharCode(97 + indexk))
                                                            });
                                                        }
                                                )
                                            }
                                        }
                                )
                            }
                        }
                )
            }
        }
    });
    this.saqQuestionForm.controls['pcidssRequirementId'].valueChanges.subscribe((value) => {
        if (value === 'null') {
            this.saqQuestionForm.controls['pcidssRequirementId'].setValue(null, {onlySelf: true});
        }
    });
}

ngOnChanges(id) {
    this.saqQuestionForm.reset({
        questionNumber: null,
        questionText: '',
        pcidssRequirementId: id,
        responseRequired: false,
        selectedExpectedTest: null,
        part: false,
        addBtnHide: false,
        newExpectedBox: false,
        newExpectTest: null
    });
    this.setSubquestion();
}

setSubquestion() {
    const sectionControl = <FormArray>this.saqQuestionForm.controls['sections'];
    sectionControl.controls = [];
    const subQControl = <FormArray>this.saqQuestionForm.controls['questions'];
    subQControl.controls = [];
    const subSectionControl = <FormArray>this.saqQuestionForm.controls['subQuestions'];
    subSectionControl.controls = [];
    const subSubQControl = <FormArray>this.saqQuestionForm.controls['expectedTests'];
    subSubQControl.controls = [];
}

createQuestionForm() {
    this.saqQuestionForm = this._fb.group({
        questionNumber: [null, [Validators.required, CustomValidators.questionNumber]],
        questionText: null,
        pcidssRequirementId: null,
        responseRequired: false,
        part: false,
        addBtnHide: false,
        newExpectedBox: false,
        selectedExpectedTest: null,
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        sections: this._fb.array([]),
        questions: this._fb.array([]),
        subQuestions: this._fb.array([]),
        expectedTests: this._fb.array([])
    });
}

get questions(): FormArray {
    return this.saqQuestionForm.get('questions') as FormArray;
};

get sections(): FormArray {
    return this.saqQuestionForm.get('sections') as FormArray;
};

getSubsubSections(form) {
    return form.controls.sections.controls;
}

getPoints(form) {
    return form.controls.questions.controls;
}

getSubsubQuestion(form) {
    return form.controls.questions.controls;
}

getSubsubsubSections(form) {
    return form.controls.sections.controls;
}

getSubSectionPoints(form) {
    return form.controls.questions.controls;
}

getSubSubSectionPoints(form) {
    return form.controls.questions.controls;
}

getExpectedTest(form) {
    return form.controls.expectedTests.controls;
}

private onError(error) {
    this._alertService.error(error.message, null, null);
}

onSelectedCategory(category) {
    this.requirementPerCategory = this.pcidssRequirements.filter((p) => p.requirementCategoryId.id === category.id);
    this.ngOnChanges(null);
}

/* Sub-Questions */
initSubQuestionRows() {
    return this._fb.group({
        questionNumber: null,
        questionText: null,
        pcidssRequirementId: this.saqQuestionForm.value.pcidssRequirementId,
        responseRequired: false,
        part: false,
        selectedExpectedTest: null,
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        addBtnHide: false,
        newExpectedBox: false,
        sections: this._fb.array([]),
        questions: this._fb.array([]),
        subQuestions: this._fb.array([]),
        expectedTests: this._fb.array([])
    });
}

/* Sections */
initSectionRows() {
    return this._fb.group({
        questionNumber: null,
        questionText: null,
        pcidssRequirementId: this.saqQuestionForm.value.pcidssRequirementId,
        responseRequired: false,
        part: true,
        addBtnHide: false,
        newExpectedBox: false,
        selectedExpectedTest: null,
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        expectedTests: this._fb.array([]),
        subQuestions: this._fb.array([]),
        sections: this._fb.array([]),
        questions: this._fb.array([])
    });
}

initExpectedTestingRows() {
    return this._fb.group(new ExpectedTest());
}

getExpectedTestsList() {
    this._expectedTestService.query().subscribe(
            (res: ResponseWrapper) => {
                this.totalItems = res.headers.get('X-Total-Count');
                this.queryCount = this.totalItems;
                this._expectedTestService.query({size: this.queryCount}).subscribe((response: ResponseWrapper) => {
                    this.existedExpectedTests = response.json;
                    this.existedExpectedTests.sort( function(expT1, expT2) {
                        if ( expT1.testName < expT2.testName ) {
                            return -1;
                        } else if ( expT1.testName > expT2.testName ) {
                            return 1;
                        } else {
                            return 0;
                        }
                    });
                },
                (response: ResponseWrapper) => this.onError(response.json)
                );
            },
            (res: ResponseWrapper) => this.onError(res.json)
    );
}

openConfirmationModal(index, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a ' + str + '?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            if (str === 'section') {
                this.control = <FormArray>this.saqQuestionForm.controls['sections'];
                this.control.removeAt(index);
                this.sections.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber:  this.saqQuestionForm.value.questionNumber + '.' + (String.fromCharCode(97 + this.sections.controls.indexOf(control)))
                            });
                        }
                )
            } else if (str === 'subquestion') {
                this.control = <FormArray>this.saqQuestionForm.controls['questions'];
                this.control.removeAt(index);
                this.questions.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber:  this.saqQuestionForm.value.questionNumber + '.' + (this.questions.controls.indexOf(control) + 1)
                            });
                        }
                )
            }
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a ' + str + '?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            if (str === 'section') {
                this.subcontrol = this.saqQuestionForm.get(`questions.${subQIndex}.sections`) as FormArray;
                this.subcontrol.removeAt(subSubIndex);
                this.subcontrol.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber: this.saqQuestionForm.value.questions[subQIndex].questionNumber
                                + '.' + (String.fromCharCode(97 + this.subcontrol.controls.indexOf(control)))
                            });
                        }
                )
            } else if (str === 'subquestion') {
                this.subcontrol = this.saqQuestionForm.get(`questions.${subQIndex}.questions`) as FormArray;
                this.subcontrol.removeAt(subSubIndex);
                this.subcontrol.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber: this.saqQuestionForm.value.questions[subQIndex].questionNumber + '.' + (this.subcontrol.controls.indexOf(control) + 1)
                            });
                        }
                )
            }
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForPoints(indexi: number, indexm: number, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqQuestionForm.get(`sections.${indexi}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqQuestionForm.value.sections[indexi].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubPoints(indexi: number, indexj: number, indexm: number) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point ?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqQuestionForm.value.questions[indexi].sections[indexj].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubSubPoints(indexi: number, indexj: number, indexk: number, indexm: number) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubSubQuestion(indexi, indexj, indexk) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a section?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subsubcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections`) as FormArray;
            this.subsubcontrol.removeAt(indexk);
            this.subsubcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqQuestionForm.value.questions[indexi].questions[indexj].questionNumber + '.'
                            + (String.fromCharCode(97 + this.subsubcontrol.controls.indexOf(control)))
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

romanize(num) {
    const lookup = { m: 1000, cm: 900, d: 500, cd: 400, c: 100, xc: 90,
            l: 50, xl: 40, x: 10, ix: 9, v: 5, iv: 4, i: 1 };
    let  roman = '', i;
    for ( i in lookup ) {
        if (lookup.hasOwnProperty(i)) {
            while ( num >= lookup[i] ) {
                roman += i;
                num -= lookup[i];
            }
        }
    }
    return roman;
}

deletePointRow(indexi: number, indexm: number, str) {
    this.openConfirmationModalForPoints(indexi, indexm, str);
}

addNewPointRow(indexi: number) {
    this.subcontrol = this.saqQuestionForm.get(`sections.${indexi}.questions`) as FormArray;
    this.subcontrol.push(this.initSubQuestionRows());
    this.subcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.sections[indexi].questionNumber + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubSectionPointRow(indexi: number, indexj: number, indexm: number) {
    this.openConfirmationModalForSubPoints(indexi, indexj, indexm);
}

addNewSubSectionPointRow(indexi: number, indexj: number) {
    this.subsubcontrol = this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions`) as FormArray;
    this.subsubcontrol.push(this.initSubQuestionRows());
    this.subsubcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.questions[indexi].sections[indexj].questionNumber + '.'
                    + this.romanize(this.subsubcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubSubSectionPointRow(indexi: number, indexj: number, indexk: number, indexm: number, str) {
    this.openConfirmationModalForSubSubPoints(indexi, indexj, indexk, indexm);
}

addNewSubSubSectionPointRow(indexi: number, indexj: number, indexk: number) {
    this.subsubsubcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions`) as FormArray;
    this.subsubsubcontrol.push(this.initSubQuestionRows());
    this.subsubsubcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questionNumber + '.'
                    + this.romanize(this.subsubsubcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

addNewSubquestionRow() {
    this.control = <FormArray>this.saqQuestionForm.controls['questions'];
    this.control.push(this.initSubQuestionRows());
    this.questions.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber:  this.saqQuestionForm.value.questionNumber + '.' + (this.questions.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubquestionRow(index: number, str) {
    this.openConfirmationModal(index, str);
}

addNewSubSectionsRow() {
    this.isSection = true;
    this.control = <FormArray>this.saqQuestionForm.controls['sections'];
    this.control.push(this.initSectionRows());
    this.sections.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber:  this.saqQuestionForm.value.questionNumber + '.' + (String.fromCharCode(97 + this.sections.controls.indexOf(control)))
                });
            }
    )
}

deleteSubSectionsRow(index: number, str) {
    this.openConfirmationModal(index, str);
}

addNewSubsubSectionsRow(index: number) {
    this.subcontrol = this.saqQuestionForm.get(`questions.${index}.sections`) as FormArray;
    this.subcontrol.push(this.initSectionRows());
    this.subcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.questions[index].questionNumber + '.' + (String.fromCharCode(97 + this.subcontrol.controls.indexOf(control)))
                });
            }
    )
}

deleteSubsubSectionsRow(subQIndex: number, subSubIndex: number, str) {
    this.openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str)
}

addNewSubsubQuestionsRow(index: number) {
    this.subcontrol = this.saqQuestionForm.get(`questions.${index}.questions`) as FormArray;
    this.subcontrol.push(this.initSubQuestionRows());
    this.subcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.questions[index].questionNumber + '.' + (this.subcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubsubQuestionsRow(subQIndex: number, subSubIndex: number, str) {
    this.openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str)
}

addNewSubsubsubSectionsRow(indexi: number, indexj: number) {
    this.subsubcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections`) as FormArray;
    this.subsubcontrol.push(this.initSectionRows());
    this.subsubcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqQuestionForm.value.questions[indexi].questions[indexj].questionNumber + '.'
                    + (String.fromCharCode(97 + this.subsubcontrol.controls.indexOf(control)))
                });
            }
    )
}

deleteSubsubsubSectionsRow(indexi: number, indexj: number, indexk: number) {
    this.openConfirmationModalForSubSubQuestion(indexi, indexj, indexk);
}

deleteFromExpectedTestList(expTestId, expectedTestsList) {
    if ((expectedTestsList).length > 0) {
        expectedTestsList.forEach((expTest) => {
            if (expTest === expTestId) {
                this._expectedTestService.delete(expTestId).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'expectedTestListModification',
                        content: 'Deleted an expectedTest'
                    });
                });
            }
        })
    }
    this.getExpectedTestsList();
}

openConfirmationModalToRemoveExpectedTest(control, index) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a expected test?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            control.removeAt(index);
        } else if (result === false) {
            return false;
        }
    });
}

deleteExpectedTestRow(etIndex: number) {
    this.expcontrol = <FormArray>this.saqQuestionForm.controls['expectedTests'];
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubExpectedTestRow(indexi: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`sections.${indexi}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deletePointExpectedTestRow(indexi: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`sections.${indexi}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubQuestionExpectedTestRow(indexi: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubExpectedTestRow(indexi: number, indexj: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubPointExpectedTestRow(indexi: number, indexj: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubQuestionExpectedTestRow(indexi: number, indexj: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubSubQuestionExpectedTestRow(indexi: number, indexj: number, indexk: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubPointExpectedTestRow(indexi: number, indexj: number, indexk: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

onSelectQuestionExpectedTest() {
    const selectedExpectedTest = this.saqQuestionForm.value.selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

addExpectedTest() {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqQuestionForm.value.selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`expectedTests`) as FormArray;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqQuestionForm.patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== 'null' && selectedExpectedTest === 'add') {
        this.saqQuestionForm.patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addNewExpTest() {
    this.expcontrol = this.saqQuestionForm.get(`expectedTests`) as FormArray;
    this.saqQuestionForm.patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.newExpectTest, this.expcontrol, this.newExpectedTestsList);
}

isExpectedTestingExisted(expTestValue, expcontrol, expectedtestList) {
    if ((this.existedExpectedTests).length > 0) {
        this.existedExpectedTests.map((expTest) => {
            if (expTest.testName === expTestValue) {
                this.isExpectedTestExist = true;
                return false;
            }
        });
    }
    if (this.isExpectedTestExist === true) {
        this._toastr.warning('The expected test already exist in the available list. Please select', 'Warning');
        this.isExpectedTestExist = false;
    } else {
        const isDuplicateExpectedTest = (expTestValue).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isTestExist = true;
        this._expectedTestService.query().subscribe(
                (res: ResponseWrapper) => {
                    const testList = res.json;
                    for (let i = 0; i < testList.length; i++) {
                        const modifiedTest  = (testList[i].testName).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                        if (modifiedTest === isDuplicateExpectedTest) {
                            this._toastr.warning(`The expected test already exist in the available list. Please select`, `Warning`);
                            isTestExist = false;
                            break;
                        }
                    }
                    if (isTestExist) {
                        this.expectedTest.testName = expTestValue;
                        if (this.expectedTest.id === undefined) {
                            this.subscribeToSaveResponseExpectedtest(
                                    this._expectedTestService.create(this.expectedTest), expcontrol, expectedtestList)
                        }
                    }
                }, (res: ResponseWrapper) => {
        this.onError(res.json)}
        );
    }
}

addSectionNewExpTest(i) {
    this.expcontrol = this.saqQuestionForm.get(`sections.${i}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`sections.${i}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.sections[i].newExpectTest, this.expcontrol, this.newSectionExpectedTestsList);
}

addSectionExpectedTest(i) {
    const selectedExpectedTest = this.saqQuestionForm.value.sections[i].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`sections.${i}.expectedTests`) as FormArray;
    const cntlstr = `sections.${i}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addQuestionsNewExpTest(i) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].newExpectTest, this.expcontrol, this.newSubQuestionExpectedTestsList);
}

addQuestionsExpectedTest(i) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addPointExpectedTest(i, m) {
    const selectedExpectedTest = this.saqQuestionForm.value.sections[i].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`sections.${i}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `sections.${i}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addPointNewExpTest(i, m) {
    this.expcontrol = this.saqQuestionForm.get(`sections.${i}.questions.${m}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`sections.${i}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.sections[i].questions[m].newExpectTest, this.expcontrol, this.newPointExpectedTestsList);
}

addSubSectionsNewExpTest(i, j) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.sections.${j}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}.sections.${j}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].sections[j].newExpectTest, this.expcontrol, this.newSubSectionExpectedTestsList);
}

addSubSectionsExpectedTest(i, j) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].sections[j].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.sections.${j}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.sections.${j}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubPointExpectedTest(i, j, m) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].sections[j].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.sections.${j}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubPointNewExpTest(i, j, m) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].sections[j].questions[m].newExpectTest, this.expcontrol, this.newSubPointExpectedTestsList);
}

addSubSubPointExpectedTest(i, j, k, m) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].questions[j].sections[k].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.questions.${j}.sections.${k}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubSubPointNewExpTest(i, j, k, m) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].questions[j].sections[k].questions[m].newExpectTest,
            this.expcontrol, this.newSubSubPointExpectedTestsList);
}

addSubQuestionsNewExpTest(i, j) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}.questions.${j}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].questions[j].newExpectTest, this.expcontrol, this.newSubSubQuestionExpectedTestsList);
}

addSubQuestionsExpectedTest(i, j) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].questions[j].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.questions.${j}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubSubSectionsNewExpTest(i, j, k) {
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.expectedTests`) as FormArray;
    this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqQuestionForm.value.questions[i].questions[j].sections[k].newExpectTest, this.expcontrol, this.newSubSubSectionExpectedTestsList);
}

addSubSubSectionsExpectedTest(i, j, k) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[i].questions[j].sections[k].selectedExpectedTest;
    this.expcontrol = this.saqQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.questions.${j}.sections.${k}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addExpectedTestCommonFunction(expcontrol, selectedExpectedTest, cntlstr) {
    this.isExpTestExist = false;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        if (expcontrol !== null && (expcontrol).length > 0) {
            expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqQuestionForm.get(cntlstr).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== 'null' && selectedExpectedTest === 'add') {
        this.saqQuestionForm.get(cntlstr).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

private subscribeToSaveResponseExpectedtest(result: Observable<ExpectedTest>, expControl, expectedTestList) {
    result.subscribe((res: ExpectedTest) =>
    this.onSaveExpectedTestSuccess(res, expControl, expectedTestList), (res: Response) => this.onSaveErrorExpectedTest(res));
}

private onSaveExpectedTestSuccess(result: ExpectedTest, expControl, expectedTestList) {
    expControl.push(this._fb.group(result));
    expectedTestList.push(result.id);
    this.getExpectedTestsList();
}

onSelectSectionExpectedTest(indexi: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.sections[indexi].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`sections.${indexi}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectPointExpectedTest(indexi: number, indexm: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.sections[indexi].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`sections.${indexi}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubQuestionExpectedTest(indexi: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSectionExpectedTest(indexi: number, indexj: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].sections[indexj].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubPointExpectedTest(indexi: number, indexj: number, indexm: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].sections[indexj].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubQuestionExpectedTest(indexi: number, indexj: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].questions[indexj].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubSubQuestionExpectedTest(indexi: number, indexj: number, indexk: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubPointExpectedTest(indexi: number, indexj: number, indexk: number, indexm: number) {
    const selectedExpectedTest = this.saqQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectedRequirement() {
    this.ngOnChanges(this.saqQuestionForm.value.pcidssRequirementId);
}

save() {
    if (this.saqQuestionForm.value.questions || this.saqQuestionForm.value.sections) {
        this.saqQuestionForm.value.subQuestions = this.saqQuestionForm.value.sections.concat(this.saqQuestionForm.value.questions);
        this.saqQuestionForm.value.sections.map((x) => {
            if (x.questions) {
                x.subQuestions = x.questions;
            }
        });
        this.saqQuestionForm.value.questions.map((x) => {
            if (x.questions || x.sections) {
                x.questions.map((y) => {
                    if (y.sections) {
                        y.sections.map((z) => {
                            z.subQuestions = z.questions;
                        })
                    }
                    y.subQuestions = y.sections;
                });
                x.sections.map((y) => {
                    y.subQuestions = y.questions;
                });
                x.subQuestions = x.sections.concat(x.questions);
            }
        });
    }
    this.saqQuestion.questionNumber = this.saqQuestionForm.value.questionNumber;
    this.saqQuestion.responseRequired = this.saqQuestionForm.value.responseRequired;
    this.saqQuestion.part = this.saqQuestionForm.value.part;
    this.saqQuestion.questionText = this.saqQuestionForm.value.questionText;
    this.saqQuestion.expectedTests = this.saqQuestionForm.value.expectedTests;
    this.saqQuestion.subQuestions = this.saqQuestionForm.value.subQuestions;
    this.saqQuestion.pcidssRequirementId = this.saqQuestionForm.value.pcidssRequirementId;
    this._saqQuestionService.findSaqQuestionByQuestionNumber(this.saqQuestion.questionNumber).subscribe(
            (res: ResponseWrapper) => {
                if (res.status === 200) {
                    this._toastr.error(`The Question ${this.saqQuestion.questionNumber} already exists`, `Error`);
                }
            },
            (res: ResponseWrapper) => {
                if ( this.saqQuestion.id === undefined ) {
                    this.subscribeToSaveResponseSaqQuestions(
                            this._saqQuestionService.create(this.saqQuestion));
                }
                this.onError(res.status);
            });
}

private subscribeToSaveResponseSaqQuestions(result: Observable<SaqQuestion>) {
    result.subscribe((res: SaqQuestion) =>
    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
}

private onSaveSuccess(result: SaqQuestion) {
    this._toastr.success(`The question ${result.questionNumber} has been successfully saved`, `Save`);
    this.ngOnChanges(null);
}

private onSaveError(error) {
    try {
        const err = error.json();
        this._toastr.error(`${err.description}`, `Error`);
    } catch (exception) {
        error.message = error.text();
    }
    this.onError(error);
}

private onSaveErrorExpectedTest(error) {
    try {
        error.json();
    } catch (exception) {
        error.message = error.text();
    }
    this.onError(error);
}

cancel() {
    this.ngOnChanges(null);
}
}
