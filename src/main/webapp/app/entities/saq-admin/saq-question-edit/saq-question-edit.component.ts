import { Component, NgZone, OnInit, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

import { SaqQuestion } from '../saq-question.model';
import { SaqQuestionService } from '../saq-question.service';
import { PcidssRequirement, PcidssRequirementService } from '../pcidss-requirement';
import { RequirementCategory, RequirementCategoryService } from '../requirement-category';
import { ExpectedTest, ExpectedTestService } from '../expected-test';
import { Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { CustomValidators } from '../validators/custom-validators';

@Component({
    selector: 'jhi-saq-question-edit',
    templateUrl: './saq-question-edit.component.html',
    styleUrls: [
                'saq-question-edit.component.scss'
                ]
})

export class SaqQuestionEditComponent implements OnInit, OnChanges {

saqEditQuestionForm: FormGroup;
saqQuestion: SaqQuestion;
expectedTest: ExpectedTest;
requirementCategories: RequirementCategory[];
pcidssRequirements: PcidssRequirement[];
requirementPerCategory: PcidssRequirement[];
saqQuestionList: SaqQuestion[];
existedExpectedTests: ExpectedTest[];
newExpectedTestsList: any = [];
newSectionExpectedTestsList: any = [];
newSubQuestionExpectedTestsList: any = [];
newSubSectionExpectedTestsList: any = [];
newSubSubQuestionExpectedTestsList: any = [];
newSubSubSectionExpectedTestsList: any = [];
newPointExpectedTestsList: any = [];
newSubPointExpectedTestsList: any = [];
newSubSubPointExpectedTestsList: any = [];
totalItems: any;
queryCount: any;
expcontrol: any;
isSection = false;
requirementcategoryId = '';
requirementId = '';
questionId = '';
control: any;
section:  any = [];
subQuestion:  any = [];
subQControl: any;
sectionControl: any;
subSection:  any = [];
subSubQuestion:  any = [];
subSubQControl: any;
subSectionControl: any;
subSubSubSection: any = [];
sectionPoint: any = [];
subSubSubSectionControl: any;
subcontrol: any;
subsubcontrol: any;
subsubsubcontrol: any;
sectionPointControl: any;
sectionSubPointControl: any;
sectionSubPoint: any = [];
sectionSubSubPointControl: any;
sectionSubSubPoint: any = [];
isExpectedTestExist = false;
selectedExpectedTest = '';
isExpTestExist = false;
isUpdate = false;

constructor( private _requirementCategoryService: RequirementCategoryService,
        private _pcidssRequirementService: PcidssRequirementService,
        private _expectedTestService: ExpectedTestService,
        private _saqQuestionService: SaqQuestionService,
        private _ngZone: NgZone,
        private _alertService: JhiAlertService,
        private _fb: FormBuilder,
        private _toastr: ToastrService,
        private _bsModalService: BsModalService,
        private router: Router,
        private eventManager: JhiEventManager) {
    this.saqQuestion = new SaqQuestion();
    this.expectedTest = new ExpectedTest();
}

ngOnInit() {
    this.editQuestionForm();
    this._ngZone.run(() => {
        this._requirementCategoryService.queryMerchant().subscribe(
                (res: ResponseWrapper) => {
                    this.requirementCategories = res.json;
                },
                (res: ResponseWrapper) => this.onError(res.json)
        );
        this.getExpectedTestsList();
    });
}

getExpectedTestsList() {
    this._expectedTestService.query().subscribe(
            (res: ResponseWrapper) => {
                this.totalItems = res.headers.get('X-Total-Count');
                this.queryCount = this.totalItems;
                this._expectedTestService.query({size: this.queryCount}).subscribe((response: ResponseWrapper) => {
                    this.existedExpectedTests = response.json;
                    this.existedExpectedTests.sort( function(expT1, expT2) {
                        if ( expT1.testName < expT2.testName ) {
                            return -1;
                        } else if ( expT1.testName > expT2.testName ) {
                            return 1;
                        } else {
                            return 0;
                        }
                    });
                },
                (response: ResponseWrapper) => this.onError(response.json)
                );
            },
            (res: ResponseWrapper) => this.onError(res.json)
    );
}

ngOnChanges(id) {
    this.saqEditQuestionForm.reset({
        questionNumber: null,
        questionText: '',
        pcidssRequirementId: id,
        responseRequired: false,
        selectedExpectedTest: null,
        part: false,
        addBtnHide: false,
        newExpectedBox: false,
        newExpectTest: null
    });
    this.setSubquestion();
}

setSubquestion() {
    this.saqEditQuestionForm.setControl('sections', this._fb.array([]));
    this.saqEditQuestionForm.setControl('questions', this._fb.array([]));
    this.saqEditQuestionForm.setControl('subQuestions', this._fb.array([]));
    this.saqEditQuestionForm.setControl('expectedTests', this._fb.array([]));
}

onSelectedCategory(category) {
    if (category === '') {
        this.requirementPerCategory = [];
        this.requirementId = '';
        this.questionId = ''
    } else {
        this.requirementId = '';
        this.requirementPerCategory = category.pcidssRequirements;
        this.questionId = ''
    }
}

onSelectRequirement(requirement) {
    this.saqQuestionList = [];
    this._saqQuestionService.findSaqLQuestionByRequirement(requirement.id).subscribe(
            (res: ResponseWrapper) => {
                this.saqQuestionList = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
    );
    if (this.requirementcategoryId && requirement === '') {
        this.saqQuestionList = [];
        this.questionId = '';
    } else {
        this.questionId = '';
    }
}

onSelectQuestion(question) {
    this.sectionControl = <FormArray>this.saqEditQuestionForm.controls['sections'];
    this.subQControl = <FormArray>this.saqEditQuestionForm.controls['questions'];
    this.expcontrol = <FormArray>this.saqEditQuestionForm.controls['expectedTests'];
    this.section = [];
    this.subQuestion = [];
    this.sectionControl.controls = [];
    this.subQControl.controls = [];
    this.subSection = [];
    this.subSubQuestion = [];
    this.subSubSubSection = [];
    if (question.subQuestions !== null && (question.subQuestions).length > 0) {
        question.subQuestions.forEach((subQ) => {
            if (/[a-z]/g.test(subQ.questionNumber)) {
                this.section.push(subQ);
            } else {
                this.subQuestion.push(subQ);
            }
        });
        this.sortControls(this.section);
        this.sortControls(this.subQuestion);
    }
    this.saqEditQuestionForm.patchValue({
        id: question.id,
        questionNumber: question.questionNumber,
        questionText: question.questionText,
        pcidssRequirementId: question.pcidssRequirementId,
        part: question.part
    });
    this.expcontrol.controls = [];
    if ((question.expectedTests).length > 0) {
        this.saqEditQuestionForm.patchValue({
            responseRequired: true
        });
        question.expectedTests.forEach((expTest) => {
            this.expcontrol.push(this._fb.group({
                id: expTest.id,
                testName: expTest.testName
            }));
        })
    }
    if ((this.section).length > 0) {
        this.section.map((section, i) => {
            this.setValueOfControls(this.sectionControl, section);
            this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.expectedTests`) as FormArray;
            this.expcontrol.controls = [];
            if ((section.expectedTests).length > 0) {
                this.saqEditQuestionForm.get(`sections.${i}`).patchValue({
                    responseRequired: true
                });
                section.expectedTests.forEach((expTest) => {
                    this.expcontrol.push(this._fb.group({
                        id: expTest.id,
                        testName: expTest.testName
                    }));
                })
            }
            this.sectionPointControl = this.saqEditQuestionForm.get(`sections.${i}.questions`) as FormArray;
            if (section.subQuestions !== null && (section.subQuestions).length > 0) {
                this.sectionPoint = [];
                this.sectionPoint = section.subQuestions;
                this.sortControls(this.sectionPoint);
                if ((this.sectionPoint).length > 0) {
                    this.sectionPoint.map((point, j) => {
                        this.setValueOfControls(this.sectionPointControl, point);
                        this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.questions.${j}.expectedTests`) as FormArray;
                        this.expcontrol.controls = [];
                        if ((point.expectedTests).length > 0) {
                            this.saqEditQuestionForm.get(`sections.${i}.questions.${j}`).patchValue({
                                responseRequired: true
                            });
                            point.expectedTests.forEach((expTest) => {
                                this.expcontrol.push(this._fb.group({
                                    id: expTest.id,
                                    testName: expTest.testName
                                }));
                            })
                        }
                    });
                }
            }
        });
    }
    if ((this.subQuestion).length > 0) {
        this.subQuestion.map((subQuestion, index) => {
            this.setValueOfControls(this.subQControl, subQuestion);
            this.expcontrol = this.saqEditQuestionForm.get(`questions.${index}.expectedTests`) as FormArray;
            this.expcontrol.controls = [];
            if ((subQuestion.expectedTests).length > 0) {
                this.saqEditQuestionForm.get(`questions.${index}`).patchValue({
                    responseRequired: true
                });
                subQuestion.expectedTests.forEach((expTest) => {
                    this.expcontrol.push(this._fb.group({
                        id: expTest.id,
                        testName: expTest.testName
                    }));
                })
            }
            this.subSectionControl = this.saqEditQuestionForm.get(`questions.${index}.sections`) as FormArray;
            this.subSubQControl = this.saqEditQuestionForm.get(`questions.${index}.questions`) as FormArray;
            if (subQuestion.subQuestions !== null && (subQuestion.subQuestions).length > 0) {
                this.subSection = [];
                this.subSubQuestion = [];
                this.subSectionControl.controls = [];
                this.subSubQControl.controls = [];
                subQuestion.subQuestions.forEach((subSubQ) => {
                    if (/[a-z]/g.test(subSubQ.questionNumber)) {
                        this.subSection.push(subSubQ);
                    } else {
                        this.subSubQuestion.push(subSubQ);
                    }
                });
                this.sortControls(this.subSection);
                this.sortControls(this.subSubQuestion);
                if ((this.subSection).length > 0) {
                    this.subSection.map((section, j) => {
                        this.setValueOfControls(this.subSectionControl, section);
                        this.expcontrol = this.saqEditQuestionForm.get(`questions.${index}.sections.${j}.expectedTests`) as FormArray;
                        this.expcontrol.controls = [];
                        if ((section.expectedTests).length > 0) {
                            this.saqEditQuestionForm.get(`questions.${index}.sections.${j}`).patchValue({
                                responseRequired: true
                            });
                            section.expectedTests.forEach((expTest) => {
                                this.expcontrol.push(this._fb.group({
                                    id: expTest.id,
                                    testName: expTest.testName
                                }));
                            })
                        }
                        this.sectionSubPointControl = this.saqEditQuestionForm.get(`questions.${index}.sections.${j}.questions`) as FormArray;
                        if (section.subQuestions !== null && (section.subQuestions).length > 0) {
                            this.sectionSubPoint = [];
                            this.sectionSubPoint = section.subQuestions;
                            this.sortControls(this.sectionSubPoint);
                            if ((this.sectionSubPoint).length > 0) {
                                this.sectionSubPoint.map((point, k) => {
                                    this.setValueOfControls(this.sectionSubPointControl, point);
                                    this.expcontrol = this.saqEditQuestionForm.get(`questions.${index}.sections.${j}.questions.${k}.expectedTests`) as FormArray;
                                    this.expcontrol.controls = [];
                                    if ((point.expectedTests).length > 0) {
                                        this.saqEditQuestionForm.get(`questions.${index}.sections.${j}.questions.${k}`).patchValue({
                                            responseRequired: true
                                        });
                                        point.expectedTests.forEach((expTest) => {
                                            this.expcontrol.push(this._fb.group({
                                                id: expTest.id,
                                                testName: expTest.testName
                                            }));
                                        })
                                    }
                                });
                            }
                        }
                    });
                }
                if ((this.subSubQuestion).length > 0) {
                    this.subSubQuestion.map((subQ, indexj) => {
                        this.setValueOfControls(this.subSubQControl, subQ);
                        this.expcontrol = this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.expectedTests`) as FormArray;
                        this.expcontrol.controls = [];
                        if ((subQ.expectedTests).length > 0) {
                            this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}`).patchValue({
                                responseRequired: true
                            });
                            subQ.expectedTests.forEach((expTest) => {
                                this.expcontrol.push(this._fb.group({
                                    id: expTest.id,
                                    testName: expTest.testName
                                }));
                            })
                        }
                        this.subSubSubSectionControl = this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections`) as FormArray;
                        if (subQ.subQuestions !== null && (subQ.subQuestions).length > 0) {
                            this.subSubSubSection = [];
                            this.subSubSubSection = subQ.subQuestions;
                            this.subSubSubSectionControl.controls = [];
                            this.sortControls(this.subSubSubSection);
                            if ((this.subSubSubSection).length > 0) {
                                this.subSubSubSection.map((section, indexk) => {
                                    this.setValueOfControls(this.subSubSubSectionControl, section);
                                    this.expcontrol = this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections.${indexk}.expectedTests`) as FormArray;
                                    this.expcontrol.controls = [];
                                    if ((section.expectedTests).length > 0) {
                                        this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections.${indexk}`).patchValue({
                                            responseRequired: true
                                        });
                                        section.expectedTests.forEach((expTest) => {
                                            this.expcontrol.push(this._fb.group({
                                                id: expTest.id,
                                                testName: expTest.testName
                                            }));
                                        })
                                    }
                                    this.sectionSubSubPointControl =
                                        this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections.${indexk}.questions`) as FormArray;
                                    if (section.subQuestions !== null && (section.subQuestions).length > 0) {
                                        this.sectionSubSubPoint = [];
                                        this.sectionSubSubPoint = section.subQuestions;
                                        this.sortControls(this.sectionSubSubPoint);
                                        if ((this.sectionSubSubPoint).length > 0) {
                                            this.sectionSubSubPoint.map((point, j) => {
                                               this.setValueOfControls(this.sectionSubSubPointControl, point);
                                               this.expcontrol =
                                               this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections.${indexk}.questions.${j}.expectedTests`) as FormArray;
                                               this.expcontrol.controls = [];
                                               if ((point.expectedTests).length > 0) {
                                                   this.saqEditQuestionForm.get(`questions.${index}.questions.${indexj}.sections.${indexk}.questions.${j}`).patchValue({
                                                       responseRequired: true
                                                   });
                                                   point.expectedTests.forEach((expTest) => {
                                                       this.expcontrol.push(this._fb.group({
                                                           id: expTest.id,
                                                           testName: expTest.testName
                                                       }));
                                                   })
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

setValueOfControls(control, value) {
    control.push(this._fb.group({
        id: value.id,
        questionNumber: value.questionNumber,
        questionText: value.questionText,
        responseRequired: false,
        pcidssRequirementId: value.pcidssRequirementId,
        part: value.part,
        selectedExpectedTest: '',
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        addBtnHide: false,
        newExpectedBox: false,
        sections: this._fb.array([]),
        questions: this._fb.array([]),
        subQuestions: this._fb.array([]),
        expectedTests: this._fb.array([])
    }));
}

sortControls(list) {
    list.sort( function(question1, question2) {
        if ( question1.questionNumber < question2.questionNumber ) {
            return -1;
        } else if ( question1.questionNumber > question2.questionNumber ) {
            return 1;
        } else {
            return 0;
        }
    });
}

editQuestionForm() {
    this.saqEditQuestionForm = this._fb.group({
        id: null,
        questionNumber: [null, [Validators.required, CustomValidators.questionNumber]],
        questionText: null,
        pcidssRequirementId: null,
        responseRequired: false,
        part: false,
        addBtnHide: false,
        newExpectedBox: false,
        selectedExpectedTest: '',
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        sections: this._fb.array([]),
        questions: this._fb.array([]),
        subQuestions: this._fb.array([]),
        expectedTests: this._fb.array([])
    });
}

/* Sub-Questions */
initSubQuestionRows() {
    return this._fb.group({
        questionNumber: null,
        questionText: null,
        pcidssRequirementId: this.saqEditQuestionForm.value.pcidssRequirementId,
        responseRequired: false,
        part: false,
        selectedExpectedTest: '',
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        addBtnHide: false,
        newExpectedBox: false,
        sections: this._fb.array([]),
        questions: this._fb.array([]),
        subQuestions: this._fb.array([]),
        expectedTests: this._fb.array([])
    });
}

/* Sections */
initSectionRows() {
    return this._fb.group({
        questionNumber: null,
        questionText: null,
        pcidssRequirementId: this.saqEditQuestionForm.value.pcidssRequirementId,
        responseRequired: false,
        part: true,
        addBtnHide: false,
        newExpectedBox: false,
        selectedExpectedTest: '',
        newExpectTest: [null, [CustomValidators.newExpectTest]],
        expectedTests: this._fb.array([]),
        subQuestions: this._fb.array([]),
        sections: this._fb.array([]),
        questions: this._fb.array([])
    });
}

/* Expected Test*/
initExpectedTestingRows() {
    return this._fb.group(new ExpectedTest());
}

get sections(): FormArray {
    return this.saqEditQuestionForm.get('sections') as FormArray;
};

get questions(): FormArray {
    return this.saqEditQuestionForm.get('questions') as FormArray;
};

getSubsubSections(form) {
    return form.controls.sections.controls;
}

getPoints(form) {
    return form.controls.questions.controls;
}

getSubSectionPoints(form) {
    return form.controls.questions.controls;
}

getSubSubSectionPoints(form) {
    return form.controls.questions.controls;
}

getSubsubQuestion(form) {
    return form.controls.questions.controls;
}

getSubsubsubSections(form) {
    return form.controls.sections.controls;
}

getExpectedTest(form) {
    return form.controls.expectedTests.controls;
}

openConfirmationModal(index, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a ' + str + '?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            if (str === 'section') {
                this.sectionControl = <FormArray>this.saqEditQuestionForm.controls['sections'];
                this.sectionControl.removeAt(index);
                this.sections.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber:  this.saqEditQuestionForm.value.questionNumber + '.' + (String.fromCharCode(97 + this.sections.controls.indexOf(control)))
                            });
                        }
                )
            } else if (str === 'subquestion') {
                this.subQControl = <FormArray>this.saqEditQuestionForm.controls['questions'];
                this.subQControl.removeAt(index);
                this.questions.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber:  this.saqEditQuestionForm.value.questionNumber + '.' + (this.questions.controls.indexOf(control) + 1)
                            });
                        }
                )
            }
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a ' + str + '?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            if (str === 'section') {
                this.subSectionControl = this.saqEditQuestionForm.get(`questions.${subQIndex}.sections`) as FormArray;
                this.subSectionControl.removeAt(subSubIndex);
                this.subSectionControl.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber: this.saqEditQuestionForm.value.questions[subQIndex].questionNumber + '.' +
                                (String.fromCharCode(97 + this.subSectionControl.controls.indexOf(control)))
                            });
                        }
                )
            } else if (str === 'subquestion') {
                this.subSubQControl = this.saqEditQuestionForm.get(`questions.${subQIndex}.questions`) as FormArray;
                this.subSubQControl.removeAt(subSubIndex);
                this.subSubQControl.controls.forEach(
                        (control) => {
                            control.patchValue({
                                questionNumber: this.saqEditQuestionForm.value.questions[subQIndex].questionNumber + '.' +
                                (this.subSubQControl.controls.indexOf(control) + 1)
                            });
                        }
                )
            }
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubSubQuestion(indexi, indexj, indexk) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a section?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subSubSubSectionControl = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections`) as FormArray;
            this.subSubSubSectionControl.removeAt(indexk);
            this.subSubSubSectionControl.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqEditQuestionForm.value.questions[indexi].questions[indexj].questionNumber + '.'
                            + (String.fromCharCode(97 + this.subSubSubSectionControl.controls.indexOf(control)))
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForPoints(indexi: number, indexm: number, str) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqEditQuestionForm.get(`sections.${indexi}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqEditQuestionForm.value.sections[indexi].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubPoints(indexi: number, indexj: number, indexm: number) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point ?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqEditQuestionForm.value.questions[indexi].sections[indexj].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

openConfirmationModalForSubSubPoints(indexi: number, indexj: number, indexk: number, indexm: number) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a point?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this.subcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions`) as FormArray;
            this.subcontrol.removeAt(indexm);
            this.subcontrol.controls.forEach(
                    (control) => {
                        control.patchValue({
                            questionNumber: this.saqEditQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questionNumber
                            + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                        });
                    }
            )
        } else if (result === false) {
            return false;
        }
    });
}

romanize(num) {
    const lookup = { m: 1000, cm: 900, d: 500, cd: 400, c: 100, xc: 90,
            l: 50, xl: 40, x: 10, ix: 9, v: 5, iv: 4, i: 1 };
    let roman = '', i;
    for ( i in lookup ) {
        if (lookup.hasOwnProperty(i)) {
            while ( num >= lookup[i] ) {
                roman += i;
                num -= lookup[i];
            }
        }
    }
    return roman;
}

deletePointRow(indexi: number, indexm: number, str) {
    this.openConfirmationModalForPoints(indexi, indexm, str);
}

addNewPointRow(indexi: number) {
    this.subcontrol = this.saqEditQuestionForm.get(`sections.${indexi}.questions`) as FormArray;
    this.subcontrol.push(this.initSubQuestionRows());
    this.subcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.sections[indexi].questionNumber + '.' + this.romanize(this.subcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubSectionPointRow(indexi: number, indexj: number, indexm: number) {
    this.openConfirmationModalForSubPoints(indexi, indexj, indexm);
}

addNewSubSectionPointRow(indexi: number, indexj: number) {
    this.subsubcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions`) as FormArray;
    this.subsubcontrol.push(this.initSubQuestionRows());
    this.subsubcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.questions[indexi].sections[indexj].questionNumber + '.' +
                    this.romanize(this.subsubcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubSubSectionPointRow(indexi: number, indexj: number, indexk: number, indexm: number, str) {
    this.openConfirmationModalForSubSubPoints(indexi, indexj, indexk, indexm);
}

addNewSubSubSectionPointRow(indexi: number, indexj: number, indexk: number) {
    this.subsubsubcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions`) as FormArray;
    this.subsubsubcontrol.push(this.initSubQuestionRows());
    this.subsubsubcontrol.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questionNumber + '.'
                    + this.romanize(this.subsubsubcontrol.controls.indexOf(control) + 1)
                });
            }
    )
}

addNewSubSectionsRow() {
    this.sectionControl = <FormArray>this.saqEditQuestionForm.controls['sections'];
    this.sectionControl.push(this.initSectionRows());
    this.sections.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber:  this.saqEditQuestionForm.value.questionNumber + '.' + (String.fromCharCode(97 + this.sections.controls.indexOf(control)))
                });
            }
    )
}

deleteSubSectionsRow(index: number, str) {
    this.openConfirmationModal(index, str);
}

addNewSubquestionRow() {
    this.subQControl = <FormArray>this.saqEditQuestionForm.controls['questions'];
    this.subQControl.push(this.initSubQuestionRows());
    this.questions.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber:  this.saqEditQuestionForm.value.questionNumber + '.' +
                    (this.questions.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubquestionRow(index: number, str) {
    this.openConfirmationModal(index, str);
}

addNewSubsubSectionsRow(index: number) {
    this.subSectionControl = this.saqEditQuestionForm.get(`questions.${index}.sections`) as FormArray;
    this.subSectionControl.push(this.initSectionRows());
    this.subSectionControl.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.questions[index].questionNumber + '.' +
                    (String.fromCharCode(97 + this.subSectionControl.controls.indexOf(control)))
                });
            }
    )
}

deleteSubsubSectionsRow(subQIndex: number, subSubIndex: number, str) {
    this.openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str);
}

addNewSubsubQuestionsRow(index: number) {
    this.subSubQControl = this.saqEditQuestionForm.get(`questions.${index}.questions`) as FormArray;
    this.subSubQControl.push(this.initSubQuestionRows());
    this.subSubQControl.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.questions[index].questionNumber + '.' +
                    (this.subSubQControl.controls.indexOf(control) + 1)
                });
            }
    )
}

deleteSubsubQuestionsRow(subQIndex: number, subSubIndex: number, str) {
    this.openConfirmationModalForSubQuestion(subQIndex, subSubIndex, str);
}

addNewSubsubsubSectionsRow(indexi: number, indexj: number) {
    this.subSubSubSectionControl = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections`) as FormArray;
    this.subSubSubSectionControl.push(this.initSectionRows());
    this.subSubSubSectionControl.controls.forEach(
            (control) => {
                control.patchValue({
                    questionNumber: this.saqEditQuestionForm.value.questions[indexi].questions[indexj].questionNumber + '.'
                    + (String.fromCharCode(97 + this.subSubSubSectionControl.controls.indexOf(control)))
                });
            }
    )
}

deleteSubsubsubSectionsRow(indexi: number, indexj: number, indexk: number) {
    this.openConfirmationModalForSubSubQuestion(indexi, indexj, indexk);
}

onSelectQuestionExpectedTest() {
    const selectedExpectedTest = this.saqEditQuestionForm.value.selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSectionExpectedTest(indexi: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.sections[indexi].selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`sections.${indexi}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubQuestionExpectedTest(indexi: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSectionExpectedTest(indexi: number, indexj: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].sections[indexj].selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubQuestionExpectedTest(indexi: number, indexj: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].questions[indexj].selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubSubQuestionExpectedTest(indexi: number, indexj: number, indexk: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].selectedExpectedTest;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectPointExpectedTest(indexi: number, indexm: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.sections[indexi].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`sections.${indexi}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubPointExpectedTest(indexi: number, indexj: number, indexm: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].sections[indexj].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

onSelectSubSubPointExpectedTest(indexi: number, indexj: number, indexk: number, indexm: number) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[indexi].questions[indexj].sections[indexk].questions[indexm].selectedExpectedTest;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions.${indexm}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    }
}

addExpectedTest() {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addNewExpTest() {
    this.expcontrol = this.saqEditQuestionForm.get(`expectedTests`) as FormArray;
    this.saqEditQuestionForm.patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.newExpectTest, this.expcontrol, this.newExpectedTestsList);
}

addSectionNewExpTest(i) {
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`sections.${i}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.sections[i].newExpectTest,
            this.expcontrol, this.newSectionExpectedTestsList);
}

isExpectedTestingExisted(expTestValue, expcontrol, expectedtestList) {
    if ((this.existedExpectedTests).length > 0) {
        this.existedExpectedTests.map((expTest) => {
            if (expTest.testName === expTestValue) {
                this.isExpectedTestExist = true;
                return false;
            }
        });
    }
    if (this.isExpectedTestExist === true) {
        this._toastr.warning('The expected test already exist in the available list. Please select', 'Warning');
        this.isExpectedTestExist = false;
    } else {
        const isDuplicateExpectedTest = (expTestValue).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isTestExist = true;
        this._expectedTestService.query().subscribe(
                (res: ResponseWrapper) => {
                    const testList = res.json;
                    for (let i = 0; i < testList.length; i++) {
                        const modifiedTest  = (testList[i].testName).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                        if (modifiedTest === isDuplicateExpectedTest) {
                            this._toastr.warning(`The expected test already exist in the available list. Please select`, `Warning`);
                            isTestExist = false;
                            break;
                        }
                    }
                    if (isTestExist) {
                        this.expectedTest.testName = expTestValue;
                        if (this.expectedTest.id === undefined) {
                            this.subscribeToSaveResponseExpectedtest(
                                    this._expectedTestService.create(this.expectedTest), expcontrol, expectedtestList)
                        }
                    }
                }, (res: ResponseWrapper) => this.onError(res.json)
        );
    }
}

private subscribeToSaveResponseExpectedtest(result: Observable<ExpectedTest>, expControl, expectedTestList) {
    result.subscribe((res: ExpectedTest) =>
    this.onSaveExpectedTestSuccess(res, expControl, expectedTestList), (res: Response) => this.onSaveErrorExpectedTest(res));
}

private onSaveExpectedTestSuccess(result: ExpectedTest, expControl, expectedTestList) {
    expControl.push(this._fb.group(result));
    expectedTestList.push(result.id);
    this.getExpectedTestsList();
}

addSectionExpectedTest(i) {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.sections[i].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(`sections.${i}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(`sections.${i}`).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addQuestionsNewExpTest(i) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].newExpectTest, this.expcontrol, this.newSubQuestionExpectedTestsList);
}

addQuestionsExpectedTest(i) {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(`questions.${i}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(`questions.${i}`).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addSubSectionsNewExpTest(i, j) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.sections.${j}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}.sections.${j}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].sections[j].newExpectTest, this.expcontrol, this.newSubSectionExpectedTestsList);
}

addSubSectionsExpectedTest(i, j) {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].sections[j].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.sections.${j}.expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(`questions.${i}.sections.${j}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(`questions.${i}.sections.${j}`).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addSubQuestionsNewExpTest(i, j) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}.questions.${j}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].questions[j].newExpectTest, this.expcontrol, this.newSubSubQuestionExpectedTestsList);
}

addSubQuestionsExpectedTest(i, j) {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].questions[j].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(`questions.${i}.questions.${j}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(`questions.${i}.questions.${j}`).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addSubSubSectionsNewExpTest(i, j, k) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].questions[j].sections[k].newExpectTest, this.expcontrol, this.newSubSubSectionExpectedTestsList);
}

addSubSubSectionsExpectedTest(i, j, k) {
    this.isExpTestExist = false;
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].questions[j].sections[k].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.expectedTests`) as FormArray;
    if (selectedExpectedTest !== '' && selectedExpectedTest !== 'add') {
        if (this.expcontrol !== null && (this.expcontrol).length > 0) {
            this.expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            this.expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}`).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== '' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}`).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

addPointExpectedTest(i, m) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.sections[i].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `sections.${i}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addPointNewExpTest(i, m) {
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${i}.questions.${m}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`sections.${i}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.sections[i].questions[m].newExpectTest, this.expcontrol, this.newPointExpectedTestsList);
}

addSubPointExpectedTest(i, j, m) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].sections[j].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.sections.${j}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubPointNewExpTest(i, j, m) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}.sections.${j}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].sections[j].questions[m].newExpectTest,
            this.expcontrol, this.newSubPointExpectedTestsList);
}

addSubSubPointExpectedTest(i, j, k, m) {
    const selectedExpectedTest = this.saqEditQuestionForm.value.questions[i].questions[j].sections[k].questions[m].selectedExpectedTest;
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}.expectedTests`) as FormArray;
    const cntlstr = `questions.${i}.questions.${j}.sections.${k}.questions.${m}`;
    this.addExpectedTestCommonFunction(this.expcontrol, selectedExpectedTest, cntlstr);
}

addSubSubPointNewExpTest(i, j, k, m) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}.expectedTests`) as FormArray;
    this.saqEditQuestionForm.get(`questions.${i}.questions.${j}.sections.${k}.questions.${m}`).patchValue({
        addBtnHide: false,
        newExpectedBox: false
    });
    this.isExpectedTestingExisted(this.saqEditQuestionForm.value.questions[i].questions[j].sections[k].questions[m].newExpectTest,
            this.expcontrol, this.newSubSubPointExpectedTestsList);
}

addExpectedTestCommonFunction(expcontrol, selectedExpectedTest, cntlstr) {
    this.isExpTestExist = false;
    if (selectedExpectedTest !== 'null' && selectedExpectedTest !== 'add') {
        if (expcontrol !== null && (expcontrol).length > 0) {
            expcontrol.controls.map((x) => {
                if ( x.value.id === selectedExpectedTest.id) {
                    this.isExpTestExist = true;
                    this._toastr.warning('Expected has been already added', 'Warning');
                }
            });
        }
        if (!this.isExpTestExist) {
            expcontrol.push(this._fb.group(selectedExpectedTest));
        }
        this.saqEditQuestionForm.get(cntlstr).patchValue({
            addBtnHide: false,
            newExpectedBox: false
        });
    } else if (selectedExpectedTest !== 'null' && selectedExpectedTest === 'add') {
        this.saqEditQuestionForm.get(cntlstr).patchValue({
            newExpectTest: null,
            addBtnHide: true,
            newExpectedBox: true
        });
    }
}

openConfirmationModalToRemoveExpectedTest(control, index) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete a expected test?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            control.removeAt(index);
        } else if (result === false) {
            return false;
        }
    });
}

deleteExpectedTestRow(etIndex: number) {
    this.expcontrol = <FormArray>this.saqEditQuestionForm.controls['expectedTests'];
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubExpectedTestRow(indexi: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${indexi}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubQuestionExpectedTestRow(indexi: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubExpectedTestRow(indexi: number, indexj: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubQuestionExpectedTestRow(indexi: number, indexj: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubSubQuestionExpectedTestRow(indexi: number, indexj: number, indexk: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deletePointExpectedTestRow(indexi: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`sections.${indexi}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubPointExpectedTestRow(indexi: number, indexj: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.sections.${indexj}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteSubSubPointExpectedTestRow(indexi: number, indexj: number, indexk: number, indexm: number, etIndex: number) {
    this.expcontrol = this.saqEditQuestionForm.get(`questions.${indexi}.questions.${indexj}.sections.${indexk}.questions.${indexm}.expectedTests`) as FormArray;
    this.openConfirmationModalToRemoveExpectedTest(this.expcontrol, etIndex);
}

deleteFromExpectedTestList(expTestId, expectedTestsList) {
    if ((expectedTestsList).length > 0) {
        expectedTestsList.forEach((expTest) => {
            if (expTest === expTestId) {
                this._expectedTestService.delete(expTestId).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'expectedTestListModification',
                        content: 'Deleted an expectedTest'
                    });
                });
            }
        })
    }
    this.getExpectedTestsList();
}

private onSaveErrorExpectedTest(error) {
    try {
        error.json();
    } catch (exception) {
        error.message = error.text();
    }
    this.onError(error);
}

private onError(error) {
    this._alertService.error(error.message, null, null);
}

cancel() {
    this.ngOnChanges(null);
    this.questionId = ''
}

save() {
    if (this.saqEditQuestionForm && this.saqEditQuestionForm.dirty) {
        if (this.saqEditQuestionForm.value.questions || this.saqEditQuestionForm.value.sections) {
            this.saqEditQuestionForm.value.subQuestions = this.saqEditQuestionForm.value.sections.concat(this.saqEditQuestionForm.value.questions);
            this.saqEditQuestionForm.value.sections.map((x) => {
                if (x.questions) {
                    x.subQuestions = x.questions;
                }
            });
            this.saqEditQuestionForm.value.questions.map((x) => {
                if (x.questions || x.sections) {
                    x.questions.map((y) => {
                        if (y.sections) {
                            y.sections.map((z) => {
                                z.subQuestions = z.questions;
                            })
                        }
                        y.subQuestions = y.sections;
                    });
                    x.sections.map((y) => {
                        y.subQuestions = y.questions;
                    });
                    x.subQuestions = x.sections.concat(x.questions);
                }
            });
        }
        this.saqQuestion.id = this.saqEditQuestionForm.value.id;
        this.saqQuestion.questionNumber = this.saqEditQuestionForm.value.questionNumber;
        this.saqQuestion.responseRequired = this.saqEditQuestionForm.value.responseRequired;
        this.saqQuestion.part = this.saqEditQuestionForm.value.part;
        this.saqQuestion.questionText = this.saqEditQuestionForm.value.questionText;
        this.saqQuestion.expectedTests = this.saqEditQuestionForm.value.expectedTests;
        this.saqQuestion.subQuestions = this.saqEditQuestionForm.value.subQuestions;
        this.saqQuestion.pcidssRequirementId = this.saqEditQuestionForm.value.pcidssRequirementId;
        if (this.saqQuestion.id !== undefined) {
            this.subscribeToSaveResponse(
                    this._saqQuestionService.update(this.saqQuestion));
        }
    } else {
        this._toastr.warning(`There is no change in the question to be updated`, `Warning`);
    }
}

private subscribeToSaveResponse(result: Observable<SaqQuestion>) {
    result.subscribe((res: SaqQuestion) =>
    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
}

private onSaveSuccess(result: SaqQuestion) {
    this.eventManager.broadcast({ name: 'SaqQuestionListModification', content: 'OK'});
    this._toastr.success(`The Question ${result.questionNumber} has been successfully updated`, `Update`);
    this.questionId = '';
    this.requirementId = '';
}

private onSaveError(error) {
    try {
        const err = error.json();
        this._toastr.error(`${err.description}`, `Error`);
    } catch (exception) {
        error.message = error.text();
    }
    this.onError(error);
}

openConfirmationModalToDeleteQuestion(question) {
    const modal = this._bsModalService.show(YesNoModalComponent);
    modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete question number ' + question.questionNumber + '?'
    );

    modal.content.onClose.subscribe((result) => {
        if (result === true) {
            this._saqQuestionService.delete(question.id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'saqQuestionListModification',
                    content: 'Deleted an saqQuestion'
                });
                this._toastr.success(`The question ${question.questionNumber} has successfully deleted`, `Delete`);
            });
            this.cancel();
            this.requirementId = '';
        } else if (result === false) {
            return false;
        }
    });
}

deleteQuestion(question) {
    this.openConfirmationModalToDeleteQuestion(question);
}
}
