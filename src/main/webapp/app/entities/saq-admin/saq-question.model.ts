import { BaseEntity } from './../../shared';

export class SaqQuestion implements BaseEntity {
    constructor(
        public id?: number,
        public questionNumber?: string,
        public questionText?: string,
        public pcidssRequirementId?: BaseEntity,
        public responseRequired?: boolean,
        public part?: boolean,
        public parentId?: SaqQuestion,
        public subQuestions?: SaqQuestion[],
        public expectedTests?: BaseEntity[]
    ) {
    }
}

export class MerchantBusinessType implements BaseEntity {
    constructor(
        public id?: number,
        public businessType?: string
    ) {
    }
}

export class PaymentChannel implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string
    ) {
    }
}

export class ServiceCategory implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public serviceTypes?: ServiceType[],
        public isCategoryExist?: boolean
    ) {
    }
}

export class ServiceType implements BaseEntity {
    constructor(
        public id?: number,
        public serviceType?: string,
        public serviceCategoryId?: BaseEntity
    ) {
    }
}
