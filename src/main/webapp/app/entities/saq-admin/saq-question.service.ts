import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { SaqQuestion } from './saq-question.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { BaseEntity } from './../../shared';

@Injectable()
export class SaqQuestionService {

    private resourceUrlMerchant = 'api';
    private resourceUrl = 'api/merchant/getQuestionById';

    constructor(private http: Http) { }

    create(saqQuestion: SaqQuestion): Observable<SaqQuestion> {
        const copy = this.convert(saqQuestion);
        return this.http.post(this.resourceUrlMerchant + '/saq-questions', copy).map((res: Response) => {
            return res.json();
        });
    }

    update(saqQuestion: SaqQuestion): Observable<SaqQuestion> {
        const copy = this.convert(saqQuestion);
        return this.http.put(this.resourceUrlMerchant + '/saq-questions', copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<SaqQuestion> {
        return this.http.get(`${this.resourceUrlMerchant}/saq-questions/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findSaqQuestionByQuestionNumber(questionNumber: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrlMerchant}/saq-questions/by-question-number?questionNumber=${questionNumber}`)
        .map((res: Response) => this.convertResponse(res));
    }

    findSaqLQuestionByRequirement(pcidssRequirementId: BaseEntity, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlMerchant}/saq-questions/questionByPcidssRequirementId/${pcidssRequirementId}`, options)
        .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrlMerchant}/saq-questions`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlMerchant}/saq-questions/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(saqQuestion: SaqQuestion): SaqQuestion {
        const copy: SaqQuestion = Object.assign({}, saqQuestion);
        return copy;
    }
}
