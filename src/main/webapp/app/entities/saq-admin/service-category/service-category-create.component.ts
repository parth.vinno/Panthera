import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ServiceCategory, ServiceType } from '../saq-question.model';
import { ServiceCategoryService } from './service-category.service';
import { ServiceTypeService } from '../service-type/service-type.service';
import { Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { CustomValidators } from './validators/custom-validators';

@Component({
    selector: 'jhi-service-category-create',
    templateUrl: './service-category-create.component.html',
    styleUrls: [
                'service-category.component.scss'
            ]
})

export class ServiceCategoryCreateComponent implements OnInit {

    serviceCategory: ServiceCategory;
    serviceCategoryForm: FormGroup;
    authorities: any[];
    isSaving: boolean;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _fb: FormBuilder,
        private alertService: JhiAlertService,
        private _bsModalService: BsModalService,
        private _serviceCategoryService: ServiceCategoryService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.serviceCategoryForm = this._fb.group({
            id: null,
            name: null,
            serviceTypes: this._fb.array([this.initServiceTypesRows()])
        });
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    previousState() {
        this.router.navigate(['/saq/admin/service-category']);
    }

    get serviceTypes(): FormArray {
        return this.serviceCategoryForm.get('serviceTypes') as FormArray;
    };

    initServiceTypesRows() {
        return this._fb.group(new ServiceType());
    }

    deleteServiceTypes(index) {
        this.openConfirmationModalToDelete(index);
    }

    addServiceTypes() {
        const control = <FormArray>this.serviceCategoryForm.controls['serviceTypes'];
        control.push(this.initServiceTypesRows());
    }

    save(category) {
        let serviceType = [];
        let isTypeExist = true;
        const isDuplicateServiceCategory = (category.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        let isNameExist = true;
        this._serviceCategoryService.query().subscribe(
            (res: ResponseWrapper) => {
                const categoryList = res.json;
                for (let i = 0; i < categoryList.length; i++) {
                    const modifiedName  = (categoryList[i].name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
                    if (modifiedName === isDuplicateServiceCategory) {
                        this._toastr.error(`The service category aleady exists`, `Error`);
                        isNameExist = false;
                        break;
                    }
                }
                if (isNameExist) {
                    this.serviceCategory = category;
                    serviceType = category.serviceTypes;
                    const isTypeDuplicate = this.checkForDuplicate(serviceType);
                    if (isTypeDuplicate) {
                        this._toastr.warning(`Please remove duplicate entry`, `Warning`);
                    } else {
                        for (let z = 0; z < categoryList.length; z++) {
                            for (let j = 0; j < serviceType.length; j++) {
                                for ( let k = 0; k < (categoryList[z].serviceTypes).length; k++) {
                                    if (serviceType[j].serviceType === categoryList[z].serviceTypes[k].serviceType) {
                                        isTypeExist = false;
                                        this._toastr.error(`The service type ${serviceType[j].serviceType} aleady exists, Please Remove`, `Error`);
                                    }
                                }
                            }
                        }
                        if (isTypeExist) {
                            this.subscribeToSaveResponse(
                                    this._serviceCategoryService.create(category));
                        }
                    }
                }
                },
            (res: ResponseWrapper) => {
                this.onError(res.json)}
        );
    }

    checkForDuplicate(reqArr) {
        for (let i = 0; i < reqArr.length; i++) {
            for (let j = i; j < reqArr.length; j++) {
                if (i !== j && (reqArr[i].serviceType === reqArr[j].serviceType)) {
                    return true;
                }
            }
        }
        return false;
    }

    private subscribeToSaveResponse(result: Observable<ServiceCategory>) {
        result.subscribe((res: ServiceCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ServiceCategory) {
        this._toastr.success(`The service category has been successfully Saved`, `Save`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this._toastr.error(`${error.message}`, `Error`);
        this.alertService.error(error.message, null, null);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDelete(index) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'Are you sure you want to delete the service type?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                const control = <FormArray>this.serviceCategoryForm.controls['serviceTypes'];
                control.removeAt(index);
                this._toastr.success('The service type has been removed', 'Remove');
            } else if (result === false) {
                return false;
            }
        });
    }
}
