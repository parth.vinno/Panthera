import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ServiceCategory } from '../saq-question.model';
import { ServiceCategoryService } from './service-category.service';

@Component({
    selector: 'jhi-service-category-detail',
    templateUrl: './service-category-detail.component.html',
    styleUrls: [
                'service-category.component.scss'
            ]
})
export class ServiceCategoryDetailComponent implements OnInit, OnDestroy {

    serviceCategory: ServiceCategory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private router: Router,
        private eventManager: JhiEventManager,
        private serviceCategoryService: ServiceCategoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInServiceCategories();
    }

    load(id) {
        this.serviceCategoryService.find(id).subscribe((serviceCategory) => {
            this.serviceCategory = serviceCategory;
        });
    }
    previousState() {
        this.router.navigate(['/saq/admin/service-category']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInServiceCategories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'serviceCategoryListModification',
            (response) => this.load(this.serviceCategory.id)
        );
    }
}
