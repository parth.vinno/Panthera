import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormsModule, ReactiveFormsModule, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ServiceCategory, ServiceType } from '../saq-question.model';
import { ServiceCategoryService } from './service-category.service';
import { ServiceTypeService } from '../service-type/service-type.service';
import { Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { CustomValidators } from './validators/custom-validators';

@Component({
    selector: 'jhi-service-category-edit',
    templateUrl: './service-category-edit.component.html',
    styleUrls: [
                'service-category.component.scss'
            ]
})

export class ServiceCategoryEditComponent implements OnInit {

    serviceCategory: ServiceCategory;
    serviceCategoryEditForm: FormGroup;
    authorities: any[];
    isSaving: boolean;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _fb: FormBuilder,
        private alertService: JhiAlertService,
        private _bsModalService: BsModalService,
        private _serviceCategoryService: ServiceCategoryService,
        private eventManager: JhiEventManager,
        private _toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.serviceCategoryEditForm = this._fb.group({
            id: null,
            name: null,
            serviceTypes: this._fb.array([])
        });
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this._serviceCategoryService.find(params['id']).subscribe((category) => {
                    const control = <FormArray>this.serviceCategoryEditForm.controls['serviceTypes'];
                    this.serviceCategoryEditForm.patchValue({
                        id: category.id,
                        name: category.name
                    })
                    category.serviceTypes.map((types) => {
                        control.push(this._fb.group({
                            id: types.id,
                            serviceType: types.serviceType
                        }));
                    });
                });
            }
        });
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    previousState() {
        this.router.navigate(['/saq/admin/service-category']);
    }

    get serviceTypes(): FormArray {
        return this.serviceCategoryEditForm.get('serviceTypes') as FormArray;
    };

    initServiceTypesRows() {
        return this._fb.group(new ServiceType());
    }

    deleteServiceTypes(index) {
        this.openConfirmationModalToDelete(index);
    }

    addServiceTypes() {
        const control = <FormArray>this.serviceCategoryEditForm.controls['serviceTypes'];
        control.push(this.initServiceTypesRows());
    }

    save(category) {
        let serviceType = [];
        let isTypeExist = true;
        const isDuplicateServiceCategory = (category.name).replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        this._serviceCategoryService.query().subscribe(
            (res: ResponseWrapper) => {
                const categoryList = res.json;
                    this.serviceCategory = category;
                    serviceType = category.serviceTypes;
                    const isTypeDuplicate = this.checkForDuplicate(serviceType);
                    if (isTypeDuplicate) {
                        this._toastr.warning(`Please remove duplicate entry`, `Warning`);
                    } else {
                        for (let z = 0; z < categoryList.length; z++) {
                            for (let j = 0; j < serviceType.length; j++) {
                                for ( let k = 0; k < (categoryList[z].serviceTypes).length; k++) {
                                    if (serviceType[j].serviceType === categoryList[z].serviceTypes[k].serviceType) {
                                        isTypeExist = false;
                                        this._toastr.error(`The service type ${serviceType[j].serviceType} aleady exists, Please Remove`, `Error`);
                                    }
                                }
                            }
                        }
                        if (isTypeExist) {
                            if (category.id !== undefined) {
                            this.subscribeToSaveResponse(
                                    this._serviceCategoryService.update(category));
                            }
                        }
                    }
                },
            (res: ResponseWrapper) => {
                this.onError(res.json)}
        );
    }

    checkForDuplicate(reqArr) {
        for (let i = 0; i < reqArr.length; i++) {
            for (let j = i; j < reqArr.length; j++) {
                if (i !== j && (reqArr[i].serviceType === reqArr[j].serviceType)) {
                    return true;
                }
            }
        }
        return false;
    }

    private subscribeToSaveResponse(result: Observable<ServiceCategory>) {
        result.subscribe((res: ServiceCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ServiceCategory) {
        this._toastr.success(`The service category has been successfully Saved`, `Save`);
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this._toastr.error(`${error.message}`, `Error`);
        this.alertService.error(error.message, null, null);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDelete(index) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
                'Confirm',
                'Are you sure you want to delete the service type?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                const control = <FormArray>this.serviceCategoryEditForm.controls['serviceTypes'];
                control.removeAt(index);
                this._toastr.success('The service type has been removed', 'Remove');
            } else if (result === false) {
                return false;
            }
        });
    }
}
