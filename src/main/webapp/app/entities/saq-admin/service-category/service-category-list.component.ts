import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Rx';
import { BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService, JhiParseLinks, JhiPaginationUtil, JhiLanguageService } from 'ng-jhipster';
import { Pipe, PipeTransform } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { ServiceCategory, ServiceType } from '../saq-question.model';
import { ServiceCategoryService } from './service-category.service';
import { ServiceTypeService } from '../service-type/service-type.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, YesNoModalComponent } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

@Pipe({
    name: 'filterServiceCategory'
})
export class FilterServiceCategory implements PipeTransform {
    transform(items: any[], criteria: any): any {
        return items.filter((item) => {
           for (const key in item ) {
             if ( ('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
             }
           }
           return false;
        });
    }
}

@Component({
    selector: 'jhi-service-category-list',
    templateUrl: './service-category-list.component.html',
    styleUrls: [
                'service-category.component.scss'
            ]
})
export class ServiceCategoryListComponent implements OnInit, OnDestroy {

    currentAccount: any;
    serviceCategories: ServiceCategory[];
    serviceCategoryCopy: ServiceCategory[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private serviceCategoryService: ServiceCategoryService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private _bsModalService: BsModalService,
        private _toastr: ToastrService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() {
        this.serviceCategoryService.queryPageable({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.links = this.parseLinks.parse(res.headers.get('link'));
                this.totalItems = res.headers.get('X-Total-Count');
                this.queryCount = this.totalItems;
                this.serviceCategories = res.json;
                this.serviceCategories.map((category) => {
                    if ((category.serviceTypes).length > 0) {
                        category.isCategoryExist = true;
                    } else {
                        category.isCategoryExist = false;
                    }
                });
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/saq/admin/service-category'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInServiceCategories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ServiceCategory) {
        return item.id;
    }

    registerChangeInServiceCategories() {
        this.eventSubscriber = this.eventManager.subscribe('ServiceCategoryListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    openConfirmationModalToDeleteCategory(id) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm Delete operation',
            'Are you sure you want to delete service category?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                this.serviceCategoryService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'ServiceCategoryListModification',
                        content: 'Service Category Deleted'
                    });
                });
                this.router.navigate(['/saq/admin/service-category']);
                this._toastr.success(`The service category has been successfully Deleted`, `Delete`);
            } else if (result === false) {
                return false;
            }
        });
    }

    deleteExpectedTest(id) {
        this.openConfirmationModalToDeleteCategory(id);
    }
}
