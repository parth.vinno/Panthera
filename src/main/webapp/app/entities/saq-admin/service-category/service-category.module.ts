import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PcidssSharedModule } from '../../../shared';
import {
    ServiceCategoryService,
    ServiceCategoryCreateComponent,
    ServiceCategoryDetailComponent,
    ServiceCategoryEditComponent,
    ServiceCategoryListComponent,
    FilterServiceCategory
} from './';
import { DisplayCategoryErrorsComponent } from '../../form-validation-error-display/display-service-category-error.component';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ServiceCategoryCreateComponent,
        ServiceCategoryListComponent,
        ServiceCategoryDetailComponent,
        ServiceCategoryEditComponent,
        FilterServiceCategory,
        DisplayCategoryErrorsComponent
    ],
    entryComponents: [
        ServiceCategoryListComponent,
        ServiceCategoryCreateComponent,
        ServiceCategoryDetailComponent,
        ServiceCategoryEditComponent,
    ],
    providers: [
        ServiceCategoryService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssServiceCategoryModule {}
