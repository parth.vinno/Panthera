import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ServiceCategory } from '../saq-question.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class ServiceCategoryService {

    private resourceUrl = 'api/service-categories/service-provider';
    private resourceUrlPageable = 'api/service-categories-pageable/service-provider';

    constructor(private http: Http) { }

    create(serviceCategory: ServiceCategory): Observable<ServiceCategory> {
        const copy = this.convert(serviceCategory);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(serviceCategory: ServiceCategory): Observable<ServiceCategory> {
        const copy = this.convert(serviceCategory);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ServiceCategory> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPageable(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlPageable, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(serviceCategory: ServiceCategory): ServiceCategory {
        const copy: ServiceCategory = Object.assign({}, serviceCategory);
        return copy;
    }
}
