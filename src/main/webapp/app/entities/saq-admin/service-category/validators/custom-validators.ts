import { FormArray, FormControl, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
    static serviceType(c: FormControl): ValidationErrors {
        const isValidNewType = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'serviceType': {
                    'message': 'Enter valid service type'
                }
        };
        return isValidNewType ? null : message;
    }
}
