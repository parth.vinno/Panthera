import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ServiceType } from '../saq-question.model';
import { ResponseWrapper, createRequestOption } from '../../../shared';

@Injectable()
export class ServiceTypeService {

    private resourceUrl = 'api/service-types/service-provider';

    constructor(private http: Http) { }

    create(serviceType: ServiceType): Observable<ServiceType> {
        const copy = this.convert(serviceType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(serviceType: ServiceType): Observable<ServiceType> {
        const copy = this.convert(serviceType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ServiceType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(serviceType: ServiceType): ServiceType {
        const copy: ServiceType = Object.assign({}, serviceType);
        return copy;
    }
}
