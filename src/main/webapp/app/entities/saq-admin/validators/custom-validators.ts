import { FormArray, FormControl, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
    static questionNumber(c: FormControl): ValidationErrors {
        const isValidQuestionNumber = /^[A-B]?\d+(\.?\d*?){0,1}$/g.test(c.value);
        const message = {
                'questionNumber': {
                    'message': 'Enter valid question number. e.g.:1 or 1.1 or A1'
                }
        };
        return isValidQuestionNumber ? null : message;
    }

    static newExpectTest(c: FormControl): ValidationErrors {
        const isValidNewExpectTest = /^[a-zA-Z0-9,.:!'\s()-?]*$/.test(c.value);
        const message = {
                'newExpectTest': {
                    'message': 'Enter valid expected test'
                }
        };
        return isValidNewExpectTest ? null : message;
    }
}
