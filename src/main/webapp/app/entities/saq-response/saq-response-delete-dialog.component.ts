import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SaqResponse } from './saq-response.model';
import { SaqResponsePopupService } from './saq-response-popup.service';
import { SaqResponseService } from './saq-response.service';

@Component({
    selector: 'jhi-saq-response-delete-dialog',
    templateUrl: './saq-response-delete-dialog.component.html'
})
export class SaqResponseDeleteDialogComponent {

    saqResponse: SaqResponse;

    constructor(
        private saqResponseService: SaqResponseService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.saqResponseService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'saqResponseListModification',
                content: 'Deleted an saqResponse'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-saq-response-delete-popup',
    template: ''
})
export class SaqResponseDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private saqResponsePopupService: SaqResponsePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.saqResponsePopupService
                .open(SaqResponseDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
