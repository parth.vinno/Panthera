import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { SaqResponse } from './saq-response.model';
import { SaqResponseService } from './saq-response.service';

@Component({
    selector: 'jhi-saq-response-detail',
    templateUrl: './saq-response-detail.component.html'
})
export class SaqResponseDetailComponent implements OnInit, OnDestroy {

    saqResponse: SaqResponse;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private saqResponseService: SaqResponseService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSaqResponses();
    }

    load(id) {
        this.saqResponseService.find(id).subscribe((saqResponse) => {
            this.saqResponse = saqResponse;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSaqResponses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'saqResponseListModification',
            (response) => this.load(this.saqResponse.id)
        );
    }
}
