import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SaqResponse } from './saq-response.model';
import { SaqResponsePopupService } from './saq-response-popup.service';
import { SaqResponseService } from './saq-response.service';
import { SaqQuestion } from '../saq-admin/saq-question.model';
import { SaqQuestionService } from '../saq-admin/saq-question.service';
import { RequirementWorkStatus, RequirementWorkStatusService } from '../requirement-work-status';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-saq-response-dialog',
    templateUrl: './saq-response-dialog.component.html'
})
export class SaqResponseDialogComponent implements OnInit {

    saqResponse: SaqResponse;
    authorities: any[];
    isSaving: boolean;

    saqQuestion: SaqQuestion[];

    requirementworkstatuses: RequirementWorkStatus[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private saqResponseService: SaqResponseService,
        private saqQuestionService: SaqQuestionService,
        private requirementWorkStatusService: RequirementWorkStatusService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.saqQuestionService.query()
            .subscribe((res: ResponseWrapper) => { this.saqQuestion = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.requirementWorkStatusService.query()
            .subscribe((res: ResponseWrapper) => { this.requirementworkstatuses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.saqResponse.id !== undefined) {
            this.subscribeToSaveResponse(
                this.saqResponseService.update(this.saqResponse));
        } else {
            this.subscribeToSaveResponse(
                this.saqResponseService.create(this.saqResponse));
        }
    }

    private subscribeToSaveResponse(result: Observable<SaqResponse>) {
        result.subscribe((res: SaqResponse) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SaqResponse) {
        this.eventManager.broadcast({ name: 'saqResponseListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSaqQuestionId(index: number, item: SaqQuestion) {
        return item.id;
    }

    trackRequirementWorkStatusById(index: number, item: RequirementWorkStatus) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-saq-response-popup',
    template: ''
})
export class SaqResponsePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private saqResponsePopupService: SaqResponsePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.saqResponsePopupService
                    .open(SaqResponseDialogComponent, params['id']);
            } else {
                this.modalRef = this.saqResponsePopupService
                    .open(SaqResponseDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
