import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SaqResponse } from './saq-response.model';
import { SaqResponseService } from './saq-response.service';

@Injectable()
export class SaqResponsePopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private saqResponseService: SaqResponseService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.saqResponseService.find(id).subscribe((saqResponse) => {
                this.saqResponseModalRef(component, saqResponse);
            });
        } else {
            return this.saqResponseModalRef(component, new SaqResponse());
        }
    }

    saqResponseModalRef(component: Component, saqResponse: SaqResponse): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.saqResponse = saqResponse;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
