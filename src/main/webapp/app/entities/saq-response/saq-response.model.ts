import { BaseEntity } from './../../shared';

export class SaqResponse implements BaseEntity {
    constructor(
        public id?: number,
        public responseInput?: string,
        public appendixType?: string,
        public appendixData?: string,
        public saqQuestionId?: BaseEntity,
        public requirementWorkStatusId?: BaseEntity,
    ) {
    }
}
