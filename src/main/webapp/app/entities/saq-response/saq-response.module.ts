import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import {
    SaqResponseService,
    SaqResponsePopupService,
    SaqResponseComponent,
    SaqResponseDetailComponent,
    SaqResponseDialogComponent,
    SaqResponsePopupComponent,
    SaqResponseDeletePopupComponent,
    SaqResponseDeleteDialogComponent,
    saqResponseRoute,
    saqResponsePopupRoute,
    SaqResponseResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...saqResponseRoute,
    ...saqResponsePopupRoute,
];

@NgModule({
    imports: [
        PcidssSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SaqResponseComponent,
        SaqResponseDetailComponent,
        SaqResponseDialogComponent,
        SaqResponseDeleteDialogComponent,
        SaqResponsePopupComponent,
        SaqResponseDeletePopupComponent,
    ],
    entryComponents: [
        SaqResponseComponent,
        SaqResponseDialogComponent,
        SaqResponsePopupComponent,
        SaqResponseDeleteDialogComponent,
        SaqResponseDeletePopupComponent,
    ],
    providers: [
        SaqResponseService,
        SaqResponsePopupService,
        SaqResponseResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssSaqResponseModule {}
