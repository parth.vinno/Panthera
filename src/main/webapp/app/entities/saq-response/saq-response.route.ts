import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SaqResponseComponent } from './saq-response.component';
import { SaqResponseDetailComponent } from './saq-response-detail.component';
import { SaqResponsePopupComponent } from './saq-response-dialog.component';
import { SaqResponseDeletePopupComponent } from './saq-response-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class SaqResponseResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const saqResponseRoute: Routes = [
    {
        path: 'saq-response',
        component: SaqResponseComponent,
        resolve: {
            'pagingParams': SaqResponseResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.saqResponse.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'saq-response/:id',
        component: SaqResponseDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.saqResponse.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const saqResponsePopupRoute: Routes = [
    {
        path: 'saq-response-new',
        component: SaqResponsePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.saqResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saq-response/:id/edit',
        component: SaqResponsePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.saqResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saq-response/:id/delete',
        component: SaqResponseDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pcidssApp.saqResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
