import { Component } from '@angular/core';

@Component({
  selector: 'jhi-assessment-home',
  templateUrl: './assessment-home.component.html',
  styleUrls: [ './assessment-home.component.scss' ]
})
export class AssessmentHomeComponent {
  constructor() { }
}
