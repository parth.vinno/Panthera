import {Component, OnInit} from '@angular/core';
import {SelfAssessmentService, ReviewResponse, QUESTION_STATUS} from '../../../shared';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'jhi-correction',
    templateUrl: './correction.component.html',
    styleUrls: ['./correction.component.scss', '../responsive.scss']
})
export class CorrectionComponent implements OnInit {
    reviewResponse: ReviewResponse[];
    isStatusDone;
    STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private route: ActivatedRoute) {
        this.STATUS = QUESTION_STATUS;
        this.route.queryParams.subscribe((params) => {
            this.isStatusDone = (params['status'] === 'complete');
        });
    }

    ngOnInit() {
        this.selfAssessmentService.getSaqReviewedQuestions(this.STATUS.REVIEWED).subscribe(
            (response) => {
                this.reviewResponse = response;
            }
        );
    }

    routeToNotification() {
        this.router.navigate(['../notification'], {queryParams: {lastPage: 'summary'}, relativeTo: this.route});
    }

    goToQuestion(review: ReviewResponse) {
        this.router.navigate(['../requirement-category' , review.saqQuestion.pcidssRequirementId.requirementCategoryId.id
            , 'requirement'
            , review.saqQuestion.pcidssRequirementId.id, 'question' , review.saqQuestion.id], {relativeTo: this.route});
    }

    routeToHome() {
        this.router.navigate(['./saq/assessment-home']);
    }
}
