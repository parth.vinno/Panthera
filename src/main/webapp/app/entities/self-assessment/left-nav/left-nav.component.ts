import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SelfAssessmentService, EventModal, GlobalEventEmitter} from '../../../shared';

@Component({
    selector: 'jhi-left-nav',
    templateUrl: './left-nav.component.html',
    styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit, OnDestroy {
    categories;
    isCategoryLoaded;
    submitSaqQuestionSubscription;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private eventEmitter: GlobalEventEmitter) {
        this.submitSaqQuestionSubscription = this.eventEmitter.submitSaqQuestion.subscribe(
            (eventModal: EventModal) => {
                if (eventModal.data === false) {
                    this.loadCategories().then(() => {
                        console.log('Categories refreshed');
                    });
                }
            });
    }

    ngOnInit() {
        this.isCategoryLoaded = this.router.url.includes('requirement-category/');
        this.loadCategories().then(() => {
            if (this.categories.length) {
                if (!this.isCategoryLoaded) {
                    if (this.categories && this.categories.length) {
                        this.goToCategory(this.categories[0].id);
                    }
                }
            }
        }, (error) => {
            console.log(error);
        })
    }

    loadCategories() {
        return new Promise((resolve, reject) => {
            this.selfAssessmentService.getAllAvailableRequirementsCategories().subscribe(
                (response) => {
                    this.categories = response;
                    resolve();
                },
                (error) => {
                    reject(error);
                });
        });
    }

    goToCategory(id) {
        this.router.navigate(['saq/assessment/requirement-category', id]);
    }

    ngOnDestroy() {
        if (this.submitSaqQuestionSubscription) {
            this.submitSaqQuestionSubscription.unsubscribe();
        }
    }
}
