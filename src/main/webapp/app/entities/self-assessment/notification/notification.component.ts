import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {SelfAssessmentService, Notification, NOTIFICATION_STATUS, ROLE} from '../../../shared';

@Component({
  selector: 'jhi-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})

export class NotificationComponent implements OnInit, OnDestroy {
    selectedQA: Notification;
    notifications: Notification[];
    isPendingPage: boolean;
    isCorrection: boolean;
    selectedOption: NOTIFICATION_STATUS;
    routerSubscription;
    STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private route: ActivatedRoute, private _ngZone: NgZone) {
        this.STATUS = NOTIFICATION_STATUS;
    }

  ngOnInit() {
      this._ngZone.run(() => {
          this.loadNotification();
          this.isCorrection = this.router.url.includes('correction');
          this.routerSubscription = this.route.queryParams.subscribe((params) => {
              this.isPendingPage = !(params['lastPage'] === 'summary');
          });
      });
  }

  loadNotification() {
      this.selfAssessmentService.getNotificationResource(ROLE.QA).subscribe(
          (res) => {
              this.notifications = res;
          }
      );
  }

  sendNotification() {
          this.selfAssessmentService.saveNotificationResponse(this.selectedQA).subscribe(
              (res) => {
                  this.routeToHome();
              });
  }

  routeToHome() {
        this.router.navigate(['./saq/assessment-home']);
  }

  reset() {
        this.isPendingPage = false;
  }

  ngOnDestroy() {
      this.routerSubscription.unsubscribe();
  }
}
