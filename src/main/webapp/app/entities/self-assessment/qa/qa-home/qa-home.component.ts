import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SelfAssessmentService, QAReviewDetails, REVIEW_STATUS} from '../../../../shared';

@Component({
    selector: 'jhi-qa-home',
    templateUrl: './qa-home.component.html',
    styleUrls: ['./qa-home.component.scss']
})
export class QAHomeComponent {
    reviewDetails: QAReviewDetails;
    STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private route: ActivatedRoute) {
        this.STATUS = REVIEW_STATUS;
        this.selfAssessmentService.getNotificationDetails().subscribe(
            (res) => {
                this.reviewDetails = res;
            }
        );
    }

    goToReviewView() {
        this.router.navigate(['all'], {relativeTo: this.route})
    }

    goToFixedView() {
        this.router.navigate(['fixed'], {relativeTo: this.route})
    }

    onDone() {
        this.router.navigate(['complete/notification'], {relativeTo: this.route});
    }

    resumeReview() {
        this.router.navigate(['all/requirement-category/' +
        this.reviewDetails.lastReviewedQuestion.pcidssRequirementId.requirementCategoryId.id + '/requirement/' +
        this.reviewDetails.lastReviewedQuestion.pcidssRequirementId.id + '/question/' +
        this.reviewDetails.lastReviewedQuestion.id], {relativeTo: this.route});
    }
}
