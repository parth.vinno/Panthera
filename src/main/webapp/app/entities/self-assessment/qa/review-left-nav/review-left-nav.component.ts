import {Component, OnInit} from '@angular/core';
import {SelfAssessmentService} from '../../../../shared';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'jhi-review-left-nav',
    templateUrl: './review-left-nav.component.html',
    styleUrls: ['./review-left-nav.component.scss']
})
export class ReviewLeftNavComponent implements OnInit {
    categories;
    isCategoryLoaded;
    isStatusDone: boolean;
    isFixed;
    height = 0;

    constructor(private selfAssessmentService: SelfAssessmentService, public router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.isCategoryLoaded = this.router.url.includes('requirement-category');
        this.isFixed = this.router.url.includes('fixed');
        this.isStatusDone = this.router.url.includes('complete');
        this.loadCategories(this.isFixed).then(() => {
            if (this.categories.length) {
                if (!this.isCategoryLoaded && !this.isStatusDone) {
                    if (this.categories && this.categories.length) {
                        this.goToCategory(this.categories[0].id);
                    }
                }
            } else {
                this.router.navigate(['./requirement-category/no-review'], {relativeTo: this.route});
            }
        }, (error) => {
            console.log(error);
        })
    }

    loadCategories(status?: boolean) {
        return new Promise((resolve, reject) => {
            this.selfAssessmentService.getAllRequirementCategories(status).subscribe(
                (response) => {
                    this.categories = response;
                    resolve();
                },
                (error) => {
                    reject(error);
                });
        });
    }

    goToCategory(id) {
        this.router.navigate(['./requirement-category', id], {relativeTo: this.route});
    }

}
