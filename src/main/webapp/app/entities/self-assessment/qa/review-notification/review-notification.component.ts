import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {SelfAssessmentService, QAReviewDetails, Notification, NOTIFICATION_STATUS, ROLE} from '../../../../shared';

@Component({
    selector: 'jhi-review-notification',
    templateUrl: './review-notification.component.html',
    styleUrls: ['./review-notification.component.scss']
})
export class ReviewNotificationComponent implements OnInit {
    isPendingPage: boolean;
    isStatusDone: boolean;
    reviewDetails: QAReviewDetails;
    selectedOption: NOTIFICATION_STATUS;
    selectedAOCOption: NOTIFICATION_STATUS;
    selectedQA: Notification;
    selectedQSA: Notification;
    selectedQSAOfficer: Notification;
    selectedISA: Notification;
    qsaNotification;
    qsaOfficerNotification;
    isaNotification;
    STATUS;
    AOC_STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private _ngZone: NgZone) {
        this.STATUS = NOTIFICATION_STATUS;
        this.AOC_STATUS = NOTIFICATION_STATUS;
        this.selectedQA = new Notification();
        this.selfAssessmentService.getNotificationDetails().subscribe(
            (res) => {
                this.reviewDetails = res;
            });
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this.isStatusDone = this.router.url.includes('complete');
            if (!this.isStatusDone) {
                this.loadNotification();
            }
        });
    }

    loadNotification() {
        Observable.forkJoin(this.selfAssessmentService.getNotificationResource(ROLE.QSA_AOC),
            this.selfAssessmentService.getNotificationResource(ROLE.QSA_OFFICER_AOC),
            this.selfAssessmentService.getNotificationResource(ROLE.ISA_AOC)).subscribe((results) => {
            const [qsaNotification, qsaOfficerNotification, isaNotification] = results;
            this.qsaNotification = qsaNotification;
            this.qsaOfficerNotification = qsaOfficerNotification;
            this.isaNotification = isaNotification;
        });
    }

    sendToQSA() {
        this.selfAssessmentService.sendQSANotification(this.selectedQA).subscribe(
            (res) => {
                this.routeToHome();
            });
    }

    sendToAOC() {
        this.selfAssessmentService.sendISANotification([this.selectedQSA, this.selectedQSAOfficer, this.selectedISA]).subscribe(
            (res) => {
                this.routeToHome();
            });
    }

    reset() {
        this.isPendingPage = false;
    }

    routeToHome() {
        this.router.navigate(['./saq/assessment/review']);
    }
}
