import {Component, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {NgForm} from '@angular/forms';

import {
    EventModal, GlobalEventEmitter, SelfAssessmentService, QUESTION_RESPONSE, SaqResponse, ReviewResponse,
    DIRECTION, YesNoModalComponent, PAGE, Comment, QUESTION_STATUS, STATE
} from '../../../../shared';

@Component({
    selector: 'jhi-review-question',
    templateUrl: './review-question.component.html',
    styleUrls: ['./review-question.component.scss', '../../fa-arrows.scss', './_ui-switch.scss']
})
export class ReviewQuestionComponent implements OnInit, OnDestroy {
    @ViewChild('reviewForm') public reviewForm: NgForm;

    saqResponse: SaqResponse;
    reviewResponse: ReviewResponse;
    curPage: PAGE;
    submitted: boolean;
    QUESTION_RESPONSE;
    PAGE = PAGE;
    curState: STATE;
    pageSubscription;
    routerSubscription;
    stateSubscription;
    showComment: boolean;
    questionId: number;
    comment: string;
    isFormChange: boolean;
    isFixed: boolean;
    STATUS;
    STATE = STATE;
    isQueRoman: boolean;
    disableSubmit: boolean;

    constructor(private _ngZone: NgZone, private selfAssessmentService: SelfAssessmentService,
                private route: ActivatedRoute, private eventEmitter: GlobalEventEmitter, private router: Router,
                private _bsModalService: BsModalService) {
        this.saqResponse = new SaqResponse();
        this.reviewResponse = new ReviewResponse();
        this.QUESTION_RESPONSE = QUESTION_RESPONSE;
        this.showComment = false;
        this.STATUS = QUESTION_STATUS;

        this.pageSubscription = this.eventEmitter.page.subscribe(
            (page) => {
                this.curPage = page;
            });

        this.stateSubscription = this.eventEmitter.state.subscribe(
            (state: STATE) => {
                this.curState = state;
            }
        );
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this.isFixed = this.router.url.includes('fixed');
            this.routerSubscription = this.route.params.subscribe((params) => {
                this.questionId = +params['questionId'];
                this.loadSaqResponse(this.questionId);
                this.loadReviewResponse(this.questionId);
            });
        });
    }

    loadReviewResponse(questionId: number) {
        this.selfAssessmentService.getReviewResponseById(questionId).subscribe(
            (response) => {
                this.reviewResponse = response;
            }, (error) => {
                if (error.status !== 404) {
                    console.error(error);
                }
            }
        )
    }

    loadSaqResponse(questionId: number) {
        this.selfAssessmentService.getRequirementQuestionsById(questionId).subscribe(
            (response) => {
                this.saqResponse = response;
                this.submitted = false;
                this.eventEmitter.questionLoaded.emit(new EventModal(questionId));
                this.isQueRoman = (/[ivxIVX]/).test(this.saqResponse.saqQuestion.questionNumber);
            });
    }

    saveResponse(status) {
        return new Promise((resolve, reject) => {
            const comment = new Comment();
            comment.comment = this.comment;

            if (this.reviewResponse.comments && comment.comment) {
                this.reviewResponse.comments.push(comment);
            } else if (!this.reviewResponse.comments && comment.comment) {
                this.reviewResponse.comments = [comment];
            }

            if (this.reviewResponse.comments || comment.comment) {
                this.selfAssessmentService.sendReviewResponse(this.reviewResponse, this.questionId, status).subscribe(
                    (response) => {
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                    });
            } else {
                resolve();
            }
        });
    }

    toggleSwitch() {
        this.showComment = !this.showComment;
    }

    deleteComment(index) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'Do you want to delete comment?'
        );
        modal.content.onClose.subscribe((result) => {
            if (result) {
                this.reviewResponse.comments.splice(index, 1);
                this.isFormChange = true;
            }
        });
    }

    saveAndContinue() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            this.saveResponse(this.STATUS.REVIEWED).then(() => {
                if ((this.curState === STATE.APPENDIX_WITH_LAST && (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE))) {
                    this.routeToSummary();
                } else if (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE) {
                    this.eventEmitter.submitSaqQuestion.emit(new EventModal(false));
                } else {
                    this.routeToNext();
                }
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        } else {
            this.disableSubmit = false;
        }
    }

    saveAndExit() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            this.saveResponse(this.STATUS.REVIEWED).then(() => {
                this.eventEmitter.submitSaqQuestion.emit(new EventModal(false));
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        } else {
            this.disableSubmit = false;
        }
    }

    saveAndPrev() {
        if (this.validateForm()) {
            this.saveResponse(this.STATUS.REVIEWED).then(() => {
                this.routeToPrev();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        }
    }

    onFixed() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            this.saveResponse(this.STATUS.FIXED).then(() => {
                if ((this.curState === STATE.APPENDIX_WITH_LAST && (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE))) {
                    this.routeToSummary();
                } else if (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE) {
                    this.eventEmitter.submitSaqQuestion.emit(new EventModal(false));
                } else {
                    this.routeToNext();
                }
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        } else {
            this.disableSubmit = false;
        }
    }

    routeToPrev() {
        this.reInit();
        this.eventEmitter.submitSaqQuestion.emit(new EventModal(DIRECTION.BACK));
    }

    routeToNext() {
        this.reInit();
        this.eventEmitter.submitSaqQuestion.emit(new EventModal(DIRECTION.FORWARD));
    }

    routeToSummary() {
        if (this.isFixed) {
            this.router.navigate(['./saq/assessment/review/fixed/summary']);
        } else {
            this.router.navigate(['./saq/assessment/review/all/summary']);

        }
    }

    back() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    private validateForm() {
        this.submitted = true;
        return (this.reviewForm && this.reviewForm.form.valid);
    }

    isFormDirty() {
        return ((this.reviewForm && this.reviewForm.form.dirty) || this.isFormChange);
    }

    private reInit() {
        this.submitted = false;
        this.showComment = false;
        this.reviewForm.reset();
        this.isFormChange = false;
        this.reviewResponse = new ReviewResponse();
        this.disableSubmit = false;
    }

    emptyComment() {
        this.comment = '';
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (!result) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    ngOnDestroy() {
        if (this.pageSubscription) {
            this.pageSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
