import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {SelfAssessmentService, Requirement, RequirementCategory, STATUS} from '../../../../shared';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'jhi-review-requirement-category',
    templateUrl: './review-requirement-category.component.html',
    styleUrls: ['./review-requirement-category.scss', '../../responsive.scss']
})
export class ReviewRequirementCategoryComponent implements OnInit, OnDestroy {
    categoryId: number;
    requirements: Requirement[];
    selectedCategory: RequirementCategory;
    STATUS;
    predicate;
    reverse;
    statusOrder;
    isFixed;
    isEmpty: boolean;
    routerSubscription;

    constructor(private selfAssessmentService: SelfAssessmentService, private route: ActivatedRoute,
                private router: Router, private _ngZone: NgZone, private store: Store<string>) {
        this.STATUS = STATUS;

        // Listed in Ascending order
        this.statusOrder = {REVISIT: 1, RESUME: 2, TAKE: 3};

        // Default values for pagination
        this.predicate = 'requirementNumber';
        this.reverse = false;
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this.routerSubscription = this.route.params.subscribe((params) => {
                this.categoryId = +params['categoryId'];
                if (params['categoryId'] === 'no-review') {
                    this.isEmpty = true;
                }
                this.isFixed = this.router.url.includes('fixed');
                this.loadRequirements(this.isFixed);
            });
        });
    }

    loadRequirements(status?: boolean) {
        Observable.forkJoin(this.selfAssessmentService.getRequirementCategory(this.categoryId),
            this.selfAssessmentService.getRequirementsByCategoryId(this.categoryId, status)).subscribe((results) => {
            const [selectedCategory, requirements] = results;
            this.selectedCategory = selectedCategory;
            this.requirements = requirements;
        });
    }

    transition() {
        this.requirements.sort((req1, req2) => {
            if (this.reverse) {
                if (isNaN(req1[this.predicate])) {
                    if (this.predicate === 'requirementStatus') {
                        return this.statusOrder[req1[this.predicate]] - this.statusOrder[req2[this.predicate]];
                    } else {
                        return req1[this.predicate].localeCompare(req2[this.predicate]);
                    }
                } else {
                    return req1[this.predicate] - req2[this.predicate];
                }
            } else {
                if (isNaN(req1[this.predicate])) {
                    if (this.predicate === 'requirementStatus') {
                        return this.statusOrder[req2[this.predicate]] - this.statusOrder[req1[this.predicate]];
                    } else {
                        return req2[this.predicate].localeCompare(req1[this.predicate]);
                    }
                } else {
                    return req2[this.predicate] - req1[this.predicate];
                }
            }
        });
    }

    goToRequirement(requirement: Requirement) {
        if (requirement.questionIds && requirement.questionIds.length) {
            this.router.navigate(['requirement', requirement.id, 'question', requirement.questionIds[0]], {relativeTo: this.route});
        } else {
            this.router.navigate(['requirement', requirement.id], {relativeTo: this.route});
        }
    }

    ngOnDestroy() {
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
    }

}
