import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {SelfAssessmentService, ReviewResponse, QUESTION_STATUS} from '../../../../shared';

@Component({
  selector: 'jhi-review-summary',
  templateUrl: './review-summary.component.html',
  styleUrls: ['./review-summary.component.scss', '../../responsive.scss']
})
export class ReviewSummaryComponent implements OnInit {
    reviewResponse: ReviewResponse[];
    STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private route: ActivatedRoute) {
        this.STATUS = QUESTION_STATUS;
    }

  ngOnInit() {
      this.selfAssessmentService.getSaqReviewedQuestions(this.STATUS.REVIEWED).subscribe(
          (response) => {
              this.reviewResponse = response;
          }
      );
  }
    goToQuestion(review: ReviewResponse) {
        this.router.navigate(['../requirement-category' , review.saqQuestion.pcidssRequirementId.requirementCategoryId.id
            , 'requirement'
            , review.saqQuestion.pcidssRequirementId.id, 'question' , review.saqQuestion.id], {relativeTo: this.route});
    }

    routeToNotification() {
        this.router.navigate(['../notification'], {relativeTo: this.route});
    }

}
