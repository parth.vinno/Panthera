import {Component, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';

import {
    EventModal, GlobalEventEmitter, SelfAssessmentService, QUESTION_RESPONSE, SaqResponse,
    DIRECTION, YesNoModalComponent, PAGE, STATE
} from '../../../shared';

@Component({
    selector: 'jhi-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss', '../fa-arrows.scss']
})
export class QuestionComponent implements OnInit, OnDestroy {
    @ViewChild('questionForm') public questionForm: NgForm;

    saqResponse: SaqResponse;
    curPage: PAGE;
    submitted: boolean;
    QUESTION_RESPONSE;
    PAGE = PAGE;
    STATE = STATE;
    curState: STATE;
    isCorrection: boolean;
    pageSubscription;
    routerSubscription;
    stateSubscription;
    isQueRoman: boolean;
    disableSubmit: boolean;

    constructor(private _ngZone: NgZone, private selfAssessmentService: SelfAssessmentService,
                private route: ActivatedRoute, private eventEmitter: GlobalEventEmitter, private router: Router,
                private _bsModalService: BsModalService, private store: Store<string>) {
        this.saqResponse = new SaqResponse();
        this.QUESTION_RESPONSE = QUESTION_RESPONSE;

        this.pageSubscription = this.eventEmitter.page.subscribe(
            (page) => {
                this.curPage = page;
            });

        this.stateSubscription = this.eventEmitter.state.subscribe(
            (state: STATE) => {
                this.curState = state;
            }
        );
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this.routerSubscription = this.route.params.subscribe((params) => this.loadSaqResponse(+params['questionId']));
            this.isCorrection = this.router.url.includes('correction');
        });
    }

    loadSaqResponse(questionId: number) {
        this.selfAssessmentService.getRequirementQuestionsById(questionId).subscribe(
            (response) => {
                this.saqResponse = response;
                this.eventEmitter.questionLoaded.emit(new EventModal(questionId));
                this.isQueRoman = (/[ivxIVX]/).test(this.saqResponse.saqQuestion.questionNumber);
            });
    }

    private validateForm() {
        this.submitted = true;
        return (this.questionForm && this.questionForm.form.valid);
    }

    isFormDirty() {
        return (this.questionForm && this.questionForm.form.dirty);
    }

    private saveResponse() {
        return new Promise((resolve, reject) => {
            this.selfAssessmentService.saveQuestionresponse(this.saqResponse).subscribe(
                (response) => {
                    resolve(response);
                },
                (error) => {
                    reject(error);
                });
        });
    }

    saveAndContinue() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            this.saveResponse().then(() => {
                if (this.curState === STATE.APPENDIX_WITH_LAST && (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE)) {
                    this.routeToSummary();
                } else if (this.curPage === PAGE.LAST || this.curPage === PAGE.SINGLE) {
                    this.eventEmitter.submitSaqQuestion.emit(new EventModal(false));
                } else {
                    this.routeToNext();
                }
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        } else {
            this.disableSubmit = false;
        }
    }

    saveAndExit() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            this.saveResponse().then(() => {
                this.eventEmitter.submitSaqQuestion.emit(new EventModal(false));
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        } else {
            this.disableSubmit = false;
        }
    }

    saveAndPrev() {
        if (this.validateForm()) {
            this.saveResponse().then(() => {
                this.routeToPrev();
            }).catch((error) => {
                console.log('error');
                alert('Error saving Response : ' + error.message);
            });
        }
    }

    routeToPrev() {
        this.resetPage();
        this.eventEmitter.submitSaqQuestion.emit(new EventModal(DIRECTION.BACK));
    }

    routeToNext() {
        this.resetPage();
        this.eventEmitter.submitSaqQuestion.emit(new EventModal(DIRECTION.FORWARD));
    }

    resetPage() {
        this.submitted = false;
        this.disableSubmit = false;
        this.questionForm.reset();
    }

    routeToSummary() {
        this.router.navigate(['./saq/assessment/summary']);
    }

    back() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.BACK);
        } else {
            this.routeToPrev();
        }
    }

    forward() {
        if (this.isFormDirty()) {
            this.openConfirmationModal(DIRECTION.FORWARD);
        } else {
            this.routeToNext();
        }
    }

    routeToCorrectionSummary() {
        this.router.navigate(['./saq/assessment/correction/summary']);
    }

    onCancel() {
        this.routeToCorrectionSummary();
    }

    onUpdate() {
        this.disableSubmit = true;
        if (this.validateForm()) {
            return new Promise((resolve, reject) => {
                this.saveResponse().then((savedRes) => {
                this.selfAssessmentService.updateQuestionResponse(this.saqResponse, this.saqResponse.saqQuestion.id).subscribe(
                    (response) => {
                        resolve(response);
                        this.routeToCorrectionSummary();
                    },
                    (error) => {
                        reject(error);
                    });
                }).catch((error) => {
                    console.log(error);
                });
            });
        } else {
            this.disableSubmit = false;
        }
    }

    openConfirmationModal(direction: DIRECTION) {
        const modal = this._bsModalService.show(YesNoModalComponent);
        modal.content.showConfirmationModal(
            'Confirm!!!',
            'You have unsaved changes. Do you want to save?'
        );

        modal.content.onClose.subscribe((result) => {
            if (result === true) {
                if (direction === DIRECTION.BACK) {
                    this.saveAndPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.saveAndContinue();
                }
            } else if (result === false) {
                if (direction === DIRECTION.BACK) {
                    this.routeToPrev();
                } else if (direction === DIRECTION.FORWARD) {
                    this.routeToNext();
                }
            }
        });
    }

    ngOnDestroy() {
        if (this.pageSubscription) {
            this.pageSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
