import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {SelfAssessmentService, RequirementCategory, Requirement, STATUS, GlobalEventEmitter} from '../../../shared';

@Component({
    selector: 'jhi-requirement-category',
    templateUrl: './requirement-category.component.html',
    styleUrls: ['./requirement-category.component.scss', '../responsive.scss']
})
export class RequirementCategoryComponent implements OnInit, OnDestroy {
    categoryId: number;
    requirements: Requirement[];
    selectedCategory: RequirementCategory;
    STATUS;
    predicate;
    reverse;
    statusOrder;
    routerSubscription;

    constructor(private selfAssessmentService: SelfAssessmentService, private route: ActivatedRoute,
                private router: Router, private _ngZone: NgZone) {
        this.STATUS = STATUS;

        // Listed in Ascending order
        this.statusOrder = { REVISIT: 1, RESUME: 2, TAKE: 3 };

        // Default values for pagination
        this.predicate = 'requirementNumber';
        this.reverse = false;
    }

    ngOnInit() {
        this._ngZone.run(() => {
                this.routerSubscription = this.route.params.subscribe((params) => {
                    this.categoryId = +params['categoryId'];
                    this.loadRequirements();
                });
        });
    }

    loadRequirements() {
        Observable.forkJoin(this.selfAssessmentService.getRequirementCategory(this.categoryId),
            this.selfAssessmentService.getRequirementsByCategoryId(this.categoryId)).subscribe((results) => {
            const [selectedCategory, requirements] = results;
            this.selectedCategory = selectedCategory;
            this.requirements = requirements;
        });
    }

    transition() {
        this.requirements.sort((req1, req2) => {
            if (this.reverse) {
                if (isNaN(req1[this.predicate])) {
                    if (this.predicate === 'requirementStatus') {
                        return this.statusOrder[req1[this.predicate]] - this.statusOrder[req2[this.predicate]];
                    } else {
                        return req1[this.predicate].localeCompare(req2[this.predicate]);
                    }
                } else {
                    return req1[this.predicate] - req2[this.predicate];
                }
            } else {
                if (isNaN(req1[this.predicate])) {
                    if (this.predicate === 'requirementStatus') {
                        return this.statusOrder[req2[this.predicate]] - this.statusOrder[req1[this.predicate]];
                    } else {
                        return req2[this.predicate].localeCompare(req1[this.predicate]);
                    }
                } else {
                    return req2[this.predicate] - req1[this.predicate];
                }
            }
        });
    }

    goToRequirement(requirement: Requirement) {
        if (requirement.requirementStatus === this.STATUS.RESUME) {
            this.router.navigate(['/saq/assessment/requirement-category/' + this.categoryId + '/requirement/'
            + requirement.id + '/question/' + requirement.lastRespondedQuestionId]);
        } else {
            if (requirement.questionIds && requirement.questionIds.length) {
                this.router.navigate(['/saq/assessment/requirement-category/' + this.categoryId + '/requirement/'
                + requirement.id + '/question/' + requirement.questionIds[0]]);
            } else {
                this.router.navigate(['/saq/assessment/requirement-category/' + this.categoryId + '/requirement/' + requirement.id]);
            }
        }
    }

    ngOnDestroy() {
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
    }

}
