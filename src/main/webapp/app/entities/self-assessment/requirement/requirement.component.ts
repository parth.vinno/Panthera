import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {
    EventModal, GlobalEventEmitter, PAGE, Requirement, SelfAssessmentService,
    DIRECTION, STATE
} from '../../../shared';

const FIRST_PAGE = 1;

@Component({
    selector: 'jhi-requirement',
    templateUrl: './requirement.component.html',
    styleUrls: ['./requirement.component.scss']
})
export class RequirementComponent implements OnInit, OnDestroy {

    curRequirement: Requirement;
    curPage: number;
    curState: string;
    submitSaqQuestionSubscription;
    questionLoadedSubscription;
    routerSubscription;
    stateSubscription;
    categoryId;
    requirements: Requirement[];

    constructor(private _ngZone: NgZone, private selfAssessmentService: SelfAssessmentService,
                private route: ActivatedRoute, private router: Router,
                private eventEmitter: GlobalEventEmitter) {
        this.curPage = FIRST_PAGE;

        this.submitSaqQuestionSubscription = this.eventEmitter.submitSaqQuestion.subscribe(
            (eventModal: EventModal) => {
                if (eventModal.data === false) {
                    this.goHome();
                } else {
                    this.goToDirection(eventModal.data);
                }
            });

        this.questionLoadedSubscription = this.eventEmitter.questionLoaded.subscribe(
            (eventModal) => {

                setTimeout(() => {
                    this.curPage = this.getPageNum(eventModal.data);
                    if (this.curPage === FIRST_PAGE && this.curPage === this.curRequirement.questionIds.length) {
                        this.eventEmitter.page.emit(PAGE.SINGLE);
                    } else if (this.curPage === FIRST_PAGE) {
                        this.eventEmitter.page.emit(PAGE.FIRST);
                    } else if (this.curRequirement && this.curPage === this.curRequirement.questionIds.length) {
                        this.eventEmitter.page.emit(PAGE.LAST);
                    } else {
                        this.eventEmitter.page.emit(PAGE.PAGE);
                    }
                }, 200);
            });
        this.stateSubscription = this.eventEmitter.state.subscribe(
            (state) => {
                this.curState = state;
            }
        );
    }

    ngOnInit() {
        this._ngZone.run(() => {
            this.route.params.subscribe((params) => {
                this.categoryId = +params['categoryId'];
            });
            this.routerSubscription = this.route.params.subscribe((params) => this.loadRequirement(+params['requirementId']));
            if (!this.router.url.includes('question')) {
                this.loadQuestion(this.curRequirement.questionIds[0]);
            }
        });
        this.selfAssessmentService.getRequirementsByCategoryId(this.categoryId).subscribe(
            (results) => {
                this.requirements = results;
                if (this.requirements[0].requirementCategoryId.categoryName.startsWith('Appendix')) {
                    if (this.curRequirement.id === this.requirements[this.requirements.length - 1].id) {
                        this.eventEmitter.state.emit(STATE.APPENDIX_WITH_LAST);
                    } else {
                        this.eventEmitter.state.emit(STATE.APPENDIX);
                    }
                } else {
                    this.eventEmitter.state.emit(STATE.OTHERS);
                }
            });
    }

    loadRequirement(requirementId: number) {
        this.selfAssessmentService.getRequirementById(requirementId).subscribe((req) => {
            this.curRequirement = req;
        }, (error) => {
            console.error(error);
        });
    }

    getPageNum(questionId): number {
        if (this.curRequirement) {
            for (let i = 0; i < this.curRequirement.questionIds.length; i++) {
                if (this.curRequirement.questionIds[i] === questionId) {
                    return i + 1;
                }
            }
            return FIRST_PAGE;
        }
    }

    getQuestionId(): number {
        return this.curRequirement.questionIds[this.curPage - 1];
    }

    goToDirection(directionSide) {
        if (directionSide === DIRECTION.FORWARD) {
            this.nextPage();
        } else {
            this.prevPage();
        }
    }

    prevPage() {
        if (this.curPage > 1) {
            this.curPage--;
            this.loadQuestion(this.getQuestionId());
        }
    }

    nextPage() {
        if (this.curPage < this.curRequirement.questionIds.length) {
            this.curPage++;
            this.loadQuestion(this.getQuestionId());
        }
    }

    goHome() {
        this.router.navigate(['../../'], {relativeTo: this.route});
    }

    loadQuestion(questionId): void {
        this.router.navigate(['./question/' + questionId], {relativeTo: this.route});
    }

    ngOnDestroy() {
        if (this.submitSaqQuestionSubscription) {
            this.submitSaqQuestionSubscription.unsubscribe();
        }
        if (this.questionLoadedSubscription) {
            this.questionLoadedSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        if (this.stateSubscription) {
            this.stateSubscription.unsubscribe();
        }
    }
}
