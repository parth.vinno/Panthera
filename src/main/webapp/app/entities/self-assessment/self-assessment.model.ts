import { BaseEntity } from '../../shared';

export class SelfAssessment implements BaseEntity {
    constructor(
        public id?: number,
    ) {
    }
}

export class RequirementCategory {
    id: number;
    categoryName: string;
    pcidssRequirements: Requirement[];
}
class Requirement {
    id: number;
    requirement: string;
    requirementNumber: number;
}
