import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap';
import { UiSwitchModule } from 'ngx-ui-switch';

import { PcidssSharedModule } from '../../shared';
import {
    SelfAssessmentComponent,
    AssessmentHomeComponent,
    LeftNavComponent,
    RequirementCategoryComponent,
    RequirementComponent,
    QuestionComponent,
    SummaryComponent,
    NotificationComponent,
    QAHomeComponent,
    ReviewLeftNavComponent,
    ReviewComponent,
    ReviewRequirementComponent,
    ReviewRequirementCategoryComponent,
    ReviewQuestionComponent,
    ReviewSummaryComponent,
    ReviewNotificationComponent,
    CorrectionComponent,
    selfAssessmentRoute,
} from './';

const ENTITY_STATES = [
    ...selfAssessmentRoute
];

@NgModule({
    imports: [
        PcidssSharedModule,
        FormsModule,
        UiSwitchModule,
        CollapseModule.forRoot(),
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
    ],
    declarations: [
        SelfAssessmentComponent,
        LeftNavComponent,
        RequirementCategoryComponent,
        QuestionComponent,
        RequirementComponent,
        SummaryComponent,
        NotificationComponent,
        QAHomeComponent,
        ReviewLeftNavComponent,
        ReviewComponent,
        ReviewRequirementComponent,
        ReviewRequirementCategoryComponent,
        ReviewQuestionComponent,
        ReviewSummaryComponent,
        AssessmentHomeComponent,
        ReviewNotificationComponent,
        CorrectionComponent,
    ],
    entryComponents: [
        SelfAssessmentComponent,
    ],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssSelfAssessmentModule {}
