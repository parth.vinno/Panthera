import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';

import {
    SelfAssessmentComponent,
    RequirementCategoryComponent,
    RequirementComponent,
    QuestionComponent,
    SummaryComponent,
    NotificationComponent,
    QAHomeComponent,
    ReviewComponent,
    ReviewRequirementComponent,
    ReviewRequirementCategoryComponent,
    ReviewQuestionComponent,
    ReviewSummaryComponent,
    ReviewNotificationComponent,
    CorrectionComponent,
    AssessmentHomeComponent
} from './';

export const selfAssessmentRoute: Routes = [
    {
        path: 'saq/assessment',
        component: SelfAssessmentComponent,
        data: {
            pageTitle: 'pcidssApp.selfAssessment.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: 'requirement-category',
                component: AssessmentHomeComponent,
                data: {
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService],
                children: [
                    {
                        path: ':categoryId',
                        component: RequirementCategoryComponent,
                        data: {
                            authorities: ['ROLE_USER'],
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        }
                    },
                    {
                        path: ':categoryId/requirement/:requirementId',
                        component: RequirementComponent,
                        data: {
                            authorities: ['ROLE_USER'],
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        },
                        children: [{
                            path: 'question/:questionId',
                            component: QuestionComponent,
                            data: {
                                pageTitle: 'pcidssApp.selfAssessment.home.title'
                            }
                        }]
                    }
                ]
            },
            {
                path: 'summary',
                component: SummaryComponent,
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService]
            },
            {
                path: 'notification',
                component: NotificationComponent,
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService]
            },
            {
                path: '',
                redirectTo: 'saq/assessment',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'saq/assessment',
        component: SelfAssessmentComponent,
        data: {
            pageTitle: 'pcidssApp.selfAssessment.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: 'correction/requirement-category',
                component: AssessmentHomeComponent,
                data: {
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService],
                children: [
                    {
                        path: ':categoryId',
                        component: RequirementCategoryComponent,
                        data: {
                            authorities: ['ROLE_USER'],
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        }
                    },
                    {
                        path: ':categoryId/requirement/:requirementId',
                        component: RequirementComponent,
                        data: {
                            authorities: ['ROLE_USER'],
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        },
                        children: [{
                            path: 'question/:questionId',
                            component: QuestionComponent,
                            data: {
                                pageTitle: 'pcidssApp.selfAssessment.home.title'
                            }
                        }]
                    }
                ]
            },
            {
                path: 'correction/summary',
                component: CorrectionComponent,
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService]
            },
            {
                path: 'correction/notification',
                component: NotificationComponent,
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService]
            },
            {
                path: '',
                redirectTo: 'saq/assessment',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'saq/assessment',
        component: SelfAssessmentComponent,
        data: {
            pageTitle: 'pcidssApp.selfAssessment.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: 'review',
                component: QAHomeComponent,
                data: {
                    authorities: ['ROLE_QA', 'ROLE_ADMIN'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService]
            },
            {
                path: 'review/:state',
                component: ReviewComponent,
                data: {
                    authorities: ['ROLE_QA', 'ROLE_ADMIN'],
                    pageTitle: 'pcidssApp.selfAssessment.home.title'
                },
                canActivate: [UserRouteAccessService],
                children: [
                    {
                        path: 'requirement-category/:categoryId',
                        component: ReviewRequirementCategoryComponent,
                        data: {
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        }
                    },
                    {
                        path: 'requirement-category/:categoryId/requirement/:requirementId',
                        component: ReviewRequirementComponent,
                        data: {
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        },
                        children: [{
                            path: 'question/:questionId',
                            component: ReviewQuestionComponent,
                            data: {
                                pageTitle: 'pcidssApp.selfAssessment.home.title'
                            }
                        }]
                    },
                    {
                        path: 'summary',
                        component: ReviewSummaryComponent,
                        data: {
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        },
                        canActivate: [UserRouteAccessService]
                    },
                    {
                        path: 'notification',
                        component: ReviewNotificationComponent,
                        data: {
                            pageTitle: 'pcidssApp.selfAssessment.home.title'
                        },
                        canActivate: [UserRouteAccessService]
                    }
                ]
            }
        ]
    }
];
