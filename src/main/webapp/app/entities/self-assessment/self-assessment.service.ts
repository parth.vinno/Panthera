import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { RequirementCategory } from './self-assessment.model';

const requirementCategories: RequirementCategory[] = [
    {
        'id': 1,
        'categoryName': 'Protect Cardholder Data',
        'pcidssRequirements': [
            {
                'id': 1,
                'requirement': 'Encrypt transmission of cardholder data across open, public networks',
                'requirementNumber': 1
            },
            {
                'id': 2,
                'requirement': 'Protect stored cardholder data',
                'requirementNumber': 2
            }
        ]
    },
    {
        'id': 2,
        'categoryName': 'Build and Maintain a Secure Network and Systems',
        'pcidssRequirements': [
            {
                'id': 3,
                'requirement': 'Install and maintain a firewall configuration to protect data',
                'requirementNumber': 3
            },
            {
                'id': 4,
                'requirement': 'Do not use vendor-supplied defaults for system passwords and other security parameters',
                'requirementNumber': 4
            }
        ]
    },
    {
        'id': 3,
        'categoryName': 'Maintain a Vulnerability Management Program',
        'pcidssRequirements': [
            {
                'id': 5,
                'requirement': 'Protect all systems against malware and regularly update anti-virus software or programs',
                'requirementNumber': 5
            },
            {
                'id': 6,
                'requirement': 'Develop and maintain secure systems and applications',
                'requirementNumber': 6
            }
        ]
    }
];

@Injectable()
export class SelfAssessmentService {

    constructor(private http: Http) { }

    getAllRequirementsCategories(): Observable<RequirementCategory[]> {
        // TODO: This is test data and we need to remove it once we integrate the endpoints
        return Observable.of(requirementCategories);
    }

    getRequirementCategory(id): Observable<RequirementCategory> {
        return Observable.of(
            requirementCategories.find( (category) => {
                return category.id = id;
            })
        );
    }
}
