import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {SelfAssessmentService, STATUS, Requirement} from '../../../shared';

@Component({
    selector: 'jhi-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss', '../responsive.scss']
})
export class SummaryComponent implements OnInit {
    requirements: Requirement[];
    STATUS;

    constructor(private selfAssessmentService: SelfAssessmentService, private router: Router,
                private route: ActivatedRoute) {
        this.STATUS = STATUS;
    }

    ngOnInit() {
        this.selfAssessmentService.getAllRequirements().subscribe(
            (res) => {
                this.requirements = res;
            }
        );
    }

    goToRequirement(requirement: Requirement) {
        if (requirement.requirementStatus === this.STATUS.RESUME) {
            this.router.navigate(['../requirement-category' , requirement.requirementCategoryId.id , 'requirement'
            , requirement.id , 'question' , requirement.lastRespondedQuestionId], {relativeTo: this.route});
        } else {
            if (requirement.questionIds && requirement.questionIds.length) {
                this.router.navigate(['../requirement-category' , requirement.requirementCategoryId.id , 'requirement'
                , requirement.id , 'question' , requirement.questionIds[0]], {relativeTo: this.route});
            } else {
                this.router.navigate(['../requirement-category' , requirement.requirementCategoryId.id , 'requirement' , requirement.id]);
            }
        }
    }

    routeToNotification() {
        this.router.navigate(['../notification'], {queryParams: {lastPage: 'summary'}, relativeTo: this.route});
    }

    onCancel() {
        this.router.navigate(['./saq/assessment-home']);
    }

    onSubmit() {
        this.routeToNotification();
    }
}
