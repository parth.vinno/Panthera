import { BaseEntity } from './../../shared';

export class State implements BaseEntity {
    constructor(
        public id?: number,
        public shortName?: string,
        public longName?: string,
        public countryCode?: string,
    ) {
    }
}
