import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PcidssSharedModule } from '../../shared';
import {
    StateService
} from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [
        PcidssSharedModule,
       // RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [],
    entryComponents: [],
    providers: [
        StateService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PcidssStateModule {}
