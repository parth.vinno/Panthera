// import { Injectable } from '@angular/core';
// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
//
// import { UserRouteAccessService } from '../../shared';
// import { JhiPaginationUtil } from 'ng-jhipster';
//
// import { StateComponent } from './state.component';
// import { StateDetailComponent } from './state-detail.component';
// import { StatePopupComponent } from './state-dialog.component';
// import { StateDeletePopupComponent } from './state-delete-dialog.component';
//
// import { Principal } from '../../shared';
//
// @Injectable()
// export class StateResolvePagingParams implements Resolve<any> {
//
//    constructor(private paginationUtil: JhiPaginationUtil) {}
//
//    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
//        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
//        return {
//            page: this.paginationUtil.parsePage(page),
//            predicate: this.paginationUtil.parsePredicate(sort),
//            ascending: this.paginationUtil.parseAscending(sort)
//      };
//    }
// }
//
// export const stateRoute: Routes = [
//    {
//        path: 'state',
//        component: StateComponent,
//        resolve: {
//            'pagingParams': StateResolvePagingParams
//        },
//        data: {
//            authorities: ['ROLE_USER'],
//            pageTitle: 'pcidssApp.state.home.title'
//        },
//        canActivate: [UserRouteAccessService]
//    }, {
//        path: 'state/:id',
//        component: StateDetailComponent,
//        data: {
//            authorities: ['ROLE_USER'],
//            pageTitle: 'pcidssApp.state.home.title'
//        },
//        canActivate: [UserRouteAccessService]
//    }
// ];
//
//
