import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import {Router} from '@angular/router';

import {Account, LoginService, Principal, StateStorageService} from '../shared';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    submitted: boolean;

    @ViewChild('signInForm') public signInForm: NgForm;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private router: Router,
        private stateStorageService: StateStorageService,
        private alertService: JhiAlertService,
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        if (this.validateForm()) {
            this.loginService.login({
                username: this.username,
                password: this.password,
                rememberMe: this.rememberMe
            }).then(() => {
                this.authenticationError = false;
                this.router.navigate(['/saq/assessment-home']);

                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });

                // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // // since login is succesful, go to stored previousState and clear previousState
                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl(undefined);
                    this.router.navigate([redirect]);
                } else {
                    this.router.navigate(['/saq/assessment-home']);
                }
            }).catch((error) => {
                this.authenticationError = true;
                this.alertService.error('error.authenticationFailed', null, null);
            });
        }
    }
    isJson(error) {
        try {
            JSON.parse(error);
        } catch (e) {
            return error;
        }
        return JSON.parse(error).message;
    }

    private validateForm() {
        this.submitted = true;
        return (this.signInForm && this.signInForm.form.valid);
    }

    isFormDirty() {
        return (this.signInForm && this.signInForm.form.dirty);
    }
}
