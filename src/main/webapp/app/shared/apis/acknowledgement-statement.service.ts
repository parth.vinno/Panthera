import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {AcknowledgementStatement} from './models/attestation.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class AcknowledgementStatementService {

    constructor(private http: Http, private es: EndpointService) { }

    getAcknowledgementStatements(): Observable<AcknowledgementStatement[]> {
        return this.http.get(this.es.getAcknowledgementStatementsUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

}
