import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {AcknowledgementStatus} from './models/attestation.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class AcknowledgementStatusService {

    constructor(private http: Http, private es: EndpointService) { }

    getAcknowledgementStatuses(): Observable<AcknowledgementStatus[]> {
        return this.http.get(this.es.getAcknowledgementStatusesUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveAcknowledgementStatuses(acknowledgementStatuses): Observable<AcknowledgementStatus[]> {
        return this.http.post(this.es.getAcknowledgementStatusesListUrl(), acknowledgementStatuses)
            .map((res: Response) => {
                return res.json();
            });
    }
}
