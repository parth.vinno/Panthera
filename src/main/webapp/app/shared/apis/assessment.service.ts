import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';

import {AssessmentInfo} from './models/attestation.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class AssessmentService {

  constructor(private http: Http, private es: EndpointService) {}
    getAssessmentDetails(): Observable<AssessmentInfo[]> {
        return this.http.get(this.es.getAssessmentUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

}
