import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {AttestationModel, FinalAttestation, IsaAttestation, QsaAttestation} from './models/attestation.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class AttestationService {

    constructor(private http: Http, private es: EndpointService) {
    }

    getAttestation(): Observable<Array<AttestationModel>> {
        return this.http.get(this.es.getAttestationUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveAttestation(attestation: AttestationModel): Observable<AttestationModel> {
        return this.http.put(this.es.getAttestationUrl(), attestation)
            .map((res: Response) => {
                return res.json()
            });
    }

    getFinalAttestations(): Observable<Array<FinalAttestation>> {
        return this.http.get(this.es.getFinalAttestationUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveFinalAttestation(attestation: FinalAttestation): Observable<FinalAttestation> {
        return this.http.put(this.es.getFinalAttestationUrl(), attestation)
            .map((res: Response) => {
                return res.json()
            });
    }

    getQsaAttestations(): Observable<Array<QsaAttestation>> {
        return this.http.get(this.es.getQsaFinalAttestationUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveQsaAttestation(attestation: QsaAttestation): Observable<QsaAttestation> {
        return this.http.put(this.es.getQsaFinalAttestationUrl(), attestation)
            .map((res: Response) => {
                return res.json()
            });
    }

    getIsaAttestations(): Observable<Array<IsaAttestation>> {
        return this.http.get(this.es.getIsaFinalAttestationUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveIsaAttestation(attestation: IsaAttestation): Observable<IsaAttestation> {
        return this.http.put(this.es.getIsaFinalAttestationUrl(), attestation)
            .map((res: Response) => {
                return res.json()
            });
    }

    deleteQsaAttestation(id: number) {
        return this.http.delete(this.es.getQsaFinalAttestationUrl() + '/' + id)
            .map((res: Response) => {
                return res;
            });
    }

    deleteIsaAttestation(id: number) {
        return this.http.delete(this.es.getIsaFinalAttestationUrl() + '/' + id)
            .map((res: Response) => {
                return res;
            });
    }
}
