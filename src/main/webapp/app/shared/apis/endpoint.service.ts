import {Injectable} from '@angular/core';

@Injectable()
export class EndpointService {
    /* User account */
    assessmentType: string;
    private userResourceUrl = 'api/users';
    private adminResourceUrl = 'api/admin';

    constructor() {
        this.assessmentType = '/service-provider';
    }

    private getUrl(url: string): string {
        return '/api' + url + this.assessmentType;
    }

    /* AOC-Legal-Exceptions and Requirements */
    getPciDssRequirementsUrl(): string {
        return this.getUrl('/pcidss-requirements');
    }

    getAocLegalExceptionsUrl(): string {
        return this.getUrl('/aoc-legal-exception-details');
    }

    getAocLegalExceptionsListUrl(): string {
        return this.getUrl('/list/aoc-legal-exception-details');
    }

    /* Non-Compliant Requirement Action plans */
    getRequirementActionPlansUrl(): string {
        return this.getUrl('/non-compliant-requirement-action-plans');
    }

    getRequirementActionPlansListUrl(): string {
        return this.getUrl('/list/non-compliant-requirement-action-plans');
    }

    /* Acknowledgement Statements */
    getAcknowledgementStatementsUrl(): string {
        return this.getUrl('/acknowledgement-statements');
    }

    /* Acknowledgement Status */
    getAcknowledgementStatusesUrl(): string {
        return this.getUrl('/acknowledgement-statuses');
    }

    getAcknowledgementStatusesListUrl(): string {
        return this.getUrl('/list/acknowledgement-statuses');
    }

    /* Compliance / Non-Compliance Attestation */
    getAttestationUrl(): string {
        return this.getUrl('/compliance-attestations');
    }

    /* Final attestation with signature */
    getFinalAttestationUrl(): string {
        return this.getUrl('/attestations');
    }

    /* Final attestation with signature */
    getQsaFinalAttestationUrl(): string {
        return this.getUrl('/qsa-attestations');
    }

    /* Final attestation with signature */
    getIsaFinalAttestationUrl(): string {
        return this.getUrl('/isa-attestations');
    }

    /* Organization*/
    getOrganizationUrl(): string {
        return this.getUrl('/organizations');
    }

    /* AssessmentInfo */
    getAssessmentUrl(): string {
        return this.getUrl('/assessment-infos');
    }

    /* Requirement Categories*/
    getRequirementCategoryUrl(isFixed?: boolean): string {
        return isFixed ? this.getUrl('/requirement-categories/fixed/available') : this.getUrl('/requirement-categories');
    }

    getAvailableRequirementsCategoriesUrl(): string {
        return this.getUrl('/requirement-categories/available');
    }

    /* Requirement by id*/
    getRequirementByIdUrl(isFixed?: boolean): string {
        return isFixed ? this.getUrl('/pcidss-requirements/with-saq-question-ids/fixed') : this.getUrl('/pcidss-requirements/with-saq-question-ids');
    }

    /* Requirement Questions*/
    getQuestionsResponseUrl(): string {
        return this.getUrl('/saq-responses/with-appendixes');
    }

    /*Requirements*/
    getAllRequirementsWithStatusUrl(isFixed?: boolean): string {
        return isFixed ? this.getUrl('/pcidss-requirements/with-status/fixed') : this.getUrl('/pcidss-requirements/with-status');
    }

    /* Notification */
    getNotificationUrl(): string {
        return '/api/users/by-role';
    }

    sendNotificationResponseUrl(): string {
        return this.getUrl('/qa-notifications/send-notification');
    }

    /*UserDetail*/
    getNotificationDetailUrl(): string {
        return this.getUrl('/qa-notifications/with-status');
    }

    getSaqReviewedQuestionUrl(): string {
        return this.getUrl('/saq-reviews');
    }

    getReviewResponseUrl(): string {
        return this.getUrl('/saq-reviews/saq-question');
    }

    sendReviewResponseUrl(): string {
        return this.getUrl('/saq-reviews/with-question-id');
    }

    /*QSA Notification*/
    sendQSANotificationUrl(): string {
        return this.getUrl('/qsa-notifications/send-notification');
    }

    /*AOC Notification*/
    sendAOCNotificationUrl(): string {
        return this.getUrl('/aoc-notifications/send-notification');
    }

    updateReviewUrl(): string {
        return this.getUrl('/saq-reviews/corrected');
    }
}
