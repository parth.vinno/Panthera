import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

// Enum with value
export enum COMPLIANT_STATEMENT {
    COMPLIANT = <any>'COMPLIANT',
    NON_COMPLIANT = <any>'NON_COMPLIANT',
    COMPLIANT_WITH_LEGAL_EXCEPTION = <any>'COMPLIANT_WITH_LEGAL_EXCEPTION'
}

export enum DIRECTION {
    FORWARD, BACK
}

export class AttestationModel {
    id?: number;
    compliantStatement: COMPLIANT_STATEMENT;
    complianceTargetDate?: string;
}

class RequirementCategoryId {
    id?: number;
    categoryName: string;
}

export class PciDssRequirement {
    id?: number;
    requirement: string;
    requirementNumber: number;
    requirementCategoryId: RequirementCategoryId;
}

export class AcknowledgementStatement {
    id?: number;
    statement: string;
    status?: boolean;
}

export class AcknowledgementStatus {
    acknowledgementStatementId?: AcknowledgementStatement;
    id?: number;
    statusResponse: boolean;

    constructor(statusResponse: boolean, acknowledgementStatementId: AcknowledgementStatement) {
        this.statusResponse = statusResponse;
        this.acknowledgementStatementId = acknowledgementStatementId;
    }
}

export class FinalAttestation {
    attestationDate?: string;
    attestorName: string;
    attestorSignature: string;
    attestorTitle: string;
    id?: number;
}

export class QsaAttestation {
    attestationDate?: string;
    qsaAttestorCompany: string;
    qsaAttestorName: string;
    qsaAttestorSignature: string;
    id?: number;

    constructor(qsaAttestorSignature: string) {
        this.qsaAttestorSignature = qsaAttestorSignature;
    }
}

export class IsaAttestation {
    attestationDate?: string;
    isaAttestorCompany: string;
    isaAttestorName: string;
    isaAttestorTitle: string;
    id?: number;
}

export class AocLegalExceptionDetail {
    id?: number;
    legalConstraintDetail: string;
    pcidssRequirementId: PciDssRequirement;
}

export class RequirementActionPlan {
    id?: number;
    compliant: boolean;
    pcidssRequirementId: PciDssRequirement;
    remediationPlan: string;
    remediationDate: string;
    // Temporary date field for datePicker format
    dpDate?: NgbDateStruct;

    constructor(pcidssRequirementId: PciDssRequirement, compliant: boolean) {
        this.pcidssRequirementId = pcidssRequirementId;
        this.compliant = compliant;
    }
}

export class AssessmentInfo {
    assessmentEndDate: string;
    assessmentType: string;
    assessor: string;
    id?: number;
}
