export class OrganizationModel {
  address: Address;
  companyName: string;
  contactName: string;
  dba: string;
  email: string;
  id?: number;
  telephone: string;
  title: string;
}

class Address {
    city: string;
    country: string;
    id?: number;
    postalCode: string;
    province: string;
    state: string;
    street1: string;
    street2: string;
    url: string;
}
