// Enum with value
export enum QUESTION_RESPONSE {
    YES = <any>'YES',
    YES_WITH_CCW = <any>'YES_WITH_CCW',
    NO = <any>'NO',
    NA = <any>'NA',
    NOT_TESTED = <any>'NOT_TESTED'
}

export enum STATUS {
    TAKE = <any>'TAKE',
    RESUME= <any>'RESUME',
    REVISIT= <any>'REVISIT'
}

export enum NOTIFICATION_STATUS {
    YES = <any> 'YES',
    NO = <any> 'NO',
}

export enum REVIEW_STATUS {
    NEW = <any>'NEW',
    PENDING = <any>'PENDING',
    RESPONSE_REQUIRED = <any>'RESPONSE_REQUIRED',
    REVIEW_REQUIRED = <any>'REVIEW_REQUIRED',
    SENT_TO_AOC = <any>'SENT_TO_AOC'
}

export enum QUESTION_STATUS {
    FIXED = <any>'FIXED',
    REVIEWED = <any>'REVIEWED'
}

export enum ROLE {
    QA = <any>'QA',
    QSA_AOC = <any>'QSA_AOC',
    QSA_OFFICER_AOC = <any>'QSA_OFFICER_AOC',
    ISA_AOC = <any>'ISA_AOC'
}

export class SaqResponse {
    id?: number;
    questionResponse?: QUESTION_RESPONSE;
    saqQuestion?: SaqQuestion;
    nonApplicabilityExplanation?: NonApplicability;
    requirementNotTestedExplanation?: RequirementNotTested;
    compensatingControlWorksheetResponses?: CompensatingControlWorksheet[];
}

export class NonApplicability {
    id?: number;
    reasonRequirementNotApplicable: string;
}

export class RequirementNotTested {
    id?: number;
    reason: string;
    requirementPartNotTested: string;
}

export class CompensatingControlWorksheet {
    id?: number;
    compensatingControlWorksheetId: CompensatingControlWorksheetId;
    explanation: string;
}

class CompensatingControlWorksheetId {
    id?: number;
    compensatingControlType: string;
    requiredInformation: string;
}

export class RequirementCategory {
    id?: number;
    categoryName: string;
    available?: boolean;
    pcidssRequirements?: Requirement[];
}

export class Requirement {
    available?: boolean;
    id?: number;
    requirement: string;
    requirementNumber: number;
    requirementStatus?: string;
    requirementCategoryId?: RequirementCategory;
    questionIds?: number[];
    questions?: SaqQuestion[];
    lastRespondedQuestionId?: number;
}

export class SaqQuestion {
    id?: number;
    questionNumber: string;
    responseRequired?: boolean;
    part?: boolean;
    parentQuestion: SaqQuestion;
    questionText: string;
    pcidssRequirementId?: Requirement;
    expectedTests?: ExpectedTest[];
    subQuestions?: SaqQuestion;
}

export class ExpectedTest {
    id?: number;
    testName: string;
}

export class Notification {
    id?: number;
    firstName: string;
    lastName?: string;
    email: string;
    title: string;
    role: string;
}

/* QA models */
export class QAReviewDetails {
    id?: number;
    correctionDate?: string;
    lastReviewedQuestion?: SaqQuestion;
    notificationDate: string;
    status: string;
    qsaId: User;
}

class User {
    id?: number;
    activated: boolean;
    email: string;
    firstName: string;
    imageUrl: string;
    langKey: string;
    lastName: string;
    login: string;
    resetDate: string;
    tenantId: string;
}

export class ReviewResponse {
    id?: number;
    comments: Comment[];
    createdDate: string;
    status: string;
    saqQuestion: SaqQuestion;
}

export class Comment {
    id?: number;
    comment: string;
    saqReviewId: SaqReview;
    editMode?: boolean;
}

class SaqReview {
    id?: number;
    comments: Comment;
    createdDate: string;
    lastUpdatedDate: string;
    status: string;
    saqQuestionId: SaqQuestion;
}
