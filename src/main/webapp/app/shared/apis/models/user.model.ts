export enum GENDER {
    MALE = <any> 'MALE',
    FEMALE = <any> 'FEMALE',
}

export class User {
    public id?: any;
    public login: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phone: string;
    public salutation: string;
    public gender: GENDER;
    public title: string;
    public activated?: Boolean;
    public langKey: string;
    public createdBy?: string;
    public createdDate?: Date;
    public lastModifiedBy?: string;
    public lastModifiedDate?: Date;
    public password?: string;
    public imageUrl?: string;
    public tenantId?: string;
    public authorities?: Authorities[];
    public userGroups: UserGroup[];
    public role: string;

    constructor() {
    }
}

export class UserGroup {
    id?: number;
    name?: string;
    authorities: Authorities[];
    resources: Resources[];
}

export class Authorities {
    id?: number;
    name?: string;
}

export class Resources {
    id?: number;
    resourceURL?: string;
    userGroups?: UserGroup[];
}

export class Tenant {
    addressId?: Address;
    companyPhoneNumber?: string;
    companyAbbreviation?: string;
    companyUrl?: string;
    contactPersonDeskphone?: string;
    contactPersonEmail?: string;
    contactPersonMobilephone?: string;
    contactPersonName?: string;
    createDate?: string;
    email?: string;
    id?: number;
    name?: string;
    serviceStartDate?: string;
    tenantId?: string;
    assessmentTypeId?: AssessmentType;
    industryId?: Industry;
}

export class Industry {
    id?: number;
    name: string;
    naics?: string;
}

export class AssessmentType {
    id?: number;
    name: string;
}

export class Address {
    city: string;
    country: string;
    id?: number;
    postalCode?: string;
    province?: string;
    state?: string;
    street1?: string;
    street2?: string;
    url?: string;
}

export class Country {
    countryCode: string;
    countryName: string;
}

export class State {
    countryCode: string;
    longName: string;
    shortName: string;
}
