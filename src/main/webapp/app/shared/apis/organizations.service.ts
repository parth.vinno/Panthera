import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';

import {OrganizationModel} from './models/organization.model';
import {EndpointService} from './endpoint.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class OrganizationsService {

  constructor(private http: Http, private es: EndpointService) { }

  getCompanyName(): Observable<Array<OrganizationModel>> {
    return this.http.get(this.es.getOrganizationUrl())
        .map((res: Response) => {
            return res.json();
        });
  }
}
