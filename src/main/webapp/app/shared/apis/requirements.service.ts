import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {AocLegalExceptionDetail, PciDssRequirement, RequirementActionPlan} from './models/attestation.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class RequirementsService {

    constructor(private http: Http, private es: EndpointService) { }

    getPciDssRequirements(): Observable<PciDssRequirement[]> {
        return this.http.get(this.es.getPciDssRequirementsUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    getAocLegalExceptions(): Observable<AocLegalExceptionDetail[]> {
        return this.http.get(this.es.getAocLegalExceptionsUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveAocLegalExceptions(legalExceptions: AocLegalExceptionDetail[]): Observable<AocLegalExceptionDetail[]> {
        return this.http.post(this.es.getAocLegalExceptionsListUrl(), legalExceptions)
            .map((res: Response) => {
                return res.json();
            });
    }

    getRequirementActionPlans(): Observable<RequirementActionPlan[]> {
        return this.http.get(this.es.getRequirementActionPlansUrl())
            .map((res: Response) => {
                return res.json();
            });
    }

    saveRequirementActionPlans(actionPlans: RequirementActionPlan[]): Observable<RequirementActionPlan[]> {
        return this.http.post(this.es.getRequirementActionPlansListUrl(), actionPlans)
            .map((res: Response) => {
                return res.json();
            });
    }

}
