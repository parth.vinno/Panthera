import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {
    RequirementCategory, Requirement, SaqResponse, Notification, QAReviewDetails, ReviewResponse
} from './models/self-assessment.model';
import {EndpointService} from './endpoint.service';

@Injectable()
export class SelfAssessmentService {

    constructor(private http: Http, private es: EndpointService) {
    }

    getAllRequirementCategories(status?): Observable<Array<RequirementCategory>> {
        return this.http.get(this.es.getRequirementCategoryUrl(status))
            .map((res: Response) => {
                return res.json();
            });
    }

    getAllAvailableRequirementsCategories(): Observable<Array<RequirementCategory>> {
        return this.http.get(this.es.getAvailableRequirementsCategoriesUrl())
            .map((res: Response) => {
            return res.json();
            });
    }

    getRequirementCategory(id): Observable<RequirementCategory> {
        return this.http.get(this.es.getRequirementCategoryUrl() + '/' + id)
            .map((res: Response) => {
            return res.json();
            });
    }

    getRequirementsByCategoryId(id, status?): Observable<Requirement[]> {
        return this.http.get(this.es.getAllRequirementsWithStatusUrl(status) + '/category-id/' + id)
            .map((res: Response) => {
            return res.json();
            });
    }

    getRequirementById(id, status?): Observable<Requirement> {
        return this.http.get(this.es.getRequirementByIdUrl(status) + '/' + id)
            .map((res: Response) => {
                return res.json();
            });
    }

    getRequirementQuestionsById(id): Observable<SaqResponse> {
        return this.http.get(this.es.getQuestionsResponseUrl() + '/' + id)
            .map((res: Response) => {
            return res.json();
            })
    }

    saveQuestionresponse(response: SaqResponse): Observable<SaqResponse> {
        return this.http.post(this.es.getQuestionsResponseUrl(), response)
            .map((res: Response) => {
            return res.json();
            })
    }

    getAllRequirements(): Observable<Requirement[]> {
        return this.http.get(this.es.getAllRequirementsWithStatusUrl())
            .map((res: Response) => {
            return res.json();
            });
    }

    getNotificationResource(role): Observable<Notification[]> {
        return this.http.get(this.es.getNotificationUrl() + '/' + role)
            .map((res: Response) => {
            return res.json();
            })
    }

    saveNotificationResponse(response: Notification): Observable<Notification> {
        return this.http.post(this.es.sendNotificationResponseUrl(), response)
            .map((res: Response) => {
            return res.json();
            })
    }

    getNotificationDetails(): Observable<QAReviewDetails> {
        return this.http.get(this.es.getNotificationDetailUrl())
            .map((res: Response) => {
            return res.json();
            })
    }

    getReviewResponseById(id): Observable<ReviewResponse> {
        return this.http.get(this.es.getReviewResponseUrl() + '/' + id)
            .map((res: Response) => {
            return res.json();
            })
    }

    sendReviewResponse(response: ReviewResponse, id, reviewStatus): Observable<ReviewResponse> {
        return this.http.post(this.es.sendReviewResponseUrl(), response, { params: {saqQuestionId: id, status: reviewStatus}} )
            .map((res: Response) => {
            return res.json();
            })
    }
    getSaqReviewedQuestions(reviewStatus): Observable<Array<ReviewResponse>> {
        return this.http.get(this.es.getSaqReviewedQuestionUrl(), {params: {status: reviewStatus}} )
            .map((res: Response) => {
            return res.json();
            })
    }

    sendQSANotification(response: Notification): Observable<Notification> {
        return this.http.post(this.es.sendQSANotificationUrl(), response)
            .map((res: Response) => {
                return res.json();
            });
    }

    sendISANotification(response: Notification[]): Observable<Notification> {
        return this.http.post(this.es.sendAOCNotificationUrl(), response)
            .map((res: Response) => {
                return res.json();
            });
    }

    updateQuestionResponse(response: SaqResponse, id): Observable<Notification> {
        return this.http.put(this.es.updateReviewUrl() + '/' + id, response)
            .map((res: Response) => {
                return res.json();
            })
    }
}
