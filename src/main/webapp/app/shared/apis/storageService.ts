import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ng2-webstorage';

const LKeys = {
    TENANT_ID: 'tenant_id',
    USER_TYPE: 'user_type',
};

@Injectable()
export class StorageService {

    constructor(private localStorageService: LocalStorageService) {
    }

    setTenantId(id: string): void {
        if (id) {
            this.localStorageService.store(LKeys.TENANT_ID, id);
            this.setUserType();
        } else {
            this.localStorageService.clear(LKeys.TENANT_ID);
        }
    }

    getTenantId(): string {
        return this.localStorageService.retrieve(LKeys.TENANT_ID);
    }

    setUserType(): void {
        this.localStorageService.store(LKeys.USER_TYPE, this.getTenantId().includes('merchant') ? 'Merchant' : 'Service Provider')
    }

    getUserType(): string {
        return this.localStorageService.retrieve(LKeys.USER_TYPE);
    }

    clearAll() {
        this.localStorageService.clear(LKeys.USER_TYPE);
        this.localStorageService.clear(LKeys.TENANT_ID);
    }
}
