import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Tenant } from './models/user.model';
import { ResponseWrapper } from '../model/response-wrapper.model';
import { createRequestOption } from '../model/request-util';

@Injectable()
export class TenantManagementService {
    private adminUrl = 'api/tenants';

    static convertResponse(res: Response): ResponseWrapper {
        const response: any = res;
        let jsonResponse;
        if ((res.status === 200 || res.status === 201) && response._body === '') {
            jsonResponse = {};
        } else {
            jsonResponse = res.json();
        }
        return new ResponseWrapper(response.headers, jsonResponse, response.status);
    }

    constructor(private http: Http) { }

    createTenant(user: Tenant): Observable<ResponseWrapper> {
        return this.http.post((this.adminUrl), user)
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    updateTenant(user: Tenant): Observable<ResponseWrapper> {
        return this.http.put(this.adminUrl, user)
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getAllTenants(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.adminUrl, options)
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getTenantDetail(id: string): Observable<ResponseWrapper> {
        return this.http.get(this.adminUrl + '/' + id)
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    deleteTenantAdmin(id: string): Observable<Response> {
        return this.http.delete(this.adminUrl + '/' + id);
    }

    getAllCountries(): Observable<ResponseWrapper> {
        return this.http.get('api/countries')
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getStates(country: string): Observable<ResponseWrapper> {
        return this.http.get('api/states/country/' + country)
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getAssessmentType(): Observable<ResponseWrapper> {
        return this.http.get('api/assessment-types')
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getIndustryType(): Observable<ResponseWrapper> {
        return this.http.get('api/industries')
            .map((res: Response) => TenantManagementService.convertResponse(res));
    }

    getDomainByTenantId(tenantId: string): Observable<string> {
        return this.http.get('api/tenants/domain/' + tenantId)
            .map((res: Response) => {
                return res['_body'];
            });
    }
}
