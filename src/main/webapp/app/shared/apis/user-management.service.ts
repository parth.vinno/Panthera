import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { User } from './models/user.model';
import { ResponseWrapper } from '../model/response-wrapper.model';
import { createRequestOption } from '../model/request-util';

@Injectable()
export class UserManagementService {
    private tenantUrl = 'api/tenant';
    private adminUrl = 'api/admin';

    static convertResponse(res: Response): ResponseWrapper {
        const response: any = res;
        let jsonResponse;
        if ((res.status === 200 || res.status === 201) && response._body === '') {
            jsonResponse = {};
        } else {
            jsonResponse = res.json();
        }
        return new ResponseWrapper(response.headers, jsonResponse, response.status);
    }

    constructor(private http: Http) { }

    registerUser(isNollySoftAdmin: boolean, user: User): Observable<ResponseWrapper> {
        return this.http.post(isNollySoftAdmin ? this.adminUrl + '/tenant-admin/register' : this.tenantUrl + '/user/register', user)
            .map((res: Response) => UserManagementService.convertResponse(res));
    }

    updateUser(isNollySoftAdmin: boolean, user: User): Observable<ResponseWrapper> {
        return this.http.put(isNollySoftAdmin ? this.adminUrl + '/tenant-admin' : this.tenantUrl + '/user', user)
            .map((res: Response) => UserManagementService.convertResponse(res));
    }

    registerAdmin(user: User): Observable<ResponseWrapper> {
        return this.http.post(this.adminUrl + '/admin/register', user)
            .map((res: Response) => UserManagementService.convertResponse(res)) ;
    }

    updateAdmin(user: User): Observable<ResponseWrapper> {
        return this.http.put(this.adminUrl + '/admin', user)
            .map((res: Response) => UserManagementService.convertResponse(res));
    }

    getAllUsers(isNollySoftAdmin: boolean, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(isNollySoftAdmin ? this.adminUrl + '/admin' : this.tenantUrl + '/user', options)
            .map((res: Response) => UserManagementService.convertResponse(res));
    }

    getUserDetail(login: string): Observable<ResponseWrapper> {
        return this.http.get('api/user/' + login)
            .map((res: Response) => UserManagementService.convertResponse(res));
    }

    deleteUser(isNollySoftAdmin: boolean, login: string, isTenantAdmin?: boolean): Observable<Response> {
        return this.http.delete((isNollySoftAdmin ? (this.adminUrl + (isTenantAdmin ? ('/tenant-admin/') : ('/admin/'))) : this.tenantUrl + '/user/') + login);
    }

    getAllUsersRoles(): Observable<ResponseWrapper> {
        return this.http.get('api/users/roles')
            .map((res: Response) => UserManagementService.convertResponse(res));
    }
}
