import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import {Tenant, User, UserGroup} from './models/user.model';
import { ResponseWrapper } from '../model/response-wrapper.model';
import { createRequestOption } from '../model/request-util';

@Injectable()
export class UserService {
    private resourceUrl = 'api/users';
    private adminUrl = 'api/admin';

    static convertResponse(res: Response): ResponseWrapper {
        const response: any = res;
        let jsonResponse;
        if ((res.status === 200 || res.status === 201) && response._body === '') {
            jsonResponse = {};
        } else {
            jsonResponse = res.json();
        }
        return new ResponseWrapper(response.headers, jsonResponse, response.status);
    }

    constructor(private http: Http) { }

    create(user: User, role?): Observable<ResponseWrapper> {
        return this.http.post((role === 'isUser' ? this.resourceUrl + '/register' : this.adminUrl + '/register'), user)
            .map((res: Response) => UserService.convertResponse(res));
    }

    update(user: User): Observable<ResponseWrapper> {
        return this.http.put(this.resourceUrl, user)
    .map((res: Response) => UserService.convertResponse(res));
    }

    find(login: string): Observable<User> {
        return this.http.get(`${this.resourceUrl}/${login}`).map((res: Response) => res.json());
    }

    query(isNollySoftAdmin?: boolean, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(isNollySoftAdmin ? this.adminUrl + '/all' : this.resourceUrl + '/tenant-users', options)
            .map((res: Response) => UserService.convertResponse(res));
    }

    delete(login: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${login}`);
    }

    authorities(): Observable<string[]> {
        return this.http.get('api/users/authorities').map((res: Response) => {
            const json = res.json();
            return <string[]> json;
        });
    }

    userGroups(): Observable<UserGroup[]> {
        return this.http.get('api/user-groups').map((res: Response) => {
            const json = res.json();
            return <UserGroup[]> json;
        });
    }

    getTenants(): Observable<Tenant[]> {
        return this.http.get('/api/tenants')
            .map((res: Response) => {
                return res.json();
            })
    }
}
