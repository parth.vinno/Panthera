import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Principal } from '../';
import { StateStorageService } from './state-storage.service';

@Injectable()
export class UserRouteAccessService implements CanActivate {
    isResetPasswordPage: boolean;

    constructor(private router: Router,
                private principal: Principal,
                private stateStorageService: StateStorageService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
        const authorities = route.data['authorities'];
        return this.checkLogin(authorities, state.url);
    }

    checkLogin(authorities: string[], url: string): Promise<boolean> {
        const principal = this.principal;
        this.isResetPasswordPage = url.includes('reset/finish');
        return Promise.resolve(principal.identity().then((account) => {

            if (account || this.isResetPasswordPage) {
                if (!authorities || authorities.length === 0 || principal.hasAnyAuthorityDirect(authorities) || this.isResetPasswordPage) {
                    return true;
                }
            } else {
                this.stateStorageService.storeUrl(url);
                this.router.navigate(['/']);
                return false;
            }

            this.router.navigate(['accessdenied']);
            return false;
        }));
    }
}
