import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.scss']
})
export class ButtonComponent implements OnInit {
    @Input() @HostBinding('class.primary') primary: boolean;
    constructor() {
    }

    ngOnInit() {
    }
}
