import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'jhi-common-title',
    templateUrl: './common-title.component.html',
    styleUrls: [ './common-title.component.scss' ]
})
export class CommonTitleComponent implements OnInit {
    @Input() title: string;

    constructor() {
    }

    ngOnInit() {
    }

}
