import { EventEmitter } from '@angular/core';

export enum PAGE {
    FIRST = <any>'FIRST',
    LAST = <any>'LAST',
    PAGE = <any>'PAGE',
    SINGLE = <any>'SINGLE',
}

export enum STATE {
    APPENDIX = <any>'APPENDIX',
    APPENDIX_WITH_LAST = <any>'APPENDIX_WITH_LAST',
    OTHERS = <any>'OTHERS'
}

export class EventModal {
    status?: string;
    data?: any;

    constructor(data: any) {
        this.data = data;
    }
}

export class GlobalEventEmitter {
    public submitSaqQuestion: EventEmitter<EventModal>;
    public questionLoaded: EventEmitter<EventModal>;
    public page: EventEmitter<PAGE>;
    public state: EventEmitter<STATE>;

    constructor() {
        this.submitSaqQuestion = new EventEmitter();
        this.questionLoaded = new EventEmitter();
        this.page = new EventEmitter();
        this.state = new EventEmitter();
    }
}
