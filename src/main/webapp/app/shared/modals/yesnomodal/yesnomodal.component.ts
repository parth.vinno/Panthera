import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'jhi-yesnomodal',
    templateUrl: './yesnomodal.component.html',
    styles: []
})
export class YesNoModalComponent implements OnInit {

    title: string;
    body: string;
    active: boolean;
    public onClose: Subject<boolean>;

    constructor(public _bsModalRef: BsModalRef) {
    }

    public ngOnInit(): void {
        this.onClose = new Subject();
    }

    public showConfirmationModal(title: string, body: string): void {
        this.title = title;
        this.body =  body;
        this.active = true;
    }

    public onConfirm(): void {
        this.active = false;
        this.onClose.next(true);
        this._bsModalRef.hide();
    }

    public onCancel(): void {
        this.active = false;
        this.onClose.next(false);
        this._bsModalRef.hide();
    }

    public hideConfirmationModal(): void {
        this.active = false;
        this.onClose.next(null);
        this._bsModalRef.hide();
    }

}
