import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
    name: 'localDateFormatPipe',
})
export class LocalDateFormatterPipe implements PipeTransform {

    constructor() {
    }

    transform(value: string) {
        if (value) {
            const [date, time] = value.split('T');
            const [year, month, day] = date.split('-');
            const [hour, minute, seconds] = time.split(':');
            const curDate = new Date(Date.UTC(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(day, 10),
                parseInt(hour, 10), parseInt(minute, 10), parseInt(seconds, 10)));
            const datePipe = new DatePipe('en-US');
            value = datePipe.transform(curDate, 'd MMM') + ' at ' + datePipe.transform(curDate, 'H:mm');
        }
        return value;
    }
}
