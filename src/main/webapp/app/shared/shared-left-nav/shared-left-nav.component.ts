import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'jhi-shared-left-nav',
    templateUrl: './shared-left-nav.component.html',
    styleUrls: [ './shared-left-nav.component.scss' ]
})
export class SharedLeftNavComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
