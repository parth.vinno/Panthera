import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgJhipsterModule } from 'ng-jhipster';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CookieModule } from 'ngx-cookie';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

@NgModule({
    imports: [
        NgbModule.forRoot(),
        NgJhipsterModule.forRoot({
            // set below to true to make alerts look like toast
            alertAsToast: false,
            i18nEnabled: true,
            defaultI18nLang: 'en'
        }),
        InfiniteScrollModule,
        CookieModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        SignaturePadModule,
        NgMultiSelectDropDownModule.forRoot(),
        Ng4LoadingSpinnerModule.forRoot(),
        InternationalPhoneNumberModule,
    ],
    exports: [
        FormsModule,
        HttpModule,
        CommonModule,
        NgbModule,
        NgJhipsterModule,
        InfiniteScrollModule,
        BsDropdownModule,
        ModalModule,
        SignaturePadModule,
        NgMultiSelectDropDownModule,
        Ng4LoadingSpinnerModule,
        InternationalPhoneNumberModule,
    ],
})
export class PcidssSharedLibsModule {}
