import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import {
    PcidssSharedLibsModule,
    PcidssSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    Principal,
    HasAnyAuthorityDirective,
    TitleComponent,
    CommonTitleComponent,
    SharedLeftNavComponent,
    ButtonComponent,
    SignaturePadComponent,
    EndpointService,
    RequirementsService,
    AcknowledgementStatementService,
    AttestationService,
    NgbCustomDateFormatter,
    AcknowledgementStatusService,
    YesNoModalComponent,
    OrganizationsService,
    AssessmentService,
    SelfAssessmentService,
    UserManagementService,
    TenantManagementService,
    GlobalEventEmitter,
    StorageService,
    LocalDateFormatterPipe
} from './';

@NgModule({
    imports: [
        PcidssSharedLibsModule,
        PcidssSharedCommonModule
    ],
    declarations: [
        HasAnyAuthorityDirective,
        TitleComponent,
        CommonTitleComponent,
        SharedLeftNavComponent,
        ButtonComponent,
        SignaturePadComponent,
        YesNoModalComponent,
        LocalDateFormatterPipe
    ],
    providers: [
        LoginService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        EndpointService,
        RequirementsService,
        AcknowledgementStatementService,
        AttestationService,
        AcknowledgementStatusService,
        OrganizationsService,
        AssessmentService,
        SelfAssessmentService,
        UserManagementService,
        TenantManagementService,
        GlobalEventEmitter,
        StorageService,
        { provide: NgbDateParserFormatter, useClass: NgbCustomDateFormatter }
    ],
    entryComponents: [YesNoModalComponent],
    exports: [
        PcidssSharedCommonModule,
        HasAnyAuthorityDirective,
        DatePipe,
        TitleComponent,
        CommonTitleComponent,
        ButtonComponent,
        SignaturePadComponent,
        YesNoModalComponent,
        SharedLeftNavComponent,
        LocalDateFormatterPipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class PcidssSharedModule {}
