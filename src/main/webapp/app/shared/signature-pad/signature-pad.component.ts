import {
    Component, ViewChild, AfterViewInit, ElementRef, OnInit, Input, Output, EventEmitter,
    SimpleChanges, OnChanges
} from '@angular/core';
import {SignaturePad} from 'angular2-signaturepad/signature-pad';

@Component({
    selector: 'jhi-signature-pad',
    templateUrl: './signature-pad.component.html',
    styleUrls: ['./signature-pad.component.scss']
})

export class SignaturePadComponent implements OnInit, AfterViewInit, OnChanges {
    @Input('selectedSignature') public selectedSignature;
    @Input('disableEdit') public disableEdit;
    @Output() signatureDone: EventEmitter<string> = new EventEmitter<string>();

    @ViewChild(SignaturePad) public signaturePad: SignaturePad;

    height: number;
    width: number;
    flag = false;
    signaturePadOptions: Object;
    childLoaded: boolean;

    constructor(private elementRef: ElementRef) {
        this.childLoaded = false;
    }

    ngOnChanges(changes: SimpleChanges) {
        for (const propName in changes) {
            if (changes.hasOwnProperty(propName) && propName === 'selectedSignature') {
                const change = changes[propName];
                const signatureData = change.currentValue;
                if (signatureData) {
                    this.drawSignature(signatureData);
                }
            }
        }
    }

    ngOnInit() {
        this.height = this.elementRef.nativeElement.querySelector('.sign-pad').offsetHeight;
        this.width = this.elementRef.nativeElement.querySelector('.sign-pad').offsetWidth;

        this.signaturePadOptions = {
            'canvasWidth': this.width,
            'canvasHeight': this.height
        };
    }

    ngAfterViewInit() {
        this.childLoaded = true;
        this.drawSignature(this.selectedSignature);
    }

    enablePad() {
        this.flag = false;
        this.signaturePad.on();
    }

    private disablePad() {
        this.flag = true;
        this.signaturePad.off();
    }

    private drawSignature(signatureData) {
        if (this.childLoaded && signatureData) {
            if (signatureData === 'CLEAR') {
                this.clear(true);
                this.disablePad();
            } else {
                this.signaturePad.fromDataURL(signatureData);
                this.disablePad();
            }
        }
    }

    done() {
        if (!this.signaturePad.isEmpty()) {
            this.selectedSignature = this.signaturePad.toDataURL();
            this.disablePad();
            this.signatureDone.emit(this.selectedSignature);
        } else {
            alert('Warning: Draw Your signature first!!!');
        }
    }

    clear(disableEmit?: boolean) {
        this.signaturePad.clear();
        if (!disableEmit) {
            this.selectedSignature = undefined;
            this.signatureDone.emit(this.selectedSignature);
        }
    }
}
