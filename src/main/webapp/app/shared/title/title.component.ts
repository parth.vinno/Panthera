import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'jhi-title',
    templateUrl: './title.component.html',
    styleUrls: [ './title.scss' ]
})
export class TitleComponent implements OnInit {
    @Input() title: any;

    constructor() {
    }

    ngOnInit() {
    }
}
