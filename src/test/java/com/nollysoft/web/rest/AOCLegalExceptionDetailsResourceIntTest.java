package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.AOCLegalExceptionDetails;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.repository.AOCLegalExceptionDetailsRepository;
import com.nollysoft.service.AOCLegalExceptionDetailsService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AOCLegalExceptionDetailsResource REST controller.
 *
 * @see AOCLegalExceptionDetailsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AOCLegalExceptionDetailsResourceIntTest {

	private static final String DEFAULT_LEGAL_CONSTRAINT_DETAIL = "Test Default Constraint Detail";
	private static final String UPDATED_LEGAL_CONSTRAINT_DETAIL = "Test Updated Constraint Detail";

	@Autowired
	private AOCLegalExceptionDetailsRepository aocLegalExceptionDetailsRepository;

	@Autowired
	private AOCLegalExceptionDetailsService aocLegalExceptionDetailsService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAOCLegalExceptionDetailsMockMvc;

	private AOCLegalExceptionDetails aocLegalExceptionDetails;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AOCLegalExceptionDetailsResource aocLegalExceptionDetailsResource = new AOCLegalExceptionDetailsResource(
				aocLegalExceptionDetailsService);
		this.restAOCLegalExceptionDetailsMockMvc = MockMvcBuilders.standaloneSetup(aocLegalExceptionDetailsResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static AOCLegalExceptionDetails createEntity(EntityManager em) {
		AOCLegalExceptionDetails aocLegalExceptionDetails = new AOCLegalExceptionDetails()
				.legalConstraintDetail(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
		PcidssRequirement pcidssRequirement = PcidssRequirementResourceIntTest.createEntity(em);
		em.persist(pcidssRequirement);
		em.flush();
		aocLegalExceptionDetails.setPcidssRequirementId(pcidssRequirement);
		return aocLegalExceptionDetails;
	}

	/**
	 * Create list of entity for this test.
	 */
	public List<AOCLegalExceptionDetails> createListOfEntity() {
		AOCLegalExceptionDetails aocLegalExceptionDetails1 = createEntity(em);
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = new ArrayList<>();
		aocLegalExceptionDetailsList.add(aocLegalExceptionDetails);
		aocLegalExceptionDetailsList.add(aocLegalExceptionDetails1);
		return aocLegalExceptionDetailsList;
	}

	@Before
	public void initTest() {
		aocLegalExceptionDetails = createEntity(em);
	}

	@Test
	@Transactional
	public void createAOCLegalExceptionDetailsForMerchant() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate + 1);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void createAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate + 1);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void createListOfAOCLegalExceptionDetailsForMerchant() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();
		List<AOCLegalExceptionDetails> aocLegalExceptionDetails = createListOfEntity();
		// Create List of new AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/list/aoc-legal-exception-details")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate + 2);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void createListOfAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();
		List<AOCLegalExceptionDetails> aocLegalExceptionDetails = createListOfEntity();
		// Create List of new AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/list/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate + 2);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void updateListOfAOCLegalExceptionDetailsForMerchant() throws Exception {
		//Creating list of 2 AOCLegalExceptionDetails in database
		aocLegalExceptionDetailsRepository.save(createListOfEntity());

		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList1 = new ArrayList<>();
		aocLegalExceptionDetailsList1.add(aocLegalExceptionDetails);

		//Clear and update with new list of AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/list/aoc-legal-exception-details")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetailsList1)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList2 = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList2).hasSize(databaseSizeBeforeCreate - 1);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList2
				.get(aocLegalExceptionDetailsList2.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void updateListOfAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		//Creating list of 2 AOCLegalExceptionDetails in database
		aocLegalExceptionDetailsRepository.save(createListOfEntity());

		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = new ArrayList<>();
		aocLegalExceptionDetailsList.add(aocLegalExceptionDetails);

		//Clear and update with new list of AOCLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/list/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetailsList)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList2 = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList2).hasSize(databaseSizeBeforeCreate - 1);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList2
				.get(aocLegalExceptionDetailsList2.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(DEFAULT_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void createAOCLegalExceptionDetailsWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails with an existing ID
		aocLegalExceptionDetails.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isBadRequest());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createAOCLegalExceptionDetailsWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails with an existing ID
		aocLegalExceptionDetails.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isBadRequest());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkPcidssRequirementIsRequiredForMerchant() throws Exception {
		// set the field null
		aocLegalExceptionDetails.setPcidssRequirementId(null);
		;

		// Create the AOCLegalExceptionDetails, which fails.

		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkPcidssRequirementIsRequiredForServiceProvider() throws Exception {
		// set the field null
		aocLegalExceptionDetails.setPcidssRequirementId(null);
		;

		// Create the AOCLegalExceptionDetails, which fails.

		restAOCLegalExceptionDetailsMockMvc
				.perform(post("/api/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllAOCLegalExceptionDetailsForMerchant() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsRepository.saveAndFlush(aocLegalExceptionDetails);

		// Get all the aocLegalExceptionDetailsList
		restAOCLegalExceptionDetailsMockMvc.perform(get("/api/aoc-legal-exception-details?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(aocLegalExceptionDetails.getId().intValue())))
				.andExpect(jsonPath("$.[*].legalConstraintDetail")
						.value(hasItem(DEFAULT_LEGAL_CONSTRAINT_DETAIL.toString())));
	}

	@Test
	@Transactional
	public void getAllAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsRepository.saveAndFlush(aocLegalExceptionDetails);

		// Get all the aocLegalExceptionDetailsList
		restAOCLegalExceptionDetailsMockMvc
				.perform(get("/api/aoc-legal-exception-details/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(aocLegalExceptionDetails.getId().intValue())))
				.andExpect(jsonPath("$.[*].legalConstraintDetail")
						.value(hasItem(DEFAULT_LEGAL_CONSTRAINT_DETAIL.toString())));
	}

	@Test
	@Transactional
	public void updateAOCLegalExceptionDetailsForMerchant() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);

		int databaseSizeBeforeUpdate = aocLegalExceptionDetailsRepository.findAll().size();

		// Update the aocLegalExceptionDetails
		AOCLegalExceptionDetails updatedAOCLegalExceptionDetails = aocLegalExceptionDetailsRepository
				.findOne(aocLegalExceptionDetails.getId());
		// Disconnect from session so that the updates on
		// updatedAOCLegalExceptionDetails are not directly saved in db
		em.detach(updatedAOCLegalExceptionDetails);
		updatedAOCLegalExceptionDetails.legalConstraintDetail(UPDATED_LEGAL_CONSTRAINT_DETAIL);

		restAOCLegalExceptionDetailsMockMvc
				.perform(put("/api/aoc-legal-exception-details").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAOCLegalExceptionDetails)))
				.andExpect(status().isOk());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeUpdate);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(UPDATED_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void updateAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);

		int databaseSizeBeforeUpdate = aocLegalExceptionDetailsRepository.findAll().size();

		// Update the aocLegalExceptionDetails
		AOCLegalExceptionDetails updatedAOCLegalExceptionDetails = aocLegalExceptionDetailsRepository
				.findOne(aocLegalExceptionDetails.getId());
		// Disconnect from session so that the updates on
		// updatedAOCLegalExceptionDetails are not directly saved in db
		em.detach(updatedAOCLegalExceptionDetails);
		updatedAOCLegalExceptionDetails.legalConstraintDetail(UPDATED_LEGAL_CONSTRAINT_DETAIL);

		restAOCLegalExceptionDetailsMockMvc
				.perform(put("/api/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAOCLegalExceptionDetails)))
				.andExpect(status().isOk());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeUpdate);
		AOCLegalExceptionDetails testAOCLegalExceptionDetails = aocLegalExceptionDetailsList
				.get(aocLegalExceptionDetailsList.size() - 1);
		assertThat(testAOCLegalExceptionDetails.getLegalConstraintDetail()).isEqualTo(UPDATED_LEGAL_CONSTRAINT_DETAIL);
	}

	@Test
	@Transactional
	public void updateNonExistingAOCLegalExceptionDetailsForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAOCLegalExceptionDetailsMockMvc
				.perform(put("/api/aoc-legal-exception-details").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = aocLegalExceptionDetailsRepository.findAll().size();

		// Create the AOCLegalExceptionDetails

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAOCLegalExceptionDetailsMockMvc
				.perform(put("/api/aoc-legal-exception-details/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AOCLegalExceptionDetails in the database
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAOCLegalExceptionDetailsForMerchant() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);

		int databaseSizeBeforeDelete = aocLegalExceptionDetailsRepository.findAll().size();

		// Get the aocLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(delete("/api/aoc-legal-exception-details/{id}", aocLegalExceptionDetails.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteAOCLegalExceptionDetailsForServiceProvider() throws Exception {
		// Initialize the database
		aocLegalExceptionDetailsService.save(aocLegalExceptionDetails);

		int databaseSizeBeforeDelete = aocLegalExceptionDetailsRepository.findAll().size();

		// Get the aocLegalExceptionDetails
		restAOCLegalExceptionDetailsMockMvc
				.perform(delete("/api/aoc-legal-exception-details/service-provider/{id}",
						aocLegalExceptionDetails.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AOCLegalExceptionDetails> aocLegalExceptionDetailsList = aocLegalExceptionDetailsRepository.findAll();
		assertThat(aocLegalExceptionDetailsList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(AOCLegalExceptionDetails.class);
		AOCLegalExceptionDetails aocLegalExceptionDetails1 = new AOCLegalExceptionDetails();
		aocLegalExceptionDetails1.setId(1L);
		AOCLegalExceptionDetails aocLegalExceptionDetails2 = new AOCLegalExceptionDetails();
		aocLegalExceptionDetails2.setId(aocLegalExceptionDetails1.getId());
		assertThat(aocLegalExceptionDetails1).isEqualTo(aocLegalExceptionDetails2);
		aocLegalExceptionDetails2.setId(2L);
		assertThat(aocLegalExceptionDetails1).isNotEqualTo(aocLegalExceptionDetails2);
		aocLegalExceptionDetails1.setId(null);
		assertThat(aocLegalExceptionDetails1).isNotEqualTo(aocLegalExceptionDetails2);
	}
}
