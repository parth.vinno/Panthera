package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.AOCNotification;
import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.User;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;
import com.nollysoft.repository.AOCNotificationRepository;
import com.nollysoft.repository.AssessmentInfoRepository;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.service.AOCNotificationService;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AOCNotificationResource REST controller.
 *
 * @see AOCNotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AOCNotificationResourceIntTest {

	private static final LocalDate DEFAULT_NOTIFICATION_DATE = LocalDate.now();
	private static final LocalDate UPDATED_NOTIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

	@Autowired
	private AOCNotificationRepository aocNotificationRepository;

	@Autowired
	private AOCNotificationService aocNotificationService;

	@Autowired
	private QANotificationService qaNotificationService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private AssessmentInfoRepository assessmentInfoRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAOCNotificationMockMvc;

	private AOCNotification aocNotification;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AOCNotificationResource aocNotificationResource = new AOCNotificationResource(aocNotificationService,
				userRepository, mailService, qaNotificationService, assessmentInfoRepository, organizationRepository);
		this.restAOCNotificationMockMvc = MockMvcBuilders.standaloneSetup(aocNotificationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static AOCNotification createEntity(EntityManager em) {
		AOCNotification aocNotification = new AOCNotification().notificationDate(DEFAULT_NOTIFICATION_DATE);
		// Add required entity
		User user = UserResourceIntTest.createEntity(em);
		em.persist(user);
		em.flush();
		aocNotification.setAocId(user);
		// Add required entity
		QANotification qaNotificationId = new QANotification().status(NOTIFICATION_STATUS.NEW)
				.notificationDate(DEFAULT_NOTIFICATION_DATE);
		qaNotificationId.setQaId(user);
		qaNotificationId.setQsaId(user);
		em.persist(qaNotificationId);
		em.flush();
		aocNotification.setQaNotificationId(qaNotificationId);
		return aocNotification;
	}

	@Before
	public void initTest() {
		aocNotification = createEntity(em);
	}

	@Test
	@Transactional
	public void createAOCNotification() throws Exception {
		int databaseSizeBeforeCreate = aocNotificationRepository.findAll().size();

		// Create the AOCNotification
		restAOCNotificationMockMvc.perform(post("/api/aoc-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(aocNotification))).andExpect(status().isCreated());

		// Validate the AOCNotification in the database
		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeCreate + 1);
		AOCNotification testAOCNotification = aocNotificationList.get(aocNotificationList.size() - 1);
		assertThat(testAOCNotification.getNotificationDate()).isEqualTo(DEFAULT_NOTIFICATION_DATE);
	}

	@Test
	@Transactional
	public void createAOCNotificationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = aocNotificationRepository.findAll().size();

		// Create the AOCNotification with an existing ID
		aocNotification.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restAOCNotificationMockMvc
				.perform(post("/api/aoc-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocNotification)))
				.andExpect(status().isBadRequest());

		// Validate the AOCNotification in the database
		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkNotificationDateIsRequired() throws Exception {
		int databaseSizeBeforeTest = aocNotificationRepository.findAll().size();
		// set the field null
		aocNotification.setNotificationDate(null);

		// Create the AOCNotification, which fails.

		restAOCNotificationMockMvc
				.perform(post("/api/aoc-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocNotification)))
				.andExpect(status().isBadRequest());

		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllAOCNotifications() throws Exception {
		// Initialize the database
		aocNotificationRepository.saveAndFlush(aocNotification);

		// Get all the aocNotificationList
		restAOCNotificationMockMvc.perform(get("/api/aoc-notifications?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(aocNotification.getId().intValue())))
				.andExpect(jsonPath("$.[*].notificationDate").value(hasItem(DEFAULT_NOTIFICATION_DATE.toString())));
	}

	@Test
	@Transactional
	public void getAOCNotification() throws Exception {
		// Initialize the database
		aocNotificationRepository.saveAndFlush(aocNotification);

		// Get the aocNotification
		restAOCNotificationMockMvc.perform(get("/api/aoc-notifications/{id}", aocNotification.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(aocNotification.getId().intValue()))
				.andExpect(jsonPath("$.notificationDate").value(DEFAULT_NOTIFICATION_DATE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingAOCNotification() throws Exception {
		// Get the aocNotification
		restAOCNotificationMockMvc.perform(get("/api/aoc-notifications/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateAOCNotification() throws Exception {
		// Initialize the database
		aocNotificationService.save(aocNotification);

		int databaseSizeBeforeUpdate = aocNotificationRepository.findAll().size();

		// Update the aocNotification
		AOCNotification updatedAOCNotification = aocNotificationRepository.findOne(aocNotification.getId());
		// Disconnect from session so that the updates on updatedAOCNotification
		// are not
		// directly saved in db
		em.detach(updatedAOCNotification);
		updatedAOCNotification.notificationDate(UPDATED_NOTIFICATION_DATE);

		restAOCNotificationMockMvc.perform(put("/api/aoc-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedAOCNotification))).andExpect(status().isOk());

		// Validate the AOCNotification in the database
		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeUpdate);
		AOCNotification testAOCNotification = aocNotificationList.get(aocNotificationList.size() - 1);
		assertThat(testAOCNotification.getNotificationDate()).isEqualTo(UPDATED_NOTIFICATION_DATE);
	}

	@Test
	@Transactional
	public void updateNonExistingAOCNotification() throws Exception {
		int databaseSizeBeforeUpdate = aocNotificationRepository.findAll().size();

		// Create the AOCNotification

		// If the entity doesn't have an ID, it will be created instead of just
		// being
		// updated
		restAOCNotificationMockMvc.perform(put("/api/aoc-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(aocNotification))).andExpect(status().isCreated());

		// Validate the AOCNotification in the database
		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAOCNotification() throws Exception {
		// Initialize the database
		aocNotificationService.save(aocNotification);

		int databaseSizeBeforeDelete = aocNotificationRepository.findAll().size();

		// Get the aocNotification
		restAOCNotificationMockMvc.perform(
				delete("/api/aoc-notifications/{id}", aocNotification.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AOCNotification> aocNotificationList = aocNotificationRepository.findAll();
		assertThat(aocNotificationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(AOCNotification.class);
		AOCNotification aocNotification1 = new AOCNotification();
		aocNotification1.setId(1L);
		AOCNotification aocNotification2 = new AOCNotification();
		aocNotification2.setId(aocNotification1.getId());
		assertThat(aocNotification1).isEqualTo(aocNotification2);
		aocNotification2.setId(2L);
		assertThat(aocNotification1).isNotEqualTo(aocNotification2);
		aocNotification1.setId(null);
		assertThat(aocNotification1).isNotEqualTo(aocNotification2);
	}
}
