package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.AcknowledgementStatement;
import com.nollysoft.repository.AcknowledgementStatementRepository;
import com.nollysoft.service.AcknowledgementStatementService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AcknowledgementStatementResource REST controller.
 *
 * @see AcknowledgementStatementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AcknowledgementStatementResourceIntTest {

	private static final String DEFAULT_STATEMENT = "Test Default Statement";
	private static final String UPDATED_STATEMENT = "Test Updated Statement";

	@Autowired
	private AcknowledgementStatementRepository acknowledgementStatementRepository;

	@Autowired
	private AcknowledgementStatementService acknowledgementStatementService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAcknowledgementStatementMockMvc;

	private AcknowledgementStatement acknowledgementStatement;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AcknowledgementStatementResource acknowledgementStatementResource = new AcknowledgementStatementResource(
				acknowledgementStatementService);
		this.restAcknowledgementStatementMockMvc = MockMvcBuilders.standaloneSetup(acknowledgementStatementResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static AcknowledgementStatement createEntity(EntityManager em) {
		AcknowledgementStatement acknowledgementStatement = new AcknowledgementStatement().statement(DEFAULT_STATEMENT);
		return acknowledgementStatement;
	}

	@Before
	public void initTest() {
		acknowledgementStatement = createEntity(em);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatementForMerchant() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement
		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeCreate + 1);
		AcknowledgementStatement testAcknowledgementStatement = acknowledgementStatementList
				.get(acknowledgementStatementList.size() - 1);
		assertThat(testAcknowledgementStatement.getStatement()).isEqualTo(DEFAULT_STATEMENT);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatementWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement with an existing ID
		acknowledgementStatement.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isBadRequest());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatementForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement
		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeCreate + 1);
		AcknowledgementStatement testAcknowledgementStatement = acknowledgementStatementList
				.get(acknowledgementStatementList.size() - 1);
		assertThat(testAcknowledgementStatement.getStatement()).isEqualTo(DEFAULT_STATEMENT);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatementWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement with an existing ID
		acknowledgementStatement.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isBadRequest());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkStatementIsRequiredForMerchant() throws Exception {
		// set the field null
		acknowledgementStatement.setStatement(null);

		// Create the AcknowledgementStatement, which fails.

		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkStatementIsRequiredForServiceProvider() throws Exception {
		// set the field null
		acknowledgementStatement.setStatement(null);

		// Create the AcknowledgementStatement, which fails.

		restAcknowledgementStatementMockMvc
				.perform(post("/api/acknowledgement-statements/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllAcknowledgementStatementsForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatementRepository.saveAndFlush(acknowledgementStatement);

		// Get all the acknowledgementStatementList
		restAcknowledgementStatementMockMvc.perform(get("/api/acknowledgement-statements?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(acknowledgementStatement.getId().intValue())))
				.andExpect(jsonPath("$.[*].statement").value(hasItem(DEFAULT_STATEMENT.toString())));
	}

	@Test
	@Transactional
	public void getAllAcknowledgementStatementsForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatementRepository.saveAndFlush(acknowledgementStatement);

		// Get all the acknowledgementStatementList
		restAcknowledgementStatementMockMvc
				.perform(get("/api/acknowledgement-statements/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(acknowledgementStatement.getId().intValue())))
				.andExpect(jsonPath("$.[*].statement").value(hasItem(DEFAULT_STATEMENT.toString())));
	}

	@Test
	@Transactional
	public void updateAcknowledgementStatementForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatementService.save(acknowledgementStatement);

		int databaseSizeBeforeUpdate = acknowledgementStatementRepository.findAll().size();

		// Update the acknowledgementStatement
		AcknowledgementStatement updatedAcknowledgementStatement = acknowledgementStatementRepository
				.findOne(acknowledgementStatement.getId());
		// Disconnect from session so that the updates on
		// updatedAcknowledgementStatement are not directly saved in db
		em.detach(updatedAcknowledgementStatement);
		updatedAcknowledgementStatement.statement(UPDATED_STATEMENT);

		restAcknowledgementStatementMockMvc
				.perform(put("/api/acknowledgement-statements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAcknowledgementStatement)))
				.andExpect(status().isOk());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeUpdate);
		AcknowledgementStatement testAcknowledgementStatement = acknowledgementStatementList
				.get(acknowledgementStatementList.size() - 1);
		assertThat(testAcknowledgementStatement.getStatement()).isEqualTo(UPDATED_STATEMENT);
	}

	@Test
	@Transactional
	public void updateAcknowledgementStatementForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatementService.save(acknowledgementStatement);

		int databaseSizeBeforeUpdate = acknowledgementStatementRepository.findAll().size();

		// Update the acknowledgementStatement
		AcknowledgementStatement updatedAcknowledgementStatement = acknowledgementStatementRepository
				.findOne(acknowledgementStatement.getId());
		// Disconnect from session so that the updates on
		// updatedAcknowledgementStatement are not directly saved in db
		em.detach(updatedAcknowledgementStatement);
		updatedAcknowledgementStatement.statement(UPDATED_STATEMENT);

		restAcknowledgementStatementMockMvc
				.perform(put("/api/acknowledgement-statements/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAcknowledgementStatement)))
				.andExpect(status().isOk());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeUpdate);
		AcknowledgementStatement testAcknowledgementStatement = acknowledgementStatementList
				.get(acknowledgementStatementList.size() - 1);
		assertThat(testAcknowledgementStatement.getStatement()).isEqualTo(UPDATED_STATEMENT);
	}

	@Test
	@Transactional
	public void updateNonExistingAcknowledgementStatementForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAcknowledgementStatementMockMvc
				.perform(put("/api/acknowledgement-statements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingAcknowledgementStatementForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = acknowledgementStatementRepository.findAll().size();

		// Create the AcknowledgementStatement

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAcknowledgementStatementMockMvc
				.perform(put("/api/acknowledgement-statements/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatement)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatement in the database
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAcknowledgementStatementForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatementService.save(acknowledgementStatement);

		int databaseSizeBeforeDelete = acknowledgementStatementRepository.findAll().size();

		// Get the acknowledgementStatement
		restAcknowledgementStatementMockMvc
				.perform(delete("/api/acknowledgement-statements/{id}", acknowledgementStatement.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteAcknowledgementStatementForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatementService.save(acknowledgementStatement);

		int databaseSizeBeforeDelete = acknowledgementStatementRepository.findAll().size();

		// Get the acknowledgementStatement
		restAcknowledgementStatementMockMvc
				.perform(delete("/api/acknowledgement-statements/service-provider/{id}",
						acknowledgementStatement.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AcknowledgementStatement> acknowledgementStatementList = acknowledgementStatementRepository.findAll();
		assertThat(acknowledgementStatementList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(AcknowledgementStatement.class);
		AcknowledgementStatement acknowledgementStatement1 = new AcknowledgementStatement();
		acknowledgementStatement1.setId(1L);
		AcknowledgementStatement acknowledgementStatement2 = new AcknowledgementStatement();
		acknowledgementStatement2.setId(acknowledgementStatement1.getId());
		assertThat(acknowledgementStatement1).isEqualTo(acknowledgementStatement2);
		acknowledgementStatement2.setId(2L);
		assertThat(acknowledgementStatement1).isNotEqualTo(acknowledgementStatement2);
		acknowledgementStatement1.setId(null);
		assertThat(acknowledgementStatement1).isNotEqualTo(acknowledgementStatement2);
	}
}
