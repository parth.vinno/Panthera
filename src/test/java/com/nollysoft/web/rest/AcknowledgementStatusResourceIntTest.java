package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.AcknowledgementStatement;
import com.nollysoft.domain.AcknowledgementStatus;
import com.nollysoft.repository.AcknowledgementStatementRepository;
import com.nollysoft.repository.AcknowledgementStatusRepository;
import com.nollysoft.service.AcknowledgementStatusService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AcknowledgementStatusResource REST controller.
 *
 * @see AcknowledgementStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AcknowledgementStatusResourceIntTest {

	private static final Boolean DEFAULT_STATUS_RESPONSE = false;
	private static final Boolean UPDATED_STATUS_RESPONSE = true;

	private static final String DEFAULT_ACKNOWLEDGEMENT_STATEMENT = "Test Default Statement";

	@Autowired
	private AcknowledgementStatementRepository acknowledgementStatementRepository;

	@Autowired
	private AcknowledgementStatusRepository acknowledgementStatusRepository;

	@Autowired
	private AcknowledgementStatusService acknowledgementStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAcknowledgementStatusMockMvc;

	private AcknowledgementStatus acknowledgementStatus;
	
	private AcknowledgementStatement acknowledgementStatementId;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AcknowledgementStatusResource acknowledgementStatusResource = new AcknowledgementStatusResource(
				acknowledgementStatusService);
		this.restAcknowledgementStatusMockMvc = MockMvcBuilders.standaloneSetup(acknowledgementStatusResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static AcknowledgementStatus createEntity(EntityManager em) {
		AcknowledgementStatus acknowledgementStatus = new AcknowledgementStatus()
				.statusResponse(DEFAULT_STATUS_RESPONSE);
		return acknowledgementStatus;
	}
	
	/**
	 * Create list of entity for this test.
	 */
	public List<AcknowledgementStatus> createListOfEntity() {
		AcknowledgementStatus acknowledgementStatus1 = createEntity(em);
		acknowledgementStatementId = acknowledgementStatementRepository
				.save(new AcknowledgementStatement().statement(DEFAULT_ACKNOWLEDGEMENT_STATEMENT));
		acknowledgementStatus1.setAcknowledgementStatementId(acknowledgementStatementId);;
		List<AcknowledgementStatus> acknowledgementStatusList = new ArrayList<>();
		acknowledgementStatusList.add(acknowledgementStatus);
		acknowledgementStatusList.add(acknowledgementStatus1);
		return acknowledgementStatusList;
	}

	@Before
	public void initTest() {
		acknowledgementStatus = createEntity(em);
		acknowledgementStatementId = acknowledgementStatementRepository
				.save(new AcknowledgementStatement().statement(DEFAULT_ACKNOWLEDGEMENT_STATEMENT));
		acknowledgementStatus.acknowledgementStatementId(acknowledgementStatementId);
	}
	
	@Test
	@Transactional
	public void createListOfAcknowledgementStatusForMerchant() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();
		List<AcknowledgementStatus> aocLegalExceptionDetails = createListOfEntity();
		// Create List of new AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/list/acknowledgement-statuses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate + 2);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}
	
	@Test
	@Transactional
	public void createListOfAcknowledgementStatusForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();
		List<AcknowledgementStatus> aocLegalExceptionDetails = createListOfEntity();
		// Create List of new AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/list/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate + 2);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}
	
	@Test
	@Transactional
	public void updateListOfAcknowledgementStatusForMerchant() throws Exception {
		//Creating list of 2 AcknowledgementStatus in database
		acknowledgementStatusRepository.save(createListOfEntity());

		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();
		
		List<AcknowledgementStatus> aocLegalExceptionDetails = new ArrayList<>();
		aocLegalExceptionDetails.add(acknowledgementStatus);
		
		//Clear and update with new list of AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/list/acknowledgement-statuses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate - 1);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}
	
	@Test
	@Transactional
	public void updateListOfAcknowledgementStatusForServiceProvider() throws Exception {
		//Creating list of 2 AcknowledgementStatus in database
		acknowledgementStatusRepository.save(createListOfEntity());

		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();
		
		List<AcknowledgementStatus> aocLegalExceptionDetails = new ArrayList<>();
		aocLegalExceptionDetails.add(acknowledgementStatus);
		
		//Clear and update with new list of AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/list/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(aocLegalExceptionDetails)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate - 1);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatusForMerchant() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate + 1);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatusForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate + 1);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(DEFAULT_STATUS_RESPONSE);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatusWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus with an existing ID
		acknowledgementStatus.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isBadRequest());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createAcknowledgementStatusWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus with an existing ID
		acknowledgementStatus.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isBadRequest());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkAcknowledgementStatementIdIsRequiredForMerchant() throws Exception {
		// set the field null
		acknowledgementStatus.setAcknowledgementStatementId(null);

		// Create the AcknowledgementStatus, which fails.

		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAcknowledgementStatementIdIsRequiredForServiceProvider() throws Exception {
		// set the field null
		acknowledgementStatus.setAcknowledgementStatementId(null);

		// Create the AcknowledgementStatus, which fails.

		restAcknowledgementStatusMockMvc
				.perform(post("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllAcknowledgementStatusesForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatusRepository.saveAndFlush(acknowledgementStatus);

		// Get all the acknowledgementStatusList
		restAcknowledgementStatusMockMvc.perform(get("/api/acknowledgement-statuses?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(acknowledgementStatus.getId().intValue())))
				.andExpect(jsonPath("$.[*].statusResponse").value(hasItem(DEFAULT_STATUS_RESPONSE.booleanValue())));
	}

	@Test
	@Transactional
	public void getAllAcknowledgementStatusesForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatusRepository.saveAndFlush(acknowledgementStatus);

		// Get all the acknowledgementStatusList
		restAcknowledgementStatusMockMvc.perform(get("/api/acknowledgement-statuses/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(acknowledgementStatus.getId().intValue())))
				.andExpect(jsonPath("$.[*].statusResponse").value(hasItem(DEFAULT_STATUS_RESPONSE.booleanValue())));
	}

	@Test
	@Transactional
	public void updateAcknowledgementStatusForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatusService.save(acknowledgementStatus);

		int databaseSizeBeforeUpdate = acknowledgementStatusRepository.findAll().size();

		// Update the acknowledgementStatus
		AcknowledgementStatus updatedAcknowledgementStatus = acknowledgementStatusRepository
				.findOne(acknowledgementStatus.getId());
		// Disconnect from session so that the updates on updatedAcknowledgementStatus
		// are not directly saved in db
		em.detach(updatedAcknowledgementStatus);
		updatedAcknowledgementStatus.statusResponse(UPDATED_STATUS_RESPONSE);

		restAcknowledgementStatusMockMvc
				.perform(put("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAcknowledgementStatus)))
				.andExpect(status().isOk());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeUpdate);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(UPDATED_STATUS_RESPONSE);
		assertThat(testAcknowledgementStatus.getAcknowledgementStatementId());
	}

	@Test
	@Transactional
	public void updateAcknowledgementStatusForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatusService.save(acknowledgementStatus);

		int databaseSizeBeforeUpdate = acknowledgementStatusRepository.findAll().size();

		// Update the acknowledgementStatus
		AcknowledgementStatus updatedAcknowledgementStatus = acknowledgementStatusRepository
				.findOne(acknowledgementStatus.getId());
		// Disconnect from session so that the updates on updatedAcknowledgementStatus
		// are not directly saved in db
		em.detach(updatedAcknowledgementStatus);
		updatedAcknowledgementStatus.statusResponse(UPDATED_STATUS_RESPONSE);

		restAcknowledgementStatusMockMvc
				.perform(put("/api/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAcknowledgementStatus)))
				.andExpect(status().isOk());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeUpdate);
		AcknowledgementStatus testAcknowledgementStatus = acknowledgementStatusList
				.get(acknowledgementStatusList.size() - 1);
		assertThat(testAcknowledgementStatus.isStatusResponse()).isEqualTo(UPDATED_STATUS_RESPONSE);
		assertThat(testAcknowledgementStatus.getAcknowledgementStatementId());
	}

	@Test
	@Transactional
	public void updateNonExistingAcknowledgementStatusForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAcknowledgementStatusMockMvc
				.perform(put("/api/acknowledgement-statuses").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingAcknowledgementStatusForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = acknowledgementStatusRepository.findAll().size();

		// Create the AcknowledgementStatus

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAcknowledgementStatusMockMvc
				.perform(put("/api/acknowledgement-statuses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(acknowledgementStatus)))
				.andExpect(status().isCreated());

		// Validate the AcknowledgementStatus in the database
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAcknowledgementStatusForMerchant() throws Exception {
		// Initialize the database
		acknowledgementStatusService.save(acknowledgementStatus);

		int databaseSizeBeforeDelete = acknowledgementStatusRepository.findAll().size();

		// Get the acknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(delete("/api/acknowledgement-statuses/{id}", acknowledgementStatus.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteAcknowledgementStatusForServiceProvider() throws Exception {
		// Initialize the database
		acknowledgementStatusService.save(acknowledgementStatus);

		int databaseSizeBeforeDelete = acknowledgementStatusRepository.findAll().size();

		// Get the acknowledgementStatus
		restAcknowledgementStatusMockMvc
				.perform(delete("/api/acknowledgement-statuses/service-provider/{id}", acknowledgementStatus.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<AcknowledgementStatus> acknowledgementStatusList = acknowledgementStatusRepository.findAll();
		assertThat(acknowledgementStatusList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(AcknowledgementStatus.class);
		AcknowledgementStatus acknowledgementStatus1 = new AcknowledgementStatus();
		acknowledgementStatus1.setId(1L);
		AcknowledgementStatus acknowledgementStatus2 = new AcknowledgementStatus();
		acknowledgementStatus2.setId(acknowledgementStatus1.getId());
		assertThat(acknowledgementStatus1).isEqualTo(acknowledgementStatus2);
		acknowledgementStatus2.setId(2L);
		assertThat(acknowledgementStatus1).isNotEqualTo(acknowledgementStatus2);
		acknowledgementStatus1.setId(null);
		assertThat(acknowledgementStatus1).isNotEqualTo(acknowledgementStatus2);
	}
}
