package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.AssessmentInfo;
import com.nollysoft.repository.AssessmentInfoRepository;
import com.nollysoft.service.AssessmentInfoService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AssessmentInfoResource REST controller.
 *
 * @see AssessmentInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AssessmentInfoResourceIntTest {

	private static final String DEFAULT_ASSESSMENT_TYPE = "AAAAAAAAAA";
	private static final String UPDATED_ASSESSMENT_TYPE = "BBBBBBBBBB";

	private static final LocalDate DEFAULT_START_ASSESSMENT_DATE = LocalDate
			.now(ZoneId.systemDefault());
	private static final LocalDate UPDATED_START_ASSESSMENT_DATE = LocalDate
			.now(ZoneId.systemDefault());

	private static final LocalDate DEFAULT_END_ASSESSMENT_DATE = LocalDate
			.now(ZoneId.systemDefault());
	private static final LocalDate UPDATED_END_ASSESSMENT_DATE = LocalDate
			.now(ZoneId.systemDefault());

	private static final String DEFAULT_ASSESSOR = "AAAAAAAAAA";
	private static final String UPDATED_ASSESSOR = "BBBBBBBBBB";

	@Autowired
	private AssessmentInfoRepository assessmentInfoRepository;

	@Autowired
	private AssessmentInfoService assessmentInfoService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAssessmentInfoMockMvc;

	private AssessmentInfo assessmentInfo;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AssessmentInfoResource assessmentInfoResource = new AssessmentInfoResource(
				assessmentInfoService);
		this.restAssessmentInfoMockMvc = MockMvcBuilders
				.standaloneSetup(assessmentInfoResource)
				.setCustomArgumentResolvers(pageableArgumentResolver)
				.setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static AssessmentInfo createEntity(EntityManager em) {
		AssessmentInfo assessmentInfo = new AssessmentInfo()
				.assessmentStartDate(DEFAULT_START_ASSESSMENT_DATE)
				.assessmentType(DEFAULT_ASSESSMENT_TYPE)
				.assessmentEndDate(DEFAULT_END_ASSESSMENT_DATE)
				.assessor(DEFAULT_ASSESSOR);
		return assessmentInfo;
	}

	@Before
	public void initTest() {
		assessmentInfo = createEntity(em);
	}

	@Test
	@Transactional
	public void createAssessmentInfoForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo
		restAssessmentInfoMockMvc.perform(
				post("/api/assessment-infos/service-provider").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isCreated());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeCreate + 1);
		AssessmentInfo testAssessmentInfo = assessmentInfoList
				.get(assessmentInfoList.size() - 1);
		assertThat(testAssessmentInfo.getAssessmentType()).isEqualTo(
				DEFAULT_ASSESSMENT_TYPE);
		assertThat(testAssessmentInfo.getAssessmentStartDate()).isEqualTo(
				DEFAULT_START_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessmentEndDate()).isEqualTo(
				DEFAULT_END_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessor())
				.isEqualTo(DEFAULT_ASSESSOR);
	}

	@Test
	@Transactional
	public void createAssessmentInfoWithExistingIdForServiceProvider()
			throws Exception {
		int databaseSizeBeforeCreate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo with an existing ID
		assessmentInfo.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restAssessmentInfoMockMvc.perform(
				post("/api/assessment-infos/service-provider").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isBadRequest());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllAssessmentInfosForServiceProvider() throws Exception {
		// Initialize the database
		assessmentInfoRepository.saveAndFlush(assessmentInfo);

		// Get all the assessmentInfoList
		restAssessmentInfoMockMvc
				.perform(
						get("/api/assessment-infos/service-provider?sort=id,desc"))
				.andExpect(status().isOk())
				.andExpect(
						content().contentType(
								MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(
						jsonPath("$.[*].id").value(
								hasItem(assessmentInfo.getId().intValue())))
				.andExpect(
						jsonPath("$.[*].assessmentType").value(
								hasItem(DEFAULT_ASSESSMENT_TYPE.toString())))
				.andExpect(
						jsonPath("$.[*].assessmentEndDate").value(
								hasItem(DEFAULT_END_ASSESSMENT_DATE.toString())))
				.andExpect(
						jsonPath("$.[*].assessor").value(
								hasItem(DEFAULT_ASSESSOR.toString())));
	}

	@Test
	@Transactional
	public void getAssessmentInfoForServiceProvider() throws Exception {
		// Initialize the database
		assessmentInfoRepository.saveAndFlush(assessmentInfo);

		// Get the assessmentInfo
		restAssessmentInfoMockMvc
				.perform(
						get("/api/assessment-infos/service-provider/{id}",
								assessmentInfo.getId()))
				.andExpect(status().isOk())
				.andExpect(
						content().contentType(
								MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(
						jsonPath("$.id").value(
								assessmentInfo.getId().intValue()))
				.andExpect(
						jsonPath("$.assessmentType").value(
								DEFAULT_ASSESSMENT_TYPE.toString()))
				.andExpect(
						jsonPath("$.assessmentStartDate").value(
								DEFAULT_START_ASSESSMENT_DATE.toString()))
				.andExpect(
						jsonPath("$.assessmentEndDate").value(
								DEFAULT_END_ASSESSMENT_DATE.toString()))
				.andExpect(
						jsonPath("$.assessor").value(
								DEFAULT_ASSESSOR.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingAssessmentInfoForServiceProvider()
			throws Exception {
		// Get the assessmentInfo
		restAssessmentInfoMockMvc.perform(
				get("/api/assessment-infos/service-provider/{id}",
						Long.MAX_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateAssessmentInfoForServiceProvider() throws Exception {
		// Initialize the database
		assessmentInfoService.save(assessmentInfo);

		int databaseSizeBeforeUpdate = assessmentInfoRepository.findAll()
				.size();

		// Update the assessmentInfo
		AssessmentInfo updatedAssessmentInfo = assessmentInfoRepository
				.findOne(assessmentInfo.getId());
		// Disconnect from session so that the updates on updatedAssessmentInfo
		// are not directly saved in db
		em.detach(updatedAssessmentInfo);
		updatedAssessmentInfo.assessmentType(UPDATED_ASSESSMENT_TYPE)
				.assessmentStartDate(UPDATED_START_ASSESSMENT_DATE)
				.assessmentEndDate(UPDATED_END_ASSESSMENT_DATE)
				.assessor(UPDATED_ASSESSOR);

		restAssessmentInfoMockMvc
				.perform(
						put("/api/assessment-infos/service-provider")
								.contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(
										TestUtil.convertObjectToJsonBytes(updatedAssessmentInfo)))
				.andExpect(status().isOk());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeUpdate);
		AssessmentInfo testAssessmentInfo = assessmentInfoList
				.get(assessmentInfoList.size() - 1);
		assertThat(testAssessmentInfo.getAssessmentType()).isEqualTo(
				UPDATED_ASSESSMENT_TYPE);
		assertThat(testAssessmentInfo.getAssessmentEndDate()).isEqualTo(
				UPDATED_START_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessmentEndDate()).isEqualTo(
				UPDATED_END_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessor())
				.isEqualTo(UPDATED_ASSESSOR);
	}

	@Test
	@Transactional
	public void updateNonExistingAssessmentInfoForServiceProvider()
			throws Exception {
		int databaseSizeBeforeUpdate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restAssessmentInfoMockMvc.perform(
				put("/api/assessment-infos/service-provider").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isCreated());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAssessmentInfoForServiceProvider() throws Exception {
		// Initialize the database
		assessmentInfoService.save(assessmentInfo);

		int databaseSizeBeforeDelete = assessmentInfoRepository.findAll()
				.size();

		// Get the assessmentInfo
		restAssessmentInfoMockMvc.perform(
				delete("/api/assessment-infos/service-provider/{id}",
						assessmentInfo.getId()).accept(
						TestUtil.APPLICATION_JSON_UTF8)).andExpect(
				status().isOk());

		// Validate the database is empty
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void createAssessmentInfoForMerchant() throws Exception {
		int databaseSizeBeforeCreate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo
		restAssessmentInfoMockMvc.perform(
				post("/api/assessment-infos").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isCreated());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeCreate + 1);
		AssessmentInfo testAssessmentInfo = assessmentInfoList
				.get(assessmentInfoList.size() - 1);
		assertThat(testAssessmentInfo.getAssessmentType()).isEqualTo(
				DEFAULT_ASSESSMENT_TYPE);
		assertThat(testAssessmentInfo.getAssessmentEndDate()).isEqualTo(
				DEFAULT_END_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessor())
				.isEqualTo(DEFAULT_ASSESSOR);
	}

	@Test
	@Transactional
	public void createAssessmentInfoWithExistingIdForMerchant()
			throws Exception {
		int databaseSizeBeforeCreate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo with an existing ID
		assessmentInfo.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restAssessmentInfoMockMvc.perform(
				post("/api/assessment-infos").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isBadRequest());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllAssessmentInfosForMerchant() throws Exception {
		// Initialize the database
		assessmentInfoRepository.saveAndFlush(assessmentInfo);

		// Get all the assessmentInfoList
		restAssessmentInfoMockMvc
				.perform(get("/api/assessment-infos?sort=id,desc"))
				.andExpect(status().isOk())
				.andExpect(
						content().contentType(
								MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(
						jsonPath("$.[*].id").value(
								hasItem(assessmentInfo.getId().intValue())))
				.andExpect(
						jsonPath("$.[*].assessmentType").value(
								hasItem(DEFAULT_ASSESSMENT_TYPE.toString())))
				.andExpect(
						jsonPath("$.[*].assessmentStartDate").value(
								hasItem(DEFAULT_START_ASSESSMENT_DATE.toString())))
				.andExpect(
						jsonPath("$.[*].assessmentEndDate").value(
								hasItem(DEFAULT_END_ASSESSMENT_DATE.toString())))
				.andExpect(
						jsonPath("$.[*].assessor").value(
								hasItem(DEFAULT_ASSESSOR.toString())));
	}

	@Test
	@Transactional
	public void getAssessmentInfoForMerchant() throws Exception {
		// Initialize the database
		assessmentInfoRepository.saveAndFlush(assessmentInfo);

		// Get the assessmentInfo
		restAssessmentInfoMockMvc
				.perform(
						get("/api/assessment-infos/{id}",
								assessmentInfo.getId()))
				.andExpect(status().isOk())
				.andExpect(
						content().contentType(
								MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(
						jsonPath("$.id").value(
								assessmentInfo.getId().intValue()))
				.andExpect(
						jsonPath("$.assessmentType").value(
								DEFAULT_ASSESSMENT_TYPE.toString()))
				.andExpect(
						jsonPath("$.assessmentEndDate").value(
								DEFAULT_END_ASSESSMENT_DATE.toString()))
				.andExpect(
						jsonPath("$.assessor").value(
								DEFAULT_ASSESSOR.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingAssessmentInfoForMerchant() throws Exception {
		// Get the assessmentInfo
		restAssessmentInfoMockMvc.perform(
				get("/api/assessment-infos/{id}", Long.MAX_VALUE)).andExpect(
				status().isNotFound());
	}

	@Test
	@Transactional
	public void updateAssessmentInfoForMerchant() throws Exception {
		// Initialize the database
		assessmentInfoService.save(assessmentInfo);

		int databaseSizeBeforeUpdate = assessmentInfoRepository.findAll()
				.size();

		// Update the assessmentInfo
		AssessmentInfo updatedAssessmentInfo = assessmentInfoRepository
				.findOne(assessmentInfo.getId());
		// Disconnect from session so that the updates on updatedAssessmentInfo
		// are not directly saved in db
		em.detach(updatedAssessmentInfo);
		updatedAssessmentInfo.assessmentType(UPDATED_ASSESSMENT_TYPE)
				.assessmentEndDate(UPDATED_END_ASSESSMENT_DATE)
				.assessor(UPDATED_ASSESSOR);

		restAssessmentInfoMockMvc
				.perform(
						put("/api/assessment-infos")
								.contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(
										TestUtil.convertObjectToJsonBytes(updatedAssessmentInfo)))
				.andExpect(status().isOk());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeUpdate);
		AssessmentInfo testAssessmentInfo = assessmentInfoList
				.get(assessmentInfoList.size() - 1);
		assertThat(testAssessmentInfo.getAssessmentType()).isEqualTo(
				UPDATED_ASSESSMENT_TYPE);
		assertThat(testAssessmentInfo.getAssessmentEndDate()).isEqualTo(
				UPDATED_END_ASSESSMENT_DATE);
		assertThat(testAssessmentInfo.getAssessor())
				.isEqualTo(UPDATED_ASSESSOR);
	}

	@Test
	@Transactional
	public void updateNonExistingAssessmentInfoForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = assessmentInfoRepository.findAll()
				.size();

		// Create the AssessmentInfo

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restAssessmentInfoMockMvc.perform(
				put("/api/assessment-infos").contentType(
						TestUtil.APPLICATION_JSON_UTF8).content(
						TestUtil.convertObjectToJsonBytes(assessmentInfo)))
				.andExpect(status().isCreated());

		// Validate the AssessmentInfo in the database
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAssessmentInfoForMerchant() throws Exception {
		// Initialize the database
		assessmentInfoService.save(assessmentInfo);

		int databaseSizeBeforeDelete = assessmentInfoRepository.findAll()
				.size();

		// Get the assessmentInfo
		restAssessmentInfoMockMvc.perform(
				delete("/api/assessment-infos/{id}", assessmentInfo.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(
				status().isOk());

		// Validate the database is empty
		List<AssessmentInfo> assessmentInfoList = assessmentInfoRepository
				.findAll();
		assertThat(assessmentInfoList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(AssessmentInfo.class);
		AssessmentInfo assessmentInfo1 = new AssessmentInfo();
		assessmentInfo1.setId(1L);
		AssessmentInfo assessmentInfo2 = new AssessmentInfo();
		assessmentInfo2.setId(assessmentInfo1.getId());
		assertThat(assessmentInfo1).isEqualTo(assessmentInfo2);
		assessmentInfo2.setId(2L);
		assertThat(assessmentInfo1).isNotEqualTo(assessmentInfo2);
		assessmentInfo1.setId(null);
		assertThat(assessmentInfo1).isNotEqualTo(assessmentInfo2);
	}
}
