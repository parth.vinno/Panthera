package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.AssessmentType;
import com.nollysoft.repository.AssessmentTypeRepository;
import com.nollysoft.service.AssessmentTypeService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AssessmentTypeResource REST controller.
 *
 * @see AssessmentTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AssessmentTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private AssessmentTypeRepository assessmentTypeRepository;

    @Autowired
    private AssessmentTypeService assessmentTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAssessmentTypeMockMvc;

    private AssessmentType assessmentType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AssessmentTypeResource assessmentTypeResource = new AssessmentTypeResource(assessmentTypeService);
        this.restAssessmentTypeMockMvc = MockMvcBuilders.standaloneSetup(assessmentTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssessmentType createEntity(EntityManager em) {
        AssessmentType assessmentType = new AssessmentType()
            .name(DEFAULT_NAME);
        return assessmentType;
    }

    @Before
    public void initTest() {
        assessmentType = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssessmentType() throws Exception {
        int databaseSizeBeforeCreate = assessmentTypeRepository.findAll().size();

        // Create the AssessmentType
        restAssessmentTypeMockMvc.perform(post("/api/assessment-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessmentType)))
            .andExpect(status().isCreated());

        // Validate the AssessmentType in the database
        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeCreate + 1);
        AssessmentType testAssessmentType = assessmentTypeList.get(assessmentTypeList.size() - 1);
        assertThat(testAssessmentType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAssessmentTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assessmentTypeRepository.findAll().size();

        // Create the AssessmentType with an existing ID
        assessmentType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssessmentTypeMockMvc.perform(post("/api/assessment-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessmentType)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = assessmentTypeRepository.findAll().size();
        // set the field null
        assessmentType.setName(null);

        // Create the AssessmentType, which fails.

        restAssessmentTypeMockMvc.perform(post("/api/assessment-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessmentType)))
            .andExpect(status().isBadRequest());

        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssessmentTypes() throws Exception {
        // Initialize the database
        assessmentTypeRepository.saveAndFlush(assessmentType);

        // Get all the assessmentTypeList
        restAssessmentTypeMockMvc.perform(get("/api/assessment-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assessmentType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAssessmentType() throws Exception {
        // Initialize the database
        assessmentTypeRepository.saveAndFlush(assessmentType);

        // Get the assessmentType
        restAssessmentTypeMockMvc.perform(get("/api/assessment-types/{id}", assessmentType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(assessmentType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAssessmentType() throws Exception {
        // Get the assessmentType
        restAssessmentTypeMockMvc.perform(get("/api/assessment-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssessmentType() throws Exception {
        // Initialize the database
        assessmentTypeService.save(assessmentType);

        int databaseSizeBeforeUpdate = assessmentTypeRepository.findAll().size();

        // Update the assessmentType
        AssessmentType updatedAssessmentType = assessmentTypeRepository.findOne(assessmentType.getId());
        updatedAssessmentType
            .name(UPDATED_NAME);

        restAssessmentTypeMockMvc.perform(put("/api/assessment-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssessmentType)))
            .andExpect(status().isOk());

        // Validate the AssessmentType in the database
        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeUpdate);
        AssessmentType testAssessmentType = assessmentTypeList.get(assessmentTypeList.size() - 1);
        assertThat(testAssessmentType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingAssessmentType() throws Exception {
        int databaseSizeBeforeUpdate = assessmentTypeRepository.findAll().size();

        // Create the AssessmentType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAssessmentTypeMockMvc.perform(put("/api/assessment-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessmentType)))
            .andExpect(status().isCreated());

        // Validate the AssessmentType in the database
        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAssessmentType() throws Exception {
        // Initialize the database
        assessmentTypeService.save(assessmentType);

        int databaseSizeBeforeDelete = assessmentTypeRepository.findAll().size();

        // Get the assessmentType
        restAssessmentTypeMockMvc.perform(delete("/api/assessment-types/{id}", assessmentType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AssessmentType> assessmentTypeList = assessmentTypeRepository.findAll();
        assertThat(assessmentTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssessmentType.class);
        AssessmentType assessmentType1 = new AssessmentType();
        assessmentType1.setId(1L);
        AssessmentType assessmentType2 = new AssessmentType();
        assessmentType2.setId(assessmentType1.getId());
        assertThat(assessmentType1).isEqualTo(assessmentType2);
        assessmentType2.setId(2L);
        assertThat(assessmentType1).isNotEqualTo(assessmentType2);
        assessmentType1.setId(null);
        assertThat(assessmentType1).isNotEqualTo(assessmentType2);
    }
}
