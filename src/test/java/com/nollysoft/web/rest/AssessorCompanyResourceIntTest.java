package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.AssessorCompany;
import com.nollysoft.repository.AssessorCompanyRepository;
import com.nollysoft.service.AssessorCompanyService;
import com.nollysoft.service.OrganizationSummaryStatusService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AssessorCompanyResource REST controller.
 *
 * @see AssessorCompanyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AssessorCompanyResourceIntTest {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LEAD_QSA_CONTACT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LEAD_QSA_CONTACT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    @Autowired
    private AssessorCompanyRepository assessorCompanyRepository;

    @Autowired
    private AssessorCompanyService assessorCompanyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;
    
    @Autowired
    private OrganizationSummaryStatusService organizationSummaryStatusService;

    @Autowired
    private EntityManager em;

    private MockMvc restAssessorCompanyMockMvc;

    private AssessorCompany assessorCompany;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        AssessorCompanyResource assessorCompanyResource = new AssessorCompanyResource(assessorCompanyService, organizationSummaryStatusService);
        this.restAssessorCompanyMockMvc = MockMvcBuilders.standaloneSetup(assessorCompanyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssessorCompany createEntity(EntityManager em) {
        AssessorCompany assessorCompany = new AssessorCompany()
            .companyName(DEFAULT_COMPANY_NAME)
            .leadQsaContactName(DEFAULT_LEAD_QSA_CONTACT_NAME)
            .title(DEFAULT_TITLE)
            .telephone(DEFAULT_TELEPHONE)
            .email(DEFAULT_EMAIL)
            .url(DEFAULT_URL);
        return assessorCompany;
    }

    @Before
    public void initTest() {
        assessorCompany = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssessorCompany() throws Exception {
        int databaseSizeBeforeCreate = assessorCompanyRepository.findAll().size();

        // Create the AssessorCompany
        restAssessorCompanyMockMvc.perform(post("/api/assessor-companies/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessorCompany)))
            .andExpect(status().isCreated());

        // Validate the AssessorCompany in the database
        List<AssessorCompany> assessorCompanyList = assessorCompanyRepository.findAll();
        
        assertThat(assessorCompanyList).hasSize(databaseSizeBeforeCreate + 1);
        AssessorCompany testAssessorCompany = assessorCompanyList.get(assessorCompanyList.size() - 1);
        assertThat(testAssessorCompany.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testAssessorCompany.getLeadQsaContactName()).isEqualTo(DEFAULT_LEAD_QSA_CONTACT_NAME);
        assertThat(testAssessorCompany.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testAssessorCompany.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testAssessorCompany.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAssessorCompany.getUrl()).isEqualTo(DEFAULT_URL);
    }

    @Test
    @Transactional
    public void createAssessorCompanyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assessorCompanyRepository.findAll().size();

        // Create the AssessorCompany with an existing ID
        assessorCompany.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssessorCompanyMockMvc.perform(post("/api/assessor-companies/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessorCompany)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AssessorCompany> assessorCompanyList = assessorCompanyRepository.findAll();
        assertThat(assessorCompanyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAssessorCompanies() throws Exception {
        // Initialize the database
        assessorCompanyRepository.saveAndFlush(assessorCompany);

        // Get all the assessorCompanyList
        restAssessorCompanyMockMvc.perform(get("/api/assessor-companies/service-provider?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assessorCompany.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME.toString())))
            .andExpect(jsonPath("$.[*].leadQsaContactName").value(hasItem(DEFAULT_LEAD_QSA_CONTACT_NAME.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
        	.andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }

    @Test
    @Transactional
    public void getAssessorCompany() throws Exception {
        // Initialize the database
        assessorCompanyRepository.saveAndFlush(assessorCompany);

        // Get the assessorCompany
        restAssessorCompanyMockMvc.perform(get("/api/assessor-companies/service-provider/{id}", assessorCompany.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(assessorCompany.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME.toString()))
            .andExpect(jsonPath("$.leadQsaContactName").value(DEFAULT_LEAD_QSA_CONTACT_NAME.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()));;
    }

    @Test
    @Transactional
    public void getNonExistingAssessorCompany() throws Exception {
        // Get the assessorCompany
        restAssessorCompanyMockMvc.perform(get("/api/assessor-companies/service-provider/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssessorCompany() throws Exception {
        // Initialize the database
        assessorCompanyService.save(assessorCompany);

        int databaseSizeBeforeUpdate = assessorCompanyRepository.findAll().size();

        // Update the assessorCompany
        AssessorCompany updatedAssessorCompany = assessorCompanyRepository.findOne(assessorCompany.getId());
        updatedAssessorCompany
            .companyName(UPDATED_COMPANY_NAME)
            .leadQsaContactName(UPDATED_LEAD_QSA_CONTACT_NAME)
            .title(UPDATED_TITLE)
            .telephone(UPDATED_TELEPHONE)
            .email(UPDATED_EMAIL)
            .url(UPDATED_URL);

        restAssessorCompanyMockMvc.perform(put("/api/assessor-companies/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssessorCompany)))
            .andExpect(status().isOk());

        // Validate the AssessorCompany in the database
        List<AssessorCompany> assessorCompanyList = assessorCompanyRepository.findAll();
        assertThat(assessorCompanyList).hasSize(databaseSizeBeforeUpdate);
        AssessorCompany testAssessorCompany = assessorCompanyList.get(assessorCompanyList.size() - 1);
        assertThat(testAssessorCompany.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testAssessorCompany.getLeadQsaContactName()).isEqualTo(UPDATED_LEAD_QSA_CONTACT_NAME);
        assertThat(testAssessorCompany.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testAssessorCompany.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testAssessorCompany.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAssessorCompany.getUrl()).isEqualTo(UPDATED_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingAssessorCompany() throws Exception {
        int databaseSizeBeforeUpdate = assessorCompanyRepository.findAll().size();

        // Create the AssessorCompany

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAssessorCompanyMockMvc.perform(put("/api/assessor-companies/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(assessorCompany)))
            .andExpect(status().isCreated());

        // Validate the AssessorCompany in the database
        List<AssessorCompany> assessorCompanyList = assessorCompanyRepository.findAll();
        assertThat(assessorCompanyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAssessorCompany() throws Exception {
        // Initialize the database
        assessorCompanyService.save(assessorCompany);

        int databaseSizeBeforeDelete = assessorCompanyRepository.findAll().size();

        // Get the assessorCompany
        restAssessorCompanyMockMvc.perform(delete("/api/assessor-companies/service-provider/{id}", assessorCompany.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AssessorCompany> assessorCompanyList = assessorCompanyRepository.findAll();
        assertThat(assessorCompanyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssessorCompany.class);
        AssessorCompany assessorCompany1 = new AssessorCompany();
        assessorCompany1.setId(1L);
        AssessorCompany assessorCompany2 = new AssessorCompany();
        assessorCompany2.setId(assessorCompany1.getId());
        assertThat(assessorCompany1).isEqualTo(assessorCompany2);
        assessorCompany2.setId(2L);
        assertThat(assessorCompany1).isNotEqualTo(assessorCompany2);
        assessorCompany1.setId(null);
        assertThat(assessorCompany1).isNotEqualTo(assessorCompany2);
    }
}
