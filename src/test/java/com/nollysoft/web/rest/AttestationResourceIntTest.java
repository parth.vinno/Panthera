package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.Attestation;
import com.nollysoft.repository.AttestationRepository;
import com.nollysoft.service.AttestationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AttestationResource REST controller.
 *
 * @see AttestationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class AttestationResourceIntTest {

	private static final String DEFAULT_ATTESTOR_TITLE = "Test Default Title";
	private static final String UPDATED_ATTESTOR_TITLE = "Test Updated Title";

	private static final String DEFAULT_ATTESTOR_NAME = "Test Default Name";
	private static final String UPDATED_ATTESTOR_NAME = "Test Updated Name";

	private static final String DEFAULT_ATTESTOR_SIGNATURE = "Test Default Signature";
	private static final String UPDATED_ATTESTOR_SIGNATURE = "Test Updated Signature";

	@Autowired
	private AttestationRepository attestationRepository;

	@Autowired
	private AttestationService attestationService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restAttestationMockMvc;

	private Attestation attestation;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final AttestationResource attestationResource = new AttestationResource(attestationService);
		this.restAttestationMockMvc = MockMvcBuilders.standaloneSetup(attestationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static Attestation createEntity(EntityManager em) {
		Attestation attestation = new Attestation().attestorTitle(DEFAULT_ATTESTOR_TITLE)
				.attestorName(DEFAULT_ATTESTOR_NAME).attestorSignature(DEFAULT_ATTESTOR_SIGNATURE);
		return attestation;
	}

	@Before
	public void initTest() {
		attestation = createEntity(em);
	}

	@Test
	@Transactional
	public void createAttestationForMerchant() throws Exception {
		int databaseSizeBeforeCreate = attestationRepository.findAll().size();

		// Create the Attestation
		restAttestationMockMvc.perform(post("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(attestation))).andExpect(status().isCreated());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeCreate + 1);
		Attestation testAttestation = attestationList.get(attestationList.size() - 1);
		assertThat(testAttestation.getAttestorTitle()).isEqualTo(DEFAULT_ATTESTOR_TITLE);
		assertThat(testAttestation.getAttestorName()).isEqualTo(DEFAULT_ATTESTOR_NAME);
		assertThat(testAttestation.getAttestorSignature()).isEqualTo(DEFAULT_ATTESTOR_SIGNATURE);
	}

	@Test
	@Transactional
	public void createAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = attestationRepository.findAll().size();

		// Create the Attestation
		restAttestationMockMvc.perform(post("/api/attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isCreated());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeCreate + 1);
		Attestation testAttestation = attestationList.get(attestationList.size() - 1);
		assertThat(testAttestation.getAttestorTitle()).isEqualTo(DEFAULT_ATTESTOR_TITLE);
		assertThat(testAttestation.getAttestorName()).isEqualTo(DEFAULT_ATTESTOR_NAME);
		assertThat(testAttestation.getAttestorSignature()).isEqualTo(DEFAULT_ATTESTOR_SIGNATURE);
	}

	@Test
	@Transactional
	public void createAttestationWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = attestationRepository.findAll().size();

		// Create the Attestation with an existing ID
		attestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAttestationMockMvc.perform(post("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(attestation))).andExpect(status().isBadRequest());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createAttestationWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = attestationRepository.findAll().size();

		// Create the Attestation with an existing ID
		attestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restAttestationMockMvc.perform(post("/api/attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isBadRequest());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkAttestorTitleIsRequiredForMerchant() throws Exception {
		// set the field null
		attestation.setAttestorTitle(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAttestorTitleIsRequiredForServiceProvider() throws Exception {
		// set the field null
		attestation.setAttestorTitle(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAttestorNameIsRequiredForMerchant() throws Exception {
		// set the field null
		attestation.setAttestorName(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAttestorNameIsRequiredForServiceProvider() throws Exception {
		// set the field null
		attestation.setAttestorName(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAttestorSignatureIsRequiredForMerchant() throws Exception {
		// set the field null
		attestation.setAttestorSignature(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkAttestorSignatureIsRequiredForServiceProvider() throws Exception {
		// set the field null
		attestation.setAttestorSignature(null);

		// Create the Attestation, which fails.

		restAttestationMockMvc
				.perform(post("/api/attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllAttestationsForMerchant() throws Exception {
		// Initialize the database
		attestationRepository.saveAndFlush(attestation);

		// Get all the attestationList
		restAttestationMockMvc.perform(get("/api/attestations?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(attestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].attestorTitle").value(hasItem(DEFAULT_ATTESTOR_TITLE.toString())))
				.andExpect(jsonPath("$.[*].attestorName").value(hasItem(DEFAULT_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].attestorSignature").value(hasItem(DEFAULT_ATTESTOR_SIGNATURE.toString())));
	}

	@Test
	@Transactional
	public void getAllAttestationsForServiceProvider() throws Exception {
		// Initialize the database
		attestationRepository.saveAndFlush(attestation);

		// Get all the attestationList
		restAttestationMockMvc.perform(get("/api/attestations/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(attestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].attestorTitle").value(hasItem(DEFAULT_ATTESTOR_TITLE.toString())))
				.andExpect(jsonPath("$.[*].attestorName").value(hasItem(DEFAULT_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].attestorSignature").value(hasItem(DEFAULT_ATTESTOR_SIGNATURE.toString())));
	}

	@Test
	@Transactional
	public void updateAttestationForMerchant() throws Exception {
		// Initialize the database
		attestationService.save(attestation);

		int databaseSizeBeforeUpdate = attestationRepository.findAll().size();

		// Update the attestation
		Attestation updatedAttestation = attestationRepository.findOne(attestation.getId());
		// Disconnect from session so that the updates on updatedAttestation are not
		// directly saved in db
		em.detach(updatedAttestation);
		updatedAttestation.attestorTitle(UPDATED_ATTESTOR_TITLE).attestorName(UPDATED_ATTESTOR_NAME)
				.attestorSignature(UPDATED_ATTESTOR_SIGNATURE);

		restAttestationMockMvc.perform(put("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedAttestation))).andExpect(status().isOk());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeUpdate);
		Attestation testAttestation = attestationList.get(attestationList.size() - 1);
		assertThat(testAttestation.getAttestorTitle()).isEqualTo(UPDATED_ATTESTOR_TITLE);
		assertThat(testAttestation.getAttestorName()).isEqualTo(UPDATED_ATTESTOR_NAME);
		assertThat(testAttestation.getAttestorSignature()).isEqualTo(UPDATED_ATTESTOR_SIGNATURE);
	}

	@Test
	@Transactional
	public void updateAttestationForServiceProvider() throws Exception {
		// Initialize the database
		attestationService.save(attestation);

		int databaseSizeBeforeUpdate = attestationRepository.findAll().size();

		// Update the attestation
		Attestation updatedAttestation = attestationRepository.findOne(attestation.getId());
		// Disconnect from session so that the updates on updatedAttestation are not
		// directly saved in db
		em.detach(updatedAttestation);
		updatedAttestation.attestorTitle(UPDATED_ATTESTOR_TITLE).attestorName(UPDATED_ATTESTOR_NAME)
				.attestorSignature(UPDATED_ATTESTOR_SIGNATURE);

		restAttestationMockMvc
				.perform(put("/api/attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedAttestation)))
				.andExpect(status().isOk());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeUpdate);
		Attestation testAttestation = attestationList.get(attestationList.size() - 1);
		assertThat(testAttestation.getAttestorTitle()).isEqualTo(UPDATED_ATTESTOR_TITLE);
		assertThat(testAttestation.getAttestorName()).isEqualTo(UPDATED_ATTESTOR_NAME);
		assertThat(testAttestation.getAttestorSignature()).isEqualTo(UPDATED_ATTESTOR_SIGNATURE);
	}

	@Test
	@Transactional
	public void updateNonExistingAttestationForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = attestationRepository.findAll().size();

		// Create the Attestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAttestationMockMvc.perform(put("/api/attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(attestation))).andExpect(status().isCreated());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = attestationRepository.findAll().size();

		// Create the Attestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restAttestationMockMvc.perform(put("/api/attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(attestation)))
				.andExpect(status().isCreated());

		// Validate the Attestation in the database
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteAttestationForMerchant() throws Exception {
		// Initialize the database
		attestationService.save(attestation);

		int databaseSizeBeforeDelete = attestationRepository.findAll().size();

		// Get the attestation
		restAttestationMockMvc
				.perform(delete("/api/attestations/{id}", attestation.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteAttestationForServiceProvider() throws Exception {
		// Initialize the database
		attestationService.save(attestation);

		int databaseSizeBeforeDelete = attestationRepository.findAll().size();

		// Get the attestation
		restAttestationMockMvc.perform(delete("/api/attestations/service-provider/{id}", attestation.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<Attestation> attestationList = attestationRepository.findAll();
		assertThat(attestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(Attestation.class);
		Attestation attestation1 = new Attestation();
		attestation1.setId(1L);
		Attestation attestation2 = new Attestation();
		attestation2.setId(attestation1.getId());
		assertThat(attestation1).isEqualTo(attestation2);
		attestation2.setId(2L);
		assertThat(attestation1).isNotEqualTo(attestation2);
		attestation1.setId(null);
		assertThat(attestation1).isNotEqualTo(attestation2);
	}
}
