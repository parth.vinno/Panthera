package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.CompensatingControlWorksheet;
import com.nollysoft.repository.CompensatingControlWorksheetRepository;
import com.nollysoft.service.CompensatingControlWorksheetService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the CompensatingControlWorksheetResource REST controller.
 *
 * @see CompensatingControlWorksheetResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class CompensatingControlWorksheetResourceIntTest {

	private static final String DEFAULT_REQUIRED_INFORMATION = "AAAAAAAAAA";
	private static final String UPDATED_REQUIRED_INFORMATION = "BBBBBBBBBB";

	private static final String DEFAULT_WORKSHEET_TYPE = "AAAAAAAAAA";
	private static final String UPDATED_WORKSHEET_TYPE = "BBBBBBBBBB";

	@Autowired
	private CompensatingControlWorksheetRepository compensatingControlWorksheetRepository;

	@Autowired
	private CompensatingControlWorksheetService compensatingControlWorksheetService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restCompensatingControlWorksheetMockMvc;

	private CompensatingControlWorksheet compensatingControlWorksheet;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		CompensatingControlWorksheetResource compensatingControlWorksheetResource = new CompensatingControlWorksheetResource(
				compensatingControlWorksheetService);
		this.restCompensatingControlWorksheetMockMvc = MockMvcBuilders
				.standaloneSetup(compensatingControlWorksheetResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static CompensatingControlWorksheet createEntity(EntityManager em) {
		CompensatingControlWorksheet compensatingControlWorksheet = new CompensatingControlWorksheet()
				.requiredInformation(DEFAULT_REQUIRED_INFORMATION).compensatingControlType(DEFAULT_WORKSHEET_TYPE);
		return compensatingControlWorksheet;
	}

	@Before
	public void initTest() {
		compensatingControlWorksheet = createEntity(em);
	}

	@Test
	@Transactional
	public void createCompensatingControlWorksheet() throws Exception {
		int databaseSizeBeforeCreate = compensatingControlWorksheetRepository.findAll().size();

		// Create the CompensatingControlWorksheet
		restCompensatingControlWorksheetMockMvc
				.perform(post("/api/compensating-control-worksheets").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheet)))
				.andExpect(status().isCreated());

		// Validate the CompensatingControlWorksheet in the database
		List<CompensatingControlWorksheet> compensatingControlWorksheetList = compensatingControlWorksheetRepository
				.findAll();
		assertThat(compensatingControlWorksheetList).hasSize(databaseSizeBeforeCreate + 1);
		CompensatingControlWorksheet testCompensatingControlWorksheet = compensatingControlWorksheetList
				.get(compensatingControlWorksheetList.size() - 1);
		assertThat(testCompensatingControlWorksheet.getRequiredInformation()).isEqualTo(DEFAULT_REQUIRED_INFORMATION);
		assertThat(testCompensatingControlWorksheet.getCompensatingControlType()).isEqualTo(DEFAULT_WORKSHEET_TYPE);
	}

	@Test
	@Transactional
	public void createCompensatingControlWorksheetWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = compensatingControlWorksheetRepository.findAll().size();

		// Create the CompensatingControlWorksheet with an existing ID
		compensatingControlWorksheet.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restCompensatingControlWorksheetMockMvc
				.perform(post("/api/compensating-control-worksheets").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheet)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<CompensatingControlWorksheet> compensatingControlWorksheetList = compensatingControlWorksheetRepository
				.findAll();
		assertThat(compensatingControlWorksheetList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllCompensatingControlWorksheets() throws Exception {
		// Initialize the database
		compensatingControlWorksheetRepository.saveAndFlush(compensatingControlWorksheet);

		// Get all the compensatingControlWorksheetList
		restCompensatingControlWorksheetMockMvc.perform(get("/api/compensating-control-worksheets?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(compensatingControlWorksheet.getId().intValue())))
				.andExpect(
						jsonPath("$.[*].requiredInformation").value(hasItem(DEFAULT_REQUIRED_INFORMATION.toString())))
				.andExpect(jsonPath("$.[*].compensatingControlType").value(hasItem(DEFAULT_WORKSHEET_TYPE.toString())));
	}

	@Test
	@Transactional
	public void getCompensatingControlWorksheet() throws Exception {
		// Initialize the database
		compensatingControlWorksheetRepository.saveAndFlush(compensatingControlWorksheet);

		// Get the compensatingControlWorksheet
		restCompensatingControlWorksheetMockMvc
				.perform(get("/api/compensating-control-worksheets/{id}", compensatingControlWorksheet.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(compensatingControlWorksheet.getId().intValue()))
				.andExpect(jsonPath("$.requiredInformation").value(DEFAULT_REQUIRED_INFORMATION.toString()))
				.andExpect(jsonPath("$.compensatingControlType").value(DEFAULT_WORKSHEET_TYPE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingCompensatingControlWorksheet() throws Exception {
		// Get the compensatingControlWorksheet
		restCompensatingControlWorksheetMockMvc
				.perform(get("/api/compensating-control-worksheets/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateCompensatingControlWorksheet() throws Exception {
		// Initialize the database
		compensatingControlWorksheetService.save(compensatingControlWorksheet);

		int databaseSizeBeforeUpdate = compensatingControlWorksheetRepository.findAll().size();

		// Update the compensatingControlWorksheet
		CompensatingControlWorksheet updatedCompensatingControlWorksheet = compensatingControlWorksheetRepository
				.findOne(compensatingControlWorksheet.getId());
		updatedCompensatingControlWorksheet.requiredInformation(UPDATED_REQUIRED_INFORMATION)
				.compensatingControlType(UPDATED_WORKSHEET_TYPE);

		restCompensatingControlWorksheetMockMvc
				.perform(put("/api/compensating-control-worksheets").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedCompensatingControlWorksheet)))
				.andExpect(status().isOk());

		// Validate the CompensatingControlWorksheet in the database
		List<CompensatingControlWorksheet> compensatingControlWorksheetList = compensatingControlWorksheetRepository
				.findAll();
		assertThat(compensatingControlWorksheetList).hasSize(databaseSizeBeforeUpdate);
		CompensatingControlWorksheet testCompensatingControlWorksheet = compensatingControlWorksheetList
				.get(compensatingControlWorksheetList.size() - 1);
		assertThat(testCompensatingControlWorksheet.getRequiredInformation()).isEqualTo(UPDATED_REQUIRED_INFORMATION);
		assertThat(testCompensatingControlWorksheet.getCompensatingControlType()).isEqualTo(UPDATED_WORKSHEET_TYPE);
	}

	@Test
	@Transactional
	public void updateNonExistingCompensatingControlWorksheet() throws Exception {
		int databaseSizeBeforeUpdate = compensatingControlWorksheetRepository.findAll().size();

		// Create the CompensatingControlWorksheet

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restCompensatingControlWorksheetMockMvc
				.perform(put("/api/compensating-control-worksheets").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheet)))
				.andExpect(status().isCreated());

		// Validate the CompensatingControlWorksheet in the database
		List<CompensatingControlWorksheet> compensatingControlWorksheetList = compensatingControlWorksheetRepository
				.findAll();
		assertThat(compensatingControlWorksheetList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteCompensatingControlWorksheet() throws Exception {
		// Initialize the database
		compensatingControlWorksheetService.save(compensatingControlWorksheet);

		int databaseSizeBeforeDelete = compensatingControlWorksheetRepository.findAll().size();

		// Get the compensatingControlWorksheet
		restCompensatingControlWorksheetMockMvc
				.perform(delete("/api/compensating-control-worksheets/{id}", compensatingControlWorksheet.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<CompensatingControlWorksheet> compensatingControlWorksheetList = compensatingControlWorksheetRepository
				.findAll();
		assertThat(compensatingControlWorksheetList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(CompensatingControlWorksheet.class);
		CompensatingControlWorksheet compensatingControlWorksheet1 = new CompensatingControlWorksheet();
		compensatingControlWorksheet1.setId(1L);
		CompensatingControlWorksheet compensatingControlWorksheet2 = new CompensatingControlWorksheet();
		compensatingControlWorksheet2.setId(compensatingControlWorksheet1.getId());
		assertThat(compensatingControlWorksheet1).isEqualTo(compensatingControlWorksheet2);
		compensatingControlWorksheet2.setId(2L);
		assertThat(compensatingControlWorksheet1).isNotEqualTo(compensatingControlWorksheet2);
		compensatingControlWorksheet1.setId(null);
		assertThat(compensatingControlWorksheet1).isNotEqualTo(compensatingControlWorksheet2);
	}
}
