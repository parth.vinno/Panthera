package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.CompensatingControlWorksheet;
import com.nollysoft.domain.CompensatingControlWorksheetResponse;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.CompensatingControlWorksheetResponseRepository;
import com.nollysoft.service.CompensatingControlWorksheetResponseService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the CompensatingControlWorksheetResponseResource REST
 * controller.
 *
 * @see CompensatingControlWorksheetResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class CompensatingControlWorksheetResponseResourceIntTest {

	private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
	private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

	@Autowired
	private CompensatingControlWorksheetResponseRepository compensatingControlWorksheetResponseRepository;

	@Autowired
	private CompensatingControlWorksheetResponseService compensatingControlWorksheetResponseService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restCompensatingControlWorksheetResponseMockMvc;

	private CompensatingControlWorksheetResponse compensatingControlWorksheetResponse;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final CompensatingControlWorksheetResponseResource compensatingControlWorksheetResponseResource = new CompensatingControlWorksheetResponseResource(
				compensatingControlWorksheetResponseService);
		this.restCompensatingControlWorksheetResponseMockMvc = MockMvcBuilders
				.standaloneSetup(compensatingControlWorksheetResponseResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static CompensatingControlWorksheetResponse createEntity(EntityManager em) {
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse = new CompensatingControlWorksheetResponse()
				.explanation(DEFAULT_EXPLANATION);
		// Add required entity
		CompensatingControlWorksheet compensatingControlWorksheetId = CompensatingControlWorksheetResourceIntTest
				.createEntity(em);
		em.persist(compensatingControlWorksheetId);
		em.flush();
		compensatingControlWorksheetResponse.setCompensatingControlWorksheetId(compensatingControlWorksheetId);
		// Add required entity
		SaqQuestion saqQuestionId = SaqQuestionResourceIntTest.createEntity(em);
		em.persist(saqQuestionId);
		em.flush();
		compensatingControlWorksheetResponse.setSaqQuestionId(saqQuestionId);
		return compensatingControlWorksheetResponse;
	}

	@Before
	public void initTest() {
		compensatingControlWorksheetResponse = createEntity(em);
	}

	@Test
	@Transactional
	public void createCompensatingControlWorksheetResponse() throws Exception {
		int databaseSizeBeforeCreate = compensatingControlWorksheetResponseRepository.findAll().size();

		// Create the CompensatingControlWorksheetResponse
		restCompensatingControlWorksheetResponseMockMvc
				.perform(post("/api/compensating-control-worksheet-responses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheetResponse)))
				.andExpect(status().isCreated());

		// Validate the CompensatingControlWorksheetResponse in the database
		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeCreate + 1);
		CompensatingControlWorksheetResponse testCompensatingControlWorksheetResponse = compensatingControlWorksheetResponseList
				.get(compensatingControlWorksheetResponseList.size() - 1);
		assertThat(testCompensatingControlWorksheetResponse.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
	}

	@Test
	@Transactional
	public void createCompensatingControlWorksheetResponseWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = compensatingControlWorksheetResponseRepository.findAll().size();

		// Create the CompensatingControlWorksheetResponse with an existing ID
		compensatingControlWorksheetResponse.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restCompensatingControlWorksheetResponseMockMvc
				.perform(post("/api/compensating-control-worksheet-responses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheetResponse)))
				.andExpect(status().isBadRequest());

		// Validate the CompensatingControlWorksheetResponse in the database
		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkExplanationIsRequired() throws Exception {
		int databaseSizeBeforeTest = compensatingControlWorksheetResponseRepository.findAll().size();
		// set the field null
		compensatingControlWorksheetResponse.setExplanation(null);

		// Create the CompensatingControlWorksheetResponse, which fails.

		restCompensatingControlWorksheetResponseMockMvc
				.perform(post("/api/compensating-control-worksheet-responses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheetResponse)))
				.andExpect(status().isBadRequest());

		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllCompensatingControlWorksheetResponses() throws Exception {
		// Initialize the database
		compensatingControlWorksheetResponseRepository.saveAndFlush(compensatingControlWorksheetResponse);

		// Get all the compensatingControlWorksheetResponseList
		restCompensatingControlWorksheetResponseMockMvc
				.perform(get("/api/compensating-control-worksheet-responses?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(compensatingControlWorksheetResponse.getId().intValue())))
				.andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())));
	}

	@Test
	@Transactional
	public void getCompensatingControlWorksheetResponse() throws Exception {
		// Initialize the database
		compensatingControlWorksheetResponseRepository.saveAndFlush(compensatingControlWorksheetResponse);

		// Get the compensatingControlWorksheetResponse
		restCompensatingControlWorksheetResponseMockMvc
				.perform(get("/api/compensating-control-worksheet-responses/{id}",
						compensatingControlWorksheetResponse.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(compensatingControlWorksheetResponse.getId().intValue()))
				.andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingCompensatingControlWorksheetResponse() throws Exception {
		// Get the compensatingControlWorksheetResponse
		restCompensatingControlWorksheetResponseMockMvc
				.perform(get("/api/compensating-control-worksheet-responses/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateCompensatingControlWorksheetResponse() throws Exception {
		// Initialize the database
		compensatingControlWorksheetResponseService.save(compensatingControlWorksheetResponse);

		int databaseSizeBeforeUpdate = compensatingControlWorksheetResponseRepository.findAll().size();

		// Update the compensatingControlWorksheetResponse
		CompensatingControlWorksheetResponse updatedCompensatingControlWorksheetResponse = compensatingControlWorksheetResponseRepository
				.findOne(compensatingControlWorksheetResponse.getId());
		// Disconnect from session so that the updates on
		// updatedCompensatingControlWorksheetResponse are not directly saved in db
		em.detach(updatedCompensatingControlWorksheetResponse);
		updatedCompensatingControlWorksheetResponse.explanation(UPDATED_EXPLANATION);

		restCompensatingControlWorksheetResponseMockMvc
				.perform(put("/api/compensating-control-worksheet-responses")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedCompensatingControlWorksheetResponse)))
				.andExpect(status().isOk());

		// Validate the CompensatingControlWorksheetResponse in the database
		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeUpdate);
		CompensatingControlWorksheetResponse testCompensatingControlWorksheetResponse = compensatingControlWorksheetResponseList
				.get(compensatingControlWorksheetResponseList.size() - 1);
		assertThat(testCompensatingControlWorksheetResponse.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
	}

	@Test
	@Transactional
	public void updateNonExistingCompensatingControlWorksheetResponse() throws Exception {
		int databaseSizeBeforeUpdate = compensatingControlWorksheetResponseRepository.findAll().size();

		// Create the CompensatingControlWorksheetResponse

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restCompensatingControlWorksheetResponseMockMvc
				.perform(
						put("/api/compensating-control-worksheet-responses").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(compensatingControlWorksheetResponse)))
				.andExpect(status().isCreated());

		// Validate the CompensatingControlWorksheetResponse in the database
		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteCompensatingControlWorksheetResponse() throws Exception {
		// Initialize the database
		compensatingControlWorksheetResponseService.save(compensatingControlWorksheetResponse);

		int databaseSizeBeforeDelete = compensatingControlWorksheetResponseRepository.findAll().size();

		// Get the compensatingControlWorksheetResponse
		restCompensatingControlWorksheetResponseMockMvc
				.perform(delete("/api/compensating-control-worksheet-responses/{id}",
						compensatingControlWorksheetResponse.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<CompensatingControlWorksheetResponse> compensatingControlWorksheetResponseList = compensatingControlWorksheetResponseRepository
				.findAll();
		assertThat(compensatingControlWorksheetResponseList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(CompensatingControlWorksheetResponse.class);
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse1 = new CompensatingControlWorksheetResponse();
		compensatingControlWorksheetResponse1.setId(1L);
		CompensatingControlWorksheetResponse compensatingControlWorksheetResponse2 = new CompensatingControlWorksheetResponse();
		compensatingControlWorksheetResponse2.setId(compensatingControlWorksheetResponse1.getId());
		assertThat(compensatingControlWorksheetResponse1).isEqualTo(compensatingControlWorksheetResponse2);
		compensatingControlWorksheetResponse2.setId(2L);
		assertThat(compensatingControlWorksheetResponse1).isNotEqualTo(compensatingControlWorksheetResponse2);
		compensatingControlWorksheetResponse1.setId(null);
		assertThat(compensatingControlWorksheetResponse1).isNotEqualTo(compensatingControlWorksheetResponse2);
	}
}
