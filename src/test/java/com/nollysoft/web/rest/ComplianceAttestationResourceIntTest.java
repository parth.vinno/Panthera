package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.ComplianceAttestation;
import com.nollysoft.domain.ComplianceAttestation.COMPLIANT_STATEMENT;
import com.nollysoft.repository.ComplianceAttestationRepository;
import com.nollysoft.service.ComplianceAttestationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ComplianceAttestationResource REST controller.
 *
 * @see ComplianceAttestationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ComplianceAttestationResourceIntTest {

	private static final COMPLIANT_STATEMENT DEFAULT_COMPLIANT_STATEMENT = COMPLIANT_STATEMENT.NON_COMPLIANT;
	private static final COMPLIANT_STATEMENT UPDATED_COMPLIANT_STATEMENT = COMPLIANT_STATEMENT.COMPLIANT;

	private static final LocalDate DEFAULT_COMPLIANCE_TARGET_DATE = LocalDate.now(ZoneId.systemDefault());
	private static final LocalDate BEFORE_DATE = LocalDate.of(2018, Month.JANUARY, 1);
	private static final LocalDate UPDATED_COMPLIANCE_TARGET_DATE = LocalDate.now(ZoneId.systemDefault()).plusDays(1);

	@Autowired
	private ComplianceAttestationRepository complianceAttestationRepository;

	@Autowired
	private ComplianceAttestationService complianceAttestationService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restComplianceAttestationMockMvc;

	private ComplianceAttestation complianceAttestation;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final ComplianceAttestationResource complianceAttestationResource = new ComplianceAttestationResource(
				complianceAttestationService);
		this.restComplianceAttestationMockMvc = MockMvcBuilders.standaloneSetup(complianceAttestationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static ComplianceAttestation createEntity(EntityManager em) {
		ComplianceAttestation complianceAttestation = new ComplianceAttestation()
				.compliantStatement(DEFAULT_COMPLIANT_STATEMENT).complianceTargetDate(DEFAULT_COMPLIANCE_TARGET_DATE);
		return complianceAttestation;
	}

	@Before
	public void initTest() {
		complianceAttestation = createEntity(em);
	}

	@Test
	@Transactional
	public void createComplianceAttestationForMerchant() throws Exception {
		int databaseSizeBeforeCreate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isCreated());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		ComplianceAttestation testComplianceAttestation = complianceAttestationList
				.get(complianceAttestationList.size() - 1);
		assertThat(testComplianceAttestation.getCompliantStatement()).isEqualTo(DEFAULT_COMPLIANT_STATEMENT);
		assertThat(testComplianceAttestation.getComplianceTargetDate()).isEqualTo(DEFAULT_COMPLIANCE_TARGET_DATE);
	}

	@Test
	@Transactional
	public void createComplianceAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isCreated());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		ComplianceAttestation testComplianceAttestation = complianceAttestationList
				.get(complianceAttestationList.size() - 1);
		assertThat(testComplianceAttestation.getCompliantStatement()).isEqualTo(DEFAULT_COMPLIANT_STATEMENT);
		assertThat(testComplianceAttestation.getComplianceTargetDate()).isEqualTo(DEFAULT_COMPLIANCE_TARGET_DATE);
	}

	@Test
	@Transactional
	public void createComplianceAttestationWithBeforeDateForMerchant() throws Exception {

		// Create the ComplianceAttestation with Target Date before
		// Current date
		complianceAttestation.setComplianceTargetDate(BEFORE_DATE);

		// An entity with Target Date before Current date cannot be created, so
		// this API call must fail
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void createComplianceAttestationWithBeforeDateForServiceProvider() throws Exception {

		// Create the ComplianceAttestation with Target Date before
		// Current date
		complianceAttestation.setComplianceTargetDate(BEFORE_DATE);

		// An entity with Target Date before Current date cannot be created, so
		// this API call must fail
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void createComplianceAttestationWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation with an existing ID
		complianceAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isBadRequest());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createComplianceAttestationWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation with an existing ID
		complianceAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isBadRequest());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkCompliantTargetDateIsRequiredForNonCompliantStatementForMerchant() throws Exception {
		complianceAttestation.setComplianceTargetDate(null);

		// Create the ComplianceAttestation, which fails.

		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkCompliantTargetDateIsRequiredForNonCompliantStatementForServiceProvider() throws Exception {
		complianceAttestation.setComplianceTargetDate(null);

		// Create the ComplianceAttestation, which fails.

		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkCompliantStatementIsRequiredForMerchant() throws Exception {
		complianceAttestation.setCompliantStatement(null);

		// Create the ComplianceAttestation, which fails.

		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkCompliantStatementIsRequiredForServiceProvider() throws Exception {
		complianceAttestation.setCompliantStatement(null);

		// Create the ComplianceAttestation, which fails.

		restComplianceAttestationMockMvc
				.perform(post("/api/compliance-attestations/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllComplianceAttestationsForMerchant() throws Exception {
		// Initialize the database
		complianceAttestationRepository.saveAndFlush(complianceAttestation);

		// Get all the complianceAttestationList
		restComplianceAttestationMockMvc.perform(get("/api/compliance-attestations?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(complianceAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].compliantStatement").value(hasItem(DEFAULT_COMPLIANT_STATEMENT.toString())))
				.andExpect(jsonPath("$.[*].complianceTargetDate")
						.value(hasItem(DEFAULT_COMPLIANCE_TARGET_DATE.toString())));
	}

	@Test
	@Transactional
	public void getAllComplianceAttestationsForServiceProvider() throws Exception {
		// Initialize the database
		complianceAttestationRepository.saveAndFlush(complianceAttestation);

		// Get all the complianceAttestationList
		restComplianceAttestationMockMvc.perform(get("/api/compliance-attestations/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(complianceAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].compliantStatement").value(hasItem(DEFAULT_COMPLIANT_STATEMENT.toString())))
				.andExpect(jsonPath("$.[*].complianceTargetDate")
						.value(hasItem(DEFAULT_COMPLIANCE_TARGET_DATE.toString())));
	}

	@Test
	@Transactional
	public void updateComplianceAttestationForMerchant() throws Exception {
		// Initialize the database
		complianceAttestationService.save(complianceAttestation);

		int databaseSizeBeforeUpdate = complianceAttestationRepository.findAll().size();

		// Update the complianceAttestation
		ComplianceAttestation updatedComplianceAttestation = complianceAttestationRepository
				.findOne(complianceAttestation.getId());
		// Disconnect from session so that the updates on updatedComplianceAttestation
		// are not directly saved in db
		em.detach(updatedComplianceAttestation);
		updatedComplianceAttestation.compliantStatement(UPDATED_COMPLIANT_STATEMENT)
				.complianceTargetDate(UPDATED_COMPLIANCE_TARGET_DATE);

		restComplianceAttestationMockMvc
				.perform(put("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedComplianceAttestation)))
				.andExpect(status().isOk());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeUpdate);
		ComplianceAttestation testComplianceAttestation = complianceAttestationList
				.get(complianceAttestationList.size() - 1);
		assertThat(testComplianceAttestation.getCompliantStatement()).isEqualTo(UPDATED_COMPLIANT_STATEMENT);
		assertThat(testComplianceAttestation.getComplianceTargetDate()).isEqualTo(UPDATED_COMPLIANCE_TARGET_DATE);
	}

	@Test
	@Transactional
	public void updateComplianceAttestationForServiceProvider() throws Exception {
		// Initialize the database
		complianceAttestationService.save(complianceAttestation);

		int databaseSizeBeforeUpdate = complianceAttestationRepository.findAll().size();

		// Update the complianceAttestation
		ComplianceAttestation updatedComplianceAttestation = complianceAttestationRepository
				.findOne(complianceAttestation.getId());
		// Disconnect from session so that the updates on updatedComplianceAttestation
		// are not directly saved in db
		em.detach(updatedComplianceAttestation);
		updatedComplianceAttestation.compliantStatement(UPDATED_COMPLIANT_STATEMENT)
				.complianceTargetDate(UPDATED_COMPLIANCE_TARGET_DATE);

		restComplianceAttestationMockMvc
				.perform(
						put("/api/compliance-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(updatedComplianceAttestation)))
				.andExpect(status().isOk());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeUpdate);
		ComplianceAttestation testComplianceAttestation = complianceAttestationList
				.get(complianceAttestationList.size() - 1);
		assertThat(testComplianceAttestation.getCompliantStatement()).isEqualTo(UPDATED_COMPLIANT_STATEMENT);
		assertThat(testComplianceAttestation.getComplianceTargetDate()).isEqualTo(UPDATED_COMPLIANCE_TARGET_DATE);
	}

	@Test
	@Transactional
	public void updateNonExistingComplianceAttestationForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restComplianceAttestationMockMvc
				.perform(put("/api/compliance-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isCreated());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingComplianceAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = complianceAttestationRepository.findAll().size();

		// Create the ComplianceAttestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restComplianceAttestationMockMvc
				.perform(
						put("/api/compliance-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(complianceAttestation)))
				.andExpect(status().isCreated());

		// Validate the ComplianceAttestation in the database
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteComplianceAttestationForMerchant() throws Exception {
		// Initialize the database
		complianceAttestationService.save(complianceAttestation);

		int databaseSizeBeforeDelete = complianceAttestationRepository.findAll().size();

		// Get the complianceAttestation
		restComplianceAttestationMockMvc
				.perform(delete("/api/compliance-attestations/{id}", complianceAttestation.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteComplianceAttestationForServiceProvider() throws Exception {
		// Initialize the database
		complianceAttestationService.save(complianceAttestation);

		int databaseSizeBeforeDelete = complianceAttestationRepository.findAll().size();

		// Get the complianceAttestation
		restComplianceAttestationMockMvc
				.perform(delete("/api/compliance-attestations/service-provider/{id}", complianceAttestation.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ComplianceAttestation> complianceAttestationList = complianceAttestationRepository.findAll();
		assertThat(complianceAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ComplianceAttestation.class);
		ComplianceAttestation complianceAttestation1 = new ComplianceAttestation();
		complianceAttestation1.setId(1L);
		ComplianceAttestation complianceAttestation2 = new ComplianceAttestation();
		complianceAttestation2.setId(complianceAttestation1.getId());
		assertThat(complianceAttestation1).isEqualTo(complianceAttestation2);
		complianceAttestation2.setId(2L);
		assertThat(complianceAttestation1).isNotEqualTo(complianceAttestation2);
		complianceAttestation1.setId(null);
		assertThat(complianceAttestation1).isNotEqualTo(complianceAttestation2);
	}
}
