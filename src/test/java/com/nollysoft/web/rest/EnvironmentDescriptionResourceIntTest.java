package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.EnvironmentDescription;
import com.nollysoft.repository.EnvironmentDescriptionRepository;
import com.nollysoft.service.EnvironmentDescriptionService;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the EnvironmentDescriptionResource REST controller.
 *
 * @see EnvironmentDescriptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class EnvironmentDescriptionResourceIntTest {

	private static final Boolean DEFAULT_IS_NETWORK_SEGMENTED = false;
	private static final Boolean UPDATED_IS_NETWORK_SEGMENTED = true;
	
	private static final String DEFAULT_DESCRIPTION = "AAAAAAAA";

	@Autowired
	private EnvironmentDescriptionRepository environmentDescriptionRepository;

	@Autowired
	private EnvironmentDescriptionService environmentDescriptionService;

    @Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restEnvironmentDescriptionMockMvc;

	private EnvironmentDescription environmentDescription;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		EnvironmentDescriptionResource environmentDescriptionResource = new EnvironmentDescriptionResource(
				environmentDescriptionService, executiveSummaryStatusService);
		this.restEnvironmentDescriptionMockMvc = MockMvcBuilders.standaloneSetup(environmentDescriptionResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static EnvironmentDescription createEntity(EntityManager em) {
		EnvironmentDescription environmentDescription = new EnvironmentDescription()
				.networkSegmented(DEFAULT_IS_NETWORK_SEGMENTED).description(DEFAULT_DESCRIPTION);
		return environmentDescription;
	}

	@Before
	public void initTest() {
		environmentDescription = createEntity(em);
	}

	@Test
	@Transactional
	public void createEnvironmentDescription() throws Exception {
		int databaseSizeBeforeCreate = environmentDescriptionRepository.findAll().size();

		// Create the EnvironmentDescription
		restEnvironmentDescriptionMockMvc
				.perform(post("/api/environment-descriptions").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(environmentDescription)))
				.andExpect(status().isCreated());

		// Validate the EnvironmentDescription in the database
		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeCreate + 1);
		EnvironmentDescription testEnvironmentDescription = environmentDescriptionList
				.get(environmentDescriptionList.size() - 1);
		assertThat(testEnvironmentDescription.isNetworkSegmented()).isEqualTo(DEFAULT_IS_NETWORK_SEGMENTED);
	}

	@Test
	@Transactional
	public void createEnvironmentDescriptionWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = environmentDescriptionRepository.findAll().size();

		// Create the EnvironmentDescription with an existing ID
		environmentDescription.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restEnvironmentDescriptionMockMvc
				.perform(post("/api/environment-descriptions").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(environmentDescription)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkNetworkSegmentedIsRequired() throws Exception {
		int databaseSizeBeforeTest = environmentDescriptionRepository.findAll().size();
		// set the field null
		environmentDescription.setNetworkSegmented(null);

		// Create the EnvironmentDescription, which fails.

		restEnvironmentDescriptionMockMvc
				.perform(post("/api/environment-descriptions").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(environmentDescription)))
				.andExpect(status().isBadRequest());

		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllEnvironmentDescriptions() throws Exception {
		// Initialize the database
		environmentDescriptionRepository.saveAndFlush(environmentDescription);

		// Get all the environmentDescriptionList
		restEnvironmentDescriptionMockMvc.perform(get("/api/environment-descriptions?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(environmentDescription.getId().intValue())))
				.andExpect(jsonPath("$.[*].networkSegmented")
						.value(hasItem(DEFAULT_IS_NETWORK_SEGMENTED.booleanValue())));
	}

	@Test
	@Transactional
	public void getEnvironmentDescription() throws Exception {
		// Initialize the database
		environmentDescriptionRepository.saveAndFlush(environmentDescription);

		// Get the environmentDescription
		restEnvironmentDescriptionMockMvc
				.perform(get("/api/environment-descriptions/{id}", environmentDescription.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(environmentDescription.getId().intValue()))
				.andExpect(jsonPath("$.networkSegmented").value(DEFAULT_IS_NETWORK_SEGMENTED.booleanValue()));
	}

	@Test
	@Transactional
	public void getNonExistingEnvironmentDescription() throws Exception {
		// Get the environmentDescription
		restEnvironmentDescriptionMockMvc.perform(get("/api/environment-descriptions/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateEnvironmentDescription() throws Exception {
		// Initialize the database
		environmentDescriptionService.save(environmentDescription);

		int databaseSizeBeforeUpdate = environmentDescriptionRepository.findAll().size();

		// Update the environmentDescription
		EnvironmentDescription updatedEnvironmentDescription = environmentDescriptionRepository
				.findOne(environmentDescription.getId());
		updatedEnvironmentDescription.networkSegmented(UPDATED_IS_NETWORK_SEGMENTED);

		restEnvironmentDescriptionMockMvc
				.perform(put("/api/environment-descriptions").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedEnvironmentDescription)))
				.andExpect(status().isOk());

		// Validate the EnvironmentDescription in the database
		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeUpdate);
		EnvironmentDescription testEnvironmentDescription = environmentDescriptionList
				.get(environmentDescriptionList.size() - 1);
		assertThat(testEnvironmentDescription.isNetworkSegmented()).isEqualTo(UPDATED_IS_NETWORK_SEGMENTED);
	}

	@Test
	@Transactional
	public void updateNonExistingEnvironmentDescription() throws Exception {
		int databaseSizeBeforeUpdate = environmentDescriptionRepository.findAll().size();

		// Create the EnvironmentDescription

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restEnvironmentDescriptionMockMvc
				.perform(put("/api/environment-descriptions").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(environmentDescription)))
				.andExpect(status().isCreated());

		// Validate the EnvironmentDescription in the database
		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteEnvironmentDescription() throws Exception {
		// Initialize the database
		environmentDescriptionService.save(environmentDescription);

		int databaseSizeBeforeDelete = environmentDescriptionRepository.findAll().size();

		// Get the environmentDescription
		restEnvironmentDescriptionMockMvc
				.perform(delete("/api/environment-descriptions/{id}", environmentDescription.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<EnvironmentDescription> environmentDescriptionList = environmentDescriptionRepository.findAll();
		assertThat(environmentDescriptionList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(EnvironmentDescription.class);
		EnvironmentDescription environmentDescription1 = new EnvironmentDescription();
		environmentDescription1.setId(1L);
		EnvironmentDescription environmentDescription2 = new EnvironmentDescription();
		environmentDescription2.setId(environmentDescription1.getId());
		assertThat(environmentDescription1).isEqualTo(environmentDescription2);
		environmentDescription2.setId(2L);
		assertThat(environmentDescription1).isNotEqualTo(environmentDescription2);
		environmentDescription1.setId(null);
		assertThat(environmentDescription1).isNotEqualTo(environmentDescription2);
	}
}
