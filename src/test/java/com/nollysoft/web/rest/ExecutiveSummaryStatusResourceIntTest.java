package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ExecutiveSummaryStatus;
import com.nollysoft.domain.enumeration.Section;
import com.nollysoft.repository.ExecutiveSummaryStatusRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the ExecutiveSummaryStatusResource REST controller.
 *
 * @see ExecutiveSummaryStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ExecutiveSummaryStatusResourceIntTest {

    private static final Section DEFAULT_SECTION = Section.CARD_DESCRIPTION;
    private static final Section UPDATED_SECTION = Section.ENV_DESCRIPTION;

    private static final LocalDateTime DEFAULT_LAST_VISITED_AT = LocalDateTime.now();

    @Autowired
    private ExecutiveSummaryStatusRepository executiveSummaryStatusRepository;

    @Autowired
    private ExecutiveSummaryStatusService executiveSummaryStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restExecutiveSummaryStatusMockMvc;

    private ExecutiveSummaryStatus executiveSummaryStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ExecutiveSummaryStatusResource executiveSummaryStatusResource = new ExecutiveSummaryStatusResource(executiveSummaryStatusService);
        this.restExecutiveSummaryStatusMockMvc = MockMvcBuilders.standaloneSetup(executiveSummaryStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExecutiveSummaryStatus createEntity(EntityManager em) {
        ExecutiveSummaryStatus executiveSummaryStatus = new ExecutiveSummaryStatus()
            .section(DEFAULT_SECTION)
            .lastUpdatedAt(DEFAULT_LAST_VISITED_AT);
        return executiveSummaryStatus;
    }

    @Before
    public void initTest() {
        executiveSummaryStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createExecutiveSummaryStatus() throws Exception {
        int databaseSizeBeforeCreate = executiveSummaryStatusRepository.findAll().size();

        // Create the ExecutiveSummaryStatus
        restExecutiveSummaryStatusMockMvc.perform(post("/api/executive-summary-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(executiveSummaryStatus)))
            .andExpect(status().isCreated());

        // Validate the ExecutiveSummaryStatus in the database
        List<ExecutiveSummaryStatus> executiveSummaryStatusList = executiveSummaryStatusRepository.findAll();
        assertThat(executiveSummaryStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ExecutiveSummaryStatus testExecutiveSummaryStatus = executiveSummaryStatusList.get(executiveSummaryStatusList.size() - 1);
        assertThat(testExecutiveSummaryStatus.getSectionAsEnum()).isEqualTo(DEFAULT_SECTION);;
    }

    @Test
    @Transactional
    public void createExecutiveSummaryStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = executiveSummaryStatusRepository.findAll().size();

        // Create the ExecutiveSummaryStatus with an existing ID
        executiveSummaryStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExecutiveSummaryStatusMockMvc.perform(post("/api/executive-summary-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(executiveSummaryStatus)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ExecutiveSummaryStatus> executiveSummaryStatusList = executiveSummaryStatusRepository.findAll();
        assertThat(executiveSummaryStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllExecutiveSummaryStatuses() throws Exception {
        // Initialize the database
        executiveSummaryStatusRepository.saveAndFlush(executiveSummaryStatus);

        // Get all the executiveSummaryStatusList
        restExecutiveSummaryStatusMockMvc.perform(get("/api/executive-summary-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(executiveSummaryStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].section").value(hasItem(DEFAULT_SECTION.toString())));
    }

    @Test
    @Transactional
    public void getExecutiveSummaryStatus() throws Exception {
        // Initialize the database
        executiveSummaryStatusRepository.saveAndFlush(executiveSummaryStatus);

        // Get the executiveSummaryStatus
        restExecutiveSummaryStatusMockMvc.perform(get("/api/executive-summary-statuses/{id}", executiveSummaryStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(executiveSummaryStatus.getId().intValue()))
            .andExpect(jsonPath("$.section").value(DEFAULT_SECTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExecutiveSummaryStatus() throws Exception {
        // Get the executiveSummaryStatus
        restExecutiveSummaryStatusMockMvc.perform(get("/api/executive-summary-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExecutiveSummaryStatus() throws Exception {
        // Initialize the database
        executiveSummaryStatusService.save(executiveSummaryStatus);

        int databaseSizeBeforeUpdate = executiveSummaryStatusRepository.findAll().size();

        // Update the executiveSummaryStatus
        ExecutiveSummaryStatus updatedExecutiveSummaryStatus = executiveSummaryStatusRepository.findOne(executiveSummaryStatus.getId());
        updatedExecutiveSummaryStatus
            .section(UPDATED_SECTION);

        restExecutiveSummaryStatusMockMvc.perform(put("/api/executive-summary-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExecutiveSummaryStatus)))
            .andExpect(status().isOk());

        // Validate the ExecutiveSummaryStatus in the database
        List<ExecutiveSummaryStatus> executiveSummaryStatusList = executiveSummaryStatusRepository.findAll();
        assertThat(executiveSummaryStatusList).hasSize(databaseSizeBeforeUpdate);
        ExecutiveSummaryStatus testExecutiveSummaryStatus = executiveSummaryStatusList.get(executiveSummaryStatusList.size() - 1);
        assertThat(testExecutiveSummaryStatus.getSection()).isEqualTo(UPDATED_SECTION.toString());
    }

    @Test
    @Transactional
    public void updateNonExistingExecutiveSummaryStatus() throws Exception {
        int databaseSizeBeforeUpdate = executiveSummaryStatusRepository.findAll().size();

        // Create the ExecutiveSummaryStatus

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExecutiveSummaryStatusMockMvc.perform(put("/api/executive-summary-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(executiveSummaryStatus)))
            .andExpect(status().isCreated());

        // Validate the ExecutiveSummaryStatus in the database
        List<ExecutiveSummaryStatus> executiveSummaryStatusList = executiveSummaryStatusRepository.findAll();
        assertThat(executiveSummaryStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteExecutiveSummaryStatus() throws Exception {
        // Initialize the database
        executiveSummaryStatusService.save(executiveSummaryStatus);

        int databaseSizeBeforeDelete = executiveSummaryStatusRepository.findAll().size();

        // Get the executiveSummaryStatus
        restExecutiveSummaryStatusMockMvc.perform(delete("/api/executive-summary-statuses/{id}", executiveSummaryStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ExecutiveSummaryStatus> executiveSummaryStatusList = executiveSummaryStatusRepository.findAll();
        assertThat(executiveSummaryStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExecutiveSummaryStatus.class);
        ExecutiveSummaryStatus executiveSummaryStatus1 = new ExecutiveSummaryStatus();
        executiveSummaryStatus1.setId(1L);
        ExecutiveSummaryStatus executiveSummaryStatus2 = new ExecutiveSummaryStatus();
        executiveSummaryStatus2.setId(executiveSummaryStatus1.getId());
        assertThat(executiveSummaryStatus1).isEqualTo(executiveSummaryStatus2);
        executiveSummaryStatus2.setId(2L);
        assertThat(executiveSummaryStatus1).isNotEqualTo(executiveSummaryStatus2);
        executiveSummaryStatus1.setId(null);
        assertThat(executiveSummaryStatus1).isNotEqualTo(executiveSummaryStatus2);
    }
}
