package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ExpectedTest;
import com.nollysoft.repository.ExpectedTestRepository;
import com.nollysoft.service.ExpectedTestService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ExpectedTestResource REST controller.
 *
 * @see ExpectedTestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ExpectedTestResourceIntTest {

    private static final String DEFAULT_TEST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TEST_NAME = "BBBBBBBBBB";

    @Autowired
    private ExpectedTestRepository expectedTestRepository;

    @Autowired
    private ExpectedTestService expectedTestService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restExpectedTestMockMvc;

    private ExpectedTest expectedTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExpectedTestResource expectedTestResource = new ExpectedTestResource(expectedTestService);
        this.restExpectedTestMockMvc = MockMvcBuilders.standaloneSetup(expectedTestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExpectedTest createEntity(EntityManager em) {
        ExpectedTest expectedTest = new ExpectedTest()
            .testName(DEFAULT_TEST_NAME);
        return expectedTest;
    }

    @Before
    public void initTest() {
        expectedTest = createEntity(em);
    }

    @Test
    @Transactional
    public void createExpectedTest() throws Exception {
        int databaseSizeBeforeCreate = expectedTestRepository.findAll().size();

        // Create the ExpectedTest
        restExpectedTestMockMvc.perform(post("/api/expected-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expectedTest)))
            .andExpect(status().isCreated());

        // Validate the ExpectedTest in the database
        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeCreate + 1);
        ExpectedTest testExpectedTest = expectedTestList.get(expectedTestList.size() - 1);
        assertThat(testExpectedTest.getTestName()).isEqualTo(DEFAULT_TEST_NAME);
    }

    @Test
    @Transactional
    public void createExpectedTestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = expectedTestRepository.findAll().size();

        // Create the ExpectedTest with an existing ID
        expectedTest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExpectedTestMockMvc.perform(post("/api/expected-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expectedTest)))
            .andExpect(status().isBadRequest());

        // Validate the ExpectedTest in the database
        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTestNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = expectedTestRepository.findAll().size();
        // set the field null
        expectedTest.setTestName(null);

        // Create the ExpectedTest, which fails.

        restExpectedTestMockMvc.perform(post("/api/expected-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expectedTest)))
            .andExpect(status().isBadRequest());

        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllExpectedTests() throws Exception {
        // Initialize the database
        expectedTestRepository.saveAndFlush(expectedTest);

        // Get all the expectedTestList
        restExpectedTestMockMvc.perform(get("/api/expected-tests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(expectedTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].testName").value(hasItem(DEFAULT_TEST_NAME.toString())));
    }

    @Test
    @Transactional
    public void getExpectedTest() throws Exception {
        // Initialize the database
        expectedTestRepository.saveAndFlush(expectedTest);

        // Get the expectedTest
        restExpectedTestMockMvc.perform(get("/api/expected-tests/{id}", expectedTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(expectedTest.getId().intValue()))
            .andExpect(jsonPath("$.testName").value(DEFAULT_TEST_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExpectedTest() throws Exception {
        // Get the expectedTest
        restExpectedTestMockMvc.perform(get("/api/expected-tests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExpectedTest() throws Exception {
        // Initialize the database
        expectedTestService.save(expectedTest);

        int databaseSizeBeforeUpdate = expectedTestRepository.findAll().size();

        // Update the expectedTest
        ExpectedTest updatedExpectedTest = expectedTestRepository.findOne(expectedTest.getId());
        // Disconnect from session so that the updates on updatedExpectedTest are not directly saved in db
        em.detach(updatedExpectedTest);
        updatedExpectedTest
            .testName(UPDATED_TEST_NAME);

        restExpectedTestMockMvc.perform(put("/api/expected-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExpectedTest)))
            .andExpect(status().isOk());

        // Validate the ExpectedTest in the database
        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeUpdate);
        ExpectedTest testExpectedTest = expectedTestList.get(expectedTestList.size() - 1);
        assertThat(testExpectedTest.getTestName()).isEqualTo(UPDATED_TEST_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingExpectedTest() throws Exception {
        int databaseSizeBeforeUpdate = expectedTestRepository.findAll().size();

        // Create the ExpectedTest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExpectedTestMockMvc.perform(put("/api/expected-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expectedTest)))
            .andExpect(status().isCreated());

        // Validate the ExpectedTest in the database
        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteExpectedTest() throws Exception {
        // Initialize the database
        expectedTestService.save(expectedTest);

        int databaseSizeBeforeDelete = expectedTestRepository.findAll().size();

        // Get the expectedTest
        restExpectedTestMockMvc.perform(delete("/api/expected-tests/{id}", expectedTest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ExpectedTest> expectedTestList = expectedTestRepository.findAll();
        assertThat(expectedTestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExpectedTest.class);
        ExpectedTest expectedTest1 = new ExpectedTest();
        expectedTest1.setId(1L);
        ExpectedTest expectedTest2 = new ExpectedTest();
        expectedTest2.setId(expectedTest1.getId());
        assertThat(expectedTest1).isEqualTo(expectedTest2);
        expectedTest2.setId(2L);
        assertThat(expectedTest1).isNotEqualTo(expectedTest2);
        expectedTest1.setId(null);
        assertThat(expectedTest1).isNotEqualTo(expectedTest2);
    }
}
