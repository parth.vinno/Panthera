package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.HostingProvider;
import com.nollysoft.repository.HostingProviderRepository;
import com.nollysoft.service.HostingProviderService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HostingProviderResource REST controller.
 *
 * @see HostingProviderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class HostingProviderResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private HostingProviderRepository hostingProviderRepository;

    @Autowired
    private HostingProviderService hostingProviderService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHostingProviderMockMvc;

    private HostingProvider hostingProvider;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        HostingProviderResource hostingProviderResource = new HostingProviderResource(hostingProviderService);
        this.restHostingProviderMockMvc = MockMvcBuilders.standaloneSetup(hostingProviderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HostingProvider createEntity(EntityManager em) {
        HostingProvider hostingProvider = new HostingProvider()
            .name(DEFAULT_NAME);
        return hostingProvider;
    }

    @Before
    public void initTest() {
        hostingProvider = createEntity(em);
    }

    @Test
    @Transactional
    public void createHostingProvider() throws Exception {
        int databaseSizeBeforeCreate = hostingProviderRepository.findAll().size();

        // Create the HostingProvider
        restHostingProviderMockMvc.perform(post("/api/hosting-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hostingProvider)))
            .andExpect(status().isCreated());

        // Validate the HostingProvider in the database
        List<HostingProvider> hostingProviderList = hostingProviderRepository.findAll();
        assertThat(hostingProviderList).hasSize(databaseSizeBeforeCreate + 1);
        HostingProvider testHostingProvider = hostingProviderList.get(hostingProviderList.size() - 1);
        assertThat(testHostingProvider.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createHostingProviderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hostingProviderRepository.findAll().size();

        // Create the HostingProvider with an existing ID
        hostingProvider.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHostingProviderMockMvc.perform(post("/api/hosting-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hostingProvider)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<HostingProvider> hostingProviderList = hostingProviderRepository.findAll();
        assertThat(hostingProviderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllHostingProviders() throws Exception {
        // Initialize the database
        hostingProviderRepository.saveAndFlush(hostingProvider);

        // Get all the hostingProviderList
        restHostingProviderMockMvc.perform(get("/api/hosting-providers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hostingProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getHostingProvider() throws Exception {
        // Initialize the database
        hostingProviderRepository.saveAndFlush(hostingProvider);

        // Get the hostingProvider
        restHostingProviderMockMvc.perform(get("/api/hosting-providers/{id}", hostingProvider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(hostingProvider.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingHostingProvider() throws Exception {
        // Get the hostingProvider
        restHostingProviderMockMvc.perform(get("/api/hosting-providers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHostingProvider() throws Exception {
        // Initialize the database
        hostingProviderService.save(hostingProvider);

        int databaseSizeBeforeUpdate = hostingProviderRepository.findAll().size();

        // Update the hostingProvider
        HostingProvider updatedHostingProvider = hostingProviderRepository.findOne(hostingProvider.getId());
        updatedHostingProvider
            .name(UPDATED_NAME);

        restHostingProviderMockMvc.perform(put("/api/hosting-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHostingProvider)))
            .andExpect(status().isOk());

        // Validate the HostingProvider in the database
        List<HostingProvider> hostingProviderList = hostingProviderRepository.findAll();
        assertThat(hostingProviderList).hasSize(databaseSizeBeforeUpdate);
        HostingProvider testHostingProvider = hostingProviderList.get(hostingProviderList.size() - 1);
        assertThat(testHostingProvider.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingHostingProvider() throws Exception {
        int databaseSizeBeforeUpdate = hostingProviderRepository.findAll().size();

        // Create the HostingProvider

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHostingProviderMockMvc.perform(put("/api/hosting-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hostingProvider)))
            .andExpect(status().isCreated());

        // Validate the HostingProvider in the database
        List<HostingProvider> hostingProviderList = hostingProviderRepository.findAll();
        assertThat(hostingProviderList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteHostingProvider() throws Exception {
        // Initialize the database
        hostingProviderService.save(hostingProvider);

        int databaseSizeBeforeDelete = hostingProviderRepository.findAll().size();

        // Get the hostingProvider
        restHostingProviderMockMvc.perform(delete("/api/hosting-providers/{id}", hostingProvider.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<HostingProvider> hostingProviderList = hostingProviderRepository.findAll();
        assertThat(hostingProviderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HostingProvider.class);
        HostingProvider hostingProvider1 = new HostingProvider();
        hostingProvider1.setId(1L);
        HostingProvider hostingProvider2 = new HostingProvider();
        hostingProvider2.setId(hostingProvider1.getId());
        assertThat(hostingProvider1).isEqualTo(hostingProvider2);
        hostingProvider2.setId(2L);
        assertThat(hostingProvider1).isNotEqualTo(hostingProvider2);
        hostingProvider1.setId(null);
        assertThat(hostingProvider1).isNotEqualTo(hostingProvider2);
    }
}
