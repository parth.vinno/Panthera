package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ISAAttestation;
import com.nollysoft.repository.ISAAttestationRepository;
import com.nollysoft.service.ISAAttestationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ISAAttestationResource REST controller.
 *
 * @see ISAAttestationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ISAAttestationResourceIntTest {

	private static final String DEFAULT_ISA_ATTESTOR_NAME = "Test Default Name";
	private static final String UPDATED_ISA_ATTESTOR_NAME = "Test Updated Name";

	private static final String DEFAULT_ISA_ATTESTOR_TITLE = "Test Default Title";
	private static final String UPDATED_ISA_ATTESTOR_TITLE = "Test Updated Title";

	private static final String DEFAULT_ISA_ATTESTOR_COMPANY = "Test Default Signature";
	private static final String UPDATED_ISA_ATTESTOR_COMPANY = "Test Updated Signature";

	@Autowired
	private ISAAttestationRepository isaAttestationRepository;

	@Autowired
	private ISAAttestationService isaAttestationService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restISAAttestationMockMvc;

	private ISAAttestation isaAttestation;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final ISAAttestationResource isaAttestationResource = new ISAAttestationResource(isaAttestationService);
		this.restISAAttestationMockMvc = MockMvcBuilders.standaloneSetup(isaAttestationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static ISAAttestation createEntity(EntityManager em) {
		ISAAttestation isaAttestation = new ISAAttestation().isaAttestorName(DEFAULT_ISA_ATTESTOR_NAME)
				.isaAttestorTitle(DEFAULT_ISA_ATTESTOR_TITLE).isaAttestorCompany(DEFAULT_ISA_ATTESTOR_COMPANY);
		return isaAttestation;
	}

	@Before
	public void initTest() {
		isaAttestation = createEntity(em);
	}

	@Test
	@Transactional
	public void createISAAttestationForMerchant() throws Exception {
		int databaseSizeBeforeCreate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation
		restISAAttestationMockMvc.perform(post("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(isaAttestation))).andExpect(status().isCreated());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		ISAAttestation testISAAttestation = isaAttestationList.get(isaAttestationList.size() - 1);
		assertThat(testISAAttestation.getIsaAttestorName()).isEqualTo(DEFAULT_ISA_ATTESTOR_NAME);
		assertThat(testISAAttestation.getIsaAttestorTitle()).isEqualTo(DEFAULT_ISA_ATTESTOR_TITLE);
		assertThat(testISAAttestation.getIsaAttestorCompany()).isEqualTo(DEFAULT_ISA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void createISAAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation
		restISAAttestationMockMvc.perform(post("/api/isa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isCreated());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		ISAAttestation testISAAttestation = isaAttestationList.get(isaAttestationList.size() - 1);
		assertThat(testISAAttestation.getIsaAttestorName()).isEqualTo(DEFAULT_ISA_ATTESTOR_NAME);
		assertThat(testISAAttestation.getIsaAttestorTitle()).isEqualTo(DEFAULT_ISA_ATTESTOR_TITLE);
		assertThat(testISAAttestation.getIsaAttestorCompany()).isEqualTo(DEFAULT_ISA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void createISAAttestationWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation with an existing ID
		isaAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restISAAttestationMockMvc.perform(post("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(isaAttestation))).andExpect(status().isBadRequest());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createISAAttestationWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation with an existing ID
		isaAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restISAAttestationMockMvc.perform(post("/api/isa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isBadRequest());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkIsaAttestorNameIsRequiredForMerchant() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorName(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkIsaAttestorNameIsRequiredForServiceProvider() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorName(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkIsaAttestorTitleIsRequiredForMerchant() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorTitle(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkIsaAttestorTitleIsRequiredForServiceProvider() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorTitle(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkIsaAttestorCompanyIsRequiredForMerchant() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorCompany(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkIsaAttestorCompanyIsRequiredForServiceProvider() throws Exception {
		// set the field null
		isaAttestation.setIsaAttestorCompany(null);

		// Create the ISAAttestation, which fails.

		restISAAttestationMockMvc
				.perform(post("/api/isa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllISAAttestationsForMerchant() throws Exception {
		// Initialize the database
		isaAttestationRepository.saveAndFlush(isaAttestation);

		// Get all the isaAttestationList
		restISAAttestationMockMvc.perform(get("/api/isa-attestations?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(isaAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].isaAttestorName").value(hasItem(DEFAULT_ISA_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].isaAttestorTitle").value(hasItem(DEFAULT_ISA_ATTESTOR_TITLE.toString())))
				.andExpect(
						jsonPath("$.[*].isaAttestorCompany").value(hasItem(DEFAULT_ISA_ATTESTOR_COMPANY.toString())));
	}

	@Test
	@Transactional
	public void getAllISAAttestationsForServiceProvider() throws Exception {
		// Initialize the database
		isaAttestationRepository.saveAndFlush(isaAttestation);

		// Get all the isaAttestationList
		restISAAttestationMockMvc.perform(get("/api/isa-attestations/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(isaAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].isaAttestorName").value(hasItem(DEFAULT_ISA_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].isaAttestorTitle").value(hasItem(DEFAULT_ISA_ATTESTOR_TITLE.toString())))
				.andExpect(
						jsonPath("$.[*].isaAttestorCompany").value(hasItem(DEFAULT_ISA_ATTESTOR_COMPANY.toString())));
	}

	@Test
	@Transactional
	public void updateISAAttestationForMerchant() throws Exception {
		// Initialize the database
		isaAttestationService.save(isaAttestation);

		int databaseSizeBeforeUpdate = isaAttestationRepository.findAll().size();

		// Update the isaAttestation
		ISAAttestation updatedISAAttestation = isaAttestationRepository.findOne(isaAttestation.getId());
		// Disconnect from session so that the updates on updatedISAAttestation are not
		// directly saved in db
		em.detach(updatedISAAttestation);
		updatedISAAttestation.isaAttestorName(UPDATED_ISA_ATTESTOR_NAME).isaAttestorTitle(UPDATED_ISA_ATTESTOR_TITLE)
				.isaAttestorCompany(UPDATED_ISA_ATTESTOR_COMPANY);

		restISAAttestationMockMvc.perform(put("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedISAAttestation))).andExpect(status().isOk());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeUpdate);
		ISAAttestation testISAAttestation = isaAttestationList.get(isaAttestationList.size() - 1);
		assertThat(testISAAttestation.getIsaAttestorName()).isEqualTo(UPDATED_ISA_ATTESTOR_NAME);
		assertThat(testISAAttestation.getIsaAttestorTitle()).isEqualTo(UPDATED_ISA_ATTESTOR_TITLE);
		assertThat(testISAAttestation.getIsaAttestorCompany()).isEqualTo(UPDATED_ISA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void updateISAAttestationForServiceProvider() throws Exception {
		// Initialize the database
		isaAttestationService.save(isaAttestation);

		int databaseSizeBeforeUpdate = isaAttestationRepository.findAll().size();

		// Update the isaAttestation
		ISAAttestation updatedISAAttestation = isaAttestationRepository.findOne(isaAttestation.getId());
		// Disconnect from session so that the updates on updatedISAAttestation are not
		// directly saved in db
		em.detach(updatedISAAttestation);
		updatedISAAttestation.isaAttestorName(UPDATED_ISA_ATTESTOR_NAME).isaAttestorTitle(UPDATED_ISA_ATTESTOR_TITLE)
				.isaAttestorCompany(UPDATED_ISA_ATTESTOR_COMPANY);

		restISAAttestationMockMvc
				.perform(put("/api/isa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedISAAttestation)))
				.andExpect(status().isOk());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeUpdate);
		ISAAttestation testISAAttestation = isaAttestationList.get(isaAttestationList.size() - 1);
		assertThat(testISAAttestation.getIsaAttestorName()).isEqualTo(UPDATED_ISA_ATTESTOR_NAME);
		assertThat(testISAAttestation.getIsaAttestorTitle()).isEqualTo(UPDATED_ISA_ATTESTOR_TITLE);
		assertThat(testISAAttestation.getIsaAttestorCompany()).isEqualTo(UPDATED_ISA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void updateNonExistingISAAttestationForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restISAAttestationMockMvc.perform(put("/api/isa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(isaAttestation))).andExpect(status().isCreated());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingISAAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = isaAttestationRepository.findAll().size();

		// Create the ISAAttestation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restISAAttestationMockMvc.perform(put("/api/isa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(isaAttestation)))
				.andExpect(status().isCreated());

		// Validate the ISAAttestation in the database
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteISAAttestationForMerchant() throws Exception {
		// Initialize the database
		isaAttestationService.save(isaAttestation);

		int databaseSizeBeforeDelete = isaAttestationRepository.findAll().size();

		// Get the isaAttestation
		restISAAttestationMockMvc.perform(
				delete("/api/isa-attestations/{id}", isaAttestation.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteISAAttestationForServiceProvider() throws Exception {
		// Initialize the database
		isaAttestationService.save(isaAttestation);

		int databaseSizeBeforeDelete = isaAttestationRepository.findAll().size();

		// Get the isaAttestation
		restISAAttestationMockMvc.perform(delete("/api/isa-attestations/service-provider/{id}", isaAttestation.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<ISAAttestation> isaAttestationList = isaAttestationRepository.findAll();
		assertThat(isaAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ISAAttestation.class);
		ISAAttestation isaAttestation1 = new ISAAttestation();
		isaAttestation1.setId(1L);
		ISAAttestation isaAttestation2 = new ISAAttestation();
		isaAttestation2.setId(isaAttestation1.getId());
		assertThat(isaAttestation1).isEqualTo(isaAttestation2);
		isaAttestation2.setId(2L);
		assertThat(isaAttestation1).isNotEqualTo(isaAttestation2);
		isaAttestation1.setId(null);
		assertThat(isaAttestation1).isNotEqualTo(isaAttestation2);
	}
}
