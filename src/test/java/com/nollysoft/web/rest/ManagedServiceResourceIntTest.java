package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.ManagedService;
import com.nollysoft.repository.ManagedServiceRepository;
import com.nollysoft.service.ManagedServiceService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ManagedServiceResource REST controller.
 *
 * @see ManagedServiceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ManagedServiceResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ManagedServiceRepository managedServiceRepository;

    @Autowired
    private ManagedServiceService managedServiceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restManagedServiceMockMvc;

    private ManagedService managedService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ManagedServiceResource managedServiceResource = new ManagedServiceResource(managedServiceService);
        this.restManagedServiceMockMvc = MockMvcBuilders.standaloneSetup(managedServiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ManagedService createEntity(EntityManager em) {
        ManagedService managedService = new ManagedService()
            .name(DEFAULT_NAME);
        return managedService;
    }

    @Before
    public void initTest() {
        managedService = createEntity(em);
    }

    @Test
    @Transactional
    public void createManagedService() throws Exception {
        int databaseSizeBeforeCreate = managedServiceRepository.findAll().size();

        // Create the ManagedService
        restManagedServiceMockMvc.perform(post("/api/managed-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedService)))
            .andExpect(status().isCreated());

        // Validate the ManagedService in the database
        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeCreate + 1);
        ManagedService testManagedService = managedServiceList.get(managedServiceList.size() - 1);
        assertThat(testManagedService.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createManagedServiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = managedServiceRepository.findAll().size();

        // Create the ManagedService with an existing ID
        managedService.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restManagedServiceMockMvc.perform(post("/api/managed-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedService)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = managedServiceRepository.findAll().size();
        // set the field null
        managedService.setName(null);

        // Create the ManagedService, which fails.

        restManagedServiceMockMvc.perform(post("/api/managed-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedService)))
            .andExpect(status().isBadRequest());

        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllManagedServices() throws Exception {
        // Initialize the database
        managedServiceRepository.saveAndFlush(managedService);

        // Get all the managedServiceList
        restManagedServiceMockMvc.perform(get("/api/managed-services?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(managedService.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getManagedService() throws Exception {
        // Initialize the database
        managedServiceRepository.saveAndFlush(managedService);

        // Get the managedService
        restManagedServiceMockMvc.perform(get("/api/managed-services/{id}", managedService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(managedService.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingManagedService() throws Exception {
        // Get the managedService
        restManagedServiceMockMvc.perform(get("/api/managed-services/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateManagedService() throws Exception {
        // Initialize the database
        managedServiceService.save(managedService);

        int databaseSizeBeforeUpdate = managedServiceRepository.findAll().size();

        // Update the managedService
        ManagedService updatedManagedService = managedServiceRepository.findOne(managedService.getId());
        updatedManagedService
            .name(UPDATED_NAME);

        restManagedServiceMockMvc.perform(put("/api/managed-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedManagedService)))
            .andExpect(status().isOk());

        // Validate the ManagedService in the database
        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeUpdate);
        ManagedService testManagedService = managedServiceList.get(managedServiceList.size() - 1);
        assertThat(testManagedService.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingManagedService() throws Exception {
        int databaseSizeBeforeUpdate = managedServiceRepository.findAll().size();

        // Create the ManagedService

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restManagedServiceMockMvc.perform(put("/api/managed-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedService)))
            .andExpect(status().isCreated());

        // Validate the ManagedService in the database
        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteManagedService() throws Exception {
        // Initialize the database
        managedServiceService.save(managedService);

        int databaseSizeBeforeDelete = managedServiceRepository.findAll().size();

        // Get the managedService
        restManagedServiceMockMvc.perform(delete("/api/managed-services/{id}", managedService.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ManagedService> managedServiceList = managedServiceRepository.findAll();
        assertThat(managedServiceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ManagedService.class);
        ManagedService managedService1 = new ManagedService();
        managedService1.setId(1L);
        ManagedService managedService2 = new ManagedService();
        managedService2.setId(managedService1.getId());
        assertThat(managedService1).isEqualTo(managedService2);
        managedService2.setId(2L);
        assertThat(managedService1).isNotEqualTo(managedService2);
        managedService1.setId(null);
        assertThat(managedService1).isNotEqualTo(managedService2);
    }
}
