package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.MerchantBusinessType;
import com.nollysoft.repository.MerchantBusinessTypeRepository;
import com.nollysoft.service.MerchantBusinessTypeService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MerchantBusinessTypeResource REST controller.
 *
 * @see MerchantBusinessTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class MerchantBusinessTypeResourceIntTest {

    private static final String DEFAULT_BUSINESS_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_TYPE = "BBBBBBBBBB";

    @Autowired
    private MerchantBusinessTypeRepository merchantBusinessTypeRepository;

    @Autowired
    private MerchantBusinessTypeService merchantBusinessTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMerchantBusinessTypeMockMvc;

    private MerchantBusinessType merchantBusinessType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MerchantBusinessTypeResource merchantBusinessTypeResource = new MerchantBusinessTypeResource(merchantBusinessTypeService);
        this.restMerchantBusinessTypeMockMvc = MockMvcBuilders.standaloneSetup(merchantBusinessTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MerchantBusinessType createEntity(EntityManager em) {
        MerchantBusinessType merchantBusinessType = new MerchantBusinessType()
            .businessType(DEFAULT_BUSINESS_TYPE);
        return merchantBusinessType;
    }

    @Before
    public void initTest() {
        merchantBusinessType = createEntity(em);
    }

    @Test
    @Transactional
    public void createMerchantBusinessType() throws Exception {
        int databaseSizeBeforeCreate = merchantBusinessTypeRepository.findAll().size();

        // Create the MerchantBusinessType
        restMerchantBusinessTypeMockMvc.perform(post("/api/merchant-business-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(merchantBusinessType)))
            .andExpect(status().isCreated());

        // Validate the MerchantBusinessType in the database
        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeCreate + 1);
        MerchantBusinessType testMerchantBusinessType = merchantBusinessTypeList.get(merchantBusinessTypeList.size() - 1);
        assertThat(testMerchantBusinessType.getBusinessType()).isEqualTo(DEFAULT_BUSINESS_TYPE);
    }

    @Test
    @Transactional
    public void createMerchantBusinessTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = merchantBusinessTypeRepository.findAll().size();

        // Create the MerchantBusinessType with an existing ID
        merchantBusinessType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMerchantBusinessTypeMockMvc.perform(post("/api/merchant-business-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(merchantBusinessType)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkBusinessTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = merchantBusinessTypeRepository.findAll().size();
        // set the field null
        merchantBusinessType.setBusinessType(null);

        // Create the MerchantBusinessType, which fails.

        restMerchantBusinessTypeMockMvc.perform(post("/api/merchant-business-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(merchantBusinessType)))
            .andExpect(status().isBadRequest());

        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMerchantBusinessTypes() throws Exception {
        // Initialize the database
        merchantBusinessTypeRepository.saveAndFlush(merchantBusinessType);

        // Get all the merchantBusinessTypeList
        restMerchantBusinessTypeMockMvc.perform(get("/api/merchant-business-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(merchantBusinessType.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessType").value(hasItem(DEFAULT_BUSINESS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getMerchantBusinessType() throws Exception {
        // Initialize the database
        merchantBusinessTypeRepository.saveAndFlush(merchantBusinessType);

        // Get the merchantBusinessType
        restMerchantBusinessTypeMockMvc.perform(get("/api/merchant-business-types/{id}", merchantBusinessType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(merchantBusinessType.getId().intValue()))
            .andExpect(jsonPath("$.businessType").value(DEFAULT_BUSINESS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMerchantBusinessType() throws Exception {
        // Get the merchantBusinessType
        restMerchantBusinessTypeMockMvc.perform(get("/api/merchant-business-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMerchantBusinessType() throws Exception {
        // Initialize the database
        merchantBusinessTypeService.save(merchantBusinessType);

        int databaseSizeBeforeUpdate = merchantBusinessTypeRepository.findAll().size();

        // Update the merchantBusinessType
        MerchantBusinessType updatedMerchantBusinessType = merchantBusinessTypeRepository.findOne(merchantBusinessType.getId());
        updatedMerchantBusinessType
            .businessType(UPDATED_BUSINESS_TYPE);

        restMerchantBusinessTypeMockMvc.perform(put("/api/merchant-business-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMerchantBusinessType)))
            .andExpect(status().isOk());

        // Validate the MerchantBusinessType in the database
        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeUpdate);
        MerchantBusinessType testMerchantBusinessType = merchantBusinessTypeList.get(merchantBusinessTypeList.size() - 1);
        assertThat(testMerchantBusinessType.getBusinessType()).isEqualTo(UPDATED_BUSINESS_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingMerchantBusinessType() throws Exception {
        int databaseSizeBeforeUpdate = merchantBusinessTypeRepository.findAll().size();

        // Create the MerchantBusinessType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMerchantBusinessTypeMockMvc.perform(put("/api/merchant-business-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(merchantBusinessType)))
            .andExpect(status().isCreated());

        // Validate the MerchantBusinessType in the database
        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMerchantBusinessType() throws Exception {
        // Initialize the database
        merchantBusinessTypeService.save(merchantBusinessType);

        int databaseSizeBeforeDelete = merchantBusinessTypeRepository.findAll().size();

        // Get the merchantBusinessType
        restMerchantBusinessTypeMockMvc.perform(delete("/api/merchant-business-types/{id}", merchantBusinessType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MerchantBusinessType> merchantBusinessTypeList = merchantBusinessTypeRepository.findAll();
        assertThat(merchantBusinessTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MerchantBusinessType.class);
        MerchantBusinessType merchantBusinessType1 = new MerchantBusinessType();
        merchantBusinessType1.setId(1L);
        MerchantBusinessType merchantBusinessType2 = new MerchantBusinessType();
        merchantBusinessType2.setId(merchantBusinessType1.getId());
        assertThat(merchantBusinessType1).isEqualTo(merchantBusinessType2);
        merchantBusinessType2.setId(2L);
        assertThat(merchantBusinessType1).isNotEqualTo(merchantBusinessType2);
        merchantBusinessType1.setId(null);
        assertThat(merchantBusinessType1).isNotEqualTo(merchantBusinessType2);
    }
}
