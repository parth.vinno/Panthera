package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.MerchantBusinessType;
import com.nollysoft.domain.MerchantBusinessTypeSelected;
import com.nollysoft.repository.MerchantBusinessTypeSelectedRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.MerchantBusinessTypeSelectedService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the MerchantBusinessTypeSelectedResource REST controller.
 *
 * @see MerchantBusinessTypeSelectedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class MerchantBusinessTypeSelectedResourceIntTest {

	private static final String DEFAULT_SPECIFICATION = "AAAAAAAAAA";
	private static final String UPDATED_SPECIFICATION = "BBBBBBBBBB";

	@Autowired
	private MerchantBusinessTypeSelectedRepository merchantBusinessTypeSelectedRepository;

	@Autowired
	private MerchantBusinessTypeSelectedService merchantBusinessTypeSelectedService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restMerchantBusinessTypeSelectedMockMvc;

	private MerchantBusinessTypeSelected merchantBusinessTypeSelected;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		MerchantBusinessTypeSelectedResource merchantBusinessTypeSelectedResource = new MerchantBusinessTypeSelectedResource(
				merchantBusinessTypeSelectedService, executiveSummaryStatusService);
		this.restMerchantBusinessTypeSelectedMockMvc = MockMvcBuilders
				.standaloneSetup(merchantBusinessTypeSelectedResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static MerchantBusinessTypeSelected createEntity(EntityManager em) {
		MerchantBusinessTypeSelected merchantBusinessTypeSelected = new MerchantBusinessTypeSelected()
				.specification(DEFAULT_SPECIFICATION);
		// Add required entity
		MerchantBusinessType merchantBusinessTypeId = MerchantBusinessTypeResourceIntTest.createEntity(em);
		em.persist(merchantBusinessTypeId);
		em.flush();
		merchantBusinessTypeSelected.setMerchantBusinessTypeId(merchantBusinessTypeId);
		return merchantBusinessTypeSelected;
	}

	@Before
	public void initTest() {
		merchantBusinessTypeSelected = createEntity(em);
	}

	@Test
	@Transactional
	public void createMerchantBusinessTypeSelected() throws Exception {
		int databaseSizeBeforeCreate = merchantBusinessTypeSelectedRepository.findAll().size();

		// Create the MerchantBusinessTypeSelected
		restMerchantBusinessTypeSelectedMockMvc
				.perform(post("/api/merchant-business-type-selected").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(merchantBusinessTypeSelected)))
				.andExpect(status().isCreated());

		// Validate the MerchantBusinessTypeSelected in the database
		List<MerchantBusinessTypeSelected> merchantBusinessTypeSelectedList = merchantBusinessTypeSelectedRepository
				.findAll();
		assertThat(merchantBusinessTypeSelectedList).hasSize(databaseSizeBeforeCreate + 1);
		MerchantBusinessTypeSelected testMerchantBusinessTypeSelected = merchantBusinessTypeSelectedList
				.get(merchantBusinessTypeSelectedList.size() - 1);
		assertThat(testMerchantBusinessTypeSelected.getSpecification()).isEqualTo(DEFAULT_SPECIFICATION);
	}

	@Test
	@Transactional
	public void createMerchantBusinessTypeSelectedWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = merchantBusinessTypeSelectedRepository.findAll().size();

		// Create the MerchantBusinessTypeSelected with an existing ID
		merchantBusinessTypeSelected.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restMerchantBusinessTypeSelectedMockMvc
				.perform(post("/api/merchant-business-type-selected").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(merchantBusinessTypeSelected)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<MerchantBusinessTypeSelected> merchantBusinessTypeSelectedList = merchantBusinessTypeSelectedRepository
				.findAll();
		assertThat(merchantBusinessTypeSelectedList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllMerchantBusinessTypeSelecteds() throws Exception {
		// Initialize the database
		merchantBusinessTypeSelectedRepository.saveAndFlush(merchantBusinessTypeSelected);

		// Get all the merchantBusinessTypeSelectedList
		restMerchantBusinessTypeSelectedMockMvc.perform(get("/api/merchant-business-type-selected?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(merchantBusinessTypeSelected.getId().intValue())))
				.andExpect(jsonPath("$.[*].specification").value(hasItem(DEFAULT_SPECIFICATION.toString())));
	}

	@Test
	@Transactional
	public void getMerchantBusinessTypeSelected() throws Exception {
		// Initialize the database
		merchantBusinessTypeSelectedRepository.saveAndFlush(merchantBusinessTypeSelected);

		// Get the merchantBusinessTypeSelected
		restMerchantBusinessTypeSelectedMockMvc
				.perform(get("/api/merchant-business-type-selected/{id}", merchantBusinessTypeSelected.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(merchantBusinessTypeSelected.getId().intValue()))
				.andExpect(jsonPath("$.specification").value(DEFAULT_SPECIFICATION.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingMerchantBusinessTypeSelected() throws Exception {
		// Get the merchantBusinessTypeSelected
		restMerchantBusinessTypeSelectedMockMvc
				.perform(get("/api/merchant-business-type-selected/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateMerchantBusinessTypeSelected() throws Exception {
		// Initialize the database
		merchantBusinessTypeSelectedService.save(merchantBusinessTypeSelected);

		int databaseSizeBeforeUpdate = merchantBusinessTypeSelectedRepository.findAll().size();

		// Update the merchantBusinessTypeSelected
		MerchantBusinessTypeSelected updatedMerchantBusinessTypeSelected = merchantBusinessTypeSelectedRepository
				.findOne(merchantBusinessTypeSelected.getId());
		updatedMerchantBusinessTypeSelected.specification(UPDATED_SPECIFICATION);

		restMerchantBusinessTypeSelectedMockMvc
				.perform(put("/api/merchant-business-type-selected").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedMerchantBusinessTypeSelected)))
				.andExpect(status().isOk());

		// Validate the MerchantBusinessTypeSelected in the database
		List<MerchantBusinessTypeSelected> merchantBusinessTypeSelectedList = merchantBusinessTypeSelectedRepository
				.findAll();
		assertThat(merchantBusinessTypeSelectedList).hasSize(databaseSizeBeforeUpdate);
		MerchantBusinessTypeSelected testMerchantBusinessTypeSelected = merchantBusinessTypeSelectedList
				.get(merchantBusinessTypeSelectedList.size() - 1);
		assertThat(testMerchantBusinessTypeSelected.getSpecification()).isEqualTo(UPDATED_SPECIFICATION);
	}

	@Test
	@Transactional
	public void updateNonExistingMerchantBusinessTypeSelected() throws Exception {
		int databaseSizeBeforeUpdate = merchantBusinessTypeSelectedRepository.findAll().size();

		// Create the MerchantBusinessTypeSelected

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restMerchantBusinessTypeSelectedMockMvc
				.perform(put("/api/merchant-business-type-selected").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(merchantBusinessTypeSelected)))
				.andExpect(status().isCreated());

		// Validate the MerchantBusinessTypeSelected in the database
		List<MerchantBusinessTypeSelected> merchantBusinessTypeSelectedList = merchantBusinessTypeSelectedRepository
				.findAll();
		assertThat(merchantBusinessTypeSelectedList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteMerchantBusinessTypeSelected() throws Exception {
		// Initialize the database
		merchantBusinessTypeSelectedService.save(merchantBusinessTypeSelected);

		int databaseSizeBeforeDelete = merchantBusinessTypeSelectedRepository.findAll().size();

		// Get the merchantBusinessTypeSelected
		restMerchantBusinessTypeSelectedMockMvc
				.perform(delete("/api/merchant-business-type-selected/{id}", merchantBusinessTypeSelected.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<MerchantBusinessTypeSelected> merchantBusinessTypeSelectedList = merchantBusinessTypeSelectedRepository
				.findAll();
		assertThat(merchantBusinessTypeSelectedList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(MerchantBusinessTypeSelected.class);
		MerchantBusinessTypeSelected merchantBusinessTypeSelected1 = new MerchantBusinessTypeSelected();
		merchantBusinessTypeSelected1.setId(1L);
		MerchantBusinessTypeSelected merchantBusinessTypeSelected2 = new MerchantBusinessTypeSelected();
		merchantBusinessTypeSelected2.setId(merchantBusinessTypeSelected1.getId());
		assertThat(merchantBusinessTypeSelected1).isEqualTo(merchantBusinessTypeSelected2);
		merchantBusinessTypeSelected2.setId(2L);
		assertThat(merchantBusinessTypeSelected1).isNotEqualTo(merchantBusinessTypeSelected2);
		merchantBusinessTypeSelected1.setId(null);
		assertThat(merchantBusinessTypeSelected1).isNotEqualTo(merchantBusinessTypeSelected2);
	}
}
