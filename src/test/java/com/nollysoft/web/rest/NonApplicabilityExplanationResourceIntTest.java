package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.NonApplicabilityExplanation;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.NonApplicabilityExplanationRepository;
import com.nollysoft.service.NonApplicabilityExplanationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the NonApplicabilityExplanationResource REST controller.
 *
 * @see NonApplicabilityExplanationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class NonApplicabilityExplanationResourceIntTest {

	private static final String DEFAULT_REASON_REQUIREMENT_NOT_APPLICABLE = "AAAAAAAAAA";
	private static final String UPDATED_REASON_REQUIREMENT_NOT_APPLICABLE = "BBBBBBBBBB";

	@Autowired
	private NonApplicabilityExplanationRepository nonApplicabilityExplanationRepository;

	@Autowired
	private NonApplicabilityExplanationService nonApplicabilityExplanationService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restNonApplicabilityExplanationMockMvc;

	private NonApplicabilityExplanation nonApplicabilityExplanation;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final NonApplicabilityExplanationResource nonApplicabilityExplanationResource = new NonApplicabilityExplanationResource(
				nonApplicabilityExplanationService);
		this.restNonApplicabilityExplanationMockMvc = MockMvcBuilders
				.standaloneSetup(nonApplicabilityExplanationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static NonApplicabilityExplanation createEntity(EntityManager em) {
		NonApplicabilityExplanation nonApplicabilityExplanation = new NonApplicabilityExplanation()
				.reasonRequirementNotApplicable(DEFAULT_REASON_REQUIREMENT_NOT_APPLICABLE);
		// Add required entity
		SaqQuestion saqQuestionId = SaqQuestionResourceIntTest.createEntity(em);
		em.persist(saqQuestionId);
		em.flush();
		nonApplicabilityExplanation.setSaqQuestionId(saqQuestionId);
		return nonApplicabilityExplanation;
	}

	@Before
	public void initTest() {
		nonApplicabilityExplanation = createEntity(em);
	}

	@Test
	@Transactional
	public void createNonApplicabilityExplanation() throws Exception {
		int databaseSizeBeforeCreate = nonApplicabilityExplanationRepository.findAll().size();

		// Create the NonApplicabilityExplanation
		restNonApplicabilityExplanationMockMvc
				.perform(post("/api/non-applicability-explanations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonApplicabilityExplanation)))
				.andExpect(status().isCreated());

		// Validate the NonApplicabilityExplanation in the database
		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeCreate + 1);
		NonApplicabilityExplanation testNonApplicabilityExplanation = nonApplicabilityExplanationList
				.get(nonApplicabilityExplanationList.size() - 1);
		assertThat(testNonApplicabilityExplanation.getReasonRequirementNotApplicable())
				.isEqualTo(DEFAULT_REASON_REQUIREMENT_NOT_APPLICABLE);
	}

	@Test
	@Transactional
	public void createNonApplicabilityExplanationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = nonApplicabilityExplanationRepository.findAll().size();

		// Create the NonApplicabilityExplanation with an existing ID
		nonApplicabilityExplanation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restNonApplicabilityExplanationMockMvc
				.perform(post("/api/non-applicability-explanations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonApplicabilityExplanation)))
				.andExpect(status().isBadRequest());

		// Validate the NonApplicabilityExplanation in the database
		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkReasonRequirementNotApplicableIsRequired() throws Exception {
		int databaseSizeBeforeTest = nonApplicabilityExplanationRepository.findAll().size();
		// set the field null
		nonApplicabilityExplanation.setReasonRequirementNotApplicable(null);

		// Create the NonApplicabilityExplanation, which fails.

		restNonApplicabilityExplanationMockMvc
				.perform(post("/api/non-applicability-explanations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonApplicabilityExplanation)))
				.andExpect(status().isBadRequest());

		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllNonApplicabilityExplanations() throws Exception {
		// Initialize the database
		nonApplicabilityExplanationRepository.saveAndFlush(nonApplicabilityExplanation);

		// Get all the nonApplicabilityExplanationList
		restNonApplicabilityExplanationMockMvc.perform(get("/api/non-applicability-explanations?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(nonApplicabilityExplanation.getId().intValue())))
				.andExpect(jsonPath("$.[*].reasonRequirementNotApplicable")
						.value(hasItem(DEFAULT_REASON_REQUIREMENT_NOT_APPLICABLE.toString())));
	}

	@Test
	@Transactional
	public void getNonApplicabilityExplanation() throws Exception {
		// Initialize the database
		nonApplicabilityExplanationRepository.saveAndFlush(nonApplicabilityExplanation);

		// Get the nonApplicabilityExplanation
		restNonApplicabilityExplanationMockMvc
				.perform(get("/api/non-applicability-explanations/{id}", nonApplicabilityExplanation.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(nonApplicabilityExplanation.getId().intValue()))
				.andExpect(jsonPath("$.reasonRequirementNotApplicable")
						.value(DEFAULT_REASON_REQUIREMENT_NOT_APPLICABLE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingNonApplicabilityExplanation() throws Exception {
		// Get the nonApplicabilityExplanation
		restNonApplicabilityExplanationMockMvc.perform(get("/api/non-applicability-explanations/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateNonApplicabilityExplanation() throws Exception {
		// Initialize the database
		nonApplicabilityExplanationService.save(nonApplicabilityExplanation);

		int databaseSizeBeforeUpdate = nonApplicabilityExplanationRepository.findAll().size();

		// Update the nonApplicabilityExplanation
		NonApplicabilityExplanation updatedNonApplicabilityExplanation = nonApplicabilityExplanationRepository
				.findOne(nonApplicabilityExplanation.getId());
		// Disconnect from session so that the updates on
		// updatedNonApplicabilityExplanation are not directly saved in db
		em.detach(updatedNonApplicabilityExplanation);
		updatedNonApplicabilityExplanation.reasonRequirementNotApplicable(UPDATED_REASON_REQUIREMENT_NOT_APPLICABLE);

		restNonApplicabilityExplanationMockMvc
				.perform(put("/api/non-applicability-explanations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedNonApplicabilityExplanation)))
				.andExpect(status().isOk());

		// Validate the NonApplicabilityExplanation in the database
		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeUpdate);
		NonApplicabilityExplanation testNonApplicabilityExplanation = nonApplicabilityExplanationList
				.get(nonApplicabilityExplanationList.size() - 1);
		assertThat(testNonApplicabilityExplanation.getReasonRequirementNotApplicable())
				.isEqualTo(UPDATED_REASON_REQUIREMENT_NOT_APPLICABLE);
	}

	@Test
	@Transactional
	public void updateNonExistingNonApplicabilityExplanation() throws Exception {
		int databaseSizeBeforeUpdate = nonApplicabilityExplanationRepository.findAll().size();

		// Create the NonApplicabilityExplanation

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restNonApplicabilityExplanationMockMvc
				.perform(put("/api/non-applicability-explanations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonApplicabilityExplanation)))
				.andExpect(status().isCreated());

		// Validate the NonApplicabilityExplanation in the database
		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteNonApplicabilityExplanation() throws Exception {
		// Initialize the database
		nonApplicabilityExplanationService.save(nonApplicabilityExplanation);

		int databaseSizeBeforeDelete = nonApplicabilityExplanationRepository.findAll().size();

		// Get the nonApplicabilityExplanation
		restNonApplicabilityExplanationMockMvc
				.perform(delete("/api/non-applicability-explanations/{id}", nonApplicabilityExplanation.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<NonApplicabilityExplanation> nonApplicabilityExplanationList = nonApplicabilityExplanationRepository
				.findAll();
		assertThat(nonApplicabilityExplanationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(NonApplicabilityExplanation.class);
		NonApplicabilityExplanation nonApplicabilityExplanation1 = new NonApplicabilityExplanation();
		nonApplicabilityExplanation1.setId(1L);
		NonApplicabilityExplanation nonApplicabilityExplanation2 = new NonApplicabilityExplanation();
		nonApplicabilityExplanation2.setId(nonApplicabilityExplanation1.getId());
		assertThat(nonApplicabilityExplanation1).isEqualTo(nonApplicabilityExplanation2);
		nonApplicabilityExplanation2.setId(2L);
		assertThat(nonApplicabilityExplanation1).isNotEqualTo(nonApplicabilityExplanation2);
		nonApplicabilityExplanation1.setId(null);
		assertThat(nonApplicabilityExplanation1).isNotEqualTo(nonApplicabilityExplanation2);
	}
}
