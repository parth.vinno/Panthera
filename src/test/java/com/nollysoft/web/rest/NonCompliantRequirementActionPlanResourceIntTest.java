package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.NonCompliantRequirementActionPlan;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.repository.NonCompliantRequirementActionPlanRepository;
import com.nollysoft.service.NonCompliantRequirementActionPlanService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the NonCompliantRequirementActionPlanResource REST controller.
 *
 * @see NonCompliantRequirementActionPlanResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class NonCompliantRequirementActionPlanResourceIntTest {

	private static final boolean DEFAULT_COMPLIANT = false;
	private static final boolean UPDATED_COMPLIANT = true;

	private static final String DEFAULT_REMEDIATION_PLAN = "Test Default Remediation Plan";
	private static final String UPDATED_REMEDIATION_PLAN = "Test Updated Remediation Plan";

	private static final LocalDate DEFAULT_REMEDIATION_DATE = LocalDate.now(ZoneId.systemDefault());
	private static final LocalDate UPDATED_REMEDIATION_DATE = LocalDate.now(ZoneId.systemDefault()).plusDays(1);
	private static final LocalDate REMEDIATION_DATE_BEFORE_CURRENT_DATE = LocalDate.of(2018, Month.JANUARY, 1);

	@Autowired
	private NonCompliantRequirementActionPlanRepository nonCompliantRequirementActionPlanRepository;

	@Autowired
	private NonCompliantRequirementActionPlanService nonCompliantRequirementActionPlanService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restNonCompliantRequirementActionPlanMockMvc;

	private NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final NonCompliantRequirementActionPlanResource nonCompliantRequirementActionPlanResource = new NonCompliantRequirementActionPlanResource(
				nonCompliantRequirementActionPlanService);
		this.restNonCompliantRequirementActionPlanMockMvc = MockMvcBuilders
				.standaloneSetup(nonCompliantRequirementActionPlanResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static NonCompliantRequirementActionPlan createEntity(EntityManager em) {
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan = new NonCompliantRequirementActionPlan()
				.isCompliant(DEFAULT_COMPLIANT).remediationPlan(DEFAULT_REMEDIATION_PLAN)
				.remediationDate(DEFAULT_REMEDIATION_DATE);
		PcidssRequirement pcidssRequirement = PcidssRequirementResourceIntTest.createEntity(em);
		em.persist(pcidssRequirement);
		em.flush();
		nonCompliantRequirementActionPlan.setPcidssRequirementId(pcidssRequirement);
		return nonCompliantRequirementActionPlan;
	}

	/**
	 * Create list of entity for this test.
	 */
	public List<NonCompliantRequirementActionPlan> createListOfEntity() {
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan1 = createEntity(em);
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = new ArrayList<>();
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan);
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan1);
		return nonCompliantRequirementActionPlanList;
	}

	@Before
	public void initTest() {
		nonCompliantRequirementActionPlan = createEntity(em);
	}

	@Test
	@Transactional
	public void createListOfNonCompliantRequirementActionPlanForMerchant() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = createListOfEntity();
		// Create List of new NonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/list/non-compliant-requirement-action-plans")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlanList)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> list = nonCompliantRequirementActionPlanRepository.findAll();
		assertThat(list).hasSize(databaseSizeBeforeCreate + 2);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = list.get(list.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void createListOfNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = createListOfEntity();
		// Create List of new NonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/list/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlanList)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> list = nonCompliantRequirementActionPlanRepository.findAll();
		assertThat(list).hasSize(databaseSizeBeforeCreate + 2);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = list.get(list.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void updateListOfNonCompliantRequirementActionPlanForMerchant() throws Exception {
		// Creating NonCompliantRequirementActionPlan in database
		nonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanRepository
				.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();

		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = new ArrayList<>();
		nonCompliantRequirementActionPlan.setRemediationPlan(UPDATED_REMEDIATION_PLAN);
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan1 = createEntity(em);
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan1);
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan);

		// Update the NonCompliantRequirementActionPlan with id in the list and create
		// if id doesn't exist
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/list/non-compliant-requirement-action-plans")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlanList)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlans list in the database
		List<NonCompliantRequirementActionPlan> list = nonCompliantRequirementActionPlanRepository.findAll();
		assertThat(list).hasSize(databaseSizeBeforeCreate + 1);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan1 = list.get(0);
		assertThat(testNonCompliantRequirementActionPlan1.getId()).isEqualTo(nonCompliantRequirementActionPlan.getId());
		assertThat(testNonCompliantRequirementActionPlan1.getRemediationPlan()).isEqualTo(UPDATED_REMEDIATION_PLAN);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan2 = list.get(1);
		assertThat(testNonCompliantRequirementActionPlan2.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void updateListOfNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		// Creating NonCompliantRequirementActionPlan in database
		nonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanRepository
				.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();

		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = new ArrayList<>();
		nonCompliantRequirementActionPlan.setRemediationPlan(UPDATED_REMEDIATION_PLAN);
		nonCompliantRequirementActionPlan.setIsCompliant(true);
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan1 = createEntity(em);
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan);
		nonCompliantRequirementActionPlanList.add(nonCompliantRequirementActionPlan1);

		// Update the NonCompliantRequirementActionPlan with id in the list and create
		// if id doesn't exist
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/list/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlanList)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlans list in the database
		List<NonCompliantRequirementActionPlan> list = nonCompliantRequirementActionPlanRepository.findAll();
		assertThat(list).hasSize(databaseSizeBeforeCreate + 1);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan1 = list.get(0);
		assertThat(testNonCompliantRequirementActionPlan1.getId()).isEqualTo(nonCompliantRequirementActionPlan.getId());
		assertThat(testNonCompliantRequirementActionPlan1.getRemediationPlan()).isEqualTo(UPDATED_REMEDIATION_PLAN);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan2 = list.get(1);
		assertThat(testNonCompliantRequirementActionPlan2.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanForMerchant() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();
		// Create the NonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeCreate + 1);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanList
				.get(nonCompliantRequirementActionPlanList.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.isCompliant()).isEqualTo(DEFAULT_COMPLIANT);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Create the NonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeCreate + 1);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanList
				.get(nonCompliantRequirementActionPlanList.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.isCompliant()).isEqualTo(DEFAULT_COMPLIANT);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(DEFAULT_REMEDIATION_PLAN);
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanWithBeForeDateForMerchant() throws Exception {

		// Create the NonCompliantRequirementActionPlan with Remediation Date before
		// Current date
		nonCompliantRequirementActionPlan.setRemediationDate(REMEDIATION_DATE_BEFORE_CURRENT_DATE);

		// An entity with Remediation Date before Current date cannot be created, so
		// this API call must fail

		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanWithBeForeDateForServiceProvider() throws Exception {

		// Create the NonCompliantRequirementActionPlan with Remediation Date before
		// Current date
		nonCompliantRequirementActionPlan.setRemediationDate(REMEDIATION_DATE_BEFORE_CURRENT_DATE);

		// An entity with Remediation Date before Current date cannot be created, so
		// this API call must fail

		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Create the NonCompliantRequirementActionPlan with an existing ID
		nonCompliantRequirementActionPlan.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isBadRequest());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createNonCompliantRequirementActionPlanWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Create the NonCompliantRequirementActionPlan with an existing ID
		nonCompliantRequirementActionPlan.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isBadRequest());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkPcidssRequirementIsRequiredForMerchant() throws Exception {
		// set the field null
		nonCompliantRequirementActionPlan.setPcidssRequirementId(null);
		;

		// Create the NonCompliantRequirementActionPlan, which fails.

		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkPcidssRequirementIsRequiredForServiceProvider() throws Exception {
		// set the field null
		nonCompliantRequirementActionPlan.setPcidssRequirementId(null);

		// Create the NonCompliantRequirementActionPlan, which fails.

		restNonCompliantRequirementActionPlanMockMvc
				.perform(post("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllNonCompliantRequirementActionPlansForMerchant() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanRepository.saveAndFlush(nonCompliantRequirementActionPlan);

		// Get all the nonCompliantRequirementActionPlanList
		restNonCompliantRequirementActionPlanMockMvc
				.perform(get("/api/non-compliant-requirement-action-plans?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(nonCompliantRequirementActionPlan.getId().intValue())))
				.andExpect(jsonPath("$.[*].compliant").value(hasItem(DEFAULT_COMPLIANT)))
				.andExpect(jsonPath("$.[*].remediationPlan").value(hasItem(DEFAULT_REMEDIATION_PLAN.toString())))
				.andExpect(jsonPath("$.[*].remediationDate").value(hasItem(DEFAULT_REMEDIATION_DATE.toString())));
	}

	@Test
	@Transactional
	public void getAllNonCompliantRequirementActionPlansFroServiceProvider() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanRepository.saveAndFlush(nonCompliantRequirementActionPlan);

		// Get all the nonCompliantRequirementActionPlanList
		restNonCompliantRequirementActionPlanMockMvc
				.perform(get("/api/non-compliant-requirement-action-plans/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(nonCompliantRequirementActionPlan.getId().intValue())))
				.andExpect(jsonPath("$.[*].compliant").value(hasItem(DEFAULT_COMPLIANT)))
				.andExpect(jsonPath("$.[*].remediationPlan").value(hasItem(DEFAULT_REMEDIATION_PLAN.toString())))
				.andExpect(jsonPath("$.[*].remediationDate").value(hasItem(DEFAULT_REMEDIATION_DATE.toString())));
	}

	@Test
	@Transactional
	public void updateNonCompliantRequirementActionPlanForMerchant() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanService.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeUpdate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Update the nonCompliantRequirementActionPlan
		NonCompliantRequirementActionPlan updatedNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanRepository
				.findOne(nonCompliantRequirementActionPlan.getId());
		// Disconnect from session so that the updates on
		// updatedNonCompliantRequirementActionPlan are not directly saved in db
		em.detach(updatedNonCompliantRequirementActionPlan);
		updatedNonCompliantRequirementActionPlan.isCompliant(UPDATED_COMPLIANT)
				.remediationPlan(UPDATED_REMEDIATION_PLAN).remediationDate(UPDATED_REMEDIATION_DATE);

		restNonCompliantRequirementActionPlanMockMvc
				.perform(put("/api/non-compliant-requirement-action-plans").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedNonCompliantRequirementActionPlan)))
				.andExpect(status().isOk());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeUpdate);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanList
				.get(nonCompliantRequirementActionPlanList.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.isCompliant()).isEqualTo(UPDATED_COMPLIANT);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(UPDATED_REMEDIATION_PLAN);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationDate()).isEqualTo(UPDATED_REMEDIATION_DATE);
	}

	@Test
	@Transactional
	public void updateNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanService.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeUpdate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Update the nonCompliantRequirementActionPlan
		NonCompliantRequirementActionPlan updatedNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanRepository
				.findOne(nonCompliantRequirementActionPlan.getId());
		// Disconnect from session so that the updates on
		// updatedNonCompliantRequirementActionPlan are not directly saved in db
		em.detach(updatedNonCompliantRequirementActionPlan);
		updatedNonCompliantRequirementActionPlan.isCompliant(UPDATED_COMPLIANT)
				.remediationPlan(UPDATED_REMEDIATION_PLAN).remediationDate(UPDATED_REMEDIATION_DATE);

		restNonCompliantRequirementActionPlanMockMvc
				.perform(put("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedNonCompliantRequirementActionPlan)))
				.andExpect(status().isOk());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeUpdate);
		NonCompliantRequirementActionPlan testNonCompliantRequirementActionPlan = nonCompliantRequirementActionPlanList
				.get(nonCompliantRequirementActionPlanList.size() - 1);
		assertThat(testNonCompliantRequirementActionPlan.isCompliant()).isEqualTo(UPDATED_COMPLIANT);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationPlan()).isEqualTo(UPDATED_REMEDIATION_PLAN);
		assertThat(testNonCompliantRequirementActionPlan.getRemediationDate()).isEqualTo(UPDATED_REMEDIATION_DATE);
	}

	@Test
	@Transactional
	public void updateNonExistingNonCompliantRequirementActionPlanForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Create the NonCompliantRequirementActionPlan

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restNonCompliantRequirementActionPlanMockMvc
				.perform(put("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Create the NonCompliantRequirementActionPlan

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restNonCompliantRequirementActionPlanMockMvc
				.perform(put("/api/non-compliant-requirement-action-plans/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(nonCompliantRequirementActionPlan)))
				.andExpect(status().isCreated());

		// Validate the NonCompliantRequirementActionPlan in the database
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteNonCompliantRequirementActionPlanForMerchant() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanService.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeDelete = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Get the nonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(delete("/api/non-compliant-requirement-action-plans/{id}",
						nonCompliantRequirementActionPlan.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteNonCompliantRequirementActionPlanForServiceProvider() throws Exception {
		// Initialize the database
		nonCompliantRequirementActionPlanService.save(nonCompliantRequirementActionPlan);

		int databaseSizeBeforeDelete = nonCompliantRequirementActionPlanRepository.findAll().size();

		// Get the nonCompliantRequirementActionPlan
		restNonCompliantRequirementActionPlanMockMvc
				.perform(delete("/api/non-compliant-requirement-action-plans/service-provider/{id}",
						nonCompliantRequirementActionPlan.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<NonCompliantRequirementActionPlan> nonCompliantRequirementActionPlanList = nonCompliantRequirementActionPlanRepository
				.findAll();
		assertThat(nonCompliantRequirementActionPlanList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(NonCompliantRequirementActionPlan.class);
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan1 = new NonCompliantRequirementActionPlan();
		nonCompliantRequirementActionPlan1.setId(1L);
		NonCompliantRequirementActionPlan nonCompliantRequirementActionPlan2 = new NonCompliantRequirementActionPlan();
		nonCompliantRequirementActionPlan2.setId(nonCompliantRequirementActionPlan1.getId());
		assertThat(nonCompliantRequirementActionPlan1).isEqualTo(nonCompliantRequirementActionPlan2);
		nonCompliantRequirementActionPlan2.setId(2L);
		assertThat(nonCompliantRequirementActionPlan1).isNotEqualTo(nonCompliantRequirementActionPlan2);
		nonCompliantRequirementActionPlan1.setId(null);
		assertThat(nonCompliantRequirementActionPlan1).isNotEqualTo(nonCompliantRequirementActionPlan2);
	}
}
