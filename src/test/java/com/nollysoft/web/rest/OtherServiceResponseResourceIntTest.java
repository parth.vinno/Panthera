package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.OtherServiceResponse;
import com.nollysoft.domain.OtherServiceResponse.SCOPE;
import com.nollysoft.domain.ServiceType;
import com.nollysoft.repository.OtherServiceResponseRepository;
import com.nollysoft.service.OtherServiceResponseService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OtherServiceResponseResource REST controller.
 *
 * @see OtherServiceResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class OtherServiceResponseResourceIntTest {

	private static final String DEFAULT_OTHER = "AAAAAAAAAA";
	private static final String UPDATED_OTHER = "BBBBBBBBBB";

	private static final SCOPE DEFAULT_SCOPE = SCOPE.SELECTED;

	@Autowired
	private OtherServiceResponseRepository otherServiceResponseRepository;

	@Autowired
	private OtherServiceResponseService otherServiceResponseService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restOtherServiceResponseMockMvc;

	private OtherServiceResponse otherServiceResponse;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		OtherServiceResponseResource otherServiceResponseResource = new OtherServiceResponseResource(
				otherServiceResponseService);
		this.restOtherServiceResponseMockMvc = MockMvcBuilders.standaloneSetup(otherServiceResponseResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static OtherServiceResponse createEntity(EntityManager em) {
		OtherServiceResponse otherServiceResponse = new OtherServiceResponse().other(DEFAULT_OTHER)
				.scope(DEFAULT_SCOPE);
		// Add required entity
		ServiceType serviceTypeId = ServiceTypeResourceIntTest.createEntity(em);
		em.persist(serviceTypeId);
		em.flush();
		otherServiceResponse.setServiceTypeId(serviceTypeId);
		return otherServiceResponse;
	}

	@Before
	public void initTest() {
		otherServiceResponse = createEntity(em);
	}

	@Test
	@Transactional
	public void createOtherServiceResponse() throws Exception {
		int databaseSizeBeforeCreate = otherServiceResponseRepository.findAll().size();

		// Create the OtherServiceResponse
		restOtherServiceResponseMockMvc
				.perform(post("/api/other-service-responses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(otherServiceResponse)))
				.andExpect(status().isCreated());

		// Validate the OtherServiceResponse in the database
		List<OtherServiceResponse> otherServiceResponseList = otherServiceResponseRepository.findAll();
		assertThat(otherServiceResponseList).hasSize(databaseSizeBeforeCreate + 1);
		OtherServiceResponse testOtherServiceResponse = otherServiceResponseList
				.get(otherServiceResponseList.size() - 1);
		assertThat(testOtherServiceResponse.getOther()).isEqualTo(DEFAULT_OTHER);
	}

	@Test
	@Transactional
	public void createOtherServiceResponseWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = otherServiceResponseRepository.findAll().size();

		// Create the OtherServiceResponse with an existing ID
		otherServiceResponse.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restOtherServiceResponseMockMvc
				.perform(post("/api/other-service-responses/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(otherServiceResponse)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<OtherServiceResponse> otherServiceResponseList = otherServiceResponseRepository.findAll();
		assertThat(otherServiceResponseList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllOtherServiceResponses() throws Exception {
		// Initialize the database
		otherServiceResponseRepository.saveAndFlush(otherServiceResponse);

		// Get all the otherServiceResponseList
		restOtherServiceResponseMockMvc.perform(get("/api/other-service-responses/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(otherServiceResponse.getId().intValue())))
				.andExpect(jsonPath("$.[*].other").value(hasItem(DEFAULT_OTHER.toString())));
	}

	@Test
	@Transactional
	public void getOtherServiceResponse() throws Exception {
		// Initialize the database
		otherServiceResponseRepository.saveAndFlush(otherServiceResponse);

		// Get the otherServiceResponse
		restOtherServiceResponseMockMvc
				.perform(get("/api/other-service-responses/service-provider/{id}", otherServiceResponse.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(otherServiceResponse.getId().intValue()))
				.andExpect(jsonPath("$.other").value(DEFAULT_OTHER.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingOtherServiceResponse() throws Exception {
		// Get the otherServiceResponse
		restOtherServiceResponseMockMvc
				.perform(get("/api/other-service-responses/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateOtherServiceResponse() throws Exception {
		// Initialize the database
		otherServiceResponseService.save(otherServiceResponse);

		int databaseSizeBeforeUpdate = otherServiceResponseRepository.findAll().size();

		// Update the otherServiceResponse
		OtherServiceResponse updatedOtherServiceResponse = otherServiceResponseRepository
				.findOne(otherServiceResponse.getId());
		updatedOtherServiceResponse.other(UPDATED_OTHER);

		restOtherServiceResponseMockMvc
				.perform(
						put("/api/other-service-responses/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(updatedOtherServiceResponse)))
				.andExpect(status().isOk());

		// Validate the OtherServiceResponse in the database
		List<OtherServiceResponse> otherServiceResponseList = otherServiceResponseRepository.findAll();
		assertThat(otherServiceResponseList).hasSize(databaseSizeBeforeUpdate);
		OtherServiceResponse testOtherServiceResponse = otherServiceResponseList
				.get(otherServiceResponseList.size() - 1);
		assertThat(testOtherServiceResponse.getOther()).isEqualTo(UPDATED_OTHER);
	}

	@Test
	@Transactional
	public void updateNonExistingOtherServiceResponse() throws Exception {
		int databaseSizeBeforeUpdate = otherServiceResponseRepository.findAll().size();

		// Create the OtherServiceResponse

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restOtherServiceResponseMockMvc
				.perform(
						put("/api/other-service-responses/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(otherServiceResponse)))
				.andExpect(status().isCreated());

		// Validate the OtherServiceResponse in the database
		List<OtherServiceResponse> otherServiceResponseList = otherServiceResponseRepository.findAll();
		assertThat(otherServiceResponseList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteOtherServiceResponse() throws Exception {
		// Initialize the database
		otherServiceResponseService.save(otherServiceResponse);

		int databaseSizeBeforeDelete = otherServiceResponseRepository.findAll().size();

		// Get the otherServiceResponse
		restOtherServiceResponseMockMvc
				.perform(delete("/api/other-service-responses/service-provider/{id}", otherServiceResponse.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<OtherServiceResponse> otherServiceResponseList = otherServiceResponseRepository.findAll();
		assertThat(otherServiceResponseList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(OtherServiceResponse.class);
		OtherServiceResponse otherServiceResponse1 = new OtherServiceResponse();
		otherServiceResponse1.setId(1L);
		OtherServiceResponse otherServiceResponse2 = new OtherServiceResponse();
		otherServiceResponse2.setId(otherServiceResponse1.getId());
		assertThat(otherServiceResponse1).isEqualTo(otherServiceResponse2);
		otherServiceResponse2.setId(2L);
		assertThat(otherServiceResponse1).isNotEqualTo(otherServiceResponse2);
		otherServiceResponse1.setId(null);
		assertThat(otherServiceResponse1).isNotEqualTo(otherServiceResponse2);
	}
}
