package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.PaymentApplication;
import com.nollysoft.domain.PaymentApplicationList;
import com.nollysoft.repository.PaymentApplicationListRepository;
import com.nollysoft.service.PaymentApplicationListService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PaymentApplicationListResource REST controller.
 *
 * @see PaymentApplicationListResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentApplicationListResourceIntTest {

    private static final String DEFAULT_PAYMENT_APPLICATION_NAME = "AAAAAAAAAA";

    private static final String DEFAULT_VERSION_NUMBER = "AAAAAAAAAA";

    private static final String DEFAULT_APPLICATION_VENDOR = "AAAAAAAAAA";

    private static final Boolean DEFAULT_IS_APPLICATION_PADSS_LISTED = false;

    private static final LocalDate DEFAULT_EXPIRY_DATE = LocalDate.ofEpochDay(0L);

    @Autowired
    private PaymentApplicationListRepository paymentApplicationListRepository;

    @Autowired
    private PaymentApplicationListService paymentApplicationListService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentApplicationListMockMvc;

    private PaymentApplicationList paymentApplicationList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PaymentApplicationListResource paymentApplicationListResource = new PaymentApplicationListResource(paymentApplicationListService);
        this.restPaymentApplicationListMockMvc = MockMvcBuilders.standaloneSetup(paymentApplicationListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentApplicationList createEntity(EntityManager em) {
        PaymentApplicationList paymentApplicationList = new PaymentApplicationList()
            .paymentApplicationName(DEFAULT_PAYMENT_APPLICATION_NAME)
            .versionNumber(DEFAULT_VERSION_NUMBER)
            .applicationVendor(DEFAULT_APPLICATION_VENDOR)
            .isApplicationPadssListed(DEFAULT_IS_APPLICATION_PADSS_LISTED)
            .expiryDate(DEFAULT_EXPIRY_DATE);
        
		// Add required entity
		PaymentApplication paymentApplicationId = PaymentApplicationResourceIntTest.createEntity(em);
		em.persist(paymentApplicationId);
		em.flush();
		paymentApplicationList.setPaymentApplicationId(paymentApplicationId);
		return paymentApplicationList;
    }

    @Before
    public void initTest() {
        paymentApplicationList = createEntity(em);
    }

    @Test
    @Transactional
    public void checkPaymentApplicationNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentApplicationListRepository.findAll().size();
        // set the field null
        paymentApplicationList.setPaymentApplicationName(null);

        // Create the PaymentApplicationList, which fails.

        restPaymentApplicationListMockMvc.perform(post("/api/payment-application-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentApplicationList)))
            .andExpect(status().isBadRequest());

        List<PaymentApplicationList> paymentApplicationListList = paymentApplicationListRepository.findAll();
        assertThat(paymentApplicationListList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkApplicationVendorIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentApplicationListRepository.findAll().size();
        // set the field null
        paymentApplicationList.setApplicationVendor(null);

        // Create the PaymentApplicationList, which fails.

        restPaymentApplicationListMockMvc.perform(post("/api/payment-application-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentApplicationList)))
            .andExpect(status().isBadRequest());

        List<PaymentApplicationList> paymentApplicationListList = paymentApplicationListRepository.findAll();
        assertThat(paymentApplicationListList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentApplicationLists() throws Exception {
        // Initialize the database
        paymentApplicationListRepository.saveAndFlush(paymentApplicationList);

        // Get all the paymentApplicationListList
        restPaymentApplicationListMockMvc.perform(get("/api/payment-application-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentApplicationList.getId().intValue())))
            .andExpect(jsonPath("$.[*].paymentApplicationName").value(hasItem(DEFAULT_PAYMENT_APPLICATION_NAME.toString())))
            .andExpect(jsonPath("$.[*].versionNumber").value(hasItem(DEFAULT_VERSION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].applicationVendor").value(hasItem(DEFAULT_APPLICATION_VENDOR.toString())))
            .andExpect(jsonPath("$.[*].isApplicationPadssListed").value(hasItem(DEFAULT_IS_APPLICATION_PADSS_LISTED.booleanValue())))
            .andExpect(jsonPath("$.[*].expiryDate").value(hasItem(DEFAULT_EXPIRY_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPaymentApplicationList() throws Exception {
        // Initialize the database
        paymentApplicationListRepository.saveAndFlush(paymentApplicationList);

        // Get the paymentApplicationList
        restPaymentApplicationListMockMvc.perform(get("/api/payment-application-lists/{id}", paymentApplicationList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentApplicationList.getId().intValue()))
            .andExpect(jsonPath("$.paymentApplicationName").value(DEFAULT_PAYMENT_APPLICATION_NAME.toString()))
            .andExpect(jsonPath("$.versionNumber").value(DEFAULT_VERSION_NUMBER.toString()))
            .andExpect(jsonPath("$.applicationVendor").value(DEFAULT_APPLICATION_VENDOR.toString()))
            .andExpect(jsonPath("$.isApplicationPadssListed").value(DEFAULT_IS_APPLICATION_PADSS_LISTED.booleanValue()))
            .andExpect(jsonPath("$.expiryDate").value(DEFAULT_EXPIRY_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentApplicationList() throws Exception {
        // Get the paymentApplicationList
        restPaymentApplicationListMockMvc.perform(get("/api/payment-application-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deletePaymentApplicationList() throws Exception {
        // Initialize the database
        paymentApplicationListService.save(paymentApplicationList);

        int databaseSizeBeforeDelete = paymentApplicationListRepository.findAll().size();

        // Get the paymentApplicationList
        restPaymentApplicationListMockMvc.perform(delete("/api/payment-application-lists/{id}", paymentApplicationList.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentApplicationList> paymentApplicationListList = paymentApplicationListRepository.findAll();
        assertThat(paymentApplicationListList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentApplicationList.class);
        PaymentApplicationList paymentApplicationList1 = new PaymentApplicationList();
        paymentApplicationList1.setId(1L);
        PaymentApplicationList paymentApplicationList2 = new PaymentApplicationList();
        paymentApplicationList2.setId(paymentApplicationList1.getId());
        assertThat(paymentApplicationList1).isEqualTo(paymentApplicationList2);
        paymentApplicationList2.setId(2L);
        assertThat(paymentApplicationList1).isNotEqualTo(paymentApplicationList2);
        paymentApplicationList1.setId(null);
        assertThat(paymentApplicationList1).isNotEqualTo(paymentApplicationList2);
    }
}
