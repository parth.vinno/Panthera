package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.PaymentApplication;
import com.nollysoft.repository.PaymentApplicationRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentApplicationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentApplicationResource REST controller.
 *
 * @see PaymentApplicationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentApplicationResourceIntTest {

	private static final Boolean DEFAULT_IS_PAYMENT_APPLICATION_MORE_THAN_ONE = false;
	private static final Boolean UPDATED_IS_PAYMENT_APPLICATION_MORE_THAN_ONE = true;

	@Autowired
	private PaymentApplicationRepository paymentApplicationRepository;

	@Autowired
	private PaymentApplicationService paymentApplicationService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restPaymentApplicationMockMvc;

	private PaymentApplication paymentApplication;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PaymentApplicationResource paymentApplicationResource = new PaymentApplicationResource(
				paymentApplicationService, executiveSummaryStatusService);
		this.restPaymentApplicationMockMvc = MockMvcBuilders.standaloneSetup(paymentApplicationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static PaymentApplication createEntity(EntityManager em) {
		PaymentApplication paymentApplication = new PaymentApplication()
				.isPaymentApplicationMoreThanOne(DEFAULT_IS_PAYMENT_APPLICATION_MORE_THAN_ONE);
		return paymentApplication;
	}

	@Before
	public void initTest() {
		paymentApplication = createEntity(em);
	}

	@Test
	@Transactional
	public void createPaymentApplication() throws Exception {
		int databaseSizeBeforeCreate = paymentApplicationRepository.findAll().size();

		// Create the PaymentApplication
		restPaymentApplicationMockMvc
				.perform(post("/api/payment-applications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentApplication)))
				.andExpect(status().isCreated());

		// Validate the PaymentApplication in the database
		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeCreate + 1);
		PaymentApplication testPaymentApplication = paymentApplicationList.get(paymentApplicationList.size() - 1);
		assertThat(testPaymentApplication.isIsPaymentApplicationMoreThanOne())
				.isEqualTo(DEFAULT_IS_PAYMENT_APPLICATION_MORE_THAN_ONE);
	}

	@Test
	@Transactional
	public void createPaymentApplicationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = paymentApplicationRepository.findAll().size();

		// Create the PaymentApplication with an existing ID
		paymentApplication.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPaymentApplicationMockMvc
				.perform(post("/api/payment-applications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentApplication)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkIsPaymentApplicationMoreThanOneIsRequired() throws Exception {
		int databaseSizeBeforeTest = paymentApplicationRepository.findAll().size();
		// set the field null
		paymentApplication.setIsPaymentApplicationMoreThanOne(null);

		// Create the PaymentApplication, which fails.

		restPaymentApplicationMockMvc
				.perform(post("/api/payment-applications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentApplication)))
				.andExpect(status().isBadRequest());

		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllPaymentApplications() throws Exception {
		// Initialize the database
		paymentApplicationRepository.saveAndFlush(paymentApplication);

		// Get all the paymentApplicationList
		restPaymentApplicationMockMvc.perform(get("/api/payment-applications?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(paymentApplication.getId().intValue())))
				.andExpect(jsonPath("$.[*].isPaymentApplicationMoreThanOne")
						.value(hasItem(DEFAULT_IS_PAYMENT_APPLICATION_MORE_THAN_ONE.booleanValue())));
	}

	@Test
	@Transactional
	public void getPaymentApplication() throws Exception {
		// Initialize the database
		paymentApplicationRepository.saveAndFlush(paymentApplication);

		// Get the paymentApplication
		restPaymentApplicationMockMvc.perform(get("/api/payment-applications/{id}", paymentApplication.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(paymentApplication.getId().intValue()))
				.andExpect(jsonPath("$.isPaymentApplicationMoreThanOne")
						.value(DEFAULT_IS_PAYMENT_APPLICATION_MORE_THAN_ONE.booleanValue()));
	}

	@Test
	@Transactional
	public void getNonExistingPaymentApplication() throws Exception {
		// Get the paymentApplication
		restPaymentApplicationMockMvc.perform(get("/api/payment-applications/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updatePaymentApplication() throws Exception {
		// Initialize the database
		paymentApplicationService.save(paymentApplication);

		int databaseSizeBeforeUpdate = paymentApplicationRepository.findAll().size();

		// Update the paymentApplication
		PaymentApplication updatedPaymentApplication = paymentApplicationRepository.findOne(paymentApplication.getId());
		updatedPaymentApplication.isPaymentApplicationMoreThanOne(UPDATED_IS_PAYMENT_APPLICATION_MORE_THAN_ONE);

		restPaymentApplicationMockMvc
				.perform(put("/api/payment-applications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedPaymentApplication)))
				.andExpect(status().isOk());

		// Validate the PaymentApplication in the database
		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeUpdate);
		PaymentApplication testPaymentApplication = paymentApplicationList.get(paymentApplicationList.size() - 1);
		assertThat(testPaymentApplication.isIsPaymentApplicationMoreThanOne())
				.isEqualTo(UPDATED_IS_PAYMENT_APPLICATION_MORE_THAN_ONE);
	}

	@Test
	@Transactional
	public void updateNonExistingPaymentApplication() throws Exception {
		int databaseSizeBeforeUpdate = paymentApplicationRepository.findAll().size();

		// Create the PaymentApplication

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPaymentApplicationMockMvc
				.perform(put("/api/payment-applications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentApplication)))
				.andExpect(status().isCreated());

		// Validate the PaymentApplication in the database
		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePaymentApplication() throws Exception {
		// Initialize the database
		paymentApplicationService.save(paymentApplication);

		int databaseSizeBeforeDelete = paymentApplicationRepository.findAll().size();

		// Get the paymentApplication
		restPaymentApplicationMockMvc.perform(delete("/api/payment-applications/{id}", paymentApplication.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<PaymentApplication> paymentApplicationList = paymentApplicationRepository.findAll();
		assertThat(paymentApplicationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(PaymentApplication.class);
		PaymentApplication paymentApplication1 = new PaymentApplication();
		paymentApplication1.setId(1L);
		PaymentApplication paymentApplication2 = new PaymentApplication();
		paymentApplication2.setId(paymentApplication1.getId());
		assertThat(paymentApplication1).isEqualTo(paymentApplication2);
		paymentApplication2.setId(2L);
		assertThat(paymentApplication1).isNotEqualTo(paymentApplication2);
		paymentApplication1.setId(null);
		assertThat(paymentApplication1).isNotEqualTo(paymentApplication2);
	}
}
