package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.PaymentCardBusinessDescription;
import com.nollysoft.repository.PaymentCardBusinessDescriptionRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentCardBusinessDescriptionService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentCardBusinessDescriptionResource REST controller.
 *
 * @see PaymentCardBusinessDescriptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentCardBusinessDescriptionResourceIntTest {

    private static final String DEFAULT_DESCRIPTION_TRANSMIT = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSMIT = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_IMPACT = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_IMPACT = "BBBBBBBBBB";

    @Autowired
    private PaymentCardBusinessDescriptionRepository paymentCardBusinessDescriptionRepository;

    @Autowired
    private PaymentCardBusinessDescriptionService paymentCardBusinessDescriptionService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentCardBusinessDescriptionMockMvc;

    private PaymentCardBusinessDescription paymentCardBusinessDescription;

    @Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PaymentCardBusinessDescriptionResource paymentCardBusinessDescriptionResource = new PaymentCardBusinessDescriptionResource(
				paymentCardBusinessDescriptionService, executiveSummaryStatusService);
		this.restPaymentCardBusinessDescriptionMockMvc = MockMvcBuilders
				.standaloneSetup(paymentCardBusinessDescriptionResource)
				.setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentCardBusinessDescription createEntity(EntityManager em) {
        PaymentCardBusinessDescription paymentCardBusinessDescription = new PaymentCardBusinessDescription()
            .descriptionTransmit(DEFAULT_DESCRIPTION_TRANSMIT)
            .descriptionImpact(DEFAULT_DESCRIPTION_IMPACT);
        return paymentCardBusinessDescription;
    }

    @Before
    public void initTest() {
        paymentCardBusinessDescription = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentCardBusinessDescription() throws Exception {
        int databaseSizeBeforeCreate = paymentCardBusinessDescriptionRepository.findAll().size();

        // Create the PaymentCardBusinessDescription
        restPaymentCardBusinessDescriptionMockMvc.perform(post("/api/payment-card-business-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCardBusinessDescription)))
            .andExpect(status().isCreated());

        // Validate the PaymentCardBusinessDescription in the database
        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentCardBusinessDescription testPaymentCardBusinessDescription = paymentCardBusinessDescriptionList.get(paymentCardBusinessDescriptionList.size() - 1);
        assertThat(testPaymentCardBusinessDescription.getDescriptionTransmit()).isEqualTo(DEFAULT_DESCRIPTION_TRANSMIT);
        assertThat(testPaymentCardBusinessDescription.getDescriptionImpact()).isEqualTo(DEFAULT_DESCRIPTION_IMPACT);
    }

    @Test
    @Transactional
    public void createPaymentCardBusinessDescriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentCardBusinessDescriptionRepository.findAll().size();

        // Create the PaymentCardBusinessDescription with an existing ID
        paymentCardBusinessDescription.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentCardBusinessDescriptionMockMvc.perform(post("/api/payment-card-business-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCardBusinessDescription)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionTransmitIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentCardBusinessDescriptionRepository.findAll().size();
        // set the field null
        paymentCardBusinessDescription.setDescriptionTransmit(null);

        // Create the PaymentCardBusinessDescription, which fails.

        restPaymentCardBusinessDescriptionMockMvc.perform(post("/api/payment-card-business-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCardBusinessDescription)))
            .andExpect(status().isBadRequest());

        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentCardBusinessDescriptions() throws Exception {
        // Initialize the database
        paymentCardBusinessDescriptionRepository.saveAndFlush(paymentCardBusinessDescription);

        // Get all the paymentCardBusinessDescriptionList
        restPaymentCardBusinessDescriptionMockMvc.perform(get("/api/payment-card-business-descriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentCardBusinessDescription.getId().intValue())))
            .andExpect(jsonPath("$.[*].descriptionTransmit").value(hasItem(DEFAULT_DESCRIPTION_TRANSMIT.toString())))
            .andExpect(jsonPath("$.[*].descriptionImpact").value(hasItem(DEFAULT_DESCRIPTION_IMPACT.toString())));
    }

    @Test
    @Transactional
    public void getPaymentCardBusinessDescription() throws Exception {
        // Initialize the database
        paymentCardBusinessDescriptionRepository.saveAndFlush(paymentCardBusinessDescription);

        // Get the paymentCardBusinessDescription
        restPaymentCardBusinessDescriptionMockMvc.perform(get("/api/payment-card-business-descriptions/{id}", paymentCardBusinessDescription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentCardBusinessDescription.getId().intValue()))
            .andExpect(jsonPath("$.descriptionTransmit").value(DEFAULT_DESCRIPTION_TRANSMIT.toString()))
            .andExpect(jsonPath("$.descriptionImpact").value(DEFAULT_DESCRIPTION_IMPACT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentCardBusinessDescription() throws Exception {
        // Get the paymentCardBusinessDescription
        restPaymentCardBusinessDescriptionMockMvc.perform(get("/api/payment-card-business-descriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentCardBusinessDescription() throws Exception {
        // Initialize the database
        paymentCardBusinessDescriptionService.save(paymentCardBusinessDescription);

        int databaseSizeBeforeUpdate = paymentCardBusinessDescriptionRepository.findAll().size();

        // Update the paymentCardBusinessDescription
        PaymentCardBusinessDescription updatedPaymentCardBusinessDescription = paymentCardBusinessDescriptionRepository.findOne(paymentCardBusinessDescription.getId());
        updatedPaymentCardBusinessDescription
            .descriptionTransmit(UPDATED_DESCRIPTION_TRANSMIT)
            .descriptionImpact(UPDATED_DESCRIPTION_IMPACT);

        restPaymentCardBusinessDescriptionMockMvc.perform(put("/api/payment-card-business-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentCardBusinessDescription)))
            .andExpect(status().isOk());

        // Validate the PaymentCardBusinessDescription in the database
        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeUpdate);
        PaymentCardBusinessDescription testPaymentCardBusinessDescription = paymentCardBusinessDescriptionList.get(paymentCardBusinessDescriptionList.size() - 1);
        assertThat(testPaymentCardBusinessDescription.getDescriptionTransmit()).isEqualTo(UPDATED_DESCRIPTION_TRANSMIT);
        assertThat(testPaymentCardBusinessDescription.getDescriptionImpact()).isEqualTo(UPDATED_DESCRIPTION_IMPACT);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentCardBusinessDescription() throws Exception {
        int databaseSizeBeforeUpdate = paymentCardBusinessDescriptionRepository.findAll().size();

        // Create the PaymentCardBusinessDescription

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaymentCardBusinessDescriptionMockMvc.perform(put("/api/payment-card-business-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCardBusinessDescription)))
            .andExpect(status().isCreated());

        // Validate the PaymentCardBusinessDescription in the database
        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaymentCardBusinessDescription() throws Exception {
        // Initialize the database
        paymentCardBusinessDescriptionService.save(paymentCardBusinessDescription);

        int databaseSizeBeforeDelete = paymentCardBusinessDescriptionRepository.findAll().size();

        // Get the paymentCardBusinessDescription
        restPaymentCardBusinessDescriptionMockMvc.perform(delete("/api/payment-card-business-descriptions/{id}", paymentCardBusinessDescription.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentCardBusinessDescription> paymentCardBusinessDescriptionList = paymentCardBusinessDescriptionRepository.findAll();
        assertThat(paymentCardBusinessDescriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentCardBusinessDescription.class);
        PaymentCardBusinessDescription paymentCardBusinessDescription1 = new PaymentCardBusinessDescription();
        paymentCardBusinessDescription1.setId(1L);
        PaymentCardBusinessDescription paymentCardBusinessDescription2 = new PaymentCardBusinessDescription();
        paymentCardBusinessDescription2.setId(paymentCardBusinessDescription1.getId());
        assertThat(paymentCardBusinessDescription1).isEqualTo(paymentCardBusinessDescription2);
        paymentCardBusinessDescription2.setId(2L);
        assertThat(paymentCardBusinessDescription1).isNotEqualTo(paymentCardBusinessDescription2);
        paymentCardBusinessDescription1.setId(null);
        assertThat(paymentCardBusinessDescription1).isNotEqualTo(paymentCardBusinessDescription2);
    }
}
