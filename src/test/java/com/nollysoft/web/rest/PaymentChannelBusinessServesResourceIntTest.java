package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.PaymentChannelBusinessServes;
import com.nollysoft.domain.PaymentChannel;
import com.nollysoft.repository.PaymentChannelBusinessServesRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentChannelBusinessServesService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentChannelBusinessServesResource REST controller.
 *
 * @see PaymentChannelBusinessServesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentChannelBusinessServesResourceIntTest {

	@Autowired
	private PaymentChannelBusinessServesRepository paymentChannelBusinessServesRepository;

	@Autowired
	private PaymentChannelBusinessServesService paymentChannelBusinessServesService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restPaymentChannelBusinessServesMockMvc;

	private PaymentChannelBusinessServes paymentChannelBusinessServes;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PaymentChannelBusinessServesResource paymentChannelBusinessServesResource = new PaymentChannelBusinessServesResource(
				paymentChannelBusinessServesService, executiveSummaryStatusService);
		this.restPaymentChannelBusinessServesMockMvc = MockMvcBuilders
				.standaloneSetup(paymentChannelBusinessServesResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static PaymentChannelBusinessServes createEntity(EntityManager em) {
		PaymentChannelBusinessServes paymentChannelBusinessServes = new PaymentChannelBusinessServes();
		// Add required entity
		PaymentChannel paymentChannelId = PaymentChannelResourceIntTest.createEntity(em);
		em.persist(paymentChannelId);
		em.flush();
		paymentChannelBusinessServes.setPaymentChannelId(paymentChannelId);
		return paymentChannelBusinessServes;
	}

	@Before
	public void initTest() {
		paymentChannelBusinessServes = createEntity(em);
	}

	@Test
	@Transactional
	public void createPaymentChannelBusinessServes() throws Exception {
		int databaseSizeBeforeCreate = paymentChannelBusinessServesRepository.findAll().size();

		// Create the PaymentChannelBusinessServes
		restPaymentChannelBusinessServesMockMvc
				.perform(post("/api/payment-channel-business-serves").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelBusinessServes)))
				.andExpect(status().isCreated());

		// Validate the PaymentChannelBusinessServes in the database
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = paymentChannelBusinessServesRepository
				.findAll();
		assertThat(paymentChannelBusinessServesList).hasSize(databaseSizeBeforeCreate + 1);
	}

	@Test
	@Transactional
	public void createPaymentChannelBusinessServesWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = paymentChannelBusinessServesRepository.findAll().size();

		// Create the PaymentChannelBusinessServes with an existing ID
		paymentChannelBusinessServes.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPaymentChannelBusinessServesMockMvc
				.perform(post("/api/payment-channel-business-serves").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelBusinessServes)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = paymentChannelBusinessServesRepository
				.findAll();
		assertThat(paymentChannelBusinessServesList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllPaymentChannelBusinessServes() throws Exception {
		// Initialize the database
		paymentChannelBusinessServesRepository.saveAndFlush(paymentChannelBusinessServes);

		// Get all the paymentChannelBusinessServesList
		restPaymentChannelBusinessServesMockMvc.perform(get("/api/payment-channel-business-serves?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(paymentChannelBusinessServes.getId().intValue())));
	}

	@Test
	@Transactional
	public void getPaymentChannelBusinessServes() throws Exception {
		// Initialize the database
		paymentChannelBusinessServesRepository.saveAndFlush(paymentChannelBusinessServes);

		// Get the paymentChannelBusinessServes
		restPaymentChannelBusinessServesMockMvc
				.perform(get("/api/payment-channel-business-serves/{id}", paymentChannelBusinessServes.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(paymentChannelBusinessServes.getId().intValue()));
	}

	@Test
	@Transactional
	public void getNonExistingPaymentChannelBusinessServes() throws Exception {
		// Get the paymentChannelBusinessServes
		restPaymentChannelBusinessServesMockMvc
				.perform(get("/api/payment-channel-business-serves/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updatePaymentChannelBusinessServes() throws Exception {
		// Initialize the database
		paymentChannelBusinessServesService.save(paymentChannelBusinessServes);

		int databaseSizeBeforeUpdate = paymentChannelBusinessServesRepository.findAll().size();

		// Update the paymentChannelBusinessServes
		PaymentChannelBusinessServes updatedPaymentChannelBusinessServes = paymentChannelBusinessServesRepository
				.findOne(paymentChannelBusinessServes.getId());

		restPaymentChannelBusinessServesMockMvc
				.perform(put("/api/payment-channel-business-serves").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedPaymentChannelBusinessServes)))
				.andExpect(status().isOk());

		// Validate the PaymentChannelBusinessServes in the database
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = paymentChannelBusinessServesRepository
				.findAll();
		assertThat(paymentChannelBusinessServesList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	public void updateNonExistingPaymentChannelBusinessServes() throws Exception {
		int databaseSizeBeforeUpdate = paymentChannelBusinessServesRepository.findAll().size();

		// Create the PaymentChannelBusinessServes

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPaymentChannelBusinessServesMockMvc
				.perform(put("/api/payment-channel-business-serves").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelBusinessServes)))
				.andExpect(status().isCreated());

		// Validate the PaymentChannelBusinessServes in the database
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = paymentChannelBusinessServesRepository
				.findAll();
		assertThat(paymentChannelBusinessServesList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePaymentChannelBusinessServes() throws Exception {
		// Initialize the database
		paymentChannelBusinessServesService.save(paymentChannelBusinessServes);

		int databaseSizeBeforeDelete = paymentChannelBusinessServesRepository.findAll().size();

		// Get the paymentChannelBusinessServes
		restPaymentChannelBusinessServesMockMvc
				.perform(delete("/api/payment-channel-business-serves/{id}", paymentChannelBusinessServes.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<PaymentChannelBusinessServes> paymentChannelBusinessServesList = paymentChannelBusinessServesRepository
				.findAll();
		assertThat(paymentChannelBusinessServesList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(PaymentChannelBusinessServes.class);
		PaymentChannelBusinessServes paymentChannelBusinessServes1 = new PaymentChannelBusinessServes();
		paymentChannelBusinessServes1.setId(1L);
		PaymentChannelBusinessServes paymentChannelBusinessServes2 = new PaymentChannelBusinessServes();
		paymentChannelBusinessServes2.setId(paymentChannelBusinessServes1.getId());
		assertThat(paymentChannelBusinessServes1).isEqualTo(paymentChannelBusinessServes2);
		paymentChannelBusinessServes2.setId(2L);
		assertThat(paymentChannelBusinessServes1).isNotEqualTo(paymentChannelBusinessServes2);
		paymentChannelBusinessServes1.setId(null);
		assertThat(paymentChannelBusinessServes1).isNotEqualTo(paymentChannelBusinessServes2);
	}
}
