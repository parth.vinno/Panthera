package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.PaymentChannel;
import com.nollysoft.repository.PaymentChannelRepository;
import com.nollysoft.service.PaymentChannelService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentChannelResource REST controller.
 *
 * @see PaymentChannelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentChannelResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private PaymentChannelRepository paymentChannelRepository;

    @Autowired
    private PaymentChannelService paymentChannelService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentChannelMockMvc;

    private PaymentChannel paymentChannel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PaymentChannelResource paymentChannelResource = new PaymentChannelResource(paymentChannelService);
        this.restPaymentChannelMockMvc = MockMvcBuilders.standaloneSetup(paymentChannelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentChannel createEntity(EntityManager em) {
        PaymentChannel paymentChannel = new PaymentChannel()
            .name(DEFAULT_NAME);
        return paymentChannel;
    }

    @Before
    public void initTest() {
        paymentChannel = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentChannel() throws Exception {
        int databaseSizeBeforeCreate = paymentChannelRepository.findAll().size();

        // Create the PaymentChannel
        restPaymentChannelMockMvc.perform(post("/api/payment-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentChannel)))
            .andExpect(status().isCreated());

        // Validate the PaymentChannel in the database
        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentChannel testPaymentChannel = paymentChannelList.get(paymentChannelList.size() - 1);
        assertThat(testPaymentChannel.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createPaymentChannelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentChannelRepository.findAll().size();

        // Create the PaymentChannel with an existing ID
        paymentChannel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentChannelMockMvc.perform(post("/api/payment-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentChannel)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentChannelRepository.findAll().size();
        // set the field null
        paymentChannel.setName(null);

        // Create the PaymentChannel, which fails.

        restPaymentChannelMockMvc.perform(post("/api/payment-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentChannel)))
            .andExpect(status().isBadRequest());

        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentChannels() throws Exception {
        // Initialize the database
        paymentChannelRepository.saveAndFlush(paymentChannel);

        // Get all the paymentChannelList
        restPaymentChannelMockMvc.perform(get("/api/payment-channels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentChannel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPaymentChannel() throws Exception {
        // Initialize the database
        paymentChannelRepository.saveAndFlush(paymentChannel);

        // Get the paymentChannel
        restPaymentChannelMockMvc.perform(get("/api/payment-channels/{id}", paymentChannel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentChannel.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentChannel() throws Exception {
        // Get the paymentChannel
        restPaymentChannelMockMvc.perform(get("/api/payment-channels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentChannel() throws Exception {
        // Initialize the database
        paymentChannelService.save(paymentChannel);

        int databaseSizeBeforeUpdate = paymentChannelRepository.findAll().size();

        // Update the paymentChannel
        PaymentChannel updatedPaymentChannel = paymentChannelRepository.findOne(paymentChannel.getId());
        updatedPaymentChannel
            .name(UPDATED_NAME);

        restPaymentChannelMockMvc.perform(put("/api/payment-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentChannel)))
            .andExpect(status().isOk());

        // Validate the PaymentChannel in the database
        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeUpdate);
        PaymentChannel testPaymentChannel = paymentChannelList.get(paymentChannelList.size() - 1);
        assertThat(testPaymentChannel.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentChannel() throws Exception {
        int databaseSizeBeforeUpdate = paymentChannelRepository.findAll().size();

        // Create the PaymentChannel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaymentChannelMockMvc.perform(put("/api/payment-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentChannel)))
            .andExpect(status().isCreated());

        // Validate the PaymentChannel in the database
        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaymentChannel() throws Exception {
        // Initialize the database
        paymentChannelService.save(paymentChannel);

        int databaseSizeBeforeDelete = paymentChannelRepository.findAll().size();

        // Get the paymentChannel
        restPaymentChannelMockMvc.perform(delete("/api/payment-channels/{id}", paymentChannel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentChannel> paymentChannelList = paymentChannelRepository.findAll();
        assertThat(paymentChannelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentChannel.class);
        PaymentChannel paymentChannel1 = new PaymentChannel();
        paymentChannel1.setId(1L);
        PaymentChannel paymentChannel2 = new PaymentChannel();
        paymentChannel2.setId(paymentChannel1.getId());
        assertThat(paymentChannel1).isEqualTo(paymentChannel2);
        paymentChannel2.setId(2L);
        assertThat(paymentChannel1).isNotEqualTo(paymentChannel2);
        paymentChannel1.setId(null);
        assertThat(paymentChannel1).isNotEqualTo(paymentChannel2);
    }
}
