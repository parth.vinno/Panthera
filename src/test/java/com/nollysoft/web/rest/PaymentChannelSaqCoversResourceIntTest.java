package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.PaymentChannel;
import com.nollysoft.domain.PaymentChannelSaqCovers;
import com.nollysoft.repository.PaymentChannelSaqCoversRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.PaymentChannelSaqCoversService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PaymentChannelSaqCoversResource REST controller.
 *
 * @see PaymentChannelSaqCoversResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentChannelSaqCoversResourceIntTest {

	@Autowired
	private PaymentChannelSaqCoversRepository paymentChannelSaqCoversRepository;

	@Autowired
	private PaymentChannelSaqCoversService paymentChannelSaqCoversService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restPaymentChannelSaqCoversMockMvc;

	private PaymentChannelSaqCovers paymentChannelSaqCovers;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PaymentChannelSaqCoversResource paymentChannelSaqCoversResource = new PaymentChannelSaqCoversResource(
				paymentChannelSaqCoversService, executiveSummaryStatusService);
		this.restPaymentChannelSaqCoversMockMvc = MockMvcBuilders.standaloneSetup(paymentChannelSaqCoversResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static PaymentChannelSaqCovers createEntity(EntityManager em) {
		PaymentChannelSaqCovers paymentChannelSaqCovers = new PaymentChannelSaqCovers();
		// Add required entity
		PaymentChannel paymentchannelId = PaymentChannelResourceIntTest.createEntity(em);
		em.persist(paymentchannelId);
		em.flush();
		paymentChannelSaqCovers.setPaymentChannelId(paymentchannelId);
		return paymentChannelSaqCovers;
	}

	@Before
	public void initTest() {
		paymentChannelSaqCovers = createEntity(em);
	}

	@Test
	@Transactional
	public void createPaymentChannelSaqCovers() throws Exception {
		int databaseSizeBeforeCreate = paymentChannelSaqCoversRepository.findAll().size();

		// Create the PaymentChannelSaqCovers
		restPaymentChannelSaqCoversMockMvc
				.perform(post("/api/payment-channel-saq-covers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelSaqCovers)))
				.andExpect(status().isCreated());

		// Validate the PaymentChannelSaqCovers in the database
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = paymentChannelSaqCoversRepository.findAll();
		assertThat(paymentChannelSaqCoversList).hasSize(databaseSizeBeforeCreate + 1);
	}

	@Test
	@Transactional
	public void createPaymentChannelSaqCoversWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = paymentChannelSaqCoversRepository.findAll().size();

		// Create the PaymentChannelSaqCovers with an existing ID
		paymentChannelSaqCovers.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPaymentChannelSaqCoversMockMvc
				.perform(post("/api/payment-channel-saq-covers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelSaqCovers)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = paymentChannelSaqCoversRepository.findAll();
		assertThat(paymentChannelSaqCoversList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllPaymentChannelSaqCovers() throws Exception {
		// Initialize the database
		paymentChannelSaqCoversRepository.saveAndFlush(paymentChannelSaqCovers);

		// Get all the paymentChannelSaqCoversList
		restPaymentChannelSaqCoversMockMvc.perform(get("/api/payment-channel-saq-covers?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(paymentChannelSaqCovers.getId().intValue())));
	}

	@Test
	@Transactional
	public void getPaymentChannelSaqCovers() throws Exception {
		// Initialize the database
		paymentChannelSaqCoversRepository.saveAndFlush(paymentChannelSaqCovers);

		// Get the paymentChannelSaqCovers
		restPaymentChannelSaqCoversMockMvc
				.perform(get("/api/payment-channel-saq-covers/{id}", paymentChannelSaqCovers.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(paymentChannelSaqCovers.getId().intValue()));
	}

	@Test
	@Transactional
	public void getNonExistingPaymentChannelSaqCovers() throws Exception {
		// Get the paymentChannelSaqCovers
		restPaymentChannelSaqCoversMockMvc.perform(get("/api/payment-channel-saq-covers/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updatePaymentChannelSaqCovers() throws Exception {
		// Initialize the database
		paymentChannelSaqCoversService.save(paymentChannelSaqCovers);

		int databaseSizeBeforeUpdate = paymentChannelSaqCoversRepository.findAll().size();

		// Update the paymentChannelSaqCovers
		PaymentChannelSaqCovers updatedPaymentChannelSaqCovers = paymentChannelSaqCoversRepository
				.findOne(paymentChannelSaqCovers.getId());

		restPaymentChannelSaqCoversMockMvc
				.perform(put("/api/payment-channel-saq-covers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedPaymentChannelSaqCovers)))
				.andExpect(status().isOk());

		// Validate the PaymentChannelSaqCovers in the database
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = paymentChannelSaqCoversRepository.findAll();
		assertThat(paymentChannelSaqCoversList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	public void updateNonExistingPaymentChannelSaqCovers() throws Exception {
		int databaseSizeBeforeUpdate = paymentChannelSaqCoversRepository.findAll().size();

		// Create the PaymentChannelSaqCovers

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPaymentChannelSaqCoversMockMvc
				.perform(put("/api/payment-channel-saq-covers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(paymentChannelSaqCovers)))
				.andExpect(status().isCreated());

		// Validate the PaymentChannelSaqCovers in the database
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = paymentChannelSaqCoversRepository.findAll();
		assertThat(paymentChannelSaqCoversList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePaymentChannelSaqCovers() throws Exception {
		// Initialize the database
		paymentChannelSaqCoversService.save(paymentChannelSaqCovers);

		int databaseSizeBeforeDelete = paymentChannelSaqCoversRepository.findAll().size();

		// Get the paymentChannelSaqCovers
		restPaymentChannelSaqCoversMockMvc
				.perform(delete("/api/payment-channel-saq-covers/{id}", paymentChannelSaqCovers.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<PaymentChannelSaqCovers> paymentChannelSaqCoversList = paymentChannelSaqCoversRepository.findAll();
		assertThat(paymentChannelSaqCoversList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(PaymentChannelSaqCovers.class);
		PaymentChannelSaqCovers paymentChannelSaqCovers1 = new PaymentChannelSaqCovers();
		paymentChannelSaqCovers1.setId(1L);
		PaymentChannelSaqCovers paymentChannelSaqCovers2 = new PaymentChannelSaqCovers();
		paymentChannelSaqCovers2.setId(paymentChannelSaqCovers1.getId());
		assertThat(paymentChannelSaqCovers1).isEqualTo(paymentChannelSaqCovers2);
		paymentChannelSaqCovers2.setId(2L);
		assertThat(paymentChannelSaqCovers1).isNotEqualTo(paymentChannelSaqCovers2);
		paymentChannelSaqCovers1.setId(null);
		assertThat(paymentChannelSaqCovers1).isNotEqualTo(paymentChannelSaqCovers2);
	}
}
