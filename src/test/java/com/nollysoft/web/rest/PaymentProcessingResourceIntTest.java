package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.PaymentProcessing;
import com.nollysoft.repository.PaymentProcessingRepository;
import com.nollysoft.service.PaymentProcessingService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentProcessingResource REST controller.
 *
 * @see PaymentProcessingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PaymentProcessingResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private PaymentProcessingRepository paymentProcessingRepository;

    @Autowired
    private PaymentProcessingService paymentProcessingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentProcessingMockMvc;

    private PaymentProcessing paymentProcessing;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PaymentProcessingResource paymentProcessingResource = new PaymentProcessingResource(paymentProcessingService);
        this.restPaymentProcessingMockMvc = MockMvcBuilders.standaloneSetup(paymentProcessingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentProcessing createEntity(EntityManager em) {
        PaymentProcessing paymentProcessing = new PaymentProcessing()
            .name(DEFAULT_NAME);
        return paymentProcessing;
    }

    @Before
    public void initTest() {
        paymentProcessing = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentProcessing() throws Exception {
        int databaseSizeBeforeCreate = paymentProcessingRepository.findAll().size();

        // Create the PaymentProcessing
        restPaymentProcessingMockMvc.perform(post("/api/payment-processings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentProcessing)))
            .andExpect(status().isCreated());

        // Validate the PaymentProcessing in the database
        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentProcessing testPaymentProcessing = paymentProcessingList.get(paymentProcessingList.size() - 1);
        assertThat(testPaymentProcessing.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createPaymentProcessingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentProcessingRepository.findAll().size();

        // Create the PaymentProcessing with an existing ID
        paymentProcessing.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentProcessingMockMvc.perform(post("/api/payment-processings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentProcessing)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentProcessingRepository.findAll().size();
        // set the field null
        paymentProcessing.setName(null);

        // Create the PaymentProcessing, which fails.

        restPaymentProcessingMockMvc.perform(post("/api/payment-processings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentProcessing)))
            .andExpect(status().isBadRequest());

        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentProcessings() throws Exception {
        // Initialize the database
        paymentProcessingRepository.saveAndFlush(paymentProcessing);

        // Get all the paymentProcessingList
        restPaymentProcessingMockMvc.perform(get("/api/payment-processings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentProcessing.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPaymentProcessing() throws Exception {
        // Initialize the database
        paymentProcessingRepository.saveAndFlush(paymentProcessing);

        // Get the paymentProcessing
        restPaymentProcessingMockMvc.perform(get("/api/payment-processings/{id}", paymentProcessing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentProcessing.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentProcessing() throws Exception {
        // Get the paymentProcessing
        restPaymentProcessingMockMvc.perform(get("/api/payment-processings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentProcessing() throws Exception {
        // Initialize the database
        paymentProcessingService.save(paymentProcessing);

        int databaseSizeBeforeUpdate = paymentProcessingRepository.findAll().size();

        // Update the paymentProcessing
        PaymentProcessing updatedPaymentProcessing = paymentProcessingRepository.findOne(paymentProcessing.getId());
        updatedPaymentProcessing
            .name(UPDATED_NAME);

        restPaymentProcessingMockMvc.perform(put("/api/payment-processings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentProcessing)))
            .andExpect(status().isOk());

        // Validate the PaymentProcessing in the database
        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeUpdate);
        PaymentProcessing testPaymentProcessing = paymentProcessingList.get(paymentProcessingList.size() - 1);
        assertThat(testPaymentProcessing.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentProcessing() throws Exception {
        int databaseSizeBeforeUpdate = paymentProcessingRepository.findAll().size();

        // Create the PaymentProcessing

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaymentProcessingMockMvc.perform(put("/api/payment-processings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentProcessing)))
            .andExpect(status().isCreated());

        // Validate the PaymentProcessing in the database
        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaymentProcessing() throws Exception {
        // Initialize the database
        paymentProcessingService.save(paymentProcessing);

        int databaseSizeBeforeDelete = paymentProcessingRepository.findAll().size();

        // Get the paymentProcessing
        restPaymentProcessingMockMvc.perform(delete("/api/payment-processings/{id}", paymentProcessing.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentProcessing> paymentProcessingList = paymentProcessingRepository.findAll();
        assertThat(paymentProcessingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentProcessing.class);
        PaymentProcessing paymentProcessing1 = new PaymentProcessing();
        paymentProcessing1.setId(1L);
        PaymentProcessing paymentProcessing2 = new PaymentProcessing();
        paymentProcessing2.setId(paymentProcessing1.getId());
        assertThat(paymentProcessing1).isEqualTo(paymentProcessing2);
        paymentProcessing2.setId(2L);
        assertThat(paymentProcessing1).isNotEqualTo(paymentProcessing2);
        paymentProcessing1.setId(null);
        assertThat(paymentProcessing1).isNotEqualTo(paymentProcessing2);
    }
}
