package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.domain.RequirementCategory;
import com.nollysoft.repository.PcidssRequirementRepository;
import com.nollysoft.service.PcidssRequirementService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PcidssRequirementResource REST controller.
 *
 * @see PcidssRequirementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class PcidssRequirementResourceIntTest {

	private static final String DEFAULT_REQUIREMENT = "AAAAAAAAAA";
	private static final String UPDATED_REQUIREMENT = "BBBBBBBBBB";

	@Autowired
	private PcidssRequirementRepository pcidssRequirementRepository;

	@Autowired
	private PcidssRequirementService pcidssRequirementService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restPcidssRequirementMockMvc;

	private PcidssRequirement pcidssRequirement;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PcidssRequirementResource pcidssRequirementResource = new PcidssRequirementResource(pcidssRequirementService);
		this.restPcidssRequirementMockMvc = MockMvcBuilders.standaloneSetup(pcidssRequirementResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static PcidssRequirement createEntity(EntityManager em) {
		PcidssRequirement pcidssRequirement = new PcidssRequirement().requirement(DEFAULT_REQUIREMENT);
		// Add required entity
		RequirementCategory requirementCategoryId = RequirementCategoryResourceIntTest.createEntity(em);
		em.persist(requirementCategoryId);
		em.flush();
		pcidssRequirement.setRequirementCategoryId(requirementCategoryId);
		return pcidssRequirement;
	}

	@Before
	public void initTest() {
		pcidssRequirement = createEntity(em);
	}

	@Test
	@Transactional
	public void createPcidssRequirement() throws Exception {
		int databaseSizeBeforeCreate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement
		restPcidssRequirementMockMvc
				.perform(post("/api/pcidss-requirements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement)))
				.andExpect(status().isCreated());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeCreate + 1);
		PcidssRequirement testPcidssRequirement = pcidssRequirementList.get(pcidssRequirementList.size() - 1);
		assertThat(testPcidssRequirement.getRequirement()).isEqualTo(DEFAULT_REQUIREMENT);
	}

	@Test
	@Transactional
	public void createPcidssRequirementWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement with an existing ID
		pcidssRequirement.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPcidssRequirementMockMvc
				.perform(post("/api/pcidss-requirements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllPcidssRequirements() throws Exception {
		// Initialize the database
		pcidssRequirementRepository.saveAndFlush(pcidssRequirement);

		// Get all the pcidssRequirementList
		restPcidssRequirementMockMvc.perform(get("/api/pcidss-requirements?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(pcidssRequirement.getId().intValue())))
				.andExpect(jsonPath("$.[*].requirement").value(hasItem(DEFAULT_REQUIREMENT.toString())));
	}

	@Test
	@Transactional
	public void getPcidssRequirement() throws Exception {
		// Initialize the database
		pcidssRequirementRepository.saveAndFlush(pcidssRequirement);

		// Get the pcidssRequirement
		restPcidssRequirementMockMvc.perform(get("/api/pcidss-requirements/{id}", pcidssRequirement.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(pcidssRequirement.getId().intValue()))
				.andExpect(jsonPath("$.requirement").value(DEFAULT_REQUIREMENT.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingPcidssRequirement() throws Exception {
		// Get the pcidssRequirement
		restPcidssRequirementMockMvc.perform(get("/api/pcidss-requirements/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updatePcidssRequirement() throws Exception {
		// Initialize the database
		pcidssRequirementService.save(pcidssRequirement);

		int databaseSizeBeforeUpdate = pcidssRequirementRepository.findAll().size();

		// Update the pcidssRequirement
		PcidssRequirement updatedPcidssRequirement = pcidssRequirementRepository.findOne(pcidssRequirement.getId());
		updatedPcidssRequirement.requirement(UPDATED_REQUIREMENT);

		restPcidssRequirementMockMvc
				.perform(put("/api/pcidss-requirements").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedPcidssRequirement)))
				.andExpect(status().isOk());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeUpdate);
		PcidssRequirement testPcidssRequirement = pcidssRequirementList.get(pcidssRequirementList.size() - 1);
		assertThat(testPcidssRequirement.getRequirement()).isEqualTo(UPDATED_REQUIREMENT);
	}

	@Test
	@Transactional
	public void updateNonExistingPcidssRequirement() throws Exception {
		int databaseSizeBeforeUpdate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPcidssRequirementMockMvc.perform(put("/api/pcidss-requirements").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement))).andExpect(status().isCreated());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePcidssRequirement() throws Exception {
		// Initialize the database
		pcidssRequirementService.save(pcidssRequirement);

		int databaseSizeBeforeDelete = pcidssRequirementRepository.findAll().size();

		// Get the pcidssRequirement
		restPcidssRequirementMockMvc.perform(delete("/api/pcidss-requirements/{id}", pcidssRequirement.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void createPcidssRequirementForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement
		restPcidssRequirementMockMvc
				.perform(post("/api/pcidss-requirements/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement)))
				.andExpect(status().isCreated());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeCreate + 1);
		PcidssRequirement testPcidssRequirement = pcidssRequirementList.get(pcidssRequirementList.size() - 1);
		assertThat(testPcidssRequirement.getRequirement()).isEqualTo(DEFAULT_REQUIREMENT);
	}

	@Test
	@Transactional
	public void createPcidssRequirementWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement with an existing ID
		pcidssRequirement.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPcidssRequirementMockMvc
				.perform(post("/api/pcidss-requirements/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllPcidssRequirementsForServiceProvider() throws Exception {
		// Initialize the database
		pcidssRequirementRepository.saveAndFlush(pcidssRequirement);

		// Get all the pcidssRequirementList
		restPcidssRequirementMockMvc.perform(get("/api/pcidss-requirements/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(pcidssRequirement.getId().intValue())))
				.andExpect(jsonPath("$.[*].requirement").value(hasItem(DEFAULT_REQUIREMENT.toString())));
	}

	@Test
	@Transactional
	public void getPcidssRequirementForServiceProvider() throws Exception {
		// Initialize the database
		pcidssRequirementRepository.saveAndFlush(pcidssRequirement);

		// Get the pcidssRequirement
		restPcidssRequirementMockMvc
				.perform(get("/api/pcidss-requirements/service-provider/{id}", pcidssRequirement.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(pcidssRequirement.getId().intValue()))
				.andExpect(jsonPath("$.requirement").value(DEFAULT_REQUIREMENT.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingPcidssRequirementForServiceProvider() throws Exception {
		// Get the pcidssRequirement
		restPcidssRequirementMockMvc.perform(get("/api/pcidss-requirements/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updatePcidssRequirementForServiceProvider() throws Exception {
		// Initialize the database
		pcidssRequirementService.save(pcidssRequirement);

		int databaseSizeBeforeUpdate = pcidssRequirementRepository.findAll().size();

		// Update the pcidssRequirement
		PcidssRequirement updatedPcidssRequirement = pcidssRequirementRepository.findOne(pcidssRequirement.getId());
		updatedPcidssRequirement.requirement(UPDATED_REQUIREMENT);

		restPcidssRequirementMockMvc
				.perform(put("/api/pcidss-requirements/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedPcidssRequirement)))
				.andExpect(status().isOk());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeUpdate);
		PcidssRequirement testPcidssRequirement = pcidssRequirementList.get(pcidssRequirementList.size() - 1);
		assertThat(testPcidssRequirement.getRequirement()).isEqualTo(UPDATED_REQUIREMENT);
	}

	@Test
	@Transactional
	public void updateNonExistingPcidssRequirementForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = pcidssRequirementRepository.findAll().size();

		// Create the PcidssRequirement

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPcidssRequirementMockMvc
				.perform(put("/api/pcidss-requirements/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(pcidssRequirement)))
				.andExpect(status().isCreated());

		// Validate the PcidssRequirement in the database
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePcidssRequirementForServiceProvider() throws Exception {
		// Initialize the database
		pcidssRequirementService.save(pcidssRequirement);

		int databaseSizeBeforeDelete = pcidssRequirementRepository.findAll().size();

		// Get the pcidssRequirement
		restPcidssRequirementMockMvc
				.perform(delete("/api/pcidss-requirements/service-provider/{id}", pcidssRequirement.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<PcidssRequirement> pcidssRequirementList = pcidssRequirementRepository.findAll();
		assertThat(pcidssRequirementList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(PcidssRequirement.class);
		PcidssRequirement pcidssRequirement1 = new PcidssRequirement();
		pcidssRequirement1.setId(1L);
		PcidssRequirement pcidssRequirement2 = new PcidssRequirement();
		pcidssRequirement2.setId(pcidssRequirement1.getId());
		assertThat(pcidssRequirement1).isEqualTo(pcidssRequirement2);
		pcidssRequirement2.setId(2L);
		assertThat(pcidssRequirement1).isNotEqualTo(pcidssRequirement2);
		pcidssRequirement1.setId(null);
		assertThat(pcidssRequirement1).isNotEqualTo(pcidssRequirement2);
	}
}
