package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.QANotification;
import com.nollysoft.domain.QANotification.NOTIFICATION_STATUS;
import com.nollysoft.domain.User;
import com.nollysoft.repository.AssessmentInfoRepository;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.QANotificationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the QANotificationResource REST controller.
 *
 * @see QANotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class QANotificationResourceIntTest {

	private static final LocalDate DEFAULT_NOTIFICATION_DATE = LocalDate.now();
	private static final LocalDate UPDATED_NOTIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

	private static final NOTIFICATION_STATUS DEFAULT_STATUS = NOTIFICATION_STATUS.NEW;
	private static final NOTIFICATION_STATUS UPDATED_STATUS = NOTIFICATION_STATUS.RESPONSE_REQUIRED;

	@Autowired
	private QANotificationRepository qaNotificationRepository;

	@Autowired
	private QANotificationService qaNotificationService;

	@Autowired
	private MailService mailService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AssessmentInfoRepository assessmentInfoRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restQANotificationMockMvc;

	private QANotification qaNotification;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final QANotificationResource qaNotificationResource = new QANotificationResource(qaNotificationService,
				mailService, userRepository, assessmentInfoRepository, organizationRepository);
		this.restQANotificationMockMvc = MockMvcBuilders.standaloneSetup(qaNotificationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static QANotification createEntity(EntityManager em) {
		QANotification qaNotification = new QANotification().status(DEFAULT_STATUS)
				.notificationDate(DEFAULT_NOTIFICATION_DATE);
		// Add required entity
		User qaId = UserResourceIntTest.createEntity(em);
		em.persist(qaId);
		em.flush();
		qaNotification.setQaId(qaId);
		qaNotification.setQsaId(qaId);
		return qaNotification;
	}

	@Before
	public void initTest() {
		qaNotification = createEntity(em);
	}

	@Test
	@Transactional
	public void createQANotification() throws Exception {
		int databaseSizeBeforeCreate = qaNotificationRepository.findAll().size();

		// Create the QANotification
		restQANotificationMockMvc.perform(post("/api/qa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qaNotification))).andExpect(status().isCreated());

		// Validate the QANotification in the database
		List<QANotification> qaNotificationList = qaNotificationRepository.findAll();
		assertThat(qaNotificationList).hasSize(databaseSizeBeforeCreate + 1);
		QANotification testQANotification = qaNotificationList.get(qaNotificationList.size() - 1);
		assertThat(testQANotification.getNotificationDate()).isEqualTo(DEFAULT_NOTIFICATION_DATE);
		assertThat(testQANotification.getStatusType()).isEqualTo(DEFAULT_STATUS);

	}

	@Test
	@Transactional
	public void createQANotificationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = qaNotificationRepository.findAll().size();

		// Create the QANotification with an existing ID
		qaNotification.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restQANotificationMockMvc.perform(post("/api/qa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qaNotification))).andExpect(status().isBadRequest());

		// Validate the QANotification in the database
		List<QANotification> qaNotificationList = qaNotificationRepository.findAll();
		assertThat(qaNotificationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllQANotifications() throws Exception {
		// Initialize the database
		qaNotificationRepository.saveAndFlush(qaNotification);

		// Get all the qaNotificationList
		restQANotificationMockMvc.perform(get("/api/qa-notifications?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(qaNotification.getId().intValue())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
				.andExpect(jsonPath("$.[*].notificationDate").value(hasItem(DEFAULT_NOTIFICATION_DATE.toString())));
	}

	@Test
	@Transactional
	public void getQANotification() throws Exception {
		// Initialize the database
		qaNotificationRepository.saveAndFlush(qaNotification);

		// Get the qaNotification
		restQANotificationMockMvc.perform(get("/api/qa-notifications/{id}", qaNotification.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(qaNotification.getId().intValue()))
				.andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
				.andExpect(jsonPath("$.notificationDate").value(DEFAULT_NOTIFICATION_DATE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingQANotification() throws Exception {
		// Get the qaNotification
		restQANotificationMockMvc.perform(get("/api/qa-notifications/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateQANotification() throws Exception {
		// Initialize the database
		qaNotificationService.save(qaNotification);

		int databaseSizeBeforeUpdate = qaNotificationRepository.findAll().size();

		// Update the qaNotification
		QANotification updatedQANotification = qaNotificationRepository.findOne(qaNotification.getId());
		// Disconnect from session so that the updates on updatedQANotification
		// are not
		// directly saved in db
		em.detach(updatedQANotification);
		updatedQANotification.notificationDate(UPDATED_NOTIFICATION_DATE).status(UPDATED_STATUS);

		restQANotificationMockMvc.perform(put("/api/qa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedQANotification))).andExpect(status().isOk());

		// Validate the QANotification in the database
		List<QANotification> qaNotificationList = qaNotificationRepository.findAll();
		assertThat(qaNotificationList).hasSize(databaseSizeBeforeUpdate);
		QANotification testQANotification = qaNotificationList.get(qaNotificationList.size() - 1);
		assertThat(testQANotification.getNotificationDate()).isEqualTo(UPDATED_NOTIFICATION_DATE);
		assertThat(testQANotification.getStatusType()).isEqualTo(UPDATED_STATUS);
	}

	@Test
	@Transactional
	public void updateNonExistingQANotification() throws Exception {
		int databaseSizeBeforeUpdate = qaNotificationRepository.findAll().size();

		// Create the QANotification

		// If the entity doesn't have an ID, it will be created instead of just
		// being
		// updated
		restQANotificationMockMvc.perform(put("/api/qa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qaNotification))).andExpect(status().isCreated());

		// Validate the QANotification in the database
		List<QANotification> qaNotificationList = qaNotificationRepository.findAll();
		assertThat(qaNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteQANotification() throws Exception {
		// Initialize the database
		qaNotificationService.save(qaNotification);

		int databaseSizeBeforeDelete = qaNotificationRepository.findAll().size();

		// Get the qaNotification
		restQANotificationMockMvc.perform(
				delete("/api/qa-notifications/{id}", qaNotification.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<QANotification> qaNotificationList = qaNotificationRepository.findAll();
		assertThat(qaNotificationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(QANotification.class);
		QANotification qaNotification1 = new QANotification();
		qaNotification1.setId(1L);
		QANotification qaNotification2 = new QANotification();
		qaNotification2.setId(qaNotification1.getId());
		assertThat(qaNotification1).isEqualTo(qaNotification2);
		qaNotification2.setId(2L);
		assertThat(qaNotification1).isNotEqualTo(qaNotification2);
		qaNotification1.setId(null);
		assertThat(qaNotification1).isNotEqualTo(qaNotification2);
	}
}
