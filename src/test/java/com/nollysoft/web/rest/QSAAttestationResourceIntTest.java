package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.QSAAttestation;
import com.nollysoft.repository.QSAAttestationRepository;
import com.nollysoft.service.QSAAttestationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the QSAAttestationResource REST controller.
 *
 * @see QSAAttestationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class QSAAttestationResourceIntTest {

	private static final String DEFAULT_QSA_ATTESTOR_NAME = "Test Default Name";
	private static final String UPDATED_QSA_ATTESTOR_NAME = "Test Updated Name";

	private static final String DEFAULT_QSA_ATTESTOR_SIGNATURE = "Test Default Signature";
	private static final String UPDATED_QSA_ATTESTOR_SIGNATURE = "Test Updated Signature";

	private static final String DEFAULT_QSA_ATTESTOR_COMPANY = "Test Default Company";
	private static final String UPDATED_QSA_ATTESTOR_COMPANY = "Test Updated Company";

	@Autowired
	private QSAAttestationRepository qsaAttestationRepository;

	@Autowired
	private QSAAttestationService qsaAttestationService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restQSAAttestationMockMvc;

	private QSAAttestation qsaAttestation;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final QSAAttestationResource qsaAttestationResource = new QSAAttestationResource(qsaAttestationService);
		this.restQSAAttestationMockMvc = MockMvcBuilders.standaloneSetup(qsaAttestationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static QSAAttestation createEntity(EntityManager em) {
		QSAAttestation qsaAttestation = new QSAAttestation().qsaAttestorName(DEFAULT_QSA_ATTESTOR_NAME)
				.qsaAttestorSignature(DEFAULT_QSA_ATTESTOR_SIGNATURE).qsaAttestorCompany(DEFAULT_QSA_ATTESTOR_COMPANY);
		return qsaAttestation;
	}

	@Before
	public void initTest() {
		qsaAttestation = createEntity(em);
	}

	@Test
	@Transactional
	public void createQSAAttestationForMerchant() throws Exception {
		int databaseSizeBeforeCreate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation
		restQSAAttestationMockMvc.perform(post("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qsaAttestation))).andExpect(status().isCreated());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		QSAAttestation testQSAAttestation = qsaAttestationList.get(qsaAttestationList.size() - 1);
		assertThat(testQSAAttestation.getQsaAttestorName()).isEqualTo(DEFAULT_QSA_ATTESTOR_NAME);
		assertThat(testQSAAttestation.getQsaAttestorSignature()).isEqualTo(DEFAULT_QSA_ATTESTOR_SIGNATURE);
		assertThat(testQSAAttestation.getQsaAttestorCompany()).isEqualTo(DEFAULT_QSA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void createQSAAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation
		restQSAAttestationMockMvc.perform(post("/api/qsa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isCreated());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeCreate + 1);
		QSAAttestation testQSAAttestation = qsaAttestationList.get(qsaAttestationList.size() - 1);
		assertThat(testQSAAttestation.getQsaAttestorName()).isEqualTo(DEFAULT_QSA_ATTESTOR_NAME);
		assertThat(testQSAAttestation.getQsaAttestorSignature()).isEqualTo(DEFAULT_QSA_ATTESTOR_SIGNATURE);
		assertThat(testQSAAttestation.getQsaAttestorCompany()).isEqualTo(DEFAULT_QSA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void createQSAAttestationWithExistingIdForMerchant() throws Exception {
		int databaseSizeBeforeCreate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation with an existing ID
		qsaAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restQSAAttestationMockMvc.perform(post("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qsaAttestation))).andExpect(status().isBadRequest());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void createQSAAttestationWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation with an existing ID
		qsaAttestation.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restQSAAttestationMockMvc.perform(post("/api/qsa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isBadRequest());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkQsaAttestorNameIsRequiredForMerchant() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorName(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkQsaAttestorNameIsRequiredForServiceProvider() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorName(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkQsaAttestorSignatureIsRequiredForMerchant() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorSignature(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkQsaAttestorSignatureIsRequiredForServiceProvider() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorSignature(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkQsaAttestorCompanyIsRequiredForMerchant() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorCompany(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void checkQsaAttestorCompanyIsRequiredForServiceProvider() throws Exception {
		// set the field null
		qsaAttestation.setQsaAttestorCompany(null);

		// Create the QSAAttestation, which fails.

		restQSAAttestationMockMvc
				.perform(post("/api/qsa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Transactional
	public void getAllQSAAttestationsForMerchant() throws Exception {
		// Initialize the database
		qsaAttestationRepository.saveAndFlush(qsaAttestation);

		// Get all the qsaAttestationList
		restQSAAttestationMockMvc.perform(get("/api/qsa-attestations?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(qsaAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].qsaAttestorName").value(hasItem(DEFAULT_QSA_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].qsaAttestorSignature")
						.value(hasItem(DEFAULT_QSA_ATTESTOR_SIGNATURE.toString())))
				.andExpect(jsonPath("$.[*].qsaAttestorCompany").value(hasItem(DEFAULT_QSA_ATTESTOR_COMPANY.toString())));
	}

	@Test
	@Transactional
	public void getAllQSAAttestationsForServiceProvider() throws Exception {
		// Initialize the database
		qsaAttestationRepository.saveAndFlush(qsaAttestation);

		// Get all the qsaAttestationList
		restQSAAttestationMockMvc.perform(get("/api/qsa-attestations/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(qsaAttestation.getId().intValue())))
				.andExpect(jsonPath("$.[*].qsaAttestorName").value(hasItem(DEFAULT_QSA_ATTESTOR_NAME.toString())))
				.andExpect(jsonPath("$.[*].qsaAttestorSignature")
						.value(hasItem(DEFAULT_QSA_ATTESTOR_SIGNATURE.toString())))
				.andExpect(jsonPath("$.[*].qsaAttestorCompany").value(hasItem(DEFAULT_QSA_ATTESTOR_COMPANY.toString())));
	}

	@Test
	@Transactional
	public void updateQSAAttestationForMerchant() throws Exception {
		// Initialize the database
		qsaAttestationService.save(qsaAttestation);

		int databaseSizeBeforeUpdate = qsaAttestationRepository.findAll().size();

		// Update the qsaAttestation
		QSAAttestation updatedQSAAttestation = qsaAttestationRepository.findOne(qsaAttestation.getId());
		// Disconnect from session so that the updates on updatedQSAAttestation are not
		// directly saved in db
		em.detach(updatedQSAAttestation);
		updatedQSAAttestation.qsaAttestorName(UPDATED_QSA_ATTESTOR_NAME)
				.qsaAttestorSignature(UPDATED_QSA_ATTESTOR_SIGNATURE).qsaAttestorCompany(UPDATED_QSA_ATTESTOR_COMPANY);

		restQSAAttestationMockMvc.perform(put("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedQSAAttestation))).andExpect(status().isOk());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeUpdate);
		QSAAttestation testQSAAttestation = qsaAttestationList.get(qsaAttestationList.size() - 1);
		assertThat(testQSAAttestation.getQsaAttestorName()).isEqualTo(UPDATED_QSA_ATTESTOR_NAME);
		assertThat(testQSAAttestation.getQsaAttestorSignature()).isEqualTo(UPDATED_QSA_ATTESTOR_SIGNATURE);
		assertThat(testQSAAttestation.getQsaAttestorCompany()).isEqualTo(UPDATED_QSA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void updateQSAAttestationForServiceProvider() throws Exception {
		// Initialize the database
		qsaAttestationService.save(qsaAttestation);

		int databaseSizeBeforeUpdate = qsaAttestationRepository.findAll().size();

		// Update the qsaAttestation
		QSAAttestation updatedQSAAttestation = qsaAttestationRepository.findOne(qsaAttestation.getId());
		// Disconnect from session so that the updates on updatedQSAAttestation are not
		// directly saved in db
		em.detach(updatedQSAAttestation);
		updatedQSAAttestation.qsaAttestorName(UPDATED_QSA_ATTESTOR_NAME)
				.qsaAttestorSignature(UPDATED_QSA_ATTESTOR_SIGNATURE).qsaAttestorCompany(UPDATED_QSA_ATTESTOR_COMPANY);

		restQSAAttestationMockMvc
				.perform(put("/api/qsa-attestations/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedQSAAttestation)))
				.andExpect(status().isOk());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeUpdate);
		QSAAttestation testQSAAttestation = qsaAttestationList.get(qsaAttestationList.size() - 1);
		assertThat(testQSAAttestation.getQsaAttestorName()).isEqualTo(UPDATED_QSA_ATTESTOR_NAME);
		assertThat(testQSAAttestation.getQsaAttestorSignature()).isEqualTo(UPDATED_QSA_ATTESTOR_SIGNATURE);
		assertThat(testQSAAttestation.getQsaAttestorCompany()).isEqualTo(UPDATED_QSA_ATTESTOR_COMPANY);
	}

	@Test
	@Transactional
	public void updateNonExistingQSAAttestationForMerchant() throws Exception {
		int databaseSizeBeforeUpdate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation

		// If the entity doesn't have an ID , it will be created instead of just being
		// updated
		restQSAAttestationMockMvc.perform(put("/api/qsa-attestations").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qsaAttestation))).andExpect(status().isCreated());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void updateNonExistingQSAAttestationForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = qsaAttestationRepository.findAll().size();

		// Create the QSAAttestation

		// If the entity doesn't have an ID , it will be created instead of just being
		// updated
		restQSAAttestationMockMvc.perform(put("/api/qsa-attestations/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(qsaAttestation)))
				.andExpect(status().isCreated());

		// Validate the QSAAttestation in the database
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteQSAAttestationForMerchant() throws Exception {
		// Initialize the database
		qsaAttestationService.save(qsaAttestation);

		int databaseSizeBeforeDelete = qsaAttestationRepository.findAll().size();

		// Get the qsaAttestation
		restQSAAttestationMockMvc.perform(
				delete("/api/qsa-attestations/{id}", qsaAttestation.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void deleteQSAAttestationForServiceProvider() throws Exception {
		// Initialize the database
		qsaAttestationService.save(qsaAttestation);

		int databaseSizeBeforeDelete = qsaAttestationRepository.findAll().size();

		// Get the qsaAttestation
		restQSAAttestationMockMvc.perform(delete("/api/qsa-attestations/service-provider/{id}", qsaAttestation.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<QSAAttestation> qsaAttestationList = qsaAttestationRepository.findAll();
		assertThat(qsaAttestationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(QSAAttestation.class);
		QSAAttestation qsaAttestation1 = new QSAAttestation();
		qsaAttestation1.setId(1L);
		QSAAttestation qsaAttestation2 = new QSAAttestation();
		qsaAttestation2.setId(qsaAttestation1.getId());
		assertThat(qsaAttestation1).isEqualTo(qsaAttestation2);
		qsaAttestation2.setId(2L);
		assertThat(qsaAttestation1).isNotEqualTo(qsaAttestation2);
		qsaAttestation1.setId(null);
		assertThat(qsaAttestation1).isNotEqualTo(qsaAttestation2);
	}
}
