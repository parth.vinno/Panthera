package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.QSANotification;
import com.nollysoft.domain.User;
import com.nollysoft.repository.OrganizationRepository;
import com.nollysoft.repository.QANotificationRepository;
import com.nollysoft.repository.QSANotificationRepository;
import com.nollysoft.repository.UserRepository;
import com.nollysoft.service.MailService;
import com.nollysoft.service.QANotificationService;
import com.nollysoft.service.QSANotificationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the QSANotificationResource REST controller.
 *
 * @see QSANotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class QSANotificationResourceIntTest {

	private static final LocalDate DEFAULT_NOTIFICATION_DATE = LocalDate.now();
	private static final LocalDate UPDATED_NOTIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

	@Autowired
	private QSANotificationRepository qsaNotificationRepository;

	@Autowired
	private QSANotificationService qsaNotificationService;

	@Autowired
	private QANotificationService qaNotificationService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private QANotificationRepository qaNotificationRepository;

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restQSANotificationMockMvc;

	private QSANotification qsaNotification;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final QSANotificationResource qsaNotificationResource = new QSANotificationResource(qsaNotificationService,
				qaNotificationRepository, userRepository, mailService, qaNotificationService, organizationRepository);
		this.restQSANotificationMockMvc = MockMvcBuilders.standaloneSetup(qsaNotificationResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static QSANotification createEntity(EntityManager em) {
		QSANotification qsaNotification = new QSANotification().notificationDate(DEFAULT_NOTIFICATION_DATE);
		// Add required entity
		User userId = UserResourceIntTest.createEntity(em);
		em.persist(userId);
		em.flush();
		qsaNotification.setQaUserId(userId);
		qsaNotification.setQsaUserId(userId);
		return qsaNotification;
	}

	@Before
	public void initTest() {
		qsaNotification = createEntity(em);
	}

	@Test
	@Transactional
	public void createQSANotification() throws Exception {
		int databaseSizeBeforeCreate = qsaNotificationRepository.findAll().size();

		// Create the QSANotification
		restQSANotificationMockMvc.perform(post("/api/qsa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qsaNotification))).andExpect(status().isCreated());

		// Validate the QSANotification in the database
		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeCreate + 1);
		QSANotification testQSANotification = qsaNotificationList.get(qsaNotificationList.size() - 1);
		assertThat(testQSANotification.getNotificationDate()).isEqualTo(DEFAULT_NOTIFICATION_DATE);
	}

	@Test
	@Transactional
	public void createQSANotificationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = qsaNotificationRepository.findAll().size();

		// Create the QSANotification with an existing ID
		qsaNotification.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restQSANotificationMockMvc
				.perform(post("/api/qsa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaNotification)))
				.andExpect(status().isBadRequest());

		// Validate the QSANotification in the database
		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkNotificationDateIsRequired() throws Exception {
		int databaseSizeBeforeTest = qsaNotificationRepository.findAll().size();
		// set the field null
		qsaNotification.setNotificationDate(null);

		// Create the QSANotification, which fails.

		restQSANotificationMockMvc
				.perform(post("/api/qsa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(qsaNotification)))
				.andExpect(status().isBadRequest());

		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllQSANotifications() throws Exception {
		// Initialize the database
		qsaNotificationRepository.saveAndFlush(qsaNotification);

		// Get all the qsaNotificationList
		restQSANotificationMockMvc.perform(get("/api/qsa-notifications?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(qsaNotification.getId().intValue())))
				.andExpect(jsonPath("$.[*].notificationDate").value(hasItem(DEFAULT_NOTIFICATION_DATE.toString())));
	}

	@Test
	@Transactional
	public void getQSANotification() throws Exception {
		// Initialize the database
		qsaNotificationRepository.saveAndFlush(qsaNotification);

		// Get the qsaNotification
		restQSANotificationMockMvc.perform(get("/api/qsa-notifications/{id}", qsaNotification.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(qsaNotification.getId().intValue()))
				.andExpect(jsonPath("$.notificationDate").value(DEFAULT_NOTIFICATION_DATE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingQSANotification() throws Exception {
		// Get the qsaNotification
		restQSANotificationMockMvc.perform(get("/api/qsa-notifications/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateQSANotification() throws Exception {
		// Initialize the database
		qsaNotificationService.save(qsaNotification);

		int databaseSizeBeforeUpdate = qsaNotificationRepository.findAll().size();

		// Update the qsaNotification
		QSANotification updatedQSANotification = qsaNotificationRepository.findOne(qsaNotification.getId());
		// Disconnect from session so that the updates on updatedQSANotification
		// are not
		// directly saved in db
		em.detach(updatedQSANotification);
		updatedQSANotification.notificationDate(UPDATED_NOTIFICATION_DATE);

		restQSANotificationMockMvc.perform(put("/api/qsa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedQSANotification))).andExpect(status().isOk());

		// Validate the QSANotification in the database
		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeUpdate);
		QSANotification testQSANotification = qsaNotificationList.get(qsaNotificationList.size() - 1);
		assertThat(testQSANotification.getNotificationDate()).isEqualTo(UPDATED_NOTIFICATION_DATE);
	}

	@Test
	@Transactional
	public void updateNonExistingQSANotification() throws Exception {
		int databaseSizeBeforeUpdate = qsaNotificationRepository.findAll().size();

		// Create the QSANotification

		// If the entity doesn't have an ID, it will be created instead of just
		// being
		// updated
		restQSANotificationMockMvc.perform(put("/api/qsa-notifications").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(qsaNotification))).andExpect(status().isCreated());

		// Validate the QSANotification in the database
		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteQSANotification() throws Exception {
		// Initialize the database
		qsaNotificationService.save(qsaNotification);

		int databaseSizeBeforeDelete = qsaNotificationRepository.findAll().size();

		// Get the qsaNotification
		restQSANotificationMockMvc.perform(
				delete("/api/qsa-notifications/{id}", qsaNotification.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<QSANotification> qsaNotificationList = qsaNotificationRepository.findAll();
		assertThat(qsaNotificationList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(QSANotification.class);
		QSANotification qsaNotification1 = new QSANotification();
		qsaNotification1.setId(1L);
		QSANotification qsaNotification2 = new QSANotification();
		qsaNotification2.setId(qsaNotification1.getId());
		assertThat(qsaNotification1).isEqualTo(qsaNotification2);
		qsaNotification2.setId(2L);
		assertThat(qsaNotification1).isNotEqualTo(qsaNotification2);
		qsaNotification1.setId(null);
		assertThat(qsaNotification1).isNotEqualTo(qsaNotification2);
	}
}
