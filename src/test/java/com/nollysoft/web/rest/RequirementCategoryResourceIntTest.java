package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.RequirementCategory;
import com.nollysoft.repository.RequirementCategoryRepository;
import com.nollysoft.service.RequirementCategoryService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the RequirementCategoryResource REST controller.
 *
 * @see RequirementCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class RequirementCategoryResourceIntTest {

	private static final String DEFAULT_CATEGORY_NAME = "AAAAAAAAAA";
	private static final String UPDATED_CATEGORY_NAME = "BBBBBBBBBB";

	@Autowired
	private RequirementCategoryRepository requirementCategoryRepository;

	@Autowired
	private RequirementCategoryService requirementCategoryService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restRequirementCategoryMockMvc;

	private RequirementCategory requirementCategory;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		RequirementCategoryResource requirementCategoryResource = new RequirementCategoryResource(
				requirementCategoryService);
		this.restRequirementCategoryMockMvc = MockMvcBuilders.standaloneSetup(requirementCategoryResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static RequirementCategory createEntity(EntityManager em) {
		RequirementCategory requirementCategory = new RequirementCategory().categoryName(DEFAULT_CATEGORY_NAME);
		return requirementCategory;
	}

	@Before
	public void initTest() {
		requirementCategory = createEntity(em);
	}

	@Test
	@Transactional
	public void createRequirementCategory() throws Exception {
		int databaseSizeBeforeCreate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory
		restRequirementCategoryMockMvc
				.perform(post("/api/requirement-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isCreated());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeCreate + 1);
		RequirementCategory testRequirementCategory = requirementCategoryList.get(requirementCategoryList.size() - 1);
		assertThat(testRequirementCategory.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
	}

	@Test
	@Transactional
	public void createRequirementCategoryWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory with an existing ID
		requirementCategory.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restRequirementCategoryMockMvc
				.perform(post("/api/requirement-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllRequirementCategories() throws Exception {
		// Initialize the database
		requirementCategoryRepository.saveAndFlush(requirementCategory);

		// Get all the requirementCategoryList
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(requirementCategory.getId().intValue())))
				.andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())));
	}

	@Test
	@Transactional
	public void getRequirementCategory() throws Exception {
		// Initialize the database
		requirementCategoryRepository.saveAndFlush(requirementCategory);

		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories/{id}", requirementCategory.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(requirementCategory.getId().intValue()))
				.andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingRequirementCategory() throws Exception {
		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateRequirementCategory() throws Exception {
		// Initialize the database
		requirementCategoryService.save(requirementCategory);

		int databaseSizeBeforeUpdate = requirementCategoryRepository.findAll().size();

		// Update the requirementCategory
		RequirementCategory updatedRequirementCategory = requirementCategoryRepository
				.findOne(requirementCategory.getId());
		updatedRequirementCategory.categoryName(UPDATED_CATEGORY_NAME);

		restRequirementCategoryMockMvc
				.perform(put("/api/requirement-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedRequirementCategory)))
				.andExpect(status().isOk());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeUpdate);
		RequirementCategory testRequirementCategory = requirementCategoryList.get(requirementCategoryList.size() - 1);
		assertThat(testRequirementCategory.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
	}

	@Test
	@Transactional
	public void updateNonExistingRequirementCategory() throws Exception {
		int databaseSizeBeforeUpdate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restRequirementCategoryMockMvc
				.perform(put("/api/requirement-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isCreated());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteRequirementCategory() throws Exception {
		// Initialize the database
		requirementCategoryService.save(requirementCategory);

		int databaseSizeBeforeDelete = requirementCategoryRepository.findAll().size();

		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(delete("/api/requirement-categories/{id}", requirementCategory.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeDelete - 1);
	}
	
	@Test
	@Transactional
	public void createRequirementCategoryForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory
		restRequirementCategoryMockMvc
				.perform(post("/api/requirement-categories/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isCreated());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeCreate + 1);
		RequirementCategory testRequirementCategory = requirementCategoryList.get(requirementCategoryList.size() - 1);
		assertThat(testRequirementCategory.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
	}

	@Test
	@Transactional
	public void createRequirementCategoryWithExistingIdForServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory with an existing ID
		requirementCategory.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restRequirementCategoryMockMvc
				.perform(post("/api/requirement-categories/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllRequirementCategoriesForServiceProvider() throws Exception {
		// Initialize the database
		requirementCategoryRepository.saveAndFlush(requirementCategory);

		// Get all the requirementCategoryList
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(requirementCategory.getId().intValue())))
				.andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())));
	}

	@Test
	@Transactional
	public void getRequirementCategoryForServiceProvider() throws Exception {
		// Initialize the database
		requirementCategoryRepository.saveAndFlush(requirementCategory);

		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories/service-provider/{id}", requirementCategory.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(requirementCategory.getId().intValue()))
				.andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingRequirementCategoryForServiceProvider() throws Exception {
		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(get("/api/requirement-categories/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateRequirementCategoryForServiceProvider() throws Exception {
		// Initialize the database
		requirementCategoryService.save(requirementCategory);

		int databaseSizeBeforeUpdate = requirementCategoryRepository.findAll().size();

		// Update the requirementCategory
		RequirementCategory updatedRequirementCategory = requirementCategoryRepository
				.findOne(requirementCategory.getId());
		updatedRequirementCategory.categoryName(UPDATED_CATEGORY_NAME);

		restRequirementCategoryMockMvc
				.perform(put("/api/requirement-categories/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedRequirementCategory)))
				.andExpect(status().isOk());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeUpdate);
		RequirementCategory testRequirementCategory = requirementCategoryList.get(requirementCategoryList.size() - 1);
		assertThat(testRequirementCategory.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
	}

	@Test
	@Transactional
	public void updateNonExistingRequirementCategoryForServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = requirementCategoryRepository.findAll().size();

		// Create the RequirementCategory

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restRequirementCategoryMockMvc
				.perform(put("/api/requirement-categories/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(requirementCategory)))
				.andExpect(status().isCreated());

		// Validate the RequirementCategory in the database
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteRequirementCategoryForServiceProvider() throws Exception {
		// Initialize the database
		requirementCategoryService.save(requirementCategory);

		int databaseSizeBeforeDelete = requirementCategoryRepository.findAll().size();

		// Get the requirementCategory
		restRequirementCategoryMockMvc.perform(delete("/api/requirement-categories/service-provider/{id}", requirementCategory.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<RequirementCategory> requirementCategoryList = requirementCategoryRepository.findAll();
		assertThat(requirementCategoryList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(RequirementCategory.class);
		RequirementCategory requirementCategory1 = new RequirementCategory();
		requirementCategory1.setId(1L);
		RequirementCategory requirementCategory2 = new RequirementCategory();
		requirementCategory2.setId(requirementCategory1.getId());
		assertThat(requirementCategory1).isEqualTo(requirementCategory2);
		requirementCategory2.setId(2L);
		assertThat(requirementCategory1).isNotEqualTo(requirementCategory2);
		requirementCategory1.setId(null);
		assertThat(requirementCategory1).isNotEqualTo(requirementCategory2);
	}
}
