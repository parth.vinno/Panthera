package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.RequirementNotTestedExplanation;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.RequirementNotTestedExplanationRepository;
import com.nollysoft.service.RequirementNotTestedExplanationService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the RequirementNotTestedExplanationResource REST controller.
 *
 * @see RequirementNotTestedExplanationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class RequirementNotTestedExplanationResourceIntTest {

    private static final String DEFAULT_REQUIREMENT_PART_NOT_TESTED = "AAAAAAAAAA";
    private static final String UPDATED_REQUIREMENT_PART_NOT_TESTED = "BBBBBBBBBB";

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private RequirementNotTestedExplanationRepository requirementNotTestedExplanationRepository;

    @Autowired
    private RequirementNotTestedExplanationService requirementNotTestedExplanationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRequirementNotTestedExplanationMockMvc;

    private RequirementNotTestedExplanation requirementNotTestedExplanation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RequirementNotTestedExplanationResource requirementNotTestedExplanationResource = new RequirementNotTestedExplanationResource(requirementNotTestedExplanationService);
        this.restRequirementNotTestedExplanationMockMvc = MockMvcBuilders.standaloneSetup(requirementNotTestedExplanationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequirementNotTestedExplanation createEntity(EntityManager em) {
        RequirementNotTestedExplanation requirementNotTestedExplanation = new RequirementNotTestedExplanation()
            .requirementPartNotTested(DEFAULT_REQUIREMENT_PART_NOT_TESTED)
            .reason(DEFAULT_REASON);
        // Add required entity
        SaqQuestion saqQuestionId = SaqQuestionResourceIntTest.createEntity(em);
        em.persist(saqQuestionId);
        em.flush();
        requirementNotTestedExplanation.setSaqQuestionId(saqQuestionId);
        return requirementNotTestedExplanation;
    }

    @Before
    public void initTest() {
        requirementNotTestedExplanation = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequirementNotTestedExplanation() throws Exception {
        int databaseSizeBeforeCreate = requirementNotTestedExplanationRepository.findAll().size();

        // Create the RequirementNotTestedExplanation
        restRequirementNotTestedExplanationMockMvc.perform(post("/api/requirement-not-tested-explanations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementNotTestedExplanation)))
            .andExpect(status().isCreated());

        // Validate the RequirementNotTestedExplanation in the database
        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeCreate + 1);
        RequirementNotTestedExplanation testRequirementNotTestedExplanation = requirementNotTestedExplanationList.get(requirementNotTestedExplanationList.size() - 1);
        assertThat(testRequirementNotTestedExplanation.getRequirementPartNotTested()).isEqualTo(DEFAULT_REQUIREMENT_PART_NOT_TESTED);
        assertThat(testRequirementNotTestedExplanation.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createRequirementNotTestedExplanationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requirementNotTestedExplanationRepository.findAll().size();

        // Create the RequirementNotTestedExplanation with an existing ID
        requirementNotTestedExplanation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequirementNotTestedExplanationMockMvc.perform(post("/api/requirement-not-tested-explanations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementNotTestedExplanation)))
            .andExpect(status().isBadRequest());

        // Validate the RequirementNotTestedExplanation in the database
        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkReasonIsRequired() throws Exception {
        int databaseSizeBeforeTest = requirementNotTestedExplanationRepository.findAll().size();
        // set the field null
        requirementNotTestedExplanation.setReason(null);

        // Create the RequirementNotTestedExplanation, which fails.

        restRequirementNotTestedExplanationMockMvc.perform(post("/api/requirement-not-tested-explanations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementNotTestedExplanation)))
            .andExpect(status().isBadRequest());

        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRequirementNotTestedExplanations() throws Exception {
        // Initialize the database
        requirementNotTestedExplanationRepository.saveAndFlush(requirementNotTestedExplanation);

        // Get all the requirementNotTestedExplanationList
        restRequirementNotTestedExplanationMockMvc.perform(get("/api/requirement-not-tested-explanations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requirementNotTestedExplanation.getId().intValue())))
            .andExpect(jsonPath("$.[*].requirementPartNotTested").value(hasItem(DEFAULT_REQUIREMENT_PART_NOT_TESTED.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void getRequirementNotTestedExplanation() throws Exception {
        // Initialize the database
        requirementNotTestedExplanationRepository.saveAndFlush(requirementNotTestedExplanation);

        // Get the requirementNotTestedExplanation
        restRequirementNotTestedExplanationMockMvc.perform(get("/api/requirement-not-tested-explanations/{id}", requirementNotTestedExplanation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(requirementNotTestedExplanation.getId().intValue()))
            .andExpect(jsonPath("$.requirementPartNotTested").value(DEFAULT_REQUIREMENT_PART_NOT_TESTED.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRequirementNotTestedExplanation() throws Exception {
        // Get the requirementNotTestedExplanation
        restRequirementNotTestedExplanationMockMvc.perform(get("/api/requirement-not-tested-explanations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequirementNotTestedExplanation() throws Exception {
        // Initialize the database
        requirementNotTestedExplanationService.save(requirementNotTestedExplanation);

        int databaseSizeBeforeUpdate = requirementNotTestedExplanationRepository.findAll().size();

        // Update the requirementNotTestedExplanation
        RequirementNotTestedExplanation updatedRequirementNotTestedExplanation = requirementNotTestedExplanationRepository.findOne(requirementNotTestedExplanation.getId());
        // Disconnect from session so that the updates on updatedRequirementNotTestedExplanation are not directly saved in db
        em.detach(updatedRequirementNotTestedExplanation);
        updatedRequirementNotTestedExplanation
            .requirementPartNotTested(UPDATED_REQUIREMENT_PART_NOT_TESTED)
            .reason(UPDATED_REASON);

        restRequirementNotTestedExplanationMockMvc.perform(put("/api/requirement-not-tested-explanations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRequirementNotTestedExplanation)))
            .andExpect(status().isOk());

        // Validate the RequirementNotTestedExplanation in the database
        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeUpdate);
        RequirementNotTestedExplanation testRequirementNotTestedExplanation = requirementNotTestedExplanationList.get(requirementNotTestedExplanationList.size() - 1);
        assertThat(testRequirementNotTestedExplanation.getRequirementPartNotTested()).isEqualTo(UPDATED_REQUIREMENT_PART_NOT_TESTED);
        assertThat(testRequirementNotTestedExplanation.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingRequirementNotTestedExplanation() throws Exception {
        int databaseSizeBeforeUpdate = requirementNotTestedExplanationRepository.findAll().size();

        // Create the RequirementNotTestedExplanation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRequirementNotTestedExplanationMockMvc.perform(put("/api/requirement-not-tested-explanations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementNotTestedExplanation)))
            .andExpect(status().isCreated());

        // Validate the RequirementNotTestedExplanation in the database
        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRequirementNotTestedExplanation() throws Exception {
        // Initialize the database
        requirementNotTestedExplanationService.save(requirementNotTestedExplanation);

        int databaseSizeBeforeDelete = requirementNotTestedExplanationRepository.findAll().size();

        // Get the requirementNotTestedExplanation
        restRequirementNotTestedExplanationMockMvc.perform(delete("/api/requirement-not-tested-explanations/{id}", requirementNotTestedExplanation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RequirementNotTestedExplanation> requirementNotTestedExplanationList = requirementNotTestedExplanationRepository.findAll();
        assertThat(requirementNotTestedExplanationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequirementNotTestedExplanation.class);
        RequirementNotTestedExplanation requirementNotTestedExplanation1 = new RequirementNotTestedExplanation();
        requirementNotTestedExplanation1.setId(1L);
        RequirementNotTestedExplanation requirementNotTestedExplanation2 = new RequirementNotTestedExplanation();
        requirementNotTestedExplanation2.setId(requirementNotTestedExplanation1.getId());
        assertThat(requirementNotTestedExplanation1).isEqualTo(requirementNotTestedExplanation2);
        requirementNotTestedExplanation2.setId(2L);
        assertThat(requirementNotTestedExplanation1).isNotEqualTo(requirementNotTestedExplanation2);
        requirementNotTestedExplanation1.setId(null);
        assertThat(requirementNotTestedExplanation1).isNotEqualTo(requirementNotTestedExplanation2);
    }
}
