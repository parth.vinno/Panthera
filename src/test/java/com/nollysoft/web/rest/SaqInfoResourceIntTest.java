package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.SaqInfo;
import com.nollysoft.repository.SaqInfoRepository;
import com.nollysoft.service.SaqInfoService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SaqInfoResource REST controller.
 *
 * @see SaqInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SaqInfoResourceIntTest {

    private static final String DEFAULT_SAQ_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_SAQ_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_REVISION_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_REVISION_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_SAQ_VERSION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SAQ_VERSION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private SaqInfoRepository saqInfoRepository;

    @Autowired
    private SaqInfoService saqInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSaqInfoMockMvc;

    private SaqInfo saqInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SaqInfoResource saqInfoResource = new SaqInfoResource(saqInfoService);
        this.restSaqInfoMockMvc = MockMvcBuilders.standaloneSetup(saqInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SaqInfo createEntity(EntityManager em) {
        SaqInfo saqInfo = new SaqInfo()
            .saqVersion(DEFAULT_SAQ_VERSION)
            .revisionNumber(DEFAULT_REVISION_NUMBER)
            .description(DEFAULT_DESCRIPTION)
            .saqVersionDate(DEFAULT_SAQ_VERSION_DATE);
        return saqInfo;
    }

    @Before
    public void initTest() {
        saqInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createSaqInfoForServiceProvider() throws Exception {
        int databaseSizeBeforeCreate = saqInfoRepository.findAll().size();

        // Create the SaqInfo
        restSaqInfoMockMvc.perform(post("/api/saq-infos/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isCreated());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeCreate + 1);
        SaqInfo testSaqInfo = saqInfoList.get(saqInfoList.size() - 1);
        assertThat(testSaqInfo.getSaqVersion()).isEqualTo(DEFAULT_SAQ_VERSION);
        assertThat(testSaqInfo.getRevisionNumber()).isEqualTo(DEFAULT_REVISION_NUMBER);
        assertThat(testSaqInfo.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSaqInfo.getSaqVersionDate()).isEqualTo(DEFAULT_SAQ_VERSION_DATE);
    }

    @Test
    @Transactional
    public void createSaqInfoWithExistingIdForServiceProvider() throws Exception {
        int databaseSizeBeforeCreate = saqInfoRepository.findAll().size();

        // Create the SaqInfo with an existing ID
        saqInfo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaqInfoMockMvc.perform(post("/api/saq-infos/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isBadRequest());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSaqInfosForServiceProvider() throws Exception {
        // Initialize the database
        saqInfoRepository.saveAndFlush(saqInfo);

        // Get all the saqInfoList
        restSaqInfoMockMvc.perform(get("/api/saq-infos/service-provider?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saqInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].saqVersion").value(hasItem(DEFAULT_SAQ_VERSION.toString())))
            .andExpect(jsonPath("$.[*].revisionNumber").value(hasItem(DEFAULT_REVISION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].saqVersionDate").value(hasItem(DEFAULT_SAQ_VERSION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getSaqInfoForServiceProvider() throws Exception {
        // Initialize the database
        saqInfoRepository.saveAndFlush(saqInfo);

        // Get the saqInfo
        restSaqInfoMockMvc.perform(get("/api/saq-infos/service-provider/{id}", saqInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(saqInfo.getId().intValue()))
            .andExpect(jsonPath("$.saqVersion").value(DEFAULT_SAQ_VERSION.toString()))
            .andExpect(jsonPath("$.revisionNumber").value(DEFAULT_REVISION_NUMBER.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.saqVersionDate").value(DEFAULT_SAQ_VERSION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSaqInfoForServiceProvider() throws Exception {
        // Get the saqInfo
        restSaqInfoMockMvc.perform(get("/api/saq-infos/service-provider/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaqInfoForServiceProvider() throws Exception {
        // Initialize the database
        saqInfoService.save(saqInfo);

        int databaseSizeBeforeUpdate = saqInfoRepository.findAll().size();

        // Update the saqInfo
        SaqInfo updatedSaqInfo = saqInfoRepository.findOne(saqInfo.getId());
        // Disconnect from session so that the updates on updatedSaqInfo are not directly saved in db
        em.detach(updatedSaqInfo);
        updatedSaqInfo
            .saqVersion(UPDATED_SAQ_VERSION)
            .revisionNumber(UPDATED_REVISION_NUMBER)
            .description(UPDATED_DESCRIPTION)
            .saqVersionDate(UPDATED_SAQ_VERSION_DATE);

        restSaqInfoMockMvc.perform(put("/api/saq-infos/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSaqInfo)))
            .andExpect(status().isOk());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeUpdate);
        SaqInfo testSaqInfo = saqInfoList.get(saqInfoList.size() - 1);
        assertThat(testSaqInfo.getSaqVersion()).isEqualTo(UPDATED_SAQ_VERSION);
        assertThat(testSaqInfo.getRevisionNumber()).isEqualTo(UPDATED_REVISION_NUMBER);
        assertThat(testSaqInfo.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSaqInfo.getSaqVersionDate()).isEqualTo(UPDATED_SAQ_VERSION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSaqInfoForServiceProvider() throws Exception {
        int databaseSizeBeforeUpdate = saqInfoRepository.findAll().size();

        // Create the SaqInfo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSaqInfoMockMvc.perform(put("/api/saq-infos/service-provider")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isCreated());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSaqInfoForServiceProvider() throws Exception {
        // Initialize the database
        saqInfoService.save(saqInfo);

        int databaseSizeBeforeDelete = saqInfoRepository.findAll().size();

        // Get the saqInfo
        restSaqInfoMockMvc.perform(delete("/api/saq-infos/service-provider/{id}", saqInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    public void createSaqInfoForMerchant() throws Exception {
        int databaseSizeBeforeCreate = saqInfoRepository.findAll().size();

        // Create the SaqInfo
        restSaqInfoMockMvc.perform(post("/api/saq-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isCreated());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeCreate + 1);
        SaqInfo testSaqInfo = saqInfoList.get(saqInfoList.size() - 1);
        assertThat(testSaqInfo.getSaqVersion()).isEqualTo(DEFAULT_SAQ_VERSION);
        assertThat(testSaqInfo.getRevisionNumber()).isEqualTo(DEFAULT_REVISION_NUMBER);
        assertThat(testSaqInfo.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSaqInfo.getSaqVersionDate()).isEqualTo(DEFAULT_SAQ_VERSION_DATE);
    }

    @Test
    @Transactional
    public void createSaqInfoWithExistingIdForMerchant() throws Exception {
        int databaseSizeBeforeCreate = saqInfoRepository.findAll().size();

        // Create the SaqInfo with an existing ID
        saqInfo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaqInfoMockMvc.perform(post("/api/saq-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isBadRequest());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSaqInfosForMerchant() throws Exception {
        // Initialize the database
        saqInfoRepository.saveAndFlush(saqInfo);

        // Get all the saqInfoList
        restSaqInfoMockMvc.perform(get("/api/saq-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saqInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].saqVersion").value(hasItem(DEFAULT_SAQ_VERSION.toString())))
            .andExpect(jsonPath("$.[*].revisionNumber").value(hasItem(DEFAULT_REVISION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].saqVersionDate").value(hasItem(DEFAULT_SAQ_VERSION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getSaqInfoForMerchant() throws Exception {
        // Initialize the database
        saqInfoRepository.saveAndFlush(saqInfo);

        // Get the saqInfo
        restSaqInfoMockMvc.perform(get("/api/saq-infos/{id}", saqInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(saqInfo.getId().intValue()))
            .andExpect(jsonPath("$.saqVersion").value(DEFAULT_SAQ_VERSION.toString()))
            .andExpect(jsonPath("$.revisionNumber").value(DEFAULT_REVISION_NUMBER.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.saqVersionDate").value(DEFAULT_SAQ_VERSION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSaqInfoForMerchant() throws Exception {
        // Get the saqInfo
        restSaqInfoMockMvc.perform(get("/api/saq-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaqInfoForMerchant() throws Exception {
        // Initialize the database
        saqInfoService.save(saqInfo);

        int databaseSizeBeforeUpdate = saqInfoRepository.findAll().size();

        // Update the saqInfo
        SaqInfo updatedSaqInfo = saqInfoRepository.findOne(saqInfo.getId());
        // Disconnect from session so that the updates on updatedSaqInfo are not directly saved in db
        em.detach(updatedSaqInfo);
        updatedSaqInfo
            .saqVersion(UPDATED_SAQ_VERSION)
            .revisionNumber(UPDATED_REVISION_NUMBER)
            .description(UPDATED_DESCRIPTION)
            .saqVersionDate(UPDATED_SAQ_VERSION_DATE);

        restSaqInfoMockMvc.perform(put("/api/saq-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSaqInfo)))
            .andExpect(status().isOk());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeUpdate);
        SaqInfo testSaqInfo = saqInfoList.get(saqInfoList.size() - 1);
        assertThat(testSaqInfo.getSaqVersion()).isEqualTo(UPDATED_SAQ_VERSION);
        assertThat(testSaqInfo.getRevisionNumber()).isEqualTo(UPDATED_REVISION_NUMBER);
        assertThat(testSaqInfo.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSaqInfo.getSaqVersionDate()).isEqualTo(UPDATED_SAQ_VERSION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSaqInfoForMerchant() throws Exception {
        int databaseSizeBeforeUpdate = saqInfoRepository.findAll().size();

        // Create the SaqInfo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSaqInfoMockMvc.perform(put("/api/saq-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqInfo)))
            .andExpect(status().isCreated());

        // Validate the SaqInfo in the database
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSaqInfoForMerchant() throws Exception {
        // Initialize the database
        saqInfoService.save(saqInfo);

        int databaseSizeBeforeDelete = saqInfoRepository.findAll().size();

        // Get the saqInfo
        restSaqInfoMockMvc.perform(delete("/api/saq-infos/{id}", saqInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SaqInfo> saqInfoList = saqInfoRepository.findAll();
        assertThat(saqInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SaqInfo.class);
        SaqInfo saqInfo1 = new SaqInfo();
        saqInfo1.setId(1L);
        SaqInfo saqInfo2 = new SaqInfo();
        saqInfo2.setId(saqInfo1.getId());
        assertThat(saqInfo1).isEqualTo(saqInfo2);
        saqInfo2.setId(2L);
        assertThat(saqInfo1).isNotEqualTo(saqInfo2);
        saqInfo1.setId(null);
        assertThat(saqInfo1).isNotEqualTo(saqInfo2);
    }
}
