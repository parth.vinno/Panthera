package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.repository.SaqQuestionRepository;
import com.nollysoft.service.SaqQuestionService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SaqQuestionResource REST controller.
 *
 * @see SaqQuestionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SaqQuestionResourceIntTest {

	private static final String DEFAULT_QUESTION_NUMBER = "AAAAAAAAAA";
	private static final String UPDATED_QUESTION_NUMBER = "BBBBBBBBBB";

	private static final Boolean DEFAULT_RESPONSE_REQUIRED = false;
	private static final Boolean UPDATED_RESPONSE_REQUIRED = true;

	private static final Boolean DEFAULT_PART = false;
	private static final Boolean UPDATED_PART = true;

	private static final String DEFAULT_QUESTION_TEXT = "AAAAAAAAAA";
	private static final String UPDATED_QUESTION_TEXT = "BBBBBBBBBB";

	@Autowired
	private SaqQuestionRepository saqQuestionRepository;

	@Autowired
	private SaqQuestionService saqQuestionService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restSaqQuestionMockMvc;

	private SaqQuestion saqQuestion;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final SaqQuestionResource saqQuestionResource = new SaqQuestionResource(saqQuestionService);
		this.restSaqQuestionMockMvc = MockMvcBuilders.standaloneSetup(saqQuestionResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static SaqQuestion createEntity(EntityManager em) {
		SaqQuestion saqQuestion = new SaqQuestion().questionNumber(DEFAULT_QUESTION_NUMBER)
				.responseRequired(DEFAULT_RESPONSE_REQUIRED).part(DEFAULT_PART).questionText(DEFAULT_QUESTION_TEXT);
		// Add required entity
		PcidssRequirement pcidssRequirementId = PcidssRequirementResourceIntTest.createEntity(em);
		em.persist(pcidssRequirementId);
		em.flush();
		saqQuestion.setPcidssRequirementId(pcidssRequirementId);
		return saqQuestion;
	}

	@Before
	public void initTest() {
		saqQuestion = createEntity(em);

	}

	@Test
	@Transactional
	public void createSaqQuestion() throws Exception {
		int databaseSizeBeforeCreate = saqQuestionRepository.findAll().size();

		// Create the SaqQuestion
		restSaqQuestionMockMvc.perform(post("/api/saq-questions").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqQuestion))).andExpect(status().isCreated());

		// Validate the SaqQuestion in the database
		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeCreate + 1);
		SaqQuestion testSaqQuestion = saqQuestionList.get(saqQuestionList.size() - 1);
		assertThat(testSaqQuestion.getQuestionNumber()).isEqualTo(DEFAULT_QUESTION_NUMBER);
		assertThat(testSaqQuestion.isResponseRequired()).isEqualTo(DEFAULT_RESPONSE_REQUIRED);
		assertThat(testSaqQuestion.getQuestionText()).isEqualTo(DEFAULT_QUESTION_TEXT);
	}

	@Test
	@Transactional
	public void createSaqQuestionWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = saqQuestionRepository.findAll().size();

		// Create the SaqQuestion with an existing ID
		saqQuestion.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restSaqQuestionMockMvc.perform(post("/api/saq-questions").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqQuestion))).andExpect(status().isBadRequest());

		// Validate the SaqQuestion in the database
		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkQuestionNumberIsRequired() throws Exception {
		int databaseSizeBeforeTest = saqQuestionRepository.findAll().size();
		// set the field null
		saqQuestion.setQuestionNumber(null);

		// Create the SaqQuestion, which fails.

		restSaqQuestionMockMvc.perform(post("/api/saq-questions").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqQuestion))).andExpect(status().isBadRequest());

		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllSaqQuestions() throws Exception {
		// Initialize the database
		saqQuestionRepository.saveAndFlush(saqQuestion);

		// Get all the saqQuestionList
		restSaqQuestionMockMvc.perform(get("/api/saq-questions?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(saqQuestion.getId().intValue())))
				.andExpect(jsonPath("$.[*].questionNumber").value(hasItem(DEFAULT_QUESTION_NUMBER.toString())))
				.andExpect(jsonPath("$.[*].responseRequired").value(hasItem(DEFAULT_RESPONSE_REQUIRED.booleanValue())))
				.andExpect(jsonPath("$.[*].questionText").value(hasItem(DEFAULT_QUESTION_TEXT.toString())));
	}

	@Test
	@Transactional
	public void getSaqQuestion() throws Exception {
		// Initialize the database
		saqQuestionRepository.saveAndFlush(saqQuestion);

		// Get the saqQuestion
		restSaqQuestionMockMvc.perform(get("/api/saq-questions/{id}", saqQuestion.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(saqQuestion.getId().intValue()))
				.andExpect(jsonPath("$.questionNumber").value(DEFAULT_QUESTION_NUMBER.toString()))
				.andExpect(jsonPath("$.responseRequired").value(DEFAULT_RESPONSE_REQUIRED.booleanValue()))
				.andExpect(jsonPath("$.questionText").value(DEFAULT_QUESTION_TEXT.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingSaqQuestion() throws Exception {
		// Get the saqQuestion
		restSaqQuestionMockMvc.perform(get("/api/saq-questions/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateSaqQuestion() throws Exception {
		// Initialize the database
		saqQuestionRepository.save(saqQuestion);

		int databaseSizeBeforeUpdate = saqQuestionRepository.findAll().size();

		// Update the saqQuestion
		SaqQuestion updatedSaqQuestion = saqQuestionRepository.findOne(saqQuestion.getId());
		// Disconnect from session so that the updates on updatedSaqQuestion are not
		// directly saved in db
		em.detach(updatedSaqQuestion);
		updatedSaqQuestion.questionNumber(UPDATED_QUESTION_NUMBER).responseRequired(UPDATED_RESPONSE_REQUIRED)
				.questionText(UPDATED_QUESTION_TEXT).part(UPDATED_PART);

		restSaqQuestionMockMvc.perform(put("/api/saq-questions").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedSaqQuestion))).andExpect(status().isOk());

		// Validate the SaqQuestion in the database
		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeUpdate);
		SaqQuestion testSaqQuestion = saqQuestionList.get(saqQuestionList.size() - 1);
		assertThat(testSaqQuestion.getQuestionNumber()).isEqualTo(UPDATED_QUESTION_NUMBER);
		assertThat(testSaqQuestion.isResponseRequired()).isEqualTo(UPDATED_RESPONSE_REQUIRED);
		assertThat(testSaqQuestion.getQuestionText()).isEqualTo(UPDATED_QUESTION_TEXT);
		assertThat(testSaqQuestion.isPart()).isEqualTo(UPDATED_PART);
	}

	@Test
	@Transactional
	public void updateNonExistingSaqQuestion() throws Exception {
		int databaseSizeBeforeUpdate = saqQuestionRepository.findAll().size();

		// Create the SaqQuestion

		// If the entity doesn't have an ID, it will be created instead of just being
		// updated
		restSaqQuestionMockMvc.perform(put("/api/saq-questions").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqQuestion))).andExpect(status().isCreated());

		// Validate the SaqQuestion in the database
		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteSaqQuestion() throws Exception {
		// Initialize the database
		saqQuestionRepository.save(saqQuestion);

		int databaseSizeBeforeDelete = saqQuestionRepository.findAll().size();

		// Get the saqQuestion
		restSaqQuestionMockMvc
				.perform(delete("/api/saq-questions/{id}", saqQuestion.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<SaqQuestion> saqQuestionList = saqQuestionRepository.findAll();
		assertThat(saqQuestionList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(SaqQuestion.class);
		SaqQuestion saqQuestion1 = new SaqQuestion();
		saqQuestion1.setId(1L);
		SaqQuestion saqQuestion2 = new SaqQuestion();
		saqQuestion2.setId(saqQuestion1.getId());
		assertThat(saqQuestion1).isEqualTo(saqQuestion2);
		saqQuestion2.setId(2L);
		assertThat(saqQuestion1).isNotEqualTo(saqQuestion2);
		saqQuestion1.setId(null);
		assertThat(saqQuestion1).isNotEqualTo(saqQuestion2);
	}
}
