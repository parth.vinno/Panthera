package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.domain.SaqResponse;
import com.nollysoft.repository.SaqResponseRepository;
import com.nollysoft.service.SaqResponseService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SaqResponseResource REST controller.
 *
 * @see SaqResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SaqResponseResourceIntTest {

    private static final SaqResponse.QUESTION_RESPONSE DEFAULT_QUESTION_RESPONSE = SaqResponse.QUESTION_RESPONSE.YES;
    private static final SaqResponse.QUESTION_RESPONSE UPDATED_QUESTION_RESPONSE = SaqResponse.QUESTION_RESPONSE.NO;

    @Autowired
    private SaqResponseRepository saqResponseRepository;

    @Autowired
    private SaqResponseService saqResponseService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSaqResponseMockMvc;

    private SaqResponse saqResponse;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SaqResponseResource saqResponseResource = new SaqResponseResource(saqResponseService);
        this.restSaqResponseMockMvc = MockMvcBuilders.standaloneSetup(saqResponseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SaqResponse createEntity(EntityManager em) {
        SaqResponse saqResponse = new SaqResponse()
            .questionResponse(DEFAULT_QUESTION_RESPONSE);
		// Add required entity
		SaqQuestion saqQuestionId = SaqQuestionResourceIntTest.createEntity(em);
		em.persist(saqQuestionId);
		em.flush();
		saqResponse.setSaqQuestionId(saqQuestionId);
        return saqResponse;
    }

    @Before
    public void initTest() {
        saqResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createSaqResponse() throws Exception {
        int databaseSizeBeforeCreate = saqResponseRepository.findAll().size();

        // Create the SaqResponse
        restSaqResponseMockMvc.perform(post("/api/saq-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqResponse)))
            .andExpect(status().isCreated());

        // Validate the SaqResponse in the database
        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeCreate + 1);
        SaqResponse testSaqResponse = saqResponseList.get(saqResponseList.size() - 1);
        assertThat(testSaqResponse.getQuestionResponse()).isEqualTo(DEFAULT_QUESTION_RESPONSE);
    }

    @Test
    @Transactional
    public void createSaqResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = saqResponseRepository.findAll().size();

        // Create the SaqResponse with an existing ID
        saqResponse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaqResponseMockMvc.perform(post("/api/saq-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqResponse)))
            .andExpect(status().isBadRequest());

        // Validate the SaqResponse in the database
        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkQuestionResponseIsRequired() throws Exception {
        int databaseSizeBeforeTest = saqResponseRepository.findAll().size();
        // set the field null
        saqResponse.setQuestionResponse(null);

        // Create the SaqResponse, which fails.

        restSaqResponseMockMvc.perform(post("/api/saq-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqResponse)))
            .andExpect(status().isBadRequest());

        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSaqResponses() throws Exception {
        // Initialize the database
        saqResponseRepository.saveAndFlush(saqResponse);

        // Get all the saqResponseList
        restSaqResponseMockMvc.perform(get("/api/saq-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saqResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].questionResponse").value(hasItem(DEFAULT_QUESTION_RESPONSE.toString())));
    }

    @Test
    @Transactional
    public void getSaqResponse() throws Exception {
        // Initialize the database
        saqResponseRepository.saveAndFlush(saqResponse);

        // Get the saqResponse
        restSaqResponseMockMvc.perform(get("/api/saq-responses/{id}", saqResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(saqResponse.getId().intValue()))
            .andExpect(jsonPath("$.questionResponse").value(DEFAULT_QUESTION_RESPONSE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSaqResponse() throws Exception {
        // Get the saqResponse
        restSaqResponseMockMvc.perform(get("/api/saq-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaqResponse() throws Exception {
        // Initialize the database
        saqResponseService.save(saqResponse);

        int databaseSizeBeforeUpdate = saqResponseRepository.findAll().size();

        // Update the saqResponse
        SaqResponse updatedSaqResponse = saqResponseRepository.findOne(saqResponse.getId());
        // Disconnect from session so that the updates on updatedSaqResponse are not directly saved in db
        em.detach(updatedSaqResponse);
        updatedSaqResponse
            .questionResponse(UPDATED_QUESTION_RESPONSE);

        restSaqResponseMockMvc.perform(put("/api/saq-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSaqResponse)))
            .andExpect(status().isOk());

        // Validate the SaqResponse in the database
        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeUpdate);
        SaqResponse testSaqResponse = saqResponseList.get(saqResponseList.size() - 1);
        assertThat(testSaqResponse.getQuestionResponse()).isEqualTo(UPDATED_QUESTION_RESPONSE);
    }

    @Test
    @Transactional
    public void updateNonExistingSaqResponse() throws Exception {
        int databaseSizeBeforeUpdate = saqResponseRepository.findAll().size();

        // Create the SaqResponse

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSaqResponseMockMvc.perform(put("/api/saq-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saqResponse)))
            .andExpect(status().isCreated());

        // Validate the SaqResponse in the database
        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSaqResponse() throws Exception {
        // Initialize the database
        saqResponseService.save(saqResponse);

        int databaseSizeBeforeDelete = saqResponseRepository.findAll().size();

        // Get the saqResponse
        restSaqResponseMockMvc.perform(delete("/api/saq-responses/{id}", saqResponse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SaqResponse> saqResponseList = saqResponseRepository.findAll();
        assertThat(saqResponseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SaqResponse.class);
        SaqResponse saqResponse1 = new SaqResponse();
        saqResponse1.setId(1L);
        SaqResponse saqResponse2 = new SaqResponse();
        saqResponse2.setId(saqResponse1.getId());
        assertThat(saqResponse1).isEqualTo(saqResponse2);
        saqResponse2.setId(2L);
        assertThat(saqResponse1).isNotEqualTo(saqResponse2);
        saqResponse1.setId(null);
        assertThat(saqResponse1).isNotEqualTo(saqResponse2);
    }
}