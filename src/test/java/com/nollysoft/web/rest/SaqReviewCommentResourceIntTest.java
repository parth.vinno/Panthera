package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.SaqReview;
import com.nollysoft.domain.SaqReviewComment;
import com.nollysoft.repository.SaqReviewCommentRepository;
import com.nollysoft.service.SaqReviewCommentService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SaqReviewCommentResource REST controller.
 *
 * @see SaqReviewCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SaqReviewCommentResourceIntTest {

	private static final String DEFAULT_COMMENT = "AAAAAAAAAA";

	@Autowired
	private SaqReviewCommentRepository saqReviewCommentRepository;

	@Autowired
	private SaqReviewCommentService saqReviewCommentService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restSaqReviewCommentMockMvc;

	private SaqReviewComment saqReviewComment;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final SaqReviewCommentResource saqReviewCommentResource = new SaqReviewCommentResource(saqReviewCommentService);
		this.restSaqReviewCommentMockMvc = MockMvcBuilders.standaloneSetup(saqReviewCommentResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static SaqReviewComment createEntity(EntityManager em) {
		SaqReviewComment saqReviewComment = new SaqReviewComment().comment(DEFAULT_COMMENT);
		// Add required entity
		SaqReview saqReviewId = SaqReviewResourceIntTest.createEntity(em);
		em.persist(saqReviewId);
		em.flush();
		saqReviewComment.setSaqReviewId(saqReviewId);
		return saqReviewComment;
	}

	@Before
	public void initTest() {
		saqReviewComment = createEntity(em);
	}

	@Test
	@Transactional
	public void getAllSaqReviewComments() throws Exception {
		// Initialize the database
		saqReviewCommentRepository.saveAndFlush(saqReviewComment);

		// Get all the saqReviewCommentList
		restSaqReviewCommentMockMvc.perform(get("/api/saq-review-comments?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(saqReviewComment.getId().intValue())))
				.andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
	}

	@Test
	@Transactional
	public void getSaqReviewComment() throws Exception {
		// Initialize the database
		saqReviewCommentRepository.saveAndFlush(saqReviewComment);

		// Get the saqReviewComment
		restSaqReviewCommentMockMvc.perform(get("/api/saq-review-comments/{id}", saqReviewComment.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(saqReviewComment.getId().intValue()))
				.andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingSaqReviewComment() throws Exception {
		// Get the saqReviewComment
		restSaqReviewCommentMockMvc.perform(get("/api/saq-review-comments/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void deleteSaqReviewComment() throws Exception {
		// Initialize the database
		saqReviewCommentService.save(saqReviewComment);

		int databaseSizeBeforeDelete = saqReviewCommentRepository.findAll().size();

		// Get the saqReviewComment
		restSaqReviewCommentMockMvc.perform(delete("/api/saq-review-comments/{id}", saqReviewComment.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<SaqReviewComment> saqReviewCommentList = saqReviewCommentRepository.findAll();
		assertThat(saqReviewCommentList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(SaqReviewComment.class);
		SaqReviewComment saqReviewComment1 = new SaqReviewComment();
		saqReviewComment1.setId(1L);
		SaqReviewComment saqReviewComment2 = new SaqReviewComment();
		saqReviewComment2.setId(saqReviewComment1.getId());
		assertThat(saqReviewComment1).isEqualTo(saqReviewComment2);
		saqReviewComment2.setId(2L);
		assertThat(saqReviewComment1).isNotEqualTo(saqReviewComment2);
		saqReviewComment1.setId(null);
		assertThat(saqReviewComment1).isNotEqualTo(saqReviewComment2);
	}
}
