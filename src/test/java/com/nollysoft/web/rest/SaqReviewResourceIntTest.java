package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.SaqQuestion;
import com.nollysoft.domain.SaqReview;
import com.nollysoft.domain.SaqReview.REVIEW_STATUS;
import com.nollysoft.repository.SaqReviewRepository;
import com.nollysoft.service.SaqReviewService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SaqReviewResource REST controller.
 *
 * @see SaqReviewResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SaqReviewResourceIntTest {

	private static final REVIEW_STATUS DEFAULT_STATUS = REVIEW_STATUS.REVIEWED;
	private static final REVIEW_STATUS UPDATED_STATUS = REVIEW_STATUS.CORRECTED;
	private static final LocalDateTime CREATED_DATE = LocalDateTime.now();
	private static final LocalDateTime UPDATED_DATE = LocalDateTime.now(ZoneId.systemDefault());

	@Autowired
	private SaqReviewRepository saqReviewRepository;

	@Autowired
	private SaqReviewService saqReviewService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restSaqReviewMockMvc;

	private SaqReview saqReview;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final SaqReviewResource saqReviewResource = new SaqReviewResource(saqReviewService);
		this.restSaqReviewMockMvc = MockMvcBuilders.standaloneSetup(saqReviewResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static SaqReview createEntity(EntityManager em) {
		SaqReview saqReview = new SaqReview().status(DEFAULT_STATUS).createdDate(CREATED_DATE)
				.lastUpdatedDate(UPDATED_DATE);
		// Add required entity
		SaqQuestion saqQuestionId = SaqQuestionResourceIntTest.createEntity(em);
		em.persist(saqQuestionId);
		em.flush();
		saqReview.setSaqQuestionId(saqQuestionId);
		return saqReview;
	}

	@Before
	public void initTest() {
		saqReview = createEntity(em);
	}

	@Test
	@Transactional
	public void createSaqReview() throws Exception {
		int databaseSizeBeforeCreate = saqReviewRepository.findAll().size();

		// Create the SaqReview
		restSaqReviewMockMvc.perform(post("/api/saq-reviews").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqReview))).andExpect(status().isCreated());

		// Validate the SaqReview in the database
		List<SaqReview> saqReviewList = saqReviewRepository.findAll();
		assertThat(saqReviewList).hasSize(databaseSizeBeforeCreate + 1);
		SaqReview testSaqReview = saqReviewList.get(saqReviewList.size() - 1);
		assertThat(testSaqReview.getStatusType()).isEqualTo(DEFAULT_STATUS);
	}

	@Test
	@Transactional
	public void createSaqReviewWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = saqReviewRepository.findAll().size();

		// Create the SaqReview with an existing ID
		saqReview.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restSaqReviewMockMvc.perform(post("/api/saq-reviews").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqReview))).andExpect(status().isBadRequest());

		// Validate the SaqReview in the database
		List<SaqReview> saqReviewList = saqReviewRepository.findAll();
		assertThat(saqReviewList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllSaqReviews() throws Exception {
		// Initialize the database
		saqReviewRepository.saveAndFlush(saqReview);

		// Get all the saqReviewList
		restSaqReviewMockMvc.perform(get("/api/saq-reviews?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(saqReview.getId().intValue())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
	}

	@Test
	@Transactional
	public void getSaqReview() throws Exception {
		// Initialize the database
		saqReviewRepository.saveAndFlush(saqReview);

		// Get the saqReview
		restSaqReviewMockMvc.perform(get("/api/saq-reviews/{id}", saqReview.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(saqReview.getId().intValue()))
				.andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingSaqReview() throws Exception {
		// Get the saqReview
		restSaqReviewMockMvc.perform(get("/api/saq-reviews/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateSaqReview() throws Exception {
		// Initialize the database
		saqReviewService.save(saqReview);

		int databaseSizeBeforeUpdate = saqReviewRepository.findAll().size();

		// Update the saqReview
		SaqReview updatedSaqReview = saqReviewRepository.findOne(saqReview.getId());
		updatedSaqReview.status(UPDATED_STATUS);

		restSaqReviewMockMvc.perform(put("/api/saq-reviews").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedSaqReview))).andExpect(status().isOk());

		// Validate the SaqReview in the database
		List<SaqReview> saqReviewList = saqReviewRepository.findAll();
		assertThat(saqReviewList).hasSize(databaseSizeBeforeUpdate);
		SaqReview testSaqReview = saqReviewList.get(saqReviewList.size() - 1);
		assertThat(testSaqReview.getStatusType()).isEqualTo(UPDATED_STATUS);
	}
	
	@Test
	@Transactional
	public void updateNonExistingSaqReview() throws Exception {
		int databaseSizeBeforeUpdate = saqReviewRepository.findAll().size();

		// Create the SaqReview

		// If the entity doesn't have an ID, it will be created instead of just
		// being
		// updated
		restSaqReviewMockMvc.perform(put("/api/saq-reviews").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(saqReview))).andExpect(status().isCreated());

		// Validate the SaqReview in the database
		List<SaqReview> saqReviewList = saqReviewRepository.findAll();
		assertThat(saqReviewList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteSaqReview() throws Exception {
		// Initialize the database
		saqReviewService.save(saqReview);

		int databaseSizeBeforeDelete = saqReviewRepository.findAll().size();

		// Get the saqReview
		restSaqReviewMockMvc
				.perform(delete("/api/saq-reviews/{id}", saqReview.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<SaqReview> saqReviewList = saqReviewRepository.findAll();
		assertThat(saqReviewList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(SaqReview.class);
		SaqReview saqReview1 = new SaqReview();
		saqReview1.setId(1L);
		SaqReview saqReview2 = new SaqReview();
		saqReview2.setId(saqReview1.getId());
		assertThat(saqReview1).isEqualTo(saqReview2);
		saqReview2.setId(2L);
		assertThat(saqReview1).isNotEqualTo(saqReview2);
		saqReview1.setId(null);
		assertThat(saqReview1).isNotEqualTo(saqReview2);
	}
}
