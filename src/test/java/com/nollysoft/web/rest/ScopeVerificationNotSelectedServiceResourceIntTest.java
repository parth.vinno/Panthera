package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ScopeVerificationNotSelectedService;
import com.nollysoft.repository.ScopeVerificationNotSelectedServiceRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ScopeVerificationNotSelectedServiceService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ScopeVerificationNotSelectedServiceResource REST
 * controller.
 *
 * @see ScopeVerificationNotSelectedServiceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ScopeVerificationNotSelectedServiceResourceIntTest {

	private static final String DEFAULT_NOT_ASSESSED_SERVICE_NAME = "AAAAAAAAAA";
	private static final String UPDATED_NOT_ASSESSED_SERVICE_NAME = "BBBBBBBBBB";

	private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
	private static final String UPDATED_COMMENT = "BBBBBBBBBB";

	@Autowired
	private ScopeVerificationNotSelectedServiceRepository scopeVerificationNotSelectedServiceRepository;

	@Autowired
	private ScopeVerificationNotSelectedServiceService scopeVerificationNotSelectedServiceService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restScopeVerificationNotSelectedServiceMockMvc;

	private ScopeVerificationNotSelectedService scopeVerificationNotSelectedService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ScopeVerificationNotSelectedServiceResource scopeVerificationNotSelectedServiceResource = new ScopeVerificationNotSelectedServiceResource(
				scopeVerificationNotSelectedServiceService, executiveSummaryStatusService);
		this.restScopeVerificationNotSelectedServiceMockMvc = MockMvcBuilders
				.standaloneSetup(scopeVerificationNotSelectedServiceResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ScopeVerificationNotSelectedService createEntity(EntityManager em) {
		ScopeVerificationNotSelectedService scopeVerificationNotSelectedService = new ScopeVerificationNotSelectedService()
				.notAssessedServiceName(DEFAULT_NOT_ASSESSED_SERVICE_NAME).comment(DEFAULT_COMMENT);
		return scopeVerificationNotSelectedService;
	}

	@Before
	public void initTest() {
		scopeVerificationNotSelectedService = createEntity(em);
	}

	@Test
	@Transactional
	public void createScopeVerificationNotSelectedService() throws Exception {
		int databaseSizeBeforeCreate = scopeVerificationNotSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationNotSelectedService
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(post("/api/scope-verification-not-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationNotSelectedService)))
				.andExpect(status().isCreated());

		// Validate the ScopeVerificationNotSelectedService in the database
		List<ScopeVerificationNotSelectedService> scopeVerificationNotSelectedServiceList = scopeVerificationNotSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationNotSelectedServiceList).hasSize(databaseSizeBeforeCreate + 1);
		ScopeVerificationNotSelectedService testScopeVerificationNotSelectedService = scopeVerificationNotSelectedServiceList
				.get(scopeVerificationNotSelectedServiceList.size() - 1);
		assertThat(testScopeVerificationNotSelectedService.getNotAssessedServiceName())
				.isEqualTo(DEFAULT_NOT_ASSESSED_SERVICE_NAME);
		assertThat(testScopeVerificationNotSelectedService.getComment()).isEqualTo(DEFAULT_COMMENT);
	}

	@Test
	@Transactional
	public void createScopeVerificationNotSelectedServiceWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = scopeVerificationNotSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationNotSelectedService with an existing ID
		scopeVerificationNotSelectedService.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(post("/api/scope-verification-not-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationNotSelectedService)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<ScopeVerificationNotSelectedService> scopeVerificationNotSelectedServiceList = scopeVerificationNotSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationNotSelectedServiceList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllScopeVerificationNotSelectedServices() throws Exception {
		// Initialize the database
		scopeVerificationNotSelectedServiceRepository.saveAndFlush(scopeVerificationNotSelectedService);

		// Get all the scopeVerificationNotSelectedServiceList
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(get("/api/scope-verification-not-selected-services/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(scopeVerificationNotSelectedService.getId().intValue())))
				.andExpect(jsonPath("$.[*].notAssessedServiceName")
						.value(hasItem(DEFAULT_NOT_ASSESSED_SERVICE_NAME.toString())))
				.andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
	}

	@Test
	@Transactional
	public void getScopeVerificationNotSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationNotSelectedServiceRepository.saveAndFlush(scopeVerificationNotSelectedService);

		// Get the scopeVerificationNotSelectedService
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(get("/api/scope-verification-not-selected-services/service-provider/{id}",
						scopeVerificationNotSelectedService.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(scopeVerificationNotSelectedService.getId().intValue()))
				.andExpect(jsonPath("$.notAssessedServiceName").value(DEFAULT_NOT_ASSESSED_SERVICE_NAME.toString()))
				.andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingScopeVerificationNotSelectedService() throws Exception {
		// Get the scopeVerificationNotSelectedService
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(get("/api/scope-verification-not-selected-services/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateScopeVerificationNotSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationNotSelectedServiceService.save(scopeVerificationNotSelectedService);

		int databaseSizeBeforeUpdate = scopeVerificationNotSelectedServiceRepository.findAll().size();

		// Update the scopeVerificationNotSelectedService
		ScopeVerificationNotSelectedService updatedScopeVerificationNotSelectedService = scopeVerificationNotSelectedServiceRepository
				.findOne(scopeVerificationNotSelectedService.getId());
		updatedScopeVerificationNotSelectedService.notAssessedServiceName(UPDATED_NOT_ASSESSED_SERVICE_NAME)
				.comment(UPDATED_COMMENT);

		restScopeVerificationNotSelectedServiceMockMvc
				.perform(put("/api/scope-verification-not-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedScopeVerificationNotSelectedService)))
				.andExpect(status().isOk());

		// Validate the ScopeVerificationNotSelectedService in the database
		List<ScopeVerificationNotSelectedService> scopeVerificationNotSelectedServiceList = scopeVerificationNotSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationNotSelectedServiceList).hasSize(databaseSizeBeforeUpdate);
		ScopeVerificationNotSelectedService testScopeVerificationNotSelectedService = scopeVerificationNotSelectedServiceList
				.get(scopeVerificationNotSelectedServiceList.size() - 1);
		assertThat(testScopeVerificationNotSelectedService.getNotAssessedServiceName())
				.isEqualTo(UPDATED_NOT_ASSESSED_SERVICE_NAME);
		assertThat(testScopeVerificationNotSelectedService.getComment()).isEqualTo(UPDATED_COMMENT);
	}

	@Test
	@Transactional
	public void updateNonExistingScopeVerificationNotSelectedService() throws Exception {
		int databaseSizeBeforeUpdate = scopeVerificationNotSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationNotSelectedService

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(put("/api/scope-verification-not-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationNotSelectedService)))
				.andExpect(status().isCreated());

		// Validate the ScopeVerificationNotSelectedService in the database
		List<ScopeVerificationNotSelectedService> scopeVerificationNotSelectedServiceList = scopeVerificationNotSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationNotSelectedServiceList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteScopeVerificationNotSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationNotSelectedServiceService.save(scopeVerificationNotSelectedService);

		int databaseSizeBeforeDelete = scopeVerificationNotSelectedServiceRepository.findAll().size();

		// Get the scopeVerificationNotSelectedService
		restScopeVerificationNotSelectedServiceMockMvc
				.perform(delete("/api/scope-verification-not-selected-services/service-provider/{id}",
						scopeVerificationNotSelectedService.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ScopeVerificationNotSelectedService> scopeVerificationNotSelectedServiceList = scopeVerificationNotSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationNotSelectedServiceList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ScopeVerificationNotSelectedService.class);
		ScopeVerificationNotSelectedService scopeVerificationNotSelectedService1 = new ScopeVerificationNotSelectedService();
		scopeVerificationNotSelectedService1.setId(1L);
		ScopeVerificationNotSelectedService scopeVerificationNotSelectedService2 = new ScopeVerificationNotSelectedService();
		scopeVerificationNotSelectedService2.setId(scopeVerificationNotSelectedService1.getId());
		assertThat(scopeVerificationNotSelectedService1).isEqualTo(scopeVerificationNotSelectedService2);
		scopeVerificationNotSelectedService2.setId(2L);
		assertThat(scopeVerificationNotSelectedService1).isNotEqualTo(scopeVerificationNotSelectedService2);
		scopeVerificationNotSelectedService1.setId(null);
		assertThat(scopeVerificationNotSelectedService1).isNotEqualTo(scopeVerificationNotSelectedService2);
	}
}
