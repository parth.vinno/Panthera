package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ScopeVerificationSelectedService;
import com.nollysoft.repository.ScopeVerificationSelectedServiceRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ScopeVerificationSelectedServiceService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ScopeVerificationSelectedServiceResource REST controller.
 *
 * @see ScopeVerificationSelectedServiceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ScopeVerificationSelectedServiceResourceIntTest {

	private static final String DEFAULT_ASSESSED_SERVICE_NAME = "AAAAAAAAAA";
	private static final String UPDATED_ASSESSED_SERVICE_NAME = "BBBBBBBBBB";

	private static final String DEFAULT_ASSESSED_SERVICE_TYPE = "AAAAAAAAAA";
	private static final String UPDATED_ASSESSED_SERVICE_TYPE = "BBBBBBBBBB";

	@Autowired
	private ScopeVerificationSelectedServiceRepository scopeVerificationSelectedServiceRepository;

	@Autowired
	private ScopeVerificationSelectedServiceService scopeVerificationSelectedServiceService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restScopeVerificationSelectedServiceMockMvc;

	private ScopeVerificationSelectedService scopeVerificationSelectedService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ScopeVerificationSelectedServiceResource scopeVerificationSelectedServiceResource = new ScopeVerificationSelectedServiceResource(
				scopeVerificationSelectedServiceService, executiveSummaryStatusService);
		this.restScopeVerificationSelectedServiceMockMvc = MockMvcBuilders
				.standaloneSetup(scopeVerificationSelectedServiceResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ScopeVerificationSelectedService createEntity(EntityManager em) {
		ScopeVerificationSelectedService scopeVerificationSelectedService = new ScopeVerificationSelectedService()
				.assessedServiceName(DEFAULT_ASSESSED_SERVICE_NAME).assessedServiceType(DEFAULT_ASSESSED_SERVICE_TYPE);
		return scopeVerificationSelectedService;
	}

	@Before
	public void initTest() {
		scopeVerificationSelectedService = createEntity(em);
	}

	@Test
	@Transactional
	public void createScopeVerificationSelectedService() throws Exception {
		int databaseSizeBeforeCreate = scopeVerificationSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationSelectedService
		restScopeVerificationSelectedServiceMockMvc
				.perform(post("/api/scope-verification-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationSelectedService)))
				.andExpect(status().isCreated());

		// Validate the ScopeVerificationSelectedService in the database
		List<ScopeVerificationSelectedService> scopeVerificationSelectedServiceList = scopeVerificationSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationSelectedServiceList).hasSize(databaseSizeBeforeCreate + 1);
		ScopeVerificationSelectedService testScopeVerificationSelectedService = scopeVerificationSelectedServiceList
				.get(scopeVerificationSelectedServiceList.size() - 1);
		assertThat(testScopeVerificationSelectedService.getAssessedServiceName())
				.isEqualTo(DEFAULT_ASSESSED_SERVICE_NAME);
		assertThat(testScopeVerificationSelectedService.getAssessedServiceType())
				.isEqualTo(DEFAULT_ASSESSED_SERVICE_TYPE);
	}

	@Test
	@Transactional
	public void createScopeVerificationSelectedServiceWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = scopeVerificationSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationSelectedService with an existing ID
		scopeVerificationSelectedService.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restScopeVerificationSelectedServiceMockMvc
				.perform(post("/api/scope-verification-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationSelectedService)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<ScopeVerificationSelectedService> scopeVerificationSelectedServiceList = scopeVerificationSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationSelectedServiceList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllScopeVerificationSelectedServices() throws Exception {
		// Initialize the database
		scopeVerificationSelectedServiceRepository.saveAndFlush(scopeVerificationSelectedService);

		// Get all the scopeVerificationSelectedServiceList
		restScopeVerificationSelectedServiceMockMvc
				.perform(get("/api/scope-verification-selected-services/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(scopeVerificationSelectedService.getId().intValue())))
				.andExpect(
						jsonPath("$.[*].assessedServiceName").value(hasItem(DEFAULT_ASSESSED_SERVICE_NAME.toString())))
				.andExpect(
						jsonPath("$.[*].assessedServiceType").value(hasItem(DEFAULT_ASSESSED_SERVICE_TYPE.toString())));
	}

	@Test
	@Transactional
	public void getScopeVerificationSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationSelectedServiceRepository.saveAndFlush(scopeVerificationSelectedService);

		// Get the scopeVerificationSelectedService
		restScopeVerificationSelectedServiceMockMvc
				.perform(get("/api/scope-verification-selected-services/service-provider/{id}",
						scopeVerificationSelectedService.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(scopeVerificationSelectedService.getId().intValue()))
				.andExpect(jsonPath("$.assessedServiceName").value(DEFAULT_ASSESSED_SERVICE_NAME.toString()))
				.andExpect(jsonPath("$.assessedServiceType").value(DEFAULT_ASSESSED_SERVICE_TYPE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingScopeVerificationSelectedService() throws Exception {
		// Get the scopeVerificationSelectedService
		restScopeVerificationSelectedServiceMockMvc
				.perform(get("/api/scope-verification-selected-services/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateScopeVerificationSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationSelectedServiceService.save(scopeVerificationSelectedService);

		int databaseSizeBeforeUpdate = scopeVerificationSelectedServiceRepository.findAll().size();

		// Update the scopeVerificationSelectedService
		ScopeVerificationSelectedService updatedScopeVerificationSelectedService = scopeVerificationSelectedServiceRepository
				.findOne(scopeVerificationSelectedService.getId());
		updatedScopeVerificationSelectedService.assessedServiceName(UPDATED_ASSESSED_SERVICE_NAME)
				.assessedServiceType(UPDATED_ASSESSED_SERVICE_TYPE);

		restScopeVerificationSelectedServiceMockMvc
				.perform(put("/api/scope-verification-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedScopeVerificationSelectedService)))
				.andExpect(status().isOk());

		// Validate the ScopeVerificationSelectedService in the database
		List<ScopeVerificationSelectedService> scopeVerificationSelectedServiceList = scopeVerificationSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationSelectedServiceList).hasSize(databaseSizeBeforeUpdate);
		ScopeVerificationSelectedService testScopeVerificationSelectedService = scopeVerificationSelectedServiceList
				.get(scopeVerificationSelectedServiceList.size() - 1);
		assertThat(testScopeVerificationSelectedService.getAssessedServiceName())
				.isEqualTo(UPDATED_ASSESSED_SERVICE_NAME);
		assertThat(testScopeVerificationSelectedService.getAssessedServiceType())
				.isEqualTo(UPDATED_ASSESSED_SERVICE_TYPE);
	}

	@Test
	@Transactional
	public void updateNonExistingScopeVerificationSelectedService() throws Exception {
		int databaseSizeBeforeUpdate = scopeVerificationSelectedServiceRepository.findAll().size();

		// Create the ScopeVerificationSelectedService

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restScopeVerificationSelectedServiceMockMvc
				.perform(put("/api/scope-verification-selected-services/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(scopeVerificationSelectedService)))
				.andExpect(status().isCreated());

		// Validate the ScopeVerificationSelectedService in the database
		List<ScopeVerificationSelectedService> scopeVerificationSelectedServiceList = scopeVerificationSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationSelectedServiceList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteScopeVerificationSelectedService() throws Exception {
		// Initialize the database
		scopeVerificationSelectedServiceService.save(scopeVerificationSelectedService);

		int databaseSizeBeforeDelete = scopeVerificationSelectedServiceRepository.findAll().size();

		// Get the scopeVerificationSelectedService
		restScopeVerificationSelectedServiceMockMvc
				.perform(delete("/api/scope-verification-selected-services/service-provider/{id}",
						scopeVerificationSelectedService.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ScopeVerificationSelectedService> scopeVerificationSelectedServiceList = scopeVerificationSelectedServiceRepository
				.findAll();
		assertThat(scopeVerificationSelectedServiceList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ScopeVerificationSelectedService.class);
		ScopeVerificationSelectedService scopeVerificationSelectedService1 = new ScopeVerificationSelectedService();
		scopeVerificationSelectedService1.setId(1L);
		ScopeVerificationSelectedService scopeVerificationSelectedService2 = new ScopeVerificationSelectedService();
		scopeVerificationSelectedService2.setId(scopeVerificationSelectedService1.getId());
		assertThat(scopeVerificationSelectedService1).isEqualTo(scopeVerificationSelectedService2);
		scopeVerificationSelectedService2.setId(2L);
		assertThat(scopeVerificationSelectedService1).isNotEqualTo(scopeVerificationSelectedService2);
		scopeVerificationSelectedService1.setId(null);
		assertThat(scopeVerificationSelectedService1).isNotEqualTo(scopeVerificationSelectedService2);
	}
}
