package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.SelectedServiceType;
import com.nollysoft.domain.ServiceType;
import com.nollysoft.repository.SelectedServiceTypeRepository;
import com.nollysoft.service.SelectedServiceTypeService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SelectedServiceTypeResource REST controller.
 *
 * @see SelectedServiceTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class SelectedServiceTypeResourceIntTest {

	@Autowired
	private SelectedServiceTypeRepository selectedServiceTypeRepository;

	@Autowired
	private SelectedServiceTypeService selectedServiceTypeService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restSelectedServiceTypeMockMvc;

	private SelectedServiceType selectedServiceType;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		SelectedServiceTypeResource selectedServiceTypeResource = new SelectedServiceTypeResource(
				selectedServiceTypeService);
		this.restSelectedServiceTypeMockMvc = MockMvcBuilders.standaloneSetup(selectedServiceTypeResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static SelectedServiceType createEntity(EntityManager em) {
		SelectedServiceType selectedServiceType = new SelectedServiceType();
		// Add required entity
		ServiceType serviceTypeId = ServiceTypeResourceIntTest.createEntity(em);
		em.persist(serviceTypeId);
		em.flush();
		selectedServiceType.setServiceTypeId(serviceTypeId);
		return selectedServiceType;
	}

	@Before
	public void initTest() {
		selectedServiceType = createEntity(em);
	}

	@Test
	@Transactional
	public void createSelectedServiceType() throws Exception {
		int databaseSizeBeforeCreate = selectedServiceTypeRepository.findAll().size();

		// Create the SelectedServiceType
		restSelectedServiceTypeMockMvc
				.perform(
						post("/api/selected-service-types/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(selectedServiceType)))
				.andExpect(status().isCreated());

		// Validate the SelectedServiceType in the database
		List<SelectedServiceType> selectedServiceTypeList = selectedServiceTypeRepository.findAll();
		assertThat(selectedServiceTypeList).hasSize(databaseSizeBeforeCreate + 1);
	}

	@Test
	@Transactional
	public void createSelectedServiceTypeWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = selectedServiceTypeRepository.findAll().size();

		// Create the SelectedServiceType with an existing ID
		selectedServiceType.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restSelectedServiceTypeMockMvc
				.perform(
						post("/api/selected-service-types/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
								.content(TestUtil.convertObjectToJsonBytes(selectedServiceType)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<SelectedServiceType> selectedServiceTypeList = selectedServiceTypeRepository.findAll();
		assertThat(selectedServiceTypeList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllSelectedServiceTypes() throws Exception {
		// Initialize the database
		selectedServiceTypeRepository.saveAndFlush(selectedServiceType);

		// Get all the selectedServiceTypeList
		restSelectedServiceTypeMockMvc.perform(get("/api/selected-service-types/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(selectedServiceType.getId().intValue())));
	}

	@Test
	@Transactional
	public void getSelectedServiceType() throws Exception {
		// Initialize the database
		selectedServiceTypeRepository.saveAndFlush(selectedServiceType);

		// Get the selectedServiceType
		restSelectedServiceTypeMockMvc
				.perform(get("/api/selected-service-types/service-provider/{id}", selectedServiceType.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(selectedServiceType.getId().intValue()));
	}

	@Test
	@Transactional
	public void getNonExistingSelectedServiceType() throws Exception {
		// Get the selectedServiceType
		restSelectedServiceTypeMockMvc.perform(get("/api/selected-service-types/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateSelectedServiceType() throws Exception {
		// Initialize the database
		selectedServiceTypeService.save(selectedServiceType);

		int databaseSizeBeforeUpdate = selectedServiceTypeRepository.findAll().size();

		// Update the selectedServiceType
		SelectedServiceType updatedSelectedServiceType = selectedServiceTypeRepository
				.findOne(selectedServiceType.getId());

		restSelectedServiceTypeMockMvc
				.perform(put("/api/selected-service-types/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedSelectedServiceType)))
				.andExpect(status().isOk());

		// Validate the SelectedServiceType in the database
		List<SelectedServiceType> selectedServiceTypeList = selectedServiceTypeRepository.findAll();
		assertThat(selectedServiceTypeList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	public void updateNonExistingSelectedServiceType() throws Exception {
		int databaseSizeBeforeUpdate = selectedServiceTypeRepository.findAll().size();

		// Create the SelectedServiceType

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restSelectedServiceTypeMockMvc
				.perform(put("/api/selected-service-types/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(selectedServiceType)))
				.andExpect(status().isCreated());

		// Validate the SelectedServiceType in the database
		List<SelectedServiceType> selectedServiceTypeList = selectedServiceTypeRepository.findAll();
		assertThat(selectedServiceTypeList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteSelectedServiceType() throws Exception {
		// Initialize the database
		selectedServiceTypeService.save(selectedServiceType);

		int databaseSizeBeforeDelete = selectedServiceTypeRepository.findAll().size();

		// Get the selectedServiceType
		restSelectedServiceTypeMockMvc
				.perform(delete("/api/selected-service-types/service-provider/{id}", selectedServiceType.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<SelectedServiceType> selectedServiceTypeList = selectedServiceTypeRepository.findAll();
		assertThat(selectedServiceTypeList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(SelectedServiceType.class);
		SelectedServiceType selectedServiceType1 = new SelectedServiceType();
		selectedServiceType1.setId(1L);
		SelectedServiceType selectedServiceType2 = new SelectedServiceType();
		selectedServiceType2.setId(selectedServiceType1.getId());
		assertThat(selectedServiceType1).isEqualTo(selectedServiceType2);
		selectedServiceType2.setId(2L);
		assertThat(selectedServiceType1).isNotEqualTo(selectedServiceType2);
		selectedServiceType1.setId(null);
		assertThat(selectedServiceType1).isNotEqualTo(selectedServiceType2);
	}
}
