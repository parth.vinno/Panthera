package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ServiceByServiceProvider;
import com.nollysoft.domain.ThirdPartyServiceProvider;
import com.nollysoft.repository.ServiceByServiceProviderRepository;
import com.nollysoft.service.ServiceByServiceProviderService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ServiceByServiceProviderResource REST controller.
 *
 * @see ServiceByServiceProviderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ServiceByServiceProviderResourceIntTest {

	private static final String DEFAULT_PROVIDER_NAME = "AAAAAAAAAA";

	private static final String DEFAULT_DESCRIPTION_OF_SERVICE_PROVIDER = "AAAAAAAAAA";

	@Autowired
	private ServiceByServiceProviderRepository serviceByServiceProviderRepository;

	@Autowired
	private ServiceByServiceProviderService serviceByServiceProviderService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restServiceByServiceProviderMockMvc;

	private ServiceByServiceProvider serviceByServiceProvider;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ServiceByServiceProviderResource serviceByServiceProviderResource = new ServiceByServiceProviderResource(
				serviceByServiceProviderService);
		this.restServiceByServiceProviderMockMvc = MockMvcBuilders.standaloneSetup(serviceByServiceProviderResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ServiceByServiceProvider createEntity(EntityManager em) {
		ServiceByServiceProvider serviceByServiceProvider = new ServiceByServiceProvider()
				.providerName(DEFAULT_PROVIDER_NAME)
				.descriptionOfServiceProvider(DEFAULT_DESCRIPTION_OF_SERVICE_PROVIDER);
		// Add required entity
		ThirdPartyServiceProvider thirdPartyServiceProviderId = ThirdPartyServiceProviderResourceIntTest
				.createEntity(em);
		em.persist(thirdPartyServiceProviderId);
		em.flush();
		serviceByServiceProvider.setThirdPartyServiceProviderId(thirdPartyServiceProviderId);
		return serviceByServiceProvider;
	}

	@Before
	public void initTest() {
		serviceByServiceProvider = createEntity(em);
	}

	@Test
	@Transactional
	public void checkProviderNameIsRequired() throws Exception {
		int databaseSizeBeforeTest = serviceByServiceProviderRepository.findAll().size();
		// set the field null
		serviceByServiceProvider.setProviderName(null);

		// Create the ServiceByServiceProvider, which fails.

		restServiceByServiceProviderMockMvc
				.perform(post("/api/service-by-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(serviceByServiceProvider)))
				.andExpect(status().isBadRequest());

		List<ServiceByServiceProvider> serviceByServiceProviderList = serviceByServiceProviderRepository.findAll();
		assertThat(serviceByServiceProviderList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void checkDescriptionOfServiceProviderIsRequired() throws Exception {
		int databaseSizeBeforeTest = serviceByServiceProviderRepository.findAll().size();
		// set the field null
		serviceByServiceProvider.setDescriptionOfServiceProvider(null);

		// Create the ServiceByServiceProvider, which fails.

		restServiceByServiceProviderMockMvc
				.perform(post("/api/service-by-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(serviceByServiceProvider)))
				.andExpect(status().isBadRequest());

		List<ServiceByServiceProvider> serviceByServiceProviderList = serviceByServiceProviderRepository.findAll();
		assertThat(serviceByServiceProviderList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllServiceByServiceProviders() throws Exception {
		// Initialize the database
		serviceByServiceProviderRepository.saveAndFlush(serviceByServiceProvider);

		// Get all the serviceByServiceProviderList
		restServiceByServiceProviderMockMvc.perform(get("/api/service-by-service-providers?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(serviceByServiceProvider.getId().intValue())))
				.andExpect(jsonPath("$.[*].providerName").value(hasItem(DEFAULT_PROVIDER_NAME.toString())))
				.andExpect(jsonPath("$.[*].descriptionOfServiceProvider")
						.value(hasItem(DEFAULT_DESCRIPTION_OF_SERVICE_PROVIDER.toString())));
	}

	@Test
	@Transactional
	public void getServiceByServiceProvider() throws Exception {
		// Initialize the database
		serviceByServiceProviderRepository.saveAndFlush(serviceByServiceProvider);

		// Get the serviceByServiceProvider
		restServiceByServiceProviderMockMvc
				.perform(get("/api/service-by-service-providers/{id}", serviceByServiceProvider.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(serviceByServiceProvider.getId().intValue()))
				.andExpect(jsonPath("$.providerName").value(DEFAULT_PROVIDER_NAME.toString()))
				.andExpect(jsonPath("$.descriptionOfServiceProvider")
						.value(DEFAULT_DESCRIPTION_OF_SERVICE_PROVIDER.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingServiceByServiceProvider() throws Exception {
		// Get the serviceByServiceProvider
		restServiceByServiceProviderMockMvc.perform(get("/api/service-by-service-providers/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void deleteServiceByServiceProvider() throws Exception {
		// Initialize the database
		serviceByServiceProviderService.save(serviceByServiceProvider);

		int databaseSizeBeforeDelete = serviceByServiceProviderRepository.findAll().size();

		// Get the serviceByServiceProvider
		restServiceByServiceProviderMockMvc
				.perform(delete("/api/service-by-service-providers/{id}", serviceByServiceProvider.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ServiceByServiceProvider> serviceByServiceProviderList = serviceByServiceProviderRepository.findAll();
		assertThat(serviceByServiceProviderList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ServiceByServiceProvider.class);
		ServiceByServiceProvider serviceByServiceProvider1 = new ServiceByServiceProvider();
		serviceByServiceProvider1.setId(1L);
		ServiceByServiceProvider serviceByServiceProvider2 = new ServiceByServiceProvider();
		serviceByServiceProvider2.setId(serviceByServiceProvider1.getId());
		assertThat(serviceByServiceProvider1).isEqualTo(serviceByServiceProvider2);
		serviceByServiceProvider2.setId(2L);
		assertThat(serviceByServiceProvider1).isNotEqualTo(serviceByServiceProvider2);
		serviceByServiceProvider1.setId(null);
		assertThat(serviceByServiceProvider1).isNotEqualTo(serviceByServiceProvider2);
	}
}
