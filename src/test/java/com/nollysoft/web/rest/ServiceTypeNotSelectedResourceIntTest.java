package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ServiceType;
import com.nollysoft.domain.ServiceTypeNotSelected;
import com.nollysoft.repository.ServiceTypeNotSelectedRepository;
import com.nollysoft.service.ServiceTypeNotSelectedService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ServiceTypeNotSelectedResource REST controller.
 *
 * @see ServiceTypeNotSelectedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ServiceTypeNotSelectedResourceIntTest {

	@Autowired
	private ServiceTypeNotSelectedRepository serviceTypeNotSelectedRepository;

	@Autowired
	private ServiceTypeNotSelectedService serviceTypeNotSelectedService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restServiceTypeNotSelectedMockMvc;

	private ServiceTypeNotSelected serviceTypeNotSelected;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ServiceTypeNotSelectedResource serviceTypeNotSelectedResource = new ServiceTypeNotSelectedResource(
				serviceTypeNotSelectedService);
		this.restServiceTypeNotSelectedMockMvc = MockMvcBuilders.standaloneSetup(serviceTypeNotSelectedResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ServiceTypeNotSelected createEntity(EntityManager em) {
		ServiceTypeNotSelected serviceTypeNotSelected = new ServiceTypeNotSelected();
		// Add required entity
		ServiceType serviceTypeId = ServiceTypeResourceIntTest.createEntity(em);
		em.persist(serviceTypeId);
		em.flush();
		serviceTypeNotSelected.setServiceTypeId(serviceTypeId);
		return serviceTypeNotSelected;
	}

	@Before
	public void initTest() {
		serviceTypeNotSelected = createEntity(em);
	}

	@Test
	@Transactional
	public void createServiceTypeNotSelected() throws Exception {
		int databaseSizeBeforeCreate = serviceTypeNotSelectedRepository.findAll().size();

		// Create the ServiceTypeNotSelected
		restServiceTypeNotSelectedMockMvc
				.perform(post("/api/service-type-not-selected/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(serviceTypeNotSelected)))
				.andExpect(status().isCreated());

		// Validate the ServiceTypeNotSelected in the database
		List<ServiceTypeNotSelected> serviceTypeNotSelectedList = serviceTypeNotSelectedRepository.findAll();
		assertThat(serviceTypeNotSelectedList).hasSize(databaseSizeBeforeCreate + 1);
	}

	@Test
	@Transactional
	public void createServiceTypeNotSelectedWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = serviceTypeNotSelectedRepository.findAll().size();

		// Create the ServiceTypeNotSelected with an existing ID
		serviceTypeNotSelected.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restServiceTypeNotSelectedMockMvc
				.perform(post("/api/service-type-not-selected/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(serviceTypeNotSelected)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<ServiceTypeNotSelected> serviceTypeNotSelectedList = serviceTypeNotSelectedRepository.findAll();
		assertThat(serviceTypeNotSelectedList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllServiceTypeNotSelecteds() throws Exception {
		// Initialize the database
		serviceTypeNotSelectedRepository.saveAndFlush(serviceTypeNotSelected);

		// Get all the serviceTypeNotSelectedList
		restServiceTypeNotSelectedMockMvc.perform(get("/api/service-type-not-selected/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(serviceTypeNotSelected.getId().intValue())));
	}

	@Test
	@Transactional
	public void getServiceTypeNotSelected() throws Exception {
		// Initialize the database
		serviceTypeNotSelectedRepository.saveAndFlush(serviceTypeNotSelected);

		// Get the serviceTypeNotSelected
		restServiceTypeNotSelectedMockMvc
				.perform(get("/api/service-type-not-selected/service-provider/{id}", serviceTypeNotSelected.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(serviceTypeNotSelected.getId().intValue()));
	}

	@Test
	@Transactional
	public void getNonExistingServiceTypeNotSelected() throws Exception {
		// Get the serviceTypeNotSelected
		restServiceTypeNotSelectedMockMvc
				.perform(get("/api/service-type-not-selected/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateServiceTypeNotSelected() throws Exception {
		// Initialize the database
		serviceTypeNotSelectedService.save(serviceTypeNotSelected);

		int databaseSizeBeforeUpdate = serviceTypeNotSelectedRepository.findAll().size();

		// Update the serviceTypeNotSelected
		ServiceTypeNotSelected updatedServiceTypeNotSelected = serviceTypeNotSelectedRepository
				.findOne(serviceTypeNotSelected.getId());

		restServiceTypeNotSelectedMockMvc
				.perform(put("/api/service-type-not-selected/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedServiceTypeNotSelected)))
				.andExpect(status().isOk());

		// Validate the ServiceTypeNotSelected in the database
		List<ServiceTypeNotSelected> serviceTypeNotSelectedList = serviceTypeNotSelectedRepository.findAll();
		assertThat(serviceTypeNotSelectedList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	public void updateNonExistingServiceTypeNotSelected() throws Exception {
		int databaseSizeBeforeUpdate = serviceTypeNotSelectedRepository.findAll().size();

		// Create the ServiceTypeNotSelected

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restServiceTypeNotSelectedMockMvc
				.perform(put("/api/service-type-not-selected/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(serviceTypeNotSelected)))
				.andExpect(status().isCreated());

		// Validate the ServiceTypeNotSelected in the database
		List<ServiceTypeNotSelected> serviceTypeNotSelectedList = serviceTypeNotSelectedRepository.findAll();
		assertThat(serviceTypeNotSelectedList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteServiceTypeNotSelected() throws Exception {
		// Initialize the database
		serviceTypeNotSelectedService.save(serviceTypeNotSelected);

		int databaseSizeBeforeDelete = serviceTypeNotSelectedRepository.findAll().size();

		// Get the serviceTypeNotSelected
		restServiceTypeNotSelectedMockMvc
				.perform(delete("/api/service-type-not-selected/service-provider/{id}", serviceTypeNotSelected.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ServiceTypeNotSelected> serviceTypeNotSelectedList = serviceTypeNotSelectedRepository.findAll();
		assertThat(serviceTypeNotSelectedList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ServiceTypeNotSelected.class);
		ServiceTypeNotSelected serviceTypeNotSelected1 = new ServiceTypeNotSelected();
		serviceTypeNotSelected1.setId(1L);
		ServiceTypeNotSelected serviceTypeNotSelected2 = new ServiceTypeNotSelected();
		serviceTypeNotSelected2.setId(serviceTypeNotSelected1.getId());
		assertThat(serviceTypeNotSelected1).isEqualTo(serviceTypeNotSelected2);
		serviceTypeNotSelected2.setId(2L);
		assertThat(serviceTypeNotSelected1).isNotEqualTo(serviceTypeNotSelected2);
		serviceTypeNotSelected1.setId(null);
		assertThat(serviceTypeNotSelected1).isNotEqualTo(serviceTypeNotSelected2);
	}
}
