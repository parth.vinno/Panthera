package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.ServiceCategory;
import com.nollysoft.domain.ServiceType;
import com.nollysoft.repository.ServiceTypeRepository;
import com.nollysoft.service.ServiceTypeService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ServiceTypeResource REST controller.
 *
 * @see ServiceTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ServiceTypeResourceIntTest {

	private static final String DEFAULT_SERVICE_TYPE = "AAAAAAAAAA";
	@Autowired
	private ServiceTypeRepository serviceTypeRepository;

	@Autowired
	private ServiceTypeService serviceTypeService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restServiceTypeMockMvc;

	private ServiceType serviceType;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ServiceTypeResource serviceTypeResource = new ServiceTypeResource(serviceTypeService);
		this.restServiceTypeMockMvc = MockMvcBuilders.standaloneSetup(serviceTypeResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ServiceType createEntity(EntityManager em) {
		ServiceType serviceType = new ServiceType().serviceType(DEFAULT_SERVICE_TYPE);

		// Add required entity
		ServiceCategory serviceCategoryId = ServiceCategoryResourceIntTest.createEntity(em);
		em.persist(serviceCategoryId);
		em.flush();
		serviceType.setServiceCategoryId(serviceCategoryId);
		return serviceType;
	}

	@Before
	public void initTest() {
		serviceType = createEntity(em);
	}

	@Test
	@Transactional
	public void createServiceTypeWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = serviceTypeRepository.findAll().size();

		// Create the ServiceType with an existing ID
		serviceType.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restServiceTypeMockMvc.perform(post("/api/service-types/service-provider")
				.contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(serviceType)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
		assertThat(serviceTypeList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllServiceTypes() throws Exception {
		// Initialize the database
		serviceTypeRepository.saveAndFlush(serviceType);

		// Get all the serviceTypeList
		restServiceTypeMockMvc.perform(get("/api/service-types/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(serviceType.getId().intValue())))
				.andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE.toString())));
	}

	@Test
	@Transactional
	public void getServiceType() throws Exception {
		// Initialize the database
		serviceTypeRepository.saveAndFlush(serviceType);

		// Get the serviceType
		restServiceTypeMockMvc.perform(get("/api/service-types/service-provider/{id}", serviceType.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(serviceType.getId().intValue()))
				.andExpect(jsonPath("$.serviceType").value(DEFAULT_SERVICE_TYPE.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingServiceType() throws Exception {
		// Get the serviceType
		restServiceTypeMockMvc.perform(get("/api/service-types/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void deleteServiceType() throws Exception {
		// Initialize the database
		serviceTypeService.save(serviceType);

		int databaseSizeBeforeDelete = serviceTypeRepository.findAll().size();

		// Get the serviceType
		restServiceTypeMockMvc.perform(delete("/api/service-types/service-provider/{id}", serviceType.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
		assertThat(serviceTypeList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ServiceType.class);
		ServiceType serviceType1 = new ServiceType();
		serviceType1.setId(1L);
		ServiceType serviceType2 = new ServiceType();
		serviceType2.setId(serviceType1.getId());
		assertThat(serviceType1).isEqualTo(serviceType2);
		serviceType2.setId(2L);
		assertThat(serviceType1).isNotEqualTo(serviceType2);
		serviceType1.setId(null);
		assertThat(serviceType1).isNotEqualTo(serviceType2);
	}
}
