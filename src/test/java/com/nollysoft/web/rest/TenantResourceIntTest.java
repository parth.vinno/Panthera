package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.Address;
import com.nollysoft.domain.Tenant;
import com.nollysoft.repository.TenantRepository;
import com.nollysoft.service.TenancyService;
import com.nollysoft.service.TenantService;
import com.nollysoft.service.UserService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the TenantResource REST controller.
 *
 * @see TenantResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class TenantResourceIntTest {

    private static final String DEFAULT_COMPANY_URL = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_URL = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PERSON_DESKPHONE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PERSON_DESKPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PERSON_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PERSON_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PERSON_MOBILEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PERSON_MOBILEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PERSON_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PERSON_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_SERVICE_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SERVICE_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TENANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_TENANT_ID = "BBBBBBBBBB";

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private TenancyService tenancyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTenantMockMvc;

    private Tenant tenant;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		TenantResource tenantResource = new TenantResource(tenantService, tenantRepository, userService,
				tenancyService);
		this.restTenantMockMvc = MockMvcBuilders.standaloneSetup(tenantResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tenant createEntity(EntityManager em) {
        Tenant tenant = new Tenant()
            .companyUrl(DEFAULT_COMPANY_URL)
            .contactPersonDeskphone(DEFAULT_CONTACT_PERSON_DESKPHONE)
            .contactPersonEmail(DEFAULT_CONTACT_PERSON_EMAIL)
            .contactPersonMobilephone(DEFAULT_CONTACT_PERSON_MOBILEPHONE)
            .contactPersonName(DEFAULT_CONTACT_PERSON_NAME)
            .createDate(DEFAULT_CREATE_DATE)
            .name(DEFAULT_NAME)
            .companyPhoneNumber(DEFAULT_COMPANY_PHONE_NUMBER)
            .email(DEFAULT_EMAIL)
            .serviceStartDate(DEFAULT_SERVICE_START_DATE)
            .tenantId(DEFAULT_TENANT_ID);
        Address addressId = AddressResourceIntTest.createEntity(em);
        tenant.setAddressId(addressId);
        return tenant;
    }

    @Before
    public void initTest() {
        tenant = createEntity(em);
    }

    @Test
    @Transactional
    public void createTenant() throws Exception {
        int databaseSizeBeforeCreate = tenantRepository.findAll().size();

        // Create the Tenant
        restTenantMockMvc.perform(post("/api/tenants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tenant)))
            .andExpect(status().isCreated());

        // Validate the Tenant in the database
        List<Tenant> tenantList = tenantRepository.findAll();
        assertThat(tenantList).hasSize(databaseSizeBeforeCreate + 1);
        Tenant testTenant = tenantList.get(tenantList.size() - 1);
        assertThat(testTenant.getCompanyUrl()).isEqualTo(DEFAULT_COMPANY_URL);
        assertThat(testTenant.getContactPersonDeskphone()).isEqualTo(DEFAULT_CONTACT_PERSON_DESKPHONE);
        assertThat(testTenant.getContactPersonEmail()).isEqualTo(DEFAULT_CONTACT_PERSON_EMAIL);
        assertThat(testTenant.getContactPersonMobilephone()).isEqualTo(DEFAULT_CONTACT_PERSON_MOBILEPHONE);
        assertThat(testTenant.getContactPersonName()).isEqualTo(DEFAULT_CONTACT_PERSON_NAME);
        assertThat(testTenant.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testTenant.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTenant.getCompanyPhoneNumber()).isEqualTo(DEFAULT_COMPANY_PHONE_NUMBER);
        assertThat(testTenant.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testTenant.getServiceStartDate()).isEqualTo(DEFAULT_SERVICE_START_DATE);
        assertThat(testTenant.getTenantId()).isEqualTo(DEFAULT_TENANT_ID);
    }

    @Test
    @Transactional
    public void createTenantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tenantRepository.findAll().size();

        // Create the Tenant with an existing ID
        tenant.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTenantMockMvc.perform(post("/api/tenants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tenant)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Tenant> tenantList = tenantRepository.findAll();
        assertThat(tenantList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTenants() throws Exception {
        // Initialize the database
        tenantRepository.saveAndFlush(tenant);

        // Get all the tenantList
        restTenantMockMvc.perform(get("/api/tenants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tenant.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyUrl").value(hasItem(DEFAULT_COMPANY_URL.toString())))
            .andExpect(jsonPath("$.[*].contactPersonDeskphone").value(hasItem(DEFAULT_CONTACT_PERSON_DESKPHONE.toString())))
            .andExpect(jsonPath("$.[*].contactPersonEmail").value(hasItem(DEFAULT_CONTACT_PERSON_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].contactPersonMobilephone").value(hasItem(DEFAULT_CONTACT_PERSON_MOBILEPHONE.toString())))
            .andExpect(jsonPath("$.[*].contactPersonName").value(hasItem(DEFAULT_CONTACT_PERSON_NAME.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].companyPhoneNumber").value(hasItem(DEFAULT_COMPANY_PHONE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].serviceStartDate").value(hasItem(DEFAULT_SERVICE_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].tenantId").value(hasItem(DEFAULT_TENANT_ID.toString())));
    }

    @Test
    @Transactional
    public void getTenant() throws Exception {
        // Initialize the database
        tenantRepository.saveAndFlush(tenant);

        // Get the tenant
        restTenantMockMvc.perform(get("/api/tenants/{id}", tenant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tenant.getId().intValue()))
            .andExpect(jsonPath("$.companyUrl").value(DEFAULT_COMPANY_URL.toString()))
            .andExpect(jsonPath("$.contactPersonDeskphone").value(DEFAULT_CONTACT_PERSON_DESKPHONE.toString()))
            .andExpect(jsonPath("$.contactPersonEmail").value(DEFAULT_CONTACT_PERSON_EMAIL.toString()))
            .andExpect(jsonPath("$.contactPersonMobilephone").value(DEFAULT_CONTACT_PERSON_MOBILEPHONE.toString()))
            .andExpect(jsonPath("$.contactPersonName").value(DEFAULT_CONTACT_PERSON_NAME.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.companyPhoneNumber").value(DEFAULT_COMPANY_PHONE_NUMBER.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.serviceStartDate").value(DEFAULT_SERVICE_START_DATE.toString()))
            .andExpect(jsonPath("$.tenantId").value(DEFAULT_TENANT_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTenant() throws Exception {
        // Get the tenant
        restTenantMockMvc.perform(get("/api/tenants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTenant() throws Exception {
        // Initialize the database
        tenantService.save(tenant);

        int databaseSizeBeforeUpdate = tenantRepository.findAll().size();

        // Update the tenant
        Tenant updatedTenant = tenantRepository.findOne(tenant.getId());
        updatedTenant
            .companyUrl(UPDATED_COMPANY_URL)
            .contactPersonDeskphone(UPDATED_CONTACT_PERSON_DESKPHONE)
            .contactPersonEmail(UPDATED_CONTACT_PERSON_EMAIL)
            .contactPersonMobilephone(UPDATED_CONTACT_PERSON_MOBILEPHONE)
            .contactPersonName(UPDATED_CONTACT_PERSON_NAME)
            .createDate(UPDATED_CREATE_DATE)
            .name(UPDATED_NAME)
            .companyPhoneNumber(UPDATED_COMPANY_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .serviceStartDate(UPDATED_SERVICE_START_DATE)
            .tenantId(UPDATED_TENANT_ID);

        restTenantMockMvc.perform(put("/api/tenants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTenant)))
            .andExpect(status().isOk());

        // Validate the Tenant in the database
        List<Tenant> tenantList = tenantRepository.findAll();
        assertThat(tenantList).hasSize(databaseSizeBeforeUpdate);
        Tenant testTenant = tenantList.get(tenantList.size() - 1);
        assertThat(testTenant.getCompanyUrl()).isEqualTo(UPDATED_COMPANY_URL);
        assertThat(testTenant.getContactPersonDeskphone()).isEqualTo(UPDATED_CONTACT_PERSON_DESKPHONE);
        assertThat(testTenant.getContactPersonEmail()).isEqualTo(UPDATED_CONTACT_PERSON_EMAIL);
        assertThat(testTenant.getContactPersonMobilephone()).isEqualTo(UPDATED_CONTACT_PERSON_MOBILEPHONE);
        assertThat(testTenant.getContactPersonName()).isEqualTo(UPDATED_CONTACT_PERSON_NAME);
        assertThat(testTenant.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testTenant.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTenant.getCompanyPhoneNumber()).isEqualTo(UPDATED_COMPANY_PHONE_NUMBER);
        assertThat(testTenant.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testTenant.getServiceStartDate()).isEqualTo(UPDATED_SERVICE_START_DATE);
        assertThat(testTenant.getTenantId()).isEqualTo(UPDATED_TENANT_ID);
    }

    @Test
    @Transactional
    public void deleteTenant() throws Exception {
        // Initialize the database
        tenantService.save(tenant);

        int databaseSizeBeforeDelete = tenantRepository.findAll().size();

        // Get the tenant
        restTenantMockMvc.perform(delete("/api/tenants/{id}", tenant.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Tenant> tenantList = tenantRepository.findAll();
        assertThat(tenantList).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tenant.class);
        Tenant tenant1 = new Tenant();
        tenant1.setId(1L);
        Tenant tenant2 = new Tenant();
        tenant2.setId(tenant1.getId());
        assertThat(tenant1).isEqualTo(tenant2);
        tenant2.setId(2L);
        assertThat(tenant1).isNotEqualTo(tenant2);
        tenant1.setId(null);
        assertThat(tenant1).isNotEqualTo(tenant2);
    }
}
