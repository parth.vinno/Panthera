package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.TestedRequirementHeader;
import com.nollysoft.repository.TestedRequirementHeaderRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.TestedRequirementHeaderService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the TestedRequirementHeaderResource REST controller.
 *
 * @see TestedRequirementHeaderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class TestedRequirementHeaderResourceIntTest {

	private static final String DEFAULT_NAME = "AAAAAAAAAA";
	private static final String UPDATED_NAME = "BBBBBBBBBB";

	@Autowired
	private TestedRequirementHeaderRepository testedRequirementHeaderRepository;

	@Autowired
	private TestedRequirementHeaderService testedRequirementHeaderService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restTestedRequirementHeaderMockMvc;

	private TestedRequirementHeader testedRequirementHeader;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		TestedRequirementHeaderResource testedRequirementHeaderResource = new TestedRequirementHeaderResource(
				testedRequirementHeaderService, executiveSummaryStatusService);
		this.restTestedRequirementHeaderMockMvc = MockMvcBuilders.standaloneSetup(testedRequirementHeaderResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static TestedRequirementHeader createEntity(EntityManager em) {
		TestedRequirementHeader testedRequirementHeader = new TestedRequirementHeader()
				.serviceAssessedName(DEFAULT_NAME);
		return testedRequirementHeader;
	}

	@Before
	public void initTest() {
		testedRequirementHeader = createEntity(em);
	}

	@Test
	@Transactional
	public void createTestedRequirementHeader() throws Exception {
		int databaseSizeBeforeCreate = testedRequirementHeaderRepository.findAll().size();

		// Create the TestedRequirementHeader
		restTestedRequirementHeaderMockMvc
				.perform(post("/api/tested-requirement-header/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(testedRequirementHeader)))
				.andExpect(status().isCreated());

		// Validate the TestedRequirementHeader in the database
		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeCreate + 1);
		TestedRequirementHeader testTestedRequirementHeader = testedRequirementHeaderList
				.get(testedRequirementHeaderList.size() - 1);
		assertThat(testTestedRequirementHeader.getServiceAssessedName()).isEqualTo(DEFAULT_NAME);
	}

	@Test
	@Transactional
	public void createTestedRequirementHeaderWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = testedRequirementHeaderRepository.findAll().size();

		// Create the TestedRequirementHeader with an existing ID
		testedRequirementHeader.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restTestedRequirementHeaderMockMvc
				.perform(post("/api/tested-requirement-header/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(testedRequirementHeader)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkNameIsRequired() throws Exception {
		int databaseSizeBeforeTest = testedRequirementHeaderRepository.findAll().size();
		// set the field null
		testedRequirementHeader.setServiceAssessedName(null);

		// Create the TestedRequirementHeader, which fails.

		restTestedRequirementHeaderMockMvc
				.perform(post("/api/tested-requirement-header/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(testedRequirementHeader)))
				.andExpect(status().isBadRequest());

		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllTestedRequirementHeaders() throws Exception {
		// Initialize the database
		testedRequirementHeaderRepository.saveAndFlush(testedRequirementHeader);

		// Get all the testedRequirementHeaderList
		restTestedRequirementHeaderMockMvc.perform(get("/api/tested-requirement-header/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(testedRequirementHeader.getId().intValue())))
				.andExpect(jsonPath("$.[*].serviceAssessedName").value(hasItem(DEFAULT_NAME.toString())));
	}

	@Test
	@Transactional
	public void getTestedRequirementHeader() throws Exception {
		// Initialize the database
		testedRequirementHeaderRepository.saveAndFlush(testedRequirementHeader);

		// Get the testedRequirementHeader
		restTestedRequirementHeaderMockMvc
				.perform(get("/api/tested-requirement-header/service-provider/{id}", testedRequirementHeader.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(testedRequirementHeader.getId().intValue()))
				.andExpect(jsonPath("$.serviceAssessedName").value(DEFAULT_NAME.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingTestedRequirementHeader() throws Exception {
		// Get the testedRequirementHeader
		restTestedRequirementHeaderMockMvc
				.perform(get("/api/tested-requirement-header/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateTestedRequirementHeader() throws Exception {
		// Initialize the database
		testedRequirementHeaderService.save(testedRequirementHeader);

		int databaseSizeBeforeUpdate = testedRequirementHeaderRepository.findAll().size();

		// Update the testedRequirementHeader
		TestedRequirementHeader updatedTestedRequirementHeader = testedRequirementHeaderRepository
				.findOne(testedRequirementHeader.getId());
		updatedTestedRequirementHeader.serviceAssessedName(UPDATED_NAME);

		restTestedRequirementHeaderMockMvc
				.perform(put("/api/tested-requirement-header/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedTestedRequirementHeader)))
				.andExpect(status().isOk());

		// Validate the TestedRequirementHeader in the database
		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeUpdate);
		TestedRequirementHeader testTestedRequirementHeader = testedRequirementHeaderList
				.get(testedRequirementHeaderList.size() - 1);
		assertThat(testTestedRequirementHeader.getServiceAssessedName()).isEqualTo(UPDATED_NAME);
	}

	@Test
	@Transactional
	public void updateNonExistingTestedRequirementHeader() throws Exception {
		int databaseSizeBeforeUpdate = testedRequirementHeaderRepository.findAll().size();

		// Create the TestedRequirementHeader

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restTestedRequirementHeaderMockMvc
				.perform(put("/api/tested-requirement-header/service-provider")
						.contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(testedRequirementHeader)))
				.andExpect(status().isCreated());

		// Validate the TestedRequirementHeader in the database
		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteTestedRequirementHeader() throws Exception {
		// Initialize the database
		testedRequirementHeaderService.save(testedRequirementHeader);

		int databaseSizeBeforeDelete = testedRequirementHeaderRepository.findAll().size();

		// Get the testedRequirementHeader
		restTestedRequirementHeaderMockMvc
				.perform(delete("/api/tested-requirement-header/service-provider/{id}", testedRequirementHeader.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<TestedRequirementHeader> testedRequirementHeaderList = testedRequirementHeaderRepository.findAll();
		assertThat(testedRequirementHeaderList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(TestedRequirementHeader.class);
		TestedRequirementHeader testedRequirementHeader1 = new TestedRequirementHeader();
		testedRequirementHeader1.setId(1L);
		TestedRequirementHeader testedRequirementHeader2 = new TestedRequirementHeader();
		testedRequirementHeader2.setId(testedRequirementHeader1.getId());
		assertThat(testedRequirementHeader1).isEqualTo(testedRequirementHeader2);
		testedRequirementHeader2.setId(2L);
		assertThat(testedRequirementHeader1).isNotEqualTo(testedRequirementHeader2);
		testedRequirementHeader1.setId(null);
		assertThat(testedRequirementHeader1).isNotEqualTo(testedRequirementHeader2);
	}
}
