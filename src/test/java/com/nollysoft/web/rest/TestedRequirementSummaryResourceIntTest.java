package com.nollysoft.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.nollysoft.PcidssApp;
import com.nollysoft.domain.PcidssRequirement;
import com.nollysoft.domain.TestedRequirementHeader;
import com.nollysoft.domain.TestedRequirementSummary;
import com.nollysoft.domain.TestedRequirementSummary.ASSESSED;
import com.nollysoft.repository.TestedRequirementSummaryRepository;
import com.nollysoft.service.TestedRequirementSummaryService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the TestedRequirementSummaryResource REST controller.
 *
 * @see TestedRequirementSummaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class TestedRequirementSummaryResourceIntTest {

	private static final ASSESSED DEFAULT_ASSESSED = ASSESSED.FULL;

	private static final String DEFAULT_JUSTIFICATION = "AAAAAAAAAA";

	@Autowired
	private TestedRequirementSummaryRepository testedRequirementSummaryRepository;

	@Autowired
	private TestedRequirementSummaryService testedRequirementSummaryService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restTestedRequirementSummaryMockMvc;

	private TestedRequirementSummary testedRequirementSummary;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		TestedRequirementSummaryResource testedRequirementSummaryResource = new TestedRequirementSummaryResource(
				testedRequirementSummaryService);
		this.restTestedRequirementSummaryMockMvc = MockMvcBuilders.standaloneSetup(testedRequirementSummaryResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static TestedRequirementSummary createEntity(EntityManager em) {
		TestedRequirementSummary testedRequirementSummary = new TestedRequirementSummary().assessed(DEFAULT_ASSESSED)
				.justification(DEFAULT_JUSTIFICATION);
		// Add required entities
		PcidssRequirement pcidssRequirement = PcidssRequirementResourceIntTest.createEntity(em);
		em.persist(pcidssRequirement);
		em.flush();
		testedRequirementSummary.setPcidssRequirementId(pcidssRequirement);

		TestedRequirementHeader testedRequirementHeader = TestedRequirementHeaderResourceIntTest.createEntity(em);
		em.persist(testedRequirementHeader);
		em.flush();
		testedRequirementSummary.setTestedRequirementHeaderId(testedRequirementHeader);
		return testedRequirementSummary;
	}

	@Before
	public void initTest() {
		testedRequirementSummary = createEntity(em);
	}

	@Test
	@Transactional
	public void createTestedRequirementSummaryWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = testedRequirementSummaryRepository.findAll().size();

		// Create the TestedRequirementSummary with an existing ID
		testedRequirementSummary.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restTestedRequirementSummaryMockMvc
				.perform(post("/api/tested-requirement-summaries/service-provider").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(testedRequirementSummary)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<TestedRequirementSummary> testedRequirementSummaryList = testedRequirementSummaryRepository.findAll();
		assertThat(testedRequirementSummaryList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllTestedRequirementSummaries() throws Exception {
		// Initialize the database
		testedRequirementSummaryRepository.saveAndFlush(testedRequirementSummary);

		// Get all the testedRequirementSummaryList
		restTestedRequirementSummaryMockMvc.perform(get("/api/tested-requirement-summaries/service-provider?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(testedRequirementSummary.getId().intValue())))
				.andExpect(jsonPath("$.[*].assessed").value(hasItem(DEFAULT_ASSESSED.toString())))
				.andExpect(jsonPath("$.[*].justification").value(hasItem(DEFAULT_JUSTIFICATION.toString())));
	}

	@Test
	@Transactional
	public void getTestedRequirementSummary() throws Exception {
		// Initialize the database
		testedRequirementSummaryRepository.saveAndFlush(testedRequirementSummary);

		// Get the testedRequirementSummary
		restTestedRequirementSummaryMockMvc
				.perform(get("/api/tested-requirement-summaries/service-provider/{id}", testedRequirementSummary.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(testedRequirementSummary.getId().intValue()))
				.andExpect(jsonPath("$.assessed").value(DEFAULT_ASSESSED.toString()))
				.andExpect(jsonPath("$.justification").value(DEFAULT_JUSTIFICATION.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingTestedRequirementSummary() throws Exception {
		// Get the testedRequirementSummary
		restTestedRequirementSummaryMockMvc.perform(get("/api/tested-requirement-summaries/service-provider/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void deleteTestedRequirementSummary() throws Exception {
		// Initialize the database
		testedRequirementSummaryService.save(testedRequirementSummary);

		int databaseSizeBeforeDelete = testedRequirementSummaryRepository.findAll().size();

		// Get the testedRequirementSummary
		restTestedRequirementSummaryMockMvc
				.perform(delete("/api/tested-requirement-summaries/service-provider/{id}", testedRequirementSummary.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<TestedRequirementSummary> testedRequirementSummaryList = testedRequirementSummaryRepository.findAll();
		assertThat(testedRequirementSummaryList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(TestedRequirementSummary.class);
		TestedRequirementSummary testedRequirementSummary1 = new TestedRequirementSummary();
		testedRequirementSummary1.setId(1L);
		TestedRequirementSummary testedRequirementSummary2 = new TestedRequirementSummary();
		testedRequirementSummary2.setId(testedRequirementSummary1.getId());
		assertThat(testedRequirementSummary1).isEqualTo(testedRequirementSummary2);
		testedRequirementSummary2.setId(2L);
		assertThat(testedRequirementSummary1).isNotEqualTo(testedRequirementSummary2);
		testedRequirementSummary1.setId(null);
		assertThat(testedRequirementSummary1).isNotEqualTo(testedRequirementSummary2);
	}
}
