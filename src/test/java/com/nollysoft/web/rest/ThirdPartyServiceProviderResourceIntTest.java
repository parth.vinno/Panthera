package com.nollysoft.web.rest;

import com.nollysoft.PcidssApp;

import com.nollysoft.domain.ThirdPartyServiceProvider;
import com.nollysoft.repository.ThirdPartyServiceProviderRepository;
import com.nollysoft.service.ExecutiveSummaryStatusService;
import com.nollysoft.service.ThirdPartyServiceProviderService;
import com.nollysoft.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ThirdPartyServiceProviderResource REST controller.
 *
 * @see ThirdPartyServiceProviderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PcidssApp.class)
public class ThirdPartyServiceProviderResourceIntTest {

	private static final Boolean DEFAULT_IS_QIR_RELATIONSHIP_PRESENT = false;
	private static final Boolean UPDATED_IS_QIR_RELATIONSHIP_PRESENT = true;

	private static final String DEFAULT_QIR_COMPANY_NAME = "AAAAAAAAAA";
	private static final String UPDATED_QIR_COMPANY_NAME = "BBBBBBBBBB";

	private static final String DEFAULT_QIR_INDIVIDUAL_NAME = "AAAAAAAAAA";
	private static final String UPDATED_QIR_INDIVIDUAL_NAME = "BBBBBBBBBB";

	private static final String DEFAULT_SERVICE_DESCRIPTION = "AAAAAAAAAA";
	private static final String UPDATED_SERVICE_DESCRIPTION = "BBBBBBBBBB";

	private static final Boolean DEFAULT_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT = false;
	private static final Boolean UPDATED_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT = true;

	@Autowired
	private ThirdPartyServiceProviderRepository thirdPartyServiceProviderRepository;

	@Autowired
	private ThirdPartyServiceProviderService thirdPartyServiceProviderService;

	@Autowired
	private ExecutiveSummaryStatusService executiveSummaryStatusService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restThirdPartyServiceProviderMockMvc;

	private ThirdPartyServiceProvider thirdPartyServiceProvider;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ThirdPartyServiceProviderResource thirdPartyServiceProviderResource = new ThirdPartyServiceProviderResource(
				thirdPartyServiceProviderService, executiveSummaryStatusService);
		this.restThirdPartyServiceProviderMockMvc = MockMvcBuilders.standaloneSetup(thirdPartyServiceProviderResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static ThirdPartyServiceProvider createEntity(EntityManager em) {
		ThirdPartyServiceProvider thirdPartyServiceProvider = new ThirdPartyServiceProvider()
				.qirRelationshipPresent(DEFAULT_IS_QIR_RELATIONSHIP_PRESENT).qirCompanyName(DEFAULT_QIR_COMPANY_NAME)
				.qirIndividualName(DEFAULT_QIR_INDIVIDUAL_NAME).serviceDescription(DEFAULT_SERVICE_DESCRIPTION)
				.serviceProviderRelationShipPresent(DEFAULT_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT);
		return thirdPartyServiceProvider;
	}

	@Before
	public void initTest() {
		thirdPartyServiceProvider = createEntity(em);
	}

	@Test
	@Transactional
	public void createThirdPartyServiceProvider() throws Exception {
		int databaseSizeBeforeCreate = thirdPartyServiceProviderRepository.findAll().size();

		// Create the ThirdPartyServiceProvider
		restThirdPartyServiceProviderMockMvc
				.perform(post("/api/third-party-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(thirdPartyServiceProvider)))
				.andExpect(status().isCreated());

		// Validate the ThirdPartyServiceProvider in the database
		List<ThirdPartyServiceProvider> thirdPartyServiceProviderList = thirdPartyServiceProviderRepository.findAll();
		assertThat(thirdPartyServiceProviderList).hasSize(databaseSizeBeforeCreate + 1);
		ThirdPartyServiceProvider testThirdPartyServiceProvider = thirdPartyServiceProviderList
				.get(thirdPartyServiceProviderList.size() - 1);
		assertThat(testThirdPartyServiceProvider.isQirRelationshipPresent())
				.isEqualTo(DEFAULT_IS_QIR_RELATIONSHIP_PRESENT);
		assertThat(testThirdPartyServiceProvider.getQirCompanyName()).isEqualTo(DEFAULT_QIR_COMPANY_NAME);
		assertThat(testThirdPartyServiceProvider.getQirIndividualName()).isEqualTo(DEFAULT_QIR_INDIVIDUAL_NAME);
		assertThat(testThirdPartyServiceProvider.getServiceDescription()).isEqualTo(DEFAULT_SERVICE_DESCRIPTION);
		assertThat(testThirdPartyServiceProvider.isServiceProviderRelationShipPresent())
				.isEqualTo(DEFAULT_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT);
	}

	@Test
	@Transactional
	public void createThirdPartyServiceProviderWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = thirdPartyServiceProviderRepository.findAll().size();

		// Create the ThirdPartyServiceProvider with an existing ID
		thirdPartyServiceProvider.setId(1L);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restThirdPartyServiceProviderMockMvc
				.perform(post("/api/third-party-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(thirdPartyServiceProvider)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<ThirdPartyServiceProvider> thirdPartyServiceProviderList = thirdPartyServiceProviderRepository.findAll();
		assertThat(thirdPartyServiceProviderList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllThirdPartyServiceProviders() throws Exception {
		// Initialize the database
		thirdPartyServiceProviderRepository.saveAndFlush(thirdPartyServiceProvider);

		// Get all the thirdPartyServiceProviderList
		restThirdPartyServiceProviderMockMvc.perform(get("/api/third-party-service-providers?sort=id,desc"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(thirdPartyServiceProvider.getId().intValue())))
				.andExpect(jsonPath("$.[*].qirRelationshipPresent")
						.value(hasItem(DEFAULT_IS_QIR_RELATIONSHIP_PRESENT.booleanValue())))
				.andExpect(jsonPath("$.[*].qirCompanyName").value(hasItem(DEFAULT_QIR_COMPANY_NAME.toString())))
				.andExpect(jsonPath("$.[*].qirIndividualName").value(hasItem(DEFAULT_QIR_INDIVIDUAL_NAME.toString())))
				.andExpect(jsonPath("$.[*].serviceDescription").value(hasItem(DEFAULT_SERVICE_DESCRIPTION.toString())))
				.andExpect(jsonPath("$.[*].serviceProviderRelationShipPresent")
						.value(hasItem(DEFAULT_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT.booleanValue())));
	}

	@Test
	@Transactional
	public void getThirdPartyServiceProvider() throws Exception {
		// Initialize the database
		thirdPartyServiceProviderRepository.saveAndFlush(thirdPartyServiceProvider);

		// Get the thirdPartyServiceProvider
		restThirdPartyServiceProviderMockMvc
				.perform(get("/api/third-party-service-providers/{id}", thirdPartyServiceProvider.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(thirdPartyServiceProvider.getId().intValue()))
				.andExpect(
						jsonPath("$.qirRelationshipPresent").value(DEFAULT_IS_QIR_RELATIONSHIP_PRESENT.booleanValue()))
				.andExpect(jsonPath("$.qirCompanyName").value(DEFAULT_QIR_COMPANY_NAME.toString()))
				.andExpect(jsonPath("$.qirIndividualName").value(DEFAULT_QIR_INDIVIDUAL_NAME.toString()))
				.andExpect(jsonPath("$.serviceDescription").value(DEFAULT_SERVICE_DESCRIPTION.toString()))
				.andExpect(jsonPath("$.serviceProviderRelationShipPresent")
						.value(DEFAULT_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT.booleanValue()));
	}

	@Test
	@Transactional
	public void getNonExistingThirdPartyServiceProvider() throws Exception {
		// Get the thirdPartyServiceProvider
		restThirdPartyServiceProviderMockMvc.perform(get("/api/third-party-service-providers/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateThirdPartyServiceProvider() throws Exception {
		// Initialize the database
		thirdPartyServiceProviderService.save(thirdPartyServiceProvider);

		int databaseSizeBeforeUpdate = thirdPartyServiceProviderRepository.findAll().size();

		// Update the thirdPartyServiceProvider
		ThirdPartyServiceProvider updatedThirdPartyServiceProvider = thirdPartyServiceProviderRepository
				.findOne(thirdPartyServiceProvider.getId());
		updatedThirdPartyServiceProvider.qirRelationshipPresent(UPDATED_IS_QIR_RELATIONSHIP_PRESENT)
				.qirCompanyName(UPDATED_QIR_COMPANY_NAME).qirIndividualName(UPDATED_QIR_INDIVIDUAL_NAME)
				.serviceDescription(UPDATED_SERVICE_DESCRIPTION)
				.serviceProviderRelationShipPresent(UPDATED_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT);

		restThirdPartyServiceProviderMockMvc
				.perform(put("/api/third-party-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(updatedThirdPartyServiceProvider)))
				.andExpect(status().isOk());

		// Validate the ThirdPartyServiceProvider in the database
		List<ThirdPartyServiceProvider> thirdPartyServiceProviderList = thirdPartyServiceProviderRepository.findAll();
		assertThat(thirdPartyServiceProviderList).hasSize(databaseSizeBeforeUpdate);
		ThirdPartyServiceProvider testThirdPartyServiceProvider = thirdPartyServiceProviderList
				.get(thirdPartyServiceProviderList.size() - 1);
		assertThat(testThirdPartyServiceProvider.isQirRelationshipPresent())
				.isEqualTo(UPDATED_IS_QIR_RELATIONSHIP_PRESENT);
		assertThat(testThirdPartyServiceProvider.getQirCompanyName()).isEqualTo(UPDATED_QIR_COMPANY_NAME);
		assertThat(testThirdPartyServiceProvider.getQirIndividualName()).isEqualTo(UPDATED_QIR_INDIVIDUAL_NAME);
		assertThat(testThirdPartyServiceProvider.getServiceDescription()).isEqualTo(UPDATED_SERVICE_DESCRIPTION);
		assertThat(testThirdPartyServiceProvider.isServiceProviderRelationShipPresent())
				.isEqualTo(UPDATED_IS_SERVICE_PROVIDER_RELATION_SHIP_PRESENT);
	}

	@Test
	@Transactional
	public void updateNonExistingThirdPartyServiceProvider() throws Exception {
		int databaseSizeBeforeUpdate = thirdPartyServiceProviderRepository.findAll().size();

		// Create the ThirdPartyServiceProvider

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restThirdPartyServiceProviderMockMvc
				.perform(put("/api/third-party-service-providers").contentType(TestUtil.APPLICATION_JSON_UTF8)
						.content(TestUtil.convertObjectToJsonBytes(thirdPartyServiceProvider)))
				.andExpect(status().isCreated());

		// Validate the ThirdPartyServiceProvider in the database
		List<ThirdPartyServiceProvider> thirdPartyServiceProviderList = thirdPartyServiceProviderRepository.findAll();
		assertThat(thirdPartyServiceProviderList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteThirdPartyServiceProvider() throws Exception {
		// Initialize the database
		thirdPartyServiceProviderService.save(thirdPartyServiceProvider);

		int databaseSizeBeforeDelete = thirdPartyServiceProviderRepository.findAll().size();

		// Get the thirdPartyServiceProvider
		restThirdPartyServiceProviderMockMvc
				.perform(delete("/api/third-party-service-providers/{id}", thirdPartyServiceProvider.getId())
						.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<ThirdPartyServiceProvider> thirdPartyServiceProviderList = thirdPartyServiceProviderRepository.findAll();
		assertThat(thirdPartyServiceProviderList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(ThirdPartyServiceProvider.class);
		ThirdPartyServiceProvider thirdPartyServiceProvider1 = new ThirdPartyServiceProvider();
		thirdPartyServiceProvider1.setId(1L);
		ThirdPartyServiceProvider thirdPartyServiceProvider2 = new ThirdPartyServiceProvider();
		thirdPartyServiceProvider2.setId(thirdPartyServiceProvider1.getId());
		assertThat(thirdPartyServiceProvider1).isEqualTo(thirdPartyServiceProvider2);
		thirdPartyServiceProvider2.setId(2L);
		assertThat(thirdPartyServiceProvider1).isNotEqualTo(thirdPartyServiceProvider2);
		thirdPartyServiceProvider1.setId(null);
		assertThat(thirdPartyServiceProvider1).isNotEqualTo(thirdPartyServiceProvider2);
	}
}
