/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AttestationComponent } from '../../../../../../main/webapp/app/entities/attestation/';

describe('Component Tests', () => {

    describe('Attestation Management Detail Component', () => {
        let comp: AttestationComponent;
        let fixture: ComponentFixture<AttestationComponent>;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [AttestationComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JhiEventManager
                ]
            }).overrideTemplate(AttestationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttestationComponent);
            comp = fixture.componentInstance;
        });

        describe('Attestation', () => {
            it('should create', () => {
                expect(comp).toBeTruthy();
            });
        });
    });

});
