/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CompensatingControlWorksheetDetailComponent } from '../../../../../../main/webapp/app/entities/compensating-control-worksheet/compensating-control-worksheet-detail.component';
import { CompensatingControlWorksheetService } from '../../../../../../main/webapp/app/entities/compensating-control-worksheet/compensating-control-worksheet.service';
import { CompensatingControlWorksheet } from '../../../../../../main/webapp/app/entities/compensating-control-worksheet/compensating-control-worksheet.model';

describe('Component Tests', () => {

    describe('CompensatingControlWorksheet Management Detail Component', () => {
        let comp: CompensatingControlWorksheetDetailComponent;
        let fixture: ComponentFixture<CompensatingControlWorksheetDetailComponent>;
        let service: CompensatingControlWorksheetService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [CompensatingControlWorksheetDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CompensatingControlWorksheetService,
                    JhiEventManager
                ]
            }).overrideTemplate(CompensatingControlWorksheetDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CompensatingControlWorksheetDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CompensatingControlWorksheetService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CompensatingControlWorksheet(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.compensatingControlWorksheet).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
