/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PrivilegeDetailComponent } from '../../../../../../main/webapp/app/admin/privilege/privilege-detail.component';
import { PrivilegeService } from '../../../../../../main/webapp/app/admin/privilege/privilege.service';
import { Privilege } from '../../../../../../main/webapp/app/admin/privilege/privilege.model';
describe('Component Tests', () => {

    describe('Privilege Management Detail Component', () => {
        let comp: PrivilegeDetailComponent;
        let fixture: ComponentFixture<PrivilegeDetailComponent>;
        let service: PrivilegeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [PrivilegeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PrivilegeService,
                    JhiEventManager
                ]
            }).overrideTemplate(PrivilegeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PrivilegeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PrivilegeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Privilege(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.privilege).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
