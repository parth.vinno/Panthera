/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RequirementWorkStatusDetailComponent } from '../../../../../../main/webapp/app/entities/requirement-work-status/requirement-work-status-detail.component';
import { RequirementWorkStatusService } from '../../../../../../main/webapp/app/entities/requirement-work-status/requirement-work-status.service';
import { RequirementWorkStatus } from '../../../../../../main/webapp/app/entities/requirement-work-status/requirement-work-status.model';

describe('Component Tests', () => {

    describe('RequirementWorkStatus Management Detail Component', () => {
        let comp: RequirementWorkStatusDetailComponent;
        let fixture: ComponentFixture<RequirementWorkStatusDetailComponent>;
        let service: RequirementWorkStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [RequirementWorkStatusDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RequirementWorkStatusService,
                    JhiEventManager
                ]
            }).overrideTemplate(RequirementWorkStatusDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RequirementWorkStatusDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RequirementWorkStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RequirementWorkStatus(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.requirementWorkStatus).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
