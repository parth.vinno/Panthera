/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../../test.module';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { ExpectedTestDetailComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/expected-test/expected-test-detail.component';
import { ExpectedTestService } from '../../../../../../../main/webapp/app/entities/saq-admin/expected-test/expected-test.service';
import { ExpectedTest } from '../../../../../../../main/webapp/app/entities/saq-admin/expected-test/expected-test.model';

describe('Component Tests', () => {

    describe('ExpectedTest Management Detail Component', () => {
        let comp: ExpectedTestDetailComponent;
        let fixture: ComponentFixture<ExpectedTestDetailComponent>;
        let service: ExpectedTestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [ExpectedTestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ExpectedTestService,
                    JhiEventManager
                ]
            }).overrideTemplate(ExpectedTestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExpectedTestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ExpectedTestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ExpectedTest(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.expectedTest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
