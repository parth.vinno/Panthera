/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../../test.module';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { PcidssRequirementDetailComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/pcidss-requirement/pcidss-requirement-detail.component';
import { PcidssRequirementService } from '../../../../../../../main/webapp/app/entities/saq-admin/pcidss-requirement/pcidss-requirement.service';
import { PcidssRequirement } from '../../../../../../../main/webapp/app/entities/saq-admin/pcidss-requirement/pcidss-requirement.model';

describe('Component Tests', () => {

    describe('PcidssRequirement Management Detail Component', () => {
        let comp: PcidssRequirementDetailComponent;
        let fixture: ComponentFixture<PcidssRequirementDetailComponent>;
        let service: PcidssRequirementService;
        let mockRouter: any;

        beforeEach(async(() => {
            mockRouter = jasmine.createSpyObj('Router', ['navigate']);
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [PcidssRequirementDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Router,
                        useValue: mockRouter
                    },
                    PcidssRequirementService,
                    JhiEventManager
                ]
            }).overrideTemplate(PcidssRequirementDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PcidssRequirementDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PcidssRequirementService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PcidssRequirement(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pcidssRequirement).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
