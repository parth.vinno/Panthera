import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend,
    RequestMethod
  } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

import { PcidssTestModule } from '../../../../test.module';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { MockAlertService } from '../../../../helpers/mock-alert.service';
import { RequirementCategoryCreateComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category-create.component';
import { RequirementCategoryService } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.service';
import { RequirementCategory } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.model';

describe('Component Tests', () => {

    describe('RequirementCategory Create Component', () => {

        let comp: RequirementCategoryCreateComponent;
        let fixture: ComponentFixture<RequirementCategoryCreateComponent>;
        let reqCategoryService: RequirementCategoryService;
        let mockRouter;
        let mockSubscription;

        beforeEach(async(() => {
            mockRouter = jasmine.createSpyObj('Router', ['navigate']);
            mockSubscription = jasmine.createSpyObj('Subscription', ['unsubscribe']);
            TestBed.configureTestingModule({
                imports: [PcidssTestModule, ReactiveFormsModule, FormsModule],
                declarations: [RequirementCategoryCreateComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: Subscription,
                        useValue: mockSubscription
                    },
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 13})
                    },
                    {
                        provide: Router,
                        useValue: mockRouter
                    },
                    RequirementCategoryService,
                    JhiEventManager,
                    {
                        provide: JhiAlertService,
                        useValue: new MockAlertService()
                    },
                    { provide: XHRBackend, useClass: MockBackend }
                ]
            }).overrideTemplate(RequirementCategoryCreateComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RequirementCategoryCreateComponent);
            comp = fixture.componentInstance;
            reqCategoryService = fixture.debugElement.injector.get(RequirementCategoryService);
        });

        describe('Before OnInit', () => {

            it('should have a defined component', () => {
                fixture.detectChanges();
                expect(comp).toBeDefined();
            });
        });

        describe('OnInit', () => {

            it('Should successfully initialized', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp).toBeDefined()
            });

            it('Should give invalid when form is empty', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp.requirementCategoryForm.valid).toBeFalsy();
            });

            it('Should check validity of CategoryName field', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                let errors = {};
                let categoryName = comp.requirementCategoryForm.controls['categoryName'];
                expect(categoryName.valid).toBeFalsy();

                // categoryName field is required
                errors = categoryName.errors || {};
                expect(errors['required']).toBeTruthy();

                // Set categoryName to some test value
                categoryName.setValue('Regularly Monitor and Test Networks');
                errors = categoryName.errors || {};
                expect(errors['required']).toBeFalsy();
                expect(errors['minLength']).toBeFalsy();
                expect(errors['maxLength']).toBeFalsy();
                expect(errors['pattern']).toBeFalsy();
                expect(categoryName.valid).toBeTruthy();

                // Set categoryName to incorrect test value
                categoryName.setValue('T');
                errors = categoryName.errors || {};
                expect(errors['minlength']).toBeTruthy();
            });

            it('Should call save() to submit the form', () => {
                let requirementCategory : RequirementCategory;
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp.requirementCategoryForm.valid).toBeFalsy();
                comp.requirementCategoryForm.controls['categoryName'].setValue('Regularly Monitor and Test Networks');
                expect(comp.requirementCategoryForm.valid).toBeTruthy();

                comp.save(comp.requirementCategoryForm.value);
                fixture.detectChanges();
                expect(comp.requirementCategory.categoryName).toBe('Regularly Monitor and Test Networks');
                expect(comp.requirementCategory.id).toBe(undefined);
            });

            it('Should Post new Requirement Category while calling save() to submit the form', 
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                fixture.detectChanges();
                comp.ngOnInit();
                comp.requirementCategoryForm.controls['categoryName'].setValue('Regularly Monitor and Test Networks');
                expect(comp.requirementCategoryForm.valid).toBeTruthy();

                comp.save(comp.requirementCategoryForm.value);
                fixture.detectChanges();
                expect(comp.requirementCategory.id).toBe(undefined);

                mockBackend.connections.subscribe((connection) => {
                    expect(connection.request.method).toBe(RequestMethod.Post);
                    connection.mockRespond(new Response(new ResponseOptions({status: 201})));
                });
                requirementCategoryService.createMerchant(comp.requirementCategory).subscribe((successResult) => {
                    expect(successResult).toBeDefined();
                    expect(successResult.status).toBe(201);
                });
                mockRouter.navigate(['requirement-category']);
                fixture.detectChanges();
                expect(mockRouter.navigate).toBeDefined();
                expect(mockRouter.navigate).toHaveBeenCalledWith(['requirement-category']);

            }));
        });

        describe('Method previousState', () => {

            it('Should call previous state on cancel', () => {
                mockRouter.navigate(['requirement-category']);
                comp.previousState();
                fixture.detectChanges();
                expect(mockRouter.navigate).toBeDefined();
                expect(mockRouter.navigate).toHaveBeenCalledWith(['requirement-category']);
            });
        });
    });
});
