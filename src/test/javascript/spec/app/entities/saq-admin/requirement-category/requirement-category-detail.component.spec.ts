/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import { PcidssTestModule } from '../../../../test.module';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { RequirementCategoryDetailComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category-detail.component';
import { RequirementCategoryService } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.service';
import { RequirementCategory } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.model';

describe('Component Tests', () => {

    describe('RequirementCategory Detail Component', () => {
        let comp: RequirementCategoryDetailComponent;
        let fixture: ComponentFixture<RequirementCategoryDetailComponent>;
        let service: RequirementCategoryService;
        let mockRouter;
        let mockSubscription;

        beforeEach(async(() => {
            mockRouter = jasmine.createSpyObj('Router', ['navigate']);
            mockSubscription = jasmine.createSpyObj('Subscription', ['unsubscribe']);
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [RequirementCategoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: Subscription,
                        useValue: mockSubscription
                    },
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Router,
                        useValue: mockRouter
                    },
                    RequirementCategoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(RequirementCategoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RequirementCategoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RequirementCategoryService);
        });

        describe('Before OnInit', () => {
            it('should have a defined component', () => {
                fixture.detectChanges();
                expect(comp).toBeDefined();
            });

            it('should not show category before OnInit', () => {
                fixture.detectChanges();
                const spy = spyOn(service, 'findMerchant');
                expect(spy.calls.any()).toBe(false, 'findMerchant() not yet called');
            });
        });

        describe('OnInit', () => {
            it('Should successfully initialized', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp).toBeDefined()
                comp.ngOnDestroy();
            });

            it('Should call load all on init', () => {
                fixture.detectChanges();
                spyOn(service, 'findMerchant').and.returnValue(Observable.of(new RequirementCategory(2, 'Protect Cardholder Data', true)));
                comp.ngOnInit();
                expect(service.findMerchant).toHaveBeenCalledWith(123);
                expect(comp.requirementCategory).toEqual(jasmine.objectContaining({id: 2, categoryName: 'Protect Cardholder Data', isRequirementExist: true}));
            });

            it('Should call unsubscribe on Destory', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                fixture.detectChanges();
                mockSubscription.unsubscribe();
                comp.ngOnDestroy();
                expect(mockSubscription.unsubscribe).toBeDefined();
                expect(mockSubscription.unsubscribe).toHaveBeenCalled();
            });

            it('Should call registerChangeInRequirementCategories method', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                spyOn(comp, 'registerChangeInRequirementCategories');
                comp.registerChangeInRequirementCategories();
                expect(comp.registerChangeInRequirementCategories).toHaveBeenCalled();
            });
        });

        describe('Method previousState', () => {
            it('Should call previous state on cancel', () => {
                mockRouter.navigate(['requirement-category']);
                comp.previousState();
                fixture.detectChanges();
                expect(mockRouter.navigate).toBeDefined();
                expect(mockRouter.navigate).toHaveBeenCalledWith(['requirement-category']);
            });
        });
    });

});
