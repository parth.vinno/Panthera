import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend,
    RequestMethod
  } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { PcidssTestModule } from '../../../../test.module';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { MockAlertService } from '../../../../helpers/mock-alert.service';
import { PcidssRequirementService } from '../../../../../../../main/webapp/app/entities/saq-admin/pcidss-requirement/pcidss-requirement.service';
import { RequirementCategoryEditComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category-edit.component';
import { RequirementCategoryService } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.service';
import { RequirementCategory } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.model';

describe('Component Tests', () => {

    describe('RequirementCategory Edit Component', () => {

        let comp: RequirementCategoryEditComponent;
        let fixture: ComponentFixture<RequirementCategoryEditComponent>;
        let reqCategoryService: RequirementCategoryService;
        let reqService: PcidssRequirementService;
        let mockRouter;
        let mockSubscription;

        beforeEach(async(() => {
            mockRouter = jasmine.createSpyObj('Router', ['navigate']);
            mockSubscription = jasmine.createSpyObj('Subscription', ['unsubscribe']);
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [RequirementCategoryEditComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: Subscription,
                        useValue: mockSubscription
                    },
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 13})
                    },
                    {
                        provide: Router,
                        useValue: mockRouter
                    },
                    PcidssRequirementService,
                    RequirementCategoryService,
                    JhiEventManager,
                    {
                        provide: JhiAlertService,
                        useValue: new MockAlertService()
                    },
                    { provide: XHRBackend, useClass: MockBackend }
                ]
            }).overrideTemplate(RequirementCategoryEditComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RequirementCategoryEditComponent);
            comp = fixture.componentInstance;
            reqCategoryService = fixture.debugElement.injector.get(RequirementCategoryService);
            reqService = fixture.debugElement.injector.get(PcidssRequirementService);
        });

        describe('Before OnInit', () => {

            it('should have a defined component', () => {
                fixture.detectChanges();
                expect(comp).toBeDefined();
            });

            it('should not show category before OnInit', () => {
                fixture.detectChanges();
                const spy = spyOn(reqCategoryService, 'findMerchant');
                expect(spy.calls.any()).toBe(false, 'findMerchant() not yet called');
            });
        });

        describe('OnInit', () => {

            it('Should successfully initialized', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp).toBeDefined()
            });

            it('Should check requirement category value based on params', () => {
                fixture.detectChanges();
                spyOn(reqCategoryService, 'findMerchant').and.returnValue(Observable.of(new RequirementCategory(2, 'Protect Cardholder Data', true)));
                comp.ngOnInit();
                expect(comp.isSaving).toBe(false);
                expect(reqCategoryService.findMerchant).toHaveBeenCalledWith(13);
                expect(comp.requirementCategory).toEqual(jasmine.objectContaining({id: 2, categoryName: 'Protect Cardholder Data', isRequirementExist: true}));
            });
        });

        describe('Method previousState', () => {

            it('Should call previous state on cancel', () => {
                mockRouter.navigate(['requirement-category']);
                comp.previousState();
                fixture.detectChanges();
                expect(mockRouter.navigate).toBeDefined();
                expect(mockRouter.navigate).toHaveBeenCalledWith(['requirement-category']);
            });
        });

        describe('Method save', () => {

            it('Should save requirement category', inject([RequirementCategoryService], (requirementCategoryService) => {
                fixture.detectChanges();
                spyOn(requirementCategoryService, 'findMerchant').and.returnValue(Observable.of(new RequirementCategory(2, 'Protect Cardholder Data', true)));
                comp.ngOnInit();
                expect(comp.requirementCategory.id).toBeDefined();
                expect(comp.requirementCategory.id).toBe(2);
                comp.save();
                expect(comp.isSaving).toBe(true);
                requirementCategoryService.updateMerchant(comp.requirementCategory).subscribe((successResult) => {
                    expect(successResult).toBeDefined();
                    expect(successResult.status).toBe(204);
                });
            }));
        });
    });
});
