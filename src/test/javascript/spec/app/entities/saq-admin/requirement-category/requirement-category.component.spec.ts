import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiPaginationUtil, JhiParseLinks, JhiDataUtils, JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend,
    RequestMethod
  } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { PcidssTestModule } from '../../../../test.module';
import { Principal } from '../../../../../../../main/webapp/app/shared/auth/principal.service';
import { MockPrincipal } from '../../../../helpers/mock-principal.service';
import { MockActivatedRoute } from '../../../../helpers/mock-route.service';
import { MockAlertService } from '../../../../helpers/mock-alert.service';
import { PcidssRequirementService } from '../../../../../../../main/webapp/app/entities/saq-admin/pcidss-requirement/pcidss-requirement.service';
import { RequirementCategoryComponent } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.component';
import { RequirementCategoryService } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.service';
import { RequirementCategory } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.model';

describe('Component Tests', () => {

    describe('RequirementCategory Edit Component', () => {

        let comp: RequirementCategoryComponent;
        let fixture: ComponentFixture<RequirementCategoryComponent>;
        let reqCategoryService: RequirementCategoryService;
        let reqService: PcidssRequirementService;
        let mockRouter;
        let mockSubscription;

        beforeEach(async(() => {
            mockRouter = jasmine.createSpyObj('Router', ['navigate']);
            mockSubscription = jasmine.createSpyObj('Subscription', ['unsubscribe']);
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [RequirementCategoryComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    JhiParseLinks,
                    JhiPaginationUtil,
                    JhiEventManager,
                    DatePipe,
                    PcidssRequirementService,
                    RequirementCategoryService,
                    {
                        provide: Subscription,
                        useValue: mockSubscription
                    },
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({ id: 2 }, { pagingParams: { page: 2, ascending: true, predicate: 'id' }})
                    },
                    {
                        provide: Router,
                        useValue: mockRouter
                    },
                    {
                        provide: Principal,
                        useClass: MockPrincipal
                    },
                    {
                        provide: JhiAlertService,
                        useValue: new MockAlertService()
                    },
                    {
                        provide: XHRBackend,
                        useClass: MockBackend 
                    }
                ]
            }).overrideTemplate(RequirementCategoryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RequirementCategoryComponent);
            comp = fixture.componentInstance;
            reqCategoryService = fixture.debugElement.injector.get(RequirementCategoryService);
            reqService = fixture.debugElement.injector.get(PcidssRequirementService);
        });

        describe('Before OnInit', () => {

            it('should have a defined component', () => {
                fixture.detectChanges();
                expect(comp).toBeDefined();
            });
        });

        describe('OnInit', () => {

            it('Should successfully initialized', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                expect(comp).toBeDefined()
            });

            it('Should load All requirement category', inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                const mockResponse = {
                        data: [
                               { id: 1, categoryName: 'Protect Cardholder Data', isRequirementExist: false },
                               { id: 2, categoryName: 'Implement Strong Access Control Measures', isRequirementExist: false },
                               { id: 3, categoryName: 'Maintain an Information Security Policy', isRequirementExist: true }
                               ]
                };
                mockBackend.connections.subscribe((connection) => {
                    connection.mockRespond(new Response(new ResponseOptions({
                        body: JSON.stringify(mockResponse)
                    })));
                });
                fixture.detectChanges();
                comp.ngOnInit();
                requirementCategoryService.queryMerchant().subscribe((requirementCategory) => {
                    expect(requirementCategory.length).toBe(3);
                    expect(requirementCategory[0].categoryName).toEqual('Protect Cardholder Data');
                    expect(requirementCategory[1].categoryName).toEqual('Implement Strong Access Control Measures');
                    expect(requirementCategory[2].isRequirementExist).toEqual(true);
                });
            }));

            it('Should call unsubscribe on Destory', () => {
                fixture.detectChanges();
                comp.ngOnInit();
                fixture.detectChanges();
                mockSubscription.unsubscribe();
                comp.ngOnDestroy();
                expect(mockSubscription.unsubscribe).toBeDefined();
                expect(mockSubscription.unsubscribe).toHaveBeenCalled(); 
            });

            it('Should call transition()', () => {
                mockRouter.navigate(['requirement-category']);
                fixture.detectChanges();
                comp.ngOnInit();
                expect(mockRouter.navigate).toBeDefined();
                expect(mockRouter.navigate).toHaveBeenCalledWith(['requirement-category']);
            });
        });
    });
});
