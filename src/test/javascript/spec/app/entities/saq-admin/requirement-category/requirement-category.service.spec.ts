/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend,
    RequestMethod
  } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { PcidssTestModule } from '../../../../test.module';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { RequirementCategoryService } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.service';
import { RequirementCategory } from '../../../../../../../main/webapp/app/entities/saq-admin/requirement-category/requirement-category.model';

describe('RequirementCategory Service Tests', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
          imports: [PcidssTestModule],
          providers: [
            RequirementCategoryService,
            { provide: XHRBackend, useClass: MockBackend }
          ]
        });
      });

    describe('RequirementCategory queryMerchant()', () => {

        it('should return an Observable<Array<RequirementCategory>>',
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                    const mockResponse = {
                            data: [
                                   { id: 1, categoryName: 'Protect Cardholder Data', isRequirementExist: true },
                                   { id: 2, categoryName: 'Regularly Monitor and Test Networks', isRequirementExist: false },
                                   { id: 3, categoryName: 'Maintain an Information Security Policy', isRequirementExist: true }
                                   ]
                    };
                    mockBackend.connections.subscribe((connection) => {
                        connection.mockRespond(new Response(new ResponseOptions({
                            body: JSON.stringify(mockResponse)
                        })));
                    });
                    requirementCategoryService.queryMerchant().subscribe((requirementCategory) => {
                        expect(requirementCategory.length).toBe(3);
                        expect(requirementCategory[0].categoryName).toEqual('Protect Cardholder Data');
                        expect(requirementCategory[1].id).toEqual(2);
                        expect(requirementCategory[2].isRequirementExist).toEqual(true);
                    });
        }));

        it('should fetch a RequirementCategory by a id',
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                    const mockResponse = {
                            data: [
                                   { id: 1, categoryName: 'Protect Cardholder Data', isRequirementExist: true }
                                   ]
                    };
                    mockBackend.connections.subscribe((connection) => {
                        expect(connection.request.url).toMatch(/\/api\/merchant\/requirementCategories\/1/);
                        connection.mockRespond(new Response(new ResponseOptions({
                            body: JSON.stringify(mockResponse)
                        })));
                    });
                    requirementCategoryService.findMerchant(1).subscribe(
                            (requirementCategory) => {
                                expect(requirementCategory.id).toBe(1);
                                expect(requirementCategory.categoryName).toBe('Protect Cardholder Data');
                                expect(requirementCategory.isRequirementExist).toBe(true);
                    });
        }));

        it('should insert new Requirement Category',
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                    mockBackend.connections.subscribe((connection) => {
                        expect(connection.request.method).toBe(RequestMethod.Post);
                        connection.mockRespond(new Response(new ResponseOptions({status: 201})));
                    });
                    const requirementCategoryData: RequirementCategory = new RequirementCategory(undefined, 'Protect Cardholder Data', true);
                    requirementCategoryService.createMerchant(requirementCategoryData).subscribe((successResult) => {
                        expect(successResult).toBeDefined();
                        expect(successResult.status).toBe(201);
                    });
         }));

        it('should save updates to an existing Requirement Category',
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                    mockBackend.connections.subscribe((connection) => {
                        expect(connection.request.method).toBe(RequestMethod.Put);
                        connection.mockRespond(new Response(new ResponseOptions({status: 204})));
                    });
                    const requirementCategoryData: RequirementCategory = new RequirementCategory(3, 'Maintain an Information Security Policy', true);
                    requirementCategoryService.updateMerchant(requirementCategoryData).subscribe((successResult) => {
                                expect(successResult).toBeDefined();
                                expect(successResult.status).toBe(204);
                    });
        }));

        it('should delete an existing Requirement Category',
                inject([RequirementCategoryService, XHRBackend], (requirementCategoryService, mockBackend) => {
                    const mockResponse = {
                            data: [
                                   { id: 1, categoryName: 'Protect Cardholder Data', isRequirementExist: true }
                                   ]
                    };
                    mockBackend.connections.subscribe((connection) => {
                        expect(connection.request.method).toBe(RequestMethod.Delete);
                        connection.mockRespond(new Response(new ResponseOptions({status: 204})));
                        connection.mockRespond(new Response(new ResponseOptions({
                            body: JSON.stringify(mockResponse)
                        })));
                    });
                    requirementCategoryService.deleteMerchant(1).subscribe(
                            (successResult) => {
                                expect(successResult).toBeDefined();
                                expect(successResult.status).toBe(204);
                    });
        }));
    });
});
