/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SaqResponseDetailComponent } from '../../../../../../main/webapp/app/entities/saq-response/saq-response-detail.component';
import { SaqResponseService } from '../../../../../../main/webapp/app/entities/saq-response/saq-response.service';
import { SaqResponse } from '../../../../../../main/webapp/app/entities/saq-response/saq-response.model';

describe('Component Tests', () => {

    describe('SaqResponse Management Detail Component', () => {
        let comp: SaqResponseDetailComponent;
        let fixture: ComponentFixture<SaqResponseDetailComponent>;
        let service: SaqResponseService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [SaqResponseDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SaqResponseService,
                    JhiEventManager
                ]
            }).overrideTemplate(SaqResponseDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SaqResponseDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SaqResponseService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SaqResponse(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.saqResponse).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
