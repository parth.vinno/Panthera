/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PcidssTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SelfAssessmentComponent } from '../../../../../../main/webapp/app/entities/self-assessment';
import { SelfAssessmentService } from '../../../../../../main/webapp/app/shared';

describe('Component Tests', () => {

    describe('SelfAssessment Management Detail Component', () => {
        let comp: SelfAssessmentComponent;
        let fixture: ComponentFixture<SelfAssessmentComponent>;
        let service: SelfAssessmentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PcidssTestModule],
                declarations: [SelfAssessmentComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SelfAssessmentService,
                    JhiEventManager
                ]
            }).overrideTemplate(SelfAssessmentComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SelfAssessmentComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SelfAssessmentService);
        });

        describe('SelfAssessment', () => {
            it('should create', () => {
                expect(comp).toBeTruthy();
            });
        });
    });

});
