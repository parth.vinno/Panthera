import { SpyObject } from './spyobject';
import { JhiAlertService, JhiAlert } from 'ng-jhipster';
import Spy = jasmine.Spy;

export class MockAlertService extends SpyObject {

    fakeToast: boolean;
    constructor() {
        super(JhiAlertService);

        this.fakeToast = true;
    }

    success(msg: string, params?: any, position?: string): JhiAlert {
        return;
    }

    error(msg: string, params?: any, position?: string): JhiAlert {
        return;
    }

    warning(msg: string, params?: any, position?: string): JhiAlert {
        return;
    }

    info(msg: string, params?: any, position?: string): JhiAlert {
        return;
    }

    addAlert(alertOptions: JhiAlert, extAlerts: JhiAlert[]): JhiAlert {
        return;
    }

    closeAlert(id: number, extAlerts?: JhiAlert[]): any { 
        return;
    }

    closeAlertByIndex(index: number, thisAlerts: JhiAlert[]): JhiAlert[] {
            return;
    }

    isToast(): boolean {return;
    }
}
