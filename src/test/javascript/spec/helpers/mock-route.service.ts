import { ActivatedRoute, Router, Data } from '@angular/router';
import { Observable } from 'rxjs';

export class MockActivatedRoute extends ActivatedRoute {

    constructor(parameters?: any, data?: Data) {
        super();
        this.queryParams = Observable.of(parameters);
        this.params = Observable.of(parameters);
        this.data = Observable.of(data);
    }
}
